<?php


define('BASE_FOLDER', '/share/Dochazka/Alveno');

define('DSI_KEY', 'meduse');
define('DSI_FOLDER', 'CteckyZaznamy');
define('DSI_SUFFIX', ' DSi čtečka.txt');

define('PSI_1_KEY', 'meduse_vyroba');
define('PSI_1_FOLDER', 'PristupyZaznamy');
define('PSI_1_SUFFIX', ' MeduseVyroba.txt');

define('PSI_2_KEY', 'medite');
define('PSI_2_FOLDER', 'PristupyZaznamy');
define('PSI_2_SUFFIX', ' MEDITE.txt');

function ends_with($haystack, $needle)
{
  $length = strlen($needle);
  if ($length == 0) {
    return TRUE;
  }
  return (substr($haystack, -$length) === $needle);
}

function main($key, $from)
{

  $timestamp = NULL;
  $last = $timestamp;
  if ($from) {
    try {
      if ($dt = DateTime::createFromFormat('YmdHis', $from)) {
        $timestamp = $dt->format('Y-m-d H:i:s');
      }
    } catch (Exception $e) {
      die($e->getMessage());
    }
  }
  if (!$timestamp) {
    die('Nebylo zadano spravne datum.');
  }

  $folder = NULL;
  $suffix = NULL;
  switch ($key) {
    case DSI_KEY:
      $folder = BASE_FOLDER . '/' . DSI_FOLDER;
      $suffix = DSI_SUFFIX;
      break;
    case PSI_1_KEY:
      $folder = BASE_FOLDER . '/' . PSI_1_FOLDER;
      $suffix = PSI_1_SUFFIX;
      break;
    case PSI_2_KEY:
      $folder = BASE_FOLDER . '/' . PSI_2_FOLDER;
      $suffix = PSI_2_SUFFIX;
      break;
    default:
      die('Nespravny identifikator terminalu.');
  }

  $files = array();
  $dir = dir($folder);
  while ($item = $dir->read()) {
    if (ends_with($item, $suffix)) {
      $file_time = substr($item, 0, 19);
      if ($file_time > $timestamp) {
        $files[] = $item;
        $last = $file_time > $last ? $file_time : $last;
      }
    }
  }

  $records = array();
  if ($files) {
    asort($files);
    foreach ($files as $filename) {
      $content = file($folder . '/' . $filename);
      $records = array_merge($records, $content);
    }

  }

  echo implode('', $records);
  echo $last;
}

// Only for debug - parameters are taken from command line.
if (defined('STDIN')) {
  $key = $argv[1];
  $from = $argv[2];
} else {
  $key = filter_input(INPUT_GET, 'c', FILTER_SANITIZE_STRING);
  $from = filter_input(INPUT_GET, 'from', FILTER_SANITIZE_NUMBER_INT);
}


main($key, $from);

