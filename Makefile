# Include variables
include .env.dist
-include .env
export

docker-up: ## Run docker stack
	docker-compose up

docker-down: ## Stop docker stack
	docker-compose down

docker-in: ## Enter docker container
	docker-compose exec web bash

db-import: ## Import sql file (DB_IMPORT_FILE=import.sql)
	docker-compose exec -T database mysql -uroot -p$(DB_ROOT_PASSWORD) $(DB_DATABASE) < $(DB_IMPORT_FILE)
