<?php
/**
 * backend pro manipulaci s daty pri multieditaci produktu
 */
class Products_MultiEdit {

	const EDIT = 1;		//UPRAVA PRODUKTU
	const DELETE = 2;	//MAZANI SOUCASTI Z PRODUKTU
	const ADD = 3;		//PRIDANI SOUCASTI DO PRODUKTU
	const DELETE_WRAP = 4;
	const ADD_WRAP = 5;
  
	/**
	 * krok multieditace
	 * @var int
	 */
	private $step;
	private $part_id;
	private $part_amount = null;
	private $new_part_amount = null;
	private $product_id;
	private $action;
	private $all_products;
	private $wrap_id;
  
	public function  __construct() {
		$this->setFromSession();
	}

	/**
	 * Nastavi akci, ktera se ma provadet v ramci multieditace
	 * prijma jako parametr vlastni konstanty
	 * @param int $action
	 */
	public function setAction($action) {
		$this->action = (int) $action;
	}

	public function setProducts(array $products) {
		$this->all_products = $products;
	}

	public function setPart($part_id){
		$this->part_id = $part_id;
	}

	public function setWrap($wrap_id){
		$this->wrap_id = $wrap_id;
	}

	/**
	 * Ma vyznam jen v pripade, ze se meni pocet soucasti v produktu
	 * @param $amount
	 */
	public function setPartAmount($amount) {
		$this->part_amount = $amount;
	}

	/**
	 * Ma vyznam jen v pripade, ze se meni pocet soucasti v produktu nebo pridava novy produkt
	 * @param $amount
	 */
	public function setNewPartAmount($amount) {
		$this->new_part_amount = $amount;
	}

	public function setProduct($product_id) {
		$this->product_id = $product_id;
	}

	public function setStep($step) {
		$this->step = (int) $step;
	}

	public function getAction(){
		return $this->action;
	}

	public function getPart(){
		return $this->part_id;
	}

	public function getWrap(){
		return $this->wrap_id;
	}

	public function getPartAmount(){
		return $this->part_amount;
	}

	public function getNewPartAmount(){
		return $this->new_part_amount;
	}

	public function getProduct(){
		return $this->product_id;
	}

	public function getSelectedProducts(){
		return (count($this->all_products) > 0 ? $this->all_products : false);
	}
  
	public function getPossibleChangedProducts(){
		$db = Zend_Registry::get('db');
		switch((int) $this->action) {
			case self::EDIT:
				$tmpPart = new Parts_Part($this->product_id);
        $subSelect = new Zend_Db_Select($db);
        $subSelect
          ->from('products_parts')
          ->where('id_products = p.id')
          ->where('id_parts = ?', $this->part_id)
          ->where('amount = ?', $tmpPart->getPartAmount($this->part_id));
        $select = new Zend_Db_Select($db);
        $select
          ->from(array('p' => 'products'), array(
            'id',
            'name',
            'description' => new Zend_Db_Expr('IFNULL(description, "")'),
          ))
          ->where('id != ?', $this->part_id)
          ->where('deleted = ?', 'n')
          ->where('EXISTS ?', $subSelect);
        return $select->query()->fetchAll();
				break;
			case self::DELETE:
        $subSelect = new Zend_Db_Select($db);
        $subSelect
          ->from('parts_subparts')
          ->where('id_multipart = p.id')
          ->where('id_parts = ?', $this->part_id);
        $select = new Zend_Db_Select($db);
        $select
          ->from(array('p' => 'parts'), array(
            'id', 
            'name', 
            'description' => new Zend_Db_Expr('IFNULL(description, "")'),
          ))
          ->where('id != ?', $this->part_id)
          ->where('deleted = ?', 'n')
          ->where('EXISTS ?', $subSelect);
        return $select->query()->fetchAll();
				break;
      case self::ADD:
        $subSelect1 = new Zend_Db_Select($db);
        $subSelect1
          ->from('parts_collections', array('id_collections'))
          ->where('id_parts = ?', $this->product_id);
        $subSelect2 = new Zend_Db_Select($db);
        $subSelect2
          ->from('parts', array('id_parts_ctg'))
          ->where('id = ?', $this->product_id)
          ->limit(1);
        $subSelect3 = new Zend_Db_Select($db);
        $subSelect3
          ->from('parts_subparts')
          ->where('id_parts = ?', $this->part_id)
          ->where('id_multipart = p.id');
        $subSelect4 = new Zend_Db_Select($db);
        $subSelect4
          ->from('parts_collections', array(
            new Zend_Db_Expr('GROUP_CONCAT(id_collections)')
          ))
          ->where('id_parts = p.id')
          ->group('id_parts');      
        $select = new Zend_Db_Select($db);
        $select
          ->from(array('p' => 'parts'), array(
            'id', 
            'name', 
            'description' => new Zend_Db_Expr('IFNULL(description, "")'),
          ))
          ->where('deleted = ?', 'n')
          ->where($db->quote($subSelect4) . ' IN ?', $subSelect1)
          ->where('id_parts_ctg = ?', $subSelect2)
          ->where('NOT EXISTS ?', $subSelect3);   
        return $select->query()->fetchAll();
				break;
			default:
				break;
		}
	}

	public function saveToSession(){
		$sess = new Zend_Session_Namespace('multi_edit');
		switch((int) $this->step) {
      case 1:
				$sess->unsetAll();
				break;
      case 2:
				$sess->unsetAll();
				$sess->product = $this->product_id;
				break;
      case 3:
				$prod = $sess->product;
				$sess->unsetAll();
				$sess->product = $prod;
				$sess->part = $this->part_id;
				$sess->action = $this->action;
				$sess->wrap = $this->wrap_id;
				if(!is_null($this->part_amount)){
					$sess->part_amount = $this->part_amount;
				}
				if(!is_null($this->new_part_amount)){
					$sess->new_part_amount = $this->new_part_amount;
				}
				break;
      case 4:
				$sess->all_products = implode('+',$this->all_products);
				break;
      default:
				throw new Exception($this->step." neznamy krok");
				break;
		}
	}

	public function setFromSession(){
		$sess = new Zend_Session_Namespace('multi_edit');
		if ($sess->product && !is_null($sess->product)) {
      $this->product_id	= $sess->product; 
    }
		if ($sess->part && !is_null($sess->part)) {
      $this->part_id = $sess->part;
		}
    if ($sess->wrap && !is_null($sess->wrap)) {
      $this->wrap_id = $sess->wrap;
		}
    if ($sess->action && !is_null($sess->action)) {
      $this->action	= $sess->action;
		}
    if ($sess->part_amount && !is_null($sess->part_amount)) {
      $this->part_amount = $sess->part_amount;
		}
    if ($sess->new_part_amount && !is_null($sess->new_part_amount)) {
      $this->new_part_amount = $sess->new_part_amount;
		}
    if ($sess->all_products && !is_null($sess->all_products)) {
      $this->all_products		= explode('+',$sess->all_products);
    }
	}

}
