<?php
/**
 * Ke smazani
 */
class Products_Product{
	
	private $_id;
	
	private $_id_collections;
	
	private $_id_products_types;
	
	private $_name;
	
	private $_description;
	
	private $_parts = NULL;
	
	private $_wraps = NULL;
	
	public function __construct($id, $amount = 1){
		$table_products = new Table_Products();
		$table_products_parts = new Table_ProductsParts();
//		$table_products_wraps = new Table_ProductsWraps();
		
		//////
		// Detail produktu
		
		if(!($row = $table_products->find($id)->current())){ 
			throw new Exception("Pokus vytvořit produkt s id=$id se nezdařil. Id nenalezeno v databázi.");
		}
		
		$this->_id = $row['id'];
		$this->_name = $row['name'];
		$this->_description = $row['description'];
		$this->_amount = $amount;
		$this->_id_products_types = $row['id_products_types'];
		$this->_id_collections = $row['id_collections'];
		
		//////
		// Soucasti produktu
		
		$this->_parts = array();
		
		$select = $table_products_parts->select();
		$select->setIntegrityCheck(false);
		$select->from(array("pp" => "products_parts"), array('amount' => 'SUM(pp.amount)', 'id_parts'))
		->joinLeft(array("p" => "parts"), "p.id = pp.id_parts", array("id_parts_ctg"))
		->where("pp.id_products LIKE '".$this->_id."'") //soucasti prirazene primo produktu
		//->orWhere("pp.id_products IN (SELECT id_parts_set FROM products_parts_set WHERE id_products LIKE '".$this->_id."')") //soucasti setu, ktery je prirazen produktu
		->order(array("id_parts_ctg", "pp.id_parts"))
		->group('pp.id_parts'); //slouceni soucasti produktu a soucasti setu produktu
		$rows = $table_products_parts->fetchAll($select);
		foreach($rows as $tmp){
			$this->_parts[] = new Parts_Part($tmp->id_parts, $tmp->amount);
		}
		
		//////
		// Obaly produktu
		
		$this->_wraps = array();
		
//		$select = $table_products_wraps->select();
//		$select->from(array("pw" => "products_wraps"))
//		->where("pw.id_products LIKE '".$this->_id."'")
//		->order("pw.id_wraps")
//		;
//
//		$rows = $table_products_wraps->fetchAll($select);
//		foreach($rows as $tmp){
//			$this->_wraps[] = new Wraps_Wrap($tmp->id_wraps, $tmp->wrap_amount);
//		}
				
		
	}
	
	public function getName(){
		return $this->_name;
	}
	
	public function getID(){
		return $this->_id;
	}
	
	public function getDesc(){
		return $this->_description;
	}
	
	public function getType(){
		return $this->_id_products_types;
	}
	
	public function getTypeName(){
		$product_types = new Table_ProductsTypes();
		if(!($row = $product_types->find($this->_id_products_types)->current())){
			throw new Exception("Neexistuje typ produktu id=".$this->_id_products_types." ");
		}
		return $row['type_name'];
	}
	
	public function getCollection(){
		return $this->_id_collections;
	}
	
	public function getCollectionName(){
		if($this->_id_collections === NULL) return '';
		$db = Zend_Registry::get('db');
		$select = $db->select()->from("collections")->where("id = ?", $this->_id_collections);
		if($row = $db->fetchRow($select))
			return $row['name'];
		else return '';
	}
	
	/**
	 * Vraci soucasti produktu vcetne soucasti obsazenych v prirazenych setech
	 * @return array[][array('id' => $v, 'name' => $v, 'amount' => $v, 'ctg' => $v)]
	 */
	public function getParts(){
		$data = array();
		foreach($this->_parts as $part){
			$data[] = array(
				'id'     => $part->getID(),
			    'name'   => $part->getName(),
				'amount' => $part->getAmount(),
				'ctg'    => $part->getCtg(),
			);
		}
		return $data;
	}
	
	/**
	 * Vraci prime soucasti primo navazane na produkt
	 * @return unknown_type
	 */
	public function getDirectChildParts(){
		$table_products_parts = new Table_ProductsParts();
		$select = $table_products_parts->select();
		$select->setIntegrityCheck(false);
		$select->from(array("pp" => "products_parts"), array("amount", "id_parts"))
		->joinLeft(array("p" => "parts"), "p.id = pp.id_parts", array("name","id_parts_ctg"))
		->where("pp.id_products LIKE '".$this->_id."'") //soucasti prirazene primo produktu
		->order(array("id_parts_ctg", "pp.id_parts"));
		
		$data = array();
		$rows = $table_products_parts->fetchAll($select);
		foreach($rows as $tmp){
			$data[] = array(
				'id'     => $tmp->id_parts,
			    'name'   => $tmp->name,
				'amount' => $tmp->amount,
				'ctg'    => $tmp->id_parts_ctg,
			);
		}
		if(count($data) < 1) return NULL;
		return $data;
	}
	
	/**
	 * Vraci soucasti navazane na sety prirazene produktu (neprimo vazane soucasti)
	 * @return unknown_type
	 */
	public function getSubSetsParts(){
		$table_products_parts = new Table_ProductsParts();
		$select = $table_products_parts->select();
		$select->setIntegrityCheck(false);
		$select->from(array("pp" => "products_parts"), array("id_products","amount", "id_parts"))
		->joinLeft(array("p" => "parts"), "p.id = pp.id_parts", array("name","id_parts_ctg"))
		//->where("pp.id_products IN (SELECT id_parts_set FROM products_parts_set WHERE id_products LIKE '".$this->_id."')") //soucasti setu, ktery je prirazen produktu
		->order(array("id_parts_ctg", "pp.id_parts"));
		
		$data = array();
		$rows = $table_products_parts->fetchAll($select);
		foreach($rows as $tmp){
			$data[$tmp->id_products][] = array(
				'id'     => $tmp->id_parts,
			    'name'   => $tmp->name,
				'amount' => $tmp->amount,
				'ctg'    => $tmp->id_parts_ctg,
			);
		}
		if(count($data) < 1) return NULL;
		return $data;
	}
	
	public function getPartAmount($id_part){
		$data = array();
		foreach($this->_parts as $part){
			if($part->getID() == $id_part)
				return $part->getAmount();
		}
		return 0;
	}
	
	/*
	public function getWraps(){
		$data = array();
		foreach($this->_wraps as $wrap){
			$data[] = array(
				'id'     => $wrap->getID(),
			    'description'   => $wrap->getDesc(),
				'amount' => $wrap->getAmount(),
				'ctg'    => $wrap->getCtg(),
			);
		}
		return $data;
	}*/
	
	/**
	 * Vrati vsechny obaly prirazene produktu - i duplicitni id_wraps
	 * @return unknown_type
	 */
//	public function getWraps(){
//		$products_wraps = new Table_ProductsWraps();
//		$select = $products_wraps->select();
//		$select->setIntegrityCheck(false);
//		$select->from(array("pw" => "products_wraps"), array("id_parts_wraps" => "id", "id_wraps", "wrap_amount"))
//		->joinLeft(array("w" => "wraps"), "pw.id_wraps LIKE w.id")
//		->where("id_products LIKE '".$this->_id."'")
//		->order('id_wraps');
//
//		$rows = $products_wraps->fetchAll($select);
//		$data = array();
//		foreach($rows as $tmp){
//			$data[] = array(
//				"id" => $tmp->id_parts_wraps,
//				"id_wraps" => $tmp->id_wraps,
//				"name" => $tmp->name,
//				"amount" => $tmp->wrap_amount,
//			);
//		}
//		return $data;
//	}
	
	/**
	 * vraci pocty obalu v produktu
	 * @param $id id obalu
	 * @return unknown_type
	 */
//	public function getWrapsCounts(){
//		$products_wraps = new Table_ProductsWraps();
//		$select = $products_wraps->select();
//		$select->setIntegrityCheck(false);
//		$select->from(array("pw" => "products_wraps"), array("amount" => "SUM(pw.wrap_amount)"))
//		->joinLeft(array("w" => "wraps"), "pw.id_wraps LIKE w.id", array("id", "name", "amount_unit"))
//		->where("pw.id_products LIKE '".$this->_id."'")
//		->group('pw.id_wraps')
//		->order('pw.id_wraps');
//
//		$rows = $products_wraps->fetchAll($select);
//		$data = array();
//		foreach($rows as $tmp){
//			$data[] = array(
//				"id" => $tmp->id,
//				"name" => $tmp->name,
//				"amount" => $tmp->amount,
//				"amount_unit" => $tmp->amount_unit,
//			);
//		}
//		return $data;
//	}
	
	/**
	 * Vraci soucasti prirazene obalu
	 * @param $id - id vazby products_wraps
	 * @return unknown_type
	 */
//	public function getWrapParts($id){
//		$product_wrap_parts = new Table_ProductsWrapsParts();
//		$select = $product_wrap_parts->select();
//		$select->setIntegrityCheck(false);
//		$select->from(array("pwp" => "products_wraps_parts"))
//		->joinLeft(array("p" => "parts"), "pwp.id_parts LIKE p.id", array("name"))
//		->where("pwp.id_products_wraps LIKE '".$id."'")
//		->order('pwp.id_parts');
//		if(count($rows = $product_wrap_parts->fetchAll($select)) < 1){
//			//return NULL;
//		}
//
//		//echo $select; die();
//		//$rows = $product_wrap_parts->fetchAll($select);
//		$data = array();
//		foreach($rows as $tmp){
//			$data[] = array(
//				"id_parts" => $tmp['id_parts'],
//				"name" => $tmp['name'],
//				"amount" => $tmp['amount']
//			);
//		}
//
//		//sety
//		$select = $product_wrap_parts->select();
//		$select->setIntegrityCheck(false);
//		$select->from(array("pws" => "products_wraps_sets"))
//		->joinLeft(array("p" => "products"), "pws.id_products LIKE p.id", array("name"))
//		->where("pws.id_products_wraps LIKE ?",$id);
//		//echo $select; // die();
//		if(count($rows = $product_wrap_parts->fetchAll($select)) > 0){
//			foreach($rows as $tmp){
//				$data[] = array(
//				"id_parts" => $tmp['id_products'],
//				"name" => $tmp['name'],
//				"amount" => $tmp['amount']
//			);
//			}
//		}
//
//		if(count($data) == 0) return null;
//
//		//Zend_Debug::dump($data); exit();
//		return $data;
//	}
	
//	public function getNoWrapedParts(){
//		return NULL;
//		$data = array();
//		foreach($this->_parts as $part){
//			$data[] = array(
//				'id' => $part->getID(),
//			    'name' => $part->getName(),
//				'amount' => $part->getAmount(),
//			);
//		}
//
//		$wraps = new Table_ProductsWraps();
//
//		$select = $wraps->select(array("id_part"))->where("id_products = ?",$this->_id);
//		$rows = $wraps->fetchAll($select);
//		//print_r($data);
//		foreach($rows as $tmp){
//			foreach($data as $key => $tmp2){
//				if($tmp2['id'] == $tmp->id_part) unset($data[$key]);
//			}
//		}
//		//echo "<hr>";print_r($data); die();
//		return $data;
//	}
	
	public function deletePart($id){
		$products_parts = new Table_ProductsParts();
		$products_parts->delete("id_products LIKE '".$this->_id."' AND id_parts LIKE '".$id."'");
	}
	
	public function deleteSubSet($set_id){
		$kontrola = new Products_Product($set_id);
		$db = Zend_Registry::get('db');
		$db->delete('products_parts_set', "id_products like '".$this->_id."' and id_parts_set like '".$kontrola->getID()."'");
	}
	
	public function deleteWrap($id){
		$product_wrap_parts = new Table_ProductsWrapsParts();
		$product_wrap = new Table_ProductsWraps();
		$db = Zend_Registry::get('db');
		
		$product_wrap_parts->delete("id_products_wraps LIKE '".$id."'");
		$db->delete("products_wraps_sets","id_products_wraps LIKE '".$id."'");
		$product_wrap->delete("id LIKE '".$id."'");
	}
	
	public function delete(){
		$product_parts = new Table_ProductsParts();
		$product = new Table_Products();
		$product_wrap_parts = new Table_ProductsWrapsParts();
		$product_wrap = new Table_ProductsWraps();
		$db = Zend_Registry::get('db');
		
		$product_wrap_parts->delete("id_products_wraps IN (SELECT id FROM products_wraps WHERE id_products LIKE '".$this->_id."')");
		$product_wrap->delete("id_products LIKE '".$this->_id."'");
		//$product_parts->delete("id_products LIKE '".$this->_id."'");
		//$db->delete('products_all_parts', "id_products LIKE '".$this->_id."'");
		//$db->delete('products_parts_set', "id_products LIKE '".$this->_id."'");
		$product->update(array('deleted' => 'y'), "id = '".$this->_id."'");
		$db->update("parts", array("product" => "n"), "id = '".$this->_id."'");
	}
	
	/**
	 * Ulozi produkt $parent_id jako produkt $id, $name, $type, $desc
	 * @param $parent_id
	 * @param $id
	 * @param $name
	 * @param $type
	 * @param $desc
	 * @return unknown_type
	 */
	public function saveAs($parent_id, $id, $name, $type, $collection, $desc){
		$db = Zend_Registry::get('db');
		try{
			$db->beginTransaction();
			$products_table = new Table_Products();
			$products_table->addProduct($id, $name, $type, $collection, $desc);
			
			//kopirovani soucasti (products_parts)
			$products_parts = new Table_ProductsParts();
			$select = $products_parts->select()->where("id_products LIKE ?", $parent_id);
			$rows = $products_parts->fetchAll($select);
			foreach($rows as $row){
				$products_parts->insert(array(
					"id_products" => $id,
					"id_parts" => $row["id_parts"],
					"amount" => $row["amount"]
				));
			}
			
			//kopirovani soucasti (products_all_parts)
			$products_all_parts = new Table_ProductsAllParts();
			$select = $products_all_parts->select()->where("id_products LIKE ?", $parent_id);
			$rows = $products_all_parts->fetchAll($select);
			foreach($rows as $row){
				$products_all_parts->insert(array(
					"id_products" => $id,
					"id_parts" => $row["id_parts"],
					"amount" => $row["amount"]
				));
			}
			
			//kopirovani obalu (parts_wraps) a soucasti v obalu (products_wraps_parts)
//			$products_wraps = new Table_ProductsWraps();
//			$products_wraps_parts = new Table_ProductsWrapsParts();
//			$select = $products_wraps->select()->where("id_products LIKE ?", $parent_id);
//			$rows = $products_wraps->fetchAll($select);
//			foreach($rows as $row){ // kopirovani obalu
//				$id_wraps = $products_wraps->insert(array(
//					"id_products" => $id,
//					"id_wraps" => $row["id_wraps"],
//					"wrap_amount" => $row["wrap_amount"]
//				));
//				$select2 = $products_wraps_parts->select()->where("id_products_wraps = ?", $row["id"]);
//				$rows2 = $products_wraps_parts->fetchAll($select2);
//				foreach($rows2 as $row2){ //kopirovani soucasti obalu
//					$products_wraps_parts->insert(array(
//						"id_products_wraps" => $id_wraps,
//						"id_parts" => $row2["id_parts"],
//						"amount" => $row2["amount"]
//					));
//				}
//			}
			
			//kopirovani setu soucasti
			/*
			$select = $db->select()->from("products_parts_set")->where("id_products LIKE ?", $parent_id);
			$rows = $db->fetchAll($select);
			foreach($rows as $row){
				$db->insert("products_parts_set", array(
					"id_products" => $id,
					"id_parts_set" => $row['id_parts_set']
				));
			}
			*/
			
			$db->commit();
		} catch ( Exception $e ) {
			$db->rollback();
			throw new exception ("Nepodarilo se ulozit produkt $parent_id jako $id, $name, $type, $desc. ".$e->getMessage());
		}
	}
	
}
