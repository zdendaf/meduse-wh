<?php
class Products_EditForm extends Meduse_Form {
	
	public function __construct ($options = null){
        parent::__construct('parts_box', 'Upravit produkt', $options);
        
		$element = new Meduse_Form_Element_Text('meduse_products_name');
		$element->setRequired(true);
		$element->setLabel('Název');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Select('meduse_products_collection');
		$element->addMultiOption(NULL, '-- žádná --');
		$element->addMultiOptions(Zend_Registry::get('db')->fetchPairs('SELECT id,name FROM collections'));
		$element->setLabel('Kolekce');
		$this->addElement($element);
	
		$element = new Meduse_Form_Element_Select('meduse_products_type');
		$element->setRequired(true);
		$element->addMultiOption(NULL, '-- žádný --');
		$element->addMultiOptions(Zend_Registry::get('db')->fetchPairs('SELECT id,type_name FROM products_types'));
		$element->setLabel('Typ');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Textarea('meduse_products_desc');
		$element->setLabel('Poznámka');
		$element->setAttrib("cols", 25);
		$element->setAttrib("rows", 6);
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);
		
	}
}
