<?php

class EmailTemplates_SendmailForm extends Zend_Form {
	protected $id_wh_type = 1;
  
  
	public function init() {

		$this->setName('sendmailForm');
		$this->setAction('/orders/notify');
		$this->setMethod(self::METHOD_POST);
		
		$element = new Zend_Form_Element_Hidden('order_id');
		$this->addElement($element);
		
		$element = new Zend_Form_Element_Hidden('customer_id');
		$this->addElement($element);
		
		$element = new Zend_Form_Element_Select('template');
		$element->addMultiOption('0', 'Bez šablony');
		$emailTemplates = new Table_EmailTemplates();
		$templates = $emailTemplates->getList($this->id_wh_type);
		foreach ($templates as $template) {
			$element->addMultiOption($template['id'], $template['name']);
		}
		$element->setLabel('Šablona e-mailu');
		$this->addElement($element);

		$element = new Zend_Form_Element('email_to');
		$element->addValidator(new Zend_Validate_EmailAddress());
		$element->setAttrib('class', 'span11');
		$element->setLabel('E-mailová adresa zákazníka');
		$this->addElement($element);
		
		$element = new Zend_Form_Element_Text('email_subject');
		$element->setAttrib('class', 'span11');
		$element->setLabel('Předmět e-mailu');
		$this->addElement($element);
		
		$element = new Zend_Form_Element_Textarea('email_body');
		$element->setAttrib('rows', '6');
		$element->setAttrib('class', 'span11');
		$element->setLabel('Text e-mailu');
		$this->addElement($element);
				
		EasyBib_Form_Decorator::setFormDecorator(
			$this, EasyBib_Form_Decorator::BOOTSTRAP_MINIMAL
        );
	}
}