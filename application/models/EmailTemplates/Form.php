<?php

class EmailTemplates_Form extends Zend_Form {
  protected $id_wh_type = 1;

  public function init() {
		
		$element = new Zend_Form_Element_Hidden('email_template_id');
		$this->addElement($element);

		$element = new Zend_Form_Element_Text('email_template_name');
		$element->setRequired(true);
		$element->setLabel('Název');
		$element->setAttrib('class', 'span6');
		$this->addElement($element);
		
		$element = new Zend_Form_Element_Text('email_template_subject');
		$element->setRequired(true);
		$element->setLabel('Předmět');
		$element->setAttrib('class', 'span6');
		$this->addElement($element);
    
    // výchozí šablona - tabák
    if ($this->id_wh_type == Table_TobaccoWarehouse::WH_TYPE) {
      $element = new Zend_Form_Element_Select('default_for');
      $element->setLabel('Výchozí pro');
      $element->setAttrib('class', 'span6');
      $element->addMultiOption('0', '(nic)');
      $element->addMultiOption(Table_Emails::EMAIL_ORDER_NEW_CUSTOMER_NOTICE, Table_Emails::$number_to_readable[Table_Emails::EMAIL_ORDER_NEW_CUSTOMER_NOTICE]);
      $element->addMultiOption(Table_Emails::EMAIL_ORDER_OPEN_CUSTOMER_NOTICE, Table_Emails::$number_to_readable[Table_Emails::EMAIL_ORDER_OPEN_CUSTOMER_NOTICE]);
      $element->addMultiOption(Table_Emails::EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE, Table_Emails::$number_to_readable[Table_Emails::EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE]);
      
      $this->addElement($element);
    }
		
		$element = new Zend_Form_Element_Textarea('email_template_body');
		$element->setLabel('Text e-mailu');
		$element->setAttrib('class', 'span6');
		$this->addElement($element);
		
		$element = new Zend_Form_Element_Submit('save');
		$element->setLabel('Uložit');
		$this->addElement($element);

		$element = new Zend_Form_Element_Submit('cancel');
		$element->setLabel('Zpět');
		$this->addElement($element);		
		
		EasyBib_Form_Decorator::setFormDecorator(
			$this, EasyBib_Form_Decorator::BOOTSTRAP_MINIMAL, 'save'
        );
	}
	
}
