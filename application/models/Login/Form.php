<?php
class Login_Form extends Meduse_Form {
	
    public function __construct ($options = null){
        parent::__construct('login_box', 'Přihlášení', $options);
		
		$element = new Meduse_Form_Element_Text('username');
		$element->setRequired(true);
		$element->setLabel('Jméno');
		$element->setAttrib('autocomplete', 'off');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Password('password');
		$element->setRequired(true);
		$element->setLabel('Heslo');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);
		
    }
}
