<?php

/**
 * @author Zdeněk Filipec <zdendaf@gmail.com>
 */
class Customers {
  public static function getDataGrid($params) {
      $tCustomers = new Table_Customers();
      if (!isset($params['wh'])) {
        $params['wh'] = Table_PartsWarehouse::WH_TYPE;
      }
      $select = $tCustomers->getDataList(TRUE, $params['wh'], $params);
      $grid = self::getGrid($params);
      $grid->setSelect($select);
      $grid->setRequest($params);
      return $grid;
  }

  private static function getGrid($params) {
    $prefix = $params['wh'] == 2 ? 'tobacco-' : '';
    $view = new Zend_View();
    $grid = new ZGrid(array(
            'title' => _("Zákazníci") ,
            'css_class' => 'table table-condensed table-stripped sticky',
      'allow_paginator'    => FALSE,
      'curr_items_per_page'=> 0,
            'add_link_url' => $view->url(array(
                'module' => 'default' , 'controller' => $prefix . 'wallet' ,
                'action' => 'add.entry'
            ), null, true) ,
			'allow_add_link' => false ,
            'add_link_title' => _('New record') ,
            'columns' => array(
                'company' => array(
                    'header' => _('Firma') ,
                    'css_style' => 'text-align: left' ,
					'sorting' => FALSE,
					'renderer' => 'url' ,
                    'url' => $view->url(array(
                        'module' => 'default' , 'controller' => $prefix . 'customers' ,
                        'action' => 'detail' , 'id' => '{id}'
                    ), null, true)
                ) ,
                'first_name' => array(
                    'header' => _('Jméno') ,
                    'css_style' => 'text-align: left' ,
					'sorting' => FALSE,
					'renderer' => 'url' ,
                    'url' => $view->url(array(
                        'module' => 'default' , 'controller' => $prefix . 'customers' ,
                        'action' => 'detail' , 'id' => '{id}'
                    ), null, true)
                ) ,
                'last_name' => array(
                    'header' => _('Příjmení') , 'sorting' => true ,
                    'css_style' => 'text-align: left' ,
					'renderer' => 'url' ,
                  'sorting' => FALSE,
                    'url' => $view->url(array(
                        'module' => 'default' , 'controller' => $prefix . 'customers' ,
                        'action' => 'detail' , 'id' => '{id}'
                    ), null, true)
                ) ,
                'phone' => array(
                    'header' => _('Telefon') , 'sorting' => false ,
                    'css_style' => 'text-align: left'
                ),
				'assign' => array(
                    'header' => '' , 'css_style' => 'text-align: center' ,
                    'renderer' => 'action' ,
					'type' => 'assign' ,
                    'url' => $view->url(array(
                        'module' => 'default' , 'controller' => $prefix . 'order-edit' ,
                        'action' => 'assign-customer' , 'customer' => '{id}' , 'order' => $params['order_id']
                    ), null, true)
				)

            )
        ));
    return $grid;
  }

  /**
   * Vrati objekt zakaznika na zaklade jeho IC (nikoli systemoveho ID)
   * @param $identNo uzivatelske IC
   * @return \Customers_Customer | NULL
   */
  public static function findByIdentNo($identNo, $wh_type = Table_PartsWarehouse::WH_TYPE) {
    $table = new Table_Customers();
    $select = $table->select()
      ->from('customers', array('id', 'id_wh_type'))
      ->where('id_wh_type = ?', $wh_type)
      ->where('ident_no = ?', $identNo);
    if ($row = $select->query(Zend_Db::FETCH_OBJ)->fetch()) {
        return new Customers_Customer($row->id);
    }
    else {
      return NULL;
    }
  }

  /**
   * Vrati objekt zakaznika na zaklade jeho IC (nikoli systemoveho ID)
   * @param $mail uzivatelsky email
   * @return \Customers_Customer | NULL
   */
  public static function findByMail($mail, $wh_type = Table_PartsWarehouse::WH_TYPE) {
    $table = new Table_Customers();
    $select = $table->select()
      ->from('customers', array('id', 'id_wh_type'))
      ->where('id_wh_type = ?', $wh_type)
      ->where('email = ?', $mail);
    if ($row = $select->query(Zend_Db::FETCH_OBJ)->fetch()) {
      return new Customers_Customer($row->id);
    }
    else {
      return NULL;
    }
  }

  public static function getCustomersTypes($withAll = TRUE) {
    return $withAll ?
      [Table_Customers::TYPE_B2B, Table_Customers::TYPE_B2C, Table_Customers::TYPE_ALL]
      : [Table_Customers::TYPE_B2B, Table_Customers::TYPE_B2C];
  }


    public static function getSellers(int $wh_type = Table_PartsWarehouse::WH_TYPE): array
    {
        $table = new Table_Customers();
        return $table->getSellers($wh_type);
    }

}
