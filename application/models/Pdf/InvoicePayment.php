<?php

/**
 * @author Zdeněk Filipec <zdendaf@gmail.com>
 */
class Pdf_InvoicePayment extends Pdf_Invoice {

  const ITEM_LINES_SIZE = 140;

  const TEMPLATE_PAYMENT_PIPES = '../public/pdf/invoice_payment.pdf';

  const TEMPLATE_PAYMENT_PIPES_NOLOGO = '../public/pdf/invoice_payment_nologo.pdf';

  const TEMPLATE_PAYMENT_TOBACCO = '../public/pdf/invoice_payment_tobacco.pdf';

  public $type = 'payment'; // Table_Invoices::TYPE_REGULAR;

	public static function load($source = Pdf_InvoicePayment::TEMPLATE_PAYMENT_PIPES, $revision = NULL) {
		return new self($source, $revision, true);
	}

public function fillNonItemsPrice($header, $coords1, $coords2) {

  $this->setLastPage();

    $item_discount = $this->currency == 'CZK' ?
            $this->getItemDiscount() * $this->getRate() : $this->getItemDiscount();
    $additional_discount = $this->currency == 'CZK' ?
            $this->getDiscountPrice() * $this->getRate() : $this->getDiscountPrice();
    $shipping = $this->showNetPrices ?
            $this->shipping['cost'] : $this->shipping['cost'] + $this->delivery['cost'] * (1 + $this->getVat());
    $shipping = $this->currency == 'CZK' ? $shipping * $this->getRate() : $shipping;

    // hlavicka
    if (!$item_discount) {
      unset($header['item_discount']);
    }
    if ($shipping) {
      $header['shipping'] = str_replace('%c', $this->shipping['text'], $header['shipping']);
    } else {
      unset($header['shipping']);
    }
    if (!$additional_discount) {
      unset($header['additional_discount']);
    }
    $this->_draw($header, Meduse_Pdf::getDefaultStyle(), $coords1, Meduse_Pdf::TEXT_ALIGN_LEFT);

    $this->setCoords($coords2);
    if ($item_discount) {
      $this->_draw(Meduse_Currency::format($item_discount, Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
        Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
    if ($shipping) {
      $this->_draw(Meduse_Currency::format($shipping, Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
        Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
    if ($additional_discount) {
      $this->_draw(Meduse_Currency::format(-$additional_discount, Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
        Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
  }

  public function printHeader($t) {
    $this->fillItemsHeader(array(
      $t->translate('header_subject'),
      $this->showNetPricesLabel ?
        $t->translate('header_price') . "\n" . $t->translate('net_vat') : $t->translate('header_price'),
      ), array(31, 496), array(31, 790), array(250, 504)
    );
  }

  public function printItems() {
    $this->fillItems(array(31, 478), array(31, 770), array(15, 34, 337, 394, 473, 529));
  }

  public function fillItems($coords, $coords2, $offset) {
    $rate = ($this->currency == Table_Invoices::CURRENCY_CZK) ? $this->rate : 1;
    $this->setCoords($coords);
    $this->setPageNumber(0);
    foreach ($this->productRows as $count => $row) {
      $new_page_index = $this->_getPageIndex($count);
      if ($new_page_index != $this->getPageNumber()) {
        $this->setPageNumber($new_page_index);
        $this->setCoords($coords2);
      }
      $newCoords = $this->getCoords();
      if (!isset($row['type'])) {
        $style = Meduse_Pdf::getDefaultStyle();
      } elseif ($row['type'] == Meduse_Pdf_Invoice::PRODUCT_LINES_TYPE_HEADER) {
        continue;
      } else {
        $style = Meduse_Pdf::getDefaultStyle();
      }
      $this->_draw($row['text'], $style, array($coords[0] + $offset[0], $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_LEFT);
      if (isset($row['price'])) {
        $number = number_format($row['price'] * $rate, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign($this->currency, $this->language), $style, array($coords[0] + $offset[5], $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
    }
  }


  public function generateProductRows($items, $customItems = array()) {
    $allItems = $this->prepareLines($items);
    foreach ($allItems as $item) {
      $textline = wordwrap($item['text'], self::ITEM_LINES_SIZE, "\n", false);
      $textArray = explode("\n", $textline);
      $unitPrice = null;
      $amount = null;
      $price = null;
      if (isset($item['end_price']) && isset($item['amount'])) {
        $unitPrice = ((float) $item['end_price']);
        $amount = (int) $item['amount'];
        $price = ((float) $item['price']);
      }
      if ($item['type'] != self::PRODUCT_LINES_TYPE_HEADER) {
        $this->productRows[] = array(
            'id' => isset($item['id']) ? $item['id'] : '',
            'text' => $textArray[0],
            'price' => $price,
            'type' => $item['type'],
        );
      }

      for ($it = 1; $it < count($textArray); $it++) {
        $this->productRows[]['text'] = $textArray[$it];
      }
    }
  }


  private function prepareLines($items) {

    $prepared = array();
    foreach ($items as $item) {
      $prepared[] = array(
        'text' => $item['description'],
        'amount' => $item['qty'],
        'end_price' => $item['price'],
        'price' => $item['price'] * $item['qty'],
        'vat' => $this->getVat(),
        'type' => self::PRODUCT_LINES_TYPE_NORMAL,
      );
    }
    return $prepared;
  }




}
