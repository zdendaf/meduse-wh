<?php

/**
 * @author Zdeněk Filipec <zdendaf@gmail.com>
 */
class Pdf_InvoiceServices extends Pdf_Invoice {

  const ITEM_LINES_SIZE = 74;

  const TEMPLATE_SERVICES_PIPES = '../public/pdf/invoice_services.pdf';

  const TEMPLATE_SERVICES_PIPES_NOLOGO = '../public/pdf/invoice_services_nologo.pdf';

  const TEMPLATE_SERVICES_PIPES_VAT = '../public/pdf/invoice_services_vat.pdf';

  const TEMPLATE_SERVICES_PIPES_VAT_NOLOGO = '../public/pdf/invoice_services_vat_nologo.pdf';

  const TEMPLATE_SERVICES_TOBACCO = '../public/pdf/invoice_services_tobacco.pdf';

  const TEMPLATE_SERVICES_TOBACCO_VAT = '../public/pdf/invoice_services_tobacco_vat.pdf';

  public $type = Table_Invoices::TYPE_REGULAR;

	public static function load($source = self::TEMPLATE_SERVICES_PIPES, $revision = NULL) {
		return new self($source, $revision, true);
	}

public function fillNonItemsPrice($header, $coords1, $coords2, $coords3) {
  $rCoef = $this->reductionCoef;
  $this->setLastPage();

    $item_discount = ($this->currency == 'CZK' ?
            $this->getItemDiscount() * $this->getRate() : $this->getItemDiscount()) * $rCoef;
    $additional_discount = ($this->currency == 'CZK' ?
            $this->getDiscountPrice() * $this->getRate() : $this->getDiscountPrice()) * $rCoef;
    $shipping = $this->showNetPrices ?
            $this->shipping['cost'] : $this->shipping['cost'] + $this->delivery['cost'] * (1 + $this->getVat());
    $shipping = ($this->currency == 'CZK' ? $shipping * $this->getRate() : $shipping) * $rCoef;

    // hlavicka
    if (!$item_discount) {
      unset($header['item_discount']);
    }
    if ($shipping && $this->shipping['show']) {
      $header['shipping'] = str_replace('%c', $this->shipping['text'], $header['shipping']);
    } else {
      unset($header['shipping']);
    }
    if (!$additional_discount) {
      unset($header['additional_discount']);
    }
    $this->_draw($header, Meduse_Pdf::getDefaultStyle(), $coords1, Meduse_Pdf::TEXT_ALIGN_LEFT);

    $this->setCoords($coords2);
    if ($item_discount) {
      $this->_draw(Meduse_Currency::format($item_discount, Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
        Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
    if ($shipping) {
      $this->_draw(Meduse_Currency::format($shipping, Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
        Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
    if ($additional_discount) {
      $this->_draw(Meduse_Currency::format(-$additional_discount, Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
        Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
  }

  public function printHeader($t) {
    if ($this->showVatSummary) {
      $this->fillItemsHeader(array(
        $t->translate('header_subject'),
        $t->translate('header_qty'),
        $this->showNetPricesLabel ?
          $t->translate('header_unit') . "\n" . $t->translate('net_vat') : $t->translate('header_unit'),
        $this->showNetPricesLabel ?
          $t->translate('header_subtotal') . "\n" . $t->translate('net_vat') : $t->translate('header_subtotal'),
        $this->showNetPricesLabel ?
          $t->translate('header_subtotal') . "\n" . $t->translate('with_vat') : $t->translate('header_subtotal'),
      ), array(31, 496), array(31, 790), array(164, 336, 392, 448, 504));

    }
    else {
      $this->fillItemsHeader(array(
        $t->translate('header_subject'),
        $t->translate('header_qty'),
        $this->showNetPricesLabel ?
          $t->translate('header_unit') . "\n" . $t->translate('net_vat') : $t->translate('header_unit'),
        $this->showNetPricesLabel ?
          $t->translate('header_price') . "\n" . $t->translate('net_vat') : $t->translate('header_price'),
        ), array(31, 496), array(31, 790), array(184, 392, 448, 504)
      );
    }
  }

  public function printItems() {
    $this->fillItems(array(31, 478), array(31, 770), array(2, 338, 417, 473, 529, 585));
  }

  public function fillItems($coords, $coords2, $offset) {
    $rate = ($this->currency == Table_Invoices::CURRENCY_CZK) ? $this->rate : 1;
    $this->setCoords($coords);
    $this->setPageNumber(0);

    // pokud nevypisujeme radkovou sumu s dani, vynechame
    $shift = $this->showVatSummary ? 0 : $offset[5] - $offset[4];

    foreach ($this->productRows as $count => $row) {
      $new_page_index = $this->_getPageIndex($count);
      if ($new_page_index != $this->getPageNumber()) {
        $this->setPageNumber($new_page_index);
        $this->setCoords($coords2);
      }
      $newCoords = $this->getCoords();
      if (!isset($row['type'])) {
        $style = Meduse_Pdf::getDefaultStyle();
      } elseif ($row['type'] == Meduse_Pdf_Invoice::PRODUCT_LINES_TYPE_HEADER) {
        continue;
      } else {
        $style = Meduse_Pdf::getDefaultStyle();
      }
      $this->_draw($row['text'], $style, array($coords[0] + $offset[0], $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_LEFT);
      if (isset($row['amount']))
        $this->_draw($row['amount'], $style, array($coords[0] + $offset[1] + $shift, $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      if (isset($row['end_price'])) {
        $number = number_format($row['end_price'] * $rate, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign($this->currency, $this->language), $style, array($coords[0] + $offset[2] + $shift, $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if (isset($row['price'])) {
        $number = number_format($row['price'] * $rate, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign($this->currency, $this->language), $style, array($coords[0] + $offset[3] + $shift, $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if ($this->showVatSummary && isset($row['price_vat'])) {
        $number = number_format($row['price_vat'] * $rate, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign($this->currency, $this->language), $style, array($coords[0] + $offset[4], $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
    }
  }


  public function generateProductRows($items, $customItems = array()) {
    $allItems = array_merge($this->prepareServices($items), $customItems);
    $addDisc = $this->discountInclude ? (100 - $this->discount) / 100 : 1;
    foreach ($allItems as $item) {
      $textline = wordwrap($item['text'], self::ITEM_LINES_SIZE + ($this->showVatSummary ? 0 : self::PRODUCT_LINES_SHIFT), "\n", false);
      $textArray = explode("\n", $textline);
      $unitPrice = null;
      $amount = null;
      $price = null;
      if (isset($item['end_price']) && isset($item['amount'])) {
        $unitPrice = ((float) $item['end_price']) * $addDisc;
        $amount = (int) $item['amount'];
        $price = ((float) $item['price']) * $addDisc;
      }
      if ($item['type'] != self::PRODUCT_LINES_TYPE_HEADER) {
        $this->productRows[] = array(
            'id' => isset($item['id']) ? $item['id'] : '',
            'text' => $textArray[0],
            'amount' => $amount,
            'end_price' => $this->showNetPrices ? $unitPrice : $unitPrice * (1 + $this->vat),
            'price' => $this->showNetPrices ? $price : $price * (1 + $this->vat),
            'price_vat' => $price * (1 + $this->vat),
            'type' => $item['type'],
        );
      }

      for ($it = 1; $it < count($textArray); $it++) {
        $this->productRows[]['text'] = $textArray[$it];
      }
    }
  }

  private function prepareServices($items) {
    $prepared = array();
    foreach ($items as $item) {
      $price = $this->type == Table_Invoices::TYPE_CREDIT ? - $item['price'] : $item['price'];
      $prepared[] = array(
        'text' => $item['description'],
        'amount' => $item['qty'],
        'end_price' => $price,
        'price' => $price * $item['qty'],
        'vat' => $this->getVat(),
        'type' => self::PRODUCT_LINES_TYPE_NORMAL,
      );
    }
    return $prepared;
  }
}
