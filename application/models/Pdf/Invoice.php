<?php

/**
 * @author Zdeněk Filipec <zdendaf@gmail.com>
 */
class Pdf_Invoice extends Meduse_Pdf_Invoice {

	public static function load($source = '/pdf/template/invoice.pdf', $revision = NULL) {
		return new self($source, $revision, true);
	}

  public function setOutput($region, $country, $customer_type, $show_vat) {
    if ($region == Table_Addresses::REGION_NON_EU) {
      // NON-EU B2B / B2C:
      $this->showNetPrices = true;
      $this->showNetPricesLabel = false;
      $this->showTotalNetPrice = true;
      $this->showTotalVatPrice = $show_vat;
      $this->showVatSummary = $show_vat;
    }
    elseif ($country != Table_Addresses::COUNTRY_CZ) {
      if ($customer_type == Table_Customers::TYPE_B2B) {
        // EU B2B:
        $this->showNetPrices = true;
        $this->showNetPricesLabel = true;
        $this->showTotalNetPrice = true;
        $this->showTotalVatPrice = $show_vat;
        $this->showVatSummary = $show_vat;
      }
      else {
        // EU B2C:
        $this->showNetPrices = true;
        $this->showNetPricesLabel = true;
        $this->showTotalNetPrice = true;
        $this->showTotalVatPrice = $show_vat;
        $this->showVatSummary = $show_vat;
      }
    }
    else {
      if ($customer_type == Table_Customers::TYPE_B2B) {
        // CZ B2B:
        $this->showNetPrices = true;
        $this->showNetPricesLabel = true;
        $this->showTotalNetPrice = true;
        $this->showTotalVatPrice = $show_vat;
        $this->showVatSummary = $show_vat;
      }
      else {
        // CZ B2C:
        $this->showNetPrices = true;
        $this->showNetPricesLabel = true;
        $this->showTotalNetPrice = true;
        $this->showTotalVatPrice = $show_vat;
        $this->showVatSummary = $show_vat;
      }
    }
  }


  public function fillItemsHeader($header, $coords, $coords2, $offsetX, $lineHeight = 1.2) {
    parent::fillItemsHeader($header, $coords, $coords2, $offsetX, $lineHeight);
  }

  public function printHeader($t) {
    if ($this->showVatSummary) {
      $this->fillItemsHeader(array(
        $t->translate('header_id_no'),
        $t->translate('header_full_description'),
        'hs' => $t->translate('header_hs_code'),
        $t->translate('header_packages'),
        $this->showNetPricesLabel ?
          $t->translate('header_unit') . "\n" . $t->translate('net_vat') : $t->translate('header_unit'),
        $this->showNetPricesLabel ?
          $t->translate('header_subtotal') . "\n" . $t->translate('net_vat') : $t->translate('header_subtotal'),
        $this->showNetPricesLabel ?
          $t->translate('header_subtotal') . "\n" . $t->translate('with_vat') : $t->translate('header_subtotal'),
        ), array(31, 496), array(31, 790), array(17, 164, 281, 338, 392, 448, 504));
    }
    else {
      $this->fillItemsHeader(array(
        $t->translate('header_id_no'),
        $t->translate('header_full_description'),
        'hs' => $t->translate('header_hs_code'),
        $t->translate('header_packages'),
        $this->showNetPricesLabel ?
          $t->translate('header_unit') . "\n" . $t->translate('net_vat') : $t->translate('header_unit'),
        $this->showNetPricesLabel ?
          $t->translate('header_subtotal') . "\n" . $t->translate('net_vat') : $t->translate('header_subtotal'),
      ), array(31, 496), array(31, 790), array(17, 204, 338, 392, 448, 504));
    }
  }

  public function printItems() {
    $this->fillItems(array(31, 478), array(31, 770), array(17, 44, 281, 338, 417, 473, 529));
  }
}
