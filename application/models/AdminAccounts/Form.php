<?php

class AdminAccounts_Form extends Meduse_FormBootstrap {

    private $id_wh_type = Table_PartsWarehouse::WH_TYPE;

    public function __construct($options = null)
    {
     if (isset($options['id_wh_type'])) {
       $this->id_wh_type = $options['id_wh_type'];
       unset($options['id_wh_type']);
     }
      parent::__construct($options);
    }

  public function init() {
        
        $this->setName('account');
        $this->setAttrib('class', 'form');
        $this->setMethod(Zend_Form::METHOD_POST);

        if ($this->id_wh_type === Table_PartsWarehouse::WH_TYPE) {
          $this->setAction('/admin/accounts-save');
        }
        else {
          $this->setAction('/tobacco-settings/accounts-save');
        }
        
        $element = new Zend_Form_Element_Hidden('id');
        $this->addElement($element);
        
        $element = new Zend_Form_Element_Hidden('id_wh_type');
        $element->setValue($this->id_wh_type);
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Text('designation');
        $element->setLabel('Označení');
        $element->setRequired(true);
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Text('name');
        $element->setLabel('Název');
        $element->setRequired(true);
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Text('address');
        $element->setLabel('Adresa');
        $element->setRequired(true);
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Text('iban');
        $element->setLabel('IBAN');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('bic');
        $element->setLabel('BIC');
        $this->addElement($element);
                
        $element = new Meduse_Form_Element_Text('account_prefix');
        $element->setLabel('Předčíslí účtu');
        $this->addElement($element);
                
        $element = new Meduse_Form_Element_Text('account_number');
        $element->setLabel('Číslo účtu');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('bank_number');
        $element->setLabel('Kód banky');
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Select('currency');
        $element->setLabel('Měna');
        $element->addMultiOptions(array(
            'CZK' => 'Česká koruna',
            'EUR' => 'Euro',
        ));
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Checkbox('is_default');
        $element->setLabel('Nastavit jako výchozí');
        $element->setCheckedValue('y');
        $element->setUncheckedValue('n');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('payment_method');
        $element->setLabel('Svázaná plat. metoda');
        $element->addMultiOption('', 'žádná');
        $element->addMultiOptions(Table_Orders::$paymentToString);
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Submit('Uložit');
        $this->addElement($element);
        
        parent::init();
    }
    
    
    
    
}

