<?php

class Reports_SalesForm extends Tobacco_Reports_SalesForm {
  protected $wh = 1;

  public function init() {
    parent::init();
    $this->removeElement('order');
    $this->removeElement('onlypaid');

    /*
    $element = new Meduse_Form_Element_Select('type');
    $element->setLabel('Typ zákazníků:');
    $element->setMultiOptions([
      Table_Customers::TYPE_ALL => 'B2B + B2C',
      Table_Customers::TYPE_B2B => 'B2B',
      Table_Customers::TYPE_B2C => 'B2C',
    ]);
    $this->addElement($element);
    */
  }
}
