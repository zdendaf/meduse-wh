<?php

  class Orders_HsForm extends Meduse_FormSkove {

    /** @var \Orders_Order  */
    protected $order;

    public function __construct(array $options) {

      if (!isset($options['order']) || ! $options['order'] instanceof Orders_Order) {
        throw new Exception('Order object not set.');
      }

      $this->order = $options['order'];
      unset($options['order']);
      parent::__construct($options);
    }

    public function init() {

      $element = new Zend_Form_Element_Hidden('order_id');
      $element->setValue($this->order->getID());
      $this->addElement($element);

      $element = new Zend_Form_Element_Hidden('product_id');
      $this->addElement($element);


      /** @var \Zend_Db_Adapter_Pdo_Mysql $db */
      $db = Zend_Registry::get('db');
      $sql = "SELECT * FROM hs_codes WHERE avail_invoices = 'y' ORDER BY weight, code";
      $hsCodes = $db->query($sql)->fetchAll(Zend_Db::FETCH_ASSOC);

      $element = new Zend_Form_Element_Radio('hs_code');
      $element->addMultiOption('original', '<div class="text"><strong>(původní)</strong></div>');
      if ($hsCodes) {
        foreach ($hsCodes as $data) {
          $value = '<div class="text"><strong>' . $data['code'] . ':</strong> '
            . $data['description'] . '<br><small><strong>' . $data['note'] . '</strong></small></div>';
          $element->addMultiOption($data['code'], $value);
        }
      }
      $this->addElement($element);

      parent::init();
    }
  }

  