<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Orders_ExpeditionForm extends Meduse_FormBootstrap {

    public function init() {

      $this->setAttrib('class', 'form');
      $this->setLegend('Expedice objednávky');

      $element = new Zend_Form_Element_Hidden('order_id');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('warehouse_id');
      $element->setLabel('Zákaznický sklad');

      $element->addMultiOption(0, '-- nenaskladňovat (normální expedice) --');
      $tWarehouses = new Table_CustomersWarehouses();
      $options = $tWarehouses->getOptionsArray(array('c.id_wh_type' => 1));
      $element->addMultiOptions($options);
      $this->addElement($element);


      $element = new Meduse_Form_Element_Submit('submit');
      $element->setLabel('Expedovat');
      $this->addElement($element);

      parent::init();
    }


  }
