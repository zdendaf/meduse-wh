<?php

class Orders_FeeForm extends Meduse_FormBootstrap
{

  /**
   * @throws Zend_Form_Exception
   */
  public function init()
    {

        $this->setAttribs(array('class' => 'form'))->setMethod('post');

        $element = new Zend_Form_Element_Hidden('id');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('name');
        $element->setLabel('Název');
        $element->setRequired();
        $this->addElement($element);

        $element = new Meduse_Form_Element_Currency('price');
        $element->setLabel('Výše poplatku [EUR]');
        $element->setRequired();
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('text_cs');
        $element->setLabel('CZE text na faktuře');
        $element->setRequired();
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('text_en');
        $element->setLabel('ENG text na faktuře');
        $element->setRequired();
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('is_coo');
        $element->setLabel('Za ověření původu');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('is_iv');
        $element->setLabel('Za ověření faktury');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('enabled');
        $element->setLabel('Aktivní');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Submit('Uložit');
        $this->addElement($element);

        parent::init();
    }

    public function populate(array $values): Orders_FeeForm
    {
      $values['is_coo'] = $values['is_coo'] ? 'y' : 'n';
      $values['is_iv'] = $values['is_iv'] ? 'y' : 'n';
      $values['enabled'] = $values['enabled'] ? 'y' : 'n';

      return parent::populate($values);
    }

}
