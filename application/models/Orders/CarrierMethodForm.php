<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Orders_CarrierMethodForm extends Meduse_FormBootstrap {

    public function init() {

      $this->setAttrib('class', 'form');

      $eMethodId = new Zend_Form_Element_Hidden('id');
      $this->addElement($eMethodId);

      $eCarrierId = new Zend_Form_Element_Hidden('id_producers');
      $this->addElement($eCarrierId);

      $eActive = new Meduse_Form_Element_Checkbox('is_active');
      $eActive->setLabel('Aktivní');
      $this->addElement($eActive);

      $eName = new Meduse_Form_Element_Text('name');
      $eName->setLabel('Název metody');
      $eName->setRequired(TRUE);
      $this->addElement($eName);

      $ePrice = new Meduse_Form_Element_Text('default_price');
      $ePrice->setLabel('Výchozí cena bez DPH');
      $this->addElement($ePrice);

      $ePrice = new Meduse_Form_Element_Text('default_price_vat');
      $ePrice->setLabel('Výchozí cena s DPH');
      $this->addElement($ePrice);

      $pos = new Meduse_Form_Element_Checkbox('is_pos');
      $pos->setLabel('Dobírka');
      $this->addElement($pos);

      $eSubmit = new Meduse_Form_Element_Submit('save');
      $eSubmit->setLabel('Uložit');
      $this->addElement($eSubmit);

      parent::init();
    }

    public function setProducer($id) {
      $element = $this->getElement('id_producers');
      $element->setValue($id);
    }

    public function populate(array $values, $dph = Table_ApplicationDph::DEFAULT_DPH) {
      parent::populate($values);
      $price = $this->getElement('default_price')->getValue();
      if ($price) {
        $price_vat = round($price * (100 + $dph) / 100, 2);
        $price = $this->getElement('default_price_vat')->setValue($price_vat);
      }
    }


  }
