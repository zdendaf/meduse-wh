<?php

class Orders_AvailableHsCodesForm extends Meduse_FormBootstrap {

  public function init() {

    $this->setAttribs(['class' => 'form']);
    parent::init();

    $decorators = $this->getDecorators();
    unset($decorators['Obal']);
    $this->setDecorators($decorators);

    $oder = 0;

    $element = new Meduse_Form_Element_Text('code');
    $element->setLabel('HS kód');
    $element->setRequired();
    $element->setAttrib('maxlength', 50);
    $element->addValidator(new Zend_Validate_StringLength(['max' => 50]));
    $element->setOrder(++$oder);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('description');
    $element->setLabel('Popis');
    $element->setRequired();
    $element->setAttrib('maxlength', 255);
    $element->addValidator(new Zend_Validate_StringLength(['max' => 255]));
    $element->setOrder(++$oder);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('note');
    $element->setLabel('Poznámka');
    $element->setAttrib('maxlength', 255);
    $element->addValidator(new Zend_Validate_StringLength(['max' => 255]));
    $element->setOrder(++$oder);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('weight');
    $element->setLabel('Váha');
    $element->setDescription('Zadejte hodnotu od -9 do 9. Kódy z nižší vahou budou výš v seznamu.');
    $element->setAttrib('maxlength', 2);
    $element->addValidator(new Zend_Validate_Int());
    $element->addValidator(new Zend_Validate_Between(['min' => -9, 'max' => 9]));
    $element->setValue(0);
    $element->setOrder(++$oder);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('avail_products');
    $element->setLabel('Dostupný u produktů');
    $decorator = $element->getDecorator('elementDiv');
    $element->removeDecorator('elementDiv');
    $decorator->setOptions(['tag' => 'div', 'style' => 'clear: left;']);
    $element->addDecorator($decorator);
    $element->setOrder(++$oder);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('avail_invoices');
    $element->setLabel('Dostupný u fakturace');
    $decorator = $element->getDecorator('elementDiv');
    $element->removeDecorator('elementDiv');
    $decorator->setOptions(['tag' => 'div', 'style' => 'clear: left;']);
    $element->addDecorator($decorator);
    $element->setOrder(++$oder);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('add');
    $element->setLabel('Uložit');
    $decorator = $element->getDecorator('elementDiv');
    $element->removeDecorator('elementDiv');
    $decorator->setOptions(['tag' => 'div', 'style' => 'clear: left; margin-top: 20px; text-align: center;']);
    $element->addDecorator($decorator);
    $element->setOrder(++$oder);
    $this->addElement($element);


  }

}