<?php

class Orders_ServicesForm extends Meduse_FormBootstrap {

  protected $rows = 0;

  public function __construct($options = NULL) {

    if (isset($options['rows'])) {
      $this->rows = $options['rows'];
    }

    parent::__construct($options);
  }

  public function init() {

    $this->setAttribs(array(
      'method' => 'post',
      'class' => 'form',
    ));

    // order
    $eOrder = new Zend_Form_Element_Hidden('id_orders');
    $this->addElement($eOrder);

    // num of rows
    $eRowsNum = new Zend_Form_Element_Hidden('rowsnum');
    $eRowsNum->setValue($this->rows);
    $this->addElement($eRowsNum);

    // rows
    $i = 0;
    while ($i < $this->rows) {
      $row = $this->createRow($i);
      $this->addElement($row['id']);
      $this->addElement($row['description']);
      $this->addElement($row['price']);
      $this->addElement($row['qty']);
      $this->addElement($row['delete']);
      $i++;
    }

    $row = $this->createRow();
    $this->addElement($row['id']);
    $this->addElement($row['description']);
    $this->addElement($row['price']);
    $this->addElement($row['qty']);
    $this->addElement($row['delete']);

    $eSubmit = new Meduse_Form_Element_Submit('submit');
    $eSubmit->setLabel('Uložit položky objednávky');
    $eSubmit->setAttrib('class', 'btn btn-large btn-success');
    $this->addElement($eSubmit);

    parent::init();
  }

  public function getRowsNum() {
    return $this->rows;
  }

  public function getRow($i) {
    if ($i >= $this->rows) {
      return FALSE;
    }
    return array(
      'id' => $this->getElement('id_' . $i),
      'description' => $this->getElement('description_' . $i),
      'price' => $this->getElement('price_' . $i),
      'qty' => $this->getElement('qty_' . $i),
      'delete' => $this->getElement('delete_' . $i),
    );
  }

  public function getNewRow() {
    return array(
      'id' => $this->getElement('id_new'),
      'description' => $this->getElement('description_new'),
      'price' => $this->getElement('price_new'),
      'qty' => $this->getElement('qty_new'),
      'delete' => $this->getElement('delete_new'),
    );
  }

  public function populate(array $values) {
    if (isset($values['rows']) && is_array($values['rows']) && !empty($values['rows'])) {
      $i = 0;
      while ($i < $this->rows) {
        $this->getElement('id_' . $i)->setValue($values['rows'][$i]['id']);
        $this->getElement('description_' . $i)->setValue($values['rows'][$i]['description']);
        $this->getElement('price_' . $i)->setValue($values['rows'][$i]['price']);
        $this->getElement('qty_' . $i)->setValue($values['rows'][$i]['qty']);
        $i++;
      }
    }
    parent::populate($values);
  }

  public function createRow($i = 'new') {

      // id
      $eId = new Zend_Form_Element_Hidden('id_' . $i);

      // description
      $eDescription = new Zend_Form_Element_Text('description_' . $i);
      $eDescription->setAttrib('class', 'description');
      $eDescription->setRequired(TRUE);

      // price
      $ePrice = new Meduse_Form_Element_Currency('price_' . $i);
      $ePrice->setAttrib('class', 'price');
      $ePrice->setRequired(TRUE);


      // quantity
      $eQty = new Meduse_Form_Element_Float('qty_' . $i);
      $eQty->setValue(1);
      $eQty->setAttrib('class', 'qty');
      $eQty->setRequired(TRUE);

      // to delete
      $eDelete = new Meduse_Form_Element_Checkbox('delete_' .$i);
      $eDelete->setAttrib('class', 'delete');
      $eDelete->setValue(FALSE);

      return array(
        'id' => $eId,
        'description' => $eDescription,
        'price' => $ePrice,
        'qty' => $eQty,
        'delete' => $eDelete,
      );

  }

}
