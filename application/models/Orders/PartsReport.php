<?php
class Orders_PartsReport {

	const ORDER_PARTS = 'parts';

	const ORDER_PRODUCERS = 'producers';

	const SHOW_INCLUDED_ONLY = 'included'; //zobrazovat pouze soucasti na objednavce

	const SHOW_ALL = 'all';

	const SHOW_MISSING_ONLY = 'missing';

	private $orders; //pole id objednavek

	private $collection = 'null';

	private $category = 'null';

	private $producer = 'null';

	private $order = null;

	private $filterArray = null;

	private $show = self::SHOW_INCLUDED_ONLY;

	public function setFilter(array $values) {
		$orders = array();
		foreach($values as $key => $value){
			if($value === 'on'){
				$orders[] = (int)$key;
			}
		}
		$this->orders = $orders;

		$filter = false;
		if(isset($values['coll']) && $values['coll'] != 'null') {
			$this->collection = $values['coll']; //array
			$filter = true;
		} else {
			$this->collection = 'null';
			$filter = true;
		}

		if(isset($values['ctg']) && $values['ctg'] != 'null') {
      $mainCtg = (int) $values['ctg'];
      $this->category = Table_PartsCtg::getChildrenCategories($mainCtg);
      $this->category[] = $mainCtg;
			$filter = true;
		}

		if(isset($values['producer']) && $values['producer'] != 'null') {
			$this->producer = (int) $values['producer'];
			$filter = true;
		}

		if($filter){
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select();
			$select->from(
				array('p' => 'parts'),
				array('id','name')
			);

			if($this->collection != 'null'){
				$select->join(
					array('pc' => 'parts_collections'),
					"pc.id_parts = p.id AND pc.id_collections in ( ".implode(',', $this->collection).")",
					array()
				);
			} else {
        $subSelect = new Zend_Db_Select($select->getAdapter());
        $subSelect->from(array('pc' => 'parts_collections'), array('id_parts'))
          ->where('p.id = pc.id_parts');
				$select->where("NOT EXISTS($subSelect)");
			}

			if($this->category != 'null'){
				$select->where("id_parts_ctg IN (?)", $this->category);
			}

			if($this->producer != 'null'){
				$select->join(
					array('pp' => 'producers_parts'),
					"pp.id_parts = p.id AND pp.	id_producers = ".$db->quote($this->producer),
					array()
				);
			}
			$this->filterArray = $db->fetchPairs($select);
		}
	}

	/**
	 * Nastavi razeni
	 * @param string $order
	 */
	public function setOrder(string $order): void
  {
		$this->order = $order;
	}

	/**
	 * Nastavi mod zobrazeni
	 * @param <type> $show
	 */
	public function setShowRows($show){
		$this->show = $show;
	}

	public function getData($raw = FALSE) {

		if(count($this->orders) < 1){
			return false;
		}

		$tOrders = new Table_Orders();
		$tOrdersProductsParts = new Table_OrdersProductsParts();
		$tWarehouse = new Table_PartsWarehouse();

		$orderPartReport = $tOrders->getOrderPartReport($this->order, $this->orders)->toArray();

		$reservations = $tOrdersProductsParts->getOrdersReservations($this->orders);

		//dopocitavani podsoucasti k objednani
    // - musi se pocitat pro cely report, protoze jinak se cisla meni
		$orderAmounts = array();
		foreach($orderPartReport as $part){
			$amounts = array(
				'sets' => $part['sets_amount'],
				'extra' => $part['extra_amount'],
			);
			$orderAmounts = $tOrders->getPartAmounts($part['id'], $amounts, $reservations, $orderAmounts);
		}
		//slevani selectu a dopocitane objednavky podsoucasti
		foreach($orderPartReport as $key => $line) {

			if(isset($orderAmounts[$line['id']])){
				//dopocitane hodnoty chybejicich soucasti do reportu
				$orderPartReport[$key]['sets_amount'] = $orderAmounts[$line['id']]['sets'];
				$orderPartReport[$key]['extra_amount'] = $orderAmounts[$line['id']]['extra'];
			}
			//prepocet sumy
			$orderPartReport[$key]['total_amount'] =
        $orderPartReport[$key]['sets_amount'] + $orderPartReport[$key]['extra_amount'];

			//filtrovani
			if($this->filterArray !== null && !array_key_exists($line['id'], $this->filterArray)) { unset($orderPartReport[$key]); continue; }

			if($this->show == self::SHOW_INCLUDED_ONLY){
				if($orderPartReport[$key]['total_amount'] == 0) { unset($orderPartReport[$key]); continue; } //pouze soucasti, ktere jsou na objednavce
			}

			$orderPartReport[$key]['reserv_amount'] = ($line['reservation'] == 1 && array_key_exists($line['id'], $reservations) ? $reservations[$line['id']] : 0);

			$amount = $line['wh_amount'] + $orderPartReport[$key]['reserv_amount'];

			if($orderPartReport[$key]['total_amount'] < $amount) {
				$orderPartReport[$key]['free_amount'] = $amount - $orderPartReport[$key]['total_amount'];
			} else {
				$orderPartReport[$key]['free_amount'] = 0;
			}

			$orderPartReport[$key]['missing_central'] = 0;
			if($line['extern'] == 1){
				$baseWarehouseAmount = $tWarehouse->getBaseAmount($line['id']) + $orderPartReport[$key]['reserv_amount'];
				if($orderPartReport[$key]['total_amount'] > $baseWarehouseAmount ){
					$orderPartReport[$key]['missing_central'] = $orderPartReport[$key]['total_amount'] - $baseWarehouseAmount;
				}
			}


			if($orderPartReport[$key]['total_amount'] > $amount ){
				$orderPartReport[$key]['missing_total'] = $orderPartReport[$key]['total_amount'] - $amount;
			}
			else {
        $orderPartReport[$key]['missing_total'] = 0;
				if($this->show == self::SHOW_MISSING_ONLY){ //zobrazi pouze chybjejici soucasti
					unset($orderPartReport[$key]);
					continue; //pouze soucasti, ktere jsou na objednavce
				}
			}

		}

		return $raw ? $orderPartReport : $this->prepareData($orderPartReport);
	}

	protected function prepareData($rows): array
  {
	  // Datove pole pro return.
	  $data = array(
	    'sums' => array(
        'production_costs' => 0,
        'production_operations' => 0,
        'ordered_costs' => 0,
        'ordered_operations' => 0,
        'time_total' => 0,
      ),
      'rows' => $rows
    );

	  // Redundantni suma vlastnich operaci vracena jako asociativni pole.
    // Klic je ID soucasti.
	  $tOperations = new Table_PartsOperations();
	  $operations = $tOperations->getOperationsSum();

    $productions = new Table_Productions();
    $parts = array();
    $first = TRUE;
    $last_producer = FALSE;
    foreach ($data['rows'] as &$row) {
      $row['_first'] = $first;
      if ($row['_new_producer'] = ($last_producer !== $row['id_producers'])) {
        $last_producer = $row['id_producers'];
        if ($first) {
          $first = FALSE;
        }
      }

      $row['_bold'] = ($row['product'] === 'y' ? "font-weight: bold;" : '');
      $row['_blue'] = (strpos($row['id'], 'S') === 0 || stripos($row['id'], 'AS') === 0 || strpos($row['id'], 'NF25') === 0 ? " color: blue;" : "");
      $row['_polotovar'] = (strtolower(substr($row['id'], -1)) === 'x' ? "style='background-color: #FFFF99;'" : '');

      // VYROBA
      $productionAmount = 0;
      $alreadyDelivered = 0;
      if ($row['production'] === '1') { //pokud existuje produkce
        $prod = $productions->getProductions($row['id']);
        $prodAmount = "";
        $prodDate = "";
        foreach ($prod as $tmp) {
          $tmp['amount'] = (int)$tmp['amount'];
          $alreadyDelivered += $tmp['amount'] - $tmp['amount_remain'];
          $prodAmount .= $alreadyDelivered . "/" . $tmp['amount'] . "<br />";
          $productionAmount += $tmp['amount_remain'];
          $prodDate .= "<a href='/producers/show-production/id/" . $tmp['id_productions'] . "' target='_blank' title='datum dodání, klikněte pro zobrazení výroby v nové záložce'>" . Meduse_Date::dbToForm($tmp['date_delivery']) . "</a><br />";
        }
        $row['_prodAmount'] = $prodAmount;
        $row['_prodDate'] = $prodDate;
      }
      $row['_productionAmount'] = $productionAmount;

      $missingAfterProduction = $row['missing_total'];
      if ($productionAmount > 0) { //pokud je zadana vyroba
        if ($row['missing_total'] > $productionAmount) {
          $missingAfterProduction = $row['missing_total'] - $productionAmount;
        }
        else {
          $missingAfterProduction = 0;
        }
        $row['_status'] = $productionAmount - $row['missing_total'];
      }
      $row['_missingAfterProduction'] = $missingAfterProduction;

      // SUMA
      if ($productionAmount > 0 || $missingAfterProduction) {
        $operationTime = array_key_exists($row['id'], $operations) ? (int) $operations[$row['id']]['time'] : 0;
        $row['operation_time'] = $row['missing_total'] * $operationTime;
        $data['sums']['time_total'] += $row['operation_time'];
        $parts[$row['id']] = array(
          'production_amount' => $productionAmount,
          'ordered_amount' => $missingAfterProduction,
        );
      }
    }
    unset($row);
    $tParts = new Table_Parts();
    $sums = $tParts->getCostOperationSums(array_keys($parts));
    if (!$sums) {
      $data['sums'] = NULL;
    }
    else {
      foreach ($sums as $item) {
        $data['sums']['production_costs'] += $parts[$item['id']]['production_amount'] * $item['costs'];
        $data['sums']['production_operations'] += $parts[$item['id']]['production_amount'] * $item['operations'];
        $data['sums']['ordered_costs'] += $parts[$item['id']]['ordered_amount'] * $item['costs'];
        $data['sums']['ordered_operations'] += $parts[$item['id']]['ordered_amount'] * $item['operations'];
      }
    }
	  return $data;
  }
}