<?php

class Orders_COODatesForm extends Meduse_FormBootstrap {

  protected $_isModalForm = TRUE;

  public function init() {

    $this->setAttribs(['class' => 'form']);
    $this->setMethod('post');

    $element = new Zend_Form_Element_Hidden('id_orders');
    $this->addElement($element);

    $element = new Meduse_Form_Element_DatePicker('coo_date_confirm');
    $element->setLabel('Vyřízeno dne');
    $this->addElement($element);

    $element = new Meduse_Form_Element_DatePicker('coo_date_validity');
    $element->setLabel('Platí do');
    $this->addElement($element);

    parent::init();
  }
}