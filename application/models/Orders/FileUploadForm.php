<?php

class Orders_FileUploadForm extends Zend_Form {

  const ALLOWED_EXTENSIONS = [
    'csv',
    'pdf',
    'doc',
    'docx',
    'xls',
    'xlsx',
    'txt',
    'jpg',
    'png',
  ];

  function init() {

    $this->setMethod(Zend_Form::METHOD_POST);
    $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    $this->setLegend('Upload souboru');
    $this->setAttribs(['class' => 'form']);

    $element = new Zend_Form_Element_Hidden('id_orders');
    $this->addElement($element);

    $element = new Zend_Form_Element_File('file');
    $element->setLabel('Soubor');
    $element->setOrder(-1);
    $element->addValidator(new Zend_Validate_File_Count(['min' => 1, 'max' => 5]));
    $element->setMultiFile(1);
    $element->addValidator(new Zend_Validate_File_Size(['max' => 102400]));
    $element->addValidator(new Zend_Validate_File_Extension(self::ALLOWED_EXTENSIONS));
    $element->setRequired(TRUE);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('title');
    $element->setLabel('Popisek');
    $element->setOrder(1);
    $element->setDecorators(array(
      'ViewHelper',
      'Description',
      'Errors',
      'Label',
      array(array('data' => 'HtmlTag')),
    ));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('upload');
    $element->setLabel('Nahrát');
    $element->setOrder(0);
    $element->setDecorators(array(
      'ViewHelper',
      'Description',
      'Errors',
      array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'pull-right')),
    ));
    $this->addElement($element);

    parent::init();
  }

  public function setOrderId($orderId) {
    $this->getElement('id_orders')->setValue($orderId);
  }
}
