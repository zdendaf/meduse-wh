<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Orders_ChangeAddressForm extends Meduse_FormBootstrap {

    protected $_orderObj = NULL;
    protected $_modal = FALSE;
    protected $_destination = NULL;

    public function __construct($options = NULL) {
      $this->_orderObj = $options['data']['order'];
      if (isset($options['data']['modal'])) {
        $this->_modal = $options['data']['modal'];
      }
      if (isset($options['data']['destination'])) {
        $this->_destination = $options['data']['destination'];
      }
      unset($options['data']);
      parent::__construct($options);
    }

    public function init() {

      $this->setAttrib('class', 'form');
      $this->_isModalForm = TRUE;

      $customer = $this->_orderObj->getCustomer(TRUE);
      $addresses = $customer->getAddresses();
      if ($addresses) {
        $element = new Meduse_Form_Element_Radio('address');
        $addresses = $customer->getAddresses();
        $options = array();
        foreach ($addresses as $address) {
          $options[$address->getId()] = $address->render('; ');
        }
        $element->addMultiOptions($options);
        $this->addElement($element);
      }

      if (!$this->_modal) {
        $element = new Meduse_Form_Element_Submit('save');
        $element->setLabel('Zvolit adresu');
        $this->addElement($element);
      }

      $element = new Zend_Form_Element_Hidden('customer');
      $element->setValue($customer->getId());
      $this->addElement($element);

      $element = new Zend_Form_Element_Hidden('type');
      $element->setValue('shipping');
      $this->addElement($element);

      if (!is_null($this->_destination)) {
        $element = new Zend_Form_Element_Hidden('destination');
        $element->setValue($this->_destination);
        $this->addElement($element);
      }

      $element = new Zend_Form_Element_Hidden('order');
      $element->setValue($this->_orderObj->getID());
      $this->addElement($element);

      parent::init();
    }
  }
