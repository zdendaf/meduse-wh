<?php
class Orders_WrapsFilterForm extends Orders_FilterForm {
    public function __construct ($options = null){
    	parent::__construct();
    	
    	//kategorie obalu
    	$db = Zend_Registry::get('db');
    	$rows = $db->fetchAll($db->select()->from('wraps_ctg'));
    	$element = new Meduse_Form_Element_Select('wctg');
    	$element->setLabel('Kategorie obalů');
    	$element->addMultiOption('null', '- všechny -');
    	foreach($rows as $row){
			$element->addMultiOption($row['id'], $row['ctg_name']);
    	}
    	$this->addElement($element);
		
		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);
		
		//$this->getElement('coll')->setLabel('Kolekce produktů');
		//$this->getElement('ctg')->setLabel('Kategorie produktů');
		$this->removeElement('coll');
		$this->removeElement('ctg');
    }
}