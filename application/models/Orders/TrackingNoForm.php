<?php

class Orders_TrackingNoForm extends Meduse_FormBootstrap {

  /** @var \Orders_Order  */
  protected $order = NULL;

  public function __construct($options = NULL) {
    if (isset($options['order'])) {
      $this->order = $options['order'];
      unset($options['order']);
    }
    parent::__construct($options);
  }

  public function init() {

    $this->setAttrib('class', 'form');
    $this->setAction('/orders/tracking-save');
    $this->setLegend('Trackování zásilky');

    $element = new Zend_Form_Element_Hidden('tracking_id_orders');
    $element->setValue($this->order->getID());
    $this->addElement($element);

    $this->addItems();
    $this->addBlankItem();

    $element = new Meduse_Form_Element_Submit('save_tracking');
    $element->setLabel('Uložit změny');
    $this->addElement($element);

    parent::init();
  }


  protected function addItems() {
    $idx = 0;
    if ($trackingCodes = $this->order->getTrackingCodes(false, false)) {
      foreach ($trackingCodes as $item) {

        $element = new Zend_Form_Element_Hidden('tracking_id_' . $idx);
        $element->setValue($item['id']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('tracking_code_' . $idx);
        $element->setValue($item['code']);
        $element->setLabel('Tracking code');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('tracking_link_' . $idx);
        $element->setValue($item['link']);
        $element->setLabel('Tracking link');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('tracking_remove_' . $idx);
        $element->setLabel('Odstranit');
        $this->addElement($element);

        $idx++;
      }
    }

    $element = new Zend_Form_Element_Hidden('count_tracking');
    $element->setValue($idx);
    $this->addElement($element);
  }

  protected function addBlankItem() {
    $element = new Zend_Form_Element_Hidden('tracking_id');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('tracking_code');
    $element->setLabel('Tracking code');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('tracking_link');
    $element->setLabel('Tracking link');
    if ($carrier = $this->order->getCarrier()) {
      if ($trackingPattern = $carrier->getTrackingPattern()) {
        $element->setValue($trackingPattern);
      }
    }
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('tracking_remove');
    $element->setLabel('Odstranit');
    $this->addElement($element);
  }

  public function getItemsCount() {
    if (!$this->order) {
      return NULL;
    }
    return count($this->order->getTrackingCodes());
  }
}