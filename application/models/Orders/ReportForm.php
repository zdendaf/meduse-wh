<?php
class Orders_ReportForm extends Meduse_FormBootstrap {

    public function init(){

		$this->setAttribs(array('class' => 'form'))
			->setMethod('post')
			->setLegend('Interaktivní přehled objednávek');

		$element = new Meduse_Form_Element_Collections('collection');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('status');
		$element->setLabel('Stav objednávky');
		$element->addMultiOptions(array(
            'all' => 'všechny',
			Table_Orders::STATUS_STRATEGIC => Table_Orders::$statusToString[Table_Orders::STATUS_STRATEGIC],
			Table_Orders::STATUS_OFFER => Table_Orders::$statusToString[Table_Orders::STATUS_OFFER],
			Table_Orders::STATUS_NEW => Table_Orders::$statusToString[Table_Orders::STATUS_NEW],
			Table_Orders::STATUS_CONFIRMED => Table_Orders::$statusToString[Table_Orders::STATUS_CONFIRMED],
			Table_Orders::STATUS_OPEN => Table_Orders::$statusToString[Table_Orders::STATUS_OPEN],
			Table_Orders::STATUS_CLOSED => Table_Orders::$statusToString[Table_Orders::STATUS_CLOSED]
		));
        $element->setValue(Table_Orders::STATUS_CLOSED);
		$this->addElement($element);

		$element = new Meduse_Form_Element_DatePicker(
			'order_close_from',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->setLabel('Datum expedice od:');
		$this->addElement($element);

		$element = new Meduse_Form_Element_DatePicker(
			'order_close_to',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->setLabel('Datum expedice do:');
		$this->addElement($element);

        $element = new Meduse_Form_Element_Text('customer');
		$element->setLabel('Zákazník');
		$this->addElement($element);

        $element = new Zend_Form_Element_Hidden('customer_id');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit("Odeslat");
		$this->addElement($element);

		parent::init();
	}
}
