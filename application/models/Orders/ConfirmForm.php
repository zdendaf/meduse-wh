<?php
class Orders_ConfirmForm extends Meduse_Form {

    public function __construct() {
      parent::__construct(null, null);
      $this->setAttribs(array('class' => 'form', 'id' => 'orders-confirm-form'));
      $this->setAction('/orders/confirm');
      $this->setMethod(Zend_Form::METHOD_POST);
      $element = new Zend_Form_Element_Hidden('id');
      $element->setOrder(6);
      $this->addElement($element);
	}
}
