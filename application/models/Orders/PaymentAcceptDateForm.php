<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of paymentAcceptDate
 *
 * @author Zdeněk
 */
class Orders_PaymentAcceptDateForm extends Meduse_FormBootstrap {
	
	public function init() {

		$this->setMethod(Meduse_FormBootstrap::METHOD_POST)
			->setAttrib('class', 'form')
			->setLegend('Datum, kdy platba došla na účet');
		
		$element = new Meduse_Form_Element_DatePicker(
			'meduse_date_payment_accept',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->setLabel('Datum');
		$element->setRequired();
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit("Odeslat");
		$this->addElement($element);		
		
		parent::init();
	}
}

