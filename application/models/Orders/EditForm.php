<?php
class Orders_EditForm extends Meduse_Form {

    public function __construct ($options = null){
    	parent::__construct('parts_box', 'Úprava objednávky', $options);

    $element = new Meduse_Form_Element_Text('meduse_orders_no');
		$element->setRequired(true);
		$element->setLabel('Číslo objednávky');
		$this->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_orders_name');
		$element->setRequired(true);
		$element->setLabel('Jméno');
		$this->addElement($element);

    	$element = new Meduse_Form_Element_DatePicker(
			'meduse_orders_deadline',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->setRequired(true);
		$element->setLabel('Deadline');
		$this->addElement($element);

		$element = new Meduse_Form_Element_DatePicker(
			'meduse_orders_pay_all',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->setLabel('Zaplaceno dne');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_orders_desc');
		$element->setLabel('Poznámka');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_orders_expected_payment');
		$element->setLabel('Očekávaná platba');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_orders_expected_payment_deposit');
		$element->setLabel('Záloha');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('meduse_orders_expected_payment_currency');
		$element->setRequired();
		$element->setLabel('Měna platby');
		$element->addMultiOption("CZK", "CZK");
		$element->addMultiOption("EUR", "EUR");
		$element->setValue('CZK');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('meduse_orders_payment_method');
		$element->setLabel('Způsob platby');
		$element->addMultiOption('null', "-- neurčen --");
		$element->addMultiOption('cash_on_delivery', "Na dobírku");
		$element->addMultiOption('cash', "V hotovosti");
		$element->addMultiOption('transfer', "Převodem");
		$element->addMultiOption('free_presentation', "Zdarma - prezentace");
		$element->setValue('CZK');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('meduse_orders_carrier');
		$element->setRequired();
		$element->setLabel('Dopravce');
		$element->addMultiOption(null, "-- vybrat --");
		$db = Zend_Registry::get('db');
		$element->addMultiOptions($db->fetchPairs("SELECT id, name FROM orders_carriers"));
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_orders_carrier_price');
		$element->setLabel('Cena dopravy');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('meduse_orders_purchaser');
		$element->setRequired();
		$element->setLabel('Typ příjemce');
		$element->addMultiOption('personal', "Koncový zákazník");
		$element->addMultiOption('business', "Obchodní kontakt");
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_orders_tracking_no');
		$element->setLabel('Tracking no');
		$this->addElement($element);

    	$element = new Meduse_Form_Element_Select('meduse_order_status');
    	$element->setLabel('Stav');
		$element->addMultiOption('null', '-- neměnit --');
		if(Zend_Registry::get('acl')->isAllowed('parts/detail/price')){
			$element->addMultiOption('strategic', 'strategická');
		}
    	$element->addMultiOption('new', 'předběžná');
		$element->addMultiOption('confirmed', 'potvrzená');
		$element->addMultiOption('open', 'otevřená');
    	$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);
    }
    
}
