<?php

class Orders_FilterForm extends Meduse_FormBootstrap {

  protected $costs = NULL;

  public function __construct($options = NULL) {
    if (isset($options['costs'])) {
      $this->costs = $options['costs'];
      unset($options['costs']);
    }
    parent::__construct($options);
  }

  public function init() {
    /**
     * Create Elements
     */
    try {
      $this->setAttribs(array('class' => 'form'))
        ->setMethod('post');

      $db = Zend_Registry::get('db');
      $select = $db->select()
        ->from('orders', array(
          "id",
          "status2" => new Zend_Db_Expr("IF(status = 'strategic', 'A', IF(status = 'new', 'B', IF(status = 'confirmed', 'C', IF(status = 'open', 'D', status))))"),
          "status",
          "no",
          "title",
          "description",
          "date_deadline"
        ))
        ->where("status NOT IN ('deleted', 'closed', 'trash','offer')")
        ->where("id_wh_type = ?", Parts_Part::WH_TYPE_PIPES)
        ->order(array('status2', 'no'));

      $rows = $db->fetchAll($select);

      $showStrategicCost = Zend_Registry::get('acl')->isAllowed('orders/show-strategic-costs');
      foreach ($rows as $row) {
        $element = new Meduse_Form_Element_Checkbox($row['id']);
        try {
          $order = new Orders_Order($row['id']);
        }
        catch (Orders_Exceptions_ForbiddenAccess $e) {
          continue;
        }
        catch (Exception $e) {
          continue;
        }
        $element->setCheckedValue('on');

        switch ($row['status']) {
          case 'new':
            $status = 'Předběžná';
            break;
          case 'confirmed':
            $status = 'Potvrzená';
            $element->setChecked(true);
            break;
          case 'strategic':
            $status = 'Strategická';
            break;
          default;
            $status = 'Otevřená';
            $element->setChecked(true);
            break;
        }

        $desc = $row['description'];

        if ($showStrategicCost && $row['status'] == Table_Orders::STATUS_STRATEGIC && isset($this->costs[$row['id']])) {
          if ($this->costs[$row['id']]['operation_time_total'] < 60) {
            $time = sprintf('%01.0f min', $this->costs[$row['id']]['operation_time_total']);;
          }
          elseif ($this->costs[$row['id']]['operation_time_total'] < 480) {
            $time = sprintf('%01.2f h', $this->costs[$row['id']]['operation_time_total'] / 60);
          }
          else {
            $time = sprintf('%1.1f DP', $this->costs[$row['id']]['operation_time_total'] / 480);
          }
          $desc .= '<div class="pull-right">operace: <strong>' . $time . '</strong>, náklady: <strong>' . Meduse_Currency::format($this->costs[$row['id']]['price_total'], 'Kč', 0) . '</strong></div>';
        }
        $element->setLabel($row['no'] . '. ' . $row['title'] . '|' . $desc . '|' . $row['date_deadline'] . '|' . $order->getCountry() . '|' . $status);
        $this->addElement($element);
      }

      // kolekce
      $this->addElement(new Meduse_Form_Element_Collections('coll'));

      // kategorie
      $this->addElement(new Meduse_Form_Element_Categories(array('name' => 'ctg', 'all' => TRUE)));

      $element = new Meduse_Form_Element_Submit('Odeslat');
      $this->addElement($element);

      parent::init();
    }
    catch (Zend_Form_Exception $e) {}
    catch (Zend_Exception $e) {}
  }

}
