<?php

class Orders_Form extends Meduse_FormBootstrap {

  protected int $wh = Parts_Part::WH_TYPE_PIPES;

  protected Orders_Order $order;
  protected array $fees = [];
  protected string $fromActionUrl;
  protected string $action;

  public int $feesCount = 0;

  public function __construct($options = null)
  {
    $this->fromActionUrl = $options['from_action'];
    unset($options['from_action']);

    $this->action = $options['action'];
    unset($options['action']);

    $this->retrieveFees();

    parent::__construct($options);
  }

  public function init() {


    try {
      $this->setAttribs(['class' => 'form']);
      $this->setLegend('Nová objednávka');
      $this->setMethod(Zend_Form::METHOD_POST);
      $this->setAction($this->action);

      $element = new Meduse_Form_Element_Text('meduse_orders_no');
      $element->setRequired(TRUE);
      $element->setLabel('Číslo objednávky');
      $element->setAttrib('maxlength', '5');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Radio('meduse_orders_type');
      $element->setRequired(TRUE);
      $element->setLabel('Typ');
      $element->addMultiOptions([
        Table_Orders::TYPE_PRODUTCS => 'objednávka produktů',
        Table_Orders::TYPE_SERVICE => 'objednávka služeb',
      ]);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('meduse_orders_name');
      $element->setRequired(TRUE);
      $element->setLabel('Název');
      if (Utils::isDisallowed('order-edit/change-order-name')) {
        $element->setAttrib('readonly', 'readonly');
      }
      $this->addElement($element);

      $element = new Meduse_Form_Element_DatePicker('meduse_orders_deadline_internal', [
        'jQueryParams' => [
          'dateFormat' => 'dd.mm.yy',
          'minDate' => 0,
        ],
      ]);
      $element->setLabel('Interní deadline');
      $this->addElement($element);

      $element = new Meduse_Form_Element_DatePicker('meduse_orders_deadline', [
          'jQueryParams' => [
            'dateFormat' => 'dd.mm.yy',
            'minDate' => 0,
          ],
        ]);
      $element->setLabel('Deadline');
      $element->addValidator(new Meduse_Validate_DateMin(date('Y-m-d')));
      $this->addElement($element);

      $element = new Meduse_Form_Element_Checkbox('meduse_di');
      $element->setLabel('DI');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('meduse_orders_desc');
      $element->setLabel('Poznámka');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('meduse_orders_status');
      $element->setLabel('Stav objednávky');
      $this->addElement($element);

      // Záložka Fixní poplatky.
      $feeCount = 0;
      if ($this->fees) {
        foreach ($this->fees as $feeId => $feeName) {
          $feeCount++;
          $element = new Meduse_Form_Element_Checkbox('fee_' . $feeId);
          $element->setLabel($feeName);
          $this->addElement($element);
        }
      }
      $element = new Zend_Form_Element_Hidden('fees_count');
      $element->setValue($feeCount);
      $this->addElement($element);

      // Záložka Platba.
      $element = new Meduse_Form_Element_Select('meduse_orders_pricelist');
      $element->setLabel('Ceník');
      $element->addMultiOption(NULL, "-- neurčen --");
      $element->setRequired(TRUE);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('meduse_orders_payment_method');
      $element->setLabel('Způsob platby');
      $element->addMultiOption('null', "-- neurčen --");
      $element->addMultiOptions(Table_Orders::$paymentToString);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('meduse_orders_expected_payment_currency');
      $element->setLabel('Měna platby');
      $element->addMultiOption("CZK", "CZK");
      $element->addMultiOption("EUR", "EUR");
      $element->setValue('CZK');
      $this->addElement($element);

      // Záložka doparava.
      $tOrdersCarriers = new Table_OrdersCarriers();
      $element = new Meduse_Form_Element_Select('meduse_orders_carrier');
      $element->setRequired();
      $element->setLabel('Dopravce');
      $element->addMultiOption(Table_OrdersCarriers::UNDEFINED, "-- neurčen --");
      $element->addMultiOptions($tOrdersCarriers->getOptionsPairs($this->wh));
      $element->setValue(Table_OrdersCarriers::UNDEFINED);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Currency('meduse_orders_carrier_price');
      $element->setLabel('Cena dopravy [Kč]');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('Odeslat');
      $element->setOrder(100);
      $this->addElement($element);

      $element = new Zend_Form_Element_Hidden('from_action');
      $element->setValue($this->fromActionUrl);
      $this->addElement($element);
    }
    catch(Zend_Form_Exception|Zend_Exception $e) {
      die($e->getTraceAsString());
    }
    parent::init();
  }

  public function setNextNo($wh = NULL) {
    $tOrders = new Table_Orders();
    $no = $tOrders->getNextNo($wh);
    $this->getElement('meduse_orders_no')->setValue($no);
  }

  public function setExpectedPayment($total, $setReadOnly = TRUE) {
    $element = $this->getElement('meduse_orders_expected_payment');
    $element->setValue($total);
    if ($setReadOnly) {
      try {
        $element->setAttrib('readonly', 'readonly');
      }
      catch(Zend_Form_Exception $e) {
        die($e->getTraceAsString());
      }
    }
  }

  public function setStates(?string $orderStatus, ?bool $isService) {

    /** @var Meduse_Form_Element_Select $element */
    $element = $this->getElement('meduse_orders_status');

    switch ($orderStatus) {

      case Table_Orders::STATUS_STRATEGIC:
        $element->addMultiOption(Table_Orders::STATUS_STRATEGIC, Table_Orders::$statusToString[Table_Orders::STATUS_STRATEGIC]);
        $element->addMultiOption(Table_Orders::STATUS_NEW, Table_Orders::$statusToString[Table_Orders::STATUS_NEW]);
        break;

      case NULL:
        if (!$isService && Utils::isAllowed('orders/special/strategic')){
          $element->addMultiOption(Table_Orders::STATUS_STRATEGIC, Table_Orders::$statusToString[Table_Orders::STATUS_STRATEGIC]);
        }
        if (!$isService) {
          $element->addMultiOption(Table_Orders::STATUS_OFFER, Table_Orders::$statusToString[Table_Orders::STATUS_OFFER]);
        }
        $element->addMultiOption(Table_Orders::STATUS_NEW, Table_Orders::$statusToString[Table_Orders::STATUS_NEW]);
        break;

      case Table_Orders::STATUS_OFFER:
      case Table_Orders::STATUS_NEW:
        if (!$isService) {
          $element->addMultiOption(Table_Orders::STATUS_OFFER, Table_Orders::$statusToString[Table_Orders::STATUS_OFFER]);
        }
        $element->addMultiOption(Table_Orders::STATUS_NEW, Table_Orders::$statusToString[Table_Orders::STATUS_NEW]);
        break;

      case Table_Orders::STATUS_CONFIRMED:
        $element->addMultiOption(Table_Orders::STATUS_NEW, Table_Orders::$statusToString[Table_Orders::STATUS_NEW]);
        $element->addMultiOption(Table_Orders::STATUS_CONFIRMED, Table_Orders::$statusToString[Table_Orders::STATUS_CONFIRMED]);
        if ($isService) {
          $element->addMultiOption(Table_Orders::STATUS_CLOSED, Table_Orders::$statusToString[Table_Orders::STATUS_CLOSED]);
        }
        else {
          $element->addMultiOption(Table_Orders::STATUS_OPEN, Table_Orders::$statusToString[Table_Orders::STATUS_OPEN]);
        }
        break;

      case Table_Orders::STATUS_TRASH:
        $element->addMultiOption(Table_Orders::STATUS_NEW, Table_Orders::$statusToString[Table_Orders::STATUS_NEW]);
        $element->addMultiOption(Table_Orders::STATUS_TRASH, Table_Orders::$statusToString[Table_Orders::STATUS_TRASH]);
        break;

      case Table_Orders::STATUS_OPEN:
      default:
        $this->removeElement('meduse_orders_status');
        return;

    }

    $element->setValue($orderStatus ?? Table_Orders::STATUS_NEW);
  }

  protected function retrieveFees()
  {
    $this->fees = [];
    $tFees = new Table_ApplicationFees();
    if ($list = $tFees->list(true)) {
      foreach ($list as $item) {
        $this->fees[$item['id']] = $item['name'] . ' (' . Meduse_Currency::format($item['price'], 'EUR', 2, false) . ')';
      }
    }
    $this->feesCount = count($this->fees);
  }

  protected function removeIndexElements() {
    $this->removeElement('meduse_orders_no');
    $this->removeElement('meduse_orders_type');
    $this->removeElement('meduse_orders_name');
    $this->removeElement('meduse_orders_desc');
    $this->removeElement('meduse_orders_deadline');
    $this->removeElement('meduse_orders_deadline_internal');
    $this->removeElement('meduse_orders_status');
    $this->removeElement('meduse_di');
  }
  protected function removeFeesElements() {
    $this->removeElement('fees_count');
    foreach ($this->fees as $feeId => $fee) {
      $this->removeElement('fee_' . $feeId);
    }
  }
  protected function removePaymentElements() {
    $this->removeElement('meduse_orders_pricelist');
    $this->removeElement('meduse_orders_payment_method');
    $this->removeElement('meduse_orders_expected_payment_currency');
    // ???
    $this->removeElement('meduse_orders_discount');
    $this->removeElement('meduse_orders_discount_type');
    $this->removeElement('meduse_orders_pay_all');
    $this->removeElement('meduse_orders_pay_deposit');
  }

  protected function removeTransportElements() {
    $this->removeElement('meduse_orders_carrier');
    $this->removeElement('meduse_orders_carrier_price');
  }

  public function showIndexElements() {
    $this->setLegend('Objednávka');
    $this->removePaymentElements();
    $this->removeTransportElements();
    $this->removeFeesElements();
  }

  public function showFeesElements() {
    $this->setLegend('Objednávka – Fixní poplatky');
    $this->removeIndexElements();
    $this->removeTransportElements();
    $this->removePaymentElements();
  }

  public function showPaymentElements() {
    $this->setLegend('Objednávka – Platba');
    $this->removeIndexElements();
    $this->removeTransportElements();
    $this->removeFeesElements();
    if ($this->order->isService()) {
      $this->removeElement('meduse_orders_pricelist');
    }
  }

  public function showTransportElements() {
    $this->setLegend("Objednávka – Doprava");
    $this->removeIndexElements();
    $this->removePaymentElements();
    $this->removeFeesElements();
  }

  /**
   * @throws Zend_Db_Statement_Exception
   */
  public function setupOrder(Orders_Order $order): void
  {
    $this->order = $order;

    $this->meduse_orders_no->setValue($order->getNO());
    $this->meduse_orders_type->setValue($order->getType());
    $this->meduse_orders_name->setValue($order->getTitle());
    $this->meduse_orders_deadline->setValue($order->fromISOtoHR($order->getDeadline()));
    $this->meduse_orders_deadline_internal->setValue($order->fromISOtoHR($order->getDeadlineInternal()));
    $this->meduse_di->setValue($order->isDI() ? 'y' : 'n');
    $this->meduse_orders_desc->setValue($order->getDesc());
    $this->meduse_orders_status->setValue($order->getStatus());
    $this->meduse_orders_carrier->setValue($order->getCarrierMethodId());
    $this->meduse_orders_carrier_price->setValue($order->getRow()->carrier_price);
    $this->meduse_orders_expected_payment_currency->setValue($order->getRow()->expected_payment_currency);
    $this->meduse_orders_payment_method->setValue($order->getRow()->payment_method);

    $status = $order->getStatus();
    $service = $order->isService();
    $this->setStates($status, $service);

    $customer = $order->getCustomer(TRUE);
    if ($customer) {

      $priceListsValid = $customer->getType() !== Table_Customers::TYPE_B2C ? Pricelists::getAllPriceLists() : [Pricelists::getMasterPricelist()];

      $priceListCurrent = $customer->getPriceList();
      if ($priceListCurrent) {
        $this->meduse_orders_pricelist->removeMultiOption(NULL);
      }
      $this->addPriceLists($priceListsValid);
      $this->meduse_orders_pricelist->setValue(
        is_null($order->getRow()->id_pricelists) ?
          $priceListCurrent : $order->getRow()->id_pricelists
      );
    }

    // Poplatky.
    if ($fees = $order->getFees()) {
      foreach ($fees as $feeId => $fee) {
        $this->getElement('fee_' . $feeId)->setValue(true);
      }
    }

  }

  protected function addPriceLists($priceLists): void
  {
    foreach ($priceLists as $list) {
      $this->meduse_orders_pricelist->addMultiOption($list['id'], $list['name']);
    }
  }
}
