<?php

/**
 * Description of CarrierForm
 *
 * @author niecj
 */
class Orders_CarrierForm extends Meduse_FormBootstrap
{

    public function init()
    {        
        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
                ->setMethod('post');

        $element = new Meduse_Form_Element_Text('meduse_carrier_name');
        $element->setRequired(true);
        $element->setLabel('Název');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('meduse_carrier_ident_no');
        $element->addValidator(new Zend_Validate_StringLength(0, 100));
        $element->setLabel('Identifikační číslo');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('meduse_carrier_address');
        $element->setLabel('Adresa');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('meduse_carrier_www');
        $element->setLabel('www');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('meduse_carrier_desc');
        $element->setLabel('Poznámka k dopravci');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Submit('Odeslat');
        $this->addElement($element);

        parent::init();
    }

}
