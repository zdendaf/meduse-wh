<?php
class Orders_Order{

  const DEFAULT_QUEUE_EXPIRATION = 1800; // 30 minut
  const NOTIFIED_STATES = array(
    Table_Orders::STATUS_NEW,
    Table_Orders::STATUS_CONFIRMED,
    Table_Orders::STATUS_OPEN,
  );

  const DATA_UPLOADS_DIR = '/../data/uploads/';
  const DATA_UPLOADS_SUBDIR_PREFIX = 'mds_order_';

	/**
	 * @var Zend_Db_Row
	 */
	protected $_order;

	protected $_packaging;

	public $_address;

	public function __construct($id, $check_private = TRUE){
		$orders = new Table_Orders();

		if (!($row = $orders->find($id)->current())){
			throw new Exception("Neexistuje objednávka s id = '$id'.");
		}
		$this->_order = $row;
		$this->_address = $row['id_addresses'];

    // kontrola, zda-li neni objednávka soukromá
    if ($check_private && $this->isPrivate()) {
      $auth = new Zend_Session_Namespace('Zend_Auth');
      $acl = Zend_Registry::get('acl');
      if ($this->getOwnerId() != $auth->user_id && !$acl->isAllowed('orders/view-private')) {
        throw new Orders_Exceptions_ForbiddenAccess('Nemáte oprávnění přistupovat k soukromé objednávce č. ' . $this->getNO() . '.');
      }
    }
	}

	/**
	 * Vrai radek tabulky
	 * @return Zend_Db_Table_Row_Abstract
	 */
	public function getRow(){
		return $this->_order;
	}

	public function getID(){
		return $this->_order->id;
	}

	public function getNO(){
		return $this->_order->no;
	}

	public function getType(){
		return $this->_order->type;
	}

	public function getTitle(){
		return $this->_order->title;
	}

	public function setTitle($title) {
    $this->_order->title = $title;
    $this->_order->save();
  }

	public function getDesc(){
		return $this->_order->description;
	}

	public function getDateRequest(){
		return $this->_order->date_request;
	}

	public function getDateOpen(){
		return $this->_order->date_open;
	}

	public function getDeadline(){
		return $this->_order->date_deadline;
	}

  public function getDeadlineInternal(){
    return $this->_order->date_deadline_internal;
  }

	public function getDatePayDeposit(){
		return $this->_order->date_pay_deposit;
	}

  public function setDatePayDeposit($date, $save = TRUE){
     $this->_order->date_pay_deposit = $date;
    if ($save) {
      $this->_order->save();
    }
  }

	public function getDatePayAll(){
		return $this->_order->date_pay_all;
	}

  public function setDatePayAll($date, $save = TRUE){
    if ($this->getPaymentMethod() === Table_Orders::PM_CASH_ON_DELIVERY || $this->getWhType() == Table_TobaccoWarehouse::WH_TYPE) {
      $this->_order->date_payment_accept = $date;
    }
    $this->_order->date_pay_all = $date;
    if ($save) {
      $this->_order->save();
    }
  }

  public function getDatePaymentAccept(){
		return $this->_order->date_payment_accept;
	}

  public function setDatePaymentAccept($date, $save = TRUE){
    $this->_order->date_payment_accept = $date;
    if ($save) {
      $this->_order->save();
    }
  }

  public function getPaymentMethod($readable = FALSE): ?string
  {
    if (!$readable) {
      return $this->_order->payment_method;
    }
    return Table_Orders::$paymentToString[$this->_order->payment_method ?? Table_Orders::PM_UNKNOWN];
  }

	public function getDateExp(){
		return $this->_order->date_exp;
	}

  public function setDateExp(Meduse_Date $date, $save = TRUE) {
    $this->_order->date_exp = $date->toString('Y-m-d');
    if ($save) {
      $this->_order->save();
    }
  }

	public function getStatus(){
		return $this->_order->status;
	}

	public function isClosed() {
	  return $this->getStatus() == Table_Orders::STATUS_CLOSED;
  }

    public function getCarrierMethod(){
      return Zend_Registry::get('db')->fetchOne("SELECT name FROM orders_carriers WHERE id = ?", $this->_order->id_orders_carriers);
    }

    public function getCarrier(){
      $cid = Zend_Registry::get('db')->fetchOne("SELECT id_producers FROM orders_carriers WHERE id = ?", $this->_order->id_orders_carriers);
      return $cid ? new Producers_Carrier($cid) : NULL;
    }

    public function getCarrierID(){
      return Zend_Registry::get('db')->fetchOne("SELECT id_producers FROM orders_carriers WHERE id = ?", $this->_order->id_orders_carriers);
    }

    public function getCarrierMethodId() {
      return $this->_order->id_orders_carriers;
    }

  /**
   * Očekávaná měna objednávky.
   * @return string [CZK|EUR]
   */
    public function getExpectedCurrency() {
        return $this->_order->expected_payment_currency;
    }

    public function setExpectedCurrency($currency) {
      $this->_order->expected_payment_currency = $currency;
      $this->_order->save();
    }

  public function getExpectedPaymentDeposit() {
    return $this->_order->expected_payment_deposit;
  }

  public function setExpectedPaymentDeposit($price, $rate = NULL, $currency = 'EUR', $save = TRUE) {
    if (is_null($rate)) {
      $rate = $this->getExpectedPaymentRate();
    }
    if ($this->getExpectedPaymentCurrency() != $currency) {
      $price = $this->getExpectedPaymentCurrency() == 'CZK' ?
        $price * $rate : $price / $rate;
    }
    $this->_order->expected_payment_deposit = $price;
    if ($save) {
      $this->_order->save();
    }
  }


  /**
   * @deprecated use self::getTrackingInfo()
   * @return string
   */
    public function getTrackingNo() {
      return $this->getTrackingCodes(TRUE)
        ->from(['otc' => 'orders_tracking_codes'], [
          new Zend_Db_Expr('group_concat(distinct otc.code separator \', \')'),
        ])
        ->group('id_orders')
        ->query()
        ->fetchColumn();
    }

    public function getTrackingCodes($asSelect = false, $reWrite = true) {
      $table = new Table_OrdersTrackingCodes();
      $select = $table->select()->where('id_orders = ?', $this->getID());
      if ($asSelect) {
        return $select;
      }
      // prepsani kodu v linku
      if ($items = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll()) {
        if ($reWrite) {
          foreach ($items as &$item) {
            if ($item['link']) {
              $item['link'] = str_replace('[[code]]', $item['code'], $item['link']);
            }
          }
        }
        return $items;
      }
      else {
        return [];
      }
    }

  /**
   * @param array $data
   *
   * @return bool
   */
  public function insertTrackingCode(array $data) {
      $table = new Table_OrdersTrackingCodes();
      $data['id_orders'] = $this->getID();
      return $table->insert($data) !== false;
    }

  /**
   * @param int $id
   * @param array $data
   *
   * @return bool
   */
  public function updateTracingCode($id, array $data) {
      $table = new Table_OrdersTrackingCodes();
      return $table->update($data, 'id = ' . $id) === 1;
    }

  /**
   * @param int $id
   *
   * @return bool
   */
  public function removeTrackingCode(int $id) {
      $table = new Table_OrdersTrackingCodes();
      return $table->delete('id = ' . $id) === 1;
    }


    public function getCarrierMethodFullName() {
      if ($carrier = $this->getCarrier()) {
        $name = $carrier->getName();
        $method = $this->getCarrierMethod();
        return ($name == $method || $method == Table_OrdersCarriers::DEF_METHOD_NAME) ?
          $name : $name . ' – ' . $method;
      }
      else {
        return NULL;
      }
    }

    /**
     * Cena prepravy
     *
     * Pokud $useOrdersCurrency = TRUE, vrati se cena v mene objednavky.
     * Pokud $useOrdersCurrency = FALSE (vychozi), vrati se v CZK.
     *
     * @return decimal | null
     *
     */
    public function getCarrierPrice($useOrdersCurrency = FALSE) {
        if (!$this->_order->carrier_price) {
          return NULL;
        }
        $czk = (float) $this->_order->carrier_price;
        if (!$useOrdersCurrency) {
          return $czk;
        }
        $currency = $this->getExpectedPaymentCurrency();
        if ($currency == 'CZK') {
          return $czk;
        }
        $rate = $this->getExpectedPaymentRate();
        return (float) $czk / $rate;
    }

    public function getWhType() {
      return $this->_order->id_wh_type;
    }

  /**
	 * Vraci adresu ve stringu, nebo null
   * @return string|null
	 */
	public function getAddress() {
		if (is_null($this->_address)){
      return null;
    }
    $addr = new Addresses_Address($this->_address);
    return $addr;
	}

 	public function getAddressId() {
    return $this->_order->id_addresses;
	}

  public function setAddress(Addresses_Address $address) {
    $this->_order->id_addresses = $address->getId();
    $this->_order->save();
    if ($invoices = $this->getInvoices()) {
      /** @var Invoice_Invoice $invoice */
      foreach ($invoices as $invoice) {
        $invoice->calculateVat(true);
      }
    }
  }

	public function getCountry(){
		if($this->_address !== null){
			$db = Zend_Registry::get('db');
			if($country = $db->fetchOne("SELECT country FROM addresses WHERE id = ".$db->quote($this->_address))){
				return $country;
			} else return null;
		} else return null;
	}

	/**
	 * Vraci pocet produktu daneho ID v obednavce
	 * @param $id_product
	 * @return unknown_type
	 */
	public function getProductAmount($id_product){
		$orders_products = new Table_OrdersProducts();
		$select = $orders_products->select();
		$select->where("id_orders = ?", $this->_order->id)
		->where("id_products LIKE ?", $id_product);
		if(($row = $orders_products->fetchRow($select)) !== NULL)
			return $row['amount'];
		else return 0;
	}

  public function getProducts() {
    $orders_products = new Table_OrdersProducts();
		$select = $orders_products->select();
		$select->where("id_orders = ?", $this->_order->id);
    return $select->query()->fetchAll();
  }

  public function hasProducts() {
    $products = $this->getProducts();
    return !empty($products);
  }

  /**
   * Test, jestli má objednávka nějaké rezervované produkty.
   */
  public function hasReservedProducts() {
    $table = new Table_OrdersProductsParts();
    $data = $table->getOrdersReservations(array($this->getID()));
    return !empty($data);
  }

  public function isService() {
    return $this->getType() == Table_Orders::TYPE_SERVICE;
  }

  public function isPrivate() {
    return $this->_order->private == 'y';
  }

    /**
     * Get amount of free items
     *
     * @param int $id_product
     * @return int
     */
    public function getFreeItemAmount($id_product) {
        $orders_products = new Table_OrdersProducts();
        $select = $orders_products->select();
        $select->where("id_orders = ?", $this->_order->id)
            ->where("id_products LIKE ?", $id_product);

        $row = $orders_products->fetchRow($select);

        if (!$row) {
            return 0;
        }

        return $row['free_amount'];
    }

	/**
	 * Vraci pocet rezervovanych produktu objednavky
	 * @param $id_product
	 * @param $id_part
	 * @return int
	 */
	public function getReadyProductAmount($idProducts){
		$db = Zend_Registry::get('db');
		$db->setFetchMode(Zend_Db::FETCH_ASSOC);

		$select = $db->select();
		$select->from(
			array('op' => 'orders_products')
		)->join(
			array('opp' => 'orders_products_parts'),
			"op.id = opp.id_orders_products",
			array('amount')
		)->where("op.id_orders = ?", $this->_order->id)
		->where("op.id_products = ?", $idProducts);

		if($row = $db->fetchRow($select)){
			return $row['amount'];
		} else return 0;
	}

	public function setNo($no){
		$db = Zend_Registry::get('db');
		$db->update('orders', array("no" => $no));
	}

	/**
	 * upravi pocet produktu v dane objednavce podle zadaneho id
	 * @param unknown_type $id_product
	 * @param unknown_type $amount
	 * @return unknown_type
	 */
	public function setProductAmount($id_product, $amount){
		$db = Zend_Registry::get('db');
		$where = $db->select();
		$where->where('id_products = ?', $id_product)
		->where('id_orders = ?', $this->_order->id);
		$where = $where->getPart('WHERE');
		$where = $where[0].' '.$where[1];
		$db->update('orders_products', array(
			'amount' => $amount,
		), $where);
	}

    /**
     * Set new amount of free item
     *
     * @param int $id_product
     * @param int $amount
     */
    public function setFreeItemAmount($id_product, $amount){
		$db = Zend_Registry::get('db');
		$select = $db->select();
		$select->where('id_products = ?', $id_product)
                       ->where('id_orders = ?', $this->_order->id);

                $where = $select->getPart('WHERE');
		$where = $where[0].' '.$where[1];

                $db->update('orders_products', array(
                    'free_amount' => $amount,
		), $where);
	}

	/**
	 * Zrusi rezervaci soucastek pro zadany produkt a danou obednavku (vrati je
	 * do skladu)
	 * @param $id_product
	 * @param $id_part
	 * @return unknown_type
	 */
	public function cancelPrepared($id_products, $id_parts){

		$parts_warehouse = new Table_PartsWarehouse();
		$db = Zend_Registry::get('db');

		$select = $db->select();
		$select->from(array('op' => 'orders_products'), array())
		->joinLeft(array('opp' => 'orders_products_parts'), "op.id = opp.id_orders_products", array('opp.id','opp.amount', 'opp.id_parts'))
		->where("op.id_orders = ?", $this->_order->id)
		->where("op.id_products = ?", $id_products)
		->where("opp.id_parts = ?", $id_parts);
		if($row = $db->fetchRow($select)){
			$parts_warehouse->insertPart($id_parts, $row['amount']);
			$db->delete("orders_products_parts","id = ".$row['id']);
		}
		return;
	}

	public function cancelAllPrepared($id_products){
		$db = Zend_Registry::get('db');
		$db->setFetchMode(Zend_Db::FETCH_ASSOC);
		$select = $db->select();
		$select->from(array('op' => 'orders_products'), array())
		->join(array('opp' => 'orders_products_parts'), "op.id = opp.id_orders_products", array('opp.id','opp.amount', 'opp.id_parts'))
		->where("op.id_orders = ?", $this->_order->id)
		->where("op.id_products = ?", $id_products);
		if($rows = $db->fetchAll($select)){
			foreach($rows as $row){
				$this->cancelPrepared($id_products, $id_products);
			}
		}
		return;
	}

	/**
	 * Vyhodi objednavku do kose, ale nesmaze ji
	 */
	public function delete(): void
    {
		$orders = new Table_Orders();
		$data = array(
			'status' => 'trash',
            'date_delete' => date("Y-m-d"),
		);
		$orders->update($data, "id = ".$this->_order->id);
	}

	/**
	 * Delete from trash
	 */
	public function deleteFromTrash(){
		$orders = new Table_Orders();
		$data = array(
			'status' => 'deleted',
		);
		$orders->update($data, "id = ".$this->_order->id);
	}

	/**
	 * Odstrani objednavku vcetne polozek, a presune vychystane polozky zpet do
	 * skladu
	 * @return unknown_type
	 */
	public function deleteFinal(){
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try{
			//presunuti rezervovanych polozek zpatky do skladu
			$parts_warehouse = new Table_PartsWarehouse();
			$orders_products_parts = new Table_OrdersProductsParts();
			$select = $parts_warehouse->select();
			$select->setIntegrityCheck(false);
			$select->from(array("opp" => "orders_products_parts"), array("id_orders_products_parts" => "id","part_amount" => "amount", "id_parts"))
			->joinLeft(array("op" => "orders_products"), "op.id = opp.id_orders_products", array())
			->where("op.id_orders = ?", $this->_order->id);
			$rows = $parts_warehouse->getAdapter()->fetchAll($select);
			$ok = "ok1";
			foreach($rows as $row){
				$ok .= "|".$row['id_parts']."|". $row['part_amount']."|".$row['id_orders_products_parts']."|";
				$parts_warehouse->insertPart($row['id_parts'], $row['part_amount']);
				$orders_products_parts->delete("id = ".$orders_products_parts->getAdapter()->quote($row['id_orders_products_parts']));
			}
			$ok.= "ok2";
			//mazani polozek objednavky
			$orders_products = new Table_OrdersProducts();

			//ukladani polozek objednavky do historie
			$orders_products->getAdapter()->query("
			INSERT INTO orders_products_history (id ,id_orders ,id_products ,amount, free_amount )
			SELECT NULL AS id, id_orders, id_products, amount, free_amount FROM orders_products WHERE id_orders = ".$orders_products->getAdapter()->quote($this->_order->id)."
			");


			$orders_products->delete("id_orders = ".$orders_products->getAdapter()->quote($this->_order->id));
			$ok.= "ok3";
			//"vymazani" objednavky
			$orders = new Table_Orders();
			$orders->update(array("status" => "deleted"),"id = ".$orders->getAdapter()->quote($this->_order->id));
			$ok.= "ok4";
			$db->commit();
		} catch ( Exception $e ) {
			$db->rollback();
			throw new Exception("$ok Nepodarilo se smazat objednavku.".$e->getMessage());
		}
	}

	/**
	 * Mazani polozky objednavky
	 * @param $id_part
	 * @return unknown_type
	 */
	public function deleteItem($id_item){
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try{
			//mazani polozek objednavky
			$orders_products = new Table_OrdersProducts();
			$select = $orders_products->select()->where("id_orders =".$this->_order->id." AND id_products LIKE '$id_item'");
			$row = $orders_products->fetchRow($select);
			$ok = "id".$row['id']."|";
			$orders_products->getAdapter()->delete("orders_products" ,"id = ".$row['id']);
		} catch ( Exception $e ) {
			$db->rollback();
			throw new Exception("$ok Nepodarilo se smazat objednavku.".$e->getMessage());
		}
	}

	/**
	 * Rezervuje vsechny produkty jedne polozky a odebere je ze skladu
	 * @param $id_product
	 * @return unknown_type
	 */
	public function reserveAll($idProducts){
		$productAmount = $this->getProductAmount($idProducts);
		if($productAmount > 0){
			$this->reserveProduct($idProducts, $productAmount);
		} else throw new Exception("Produkt $idProducts neni v objednavce.");
	}

	/**
	 *
	 * @param <type> $idProducts
	 * @param <type> $amount pocet k zarezervovani
	 */
	public function reserveProduct($idProducts, $amount){
		$wh = new Table_PartsWarehouse();
		$db = $wh->getAdapter();
		$whAmount = $wh->getAmount($idProducts);
		$readyAmount = $this->getReadyProductAmount($idProducts); //pocet uz rezervovanych produktu
		$maxAmount = $this->getProductAmount($idProducts);

		if( ($readyAmount + $amount) > $maxAmount ){
			$amount = $maxAmount;
		}

		if( ($amount - $readyAmount) > 0 && ($amount - $readyAmount) <= $whAmount ){
			$idOrderProducts = $db->fetchOne("SELECT id FROM orders_products WHERE id_products = '$idProducts' AND id_orders = ".$this->getID());
			$wh->removePart($idProducts, ($amount - $readyAmount));
			if($db->fetchRow("SELECT * FROM orders_products_parts WHERE id_orders_products = ".$idOrderProducts)){
				$db->update("orders_products_parts", array('amount' => $amount,'id_parts'=>$idProducts), "id_orders_products = ".$idOrderProducts); //id_parts => pozustatek rezervace podsoucasti produtku
			} else {
				$db->insert("orders_products_parts", array('amount' => $amount, 'id_parts' => $idProducts, 'id_orders_products' => $idOrderProducts));
			}
		} elseif($amount > $whAmount) {
			 throw new Parts_Exceptions_LowAmount($idProducts);
		}
	}

	/**
	 * Vlozi rezervaci soucasti do db
	 * @param $id_products
	 * @param $id_parts
	 * @param $amount
	 * @return unknown_type
	 */
	public function reservePart($id_products, $id_parts, $amount){
		$db = Zend_Registry::get('db');
		$parts_warehouse = new Table_PartsWarehouse();

		$select = $db->select();
		$select->from(array('op' => 'orders_products'), array('id'))
		->where("op.id_orders = ?", $this->_order->id)
		->where("op.id_products = ?", $id_products);
		if($row1 = $db->fetchRow($select)){
				$db->insert('orders_products_parts', array(
					'id_orders_products' => $row1['id'],
					'id_parts' => $id_parts,
					'amount' => $amount,
				));
				$parts_warehouse->removePart($id_parts, $amount);
		} else throw new Exception ('chyba pri rezervaci soucasti '.$id_parts.' produktu '.$id_products);
	}

	/**
	 * Vraci na sklad zbozi expedovane objednavky a nastavuje ji jako zavrenou
	 * Ma smysl u expedovanych objednavek, ktere byly doruceny na dobirku
	 */
	public function returnFromClosed() {

		// vrati stav
		if ($this->_order->status == 'closed') {
			$this->_order->status = 'open';
			$this->_order->save();
			$db = Zend_Db_Table::getDefaultAdapter();
      $select = new Zend_Db_Select($db);
      $select->from('orders_products')->where('id_orders = ?', $this->_order->id);
			$products = $select->query()->fetchAll();
			$wh = new Table_PartsWarehouse();
			foreach ($products as $product) {
				$wh->insertPart($product['id_products'], $product['amount']);
			}
		}
	}

	/**
	 * Vraci true v pripade, ze jsou vsechny polozky objednavky nachystane
	 * @return unknown_type
	 */
	public function isReady() {
    if ($this->isService()) {
      return TRUE;
    }
		$orders_products = new Table_OrdersProducts();

		$select = $orders_products->select();
		$select->where("id_orders LIKE ?", $this->_order->id);
		$rows = $orders_products->fetchAll($select);

		foreach($rows as $row){
			if($row->amount > $this->getReadyProductAmount($row->id_products)){
				return FALSE;
			}
		}
		if (count($rows) > 0) {
			return TRUE;
		}
		else {
			return FALSE;
		}

	}

	/**
	 * Vykoná expedici objednávky
	 * @return unknown_type
	 */
	public function processExpedition(){

		if($this->isReady() || $this->isService()){

			$db = Zend_Registry::get('db');

			$orders = new Table_Orders();
			$orders_products = new Table_OrdersProducts();
			$orders_products_parts = new Table_OrdersProductsParts();
			$orders_producs_rows = $orders_products->fetchAll("id_orders = ".$this->_order->id);


			$id_orders_products_parts = '(';
			foreach($orders_producs_rows as $row){
				$id_orders_products_parts .= $row->id.',';
			}
			$id_orders_products_parts = substr($id_orders_products_parts, 0, -1); //odstraneni posledni carky
			$id_orders_products_parts .= ')';

			$db->beginTransaction();
			try{
				if(count($orders_producs_rows) > 0){ //pokud existuji polozky
					$orders_products_parts->delete("id_orders_products IN ".$id_orders_products_parts);
				}
				$data = array(
					'date_exp' => new Zend_Db_Expr("NOW()"),
					'status' => 'closed',
				);
				$orders->update($data, "id = ".$this->_order->id);
				$db->commit();
			} catch ( Exception $e ) {
				$db->rollback();
				throw new Exception ($e->getMessage());
			}

		} else {
			throw new Exception ("Nelze expedovat nehotovou objednavku.");
		}
	}

	/**
	 * Datum z ISO formatu do Human Readable
	 * @return unknown_type
	 */
	public function fromISOtoHR($date){
            if(!empty($date)){
                $tmp_date = new Zend_Date($date, "yyyy-MM-dd");
                return $tmp_date->toString("d.M.yyyy");
            } else return '';
	}

	/**
	 * Datum z Human Readable formatu do ISO
	 * @return unknown_type
	 */
	public function fromHRtoISO($date){
		$tmp_date = new Zend_Date($date, "d.MM.yyyy");
		return $tmp_date->toString("yyyy-MM-dd");
        }


        public function assignHolograms($holograms, $product) {
            foreach ($holograms as $hologram_id) {
                $hologram = new Holograms_Hologram(array('id' => $hologram_id));
                if ($hologram->isFree()) {
                    $hologram->assignTo($this->getID(), $product->getID());
                } else {
                    throw new Zend_Exception("Hologram id = $hologram_id je již použitý.");
                }
            }

        }




        public function unassignAllHolograms($part) {
            $tHolograms = new Table_Holograms();
            $where = $tHolograms->getAdapter()->quoteInto('id_orders = ?', $this->getID())
                   . $tHolograms->getAdapter()->quoteInto(' AND id_parts = ?', $part->getID());
            $tHolograms->update(array('id_orders' => NULL, 'id_parts' => NULL), $where);
        }

        public function getHologramProducts() {
            $db = Zend_Registry::get('db');

            $subSelect = new Zend_Db_Select($db);
            $subSelect
              ->from('holograms', array(
                'id_parts', 'assigned' => new Zend_Db_Expr('COUNT(id_parts)')))
              ->where('id_orders = ?', $this->getID());

            $select = new Zend_Db_Select($db);
            $select
              ->from(array('op' => 'orders_products'),
                array('id_orders', 'id' => 'id_products', 'amount'))
              ->joinLeft(array('p' => 'parts'),
                'op.id_products = p.id',
                array('name'))
              ->joinLeft(array('pc' => 'parts_collections'),
                'op.id_products = pc.id_parts',
                array('id_collections'))
              ->joinLeft(array('co' => 'collections'), 'pc.id_collections = co.id',
                array('collection' => 'name'))
              ->joinLeft(array('ho' => $subSelect),
                'op.id_products = ho.id_parts',
                array('assigned'))
              ->where('p.id_parts_ctg IN (?)', array(
                Table_PartsCtg::TYPE_SETS,
                Table_PartsCtg::EXTRA_ACCESSORIES))
              ->where('id_orders = ?', $this->getID())
              ->order('collection');
            return $select->query()->fetchAll();
        }

        public function getCountHolograms($id_parts) {
            $db = Zend_Registry::get('db');
            $row = $db->fetchRow('SELECT amount FROM orders_products AS op
                LEFT JOIN parts AS p ON op.id_products = p.id
                WHERE (p.id_parts_ctg = 23 OR p.id_parts_ctg = 7)
                AND id_orders = :id_orders AND op.id_products = :id_parts',
                array('id_orders' => $this->getID(),
                      'id_parts' => $id_parts));
            return $row["amount"];
        }

        public function getAssignedHolograms($part_id) {
            $part = new Parts_Part($part_id);

            $db = Zend_Registry::get('db');
            return $db->fetchAll('
                SELECT * FROM holograms
                LEFT JOIN collections ON holograms.id_categories = collections.id
                WHERE id_orders = :order_id AND id_parts = :part_id',
                array('order_id' => $this->getID(),
                      'part_id' => $part_id)
            );

        }



        public function getAvailableHolograms($part_id) {
            $part = new Parts_Part($part_id);
            $db = Zend_Registry::get('db');
            return $db->fetchAll('
                SELECT h.id, h.id_orders, h.id_parts, h.hologram, h.changed,
                h.inserted, c.name, c.abbr FROM holograms AS h
                LEFT JOIN collections AS c ON h.id_categories = c.id
                WHERE ((id_orders IS NULL AND id_parts IS NULL)
                OR (id_orders = :order_id AND id_parts = :part_id))
                AND id_categories = :cat_id',
                array('order_id' => $this->getID(),
                      'part_id' => $part_id,
                      'cat_id' => $part->getCollection())
            );
        }

    public function getPricelist() {
      return $this->_order->id_pricelists;
    }

		public function getCustomer($object = FALSE) {
			$tCustomers = new Table_Customers();
      if (is_null($this->_order['id_customers'])) {
        return null;
      }
			$customer = $tCustomers->find($this->_order['id_customers'])->current();
			return $object ? new Customers_Customer($customer->id) : $customer;
		}

		public function setCustomer(Customers_Customer $customerObj) {
	    $this->_order->id_customers = $customerObj->getId();
	    $this->_order->id_addresses = $customerObj->getBillingAddressId();
	    $this->_order->save();
    }

        public function getProformaNo() {
            return $this->_order['proforma_no'];
        }


        public function getOrderItemsPriceCount($asArray = TRUE) {
            $data = null;
            $customer = $this->getCustomer();
            $addVat = $this->getRegion() === Table_Addresses::REGION_NON_EU && !$this->getCustomer(TRUE)->isB2B();
            $vat = $addVat ? 1 + Utils::getActualVat() : 1;
            if ($customer) {
              $tPricelist = new Table_Pricelists();
              $pricelist = $this->getPricelist();
              if ($pricelist) {
                $select = new Zend_Db_Select($tPricelist->getAdapter());
                $select
                  ->from(array('op' => 'orders_products'),
                    array('CntEur' => new Zend_Db_Expr('SUM((op.amount - op.free_amount) * IF(pr.price IS NULL, 0, pr.price * ' . $vat . '))')))
                  ->joinLeft(array('pr' => 'prices'), 'pr.id_parts = op.id_products', array())
                  ->where('op.id_orders = ?', $this->_order->id)
                  ->where('pr.id_pricelists = ?', $pricelist);
                $data = $select->query()->fetch();
              }
            }
            if(!$data) {
                return $asArray ? array('CntEur' => '0.00') : 0;
            }
            else {
              return $asArray ? $data : (float) $data['CntEur'];
            }

        }


        public function generateProformaNo() {
          $proformNo = $this->getProformaNo();
          if (empty($proformNo)) {
            $tOrders = new Table_Orders();
            $tOrders->update(array('proforma_no' => $tOrders->getNextProformaNo($this->getWhType())), 'id = ' . $this->getID());
          }
        }

        public function setExpectedPayment($price, $rate = NULL, $currency = 'EUR') {
          if (is_null($rate)) {
            $rate = $this->getExpectedPaymentRate();
          }
          if ($this->getExpectedPaymentCurrency() != $currency) {
            $price = $this->getExpectedPaymentCurrency() == 'CZK' ?
              $price * $rate : $price / $rate;
          }
          $tOrders = new Table_Orders();
          $price =  round($price, $currency == 'EUR' ? 2 : 0);
          $tOrders->update(array('expected_payment' => $price), 'id = ' . $this->getID());
        }

        public function getExpectedPaymentCurrency() {
          return $this->_order['expected_payment_currency'];
        }

        public function getExpectedPaymentRate($default_as_null = FALSE) {
          if ($this->isExpectedPaymentRateDefault()) {
            return $default_as_null ? NULL : Utils::getCnbRates('EUR');
          }
          else {
            return $this->_order['expected_payment_rate'];
          }
        }

        public function getExpectedPayment() {
           return $this->_order['expected_payment'];
        }

        public function setExpectedPaymentRate($rate) {
          $tOrders = new Table_Orders();
          $tOrders->update(array('expected_payment_rate' => (float) $rate), 'id = ' . $this->getID());
        }

        public function isExpectedPaymentRateDefault() {
          return is_null($this->_order['expected_payment_rate']);
        }

        public function setPricelist($idPricelist) {
          $tOrders = new Table_Orders();
          $tOrders->update(array(
            'id_pricelists' => $idPricelist,
          ), 'id = ' . $this->getID());
        }
        public function getChangeAddressForm(
          $destination = NULL,
          $modal = FALSE) {
          return new Orders_ChangeAddressForm(array(
            'data' => array(
              'order' => $this,
              'destination' => $destination,
              'modal' => $modal,
            ),
          ));
        }

    /**
     * Ma objednavka sluzby.
     */
    public function hasServices() {
      $services = $this->getServices();
      return !empty($services);
    }

  public function getServices() {
    $tServices = new Table_OrdersServices();
    $select = $tServices->select()->where('id_orders = ?', $this->getID());
    return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }

  public function getItems() {
    return $this->isService() ? $this->getServices() : $this->getProducts();
  }

  public function hasItems() {
      $items = $this->getItems();
      return !empty($items);
  }

  public function getServicesTotalPrice()
  {
    $tServices = new Table_OrdersServices();
    $select = $tServices->select()->where('id_orders = ?', $this->getID());
    $select->from('orders_services', array(
      'total' => new Zend_Db_Expr('SUM(price*qty)'),
    ));
    return $select->query()->fetchColumn();
  }

  public function setServices(array $services)
  {

    $tServices = new Table_OrdersServices();
    $tServices->getAdapter()->beginTransaction();
    try {
      foreach ($services as $service) {
        $id = $service['id'];
        unset($service['id']);
        // existujici sluzba
        if (!is_null($id)) {
          if ($service['delete']) {
            $tServices->delete('id = ' . $id);
          } else {
            unset($service['delete']);
            $tServices->update($service, 'id = ' . $id);
          }
        } // nova sluzba
        elseif (!$service['delete']) {
          $data = $service + array('id_orders' => $this->getID());
          unset($data['delete']);
          $tServices->insert($data);
        }
      }
    } catch (Exception $e) {
      $tServices->getAdapter()->rollBack();
      return FALSE;
    }
    $tServices->getAdapter()->commit();
    return TRUE;
  }

    /**
     * @param bool $numbered_only
     * @return array<Invoice_Invoice>
     */
  public function getInvoices(bool $numbered_only = FALSE): array
  {
    $invoices = array();
    $tInvoices = new Table_Invoices();
    if ($invoicesList = $tInvoices->getAllInvoicesByOrder($this->getID(), $numbered_only)) {
      foreach ($invoicesList as $invoice) {
        try {
          $invoices[] = new Invoice_Invoice($invoice['id']);
        } catch (Exception $e) {
          Utils::logger()->err($e->getMessage());
        }
      }
    }
    return $invoices;
  }

  public function getRegularInvoice(): ?Invoice_Invoice
  {
    if ($invoices = $this->getInvoices(TRUE)) {
        foreach ($invoices as $invoice) {
        if ($invoice->isRegular()) {
            return $invoice;
        }
      }
    }
    return NULL;
  }

  public function getOwnerId()
  {
    return $this->_order->owner;
  }


  /**
   * Otestuje, jestli může být k objednávce vystavená regular faktura.
   * @return bool
   */
  public function checkRegularCondition() {
    return TRUE;
  }

  /**
   * Vrátí výchozí typ faktury podle stavu objenávky.
   * @return string|void
   * @throws Exception
   */
  public function getDefaultInvoiceType() {
    switch ($this->getStatus()) {
      case Table_Orders::STATUS_OFFER:
        return Table_Invoices::TYPE_OFFER;
        break;
      case Table_Orders::STATUS_NEW:
        return Table_Invoices::TYPE_PROFORM;
        break;
      case Table_Orders::STATUS_CONFIRMED:
      case Table_Orders::STATUS_OPEN:
      case Table_Orders::STATUS_CLOSED:
        return $this->checkRegularCondition() ? Table_Invoices::TYPE_REGULAR : Table_Invoices::TYPE_PROFORM;
        break;
      default:
        // HOTFIX. @todo: co se smazanou objednavkou?
        return Table_Orders::STATUS_OFFER;
        //throw new Exception('K objednávce nelze generovat fakturu, protože její status je "'. $this->getStatus() . '".');
    }
  }

  /**
   * Otestuje, zda-li je objednávka uhrazená,
   * tzn. jestli souhrn úhrad je alespoň takový, jako cena objednávky.
   */
  public function isPaid(): bool
  {
    return ($this->getPaidPercentage() >= 1);
  }

  /**
   * Otestuje, zda-li je uhrazená požadovaná záloha,
   * tzn. jestli souhr úhrad je alespoň jako požadovaná zláloha.
   * @return bool
   */
  public function isDepositPaid(): bool
  {
    return ($this->getPaidPercentage() >= $this->getExpectedPaymentDepositPercentage());
  }

  /**
   * Otestuje, zda-li ke všem doposud vystaveným fakturám objednávky existují úhrady v plné výši.
   * @deprecated
   */
   public function allInvoicesArePaid(): bool
   {
    $invoices = $this->getInvoices(TRUE);
    $paid = TRUE;
    // objednávka bez žádné faktury je neuhrazená.
    if (!$invoices) {
      return FALSE;
    }
    foreach($invoices as $invoice) {
      // pokud nalezneme ostrou uhrazenou fakturu, objednávka je uhrazená.
      if ($invoice->isRegular() && $invoice->isPaid()) {
        return TRUE;
      }
      // pokud nalezneme alespoň jednu neuhrazenou ostrou fakturu, končíme.
      elseif (!$invoice->isPaid()) {
        $paid = FALSE;
      };
    }
    return $paid;
  }

  /**
   * Testuje, jestli je požadované číslo je volné.
   *
   * @param $requestedNo
   * @param $orderId
   * @param int $whType
   * @return bool
   * @throws Zend_Exception
   */
  public static function isValidNo($requestedNo, $orderId, $whType = Table_PartsWarehouse::WH_TYPE) {
    if (!$requestedNo) {
      return FALSE;
    }
    $select = Zend_Registry::get('db')->select()
      ->from('orders', array('id'))
      ->where('no = ?', $requestedNo)
      ->where('id_wh_type = ?', $whType);
    if(!$fetch = $select->query()->fetchColumn()) {
      return TRUE;
    }
    else {
      return $orderId == $fetch;
    }
  }


  public function getDefaultAccount() {
    $table = new Table_Accounts();
    return $table->getDefaultAccountId($this->getWhType(), $this->getExpectedPaymentCurrency(), $this->getPaymentMethod());
  }


  public function getQueue($create = TRUE) {
    try {
      if ($this->_order->queue) {
        return EmailFactory_Queue::getQueue($this->_order->queue);
      }
    } catch (Exception $e) {}
    if ($create) {
      $queue = EmailFactory_Queue::createQueue($this->getOwnerId());
      $queue->setExpiration(self::DEFAULT_QUEUE_EXPIRATION);
      $queue->setEmailType(Table_Emails::EMAIL_NOTICE_ORDER_CHANGE);
      $queue->setSubject('Změna objednávky č. ' . $this->getNO() . ' – ' . $this->getTitle());
      $queue->setDescription('V objednávce "' . $this->getTitle() . '" č. ' . $this->getNO() . ' byly provedeny tyto změny:<br >');
      $queue->addRecipients(EmailFactory::USER_ID_WAREHOUSE);
      $this->_order->queue = $queue->getId();
      $this->_order->save();
      return $queue;
    }
    return NULL;
  }

  /**
   * @throws Orders_Exceptions_NoCustomer
   */
  public function getDue(): int
  {
    if ($this->getPaymentMethod() === Table_Orders::PM_CASH_ON_DELIVERY) {
      return Customers_Customer::DUE_DEFAULT_COD;
    }

    $customer = $this->getCustomer(TRUE);
    if (! $customer instanceof Customers_Customer) {
      throw new Orders_Exceptions_NoCustomer();
    }
    return $customer->getDue();
  }

  public function recalculateExpectedPayment($invalid_invoices = TRUE): void
  {
    $rate = $this->getExpectedPaymentRate();
    $currency = $this->getExpectedPaymentCurrency();
    if ($this->isService()) {
      $price = $this->getServicesTotalPrice();
      $carrierPrice = 0;
    }
    else {
      $price = $this->getOrderItemsPriceCount(FALSE);
      $carrierPrice = $this->getCarrierPrice(TRUE);
      if ($currency === Table_Orders::CURRENCY_CZK) {
        $price *= $rate;
      }
    }
    $this->setExpectedPayment($price + $carrierPrice, $rate, $currency);
    if ($invalid_invoices) {
      $this->invalidInvoices();
    }
  }

  public function invalidInvoices(): void
  {
    if ($invoices = $this->getInvoices()) {
      $rate = $this->getExpectedPaymentRate(TRUE);
      $currency = $this->getExpectedPaymentCurrency();
      /** @var  $invoice Invoice_Invoice */
      foreach ($invoices as $invoice) {
        if (!$invoice->isRegistered()) {
          $invoice->setRate($rate);
          $invoice->setCurrency($currency);
          $invoice->setInvalid();
        }
      }
    }

  }

  public function getPackaging($sums = FALSE) {
    if (!$this->_packaging) {
      $table = new Table_Packaging();
      $this->_packaging = $table->select()
        ->where('id_orders = ?', $this->getID())
        ->query()->fetchAll();
    }
    if ($sums && $this->_packaging) {
      $packaging = ['items' => [], 'volume' => 0, 'weight' => 0];
      foreach ($this->_packaging as $item) {
        $volume = $item['width'] * $item['depth'] * $item['height'] * (pow(10, -6));
        $item['volume'] = $volume;
        $packaging['items'][] = $item;
        $packaging['volume'] += $volume;
        $packaging['weight'] += $item['weight_brutto'];
      }
      return $packaging;
    }

    return $this->_packaging;
  }

  public function getPackagingCount() {
    $packaging = $this->getPackaging();
    return count($packaging);
  }

  public function getPackagingForm() {
    return new Orders_PackagingForm(['order' => $this]);
  }

  public function getTrackingForm() {
    return new Orders_TrackingNoForm(['order' => $this]);
  }


  public function removePackaging($id) {
    $table = new Table_Packaging();
    $table->delete('id = ' . $id);
  }

  public function updatePackaging($id, array $data) {
    $table = new Table_Packaging();
    $table->update($data, 'id = ' . $id);
  }

  public function insertPackaging(array $data) {
    $table = new Table_Packaging();
    $data['id_orders'] = $this->getID();
    $table->insert($data);
  }

  public function getFilesList() {
    $table = new Table_OrdersFiles();
    return $table->getFiles($this->getID());
  }

  public function fileDelete($fileId) {
    $table = new Table_OrdersFiles();
    try {
      $dir = APPLICATION_PATH . self::DATA_UPLOADS_DIR . self::DATA_UPLOADS_SUBDIR_PREFIX . $this->getID();
      $info = $table->find($fileId)->current()->toArray();
      if (unlink($dir . '/' . $info['filename'])) {
        $table->delete('id = ' . $fileId);
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    catch (Exception $e) {
      return FALSE;
    }
  }

  public function getFile($fileId) {
    $table = new Table_OrdersFiles();
    return $table->find($fileId)->current()->toArray();
  }

  public function isRequiredCOO(): bool
  {
    if ($fees = $this->getFees()) {
      foreach ($fees as $fee) {
        if ($fee['is_coo']) {
          return true;
        }
      }
    }
    return false;
  }

  public function getCOODateConfirm($format = FALSE) {
    return $format ? Meduse_Date::dbToForm($this->_order->coo_date_confirm) : $this->_order->coo_date_confirm;
  }

  public function getCOODateValidity($format = FALSE) {
    return $format ? Meduse_Date::dbToForm($this->_order->coo_date_validity) : $this->_order->coo_date_validity;
  }

  public function setCOODates(Meduse_Date $confirmDate = NULL, Meduse_Date $validityDate = NULL) {
    $this->_order->coo_date_confirm = $confirmDate ? $confirmDate->get('Y-m-d') : NULL;
    $this->_order->coo_date_validity = $validityDate ? $validityDate->get('Y-m-d') : NULL;
    $this->_order->save();
  }

  public function isRequiredInvoiceVerify(): bool
  {
    if ($fees = $this->getFees()) {
      foreach ($fees as $fee) {
        if ($fee['is_iv']) {
          return true;
        }
      }
    }
    return false;
  }

  public function isDI() {
    return $this->_order->di === 'y';
  }

  public function getCOODatesForm() {
    $form = new Orders_COODatesForm();
    $form->setAction('/orders/save-coo-dates');
    $form->id_orders->setValue($this->getID());
    $form->coo_date_confirm->setValue($this->getCOODateConfirm(TRUE));
    $form->coo_date_validity->setValue($this->getCOODateValidity(TRUE));
    return $form;
  }

  public function getHSForm() {
    return new Orders_HsForm(['order' => $this]);
  }

  /**
   * @param $priceListId int|null
   * @param $save bool
   *
   * @return void
   */
  public function assignPriceList($priceListId, $save = TRUE) {
    $this->getRow()->id_pricelists = $priceListId;
    if ($save) {
      $this->getRow()->save();
    }
  }

  public function getRegion() {
    $address = $this->getAddress();
    return $address ? $address->getRegion() : NULL;
  }
  
  
  
  public function checkPayments() {
    // Uhrada zalohy probehla.
    if ($this->isDepositPaid()) {
      /** @var Invoice_Payment $payment */
      $payment = $this->getLastPayment();
      if (!$this->getDatePayDeposit()) {
        $this->setDatePayDeposit($payment->getDate());
      }
      // Objednavka je plne uhrazena.
      if ($this->isPaid()) {
        $this->setDatePayAll($payment->getDate());
      }
      else {
        // Objednavna neni plne uhrazena.
        $this->setDatePayAll(NULL);
      }
    }
    else {
      // Neprobehla asi platba zalohy.
      $this->setDatePayDeposit(NULL);
    }
  }


  /**
   * Vrátí procentuální vyjádření zálohy vůči očekavané ceně objednávky.
   *
   * @return float
   */
  public function getExpectedPaymentDepositPercentage(): float
  {
    $expected = round($this->getExpectedPayment());
    $deposit = round($this->getExpectedPaymentDeposit());
    return $expected === 0.0 ? 1.0 : round($deposit) / round($expected);
  }

  /**
   * Vráti procentuální vyjádření sumy úhrad faktur vůči celkové ceně objednávky.
   * Pokud je vrácená hodnota >= 1, je objednávka uhrazená.
   * @param bool $recalculate
   * @return float
   */
  public function getPaidPercentage(bool $recalculate = TRUE): float
  {
    if (!$recalculate) {
      return $this->_order->paid_percentage ? $this->_order->paid_percentage / 100 : 0.0;
    }
    $expected = $this->getExpectedPayment();
    if ($expected < 0) {
      Utils::logger()->alert("Order ID = {$this->getID()} has set negative expected payment value.");
    }
    $paid = round($this->getPaymentsSum());
    $expected = abs(round($expected));
    $percentage = $expected === 0.0 ? 1.0 : round($paid) / round($expected);
    $this->_order->paid_percentage = $percentage * 100;
    $this->_order->save();
    return $percentage;
  }

  public function getPaymentsSum(): float
  {
    $paid = 0.0;
    if ($payments = $this->getPayments()) {
      /** @var Invoice_Payment $payment */
      foreach ($payments as $payment) {
        // Pokud nema objednavka vystavenou ostrou fakturu,
        // pak se do sumy uhrad se musi pocitat vsechny proformy.
        // Pokud má faktura už vystavenou ostrou objednavku,
        // pak se do sumy uhrad započítávají úhrady faktury a očíslovaných proforem.
        $hasRegular = $this->hasRegularInvoice();
        if (!$hasRegular || $payment->hasNo() || $payment->getInvoice()->isRegular()) {
          $paid += (float) $payment->getAmount();
        }
      }
    }
    return $paid;
  }

  public function hasRegularInvoice(): bool
  {
    return (bool)$this->getRegularInvoice();
  }

  public function getPayments($sorted = FALSE): array
  {
    $allPayments = [];
    $invoices = $this->getInvoices();
    /** @var Invoice_Invoice $invoice */
    foreach ($invoices as $invoice) {
      if ($payments = $invoice->getPayments()) {
        $allPayments = array_merge($allPayments, $payments);
      }
    }
    if ($sorted) {
      $this->sortPayments($allPayments);
    }
    return $allPayments;
  }

  protected function sortPayments(&$payments) {
    uasort($payments, function(Invoice_Payment $pa, Invoice_Payment $pb) {
      $da = $pa->getDate();
      $db = $pb->getDate();
      return $da === $db ? 0 : $da < $db;
    });
  }

  public function getLastPayment() {
    $payments = $this->getPayments(TRUE);
    return reset($payments);
  }

  public function setFees(array $feeIds) {
    if (!$feeIds) {
      return;
    }

    $tFees = new Table_ApplicationFees();
    $list = $tFees->list();

    $tOrderFees = new Table_OrdersFees();
    foreach ($feeIds as $feeId) {
      $tOrderFees->setFee($feeId, $list[$feeId]['price'], $this->getID());
    }
  }
  public function unsetFees(array $feeIds) {
    if (!$feeIds) {
      return;
    }

    $tOrderFees = new Table_OrdersFees();
    foreach ($feeIds as $feeId) {
      $tOrderFees->unsetFee($feeId, $this->getID());
    }
  }

  public function getFees(): array
  {
    $tOrderFees = new Table_OrdersFees();
    return $tOrderFees->getOrderFees($this->getID());
  }
}
