<?php
class Orders_TemplateForm extends Meduse_FormBootstrap {

  public function init(){
    // parent::__construct('parts_box', 'Nová šablona objednávky', $options);

    $this->setName('parts_box');
    $this->setMethod('post');
    $this->setAttrib('class', 'form');
    $this->setLegend('Nová šablona objednávky');

    $element = new Meduse_Form_Element_Text('meduse_orders_name');
		$element->setRequired(true);
		$element->setLabel('Jméno');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('meduse_orders_purchaser');
		$element->setRequired();
		$element->setLabel('Typ příjemce');
		$element->addMultiOption('personal', "Koncový zákazník");
		$element->addMultiOption('business', "Obchodní kontakt");
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_orders_desc');
		$element->setLabel('Poznámka');
		$this->addElement($element);

		$db = Zend_Registry::get('db');
		$element = new Meduse_Form_Element_Select('meduse_orders_carrier');
		$element->setRequired();
		$element->setLabel('Dopravce');
		$element->addMultiOptions($db->fetchPairs("SELECT id, name FROM orders_carriers"));
		$element->setValue(5);
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('meduse_orders_payment_method');
		$element->setLabel('Způsob platby');
		$element->addMultiOption('null', "-- neurčen --");
		$element->addMultiOption('cash_on_delivery', "Na dobírku");
		$element->addMultiOption('cash', "V hotovosti");
		$element->addMultiOption('transfer', "Převodem");
		$element->addMultiOption('free_presentation', "Zdarma - prezentace");
		$this->addElement($element);

    $element = new Meduse_Form_Element_Radio('meduse_orders_type');
    $element->setLabel('Typ');
		$element->addMultiOption('products', "Produkty");
		$element->addMultiOption('service', "Služby");
    $this->addElement($element);

		$element = new Zend_Form_Element_Hidden('address');
		$this->addElement($element);

		$element = new Zend_Form_Element_Hidden('order');
		$this->addElement($element);


    parent::init();
	}

}
