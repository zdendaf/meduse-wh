<?php


class Orders_PackagingForm extends Meduse_FormBootstrap {
  /** @var \Orders_Order  */
  protected $order = NULL;

  public function __construct($options = NULL) {
    if (isset($options['order'])) {
      $this->order = $options['order'];
      unset($options['order']);
    }
    parent::__construct($options);
  }

  public function init() {

    $this->setAttrib('class', 'form');
    $this->setAction('/orders/packaging-save');
    $this->setLegend('Rozměry a hmotnost balení');

    $element = new Zend_Form_Element_Hidden('id_orders');
    $element->setValue($this->order->getID());
    $this->addElement($element);

    $this->addItems();
    $this->addBlankItem();
    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Uložit změny');
    $this->addElement($element);

    parent::init();
  }

  protected function addItems() {
    $idx = 0;
    if ($packaging = $this->order->getPackaging()) {
      foreach ($packaging as $item) {

        $element = new Meduse_Form_Element_Select('type_' . $idx);
        $element->setLabel('Typ položky');
        $element->addMultiOptions(['box' => 'box', 'pallete' => 'paleta']);
        $element->setValue($item['type']);
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('id_' . $idx);
        $element->setValue($item['id']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('width_' . $idx);
        $element->setLabel('Šířka [cm]');
        $element->setRequired(TRUE);
        $element->addFilter(new Zend_Filter_Int());
        $element->addValidator(new Zend_Validate_GreaterThan(0));
        $element->setValue($item['width']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('depth_' . $idx);
        $element->setLabel('Hloubka [cm]');
        $element->setRequired(TRUE);
        $element->addFilter(new Zend_Filter_Int());
        $element->addValidator(new Zend_Validate_GreaterThan(0));
        $element->setValue($item['depth']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('height_' . $idx);
        $element->setLabel('Výška [cm]');
        $element->setRequired(TRUE);
        $element->addFilter(new Zend_Filter_Int());
        $element->addValidator(new Zend_Validate_GreaterThan(0));
        $element->setValue($item['height']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Float('weight_' . $idx);
        $element->setLabel('Váha [kg]');
        $element->setRequired(TRUE);
        $element->addValidator(new Zend_Validate_GreaterThan(0));
        $element->setValue($item['weight_brutto']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('remove_' . $idx);
        $element->setLabel('Odstranit');
        $this->addElement($element);

        $idx++;
      }
    }
    $element = new Zend_Form_Element_Hidden('count');
    $element->setValue($idx);
    $this->addElement($element);
  }

  protected function addBlankItem() {
    $element = new Zend_Form_Element_Hidden('id');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Select('type');
    $element->setLabel('Typ položky');
    $element->addMultiOptions([Table_Packaging::TYPE_BOX => 'box', Table_Packaging::TYPE_PALETTE => 'paleta']);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('width');
    $element->setLabel('Šířka [cm]');
    $element->addFilter(new Zend_Filter_Int());
    $element->addValidator(new Zend_Validate_GreaterThan(0));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('depth');
    $element->setLabel('Hloubka [cm]');
    $element->addFilter(new Zend_Filter_Int());
    $element->addValidator(new Zend_Validate_GreaterThan(0));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('height');
    $element->setLabel('Výška [cm]');
    $element->addFilter(new Zend_Filter_Int());
    $element->addValidator(new Zend_Validate_GreaterThan(0));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Float('weight');
    $element->setLabel('Váha [kg]');
    $element->addValidator(new Zend_Validate_GreaterThan(0));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('remove');
    $element->setLabel('Odstranit');
    $this->addElement($element);
  }

  public function getItemsCount() {
    if (!$this->order) {
      return NULL;
    }
    return $this->order->getPackagingCount();
  }
}