<?php
  
class Pipes_Parts_Part extends Parts_Part {
  const WH_TYPE = 1;
  public function __construct($id, $amount = 1) {
    parent::__construct($id, $amount, self::WH_TYPE);
  }
}