<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 27.6.2018
 * Time: 15:20
 */

class Utils {

  const RATE_FILE = 'https://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt';

  public static function getCnbRates($idx = NULL) {
    if (!$content = @file_get_contents(self::RATE_FILE)) {
      return 0;
    }
    $content = explode("\n", $content);
    unset($content[0]);
    unset($content[1]);

    $rates = array();
    foreach ($content as $it) {
      $rate = explode("|", $it);
      if (isset($rate[3])) {
        $rates[$rate[3]] = str_replace(",", ".", $rate[4]);
      }
    }
    if ($idx) {
      return $rates[$idx];
    }
    else {
      return $rates;
    }
  }

  /**
   * Utilita logování.
   */
  public static function logger(): Zend_Log {
    try {
      return Zend_Registry::get('logger');
    }
    catch (Zend_Exception $e) {
      die($e->getMessage());
    }
  }

  public static function addActuality($message) {
    try {
      $act = Zend_Registry::get('actualities');
    }
    catch (Zend_Exception $e) {
      die($e->getMessage());
    }
    $act->add($message);
  }

  public static function getActualVat($percent = FALSE) {
    $table = new Table_ApplicationDph();
    return $percent ? $table->getActualDph() : $table->getActualDph() / 100;
  }

  public static function getRate($wh = Table_PartsWarehouse::WH_TYPE): float {
      return (float) (new Table_ApplicationRate())->getRate($wh);
  }

  public static function toEur($value, $wh = Table_PartsWarehouse::WH_TYPE) {
    return $value / self::getRate($wh);
  }

  public static function toCzk($value, $wh = Table_PartsWarehouse::WH_TYPE) {
    return $value * self::getRate($wh);
  }

  public static function getSetting($name, $default = NULL) {
    try {
      return Zend_Registry::get('settings')->get($name, $default);
    }
    catch (Zend_Exception $e) {
      self::logger()->err($e->getMessage());
      return NULL;
    }
  }

  public static function setSetting($name, $value, $type = NULL, $description = NULL): bool {
    try {
      Zend_Registry::get('settings')->set($name, $value, $type, $description);
      return TRUE;
    }
    catch (Zend_Exception $e) {
      self::logger()->err($e->getMessage());
      return FALSE;
    }
  }

  public static function getCurrentUserId() {
    $authNamespace = new Zend_Session_Namespace('Zend_Auth');
    return $authNamespace->user_id;
  }
  
  public static function getCurrentUserLogin() {
    $auth = Zend_Auth::getInstance();
    return $auth->getIdentity();
  }

  public static function getCurrentUserRoles(): array
  {
    $authNamespace = new Zend_Session_Namespace('Zend_Auth');
    return $authNamespace->user_role ?? [];
  }

  public static function hasCurrentUserRole(string $role): bool
  {
    $roles = self::getCurrentUserRoles();
    return in_array($role, $roles);
  }

  public static function isAllowed($resource): bool {
      try {
          return Zend_Registry::get('acl')->isAllowed($resource);
      }
      catch (Zend_Exception $e) {
          self::logger()->warn($e->getMessage());
          return FALSE;
      }
  }

    public static function isDisallowed($resource): bool {
      return !self::isAllowed($resource);
    }
  
  /**
   * @return \Zend_Db_Adapter_Abstract;
   */
    public static function getDb(): Zend_Db_Adapter_Abstract {
      try {
        return Zend_Registry::get('db');
      }
      catch (Zend_Exception $e) {
        self::logger()->err($e->getMessage());
        die($e->getMessage());
      }
    }
}