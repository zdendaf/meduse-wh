<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Tobacco_Reports_EkokomForm extends Meduse_FormBootstrap {
    
    public function init() {
      
      $this->setLegend('Nastavení reportu');
      $this->setMethod(Zend_Form::METHOD_POST);
      $this->setAttribs(array('class' => 'form'));
      
      $element = new Meduse_Form_Element_DatePicker('from', array('jQueryParams' => array('dateFormat' => 'dd.mm.yy')));
      $element->setLabel('Počáteční datum:');
      
      $date = new DateTime();
      $date->sub(date_interval_create_from_date_string('1 month'));
      $element->setValue($date->format('d.m.Y'));
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_DatePicker('to', array('jQueryParams' => array('dateFormat' => 'dd.mm.yy')));
      $element->setLabel('Koncové datum:');
      $this->addElement($element);
      
      $this->addElement(new Meduse_Form_Element_Submit('Zobrazit'));
      
      parent::init();
    }
  
    
    
  }

  