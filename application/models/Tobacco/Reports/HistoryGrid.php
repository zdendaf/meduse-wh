<?php
/**
 * Zkonstruování ZGridu pro reporty historie a operace nad ním 
 *
 * @author Vojtěch Mrkývka
 */
class Tobacco_Reports_HistoryGrid {
  
  public function generateGrid($params = NULL) {
    $tUsersActionLog = new Table_UsersActionLog();
    $select = $tUsersActionLog->getDataForReportGrid();
    
    $grid = new ZGrid(array(
        'css_class' => 'table table-bordered table-hover table-striped',
        'curr_sort' => 'date',
        'curr_sort_order' => 'desc',
        'curr_items_per_page' => 25,
        'columns' => array(
            'date' => array(
                'renderer' => 'datetime',
                'header' => _('Datum a čas'),
                'css_style' => 'text-align: left',
                'sorting' => true,
            ),
            'username' => array(
                'header' => _('Uživatel'),
                'css_style' => 'text-align: left',
                'sorting' => true,
            ),
            'action' => array(
                'renderer' => 'function',
                'class_name' => 'Tobacco_Reports_HistoryGrid',
                'function_name' => 'getReadableControllerAction',
                'function_attributes' => array('[[action]]'),
                'header' => _('Adresa'),
                'css_style' => 'text-align: left',
                'sorting' => true,
            ),
            'operation' => array(
                'renderer' => 'function',
                'class_name' => 'Tobacco_Reports_HistoryGrid',
                'function_name' => 'getReadableOperation',
                'function_attributes' => array('[[id]]', '[[action]]', '[[params]]'),
                'header' => _('Akce'),
                'css_style' => 'text-align: left',
                'sorting' => false,
            ),
        ),
    ));
    $grid->setSelect($select);
    $grid->setRequest($params);
    return $grid->render();
  }
  
  public function getReadableControllerAction($controlleraction) {
    $tUAL = new Table_UsersActionLog();
    $addr = $tUAL->splitControllerAndAction($controlleraction);
    return $addr['controller'] . ' &rarr; ' . $addr['action'];
  }
  
  public function getReadableOperation($id,$controlleraction, $params) {
    $tUAL = new Table_UsersActionLog();
    $addr = $tUAL->splitControllerAndAction($controlleraction);
    $params = $tUAL->splitParamsToArray($params);
    $controller = $addr['controller'];
    $action = $addr['action'];
    
    $tSAL = new Table_SystemActionLog();
    $sys_actions_raw = $tSAL->select()->where('id_users_action_log = ?', $id)->query()->fetchAll();
    
    $sys_actions = '';
    if ($sys_actions_raw) {
      foreach ($sys_actions_raw as $sys_action_raw) {
        $sys_actions .= '<li><b>' . $sys_action_raw['action'] . '</b><br />'
                      . $sys_action_raw['params'] . '</li>';
      }
      $sys_actions = ' <button class="sys_params_toggle btn btn-mini btn-warning" id="' . $id . '">SYS.</button>'
              . '<div class="sys_params hidden" id="_params' . $id . '">' . $sys_actions . '</div>';
    }
    
    if ($controller == 'admin') {
      if ($action == 'flush-acl-cache') {
        $return = 'Vymazání ACL cache';
      }
    } elseif ($controller == 'email-templates') {
      if ($action == 'save') {$return = 'Přidání/upravení šablony e-mailu: ' . $params['email_template_name'];}
    } elseif ($controller == 'tobacco-order-edit') {
      if ($action == 'process') {
        if (isset($params['order'])) {
          $order = new Orders_Order($params['order']);
          $return = 'Objednávka č. ' . $order->getNO() . ': úprava';
        } else {
          $return = 'Objednávka č. ' . $params['meduse_orders_no'] . ': založení';   
        }
      }
    } elseif ($controller == 'tobacco') {
      if ($action == 'add-item') {  
          //v případě že parametry nejsou definované
          $params['id'] = (empty($params['id'])) ? '' : $params['id'];
          $params['amount'] = (empty($params['amount'])) ? '' : $params['amount'];
          
        $return = 'Množství položky ' . $params['id'] . ' zvýšeno o ' . $params['amount'] . ' mj';
      }
      
      if ($action=='macerations-check') {
        $return = 'Upraveno množství macerátu šarže <b>' . $params['batch'] . '</b> na ' . $params ['amount'] . ' kg';
      }
      
      if ($action=='macerations-merge') {
        //V db modelu ošetřeno, aby se braly jen řádky s odeslaným formulářem
        $return = 'Bylo vmícháno množství ' . $params['merge_amount'] . ' kg z macerátu <b>' . $params['merge_from'] . '</b> do <b>' . $params['merge_to'] . '</b>';
      }
      
      if ($action == 'macerations-save') {
        if(empty($params['id'])) { 
            //v případě že parametry nejsou definované 
            $params['name'] = (empty($params['name'])) ? '' : $params['name'];
            $params['total'] = (empty($params['total'])) ? '' : $params['total'];
            
          $return = 'Přidán macerát: <b>' . $params['name'] . '</b> (' . $params['total'] . ' kg)';
        } else {
          $tMacerations = new Table_Macerations();
          $maceration = $tMacerations->find((int)$params['id'])->current();
          $return = 'Upraven macerát: <b>' . $maceration['batch'] . ' &ndash; ' . $params['name'] . '</b> (' . $params ['total'] . ' kg)';
        }
      }
      
      if ($action=='macerations-storno') {
        $return = 'Stornován macerát <b>' . $params['batch_storno'] . '</b> se zbýv. množstvím ' . $params ['amount_storno'] . ' kg';
      }
  
      if ($action == 'parts-add') {
        $return = 'Přidána součást: ' . $params['name'];
      }
      
      if ($action == 'parts-pack-process') {
        $success = $tSAL->select()
                        ->where('id_users_action_log = ?', $id)
                        ->where('action = ?', 'add part')->query()->fetch();
        
        if ($success) {
          $return = 'Bylo nabaleno ' . $params['amount'] . ' ks <b>' . $params['part_id'] . '</b>';
        
        } else {
          $return = 'Nabalení ' . $params['amount'] . ' ks ' . $params['part_id'] . ' nebylo úspěšné';
        }
      }
    } elseif ($controller == 'tobacco-orders') {
      if ($action == 'delete-item') {
        $order = new Orders_Order($params['order']);
        $return = 'Objednávka č. ' . $order->getNO() . ': odebrána položka (' . $params['item'] . ')';
      }
      
      if ($action == 'stamp-all') {
        $order = new Orders_Order($params['id']);
        $return = 'Objednávka č. ' . $order->getNO() . ': okolkovat vše';
      }
    } elseif ($controller == 'tobacco-reports') {
      $return =  'Zobrazení stránky: ' . $action;
    }
    
    if (!isset($return)) {
      $return = '<i>NEZADÁN POPIS</i>';
    }
    
    return $return . $sys_actions;
  }
}
