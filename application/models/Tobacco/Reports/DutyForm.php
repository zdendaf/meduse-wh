<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Tobacco_Reports_DutyForm extends Meduse_FormBootstrap {

    public function init() {

      $this->setAttrib('class', 'form');
      $this->setLegend('Nastavení reportu pro Celní správu');

      $element = new Meduse_Form_Element_DatePicker('from');
      $element->setLabel('Začátek reportu:');
      $element->setRequired(TRUE);
      $this->addElement($element);

      $element = new Meduse_Form_Element_DatePicker('to');
      $element->setLabel('Konec reportu:');
      $element->setRequired(TRUE);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Radio('type');
      $element->setLabel('Typ reportu');
      $element->setRequired(TRUE);
      $element->addMultiOptions(array(
        Tobacco_Reports_Duty::TYPE_STAMPS => 'kolky',
        Tobacco_Reports_Duty::TYPE_SALES => 'prodeje',
        Tobacco_Reports_Duty::TYPE_PRODUCTION => 'výroba',
        Tobacco_Reports_Duty::TYPE_PRODUCTS => 'produkty',
      ));
      $this->addElement($element);

      $element = new Meduse_Form_Element_Checkbox('export');
      $element->setLabel('Exportní');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Radio('output');
      $element->setLabel('Výstup reportu');
      $element->setRequired(TRUE);
      $element->addMultiOptions(array(
        Tobacco_Reports_Duty::OUTPUT_SCREEN => 'na obrazovku',
        Tobacco_Reports_Duty::OUTPUT_FILE => 'do souboru',
      ));
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('gererate');
      $element->setLabel('Generovat');
      $this->addElement($element);

      parent::init();

    }

    public function setFrom(Meduse_Date $date) {
      $this->getElement('from')->setValue($date->toString('d.m.Y'));
    }

    public function setTo(Meduse_Date $date) {
      $this->getElement('to')->setValue($date->toString('d.m.Y'));
    }

    public function setType($type) {
      $this->getElement('type')->setValue($type);
    }

    public function setExport($bool) {
      $this->getElement('export')->setValue($bool);
    }

    public function setOutput($output) {
      $this->getElement('output')->setValue($output);
    }

  }
