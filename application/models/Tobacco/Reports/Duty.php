<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Tobacco_Reports_Duty {

    const TYPE_STAMPS = 'stamps';
    const TYPE_SALES = 'sales';
    const TYPE_PRODUCTS = 'products';
    const TYPE_PRODUCTION = 'production';

    const OUTPUT_SCREEN = 'screen';
    const OUTPUT_FILE = 'file';

    const CSV_SEPARATOR = ';';
    const CSV_QUOTE = "\"";
    const CSV_NEWLINE = "\n";

    const AMOUNTS = array(10, 50, 100, 150, 250);

    const DOWNLOAD_PATH = '/export/';
    const PRODUCTS_TEMPLATE   = '../public/duty/Evidence_37_verze_ 2.3_m01.xlsx';

    const PRODUCTS_XLSX_ROW_OFFSET = 7;
    const PRODUCTS_XLSX_ROW_MAX = 22;

    protected $_type = NULL;
    protected $_export = NULL;
    protected $_from = NULL;
    protected $_to = NULL;
    protected $_output = NULL;
    /** @var \Zend_Db_Select */
    protected $_select = NULL;
    protected $_flavour_selects = NULL;
    /** @var \Zend_Db_Statement */
    protected $_result = NULL;
    protected $_data = NULL;
    protected $_amounts = NULL;
    protected $_flavours = NULL;

    protected $_products = NULL;
    protected $_hash = NULL;

    public function init(array $options = array()) {
      if (isset($options['type'])) {
        $this->setType($options['type']);
      }
      if (isset($options['output'])) {
        $this->setType($options['output']);
      }
      if (isset($options['from'])) {
        $this->setFrom($options['from']);
      }
      if (isset($options['to'])) {
        $this->setTo($options['to']);
      }
      return $this;
    }

    public function setType($type) {
      if (!in_array($type, array(self::TYPE_STAMPS, self::TYPE_SALES, self::TYPE_PRODUCTS, self::TYPE_PRODUCTION))) {
        throw new Exception('Unrecognized type of report.');
      }
      $this->_type = $type;
      return $this;
    }

    public function setExport($bool) {
      $this->_export = $bool;
      return $this;
    }
    public function setOutput($output) {
      if (!in_array($output, array(self::OUTPUT_FILE, self::OUTPUT_SCREEN))) {
        throw new Exception('Unrecognized output of report.');
      }
      if ($this->_type == self::TYPE_PRODUCTION) {
        $this->_output = self::OUTPUT_SCREEN;
      }
      elseif ($this->_type == self::TYPE_PRODUCTS) {
        $this->_output = self::OUTPUT_FILE;
      }
      else {
        $this->_output = $output;
      }
      return $this;
    }
    public function setFrom(Meduse_Date $date) {
      $this->_from = $date;
      return $this;
    }

    public function getFrom() {
      return $this->_from;
    }

    public function setTo(Meduse_Date $date) {
      $this->_to = $date;
      return $this;
    }

    public function getTo() {
      return $this->_to;
    }

    public function setProducts($products) {
      $this->_products = $products;
    }
    public function setHash($hash) {
      $this->_hash = $hash;
    }
    public function setAmounts(array $amounts) {
      $this->_amounts = $amounts;
    }

    public function generate(array $options = array()) {
      if (!empty($options)) {
        $this->init($options);
      }
      if (is_null($this->_type)) {
        throw new Exception('Report type is not set.');
      }
      if (is_null($this->_from)) {
        throw new Exception('Report from date is not set.');
      }
      if (is_null($this->_to)) {
        throw new Exception('Report to date is not set.');
      }
      if (is_null($this->_amounts)) {
        $this->_amounts = self::AMOUNTS;
      }
      switch($this->_type) {
        case self::TYPE_STAMPS:
          $this->setSelect_stamps();
          break;
        case self::TYPE_SALES:
          $this->setSelect_flavours();
          $this->setSelect_sales();
          break;
        case self::TYPE_PRODUCTION:
          $this->setSelect_production();
          break;
        case self::TYPE_PRODUCTS:
          $this->setSelect_products();
          break;
      }
      return $this;
    }

    protected function setSelect_stamps() {

      $db = Zend_Registry::get('db');
      $this->_select = $db->select();

      $this->_select->from(array('i' => 'invoice'), array(
        'datum_prodeje' => new Zend_Db_Expr('DATE_FORMAT(issue_date, "%d.%m.%Y")'),
        'cislo_faktury' => 'no',
      ));

      $this->_select->joinLeft(array('o' => 'orders'), 'o.id = i.id_orders', array());

      $this->_select->joinLeft(array('c' => 'customers'), 'c.id = o.id_customers', array(
        'firma' => 'company',
        'ic' => 'ident_no',
        'dic' => 'vat_no',
        'odp_osoba' => new Zend_Db_Expr("CONCAT(c.first_name, ' ', c.last_name)")
      ));

      $this->_select->joinLeft(array('op' => 'orders_products'), 'op.id_orders = o.id', array(
        'celkem' => new Zend_Db_Expr("SUM(op.amount)"),
      ));

      $this->_select->joinLeft(array('p' => 'parts'), 'p.id = op.id_products', array());
      $this->_select->joinLeft(array('ps' => 'parts_subparts'), 'p.id = ps.id_multipart', array());
      $this->_select->joinInner(array('p2' => 'parts'),
        'p2.id = ps.id_parts AND p2.id_parts_ctg = ' . Table_TobaccoWarehouse::CTG_TOBACCO_NFS, array());

      $this->_select->joinLeft(array('pm010' => 'parts_mixes'),
        'pm010.id_parts = p2.id AND pm010.amount = 0.010', array(
        'mnozstvi_10g' => new Zend_Db_Expr('SUM(IF(ISNULL(pm010.amount), 0, op.amount))')
      ));

      $this->_select->joinLeft(array('pm050' => 'parts_mixes'),
        'pm050.id_parts = p2.id AND pm050.amount = 0.050', array(
        'mnozstvi_50g' => new Zend_Db_Expr('SUM(IF(ISNULL(pm050.amount), 0, op.amount))')
      ));

      $this->_select->joinLeft(array('pm100' => 'parts_mixes'),
        'pm100.id_parts = p2.id AND pm100.amount = 0.100', array(
          'mnozstvi_100g' => new Zend_Db_Expr('SUM(IF(ISNULL(pm100.amount), 0, op.amount))')
        ));

      $this->_select->joinLeft(array('pm150' => 'parts_mixes'),
        'pm150.id_parts = p2.id AND pm150.amount = 0.150', array(
        'mnozstvi_150g' => new Zend_Db_Expr('SUM(IF(ISNULL(pm150.amount), 0, op.amount))')
      ));

      $this->_select->joinLeft(array('pm250' => 'parts_mixes'),
        'pm250.id_parts = p2.id AND pm250.amount = 0.250', array(
        'mnozstvi_250g' => new Zend_Db_Expr('SUM(IF(ISNULL(pm250.amount), 0, op.amount))')
      ));

      $this->_select->where('i.id_wh_type = ?', 2);
      $this->_select->where('i.no <> ?', '');
      $this->_select->where('i.`type` = ?', Table_Invoices::TYPE_REGULAR);
      $this->_select->where('o.status = ?', Table_Orders::STATUS_CLOSED);
      $this->_select->where('p.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_TOBACCO_FS);
      $this->_select->where('o.date_exp >= ?', $this->_from->get('Y-m-d'));
      $this->_select->where('o.date_exp <= ?', $this->_to->get('Y-m-d'));

      $this->_select->group('i.id');
      $this->_select->order(array('i.no ASC', 'i.issue_date ASC'));
      $this->_result = $this->_select->query(Zend_Db::FETCH_OBJ);
    }

    protected function setSelect_sales() {

      $this->_result = array();

      $db = Zend_Registry::get('db');

      // prodej kolkovaneho zbozi
      $select1 = $db->select();
      $select1->from(array('i' => 'invoice'), array(
        'datum_prodeje' => new Zend_Db_Expr('DATE_FORMAT(issue_date, "%d.%m.%Y")'),
        'cislo_faktury' => 'no',
      ));
      $select1->join(array('o' => 'orders'), 'o.id = i.id_orders', array());
      $select1->join(array('c' => 'customers'), 'c.id = o.id_customers', array(
        'firma' => 'company',
        'ic' => 'ident_no',
        'dic' => 'vat_no',
        'seed' => 'seed_id',
        'seed' => 'seed_id',
        'odp_osoba' => new Zend_Db_Expr("CONCAT(c.first_name, ' ', c.last_name)")
      ));
      $select1->joinLeft(array('op' => 'orders_products'), 'op.id_orders = o.id', array(
        'celkem' => new Zend_Db_Expr("SUM(op.amount)"),
      ));
      $select1->joinLeft(array('p' => 'parts'), 'p.id = op.id_products', array());
      $select1->joinLeft(array('ps' => 'parts_subparts'), 'p.id = ps.id_multipart', array());
      $select1->joinInner(array('p2' => 'parts'),
        'p2.id = ps.id_parts AND p2.id_parts_ctg = ' . Table_TobaccoWarehouse::CTG_TOBACCO_NFS, array());
      $select1->where('i.id_wh_type = ?', 2);
      $select1->where('i.`type` = ?', Table_Invoices::TYPE_REGULAR);
      $select1->where('i.`no` <> ?', '');
      $select1->where('o.`status` = ?', Table_Orders::STATUS_CLOSED);
      $select1->where('p.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_TOBACCO_FS);
      $select1->where('i.issue_date >= ?', $this->_from->get('Y-m-d'));
      $select1->where('i.issue_date <= ?', $this->_to->get('Y-m-d'));
      $select1->where('i.export = ?', $this->_export ? 'y' : 'n');
      $select1->group('i.id');
      $select1->order(array('i.no ASC', 'i.issue_date ASC'));

      // prodej nekolkovaneho zbozi
      $select2 = $db->select();
      $select2->from(array('i' => 'invoice'), array(
        'datum_prodeje' => new Zend_Db_Expr('DATE_FORMAT(issue_date, "%d.%m.%Y")'),
        'cislo_faktury' => 'no',
      ));
      $select2->join(array('o' => 'orders'), 'o.id = i.id_orders', array());
      $select2->join(array('c' => 'customers'), 'c.id = o.id_customers', array(
        'firma' => 'company',
        'ic' => 'ident_no',
        'dic' => 'vat_no',
        'seed' => 'seed_id',
        'odp_osoba' => new Zend_Db_Expr("CONCAT(c.first_name, ' ', c.last_name)")
      ));
      $select2->joinLeft(array('op' => 'orders_products'), 'op.id_orders = o.id', array(
        'celkem' => new Zend_Db_Expr("SUM(op.amount)"),
      ));
      $select2->joinLeft(array('p' => 'parts'), 'p.id = op.id_products AND p.id_parts_ctg = ' . Table_TobaccoWarehouse::CTG_TOBACCO_NFS, array());
      $select2->where('i.id_wh_type = ?', 2);
      $select2->where('i.`type` = ?', Table_Invoices::TYPE_REGULAR);
      $select2->where('i.`no` <> ?', '');
      $select2->where('o.`status` = ?', Table_Orders::STATUS_CLOSED);
      $select2->where('p.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_TOBACCO_NFS);
      $select2->where('i.issue_date >= ?', $this->_from->get('Y-m-d'));
      $select2->where('i.issue_date <= ?', $this->_to->get('Y-m-d'));
      $select2->where('i.export = ?', $this->_export ? 'y' : 'n');
      $select2->group('i.id');
      $select2->order(array('i.no ASC', 'i.issue_date ASC'));
      $this->_select = $db->select()->union(array('(' . $select1 . ')', '(' . $select2 .')'));
      $this->_select->order('cislo_faktury');
      $this->_result['main'] = $this->_select->query(Zend_Db::FETCH_OBJ);
      $this->_result['flavours'] = array();
      foreach ($this->_flavour_selects as $idx => $select) {
        $this->_result['flavours'][$idx] = $select->query(Zend_Db::FETCH_OBJ);
      }
    }

    protected function setSelect_products() {

      if (is_null($this->_products)) {
        $products = self::getProductsList();
        $this->_products = array();
        foreach ($products as $row) {
          $this->_products[] = $row->id;
        }
      }
      $db = Zend_Registry::get('db');
      $this->_select = array();
      $this->_result = array();

      foreach ($this->_products as $id) {
        $product = new Tobacco_Parts_Part($id);
        if ($product->isTobacco() || $product->isTobaccoNFS()) {
          $this->_select[$id] = $db->select()
            ->from(array('op' => 'orders_products'), array('amount'))
            ->joinLeft(array('o' => 'orders'), 'o.id = op.id_orders', array('date_request', 'date_exp', 'purchaser'))
            ->joinLeft(array('a' => 'addresses'), 'o.id_addresses = a.id', array(
              'code' => 'country',
              'address' => new Zend_Db_Expr("CONCAT(a.`company`, ' ', `person`, ', ', `street`, ' ', `pop_number`,  '/', `orient_number`, ' ', `city`, ' ', `zip`, ' ', `country`)")
            ))
            ->joinLeft(array('cc' => 'c_country'), 'cc.code = a.country', array('country' => 'name', 'region'))
            ->joinLeft(array('i' => 'invoice'), 'i.id_orders = o.id AND i.`type` = \'regular\'', array('invoice' => 'no'))
            ->joinLeft(array('c' => 'customers'), 'c.id = o.id_customers', array(
              'customer' => new Zend_Db_Expr('CONCAT(c.first_name, \' \', c.last_name, \' - \', c.company)'),
              'ident_no', 'seed_id'
            ))
            ->joinLeft(array('oc' => 'orders_carriers'), 'o.id_orders_carriers = oc.id', array())
            ->joinLeft(array('p' => 'producers'), 'p.id = oc.id_producers', array('carrier_id' => 'id', 'carrier_name' => 'name', 'carrier_ident_no' => 'ident_no'))
            ->where('o.`status` = ?', Table_Orders::STATUS_CLOSED)
            ->where('op.id_products = ?', $id)
            ->where('o.date_exp >= ?', $this->_from->get('Y-m-d'))
            ->where('o.date_exp <= ?', $this->_to->get('Y-m-d'))
            ->order('o.date_request ASC');
        }
        $this->_result[$id] = $this->_select[$id]->query(Zend_Db::FETCH_OBJ);
      }
    }

    protected function setSelect_production() {

      $this->_select = [];
      $this->_result = [];

      $this->_select['recipes'] = Zend_Registry::get('db')->select();
      $this->_select['sums'] = Zend_Registry::get('db')->select();

      $this->_select['recipes']
        ->from(['ph' => 'parts_warehouse_history'], ['id_parts', 'amount' => new Zend_Db_Expr('SUM(-ph.amount)')])
        ->join(['mp' => 'macerations_parts_warehouse_history'], 'mp.id_parts_warehouse_history = ph.id', [])
        ->join(['m' => 'macerations'], 'm.id = mp.id_macerations', ['id_recipes'])
        ->join(['r' => 'recipes'], 'r.id = m.id_recipes', ['name_recipes' => 'name'])
        ->join(['p' => 'parts'], 'p.id = ph.id_parts', ['name_parts' => 'name', 'measure'])
        ->where('ph.last_change >= ?', $this->_from->get('Y-m-d 00:00:00'))
        ->where('ph.last_change <= ?', $this->_to->get('Y-m-d 23:59:59'))
        ->group(['id_recipes', 'id_parts'])
        ->order(['id_recipes', 'id_parts']);
      $this->_result['recipes'] = $this->_select['recipes']->query(Zend_Db::FETCH_OBJ);

      $this->_select['sums']
        ->from(['ph' => 'parts_warehouse_history'], ['amount' => new Zend_Db_Expr('SUM(-ph.amount)')])
        ->join(['mp' => 'macerations_parts_warehouse_history'], 'mp.id_parts_warehouse_history = ph.id', ['id_macerations'])
        ->join(['m' => 'macerations'], 'm.id = mp.id_macerations', ['id_recipes', 'batch'])
        ->join(['r' => 'recipes'], 'r.id = m.id_recipes', [])
        ->where('ph.last_change >= ?', $this->_from->get('Y-m-d 00:00:00'))
        ->where('ph.last_change <= ?', $this->_to->get('Y-m-d 23:59:59'))
        ->group(['id_macerations'])
        ->order(['id_recipes', 'id_macerations']);
      $this->_result['sums'] = $this->_select['sums']->query(Zend_Db::FETCH_OBJ);
    }


    public function getResult() {
      return $this->_result;
    }

    public function fetchData() {
      if ($this->_type == self::TYPE_STAMPS) {
        $this->_data = $this->_result->fetchAll();
      }
      elseif ($this->_type == self::TYPE_SALES) {
        $data = array();
        $data['main'] = $this->_result['main']->fetchAll();
        $data['flavours'] = array();
        foreach ($this->_result['flavours'] as $idx => $result) {
          $data['flavours'][$idx] = $result->fetchAll();
        }

        $fl_data = array();
        foreach ($this->_flavours as $flavour) {
          $fl_data[$flavour->mix] = $flavour;
        }

        $this->_data = array();
        foreach ($data['flavours'] as $f => $flavour) {
          $total = array();
          $this->_data[$f] = array(
            'flavor' => $fl_data[$f],
            'rows' => array(),
            'summary' => NULL,
          );
          foreach ($flavour as $id => $row) {
            $include = FALSE;
            foreach ($this->_amounts as $a) {
              $var = "mnozstvi_{$a}_{$f}";
              $amount = (int) $row->{$var};
              if (!isset($total["amount_{$a}_{$f}"])) {
                $total["amount_{$a}_{$f}"] = 0;
                $total["weight_{$a}_{$f}"] = 0;
              }
              if ($amount > 0) {
                $include = TRUE;
                $total["amount_{$a}_{$f}"] += $amount;
                $total["weight_{$a}_{$f}"] += ($amount * ($a / 1000));
              }
            }
            if ($include) {
              $this->_data[$f]['rows'][] = (object) array_merge((array)$data['main'][$id], (array)$row);
            }
            $this->_data[$f]['summary'] = (object) $total;
          }
        }
      }
      elseif ($this->_type == self::TYPE_PRODUCTS) {
        $this->_data = array();
        foreach ($this->_result as $productId => $result) {
          $this->_data[$productId] = new stdClass();
          $this->_data[$productId]->product = new Tobacco_Parts_Part($productId);
          $this->_data[$productId]->report = $result->fetchAll();
        }
      }
      elseif ($this->_type == self::TYPE_PRODUCTION) {
        $data = [];
        while($row = $this->_result['recipes']->fetch()) {
          if (!isset($data[$row->id_recipes])) {
            $data[$row->id_recipes] = [
              'id_recipes' => $row->id_recipes,
              'name_recipes' => $row->name_recipes,
              'total' => 0,
              'parts' => [],
              'batches' => [],
            ];
          }
          $amount = $row->amount / 1000;
          $data[$row->id_recipes]['total'] += $amount;
          $data[$row->id_recipes]['parts'][$row->id_parts] = [
            'id_parts' => $row->id_parts,
            'name_parts' => $row->name_parts,
            'amount' => $amount,
          ];
        }
        while($row = $this->_result['sums']->fetch()) {
          $data[$row->id_recipes]['batches'][$row->id_macerations] = [
            'id_macerations' => $row->id_macerations,
            'batch' => $row->batch,
            'amount' => $row->amount / 1000,
          ];
        }
        $this->_data = $data;
      }
      return $this;
    }

    public function getData() {
      return $this->_data;
    }

    public function getFlavours() {
      return $this->_flavours;
    }

    public function getSummary() {

      switch ($this->_type) {

        case self::TYPE_STAMPS:
          $summary = new stdClass();
          $summary->total_10g = 0;
          $summary->total_50g = 0;
          $summary->total_100g = 0;
          $summary->total_150g = 0;
          $summary->total_250g = 0;
          $summary->total = 0;
          if ($this->_data) {
            foreach ($this->_data as $row) {
              $summary->total_10g += $row->mnozstvi_10g;
              $summary->total_50g += $row->mnozstvi_50g;
              $summary->total_100g += $row->mnozstvi_100g;
              $summary->total_150g += $row->mnozstvi_150g;
              $summary->total_250g += $row->mnozstvi_250g;
            }
            $summary->total =
              $summary->total_10g
              + $summary->total_50g
              + $summary->total_100g
              + $summary->total_150g
              + $summary->total_250g;
          }
          break;
        case self::TYPE_PRODUCTS:
          $summary = [
            'date_from' => $this->getFrom(),
            'date_to' => $this->getTo(),
          ];
          break;
        default:
          $summary = NULL;
      }
      return $summary;
    }

    public function getSelect() {
      return $this->_select;
    }

    public function render($output = NULL) {
      if ($output) {
        $this->setOutput($output);
      }
      switch ($this->_output) {

        case self::OUTPUT_SCREEN:
          return $this->_render_html();
          break;

        case self::OUTPUT_FILE:
          switch($this->_type) {
            case self::TYPE_PRODUCTS: return $this->_render_xlsx_products();
            case self::TYPE_STAMPS: return $this->_render_xlsx_stamps();
            case self::TYPE_SALES: return $this->_render_xlsx_sales();
          }
          break;
      }
    }

    public function __toString() {
      return $this->render();
    }

    protected function _render_html() {
      $view = new Zend_View();
      $view->setScriptPath(APPLICATION_PATH . '/views/scripts');
      $view->data = $this->fetchData()->getData();
      $view->summary = $this->getSummary();
      switch ($this->_type) {
        case self::TYPE_STAMPS:
          $html = $view->render('tobacco-reports/duty-stamps.phtml');
          break;
        case self::TYPE_SALES:
          $view->variants = $this->getVariants();
          $view->amounts = $this->getAmounts();
          $view->from = $this->getFrom();
          $view->to = $this->getTo();
          $html = $view->render('tobacco-reports/duty-sales.phtml');
          break;
        case self::TYPE_PRODUCTS:
          $html = $view->render('tobacco-reports/duty-products.phtml');
          break;
        case self::TYPE_PRODUCTION:
          $html = $view->render('tobacco-reports/duty-production.phtml');
          break;
      }
      return $html;
    }

    protected function _render_xlsx_stamps() {

      $doc = new PHPExcel();
      $sheet = $doc->getActiveSheet();

      $header = array(
          'datum prodeje',
          'čislo faktury',
          'firma',
          'IČ',
          'DIČ',
          'odp. osoba',
          '10 g',
          '50 g',
          '100 g',
          '150 g',
          '250 g',
          'celkem'
      );
      $r = 1;
      $c = 0;
      foreach ($header as $value) {
        $sheet->getCellByColumnAndRow($c, $r)->setValue($value);
        $c++;
      }

      $this->fetchData()->getData();
      if ($this->_data) {
        foreach($this->_data as $row) {
          $r++;
          $line = array(
            $row->datum_prodeje,
            $row->cislo_faktury,
            $row->firma,
            $row->ic,
            $row->dic,
            $row->odp_osoba,
            $row->mnozstvi_10g,
            $row->mnozstvi_50g,
            $row->mnozstvi_100g,
            $row->mnozstvi_150g,
            $row->mnozstvi_250g,
            $row->celkem,
          );
          $c = 0;
          foreach ($line as $value) {
            $sheet->getCellByColumnAndRow($c, $r)->setValue($value);
            $c++;
          }
        }

        $summary = $this->getSummary();
        $footer = array(
            'celkem', '', '', '', '', '',
            $summary->total_10g,
            $summary->total_50g,
            $summary->total_100g,
            $summary->total_150g,
            $summary->total_250g,
            $summary->total,
          );
        $r++;
        $c = 0;
        foreach ($footer as $value) {
          $sheet->getCellByColumnAndRow($c, $r)->setValue($value);
          $c++;
        }
      }
      return $doc;
    }

    protected function _render_xlsx_sales() {

      $doc = new PHPExcel();
      $this->fetchData()->getData();

      if (!$this->_data) {
        return $doc;
      }

      $doc->removeSheetByIndex();
      $amounts = $this->getAmounts();

      foreach ($this->_data as $f => $section) {
        $title = substr($section['flavor']->name, 0, 31);
        $sheet = new PHPExcel_Worksheet($doc, $title);
        $hcols = array(
          'datum prodeje',
          'čislo faktury',
          'firma',
          'IČ',
          'DIČ',
          'SEED',
          'odp. osoba'
        );
        foreach ($amounts as $a) {
          $hcols[] = ($a / 1000) . ' kg';
        }

        $r = 1;
        $c = 0;
        foreach ($hcols as $value) {
          $sheet->getCellByColumnAndRow($c, $r)->setValue($value);
          $c++;
        }

        foreach($section['rows'] as $row) {
          $r++;
          $line = array(
            $row->datum_prodeje,
            $row->cislo_faktury,
            $row->firma,
            $row->ic,
            $row->dic,
            $row->seed,
            $row->odp_osoba,
          );
          foreach($amounts as $a) {
            $line[] = $row->{"mnozstvi_{$a}_{$f}"};
          }
          $c = 0;
          foreach ($line as $value) {
            $sheet->getCellByColumnAndRow($c, $r)->setValue($value);
            $c++;
          }
        }

        $r += 2;
        $scols = array("Sumarizace prodeje výrobků s příchutí {$section['flavor']->name}", '', '', '', '', '', 'balení');
        foreach ($amounts as $a) {
          $scols[] = ($a / 1000) . ' kg';
        }
        $c = 0;
        foreach ($scols as $value) {
          $sheet->getCellByColumnAndRow($c, $r)->setValue($value);
          $c++;
        }

        $r++;
        $text = 'za období ' . $this->getFrom()->get('d.m.Y') . ' – ' . $this->getTo()->get('d.m.Y');
        $scols = array($text, '', '', '', '', '', 'Počet celkem [ks]');
        foreach ($amounts as $a) {
          $scols[] = $section['summary']->{"amount_{$a}_{$f}"};
        }
        $c = 0;
        foreach ($scols as $value) {
          $sheet->getCellByColumnAndRow($c, $r)->setValue($value);
          $c++;
        }

        $r++;
        $scols = array('', '', '', '', '', '', 'Váha celkem [kg]');
        foreach ($amounts as $a) {
          $scols[] = $section['summary']->{"weight_{$a}_{$f}"};
        }
        $c = 0;
        foreach ($scols as $value) {
          $sheet->getCellByColumnAndRow($c, $r)->setValue($value);
          $c++;
        }
        $doc->addSheet($sheet);
      }

      return $doc;
    }

    private function q($str) {
      $str = str_replace(self::CSV_QUOTE, '\\' . self::CSV_QUOTE, $str);
      return self::CSV_QUOTE . $str . self::CSV_QUOTE;
    }

    protected function fetchFlavours() {

      $db = Zend_Registry::get('db');
      $select = $db->select();
      $select->from(array('pm' => 'parts_mixes'), array('mix' => 'id_mixes'));
      $select->join(array('m' => 'mixes'), 'm.id = pm.id_mixes', array('name'));
      $select->group(array('pm.id_mixes'));
      $select->order(array('pm.id_mixes'));
      $this->_flavours = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
    }

    public function getVariants() {
      $variants = array();
      foreach ($this->_flavours as $flavour) {
        foreach ($this->_amounts as $amount) {
          $variants[$amount . '_' . $flavour->mix] = $flavour->name . ' ' . $amount . 'g';
        }
      }
      return $variants;
    }

    public function getAmounts() {
      return $this->_amounts;
    }

    protected function setSelect_flavours() {

      $this->_flavour_selects = array();
      $this->fetchFlavours();

      if ($this->_flavours) {

        $db = Zend_Registry::get('db');
        foreach ($this->_flavours as $variant) {

          // prodej kolkovaneho zbozi
          $select1 = $db->select();
          $select1->from(array('i' => 'invoice'), array(
            'cislo_faktury' => 'no',
          ));
          $select1->join(array('o' => 'orders'), 'o.id = i.id_orders', array());
          $select1->joinLeft(array('op' => 'orders_products'), 'op.id_orders = o.id', array());
          $select1->joinLeft(array('p' => 'parts'), 'p.id = op.id_products', array());
          $select1->joinLeft(array('ps' => 'parts_subparts'), 'p.id = ps.id_multipart', array());
          $select1->joinInner(array('p2' => 'parts'),
            'p2.id = ps.id_parts AND p2.id_parts_ctg = ' . Table_TobaccoWarehouse::CTG_TOBACCO_NFS, array());
          foreach ($this->_amounts as $amount) {
            $vid = $amount . '_' . $variant->mix;
            $select1->joinLeft(array('pm' . $vid => 'parts_mixes'),
              'pm' . $vid . '.id_parts = p2.id'
              . ' AND pm' . $vid . '.amount = ' . $amount / 1000
              . ' AND pm' . $vid . '.id_mixes = ' . $variant->mix, array(
              'mnozstvi_' . $vid => new Zend_Db_Expr('SUM(IF(ISNULL(pm' . $vid . '.amount), 0, op.amount))')
            ));
          }
          $select1->where('i.id_wh_type = ?', 2);
          $select1->where('i.`type` = ?', Table_Invoices::TYPE_REGULAR);
          $select1->where('i.`no` <> ?', '');
          $select1->where('o.`status` = ?', Table_Orders::STATUS_CLOSED);
          $select1->where('p.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_TOBACCO_FS);
          $select1->where('i.issue_date >= ?', $this->_from->get('Y-m-d'));
          $select1->where('i.issue_date <= ?', $this->_to->get('Y-m-d'));
          $select1->where('i.export = ?', $this->_export ? 'y' : 'n');
          $select1->group('i.id');
          $select1->order(array('i.no ASC', 'i.issue_date ASC'));

          // prodej nekolkovaneho zbozi do zahranici
          $select2 = $db->select();
          $select2->from(array('i' => 'invoice'), array(
            'cislo_faktury' => 'no',
          ));
          $select2->join(array('o' => 'orders'), 'o.id = i.id_orders', array());
          $select2->joinLeft(array('op' => 'orders_products'), 'op.id_orders = o.id', array());
          $select2->joinLeft(array('p' => 'parts'), 'p.id = op.id_products AND p.id_parts_ctg = ' . Table_TobaccoWarehouse::CTG_TOBACCO_NFS, array());
          foreach ($this->_amounts as $amount) {
            $vid = $amount . '_' . $variant->mix;
            $select2->joinLeft(array('pm' . $vid => 'parts_mixes'),
              'pm' . $vid . '.id_parts = p.id'
              . ' AND pm' . $vid . '.amount = ' . $amount / 1000
              . ' AND pm' . $vid . '.id_mixes = ' . $variant->mix, array(
              'mnozstvi_' . $vid => new Zend_Db_Expr('SUM(IF(ISNULL(pm' . $vid . '.amount), 0, op.amount))')
            ));
          }

          $select2->where('i.id_wh_type = ?', 2);
          $select2->where('i.`type` = ?', Table_Invoices::TYPE_REGULAR);
          $select2->where('i.`no` <> ?', '');
          $select2->where('o.`status` = ?', Table_Orders::STATUS_CLOSED);
          $select2->where('p.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_TOBACCO_NFS);
          $select2->where('i.issue_date >= ?', $this->_from->get('Y-m-d'));
          $select2->where('i.issue_date <= ?', $this->_to->get('Y-m-d'));
          $select2->where('i.export = ?', $this->_export ? 'y' : 'n');
          $select2->group('i.id');
          $select2->order(array('i.no ASC', 'i.issue_date ASC'));

          $select = $db->select();
          $select->union(array('(' . $select1 . ')', '(' . $select2 . ')'))->order('cislo_faktury');

          $this->_flavour_selects[$variant->mix] = $select;
        }

      }
    }

    protected function _render_xlsx_products() {

      $data = $this->fetchData()->getData();
      $summary = $this->getSummary();

      foreach ($data as $pid => $dataset) {
        $path = Zend_Registry::get('config')->aclCache->cacheDir . self::DOWNLOAD_PATH;
        if (!file_exists($path)) {
          mkdir($path);
        }
        $filename = $path . $this->_hash . '_' . $dataset->product->getID() . '.xlsx';
        $objPHPExcel = $this->_getProductsXLSX($dataset, $summary);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(TRUE);
        $objWriter->save($filename);
      }
      return $filename;
    }

    public static function getZipFile($hash) {
      $zip = new ZipArchive();
      $path = Zend_Registry::get('config')->aclCache->cacheDir . self::DOWNLOAD_PATH;
      $zipname = Zend_Registry::get('config')->aclCache->cacheDir . self::DOWNLOAD_PATH . base64_decode($hash) . '.zip';
      if ($zip->open($zipname, ZipArchive::CREATE) !== TRUE) {
        die("Could not open archive " . $zipname);
      }
      $list = array();
      if ($handle = opendir($path)) {
        while (false !== ($entry = readdir($handle))) {
          if (strpos($entry, $hash) !== FALSE) {
            $list[] = $entry;
          }
        }
        closedir($handle);
      }

      foreach ($list as $pid => $file) {
        list($x, $name) = explode('_', $file, 2);
        $zip->addFile($path . $file, $name);
      }
      $zip->close();
      foreach ($list as $file) {
        unlink($path . $file);
      }
      return $zipname;
    }

    private function _getProductsXLSX($dataset, $summary) {

      $title = 'Report za období ' . $this->getFrom()->get('d.m.Y') . ' - ' . $this->getFrom()->get('d.m.Y');
      $reader = new PHPExcel_Reader_Excel2007();
      $objPHPExcel = $reader->load($this::PRODUCTS_TEMPLATE);
      $objPHPExcel->getProperties()->setCreator('MEDITE s.r.o.');
      $objPHPExcel->getProperties()->setLastModifiedBy('MEDITE s.r.o.');
      $objPHPExcel->getProperties()->setTitle($title);
      $objPHPExcel->getProperties()->setSubject($title);

      $numRows = count($dataset->report);
      $offset = self::PRODUCTS_XLSX_ROW_OFFSET;
      $last = self::PRODUCTS_XLSX_ROW_OFFSET + $numRows - 1;
      $sum = self::PRODUCTS_XLSX_ROW_OFFSET + $numRows;

      $sheet = $objPHPExcel->setActiveSheetIndex(1)->copy();
      $sheet->setTitle($dataset->product->getID());

      $sheet->setCellValue("B1", $summary['date_from']->get('Y'));
      $sheet->setCellValue("G1", $dataset->product->getHS());
      $sheet->setCellValue("I1", $dataset->product->getID());
      $sheet->setCellValue("N1", $dataset->product->getName());

      $sheet->setCellValue("R1", 'Počáteční stav množství vybraných výrobků k ' . $summary['date_from']->get('d.m.'));
      $sheet->setCellValue("U1", 0);
      $sheet->setCellValue("A29", 'Součet k ' . $summary['date_to']->get('d.m.'));

      // vlozeni dalsich radku, pokud je jich v reportu vic

      if ($numRows > self::PRODUCTS_XLSX_ROW_MAX) {
        for ($i = 0; ($numRows - self::PRODUCTS_XLSX_ROW_MAX) > $i; $i++) {
          $sheet->insertNewRowBefore(self::PRODUCTS_XLSX_ROW_OFFSET + self::PRODUCTS_XLSX_ROW_MAX, 1);
        }
      }
      elseif ($numRows < self::PRODUCTS_XLSX_ROW_MAX) {
        $sheet->removeRow(self::PRODUCTS_XLSX_ROW_OFFSET + $numRows, self::PRODUCTS_XLSX_ROW_MAX - $numRows);
      }

      // plneni daty
      $idx = 0;
      foreach ($dataset->report as $line) {

        $row = $idx + $this::PRODUCTS_XLSX_ROW_OFFSET;

        $sheet->setCellValue("A$row", ($idx + 1));
        $sheet->setCellValue("B$row", $line->date_request);
        $sheet->setCellValue("C$row", $line->date_exp);
        $sheet->setCellValue("D$row", $line->invoice);
        $sheet->setCellValue("E$row", $line->amount);

        $sheet->setCellValue("M$row", $line->amount);
        $sheet->setCellValue("N$row",$line->carrier_ident_no ? $line->carrier_ident_no : ($line->carrier_id == Table_OrdersCarriers::EX_WORKS_TOBACCO ? '' : $line->carrier_name));

        $sheet->setCellValue("R$row", $line->seed_id);
        $sheet->setCellValue("V$row", $line->code == Table_Addresses::COUNTRY_CZ ? ($line->purchaser == 'business' ? $line->ident_no : 'FO') : $line->address);

        $sheet->setCellValue("X$row", 'ne');

        $idx++;
      }

      if ($numRows > 0) {
        $sheet->setCellValue("E$sum", "=SUM(E$offset:E$last)");
        $sheet->setCellValue("M$sum", "=SUM(M$offset:M$last)");
      }

      $sheet->setCellValue("E" . ($sum + 1), 0);

      $objPHPExcel->addSheet($sheet);
      $objPHPExcel->removeSheetByIndex(1);

      return $objPHPExcel;
    }

    public static function getProductsList() {
      $tParts = new Table_Parts();
      return $tParts->getProducts(Table_TobaccoWarehouse::WH_TYPE);
    }

  }
