<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Tobacco_Reports_SalesForm extends Meduse_FormBootstrap {

    protected $wh = Table_TobaccoWarehouse::WH_TYPE;

    public function init() {

      $this->setLegend('Nastavení reportu');
      $this->setMethod(Zend_Form::METHOD_POST);
      $this->setAttribs(array('class' => 'form'));

      $element = new Meduse_Form_Element_DatePicker('from', array('jQueryParams' => array('dateFormat' => 'dd.mm.yy')));
      $element->setLabel('Počáteční datum:');

      $date = new DateTime();
      $date->sub(date_interval_create_from_date_string('1 month'));
      $element->setValue($date->format('d.m.Y'));
      $this->addElement($element);

      $element = new Meduse_Form_Element_DatePicker('to', array('jQueryParams' => array('dateFormat' => 'dd.mm.yy')));
      $element->setLabel('Koncové datum:');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('country');
      $db = Zend_Registry::get('db');
      $rows = $db->fetchPairs("SELECT code, CONCAT(name,' (',code,')') FROM c_country WHERE code <> 'CZ' ORDER BY name ");
      $element->addMultiOption('', '-- všechny země --');
      $element->addMultiOption('CZ', 'Česká republika (CZ)');
      $element->addMultiOption('EU', 'Evropská unie');
      $element->addMultiOption('EU_NOT_CZ', 'Evropská unie mimo ČR');
      $element->addMultiOption('NOT_CZ', 'všechny země mimo ČR');
      $element->addMultiOptions($rows);
      $element->setLabel('Země');
      $this->addElement($element);

      $tCustomers = new Table_Customers();
      $sql = "SELECT c.id, TRIM(IF(c.company <> ' ', c.company, IF (c.last_name <> '' OR c.first_name <> '', CONCAT(c.last_name, ' ', c.first_name), IF(a.person <> '' OR a.company <> '', CONCAT(a.person, ' ', a.company), CONCAT('[ID: ', c.id, ']'))))) AS name FROM customers c LEFT JOIN addresses a on c.id_addresses = a.id WHERE id_wh_type = :wh ORDER BY name;";
      $customers = $tCustomers->getAdapter()->fetchPairs($sql, array('wh' => $this->wh));
      $element = new Meduse_Form_Element_Select('customer');
      $element->setLabel('Zákazník');
      $element->addMultiOption('', '-- všichni zákazníci --');
      $element->addMultiOptions($customers);
      $this->addElement($element);

      if ($this->wh == Table_TobaccoWarehouse::WH_TYPE) {
        $element = new Meduse_Form_Element_Select('pkg');
        $element->setLabel('Balení tabáku');
        $element->addMultiOptions(array(
          '' => '-- všechna balení --',
          '10'  => '10 gramů',
          '50'  => '50 gramů',
          '100' => '100 gramů',
          '150' => '150 gramů',
          '250' => '250 gramů',
        ));
        $this->addElement($element);

        $tFlavors = new Table_BatchFlavors();
        $element = new Meduse_Form_Element_Select('flavor');
        $element->setLabel('Příchuť tabáku');
        $element->addMultiOption('', '-- všechny příchutě --');
        $element->addMultiOptions($tFlavors->getAssocArray());
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('type');
        $element->setLabel('Typ tabáku');
        $element->addMultiOptions(array(
          'all' => 'vše', 'fs' => 'kolkovaný', 'nfs' => 'nekolkovaný',
        ));
        $this->addElement($element);
      }

      $element = new Meduse_Form_Element_Checkbox('onlypaid');
      $element->setLabel('Pouze zaplacené');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Radio('order');
      $element->setLabel('Řadit dle');
      $element->addMultiOptions(array(
        'name' => 'názvu', 'pcs' => 'počtu', 'weight' => 'váhy', 'price' => 'ceny'
      ));
      $this->addElement($element);

      $this->addElement(new Meduse_Form_Element_Submit('Zobrazit'));

      parent::init();
    }

  }

