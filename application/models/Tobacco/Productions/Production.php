<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 17.12.2018
 * Time: 17:22
 */

class Tobacco_Productions_Production {

  public $id, $producer, $date_ordering, $date_delivery, $status, $parts = array();
  protected $row, $wh = Table_TobaccoWarehouse::WH_TYPE;

  public function __construct($id = NULL) {
    $table = new Table_Productions();
    if ($id) {
      if ($this->row = $table->find($id)->current()) {
        $this->id = $this->row->id;
        $this->producer = new Producers_Producer($this->row->id_producers);
        $this->date_ordering = new Meduse_Date($this->row->date_ordering, 'Y-m-d');
        $this->date_delivery = new Meduse_Date($this->row->date_delivery, 'Y-m-d');
        $this->status = $this->row->status;

        $table = new Table_ProductionsParts();
        $query = $table->select()
          ->setIntegrityCheck(FALSE)
          ->from('productions_parts')
          ->where('id_productions = ?', $this->id)
          ->query(Zend_Db::FETCH_OBJ);
        while ($row = $query->fetch()) {
          $this->parts[$row->id_parts] = array(
            'id' => $row->id,
            'part' => new Tobacco_Parts_Part($row->id_parts),
            'amount' => $row->amount,
            'amount_remain' => $row->amount_remain,
            'delete' => FALSE,
            'inserted' => $this->date_ordering,
            'row' => $table->find($row->id)->current(),
          );
        }
      }
    }
    else {
      $this->status = $table::STATUS_OPENED;
    }
  }

  public function setProducer($producer, $save = FALSE) {
    if ($producer instanceof Producers_Producer) {
      $this->producer = $producer;
    }
    elseif (is_int($producer)) {
      $this->producer = new Producers_Producer($producer);
    }
    else {
      throw new Exception('Výrobce nelze přiřadit.');
    }
    if ($save) {
      $this->save();
    }
    return $this;
  }

  public function setDateOrdering($date, $save = FALSE) {
    if ($date instanceof Meduse_Date) {
      $this->date_ordering = $date;
    }
    elseif (is_string($date)) {
      $this->date_ordering = new Meduse_Date($date, 'Y-m-d');
    }
    else {
      throw new Exception('Datum nelze přiřadit.');
    }
    if ($save) {
      $this->save();
    }
  }

  public function setParts(array $parts, $save = FALSE) {
    if (empty($parts)) {
      throw new Exception('Není zadán žádný materiál.');
    }
    foreach ($parts as $partId => $amount) {
      if ($amount > 0 && !isset($this->parts[$partId])) {
        $this->parts[$partId] = array(
          'id' => NULL,
          'part' => new Tobacco_Parts_Part($partId),
          'amount' => $amount,
          'amount_remain' => $amount,
          'delete' => FALSE,
          'inserted' => $this->date_ordering,
        );
      }
      elseif ($amount > 0) {
        $this->parts[$partId]['amount'] = $amount;
        $this->parts[$partId]['amount_remain'] = $amount;
      }
      else {
        $this->parts[$partId]['delete'] = TRUE;
      }
    }
    if ($save) {
      $this->save();
    }
    return $this;
  }

  public function update(array $values) {
    if (isset($values['id_producer'])) {
      $this->setProducer($values['id_producers']);
    }
    if (isset($values['date_ordering'])) {
      $this->setDateOrdering($values['date_ordering']);
    }
    if (isset($values['parts'])) {
      $this->setParts($values['parts']);
    }
    $this->save();
  }

  public function save() {
    $table = new Table_Productions();
    if (!$this->row) {
      $this->row = $table->createRow();
    }
    $this->row->setFromArray(array(
      'id' => $this->id,
      'id_wh_type' => $this->wh,
      'id_producers' => $this->producer ? $this->producer->getId() : NULL,
      'date_ordering' => $this->date_ordering ? $this->date_ordering->get('Y-m-d') : NULL,
      'date_delivery' => $this->date_delivery ? $this->date_delivery->get('Y-m-d') : NULL,
      'status' => $this->status,
      'inserted' => $this->date_ordering ? $this->date_ordering->get('Y-m-d') : NULL, // TODO Remove.
    ));
    $this->id = $this->row->save();
    $table = new Table_ProductionsParts();
    foreach ($this->parts as $partId => $item) {
      if (!isset($item['row'])) {
        $item['row'] = $table->createRow();
      }
      if (isset($item['delete']) && $item['delete']) {
        $item['row']->delete();
      }
      else {
        $item['row']->setFromArray(
          array(
            'id' => $item['id'],
            'id_productions' => $this->id,
            'id_parts' => $partId,
            'amount' => $item['amount'],
            'amount_remain' => $item['amount_remain'],
            'inserted' => $item['inserted']->get('Y-m-d'),
          )
        );
        $item['row']->save();
      }
    }
    return $this;
  }

  public function confirmDeliveryAll($date_delivery, $description = NULL, $invoice_id = NULL) {
    $message = '';
    $production_status = TRUE;
    $date = $date_delivery->get('Y-m-d');
    $table = new Table_ProductionsDeliveries();
    $row = $table->createRow(array(
      'id_productions' => $this->id,
      'description' => $description,
      'invoice_id' => $invoice_id,
      'delivery_date' => $date,
      'inserted' => $date,
    ));
    $id = $row->save();

    $table = new Table_ProductionsDeliveriesParts();
    foreach ($this->parts as &$item) {
      $row = $table->createRow(array(
        'id_productions_deliveries' => $id,
        'id_productions_parts' => $item['id'],
        'delivery_amount' => $item['amount_remain'],
        'inserted' => $date,
      ));
      $status = Tobacco_Warehouse::addItem($item['part'], $item['amount_remain']);
      if ($status !== FALSE) {
        $message .= $status;
        $row->save();
        $item['amount_remain'] = 0;
      }
      else {
        $production_status = FALSE;
      }
    }

    $this->date_delivery = $date_delivery;
    $this->status = $production_status ? Table_Productions::STATUS_CLOSED : Table_Productions::STATUS_PARTIAL;
    $this->save();
    return $message;
  }

  public function getInvoices() {
    $invoices = array();
    $table = new Table_ProductionsDeliveries();
    $query = $table->select()
      ->from($table, array('invoice_id'))
      ->where('id_productions = ?', $this->id)
      ->query(Zend_Db::FETCH_ASSOC);
    while ($invoice = $query->fetchColumn()) {
      $invoices[] = $invoice;
    }
    return $invoices;
  }

  public function getNotes() {
    $notes = array();
    $table = new Table_ProductionsDeliveries();
    $query = $table->select()
      ->from($table, array('description'))
      ->where('id_productions = ?', $this->id)
      ->query(Zend_Db::FETCH_ASSOC);
    while ($note = $query->fetchColumn()) {
      $notes[] = $note;
    }
    return $notes;
  }
}