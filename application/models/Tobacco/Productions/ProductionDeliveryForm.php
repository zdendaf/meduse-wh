<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 19.12.2018
 * Time: 9:37
 */

class Tobacco_Productions_ProductionDeliveryForm extends Meduse_FormBootstrap {

  public function init() {

    $this->setAction('/tobacco/productions-delivery');
    $this->setAttrib('class', 'form');
    $this->setLegend('Potvrzení přijetí');
    $this->_isModalForm = TRUE;

    $element = new Zend_Form_Element_Hidden('id');
    $this->addElement($element);

    $element = new Meduse_Form_Element_DatePicker('date_delivery');
    $element->setLabel('Datum přijetí');
    $element->setRequired(TRUE);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('description');
    $element->setLabel('Poznámka');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('invoice_id');
    $element->setLabel('Číslo faktury');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Potvrdit');
    $this->addElement($element);

    parent::init();

  }

  public function setProductionId($id) {
    $this->getElement('id')->setValue($id);
  }

  public function getValidValues($data, $suppressArrayNotation = FALSE) {
    $values = parent::getValidValues($data, $suppressArrayNotation);
    $values['id'] = (int) $values['id'];
    $values['date_delivery'] = new Meduse_Date($values['date_delivery'], 'd.m.Y');
    $values['description'] = $values['description'] ? $values['description'] : NULL;
    $values['invoice_id'] = $values['invoice_id'] ? $values['invoice_id'] : NULL;
    return $values;
  }
}