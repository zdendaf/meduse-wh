<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 17.12.2018
 * Time: 17:21
 */

class Tobacco_Productions_ProductionForm extends Meduse_FormBootstrap {

  public function init() {

    $this->addAttribs(array('class' => 'form'));
    $this->setAction('/tobacco/productions-save');

    // 1
    $element = new Zend_Form_Element_Hidden('id');
    $element->setOrder(1003);
    $this->addElement($element);

    // 2
    $element = new Meduse_Form_Element_DatePicker('date_ordering');
    $element->setLabel('Datum objednávky');
    $element->setRequired(TRUE);
    $element->setOrder(1);
    $this->addElement($element);

    // 3
    $element = new Meduse_Form_Element_Select('id_producers');
    $element->setLabel('Dodavatel');
    $element->addMultiOption(NULL, '-- zvol dodavatele --');
    $element->addDecorator(['elementDiv' => 'HtmlTag']);
    $table = new Table_Producers();
    if ($rows = $table->getProducers(Table_TobaccoWarehouse::WH_TYPE)) {
      foreach ($rows as $row) {
        $element->addMultiOption($row->id, $row->name);
      }
    }
    $element->setRequired(TRUE);
    $element->setOrder(2);
    $this->addElement($element);

    // 4
    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Uložit objednávku');
    $element->setOrder(1002);
    $this->addElement($element);

    parent::init();
  }

  public function addParts($id_producers) {
    if ($rows = Parts::findByProducer($id_producers)) {
      $order = 3;
      foreach ($rows as $row) {
        $element = new Meduse_Form_Element_Text('part_' . $row->id);
        $element->setLabel($row->id);
        $desc = $row->name;
        if ($row->description) {
          $desc .= ' – ' . $row->description;
        }
        $element->setDescription($desc);
        $element->setAttrib('placeholder', $row->measure == 'weight' ? 'kg' : 'ks');
        $element->setAttrib('data-measure', $row->measure);
        $element->setAttrib('class', 'part');
        $element->addDecorator(['data' => 'HtmlTag']);
        $element->addDecorator(['elementDiv' => 'HtmlTag']);
        $element->setOrder(++$order);
        $this->addElement($element);
      }
    }
  }

  public function getValidValues($data, $suppressArrayNotation = FALSE) {
    $values = array();
    if ($parents = parent::getValidValues($data, $suppressArrayNotation)) {
      foreach ($parents as $key => $val) {
        if (strpos($key, 'part_') === 0) {
          if (!isset($values['parts'])) {
            $values['parts'] = array();
          }
          $values['parts'][substr($key, 5)] = $val;
        }
        elseif ($key == 'id_producers') {
          $values[$key] = (int) $val;
        }
        else {
          $values[$key] = $val;
        }
      }
    }
    return $values;
  }

  public function isValid($params) {
    $valid = parent::isValid($params);
    $parts = FALSE;
    foreach ($params as $key => $val) {
      if (strpos($key, 'part_') === 0) {
        if (floatval($val) > 0) {
          $parts = TRUE;
          break;
        }
      }
    }
    return $valid && $parts;
  }
}