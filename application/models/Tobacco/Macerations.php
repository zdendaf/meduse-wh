<?php


class Tobacco_Macerations {


  public static function getSnapshotsYears() {
    $table = new Table_MacerationsSnapshot();
    $select = $table->select()->distinct(TRUE);
    $select->from(['s' => 'macerations_snapshot'], [
      'year' => new Zend_Db_Expr('YEAR(s.date)')
    ]);
    $select->order('date DESC');
    $years = [];
    $result = $select->query();
    while($year = $result->fetchColumn()) {
      $years[] = $year;
    }
    return $years;
  }

  public static function getSnapshots(Tobacco_Macerations_Maceration $maceration = NULL, Meduse_Date $date = NULL, Meduse_Date $to = NULL, $sort = 'DESC') {
    $tSnap = new Table_MacerationsSnapshot();
    $select = $tSnap->select()->setIntegrityCheck(FALSE);
    $select->from(['ms' => 'macerations_snapshot']);
    $select->join(['m' => 'macerations'], 'm.id = ms.id_macerations', ['name', 'batch', 'id_recipes']);
    $select->join(['r' => 'recipes'], 'r.id = m.id_recipes', ['recipe' => 'name']);
    $select->order('ms.date ' . $sort);
    if ($maceration) {
      $select->where('ms.id_macerations = ?', $maceration->getId());
    }
    if ($date && !$to) {
      $select->where('DATE(ms.date) = ?', $date->toString('Y-m-d'));
    }
    elseif ($date && $to) {
      $select->where('? <= DATE(ms.date)', $date->toString('Y-m-d'));
      $select->where('DATE(ms.date) <= ?', $to->toString('Y-m-d'));
    }
    $result = $select->query()->fetchAll();
    return $result;
  }

  public static function getSnapshot($id) {
    $tSnap = new Table_MacerationsSnapshot();
    return $tSnap->find($id)->current()->toArray();
  }

  public static function fixSnapshot($id) {
    $tSnap = new Table_MacerationsSnapshot();
    $snap = self::getSnapshot($id);
    if ($diff = $snap['amount_wh'] - $snap['amount_real']) {
      $maceration = new Tobacco_Macerations_Maceration($snap['id_macerations']);
      $id_macerations_history = NULL;
      $maceration->check($snap['amount_real'], $snap['date'], $snap['note'], $id_macerations_history);
      $tSnap->update([
        'is_fixed' => 'y',
        'date' => $snap['date'],
        'id_macerations_history' => (int) $id_macerations_history,
      ], 'id = ' . $id);
    }
  }

  public static function fixSnapshots(array $ids = []) {
    if ($ids) {
      foreach ($ids as $id) {
        self::fixSnapshot($id);
      }
    }
  }

  public static function getMacerationsByRecipes($recipeIds) {
    if (!is_array($recipeIds)) {
      $recipeIds = [$recipeIds];
    }
    $tMacerations = new Table_Macerations();
    return $tMacerations->getBarrels($recipeIds, TRUE);
  }

  public static function getMacerationsWeight(Meduse_Date $date = NULL, $recipeIds = NULL) {
    $weight = 0.0;
    if (!is_null($recipeIds) && !is_array($recipeIds)) {
      $recipeIds = [$recipeIds];
    }
    if (is_null($date)) {
      $date = new Meduse_Date();
    }
    if ($maceration = self::getMacerationsByRecipes($recipeIds)) {
     foreach ($maceration as $item) {
       $barrel = new Tobacco_Macerations_Maceration($item['id']);
       $d = $date->get('Y-m-d');
       $weight += $barrel->getWeight($date);
     }
    }
    return $weight;
  }

  public static function getChanges($barrelsIds, Meduse_Date $from = NULL, Meduse_Date $to = NULL, $sum = FALSE, $partIds = []) {
    if (!$from) {
      $from = new Meduse_Date(date('Y-01-01'), 'Y-m-d');
    }
    if (!$to) {
      $to = clone $from;
      $to->addYear(1);
    }
    $table = new Table_MacerationsHistory();
    $changes = $table->getChanges($barrelsIds, $from->get('Y-m-d H:i:s'), $to->get('Y-m-d H:i:s'), $sum, $partIds);
    return $changes;
  }

  public static function getPartsHistoryInfo($partId = NULL, Meduse_Date $date = NULL) {
    $table = new Table_MacerationsPartsWarehouseHistory();
    return $table->getPartsHistoryInfo($partId, $date);
  }

  public static function getClosingReport($year) {
    $report = [];
    $closing_date = new Meduse_Date($year . '-12-31', 'Y-m-d');
    $table = new Table_Macerations();
    $data = $table->getClosingReport($year);

    foreach ($data as $item) {
      $real_date = !is_null($item['amount_real_date']) ? new Meduse_Date($item['amount_real_date'], 'Y-m-d H:i:s') : FALSE;
      $diff_abs = $item['amount_real'] - $item['amount'];
      $diff = ($item['remove_total'] > 0 && !is_null($item['amount_real'])) ? $diff_abs / $item['remove_total'] : FALSE;
      if (($diff !== FALSE && ($diff <= -0.05 || $diff > 0)) || ($real_date && $real_date->get('ym') < $closing_date->get('ym'))) {
        $class = 'alert-danger';
      }
      elseif ($diff !== FALSE && (-0.05 < $diff && $diff <= 0)) {
        $class = 'alert-success';
      }
      else {
        $class = '';
      }

      $report[$item['id']] = [
        'name' => $item['name'],
        'batch' => $item['batch'],
        'start_amount' => sprintf('%01.3f', $item['snap_start']),
        'amount_add' => sprintf('%01.3f', $item['amount_add']),
        'amount_remove' => sprintf('%01.3f', $item['amount_remove']),
        'changes' => sprintf('%01.3f', $item['amount_add'] - $item['amount_remove']),
        'remove_total' => sprintf('%01.3f', $item['remove_total']),
        'end_amount' => sprintf('%01.3f', $item['amount']),
        'snap_id' => $item['snap_id'],
        'amount_real' => !is_null($item['amount_real']) ? sprintf('%01.3f', $item['amount_real']) : FALSE,
        'amount_real_date' => $real_date,
        'diff_abs' => $diff_abs,
        'diff' => $diff !== FALSE ? sprintf('%01.2f %%', $diff * 100) : '—',
        'class' => 'maceration_row' . ($class ? ' ' . $class : ''),
      ];
    }
      return $report;
  }

  public static function closeYear($year) {
    $tMacerations = new Table_Macerations();
    if ($data = $tMacerations->getClosingReport($year)) {

      $tSnap = new Table_MacerationsSnapshot();
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $date = ($year + 1) . '-01-01 00:00:00';

      foreach ($data as $item) {

        // kdyz je odchylka, tak vyrovnat
        if (!is_null($item['snap_id']) && $item['amount'] != $item['amount_real']) {
          self::fixSnapshot($item['snap_id']);
        }

        // zapsat stav k prvnimu
        $amount = is_null($item['amount_real']) ? $item['amount'] : $item['amount_real'];
        $tSnap->setSnapshot(
          $item['id'],
          $amount,
          $amount,
          $date,
          $authNamespace->user_id,
          'Počáteční stav k 01.01.' . ($year + 1),
          TRUE
        );
      }
    }
    else {
      throw new Exception("No data for closing year.");
    }
  }

  public static function getMacerationXLSX() {

    $tMacaretions = new Table_Macerations();
    $barrels = $tMacaretions->getBarrels(NULL, FALSE, TRUE);

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator('Medite - Warehouse');
    $objPHPExcel->getProperties()->setLastModifiedBy('Medite - Warehouse');
    $objPHPExcel->getProperties()->setTitle('EXPORT SKLADU MACERACE K DATU ' . date('d.m.Y'));
    $objPHPExcel->getProperties()->setSubject('EXPORT SKLADU MACERACE K DATU ' . date('d.m.Y'));
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setTitle('Simple');

    $total_weight = 0;
    $total_price = 0;

    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(0, 1, 'EXPORT SKLADU MACERACE K DATU '
        . date('d.m.Y') . ' (celkový stav)');
    $row = 3;

    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow($col = 0, ++$row, "Šarže")
      ->setCellValueByColumnAndRow(++$col, $row, "Příchuť")
      ->setCellValueByColumnAndRow(++$col, $row, "Jedn. cena")
      ->setCellValueByColumnAndRow(++$col, $row, "Akt. váha")
      ->setCellValueByColumnAndRow(++$col, $row, "Celk. cena")
      ->setCellValueByColumnAndRow(++$col, $row, "Název")
      ->setCellValueByColumnAndRow(++$col, $row, "Založeno")
      ->setCellValueByColumnAndRow(++$col, $row, "Poč. váha");

    foreach ($barrels as $barrel) {
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow($col = 0, ++$row, $barrel['batch'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['recipe_name'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['unit'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['weight'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['barrel_current'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['name'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['start'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['amount_in']);
      $total_weight += $barrel['weight'];
      $total_price += $barrel['barrel_current'];
    }

    $row += 2;
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(0, $row, "CELKEM: ");
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(3, $row, $total_weight);
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(4, $row, $total_price);


    $objPHPExcel->getActiveSheet()->getStyle('C1:C' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('D1:D' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.000');
    $objPHPExcel->getActiveSheet()->getStyle('E1:E' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('H1:H' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.000');

    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle('D' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.000');
    $objPHPExcel->getActiveSheet()->getStyle('E' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.00');
    return $objPHPExcel;
  }
}