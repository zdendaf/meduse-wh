<?php

class Tobacco_Mixes_RecipesForm extends Meduse_FormSkove {
	public function init() {
		$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			->setMethod(Zend_Form::METHOD_POST);

		$element = new Meduse_Form_Element_Select('recipe_id');
		$element->setLabel('Recept');
		$table = new Table_Recipes();

		$recipes = $table->getRecipesPairs();
    $element->addMultiOptions($recipes);
		$this->addElement($element);

		$element = new Meduse_Form_Element_Float('ratio');
		$element->setRequired(true);
		$element->setLabel('Množství');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Přidat');
		$this->addElement($element);

		parent::init();
	}
}