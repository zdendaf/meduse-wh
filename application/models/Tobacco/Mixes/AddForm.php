<?php

  class Tobacco_Mixes_AddForm extends Meduse_FormBootstrap {

    function init() {
      
      $this->setLegend('Vytvoření nového mixu');
      $this->setMethod(Zend_Form::METHOD_POST);
      
      $element = new Meduse_Form_Element_Text('name');
      $element->setRequired(true);
      $element->setLabel('Název');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('flavor_id');
      $element->setLabel('Příchuť');

      $table = new Table_BatchFlavors();
      $element->addMultiOptions($table->getAssocArray());
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('Odeslat');
      $this->addElement($element);

      parent::init();
    }
  }
