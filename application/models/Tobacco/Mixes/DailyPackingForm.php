<?php


class Tobacco_Mixes_DailyPackingForm extends Zend_Form {

  public const ALLOWED_PACKING = [0.01, 0.05, 0.25];

  protected Table_Mixes $table;
  public array $parts = [];
  public array $barrels = [];

  public function __construct($options = NULL) {
    $this->table = new Table_Mixes();
    parent::__construct($options);
  }


  /**
   * @throws Zend_Form_Exception
   * @throws Exception
   */
  public function init(): void
  {

    $this->addAttribs(['class' => 'form', 'id' => 'packing-form']);

    $this->prepareParts();
    $this->prepareBarrels();

    $element = new Meduse_Form_Element_DatePicker('date');
    $element->setLabel('Datum nabalení');
    $element->setValue(date('d.m.Y'));
    $element->setRequired();
    $element->setDecorators([
      'UiWidgetElement',
      'Description',
      'Errors',
      ['Label', ['style' => 'font-weight: bold;']],
    ]);
    $this->addElement($element);

    foreach ($this->parts as $id_mixes => $mix) {
      if (isset($this->barrels[$id_mixes])) {
        $fit = 0;
        foreach ($this->barrels[$id_mixes]['flavors'] as $flavor) {
          $element = new Meduse_Form_Element_Radio('batch_' . $id_mixes . '_' . $fit);
          $element->setAttrib('tabindex', '-1');
          $element->setAttrib('data-ratio', $flavor['ratio']);
          $element->setLabel($flavor['name_flavor']);
          $mid = 0;
          foreach ($flavor['barrels'] as $id_macerations => $barrel) {
            $element->addMultiOption($id_macerations, $barrel['batch'] . ' (' . $barrel['amount'] . ' kg)');
            if ($mid === 0) {
              $element->setValue($id_macerations);
            }
            $mid++;
          }
          $fit++;
          $element->setDecorators([
              'ViewHelper',
              'Description',
              'Errors',
              [
                ['data2' => 'HtmlTag'],
                ['tag' => 'div', 'class' => 'form_element_radio']
              ],
              ['Label', ['tag' => 'div']],
              [
                ['elementDiv' => 'HtmlTag'],
                ['tag' => 'div', 'class' => 'flavor_item']
              ]
            ]);
          $this->addElement($element);
        }
      }
      foreach ($mix['parts'] as $parts) {
        foreach ($parts as $partId => $part) {
          $element = new Meduse_Form_Element_Text('amount_' . $id_mixes . '_' . $partId);
          $element->setAttrib('data-amount', $part['amount']);
          $element->setDecorators([
            'ViewHelper',
            'Description',
            'Errors',
            [
              ['data' => 'HtmlTag'],
              ['tag' => 'div', 'class' => 'text-right'],
            ],
            [['elementDiv' => 'HtmlTag'], ['tag' => 'div', 'class' => 'amount amount']],
          ]);
          $this->addElement($element);
        }
      }
    }

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Zadat nabalení');
    $element->setAttribs(['class' => 'btn btn-big btn-primary']);
    $element->setDecorators([
      'ViewHelper',
      'Description',
      'Errors',
    ]);
    $this->addElement($element);

    parent::init();
  }

  public function render(Zend_View_Interface $view = NULL): string
  {

    if (null !== $view) {
      $this->setView($view);
    }

    $packingCount = count(self::ALLOWED_PACKING);

    $html =
      '<style>
        #packing-form { padding-bottom: 50px; }
        #packing-form .flavor_item { border-bottom: 1px solid #dddddd; }
        #packing-form .flavor_item:last-child { border-bottom: none; }
        #packing-form .flavor_item div { display: inline-block; }
        #packing-form .flavor_item label.optional { width: 180px; overflow: hidden; text-align: left; }
        #packing-form .flavor_item input[type=radio] { width: auto; margin: 0 5px 0 0; }
        #packing-form .flavor_item .form_element_radio { vertical-align: top; white-space: nowrap; };
        #packing-form .flavor_item .form_element_radio label { width: auto; }
        #packing-form .amount input { width: 40px; text-align: right; }
        #packing-form .footer div { display: inline-block; }
      </style>';

    $html .= '<form id="' . $this->getAttrib('id') . '" class="' . $this->getAttrib('class') . '" method="';
    $html .= $this->getMethod() . '" action="' . $this->getAction() . '">';
    $html .= '<table class="table table-condensed sticky">';
    $html .= '<thead><tr><th rowspan="2">název mixu</th><th rowspan="2" class="text-center">složky / č. šarže</th>';
    $html .= '<th colspan="' . $packingCount .'" class="text-center">počet</th></tr>';
    $html .= '<tr>';

    foreach (self::ALLOWED_PACKING as $value) {
      $value *= 1000;
      $html .= "<th class='text-right'>$value g</th>";
    }
    $html .= '</tr></thead><tbody>';

    foreach ($this->parts as $id_mixes => $mix) {

      if (isset($this->barrels[$id_mixes])) {
        $html .= '<tr><th>' . $mix['name_mixes'].  '</th><td class="flavors">';
        $fit = 0;
        foreach ($this->barrels[$id_mixes]['flavors'] as $ignored) {
          $element = $this->getElement('batch_' . $id_mixes . '_' . $fit);
          $html .= $element;
          $fit++;
        }
        foreach (self::ALLOWED_PACKING as $p) {
          $idx = sprintf('%0.3f', $p);
          $html .= '<td class="text-right"><div style="display: flex; justify-content: space-around">';
          if (isset($mix['parts'][$idx])) {
            foreach ($mix['parts'][$idx] as $partId => $part) {
              $element = $this->getElement('amount_' . $id_mixes . '_' . $partId);
              $html .= '<div class="item-wrapper">' . $element;
              $html .= '<small><a tabindex="-1" href="/tobacco/parts-detail/id/' . $partId . '" target="_blank">'
                . $partId . '</a></small></div>';
            }
          }
          $html .= '</div></td>';
        }
        $html .= '</td></tr>';
      }

    }
    $html .= '</tbody></table>';

    $html .= '<div class="footer">';
    $html .= $this->getElement('save');
    $html .= '</div></form>';

    return $html;
  }


  /**
   * @throws Exception
   */
  private function prepareParts(): void
  {
    if ($data = $this->table->getPartsForPacking(self::ALLOWED_PACKING)) {
      foreach ($data as $item) {
        if (!isset($this->parts[$item['id_mixes']])) {
          $this->parts[$item['id_mixes']] = [
            'id_mixes' => $item['id_mixes'],
            'name_mixes' => $item['name_mixes'],
            'parts' => [],
          ];
        }
        if (!isset($this->parts[$item['id_mixes']]['parts'][$item['amount']])) {
          $this->parts[$item['id_mixes']]['parts'][$item['amount']] = [];
        }
        $this->parts[$item['id_mixes']]['parts'][$item['amount']][$item['id_parts']] = [
          'amount' => $item['amount'],
          'id_parts' => $item['id_parts'],
        ];
      }
    }
    else {
      throw new RuntimeException('No parts were found.');
    }
  }

  private function prepareBarrels(): void
  {
    if ($data = $this->table->getBarrelsForPacking()) {
      foreach ($data as $item) {
        if (!isset($this->barrels[$item['id_mixes']])) {
          $this->barrels[$item['id_mixes']] = [
            'id_mixes' => $item['id_mixes'],
            'name_mixes' => $item['name_mixes'],
            'flavors' => [],
          ];
        }
        if (!isset($this->barrels[$item['id_mixes']]['flavors'][$item['id_flavor']])) {
          $this->barrels[$item['id_mixes']]['flavors'][$item['id_flavor']] = [
            'id_flavor' => $item['id_flavor'],
            'name_flavor' => $item['name_flavor'],
            'ratio' => $item['ratio'],
            'barrels' => [],
          ];
        }
        $this->barrels[$item['id_mixes']]['flavors'][$item['id_flavor']]['barrels'][$item['id_macerations']] = [
          'id_macerations' => $item['id_macerations'],
          'batch' => $item['batch'],
          'amount' => $item['amount_current'],
        ];
      }
    }
    else {
      throw new RuntimeException('No barrels were found.');
    }
  }
}