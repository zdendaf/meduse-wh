<?php

class Tobacco_Mixes_EditForm extends Meduse_FormSkove {
	public function init() {
		$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			->setMethod(Zend_Form::METHOD_POST);

		$element = new Meduse_Form_Element_Text('name');
		$element->setRequired(true);
		$element->setLabel('Název');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('id_flavor');
    $element->setLabel('Příchuť');
    $table = new Table_BatchFlavors();
    $element->addMultiOptions($table->getAssocArray());
    $this->addElement($element);

		$element = new Meduse_Form_Element_Textarea('description');
		$element->setLabel('Poznámka');
		$element->setAttrib("cols", 25);
		$element->setAttrib("rows", 6);
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Select('state');
		$options = Table_Mixes::$state_names;
		unset($options[Table_Mixes::STATE_HIDDEN]);
		$element->addMultiOptions($options);
		$element->setLabel('Stav');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);

		parent::init();
	}
}