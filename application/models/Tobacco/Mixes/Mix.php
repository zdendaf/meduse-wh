<?php

class Tobacco_Mixes_Mix {

  static public $state_names = array(
    'active' => 'Aktivní',
    'inactive' => 'Neaktivní',
    'hidden' => 'N/A'
  );

  protected $_id;
  protected $_name;
  protected $_id_flavor;
  protected $_description;
  protected $_created;
  protected $_updated;
  protected $_state;
  protected $_flavor_name;
  protected $_ingredients = array();


  public function __construct($id) {
    $this->init($id);
  }

  public function toArray() {
    return array(
      'name' => $this->getName(),
      'id_flavor' => $this->getFlavorId(),
      'description' => $this->getDescription(),
      'created' => $this->getCreated(),
      'updated' => $this->getUpdated(),
      'state' => $this->getState()
    );
  }

  public function init($id) {
    $table = new Table_Mixes();
    $select = $table->select()->setIntegrityCheck(false);
    $select->from($table)->where('id = ?', $id);
    $result = $select->query()->fetch();
    if ($result) {
      $this->_id = $result['id'];
      $this->_name = $result['name'];
      $this->_description = $result['description'];
      $this->_id_flavor = $result['id_flavor'];
      $this->_created = $result['create'];
      $this->_updated = $result['update'];
      $this->_state = $result['state'];
      $this->getIngredients(false);
    }
  }

  public function getId() {
    return $this->_id;
  }

  public function setId($id) {
    $this->_id = $id;
    return $this;
  }

  public function getName() {
    return $this->_name;

  }
  public function setName($name) {
    $this->_name = $name;
    return $this;
  }

  public function getFlavorId() {
    return $this->_id_flavor;
  }

  public function setFlavorId($flavor_id) {
    $this->_id_flavor = $flavor_id;
    return $this;
  }

  public function getFlavorName() {
    if (empty($this->_flavor_name)) {
      $table = new Table_BatchFlavors();
      $this->_flavor_name = $table->getName($this->getFlavorId());
    }
    return $this->_flavor_name;
  }

  public function getDescription() {
    return $this->_description;
  }

  public function setDescription($description) {
    $this->_description = $description;
    return $this;
  }

  public function getCreated() {
    return $this->_created;
  }

  public function setCreated($created) {
    $this->_created = $created;
    return $this;
  }

  public function getUpdated() {
    return $this->_updated; }

  public function setUpdated($updated) {
    $this->_updated = $updated;
    return $this;
  }

  public function getState() {
    return $this->_state;
  }

  public function setState($state) {
    $this->_state = $state;
    return $this;
  }

  public function getStateString() {
    return self::$state_names[$this->_state];
  }

  /**
   * @param bool $return
   * @deprecated
   * @return array|null
   */
  public function getIngredients($return = TRUE) {
    return $this->getRecipes($return);
  }

  public function getRecipes($return = TRUE) {
    $table = new Table_MixesRecipes();
    $select = $table->select()->setIntegrityCheck(false);
    $select->from(array('tr' => 'mixes_recipes'), array('id_recipes', 'ratio'));
    $select->joinLeft(array('r' => 'recipes'), 'r.id = tr.id_recipes', array('name'));
    $select->where('id_mixes = ?', $this->_id);
    $result = $select->query()->fetchAll(Zend_Db::FETCH_ASSOC);
    if ($result) {
      foreach ($result as $row) {
        $this->_ingredients[$row['id_recipes']] = array (
          'id' => $row['id_recipes'],
          'name' => $row['name'],
          'ratio' => (int) $row['ratio'],
        );
      }
    }
    if ($return) {
      return $result;
    }
    return NULL;
  }

  public function getMacerations() {
    $recipeIds = [];
    if ($recipes = $this->getRecipes()) {
      foreach ($recipes as $item) {
        $recipeIds[] = $item['id_recipes'];
      }
    }
    $macerations = Tobacco_Macerations::getMacerationsByRecipes($recipeIds);
    return $macerations;
  }

  public function getRelatedParts($asObject = FALSE) {
    $tMixes = new Table_Mixes();
    $parts = $tMixes->getRelatedParts($this->getId());
    if ($asObject) {
      $relatedParts = [];
      if ($parts) {
        foreach ($parts as $item) {
          $relatedParts[] = new Tobacco_Parts_Part($item['id']);
        }
      }
      return $relatedParts;
    }
    else {
      return $parts;
    }
  }

  public function getUnitPrice() {
    $table = new Table_MixesRecipes();
    $row = $table->getUnitPrice($this->getId());
    return $row['unit_price'];
  }

}
