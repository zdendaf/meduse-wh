<?php
class Tobacco_Customers_Form extends Meduse_FormBootstrap {

  public function init() {


    $this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
     ->setMethod(Zend_Form::METHOD_POST);

		$element = new Zend_Form_Element_Hidden('id_wh_type');
    $element->setValue(2);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('customer_company');
		$element->setLabel('Název firmy');
    $element->setRequired(true);
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('customer_first_name');
		$element->setLabel('Jméno');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('customer_last_name');
		$element->setLabel('Příjmení');
		$this->addElement($element);

    // Pole pouze pro noveho zakaznika -----------------------------------------
    $element = new Meduse_Form_Element_Text('address_street');
    $element->setLabel('Ulice');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('address_pop_number');
    $element->setLabel('Číslo popisné');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('address_orient_number');
    $element->setLabel('Číslo orientační');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('address_city');
    $element->setLabel('Město');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('address_zip');
    $element->setLabel('PSČ');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Select('address_country');
    $db = Zend_Registry::get('db');
    $rows = $db->fetchPairs('SELECT code, CONCAT(name, " (", code, ")" ) FROM c_country ORDER BY name');
    $element->addMultiOptions($rows);
    $element->setLabel('Země');
    $element->setValue('CZ');
    $this->addElement($element);
    // -------------------------------------------------------------------------

		$element = new Meduse_Form_Element_Phone('customer_phone');
		$element->setLabel('Telefon');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('customer_email');
		$element->setLabel('e-mail');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('customer_ident_no');
		$element->setLabel('ID No');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('customer_vat_no');
		$element->setLabel('Vat No');
		$this->addElement($element);

    $element = new Meduse_Form_Element_Select('customer_vat_checked');
    $element->setLabel('Ověření VAT');
    $element->addMultiOptions(array(
      '' => 'neověřeno',
      'n' => 'nevalidní',
      'y' => 'validní',
    ));

    $this->addElement($element);
		$element = new Meduse_Form_Element_Text('customer_seed_id');
		$element->setLabel('Seed ID');
    $element->addValidator(new Zend_Validate_StringLength(0, 13));
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('customer_type');
		$element->setLabel('Typ zákazníka');
    $element->setRequired(true);
                $element->addMultiOption(null, 'Nezadán');
		$element->addMultiOption('B2B', 'B2B');
		$element->addMultiOption('B2C', 'B2C');
		$this->addElement($element);

    //ceníky - budou se zobrazovat dle vybraného typu zákazníka
    $tPricelists = new Table_Pricelists();

    //ceníky B2B
    $element = new Meduse_Form_Element_Select('pricelist_B2B');
    $element->setLabel('Výběr ceníku');
    $element->addMultiOption(NULL, '-- bez ceníku --');
    foreach ($tPricelists->getPricelistsByType(Table_TobaccoWarehouse::WH_TYPE, Table_Pricelists::TYPE_B2B)->query()->fetchAll()
            as $option) {
      $element->addMultiOption($option['id'], $option['name']);
      if ($option['default']=='y') {$element->setValue($option['id']);}
    }
    $this->addElement($element);

    //ceníky B2C
    $element = new Meduse_Form_Element_Select('pricelist_B2C');
    $element->setLabel('Výběr ceníku');
    $element->addMultiOption(NULL, '-- bez ceníku --');
    foreach ($tPricelists->getPricelistsByType(Table_TobaccoWarehouse::WH_TYPE, Table_Pricelists::TYPE_B2C)->query()->fetchAll()
            as $option) {
      $element->addMultiOption($option['id'], $option['name']);
      if ($option['default']=='y') {$element->setValue($option['id']);}
    }
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('allow_invoicing');
    $element->setLabel('Povolit fakturaci');
    if (!Zend_Registry::get('acl')->isAllowed('tobacco-customers/set-allow-invoicing')) {
      $element->setAttrib('readonly', 'readonly');
    }
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('due');
    $element->setLabel('Splatnost [dny]');
    $element->addValidator(new Zend_Validate_Int());
    $element->addValidator(new Zend_Validate_GreaterThan(-1));
    $element->addValidator(new Zend_Validate_LessThan(91));
    $this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);

    parent::init();
	}

	public function populate(array $values) {
		$this->customer_company->setValue($values['company']);
		$this->customer_first_name->setValue($values['first_name']);
		$this->customer_last_name->setValue($values['last_name']);
		$this->customer_phone->setValue($values['phone']);
		$this->customer_email->setValue($values['email']);
    $this->customer_ident_no->setValue($values['ident_no']);
		$this->customer_vat_no->setValue($values['vat_no']);
    $this->customer_vat_checked->setValue($values['vat_checked']);
    $this->customer_seed_id->setValue($values['seed_id']);
		$this->customer_type->setValue($values['type']);
    $this->allow_invoicing->setValue($values['allow_invoicing']);
    $this->due->setValue($values['due']);

    $tPricelists = new Table_Pricelists();
    $val = $tPricelists->getCustomersValidPricelist($values['id']);
    if (!is_null($val)) {
      if ($values['type'] == Table_Customers::TYPE_B2B) {
        $this->pricelist_B2B->setValue($val);
      } else {
        $this->pricelist_B2C->setValue($val);
      }
    }
	}

}
