<?php

class Tobacco_Producers_Form extends Meduse_FormBootstrap
{

    public function init()
    {
        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('post');

        $element = new Zend_Form_Element_Checkbox('is_active');
        $element->setLabel('Status');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('name');
        $element->setRequired(true);
        $element->addValidator(new Meduse_Validate_NotEmpty());
        $element->setLabel('Jméno firmy');
        $this->addElement($element);

        /*
        $element = new Meduse_Form_Element_SelectChosen('meduse_producer_warehouse');
        $element->addMultiOption(NULL, '- subjekt není zpracovatelem -');
        $element->addMultiOptions(Zend_Registry::get('db')->fetchPairs('SELECT id,name FROM extern_warehouses e WHERE NOT EXISTS (SELECT * FROM producers WHERE id_extern_warehouses = e.id)'));
        $element->setLabel('Externí sklad');
        $this->addElement($element);

        $element = new Meduse_Form_Element_SelectChosen('meduse_producer_category');
        $element->setRequired(true);
        $element->addMultiOption(NULL, '-- vybrat --');
        $element->addMultiOptions(Zend_Registry::get('db')->fetchPairs('SELECT id,name FROM producers_ctg ORDER BY name'));
        $element->setLabel('Kategorie');
        $this->addElement($element);
        */
        $element = new Zend_Form_Element_Text('address');
        $element->setLabel('Adresa');
        $this->addElement($element);


        $element = new Zend_Form_Element_Text('www');
        $element->setLabel('www');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('description');
        $element->setLabel('Poznámka k firmě');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('activity');
        $element->setLabel('Činnost firmy');
        $this->addElement($element);

        $element = new Zend_Form_Element_Submit('save');
        $element->setLabel('Uložit');
        $this->addElement($element);

        parent::init();
    }

    public function populate(array $values) {
      if (isset($values['is_active'])) {
        $values['is_active'] = $values['is_active'] == 'y';
      }
      parent::populate($values);
    }

}
