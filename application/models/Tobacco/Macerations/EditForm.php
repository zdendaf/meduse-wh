<?php

  class Tobacco_Macerations_EditForm extends Meduse_FormBootstrap {

    public function init() {

      $this->setLegend('Editace macerace');
      $this->setMethod(Zend_Form::METHOD_POST);
      $this->setAction('/tobacco/macerations-save');

      $element = new Zend_Form_Element_Hidden('id');
      $this->addElement($element);

      $element = new Zend_Form_Element_Hidden('recipe');
      $this->addElement($element);

      $element = new Zend_Form_Element_Hidden('created');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('total');
      $element->setAttrib('readonly', 'readonly');
      $element->setLabel('Celková hmotnost');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Checkbox('ratio');
      $element->setLabel('Přepočítávat podle receptu');
      $element->setValue(true);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('name');
      $element->setLabel('Název');
      $element->setRequired(true);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Textarea('description');
      $element->setAttrib('rows', '3');
      $element->setLabel('Popis macerace');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('amount_critical');
      $element->setLabel('Limitní množství [kg]');
      $this->addElement($element);


      $element = new Meduse_Form_Element_Submit('Uložit');
      $element->setOrder(999);
      $this->addElement($element);

      parent::init();
    }

    public function setItems($items) {
      if (is_array($items) && count($items) > 0) {
        $count = 0;
        $totalWeight = 0;

        foreach ($items as $item) {

         if (empty($item['id_parts'])) {
           continue;
         }

         $element = new Zend_Form_Element_Hidden('item_name_' . $count);
         $element->setOrder(700 + $count);
         $this->addElement($element);

         $element = new Zend_Form_Element_Hidden('item_id_' . $count);
         $element->setValue(/*$item['id']*/$item['id_parts']);
         $element->setOrder(800 + $count);
         $this->addElement($element);

         $element = new Meduse_Form_Element_Text('item_weight_' . $count);
         $element->setLabel($item['id_parts'] . ' – ' . $item['name'] . ' [kg]:');
         $element->setValue($item['amount']);
         $element->setAttrib('class', 'weight');
         $this->addElement($element);

         $count++;
         $totalWeight += $item['amount'];
        }

        $this->getElement('total')->setValue($totalWeight);
      } else {
        $this->addElement('hidden', 'plaintext', array(
          'description' => '<p class="text-error">Receptura neobsahuje žádné ingredience!</p>',
          'ignore' => true,
          'decorators' => array(
            array('Description', array('escape'=>false, 'tag'=>'')),
          ),
        ));
      }
    }

    public function setId($id) {
      $this->getElement('id')->setValue($id);
    }

    public function setRecipeId($recipeId) {
      $this->getElement('recipe')->setValue($recipeId);
    }

    public function setWeight($weight) {
      $this->getElement('total')->setValue($weight);
    }

    public function setCreated($created) {
      $this->getElement('created')->setValue($created);
    }

    public function setCritical($crit) {
      $this->getElement('amount_critical')->setValue($crit);
    }
  }
