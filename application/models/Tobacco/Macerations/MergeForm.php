<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Tobacco_Macerations_MergeForm extends Meduse_FormBootstrap {
    
    protected $batch = null;
    
    public function __construct($options) {
      if (isset($options['batch'])) {
        $this->batch = $options['batch'];
      }
      parent::__construct($options);
    }
    
    public function init() {
      if (is_null($this->batch)) {
        throw new Tobacco_Macerations_Exceptions_NoBatchForMergeException();
      }      
      
      $maceration = new Tobacco_Macerations_Maceration($this->batch);
      
      $this->setAttribs(array('class' => 'form'));
      $this->setLegend('Macerát ' . $this->batch . ' (' . $maceration->getName() . ')');
      
      $element = new Zend_Form_Element_Hidden('merge_from');
      $element->setValue($this->batch);
      $this->addElement($element);
      
      $tMacerations = new Table_Macerations();
      $barrels = $tMacerations->getBarrels($maceration->getRecipeId(), TRUE);
      
      if ($barrels) {
        $element = new Meduse_Form_Element_Select('merge_to');
        $element->setLabel('Vmíchat do sudu');
        foreach ($barrels as $idx => $barrel) {
          if ($barrel['batch'] != $this->batch) {
            $element->addMultiOption($barrel['batch'], $barrel['batch'] . ' (' . $barrel['name'] . ')');
          }
        }
        $this->addElement($element);
      }
      
      $maxAmount = $maceration->getWeight();
      $element = new Meduse_Form_Element_Float('merge_amount');
      $element->setLabel('Vmíchávané množství');
      $element->setValue($maxAmount);
      $element->addValidator(new Zend_Validate_Between(0, $maxAmount));
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Submit('Vmíchat');
      $this->addElement($element);
      
      parent::init();
    }
  }

  