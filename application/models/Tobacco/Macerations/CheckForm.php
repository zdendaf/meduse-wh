<?php

  /**
   * Formular pro kontrolu maceratu
   *
   * @author Zdenek
   */
  class Tobacco_Macerations_CheckForm extends Zend_Form {

    public function init() {

      $this->setAttrib('class', 'form');
      $this->setAction('/tobacco/macerations-check');

      $element = new Zend_Form_Element_Hidden('batch');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Float('amount');
      $element->setLabel('Aktuální hmotnost');
      $this->addElement($element);

      parent::init();
    }
  }
