<?php

  /**
   * Description of PackForm
   *
   * @author Zdenek
   */
  class Tobacco_Macerations_PackForm extends Meduse_FormBootstrap {
    
    public function init(){
            
      $this->setAttrib('class', 'form');
      $this->setAttrib('id', 'pack');
      $this->setAction('/tobacco/macerations-pack-post');
      $this->setLegend('Nabalení macerovaného tabáku');
      
      $element = new Meduse_Form_Element_Text('batch');
      $element->setLabel('Šarže');
      $element->setRequired(true);
      $element->setAttrib('readonly', 'readonly');
      $element->setOrder(100);
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Text('max_amount');
      $element->setLabel('Max. množství');
      $element->setRequired(true);
      $element->setAttrib('readonly', 'readonly');
      $element->setOrder(110);
      $this->addElement($element);
     
      $element = new Meduse_Form_Element_Text('remain');
      $element->setLabel('Zbývá');
      $element->setRequired(true);
      $element->setAttrib('readonly', 'readonly');
      $element->setOrder(120);
      $element->addValidator(new Zend_Validate_GreaterThan(0));
      $this->addElement($element);

      $element = new Zend_Form_Element_Button('Přepočti');
      $element->setAttrib('class', 'btn btn-default');
      $element->setOrder(900);
      $this->addElement($element);

      
      $element = new Meduse_Form_Element_Submit('Nabal');
      $element->setOrder(910);
      $this->addElement($element);
      
      parent::init();
    }
    
    protected function setParts($parts) {
      if (empty($parts)) { 
        throw new Exception('Neexistují žádné produkty s receptem daného macerátu.');
      }
      
      $it = 0;
      foreach ($parts as $part) {
        
        $element = new Zend_Form_Element_Hidden('name_' . $it);
        $element->setAttrib('class', 'name');
        $element->setValue($part['id_parts']);
        $element->setOrder(500 + ($it * 3));
        $this->addElement($element);        

        $element = new Zend_Form_Element_Hidden('weight_' . $it);
        $element->setAttrib('class', 'weight');
        $element->setValue((float)$part['amount']);
        $element->setOrder(501 + ($it * 3));
        $this->addElement($element);          
        
        $element = new Zend_Form_Element_Text('part_' . $it);
        $element->setLabel($part['name'] . " [" . $part['amount'] . "kg]:");
        $element->setAttrib('size', '5');
        $element->setAttrib('class', 'amount');
        $element->setValue(0);
        $element->setOrder(502 + ($it * 3));
        $this->addElement($element);
        $it++;
      }
    }
    
    public function setMaceration($batch) {
      
      $maceration = new Tobacco_Macerations_Maceration($batch);
      
      $this->getElement('batch')->setValue($maceration->__get('batch'));
      $this->getElement('max_amount')->setValue($maceration->__get('weight'));
      $this->getElement('remain')->setValue($maceration->__get('weight'));
        
      $tPartsRecipes = new Table_PartsRecipes();
      $parts = $tPartsRecipes->getParts($maceration->__get('recipe'));
      $this->setParts($parts);
    }
  
    public function getParts($params) {
      $parts = array();
      foreach ($params as $key => $param) {
        if (substr($key, 0, 4) == 'name') {
          $idx = substr($key, 5);
          $id = (string) $params['name_' . $idx];
          $amount = (int) $params['part_' . $idx];
          $weight = (float) $params['weight_' . $idx];
          $parts[] = array(
            'id_parts' => $id,
            'amount' => $amount,
            'weight' => $weight,
            'total' =>  $weight * $amount,
            'batch' => $params['batch']
          );
        }
      }
      return $parts;
    }
    
    
  }
