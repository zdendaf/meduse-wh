<?php

  /**
   * Description of CancelForm
   *
   * @author Zdenek
   */
  class Tobacco_Macerations_CancelForm extends Zend_Form {

    public function init() {
      $this->setAttrib('class', 'form');
      $this->setAction('/tobacco/macerations-storno');

      $element = new Meduse_Form_Element_DatePicker('date');
      $element->setLabel('Datum');
      $element->setRequired();
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('batch_storno');
      $element->setLabel('Šarže');
      $element->setAttrib('readonly', 'readonly');
      $element->setRequired();
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('amount_storno');
      $element->setLabel('Zb. hmotnost');
      $element->setAttrib('readonly', 'readonly');
      $element->setRequired();
      $this->addElement($element);

      $element = new Meduse_Form_Element_Textarea('reason');
      $element->setLabel('Důvod');
      $element->setAttrib('rows', '3');
      $element->setRequired();
      $this->addElement($element);

      parent::init();
    }
  }
