<?php


/**
 * Class Tobacco_Macerations_Fixer
 */
class Tobacco_Macerations_Fixer {

  const AMOUNT_DIFF_LIMIT = 0.5;

  protected $recipe, $startDate, $endDate, $startAmount, $endAmount;
  protected $batchList = [];
  protected $messages = [];
  protected $checkStatus, $fixStatus;
  protected $barrels = [];
  protected $startDiff, $endDiff;
  protected $shifts = [];

  /**
   * Tobacco_Macerations_Fixer constructor.
   *
   * @param \Tobacco_Recipes_Recipe $recipe
   *  Receptura, pro kterou se maji fixovat stavy macerace.
   * @param \Meduse_Date $startDate
   *  Datum zacatku obdobi fixace.
   * @param float $startAmount
   *  Pozadovana vaha maceratu na zacatku obdobi.
   * @param \Meduse_Date $endDate
   *  Datum konce obdobi fixace.
   * @param float $endAmount
   *  Pozadovana vaha maceratu na konci obdobi.
   *
   *
   */
  public function __construct(Tobacco_Recipes_Recipe $recipe, Meduse_Date $startDate, float $startAmount, Meduse_Date $endDate, float $endAmount) {
    $this->recipe = $recipe;
    $this->startDate = $startDate;
    $this->startAmount = $startAmount;
    $this->endDate = $endDate;
    $this->endAmount = $endAmount;
    $this->init();
  }

  /**
   * @param array $batchList
   *  Seznam zalozenych sudu a jejich hmotnosti na vstupu.
   *  Klic je cislo sarze a hodnotou je hmotnost (float).
   */
  public function setBatchList(array $batchList) {
    $this->batchList = $batchList;
  }

  public function init() {
    if ($barrels = Tobacco_Macerations::getMacerationsByRecipes($this->recipe->getId())) {
      foreach ($barrels as $barrel) {
        $maceration = new Tobacco_Macerations_Maceration($barrel['id']);
        if ($changes = $maceration->getChanges($this->startDate, $this->endDate)) {
          $this->barrels[] = [
            'id' => $maceration->getId(),
            'batch' => $batch = $maceration->getBatchNumber(),
            'maceration' => $maceration,
            'changes' => $changes,
          ];
        }
      }
      $this->checkStatus = TRUE;
    }
    else {
      $this->addMessage('Nebyly nalezeny zadne sudy.');
      $this->checkStatus = FALSE;
    }
    return $this;
  }


  /**
   * @return array
   */
  public function getMessages(): array {
    return $this->messages;
  }


  public function check() {
    $name = $this->recipe->getName();
    $id = $this->recipe->getId();
    $flavorId = $this->recipe->getFlavorId();
    $flavor = $this->recipe->getFlavorName();
    $this->addMessage("<br>Zacina kontrola maceratu receptury #$id $name<br>Prichut: #$flavorId - $flavor");
    $this->checkStatus &= TRUE;
    $this->testMacerationExist();
    $status = $this->checkStatus ? 'PASS' : 'FAIL';
    $this->addMessage("<br>Kontrola ukoncena, status: $status.");
    return $this;
  }

  public function fix() {
    $this->check();

    if ($this->checkStatus) {
      $name = $this->recipe->getName();
      $id = $this->recipe->getId();
      $flavorId = $this->recipe->getFlavorId();
      $flavor = $this->recipe->getFlavorName();
      $this->fixStatus = TRUE;
      $this->addMessage("<br>Zacina fixace maceratu receptury #$id $name<br>Prichut: #$flavorId - $flavor");
      $this->addMessage("Pocatecni datum: {$this->startDate->get('d.m.Y')}, pozadovana vaha: " . sprintf('%1.3f kg', $this->startAmount));
      $this->addMessage("Koncove datum: {$this->endDate->get('d.m.Y')}, pozadovana vaha: " . sprintf('%1.3f kg', $this->endAmount));

      $this->calculateDiffs();
      $this->findShifts();

      $status = $this->fixStatus ? 'PASS' : 'FAIL';
      $this->addMessage("<br>Fixace ukoncena, status: $status.");
    }
    return $this;
  }


  protected function addMessage(string $message) {
    $this->messages[] = $message;
    return $this;
  }

  protected function testMacerationExist() {
    $this->addMessage("<br>Kontrola vstupu vyroby:");
    if (!$this->batchList) {
      $this->addMessage('Nebyla zadana vyroba.');
    }
    elseif ($this->barrels) {
      foreach ($this->barrels as $barrel) {
        $batch = $barrel['batch'];
        if (!key_exists($batch, $this->batchList)) {
          $this->addMessage("Sud $batch nebyl nalezen v seznamu vyroby.");
          $this->checkStatus = FALSE;
        }
        else {
          /** @var \Tobacco_Macerations_Maceration $maceration  */
          $maceration = $barrel['maceration'];
          $amount = $this->batchList[$batch];
          $diff = $amount - $maceration->getAmountIn();
          if (abs($diff) > self::AMOUNT_DIFF_LIMIT) {
            $this->addMessage("Sud $batch ma jine vstupni mnozstvi, nez bylo uvedeno v seznamu vyroby. WH rozdil je $diff kg.");
            $this->checkStatus = FALSE;
          }
          else {
            $diff = sprintf("%+0.3f", $diff);
            $amount = sprintf("%0.3f", $amount);
            $this->addMessage("Sud $batch nalezen, hmotnost na vstupu: $amount kg, WH rozdil je $diff kg.");
          }
        }
      }
    }
    return $this;
  }

  protected function calculateDiffs() {
    $startAmount = 0;
    $endAmount = 0;

    foreach ($this->barrels as $barrel) {
      /** @var \Tobacco_Macerations_Maceration $maceration */
      $maceration = $barrel['maceration'];
      try {
        $startAmount += $maceration->getWeight($this->startDate);
        $endAmount += $maceration->getWeight($this->endDate);
      }
      catch (Zend_Date_Exception $e) {
        $this->addMessage($e->getMessage());
        $this->fixStatus = FALSE;
        return $this;
      }
    }
    $this->startDiff = $this->startAmount - $startAmount;
    $this->endDiff = $this->endAmount - $endAmount;

    $this->addMessage("Celkem maceratu ke dni {$this->startDate->get('d.m.Y')}: $startAmount kg, rozdil " . sprintf('%+01.3f kg', $this->startDiff));
    $this->addMessage("Celkem maceratu ke dni {$this->endDate->get('d.m.Y')}: $endAmount kg, rozdil " . sprintf('%+01.3f kg', $this->endDiff));

    return $this;
  }

  /**
   * Nalezeni moznych zmen k presunu.
   * Od druheho sudu prochazim pres sudy. Vyberu cil, zjistim max. mozneho množství a datum zalozeni
   * Prochazim predchozi sudy - zdroje, ukladam si mnozstvi zmen.
   * Cil:
   * - neni prvni A (ma nenulovou vahu NEBO byl vmíchán)
   * - maximum vaha + množství vmíchání
   * Zdroj:
   * - neni poslední
   * - mnozsvi soucet zmen, ktere jsou dva dny po zalozeni cile
   */
  protected function findShifts() {
    $cnt = count($this->barrels);
    for ($i = 0; $i < $cnt; $i++) {
      if ($max = $this->findMaximum($this->barrels[$i])) {
        /** @var \Tobacco_Macerations_Maceration $maceration */
        $maceration = $this->barrels[$i]['maceration'];
        $ready = $maceration->getStart(TRUE)->addDay(2)->get('Y-m-d 00:00:00');
        $this->shifts[$i] = ['max' => $max, 'sources' =>[]];
        for ($j = 0; $j < $i; $j++) {
          $this->shifts[$i]['sources'][$j] = ['sum' => 0, 'changes' => []];
          foreach ($this->barrels[$j]['changes'] as $change) {
            if (in_array($change['type'], ['pack', 'break']) && $change['date'] > $ready) {
              $new = $this->shifts[$i]['sources'][$j]['sum'] + $change['amount'];
              if ($new > $max) {
                $this->shifts[$i]['sources'][$j]['changes'] = $change;
                $this->shifts[$i]['sources'][$j]['sum'] = $new;
              }
              else {
                break;
              }
            }
          }

        }
      }
    }

  return $this;
  }

  private function findMaximum($barrel) {
    /** @var \Tobacco_Macerations_Maceration $maceration */
    $maceration = $barrel['maceration'];
    $weight = $maceration->getWeight();
    $changes = 0;
    foreach ($barrel['changes'] as $change) {
      if (in_array($change['type'], ['merge_to', 'check'])) {
        $changes += $change['amount'];
      }
    }
    return $weight + $changes;
  }

}