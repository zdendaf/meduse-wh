<?php

  class Tobacco_Macerations_Form extends Meduse_FormBootstrap {

    function init() {

      $this->setLegend('Založení nové macerace');
      $this->setMethod(Zend_Form::METHOD_POST);
      $this->setAction('/tobacco/macerations-edit');

      $element = new Meduse_Form_Element_Select('recipe');
      $element->setLabel('Zvol požadovanou recepturu');
      $element->addMultiOption(NULL, '- zvol -');
      $element->setRequired(true);
      $tRecipes = new Table_Recipes();
      $options = $tRecipes->getRecipesPairs();
      $element->addMultiOptions($options);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Float('weight');
      $element->setLabel('Požadované množství [kg]');
      $element->setRequired(true);
      $element->addValidator(new Zend_Validate_GreaterThan(0));
      $this->addElement($element);

      $element = new Meduse_Form_Element_DatePicker('created',
        array('jQueryParams' => array(
          'dateFormat' => 'dd.mm.yy'
        )
      ));
      $element->setLabel('Datum vytvoření');
      $element->setValue(date('d.m.Y'));
      $element->setRequired(true);
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Float('amount_critical');
      $element->setLabel('Limitní množství [kg]');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('Pokračovat');
      $this->addElement($element);

      parent::init();
    }
  }
