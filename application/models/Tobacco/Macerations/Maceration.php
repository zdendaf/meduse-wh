<?php


  class Tobacco_Macerations_Maceration {

    protected $_id;
    protected $_batch;
    protected $_name;
    protected $_recipe;
    protected $_description;
    protected $_amount_in;
    protected $_amount_critical;
    protected $_weight;
    protected $_start;
    protected $_end;
    protected $_checked;
    protected $_discarded;
    protected $_ingredients = array();

    protected $_recipe_ptr;

    const ChangeBatchPatternDate = '2018-06-15';
    const SnapshotBreak = '2019-01-01';

    const PRICE_TYPE_UNIT = 'unit';
    const PRICE_TYPE_BARREL_IN = 'barrel_in';
    const PRICE_TYPE_BARREL_CURRENT = 'barrel_current';

    public function __construct($identifier = null) {
      if (!is_null($identifier)) {
        if (is_numeric($identifier)) {
          $this->init($identifier);
        } else {
          $this->setBatchNumber($identifier);
          $this->init();
        }
      }
    }

    public function setId($id) {
      $this->_id = $id;
      return $this;
    }

    public function getId() {
      return $this->_id;
    }

    public function getRecipeId() {
      return $this->_recipe;
    }

    public function getRecipe() {
      if (empty($this->_recipe_ptr)) {
        $this->_recipe_ptr = new Tobacco_Recipes_Recipe($this->getRecipeId());
      }
      return $this->_recipe_ptr;
    }

    public function getDescription() {
      return $this->_description;
    }

    public function getAmountIn() {
      return $this->_amount_in;
    }

    public function setName($name) {
      $this->_name = $name;
      return $this;
    }
    public function getName() {
      return $this->_name;
    }

    public function setAmountIn($weight) {
      $this->_amount_in = $weight;
      return $this;
    }

    public function setWeight($weight) {
      $this->_weight = $weight;
      return $this;
    }

    /**
     * Vrací množství macerátu na skladě.
     * Pokud je zadáno datum, vrací množství k tomuto datu.
     *
     * @param \Meduse_Date|NULL $date
     * @throws Zend_Date_Exception
     *
     * @return int
     *
     * Tato funkce musi byt upravena. Vezme se nejblizsi inventarni stav
     * a z nej se dopocita stav ke pozadovanemu dni.
     */
    public function getWeight(Meduse_Date $date = NULL) {
      // Bez datumu se ptame na soucasne mnozstvi.
      $weight = $this->_weight;
      if (is_null($date)) {
        return $weight;
      }
      // Pokud chceme mnoztvi pred okamzikem zalozeni, vratime NULL.
      if ($date < $this->getStart(TRUE)) {
        return NULL;
      }
      $to = new Meduse_Date();
      if ($snaps = $this->getSnapshots($date, $to, 'ASC')) {
        // Pokud najdeme inventurni stav, zmeny pocitame od tohoto okamziku.
        $snap = array_shift($snaps);
        $to = new Meduse_Date($snap['date'], 'Y-m-d H:i:s');
        $weight = $snap['amount_wh'];
      }
      $changes = $this->getChanges($date, $to, TRUE);
      return $weight - $changes['amount'];
    }

    public function getWeight2(Meduse_Date $date = NULL) {
      // dulezite je datum. od 01.01.2019 mame pocatecni stavy

      $weight = NULL;

      // Pokud chceme mnoztvi pred okamzikem zalozeni, vratime NULL.
      if ($date < $this->getStart(TRUE)) {
        return $weight;
      }

      $weight = $this->_weight;

      // Bez datumu se ptame na soucasne mnozstvi.
      if (is_null($date)) {
        return $weight;
      }


      // debug
      $d1 = $date->toString('Y-m-d');


      // dulezite je datum
      // pokud se dotazujeme na stav pred 2019-01-01 od vahu zmeny odecitame
      // pokud se dotazujeme na stav po 2019-01-01, k vaze zmeny pricitame (urcite existuje snap)
      $break = new Meduse_Date(self::SnapshotBreak, 'Y-m-d');

      if ($date < $break) {
        // najdeme nejblisti vyssi snaphot a odecitame
        if ($snaps = $this->getSnapshots($date, $break, 'ASC')) {
          // Pokud najdeme inventurni stav, zmeny pocitame od tohoto okamziku zpet
          $snap = array_shift($snaps);
          $to = new Meduse_Date($snap['date'], 'Y-m-d H:i:s');
          $d2 = $to->toString('Y-m-d');
          $weight = $snap['amount_wh'];
          $changes = $this->getChanges($date, $to, TRUE);
          $weight -= $changes['amount'];
        }
      }
      else {
        // najdeme nejblisti nizsi snaphot a pricitame
        if ($snaps = $this->getSnapshots($break, $date, 'DESC')) {
          // Pokud najdeme inventurni stav, zmeny pocitame od tohoto okamziku dal.
          $snap = array_shift($snaps);
          $from = new Meduse_Date($snap['date'], 'Y-m-d H:i:s');
          $d2 = $from->toString('Y-m-d');
          $weight = $snap['amount_wh'];
          $changes = $this->getChanges($from, $date, TRUE);
          $weight += (float) $changes['amount'];
        }
      }
      return $weight;
    }

    public function getStart($asObject = FALSE) {
      $date = NULL;
      if ($this->_start) {
        $date = new Meduse_Date($this->_start);
      }
      if (is_null($date)) {
        return NULL;
      }
      else {
        return $asObject ? $date : $date->get('d.m.Y');
      }
    }

    public function getEnd($asObject = FALSE) {
      $date = NULL;
      if ($this->_end) {
        $date = new Meduse_Date($this->_end);
      }
      if (is_null($date)) {
        return NULL;
      }
      else {
        return $asObject ? $date : $date->get('d.m.Y');
      }
    }

    public function getLastCheck($asObject = FALSE) {
      $date = NULL;
      if ($this->_checked) {
        $date = new Meduse_Date($this->_checked);
      }
      if (is_null($date)) {
        return NULL;
      }
      else {
        return $asObject ? $date : $date->get('d.m.Y');
      }
    }

    public function __get($name) {
      return $this->{'_' . $name};
    }

    public function init($id = null) {
      $tMacerations = new Table_Macerations();
      $select = $tMacerations->select()->setIntegrityCheck(false);
      $select->from($tMacerations);
      if (is_null($id)) {
        $select->where('batch = ?', $this->_batch);
      } else {
        $select->where('id = ?', $id);
      }
      $result = $select->query()->fetch();
      if ($result) {
        $this->_id = $result['id'];
        $this->_batch = $result['batch'];
        $this->_name = $result['name'];
        $this->_recipe = $result['id_recipes'];
        $this->_description = $result['description'];
        $this->_amount_in = $result['amount_in'];
        $this->_amount_critical = $result['amount_critical'];
        $this->_weight = $result['amount_current'];
        $this->_start = $result['start'];
        $this->_end = $result['end'];
        $this->_checked = $result['checked'];
        $this->_discarded = $result['discarded'] === 'y';
        $this->getIngredients(false);
      }
    }


    public function addIngredient($ingredientId, $weight) {
      $tWarehouse = new Table_TobaccoWarehouse();
      $disp = $tWarehouse->getAmount($ingredientId) * 0.001;
      $part = new Parts_Part($ingredientId);

      if ($disp < $weight) {
        throw new Exception(
          'Na skladě není dostatečné množství suroviny "<a href="/tobacco/parts-detail/id/' . $ingredientId . '">'
          . $ingredientId . ' ' . $part->getName() . '</a>" (maximum je ' . $disp . ' kg).'
        );
      }
      $this->_ingredients[$ingredientId] = $weight;
    }

    public function getIngredients($return = true) {
      $tMacerationsParts = new Table_MacerationsParts();
      $select = $tMacerationsParts->select()->setIntegrityCheck(false);
      $select->from(array('mp' => 'macerations_parts'))->where('id_macerations = ?', $this->_id);
      $select->joinLeft(array('p' => 'parts'), 'p.id = mp.id_parts', array('name'));
      $result = $select->query()->fetchAll(Zend_Db::FETCH_ASSOC);
      if ($result) {
        foreach ($result as $row) {
          $this->_ingredients[$row['id_parts']] = (float) $row['amount'];
        }
      }
      if ($return) {
        return $result;
      }
    }

    public function setBatchNumber($batchNumber, $check = false) {
      if ($check) {
        $this->checkBatchNumber($batchNumber);
      }
      $this->_batch = $batchNumber;
    }

    public function getBatchNumber() {
      return $this->_batch;
    }

    public function checkBatchNumber($batchNumber) {
      if (!Tobacco_Batches_Batch::isFreeBatch($batchNumber)) {
        throw new Exception('Macerace se šarží "' . $batchNumber . '" již existuje.');
      }
    }

    public function isFreeBatchNumber($batch) {
      return Tobacco_Batches_Batch::isFreeBatch($batch);
    }


    public function setRecipe($recipeId) {
      $this->_recipe = $recipeId;
    }

    public function setCritical($value) {
      $this->_amount_critical = $value;
    }

    public function getCritical() {
      return $this->_amount_critical;
    }

    public function storno($reason, $amount = null, $date = null) {
      $diff = is_null($amount)? $this->_weight : $amount;
      $when = is_null($date)? date('Y-m-d') : $date;
      $this->_weight -= $diff;
      $tMaceration = new Table_Macerations();
      $tMaceration->update(['amount_current' => $this->_weight, 'discarded' => 'y'], 'id = ' . $this->_id);
      $this->history(Table_MacerationsHistory::TYPE_DESTROY, -$diff, 'Storno: ' . $reason, $when);
    }

    public function history($type, $amount, $description, $when = null) {
      $tHistory = new Table_MacerationsHistory();
      if (!$tHistory->checkType($type)) {
        throw new Exception('Neplatný typ změny.');
      }
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $data = array(
        'id_macerations' => $this->_id,
        'id_users' => $authNamespace->user_id,
        'amount' => $amount,
        'type' => $type,
        'description' => $description,
      );
      if (!is_null($when)) {
        $data['last_change'] = $when;
      }
      return $tHistory->insert($data);
    }

    public function setDescription($description) {
      $this->_description = $description;
      return $this;
    }

    public function update(array $items = array()) {
      $success = TRUE;
      if ($items) {
        $success = $this->updateIngredients($items);
      }
      if ($success) {
        $table = new Table_Macerations();
        $data = array(
          'name' => $this->getName(),
          'description' => $this->getDescription(),
          'amount_critical' => $this->getCritical(),
          'amount_in' => $this->getAmountIn(),
          'amount_current' => $this->getWeight(),
        );
        $where = $table->getAdapter()->quoteInto('id=?', $this->getId());
        try {
          $table->update($data, $where);
          return TRUE;
        }
        catch (Exception $e) {
          return FALSE;
        }
      }
      else {
        return FALSE;
      }
    }

    public function updateIngredients(array $ingred_new) {
      $success = TRUE;
      $ingred_old = $this->getIngredients();
      $diff = [];
      foreach ($ingred_old as $item) {
        $diff[$item['id_parts']] = ['old' => (float) $item['amount'], 'new' => 0, 'diff' => 0];
      }
      foreach ($ingred_new as $item) {
        if (!isset($diff[$item['id']])) {
          $diff[$item['id_parts']] = ['old' => 0];
        }
        $diff[$item['id']]['new'] = (float) $item['weight'];
        $diff[$item['id']]['diff'] = $diff[$item['id']]['old'] - $diff[$item['id']]['new'];
      }

      $tmp = new Table_MacerationsParts();
      $db = $tmp->getAdapter();
      $tWarehouse = new Table_TobaccoWarehouse();
      $db->beginTransaction();
      try {
        foreach ($diff as $part_id => $item) {
          // zmena na sklade
          // kladna hodnota naskladujeme zpet, zaporna vyskladnujeme
          if ($item['diff'] > 0) {
            $tWarehouse->addPart($part_id, $item['diff'] * 1000);
          }
          else {
            $tWarehouse->removePart($part_id, $item['diff'] * -1000);
          }
          // zmena v maceraci
          $tmp->update(['amount' => $item['new']],
            'id_macerations = ' . $this->getId() . ' AND id_parts = "' . $part_id . '"'
          );
        }
        $db->commit();
      }
      catch (Exception $e) {
        $success = FALSE;
        $db->rollBack();
      }
      return $success;
    }

    public function create() {
      if (!$this->_batch) {
        throw new Exception('Macerace musí mít přiřazenou šarži');
      }
      if (!$this->_ingredients) {
        throw new Exception('Macerace musí mít přiřazeny ingredience');
      }
      $tMacerations = new Table_Macerations();
      $tMacerations->getAdapter()->beginTransaction();
      $total = 0;
      foreach ($this->_ingredients as $id => $amount) {
        $total += $amount;
      }
      $tRecipe = new Table_Recipes();
      $recipe = $tRecipe->find($this->_recipe)->toArray();

      $startDate = new Meduse_Date();
      if ($this->_start) {
        $startDate->set($this->_start);
      } else {
        $startDate->set('now');
      }

      $endDate = new Meduse_Date($startDate);
      $endDate->addDay($recipe[0]['due']);

      $description = 'Založení sudu ' . $this->getBatchNumber();

      try {
        // zapis macerace do tabulky
        $data = array(
          'id_recipes' => $this->_recipe,
          'description' => $this->_description,
          'name' => $this->_name,
          'batch' => $this->_batch,
          'amount_in' => $total,
          'amount_current' => $total,
          'amount_critical' => $this->_amount_critical,
          'start' => $startDate->toString('Y-m-d'), //new Zend_Db_Expr('now()'),
          'end' => $endDate->toString('Y-m-d'), //new Zend_Db_Expr('DATE_ADD(NOW(), INTERVAL ' . $recipe[0]['due'] . ' DAY)'),
        );

        $macerationId = $tMacerations->insert($data);
        $this->_id = $macerationId;

        $id_history = $this->history(Table_MacerationsHistory::TYPE_CREATE, $total, $description, $startDate->toString('Y-m-d H:i:s'));
        $this->setSnapshot($total, $startDate, $total, $description, $id_history);

        // vyskladneni a naskladneni surovin
        $tWarehouse = new Table_TobaccoWarehouse();
        $tMacerParts = new Table_MacerationsParts();
        $critical = array(); // kritické množství surovin
        $tMPWH = new Table_MacerationsPartsWarehouseHistory();

        foreach($this->_ingredients as $id => $amount) {
          $tWarehouse->removePart($id, $amount * 1000, FALSE, $startDate->toString('Y-m-d'), $description, $id_history);
          $tMPWH->insert(['id_macerations' => $macerationId, 'id_parts_warehouse_history' => $id_history]);
          $crit = $tWarehouse->checkCritical($id);
          if ($crit) {
            $critical[] = $crit;
          }
          $tMacerParts->insert(array(
            'id_macerations' => $macerationId,
            'id_parts' => $id,
            'amount' => $amount,
          ));
        }
        $tMacerations->getAdapter()->commit();
      } catch (Exception $e) {

        $tMacerations->getAdapter()->rollBack();
        exit;
      }

      return $critical;
    }

    public function check($amount, $date = NULL, $description = NULL, &$history_id = NULL) {

      $tMacerations = new Table_Macerations();
      $db = $tMacerations->getAdapter();


      // Pokud nezadame datum, kontrola se provede k tomuto okamziku.
      if (is_null($date)) {
        $date = date('Y-m-d H:i:s');
      }

      // Zjistime rozdil namerene vahy a vahy k danemu datu maceratu.
      $diff = $amount - $this->getWeight(new Meduse_Date($date, 'Y-m-d H:i:s'));

      // Pokud je datum vetsi nez datum posledni kontroly maceratu,
      // nastavime toto datum,
      // zmenime aktualni hmotnost maceratu o vypocitany rozdil...
      // a aktualizujeme data v tabulce macerartu.
      if ($date > $this->_checked) {
        $this->_checked = $date;
        $this->_weight += $diff;
        if ($this->_weight < 0) {
          $this->_weight = 0;
        }
        $tMacerations->update(array(
          'checked' => $date,
          'amount_current' => (float) $this->_weight,
        ), 'id = ' . $db->quote($this->_id));
      }

      // Otiskneme tuto kontrolu do historie macerace.
      if (!$description) {
        $description = 'Kontrola: zváženo ' . $amount . ' kg';
      }
      $history_id = $this->history(Table_MacerationsHistory::TYPE_CHECK, $diff, $description, $date);

      // Zjistime, jestli neni maceratu podlimitni mnozstvi.
      $data = $tMacerations->find($this->_id)->current()->toArray();
      return ($data['amount_critical'] >= $data['amount_current']);
    }

    public function pack($part, $amount, $weight) {
      $when = date('Y-m-d');
      $diff = $amount * $weight;
      $this->_weight -= $diff;

      $tTW = new Table_TobaccoWarehouse();
      $tMaceration = new Table_Macerations();
      $partId = $part->getID();

      $tTW->addPart($partId, $amount);
      $tMaceration->update(array(
        'amount_current' => $this->_weight
        ), 'id = ' . $this->_id);

      $tPB = new Table_PartsBatch();

      $select = $tPB->select()->setIntegrityCheck(false);
      $select->from($tPB);
      $select->where('id_parts = ?', $part->getID());
      $select->where('batch = ?', $this->getBatchNumber());
      $result = $select->query()->fetch(Zend_Db::FETCH_ASSOC);

      if ($result) {
        $tPB->update(array('amount' => (int)$result['amount'] + $amount),
          'id = ' . $result['id']);
      } else {
        $tPB->insert(array(
          'id_parts' => $part->getID(),
          'batch' => $this->getBatchNumber(),
          'amount' => $amount,
        ));
      }

      $this->history(Table_MacerationsHistory::TYPE_PACK, -$diff, 'Packing: ' . $partId . ' ('
        . $amount . ' x ' . $weight . 'kg)');
    }

    public function setStart($date) {
      $this->_start = $date;
    }

    /**
     * Vmichani casti maceratu do jineho sudu.
     * @param Tobacco_Macerations_Maceration $maceration cilovy sud s maceratem
     * @param float $weight mnoztvi ubiraneho maceratu
     */
    public function mergeTo(Tobacco_Macerations_Maceration $maceration, $weight = null) {

      // kolik objemu sudu vmichame do jineho
      if (is_null($weight)) {
        $weight = (float) $this->getWeight();
      }

      /** Tento proces byl odstranen na zaklade ukolu #262.

      // pomer mezi vmichavanym obsahem a pocatecni vahou
      $ratio = $weight / $this->getAmountIn();

      // prochazime ingredience a dle pomeru menime mnoztvi v cilovem sudu
      $ingrFrom = $this->getIngredients();
      $ingrTo = $maceration->getIngredients();
      $tmp = new Table_MacerationsParts();
      foreach ($ingrFrom as $if) {
        $update = FALSE;
        foreach ($ingrTo as $it) {
          if ($if['id_parts'] == $it['id_parts']) {
            $id = $it['id'];
            $amount = $it['amount'];
            // $id_macerations = $it['id_macerations'];
            $update = TRUE;
            break;
          }
        }

        // ingredience v cilovem sudu je
        if ($update) {
          $tmp->update(array('amount' => $amount + $if['amount'] * $ratio), 'id = ' . $id);
        }

        // ingredience v cilovem sudu neni
        else {
          $tmp->insert(array(
            'amount' => $if['amount'],
            'id_macerations' => $maceration->getId(),
            'id_parts' => $if['id_parts'] * $ratio,
            )
          );
        }
      }
      */

      // zmenime celkovou vahu obou sudu a zapiseme historii
      $wf = (float) $this->getWeight();
      $wt = (float) $maceration->getWeight();
      $this->setWeight(($wf < $weight) ? 0 : $wf - $weight)->update();
      $this->history(Table_MacerationsHistory::TYPE_MERGE_TO, ($wf < $weight) ? -$wf : -$weight, 'Vmícháno do sudu ' . $maceration->getBatchNumber());
      $maceration->setWeight(($wf < $weight) ? $wt + $wf : $wt + $weight)->update();
      $maceration->history(Table_MacerationsHistory::TYPE_MERGE_FROM, ($wf < $weight) ? $wf : $weight, 'Přimícháno ze sudu ' . $this->getBatchNumber());
    }

    public static function isOldBatch($batch) {
      // Stara cisla sarzi jsou ve formatu L123456A78 (s pismenem treti pozici od konce),
      // pouze posledni 2 cisla oznacuji prichut. Nova cisla sarzi jsou ve formatu L123456789
      // (bez pismene na treti pozici od konce), posledni 3 cisla oznacuji prichut.
      return preg_match('/L\d{6}[A-Z]\d{2}/', $batch) === 1;
    }

    public static function isFreeBatch($batch) {
      $tMaceration = new Table_Macerations();
      $select = $tMaceration->select()->setIntegrityCheck(false);
      $select->from($tMaceration, array('batch'));
      if (self::isOldBatch($batch)) {
        $batchForSelect = preg_replace('/(L\d{6})[A-Z](\d{2})/', '$1_$2', $batch);
        $condition = "batch LIKE '$batchForSelect'";
      }
      else {
        $condition = "batch LIKE '$batch'";
      }
      $select->where($condition);
      if ($select->query()->fetch()) {
        return FALSE;
      }
      else {
        return TRUE;
      }
    }

    public static function generateBatch(Meduse_Date $date, $idFlavor) {
      $breakTime = new Meduse_Date(self::ChangeBatchPatternDate);
      if ($breakTime > $date) {
        // stary format
        return 'L' . $date->toString('ymd') . 'A' . str_pad($idFlavor, 2, '0', STR_PAD_LEFT);
      }
      else {
        // novy format
        return 'L' . $date->toString('ymd') . str_pad($idFlavor, 3, '0', STR_PAD_LEFT);
      }
    }

    public function getChanges(Meduse_Date $from = NULL, Meduse_Date $to = NULL, $sum = FALSE, $partIds = []) {
      if (!$from) {
        $from = new Meduse_Date(date('Y-01-01'), 'Y-m-d');
      }
      if (!$to) {
        $to = clone $from;
        $to->addYear(1)->addSecond(-1);
      }
      $table = new Table_MacerationsHistory();
      $changes = $table->getChanges($this->getId(), $from->get('Y-m-d H:i:s'), $to->get('Y-m-d H:i:s'), $sum, $partIds);
      return $changes;
    }

    private function unify(array $changes) {
      // Kdyz nejsou zmeny, hned pryc.
      if (!$changes) {
        return $changes;
      }

      // Prochazej postupne zaznamy.
      // Jde o packing?
      // - ANO: Je neco v bufferu?
      //        - ANO: Sedi id a datum?
      //               - ANO: Pridej do bufferu a pokracuj.
      //               - NE:  Vypis buffer, pridej do bufferu, poznac si ID a datum a pokracuj.
      //        - NE:  Pridej do bufferu, poznac si ID a datum.
      // - NE:  Vypis buffer, vypis zaznam a pokracuj


      // Unifikovane pole zmen.
      $uni = [];
      // posledni part ID a datum
      $date_last = NULL;
      $part_last = NULL;
      $amount_last = NULL;
      // buffer
      $buffer = [];

      foreach ($changes as $change) {

        // Jde o packing.
        if ($change['type'] == Table_MacerationsHistory::TYPE_PACK) {
          $matches = [];
          preg_match('/Packing: (\w{8}) \((\d+) ks.+\)/', $change['description'], $matches);
          $part = $matches[1];
          $amount = $matches[2];
          $date = $change['date'];

          // Je neco v bufferu a sedi datum i cas
          if ($buffer && $part == $part_last && $date == $date_last && $amount == $amount_last)  {
            $buffer[] = $change;
            continue;
          }
          else {
            // Nesedi to, vyprazdnime buffer.
            if ($buffer) {
              $uni[] = $this->unify_buffer($buffer);
            }
            $buffer[] = $change;
            $part_last = $part;
            $amount_last = $amount;
            $date_last = $date;
          }
        }
        // Nejde o packing.
        else {
          if ($buffer) {
            $uni[] = $this->unify_buffer($buffer);
          }
          $uni[] = $change;
        }
      }
      return $uni;
    }


    private function unify_buffer(array &$buffer) {
      $weight = 0.0;
      while ($item = array_shift($buffer)) {
        $uni = $item;
        $matches = [];
        preg_match('/Packing: (\w{8}) \((\d+) ks.+\)/', $item['description'], $matches);
        $weight += (float) $item['amount'];
      }
      $uni['amount'] = $weight;
      $uni['description'] = "Packing: $matches[1] ($matches[2] ks ~ " . abs($weight) . "kg)";
      return $uni;
    }


    public function setSnapshot($amount_real, Meduse_Date $date = NULL, $amount_wh = NULL, $note = NULL, $id_macerations_history = NULL, $id_snapshot = NULL) {
      if (!$date) {
        $date = new Meduse_Date();
      }
      if (!$amount_wh) {
        $amount_wh = $this->getWeight($date);
      }
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $tSnap = new Table_MacerationsSnapshot();
      if ($id_snapshot) {
        $tSnap->update([
          'id_macerations' => $this->getId(),
          'id_users' => $authNamespace->user_id,
          'id_macerations_history' => $id_macerations_history,
          'amount_wh' => $amount_wh,
          'amount_real' => $amount_real,
          'date' => $date->toString('Y-m-d H:i:s'),
          'note' => $note,
          'is_fixed' => $amount_real != $amount_wh ? 'n' : 'y',
        ], 'id = ' . $id_snapshot);
      }
      else {
        $id_snapshot = $tSnap->insert([
          'id_macerations' => $this->getId(),
          'id_users' => $authNamespace->user_id,
          'id_macerations_history' => $id_macerations_history,
          'amount_wh' => $amount_wh,
          'amount_real' => $amount_real,
          'date' => $date->toString('Y-m-d H:i:s'),
          'note' => $note,
          'is_fixed' => $amount_real != $amount_wh ? 'n' : 'y',
        ]);
      }
      return $id_snapshot;
    }

    public function getSnapshots(Meduse_Date $date = NULL, Meduse_Date $to = NULL, $sort = 'DESC') {
      return Tobacco_Macerations::getSnapshots($this, $date, $to, $sort);
    }

    /**
     * Vrati posledni snapshot v danem obdobi.
     *
     * @param \Meduse_Date|NULL $date
     * @param \Meduse_Date|NULL $to
     *
     * @return mixed|null
     */
    public function getLastSnapshot(Meduse_Date $date = NULL, Meduse_Date $to = NULL) {
      $snapshot = NULL;
      $dF = $date->get('Y-m-d H:i:s');
      $dT = $to->get('Y-m-d H:i:s');
      if ($snapshots = $this->getSnapshots($date, $to)) {
        $snapshot = array_shift($snapshots);
      }
      return $snapshot;
    }

    public function getRelatedParts($asObject = FALSE) {
      $tMaceration = new Table_Macerations();
      $parts = $tMaceration->getRelatedParts($this->getRecipeId());
      if ($asObject) {
        $relatedParts = [];
        if ($parts) {
          foreach ($parts as $item) {
            $relatedParts[] = new Tobacco_Parts_Part($item['id']);
          }
        }
        return $relatedParts;
      }
      else {
        return $parts;
      }
    }

    public function getPriceUnit() {
      return $this->getPrice();
    }

    public function getPriceBarrelIn() {
      return $this->getPrice(self::PRICE_TYPE_BARREL_IN);
    }

    public function getPriceBarrelCurrent() {
      return $this->getPrice(self::PRICE_TYPE_BARREL_CURRENT);
    }

    public function getPrice($type = self::PRICE_TYPE_UNIT) {

      $table = new Table_Macerations();
      $prices = $table->getPrice($this->getId());

      switch ($type) {

        case self::PRICE_TYPE_BARREL_IN:
          return $prices[self::PRICE_TYPE_BARREL_IN];

        case self::PRICE_TYPE_BARREL_CURRENT:
          return $prices[self::PRICE_TYPE_BARREL_CURRENT];

        default:
          return $prices[self::PRICE_TYPE_UNIT];
      }
    }

    /**
     * Je macerát vyřazený?
     *
     * @return bool
     */
    public function isDiscarded(): bool
    {
      return (bool) $this->_discarded;
    }

    /**
     * Vyřadí případě zruší vyřazení macerátu.
     *
     * @param bool $discard
     * @return void
     */
    public function discard(bool $discard = TRUE) {
      $this->_discarded = $discard;
      $table = new Table_Macerations();
      $table->update(['discarded' => $discard ? 'y' : 'n'], 'id = ' . $table->getAdapter()->quote($this->getId()));
    }
  }
