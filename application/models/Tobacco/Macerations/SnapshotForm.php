<?php
class Tobacco_Macerations_SnapshotForm extends Meduse_FormBootstrap {

  public function init() {

    $this->setAttribs(['enctype' => 'multipart/form-data', 'class' => 'form'])
      ->setMethod(Zend_Form::METHOD_POST);

    $element = new Zend_Form_Element_Hidden('id_macerations');
    $this->addElement($element);

    $element = new Meduse_Form_Element_DatePicker('date');
    $element->setLabel('Datum zápisu');
    $element->setValue(date('d.m.Y'));
    $element->setRequired(TRUE);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('amount_wh');
    $element->setLabel('Množství ve WH');
    $element->setAttrib('readonly', 'readonly');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Float('amount_real');
    $element->setLabel('Skutečné množství');
    $element->addFilter(new Meduse_Filter_Float());
    $element->setRequired(TRUE);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('note');
    $element->setLabel('Poznámka');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('fix');
    $element->setLabel('Vyrovnat sklad');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Uložit');
    $this->addElement($element);

    parent::init();
  }

  public function setMaceration(Tobacco_Macerations_Maceration $maceration) {
    $this->getElement('id_macerations')->setValue($maceration->getId());
    $amount = $maceration->getWeight();
    $this->getElement('amount_wh')->setValue($amount);
    $this->setLegend('Zanesení fyzického stavu macerace '
      . $maceration->getBatchNumber() . ' – ' . $maceration->getName());
  }

  public function setDate(Meduse_Date $date) {
    $this->getElement('date')->setValue($date->get('d.m.Y'));
  }

  public function setNote(string $str) {
    $this->getElement('note')->setValue($str);
  }

  public function removeFix() {
    $this->removeElement('fix');
  }
}