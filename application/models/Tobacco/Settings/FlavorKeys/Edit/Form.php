<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author Vojtěch Mrkývka
 */
class Tobacco_Settings_FlavorKeys_Edit_Form extends Meduse_FormBootstrap {
  public function init() {
    $this->setAttrib('class', 'form');
    $this->setMethod(Zend_Form::METHOD_POST);

    $element = new Zend_Form_Element_Hidden('id');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('id_new');
    $element->setLabel('ID');
    $validator = new Zend_Validate_Regex('/^[0-9]{1,3}$/i');
    $validator->setMessage('Musíte zadat pouze čísla od 0 do 999');
    $element->addValidator($validator);
    $element->setRequired(true);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('name');
    $element->setLabel('Název');
    $element->setRequired(true);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('is_mix');
    $element->setLabel('Jedná se o fusion?');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('Uložit');
    $this->addElement($element);

    parent::init();
  }
}
