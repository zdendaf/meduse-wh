<?php

/**
 * Description of Form
 *
 * @author Vojtěch Mrkývka
 */
class Tobacco_Settings_Excise_Form extends Meduse_FormBootstrap {

  public function init() {
    $this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
            ->setMethod(Zend_Form::METHOD_POST);

    $element = new Meduse_Form_Element_Text('value');
    $element->setRequired(true);
    $element->setLabel('Hodnota (v Kč za kg)');
    $element->setAttrib('class', 'span1');
    $element->addValidator('int');
    $this->addElement($element);

    $element = new Meduse_Form_Element_DatePicker('active_from_date', array('jQueryParams' => array(
            'dateFormat' => 'dd.mm.yy'
    )));
    $element->setRequired(true);
    $element->setDescription('(DD.MM.YYYY)');
    $element->setLabel('Aktivní od:');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('Odeslat');
    $this->addElement($element);

    parent::init();
  }
}
