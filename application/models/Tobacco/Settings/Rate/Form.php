<?php
/**
 * Formulář pro kurz CZK/EUR v sekci tabák
 *
 * @author Vojtěch Mrkývka
 */
class Tobacco_Settings_Rate_Form extends AdminRate_Form {
  protected $action = '/tobacco-settings/rate-update';
  protected $warehouse = 2;
          
  public function init() {
    parent::init();
  }
}
