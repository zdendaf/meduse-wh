<?php


  class Tobacco_Batches_Batch {

    protected $_partId;
    protected $_batch;
    protected $_amount;
    protected $_mix = array();

    public function __construct($batch = null) {
      if (!is_null($batch)) {
        $this->load($batch);
      }
    }

    public function load($batch) {
      $tPB = new Table_PartsBatch();

      $select = $tPB->select()->setIntegrityCheck(false);
      $select->from($tPB);
      $select->where('batch = ?', $batch);
      $result = $select->query()->fetch(Zend_Db::FETCH_ASSOC);
      if ($result) {
        $this->_amount = $result['amount'];
        $this->_batch = $result['batch'];
        $this->_partId = $result['id_parts'];
      }

      $tBM = new Table_BatchMixes();
      $select = $tBM->select()->setIntegrityCheck(false);
      $select->from(array('bm' => 'batch_mixes'), array());
      $select->joinLeft(array('m' => 'macerations'), 'm.id = bm.id_macerations', array('batch'));
      $select->where('batch = ?', $batch);
      $this->_mix = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }

    public function setPartId($partId) {
      $this->_partId = $partId;
    }

    public function setMixes($barrels) {
      $batches = [];
      if (!is_array($barrels)) {
        $barrels = [$barrels];
      }
      foreach ($barrels as $identifier) {
        $maceration = new Tobacco_Macerations_Maceration($identifier);
        $batches[] = $maceration->getBatchNumber();
      }
      $this->_mix = $batches;
    }

    public function setAmount($amount) {
      $this->_amount = $amount;
    }

    public function generateBatch(){
      $part = new Tobacco_Parts_Part($this->_partId);
      $mix = $part->getMix();
      $minDate = '999999';
      foreach ($this->_mix as $item) {
        $date = substr($item, 1, 6);
        if ($date < $minDate) {
          $minDate = $date;
        }
      }
      $expr = 'L' . $minDate . sprintf('%1$03d', $mix['id_flavor']);
      // Preskakujeme zjisteni, kolikatym balenim byl vyrobek vytvoren.
      // Cislo sarze vyrobku bude vzdy s datem nejstarsiho maceratu a cisla prichute.
      /*
      $table = new Table_PartsBatch();
      $select = $table->select()->setIntegrityCheck(false);
      $select->from($table)->where('batch LIKE ?', $expr);
      $result = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
      $this->_batch = 'L' . $minDate . chr(65 + count($result)) . sprintf('%1$02d', $mix['id_flavor']);
      */
      $this->_batch = $expr;

      // HACK, adhoc fixed batch
      // $this->_batch = 'L141218A' . sprintf('%1$02d', $mix['id_flavor']);
    }

    public function getBatch() {
      return $this->_batch;
    }

    public function save() {

      $part = new Tobacco_Parts_Part($this->_partId);
      $mix = $part->getMix();

      /* Zde dochazi k chybe kvuli hacku v metode generateBatch().
       * Proto je nutne zkontrolovat, jestli jiz v databazi k danemu soucasti neexistuje stejne cislo sarze.
       * Pokud neexistuje, vlozime (insert). Pokud existuje, musime provest aktualizaci mnostvi (update)
       */
      $tPB = new Table_PartsBatch();
      $result = $tPB->select()
        ->where('id_parts = ?', $this->_partId)
        ->where('batch = ?', $this->_batch)
        ->query()->fetch();
      if ($result) {
        $tPB->update(array(
          'amount' => new Zend_Db_Expr('amount + ' . $this->_amount)
        ), array(
          'id_parts = ' . $tPB->getAdapter()->quote($this->_partId),
          'batch = ' . $tPB->getAdapter()->quote($this->_batch),
        ));
      }
      else {
        $tPB->insert(array(
          'id_parts' => $this->_partId,
          'batch' => $this->_batch,
          'amount' => $this->_amount,
        ));
      }

      $tBM = new Table_BatchMixes();
      foreach ($this->_mix as $batch) {
        $maceration = new Tobacco_Macerations_Maceration($batch);
        $tBM->insert(array(
          'id_mixes' => $mix['id_mixes'],
          'id_macerations' => $maceration->getId(),
          'batch' => $this->_batch,
        ));
      }

      }

      public static function getFlavors() {
        $tFlavors = new Table_BatchFlavors();
        return $tFlavors->getAssocArray();
      }
    }

