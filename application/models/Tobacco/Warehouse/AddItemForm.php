<?php
  /**
   * Description of AddItemForm
   *
   * @author Zdenek
   */
  class Tobacco_Warehouse_AddItemForm extends Zend_Form {
    
    public function __construct($options = null) {
          
      $this->setAttribs(array(
        'enctype' => 'multipart/form-data', 
        'class' => 'form'));
      $this->setMethod(Zend_Form::METHOD_POST);
      $this->setAction("/tobacco/add-item");
      
      
      $element = new Zend_Form_Element_Hidden('id');
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Float('amount');
      $element->setLabel('Množství');
      $element->setRequired(true);
      $this->addElement($element);

      parent::__construct($options);      
      
    }    
    
    
    
    
  }
