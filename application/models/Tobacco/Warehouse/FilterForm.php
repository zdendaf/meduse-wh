<?php

class Tobacco_Warehouse_FilterForm extends Meduse_FormBootstrap {

  public function init() {

    $this->setAttribs(array('class' => 'form'));
    $this->setMethod(Zend_Form::METHOD_POST);
    $this->setLegend('Filtrace');

    $element = new Meduse_Form_Element_Text('filter_id');
    $element->setLabel('ID');
    $element->setValue(Zend_Registry::get('tobacco_warehouse')->filter_id);
    $this->addElement($element);


    $element = new Meduse_Form_Element_Text('filter_name');
    $element->setLabel('Název');
    $element->setValue(Zend_Registry::get('tobacco_warehouse')->filter_name);
    $this->addElement($element);

    $db = Zend_Registry::get('db');
    $rows = $db->fetchAll(
      $db->select()->from('producers')
        ->where("EXISTS(SELECT * FROM producers_parts WHERE id_producers = producers.id)")
        ->where('id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE)
        ->order('name'));
    $element = new Meduse_Form_Element_Select('filter_producer');
    $element->setLabel('Dodavatel');
    $element->addMultiOption('null', '- všichni -');
    foreach($rows as $row){
      $element->addMultiOption($row['id'], $row['name']);
    }
    $element->setValue(Zend_Registry::get('tobacco_warehouse')->filter_producer);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('Filtrovat');
    $this->addElement($element);

    parent::init();
  }

  public function removeProducers() {
    $this->removeElement('filter_producer');
  }

}
