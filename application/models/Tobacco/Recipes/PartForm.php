<?php

class Tobacco_Recipes_PartForm extends Meduse_FormSkove {

  private $excluded_ids = array();

  public function __construct($options = null) {
    if (isset($options['excluded'])) {
      foreach ($options['excluded'] as $ex) {
        $this->excluded_ids[] = $ex['id_parts'];
      }
      unset($options['excluded']);
    }
    parent::__construct($options);
  }

  public function init() {
		$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			->setMethod(Zend_Form::METHOD_POST);

		$element = new Meduse_Form_Element_Select('part_id');
		$element->setLabel('Surovina');
		$table = new Table_TobaccoWarehouse();
    $select = $table->getFiltredTobaccoWarehouseStatus(Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS);
    $result = $table->getAdapter()->fetchAll($select);
		$parts = array();
		foreach ($result as $part) {
		  if (!in_array($part['id'], $this->excluded_ids)) {
			  $parts[$part['id']] = $part['name'];
      }
		}
		$element->addMultiOptions($parts);
		$this->addElement($element);

		$element = new Meduse_Form_Element_Float('percent');
		$element->setRequired(true);
		$element->setLabel('Procento');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Přidat');
		$this->addElement($element);

		parent::init();
	}
}