<?php

class Tobacco_Recipes_Recipe {

	static public $state_names = array(
		'active' => 'Aktivní',
		'inactive' => 'Neaktivní',
		'hidden' => 'N/A'
	);

	protected $_id;
	protected $_name;
  protected $_id_flavor;
	protected $_description;
	protected $_due;
	protected $_created_at;
	protected $_last_update;
	protected $_state;
  protected $_flavor_part;
  protected $_flavor_percentage;
  protected $_parts;

  public function __construct($id = NULL) {

    if (is_null($id)) {
      $this->setCreatedAt(date('Y-m-d H:i:s'), FALSE);
    }
    else {

      $tRecipes = new Table_Recipes();
      $select = $tRecipes->select()->where('id = ?', $id);
      $data = $tRecipes->fetchRow($select);

      /*
      $flavor = NULL;
      $percentage = 0;

      $tParts = new Table_RecipesParts();
      $select = $tParts->select()->setIntegrityCheck(FALSE)
        ->from(array('r' => 'recipes_parts'), array())
        ->join(array('p' => 'parts'), 'p.id = r.id_parts', array('id'))
        ->where('r.id_recipes = ?', $id)
        ->where('p.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS);
      $rows = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
      if ($rows) {
        $flavor = new Tobacco_Parts_Part($row->id);
        $recipe_parts = new Tobacco_Recipes_Parts($tRecipes->getRecipeParts($id));
        if ($recipe_parts) {
          foreach ($recipe_parts->getParts() as $part) {
            $this->_parts[$part['id_parts']] = $part['share'];
            if ($part['id_parts'] == $flavor->getID()) {
              $percentage = $part['share'];
            }
          }
        }
      }
      */
      $this->_parts = new Tobacco_Recipes_Parts($tRecipes->getRecipeParts($id));


      $this
        ->setId($id, FALSE)
        ->setFlavorId($data['id_flavor'], FALSE)
        ->setName($data['name'], FALSE)
        ->setDescription($data['description'], FALSE)
        ->setDue($data['due'], FALSE)
        ->setCreatedAt($data['created_at'], FALSE)
        ->setLastUpdate($data['last_update'], FALSE)
        ->setState($data['state'], FALSE);
        //->setFlavorPart($flavor, FALSE)
        //->setFlavorPercentage($percentage, FALSE);
    }

  }

  public function toArray() {
		return array(
			'name' => $this->getName(),
      'id_flavor' => $this->getFlavorId(),
			'description' => $this->getDescription(),
			'due' => $this->getDue(),
			'created_at' => $this->getCreatedAt(),
			'last_update' => $this->getLastUpdate(),
			'state' => $this->getState(),
      'parts' => $this->getParts(),
		);
	}
	
	public function getId() {
    return $this->_id;
  }

	public function setId($id, $save = TRUE) {
		$this->_id = $id;
		if ($save) {
		  $this->save();
    }
		return $this;
	}

	public function getName() {
    return $this->_name;
  }

  public function setName($name, $save = TRUE) {
		$this->_name = $name;
    if ($save) {
      $this->save();
    }
		return $this;
	}

	public function getParts() {

    return is_null($this->_parts) ? array() : $this->_parts->getParts();
  }

  public function getFlavorId() {
    return $this->_id_flavor;
  }

  public function setFlavorId($flavor_id, $save = TRUE) {
    $this->_id_flavor = $flavor_id;
    if ($save) {
      $this->save();
    }
    return $this;
  }

  public function getFlavorName() {
    if (empty($this->_flavor_name)) {
      $table = new Table_BatchFlavors();
      $this->_flavor_name = $table->getName($this->getFlavorId());
    }
    return $this->_flavor_name;
  }

	public function getDescription() {
    return $this->_description;
  }

  public function setDescription($description, $save = TRUE) {
		$this->_description = $description;
    if ($save) {
      $this->save();
    }
		return $this;
	}

	public function getDue() {
    return $this->_due;
  }

  public function setDue($due, $save = TRUE) {
		$this->_due = $due;
    if ($save) {
      $this->save();
    }
		return $this;
	}

	public function getCreatedAt() {
    return $this->_created_at;
  }

	public function setCreatedAt($created_at, $save = TRUE) {
		$this->_created_at = $created_at;
    if ($save) {
      $this->save();
    }
		return $this;
	}

	public function getLastUpdate() {
    return $this->_last_update;
  }

	public function setLastUpdate($last_update, $save = TRUE) {
		$this->_last_update = $last_update;
    if ($save) {
      $this->save();
    }
		return $this;
	}

	public function getState() {
    return $this->_state;
  }

	public function setState($state, $save = TRUE) {
		$this->_state = $state;
    if ($save) {
      $this->save();
    }
		return $this;
	}

	public function getStateString() {
		return self::$state_names[$this->_state];
	}

  /**
   * Zkontroluje, jestli prichut jednoslozkoveho receptu odpovida prichuti.
   *
   * @return bool|NULL
   */
	public function checkFlavor() {
    $tParts = new Table_RecipesParts();
    $select = $tParts->select()->setIntegrityCheck(FALSE)
      ->from(array('r' => 'recipes_parts'), array())
      ->join(array('p' => 'parts'), 'p.id = r.id_parts', array())
      ->join(array('f' => 'parts_flavors'), 'f.id_parts = p.id', array(
        'flavor' => 'id_batch_flavors'))
      ->where('r.id_recipes = ?', $this->getId())
      ->where('p.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS);
    $rows = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
    if (count($rows) != 1) {
      return NULL;
    }
    return ($rows[0] && $rows[0]->flavor == $this->getFlavorId());
  }

  public function setFlavorPart($part, $save = TRUE) {
    $obj = $part instanceof Tobacco_Parts_Part ? $part : new Tobacco_Parts_Part($part);
	  $this->_flavor_part = $obj;
    if ($save) {
      $this->save();
    }
    return $this;
  }



  public function getFlavorPercentage() {
	  $percentage = 0;
	  if ($this->_parts) {
      foreach ($this->getParts() as $part) {
        if ($part['flavor']) {
          $percentage += $part['share'];
        }
      }
	  }
    return $percentage;
  }

  public function setFlavorPercentage($percentage, $save = TRUE) {
	  $this->_flavor_percentage = $percentage;
    if ($save) {
      $this->save();
    }
    return $this;
  }

  public function save() {

    $id = $this->getId();
	  $data = array(
	    'description' => $this->getDescription(),
      'due' => $this->getDue(),
      'id_flavor' => $this->getFlavorId(),
      'name' => $this->getName(),
      'state' => $this->getState(),
      'created_at' => $this->getCreatedAt(),
      'last_update' => date('Y-m-d H:i:s'),
    );

    $tRecipes = new Table_Recipes();
    if ($id) {
      $where = $tRecipes->getAdapter()->quoteInto('id = ?', $id);
      $tRecipes->update($data, $where);
    }
    else {
      $id = $tRecipes->insert($data);
      $this->setId($id, FALSE);
    }

    $settings = Zend_Registry::get('settings');

    $tobacco = $settings->get('recipe_percentage_total_tobacco');
    $preservative = $settings->get('recipe_percentage_total_preservative');
    $sugar_glycerin_ratio = $settings->get('recipe_percentage_sugar_glycerin');
    $flavor = $this->getFlavorPercentage();


    $sugar_glycerin = (100 - ($tobacco + $preservative + $flavor));

    if ($sugar_glycerin <= 0) {
      throw new Tobacco_Recipes_Exceptions_BadRatioException('Příliš mnoho příchutě, musí být méně než ' . (100 - ($tobacco + $preservative)) . '%.');
    }

    $sugar = $sugar_glycerin * ($sugar_glycerin_ratio / 100);
    $glycerin = $sugar_glycerin - $sugar;

    $tobaccoId = $settings->get('recipe_tobacco_id');
    $preservativeId = $settings->get('recipe_preservative_id');
    $sugarId = $settings->get('recipe_sugar_id');
    $glycerinId = $settings->get('recipe_glycerin_id');

    $tRecipes->deleteAllParts($id);

    $tRecipes->insertPart($id, $tobaccoId, $tobacco);
    $tRecipes->insertPart($id, $preservativeId, $preservative);
    $tRecipes->insertPart($id, $sugarId, $sugar);
    $tRecipes->insertPart($id, $glycerinId, $glycerin);
    foreach ($this->getParts() as $part) {
      if (!$part['fixed']) {
        $tRecipes->insertPart($id, $part['id_parts'], $part['ratio']);
      }
    }
  }

  public function saveAs($newName = null, $deactivateCurrent = true) {

	  // ID puvodni receptury.
	  $recipeId = $this->getId();

	  // Ziskani dat z puvodni receptury.
	  $tableRecipes = new Table_Recipes();
	  $select = $tableRecipes->select()->where('id = ' . $recipeId);
	  if (!$dataRecipe = $select->query(Zend_Db::FETCH_ASSOC)->fetch()) {
	    return false;
    }

	  // Ziskani dat o slozkach puvodni receptury.
    $tableRecipesParts = new Table_RecipesParts();
	  $select = $tableRecipesParts->select()->where('id_recipes = ' . $recipeId);
	  if (!$dataParts = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll()) {
	    return false;
    }

	  // Zahajeni transakce pro vlozeni nove receptury.
    $db = $tableRecipes->getAdapter();
    $db->beginTransaction();

    // Vlozeni nove receptury.
	  $now = date('Y-m-d H:i:s');
	  unset($dataRecipe['id']);
    $dataRecipe['created_at'] = $now;
	  if ($newName) {
	    $dataRecipe['name'] = $newName;
    }
	  $newRecipeId = $tableRecipes->insert($dataRecipe);

	  // Vlozeni slozek nove receptury.
	  foreach ($dataParts as $item) {
	    unset($item['id']);
	    $item['id_recipes'] = $newRecipeId;
	    $tableRecipesParts->insert($item);
    }

	  // Ukonceni transakce.
	  $db->commit();

	  // Nacetni a preulozeni - provede prepocet fixnich slozek.
    $newRecipe = new Tobacco_Recipes_Recipe($newRecipeId);
	  $newRecipe->save();

	  // Pripadna deaktivace puvodni receptury.
	  if ($deactivateCurrent) {
	    $tableRecipes->update(['state' => Table_Recipes::STATE_INACTIVE], 'id = ' . $recipeId);
    }

	  // Vraceni nove receptury.
	  return $newRecipe;
  }


  public function set(array $data, $save = TRUE) {

    if (isset($data['name'])) {
      $this->setName($data['name'], FALSE);
    }
    if (isset($data['description'])) {
      $this->setDescription($data['description'], FALSE);
    }
    if (isset($data['due'])) {
      $this->setDue($data['due'], FALSE);
    }
    if (isset($data['state'])) {
      $this->setState($data['state'], FALSE);
    }
    if (isset($data['state'])) {
      $this->setFlavorId($data['id_flavor'], FALSE);
    }
    if (isset($data['flavor_part_id'])) {
      $this->setFlavorPart($data['flavor_part_id'], FALSE);
    }
    if (isset($data['flavor_percentage'])) {
      $this->setFlavorPercentage($data['flavor_percentage'], FALSE);
    }

    if ($save) {
      $this->save();
    }

    return $this;
	}

	public static function getFixedParts(): array
    {
    $settings = new Table_Settings();
	  return array(
	    'tobacco' => $settings::get('recipe_tobacco_id'),
        'preservative' => $settings::get('recipe_preservative_id'),
        'sugar' => $settings::get('recipe_sugar_id'),
        'glycerin' => $settings::get('recipe_glycerin_id'),
    );
  }

  public function removePart($delete_id) {
	  $this->_parts->removePart($delete_id);
    $this->save();
    $tRecipes = new Table_Recipes();
    $this->_parts = new Tobacco_Recipes_Parts($tRecipes->getRecipeParts($this->getId()));
  }

  public function addPart($partId, $percentage) {
	  $part = new Tobacco_Parts_Part($partId);
	  $this->_parts->addPart($partId, $percentage, $part->isFlavor());
	  try {
      $this->save();
    }
    catch(Exception $e) {
      throw new Exception($e->getMessage());
    }
    finally {
      $tRecipes = new Table_Recipes();
      $this->_parts = new Tobacco_Recipes_Parts($tRecipes->getRecipeParts($this->getId()));
    }
  }

  public function getUnitPrice() {
	  $table = new Table_Recipes();
	  $row = $table->getUnitPrice($this->getId());
	  return $row['unit_price'];
  }

public function getCalculatorData(): array
{
    if (!$items = $this->toArray()) {
        return [];
    }

    $data = [
        'name' => $this->getName(),
        'description' => $this->getDescription(),
        'ratio_sum' => 0.0,
        'parts' => [],
    ];
    $flavour = 0;
    $fixed = self::getFixedParts();

    foreach ($items['parts'] as $item) {
        $data['ratio_sum'] += (float) $item['ratio'];

        switch ($item['id_parts']) {
            case $fixed['tobacco']:
                $data['parts'][0] = [
                    'id' => $item['id_parts'],
                    'name' => $item['name'],
                    'editable' => true,
                    'ratio' => (float) $item['ratio'],
                    'tobacco' => true,
                ];
                break;

            case $fixed['sugar']:
                $data['parts'][1] = [
                    'id' => $item['id_parts'],
                    'name' => $item['name'],
                    'editable' => true,
                    'ratio' => (float) $item['ratio'],
                    'tobacco' => false,
                ];
                break;

            case $fixed['glycerin']:
                $data['parts'][2] = [
                    'id' => $item['id_parts'],
                    'name' => $item['name'],
                    'editable' => true,
                    'ratio' => (float) $item['ratio'],
                    'tobacco' => false,
                ];
                break;

            case $fixed['preservative']:
                $data['parts'][3] = [
                    'id' => $item['id_parts'],
                    'name' => $item['name'],
                    'editable' => false,
                    'ratio' => (float) $item['ratio'],
                    'tobacco' => false,
                ];
                break;

            default:
                $data['parts'][4 + $flavour] = [
                    'id' => $item['id_parts'],
                    'name' => $item['name'],
                    'editable' => false,
                    'ratio' => (float) $item['ratio'],
                    'tobacco' => false,
                ];
                $flavour++;
                break;
        }
    }

    ksort($data['parts']);

    return $data;
}

    /**
     * @throws Zend_Form_Exception
     */
    public static function getCalculatorSettingForm(int $recipe, float $weight = 1.0, string $title = ''): Meduse_FormBootstrap
    {
        $tRecipes = new Table_Recipes();
        $eRecipe = (new Meduse_Form_Element_Select('recipe'))
            ->setLabel('Receptura')
            ->addMultiOption(null, '-- zvolit --')
            ->addMultiOptions($tRecipes->getRecipesPairs())
            ->setValue($recipe);

        $eWeight = (new Meduse_Form_Element_Float('weight'))
            ->setLabel('Váha')
            ->setValue($weight);


        $eSubmit = (new Meduse_Form_Element_Submit('save'))
            ->setLabel('Nastavit');

        $form = (new Meduse_FormBootstrap())
            ->setLegend($title)
            ->setAttrib('class', 'form')
            ->addElement($eRecipe)
            ->addElement($eWeight)
            ->addElement($eSubmit)
            ->setMethod(Zend_Form::METHOD_POST);

        $form->init();

        return $form;
    }

    /**
     * @throws Zend_Form_Exception
     */
    public static function getCalculatorForm(Tobacco_Recipes_Recipe $recipe, float $weight): Meduse_FormBootstrap
    {

        $data = $recipe->getCalculatorData();

        $ratioSum = $data['ratio_sum'];
        $unit = $weight / $ratioSum;

        $form = (new Meduse_FormBootstrap())
            ->setAttrib('class', 'form')
            ->setName('calc');


        $form->addElement((new Meduse_Form_Element_Text('weight'))
            ->setOrder(0)
            ->setLabel('Celková váha')
            ->setAttrib('readonly', 'readonly')
            ->setAttrib('class', 'text-right')
            ->setValue(Meduse_Currency::format($weight, null, 3)));

        $order = 1;
        foreach ($data['parts'] as $item) {
            $element = (new Meduse_Form_Element_Text($item['id']))
                ->setOrder(++$order)
                ->setLabel($item['id'] . PHP_EOL . $item['name'])
                ->setAttrib('class', 'text-right')
                ->setValue(Meduse_Currency::format($unit * $item['ratio'], null, 3));

            if ($item['editable']) {
                $element
                    ->setRequired()
                    ->setAttrib('data-ratio', $item['ratio'])
                    ->setAttrib('class', ($item['tobacco'] ? 'group-a' : 'group-b') . ' text-right');
            } else {
                $element->setAttrib('readonly', 'readonly');
            }

            $form->addElement($element);
        }

        $form->init();

        return $form;
    }


}