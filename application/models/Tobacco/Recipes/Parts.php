<?php

class Tobacco_Recipes_Parts {

  private $recipe_parts;

  public function __construct($parts) {
    if (!is_array($parts)) {
      $parts = array($parts);
    }
    $this->recipe_parts = $parts; 
    $this->recalculate();
  }

  public function getParts() {
    return $this->recipe_parts;
  }

  public function removePart($delete_id) {
    $idx = NULL;
    foreach ($this->recipe_parts as $i => $part) {
      if ($part['id'] == $delete_id) {
        $idx = $i;
        break;
      }
    }
    unset($this->recipe_parts[$idx]);
  }

  public function addPart($partId, $percentage, $flavor, $fixed = FALSE) {
    $this->recipe_parts[] = array(
      'id_parts' => $partId,
      'share' => $percentage,
      'ratio' => $percentage,
      'flavor' => $flavor,
      'fixed' => $fixed,
    );
  }

  public function changePartRatio($partId, $ratio, $save = TRUE) {
    foreach ($this->recipe_parts as &$part) {
      if ($part['id_parts'] == $partId) {
        $part['share'] = $ratio;
        $part['ratio'] = $ratio;
        break;
      }
    }
    if ($save) {
      $this->save();
    }
  }

  protected function recalculate() {
    $total_ratio = 0;
    foreach ($this->recipe_parts as $recipe_part) {
      $total_ratio += $recipe_part['ratio'];
    }
    foreach ($this->recipe_parts as &$recipe_part) {
      $recipe_part['share'] = round(($recipe_part['ratio'] / $total_ratio * 100), 2);
    }
  }
}