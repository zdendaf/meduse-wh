<?php

class Tobacco_Recipes_EditForm extends Meduse_FormSkove {
	public function init() {
		$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			->setMethod(Zend_Form::METHOD_POST);

		$element = new Meduse_Form_Element_Text('name');
		$element->setRequired(true);
		$element->setLabel('Název');
		$this->addElement($element);


		$element = new Meduse_Form_Element_Select('id_flavor');
		$element->setLabel('Příchuť');
    $element->addMultiOption(NULL, '-- zvol příchuť --');
    $element->setRequired(TRUE);
    $table = new Table_BatchFlavors();
    $element->addMultiOptions($table->getAssocArray());
		$this->addElement($element);

		$element = new Meduse_Form_Element_Textarea('description');
		$element->setLabel('Poznámka');
		$element->setAttrib("cols", 25);
		$element->setAttrib("rows", 6);
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('due');
		$element->setRequired(true);
		$element->setLabel('Doba macerace [dny]');
		$element->addFilter(new Zend_Filter_Int());
		$element->addValidator(new Zend_Validate_GreaterThan(1));
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('state');
		$options = Tobacco_Recipes_Recipe::$state_names;
		unset($options[Table_Recipes::STATE_HIDDEN]);
		$element->addMultiOptions($options);
		$element->setLabel('Stav');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);

		parent::init();
	}

	public function populate(array $values) {

    return parent::populate($values);
  }

}