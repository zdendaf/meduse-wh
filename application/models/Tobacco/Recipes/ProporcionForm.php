<?php

/**
 * @author Zdeněk Filipec <zdendaf@gmail.com>
 */
class Tobacco_Recipes_ProporcionForm extends Meduse_FormBootstrap {

  public function init() {

    $this->setAttrib('class', 'form');
    $this->setLegend('Nastavení poměrů fixních surovin receptu');

    $element = new Meduse_Form_Element_Float('recipe_percentage_total_tobacco');
    $element->setLabel('Podíl tabáku na celkovém množství [%]');
    $element->setRequired(TRUE);
    $element->setValue(Zend_Registry::get('settings')->get('recipe_percentage_total_tobacco', 95));
    $element->addValidator(new Zend_Validate_GreaterThan(0));
    $element->addValidator(new Zend_Validate_LessThan(100));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Float('recipe_percentage_total_preservative');
    $element->setLabel('Podíl konzervantu na celkovém množství [%]');
    $element->setRequired(TRUE);
    $element->setValue(Zend_Registry::get('settings')->get('recipe_percentage_total_preservative', 1));
    $element->addValidator(new Zend_Validate_GreaterThan(0));
    $element->addValidator(new Zend_Validate_LessThan(100));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Float('recipe_percentage_sugar_glycerin');
    $element->setLabel('Poměr cukru vůči glycerinu [%]');
    $element->setRequired(TRUE);
    $element->setValue(Zend_Registry::get('settings')->get('recipe_percentage_sugar_glycerin', 50));
    $element->addValidator(new Zend_Validate_GreaterThan(0));
    $element->addValidator(new Zend_Validate_LessThan(100));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('recipe_tobacco_id');
    $element->setLabel('ID surového tabáku');
    $element->setRequired(TRUE);
    $element->setValue(Zend_Registry::get('settings')->get('recipe_tobacco_id'));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('recipe_preservative_id');
    $element->setLabel('ID konzervantu');
    $element->setRequired(TRUE);
    $element->setValue(Zend_Registry::get('settings')->get('recipe_preservative_id'));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('recipe_sugar_id');
    $element->setLabel('ID cukru');
    $element->setRequired(TRUE);
    $element->setValue(Zend_Registry::get('settings')->get('recipe_sugar_id'));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('recipe_glycerin_id');
    $element->setLabel('ID glycerinu');
    $element->setRequired(TRUE);
    $element->setValue(Zend_Registry::get('settings')->get('recipe_glycerin_id'));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('recipe_recalculate');
    $element->setLabel('Přepočítat aktuální receptury');
    $element->setValue('n');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Uložit');
    $this->addElement($element);

    parent::init();
  }



}
