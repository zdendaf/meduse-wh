<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 19.12.2018
 * Time: 18:58
 */

class Tobacco_Warehouse {

  public static function addItem($item, $amount) {
    $part = NULL;
    if ($item instanceof Tobacco_Parts_Part) {
      $part = $item;
    }
    elseif (is_string($item)) {
      $part = new Tobacco_Parts_Part($item);
    }
    if ($part) {
      $amount = ($part->getMeasure() == Parts_Part::WH_MEASURE_WEIGHT) ? round($amount * 1000) : $amount;
      $tTW = new Table_TobaccoWarehouse();
      return $tTW->addPart($part->getID(), $amount, FALSE);
    }
    return FALSE;
  }

  public function setSnapshot(Tobacco_Parts_Part $part, $amount_real, Meduse_Date $date = NULL, $amount_wh = NULL, $note = NULL, $id_snapshot = NULL) {
    if (!$date) {
      $date = new Meduse_Date();
    }
    if (!$amount_wh) {
      $amount_wh = $part->getWHAmount($date);
    }
    $authNamespace = new Zend_Session_Namespace('Zend_Auth');
    $tSnap = new Table_PartsWarehouseSnapshot();
    return $tSnap->setSnapshot(
      $part->getID(),
      $amount_wh,
      $amount_real,
      $part->getMeasure(),
      $date->toString('Y-m-d H:i:s'),
      $authNamespace->user_id,
      $note,
      FALSE,
      $id_snapshot
    );
  }

  public function getSnapshot($id) {
    $tSnap = new Table_PartsWarehouseSnapshot();
    return $tSnap->find($id)->current()->toArray();
  }

  public function getSnapshots($part = NULL, Meduse_Date $date = NULL, Meduse_Date $to = NULL, $sort = 'DESC', $limit = NULL) {
    $tSnap = new Table_PartsWarehouseSnapshot();
    $select = $tSnap->select()->setIntegrityCheck(FALSE);
    $select->from(['pws' => 'parts_warehouse_snapshot']);
    $select->join(['p' => 'parts'], 'p.id = pws.id_parts', ['name', 'measure']);
    $select->order('pws.date ' . $sort);
    if ($part) {
      if (is_a($part, 'Parts_Part')) {
        $select->where('pws.id_parts = ?', $part->getID());
      }
      else {
        $select->where('pws.id_parts = ?', $part);
      }
    }
    if ($date && !$to) {
      $select->where('DATE(pws.date) = ?', $date->toString('Y-m-d'));
    }
    elseif ($date && $to) {
      $select->where('? <= DATE(pws.date)', $date->toString('Y-m-d'));
      $select->where('DATE(pws.date) <= ?', $to->toString('Y-m-d'));
    }
    if ($limit) {
      $select->limit($limit);
    }
    $result = $select->query()->fetchAll();
    return $result;
  }

  public function getLastSnapshot($part, Meduse_Date $date = NULL, Meduse_Date $to = NULL) {
    if ($snaps = $this->getSnapshots($part, $date, $to, 'DESC', 1)) {
      return $snaps[0];
    }
    else {
      return FALSE;
    }
  }

  public function getSnapshotsYears() {
    // SELECT DISTINCT YEAR(s.`date`) AS `year` FROM parts_warehouse_snapshot s
    // JOIN parts p ON p.id = s.id_parts AND p.id_wh_type = 2
    // ORDER BY s.`date` DESC;

    $tSnap = new Table_PartsWarehouseSnapshot();
    $select = $tSnap->select()->setIntegrityCheck(FALSE);
    $select->distinct(TRUE);
    $select->from(['s' => 'parts_warehouse_snapshot'], ['year' => new Zend_Db_Expr('YEAR(s.`date`)')]);
    $select->join(['p' => 'parts'], 'p.id = s.id_parts AND p.id_wh_type = 2', []);
    $select->order('s.date DESC');
    $years = [];
    if ($result = $select->query()) {
      while ($year = $result->fetchColumn()) {
        $years[] = $year;
      }
    }
    return $years;
  }

  public function fixSnapshot($id) {
    $tSnap = new Table_PartsWarehouseSnapshot();
    $snap = $this->getSnapshot($id);
    if ($diff = $snap['amount_wh'] - $snap['amount_real']) {
      $msg = 'Kontrola stavu, realný stav ' . $snap['amount_real'];
      try {
        $tWarehouse = new Table_PartsWarehouse();
        $db = $tWarehouse->getAdapter();
        $db->beginTransaction();
        $last_id = NULL;
        if ($diff > 0) {
          $tWarehouse->removePart($snap['id_parts'], $diff, FALSE, $snap['date'], $msg, $last_id);
        }
        else {
          $tWarehouse->addPart($snap['id_parts'], -$diff, FALSE, '', $snap['date'], $msg, $last_id);
        }
        $tSnap->update([
          'is_fixed' => 'y',
          'date' => $snap['date'],
          'id_parts_warehouse_history' => $last_id,
        ], 'id = ' . $id);
        $db->commit();
        return TRUE;
      }
      catch (Exception $e) {
        $db->rollBack();
        return FALSE;
      }
    }
    return TRUE;
  }

  public function fixSnapshots(array $ids = []) {
    if ($ids) {
      foreach ($ids as $id) {
        $this->fixSnapshot($id);
      }
    }
  }


  public function getClosingReport($year) {
    $report = [];
    $closing_date = new Meduse_Date($year . '-12-31', 'Y-m-d');
    $table = new Table_Parts();
    $data = $table->getClosingReport($year);

    foreach ($data as $item) {
      if (!isset($report[$item['id_parts_ctg']])) {
        $report[$item['id_parts_ctg']] = [
          'category_name' => Table_TobaccoWarehouse::$ctgToString[$item['id_parts_ctg']],
          'parts' => [],
        ];
      }

      $real_date = !is_null($item['amount_real_date']) ? new Meduse_Date($item['amount_real_date'], 'Y-m-d H:i:s') : FALSE;
      $diff_abs = $item['amount_real'] - $item['amount'];
      $diff = $item['amount_remove'] && !is_null($item['amount_real']) ? $diff_abs / $item['amount_remove'] : FALSE;

      if (($diff !== FALSE && ($diff <= -0.05 || $diff > 0)) || ($real_date && $real_date->get('ym') < $closing_date->get('ym'))) {
        $class = 'alert-danger';
      }
      elseif ($diff !== FALSE && (-0.05 < $diff && $diff <= 0)) {
        $class = 'alert-success';
      }
      else {
        $class = '';
      }


      $report[$item['id_parts_ctg']]['parts'][$item['id']] = [
        'name' => $item['name'],
        'unit' => $item['measure'] == 'unit' ? 'ks' : 'kg',
        'start_amount' => $item['measure'] == 'unit' ? $item['snap_start'] : sprintf('%01.3f', $item['snap_start'] / 1000),
        'amount_add' => $item['measure'] == 'unit' ? $item['amount_add'] : sprintf('%01.3f', $item['amount_add']  / 1000),
        'amount_remove' => $item['measure'] == 'unit' ? $item['amount_remove'] : sprintf('%01.3f', $item['amount_remove'] / 1000),
        'end_amount' => $item['measure'] == 'unit' ? $item['amount'] : sprintf('%01.3f', $item['amount'] / 1000),
        'snap_id' => $item['snap_id'],
        'amount_real' => !is_null($item['amount_real'])  ? ($item['measure'] == 'unit' ? $item['amount_real'] : sprintf('%01.3f', $item['amount_real'] / 1000)) : FALSE,
        'amount_real_date' => $real_date,
        'diff_abs' => $diff_abs,
        'diff' => $diff !== FALSE ? sprintf('%01.2f %%', $diff * 100) : '—',
        'class' => 'part_row' . ($class ? ' ' . $class : ''),
      ];
    }

    return $report;
  }


  public function closeYear($year) {

    $tParts = new Table_Parts();
    if ($data = $tParts->getClosingReport($year)) {

      $tSnap = new Table_PartsWarehouseSnapshot();
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $date = ($year + 1) . '-01-01 00:00:00';

      foreach ($data as $item) {

        // kdyz je odchylka, tak vyrovnat
        if (!is_null($item['snap_id']) && $item['amount'] != $item['amount_real']) {
          $this->fixSnapshot($item['snap_id']);
        }

        // zapsat stav k prvnimu
        $amount = is_null($item['amount_real']) ? $item['amount'] : $item['amount_real'];
        if ($item['measure'] === Parts_Part::WH_MEASURE_WEIGHT) {
          $amount = $amount / 1000;
        }
        $tSnap->setSnapshot(
          $item['id'],
          $amount,
          $amount,
          $item['measure'],
          $date,
          $authNamespace->user_id,
          'Počáteční stav k 01.01.' . ($year + 1),
          TRUE
        );
      }
    }
    else {
      throw new Exception("No data for closing year.");
    }
  }

}