<?php

  class Tobacco_Parts_Form extends Meduse_FormBootstrap {

    protected $category = null;

    public function __construct($options = null) {
      $this->init($options);
    }

    public function init($options = null) {
      $this
          ->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
          ->setMethod(Zend_Form::METHOD_POST);

      if (in_array($options['category'], array(
          Table_TobaccoWarehouse::CTG_MATERIALS,
          Table_TobaccoWarehouse::CTG_MATERIALS_TOBACCO,
          Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS,
          Table_TobaccoWarehouse::CTG_MATERIALS_OTHER
        ))) {
        $element = new Meduse_Form_Element_Text('critical_amount');
        $element->setLabel('Kritické množství [g]');
        $element->addFilter('Int');
        $element->setOrder(80);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('category');
        $element->addMultiOptions($this->retrieveCategories(Table_TobaccoWarehouse::CTG_MATERIALS));
        $element->setLabel('Kategorie');
        $element->setOrder(30);
        $element->setRequired(true);
        $element->setValue($options['category']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('flavor');
        $element->addMultiOptions($this->retrieveFlavors());
        $element->setLabel('Příchuť');
        $element->setOrder(31);
        if (isset($options['flavor'])) {
          $element->setValue($options['flavor']);
        }
        $this->addElement($element);
      }
      else {

        if (
        in_array($options['category'], array(
          Table_TobaccoWarehouse::CTG_PACKAGING,

          Table_TobaccoWarehouse::CTG_PACKAGING_LABELS,
          Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_BOX_010,
          Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_BOX_050,
          Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_SACHET_010,
          Table_TobaccoWarehouse::CTG_PACKAGING_SACHETS,
          Table_TobaccoWarehouse::CTG_PACKAGING_BOXES,
          Table_TobaccoWarehouse::CTG_PACKAGING_OTHER,
        ))) {

          $element = new Meduse_Form_Element_Select('category');
          $element->addMultiOptions($this->retrieveCategories(Table_TobaccoWarehouse::CTG_PACKAGING));
          $element->setLabel('Kategorie');
          $element->setOrder(30);
          $element->setRequired(true);
          $element->setValue($options['category']);
          $this->addElement($element);

          $element = new Meduse_Form_Element_Text('weight');
          $element->setLabel('Hmotnost [g]');
          $element->addValidator(new Zend_Validate_Int());
          $element->setOrder(54);
          $this->addElement($element);

          $element = new Meduse_Form_Element_Select('ekokom_ctg');
          $element->setLabel('Zařazení EKOKOM');
          $element->addMultiOptions($this->retrieveEkokomCategories());
          $element->setOrder(55);
          $this->addElement($element);
        }
        else {
          $element = new Zend_Form_Element_Hidden('category');
          $element->setOrder(800);
          if (isset($options['category'])) {
            $element->setValue($options['category']);
            $this->category = $options['category'];
          }
          $this->addElement($element);
        }

        $element = new Meduse_Form_Element_Text('critical_amount');
        $element->setLabel('Kritické množství [ks]');
        $element->addFilter('Int');
        $element->setOrder(80);
        $this->addElement($element);

      }

      $element = new Meduse_Form_Element_Text('id');
      $element->setLabel('Kód');
      $element->setRequired(true);
      $this->addElement($element);
      $element->setOrder(10);

      $element = new Meduse_Form_Element_Text('name');
      $element->setLabel('Název');
      $element->setRequired(true);
      $this->addElement($element);
      $element->setOrder(20);

      $element = new Meduse_Form_Element_Textarea('description');
      $element->setLabel('Popis');
      $element->setAttrib('rows', 3);
      $this->addElement($element);
      $element->setOrder(40);

      if ($this->category != Table_TobaccoWarehouse::CTG_TOBACCO_FS && $this->category != Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
        $element = new Meduse_Form_Element_Select('producers');
        $element->setLabel('Dodavatel');
        $element->addMultiOption(NULL, '-- zvol --');
        $element->addMultiOptions($this->retrieveProducers());
        $this->addElement($element);
        $element->setOrder(50);
      }

      $element = new Meduse_Form_Element_Currency('price');
      $element->setLabel('Cena');
      $this->addElement($element);
      $element->setOrder(60);

      if ($this->category == Table_TobaccoWarehouse::CTG_ACCESSORIES) {
        $element = new Meduse_Form_Element_Checkbox('product');
        $element->setLabel('Produkt');
        $element->setOrder(70);
        $this->addElement($element);
      }
      elseif ($this->category == Table_TobaccoWarehouse::CTG_TOBACCO_NFS ||
        $this->category == Table_TobaccoWarehouse::CTG_TOBACCO_FS) {
        $element = new Zend_Form_Element_Hidden('product');
        $element->setOrder(1);
        $this->addElement($element);
      }

      if (in_array($this->category, array(
        Table_TobaccoWarehouse::CTG_TOBACCO_NFS,
        Table_TobaccoWarehouse::CTG_TOBACCO_FS,
        Table_TobaccoWarehouse::CTG_ACCESSORIES,
        ))) {
        $element = new Meduse_Form_Element_Text('sku');
        $element->setLabel('SKU (eshop)');
        $this->addElement($element);
        $element->setOrder(85);
      }

      $element = new Meduse_Form_Element_Text('hs');
      $element->setLabel('HS code');
      $element->setOrder(90);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('Odeslat');
      $element->setOrder(999);
      $this->addElement($element);

      parent::init($options);
    }

    protected function retrieveProducers() {
      $tProducers = new Table_Producers();
      $select = $tProducers->select()->setIntegrityCheck(false);
      $select->from(array('p' => 'producers'), array('id', 'name'));
      $select->joinLeft(array('oc' => 'orders_carriers'), 'oc.id_producers = p.id', array());
      $select->where('p.is_active = ?', 'y');
      $select->where('p.id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE);
      $select->where('oc.id IS NULL');
      $select->order('p.name ASC');
      return Zend_Registry::get('db')->fetchPairs($select);
    }

    protected function retrieveCategories($supCategory, $depth = 0) {
      $db = Zend_Db_Table::getDefaultAdapter();
      $select = $db->select()
        ->from('parts_ctg', array('id', 'name'))
        ->order('ctg_order')
        ->where('parent_ctg = ?', $supCategory);
      $pairs = [];
      if ($items = $db->fetchPairs($select)) {
        $depth++;
        foreach ($items as $cid => $name) {
          $pairs[$cid] = str_repeat('-', $depth-1) . ' ' . $name;
          $pairs += $this->retrieveCategories($cid, $depth);
        }
      }
      return $pairs;
    }

    protected function retrieveFlavors() {
      $table = new Table_BatchFlavors();
      $select = $table->select()
        ->from($table, array('id', 'name'))
        ->where('is_mix = ?', 'n')
        ->where('active = ?', 'y')
        ->order('name');
      return $table->getAdapter()->fetchPairs($select);
    }

    protected function retrieveEkokomCategories() {
      $db = Zend_Db_Table::getDefaultAdapter();
      $select = $db->select()->from('ekokom_ctg', array('id', 'name'));
      $categories = array('null' => 'nezařazeno') + $db->fetchPairs($select);
      return $categories;
    }
  }

