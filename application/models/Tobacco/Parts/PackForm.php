<?php

  class Tobacco_Parts_PackForm extends Meduse_FormBootstrap {
    
    public function init($options = null) {
      
      $this->setAction('/tobacco/parts-pack-process');
      $this->setName('parts-pack-form');
      $this->setAttrib('class', 'form');
      $this->setLegend('Nabalení produktu ' . $options['part_id'] . ' (' . $options['weight'] .' kg)');
    
      $element = new Zend_Form_Element_Hidden('part_id');
      $element->setValue($options['part_id']);
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Text('amount');
      $element->setRequired(true);
      $element->setLabel('Počet kusů');
      $element->addValidator(new Zend_Validate_Int());
      $element->addValidator(new Zend_Validate_GreaterThan(1));
      $element->setValue(0);
      $this->addElement($element);
      
      $iit = 0;
      if ($options['ingredients']) {
        foreach ($options['ingredients'] as $row) {
          $element = new Meduse_Form_Element_Radio('barrels_' . $iit);
          $element->setRequired(true);
          $element->setLabel($row['name'] . ' (' . $row['ratio'] . ' d.)');
          foreach ($row['barrels'] as $barrel) {
            if ((int)$barrel['days_left'] <= 0) {
              $element->addMultiOption($barrel['batch'], '<nobr>' . $barrel['batch'] . '<br />(' . $barrel['weight'] . ' kg)</nobr>');
              $this->addElement($element);
            }
          }
          $iit++;
        }
      }
      
      $element = new Zend_Form_Element_Hidden('count');
      $element->setValue($iit);
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Submit('Nabalit');
      $this->addElement($element);
      
      parent::init();
    }  
  }
