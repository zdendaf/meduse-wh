<?php
class Tobacco_Parts_BreakForm extends Meduse_FormBootstrap {
	
  private $_part;
  
  public function init() {
    
		$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			->setMethod(Zend_Form::METHOD_POST);
    
    $element = new Meduse_Form_Element_Submit('Odeslat');
    $element->setOrder(999);
    $this->addElement($element);
    
    parent::init();
}
  
  public function appendBatches($part_id) {
    
    if (is_null($part_id)) {
      throw new Tobacco_Parts_Exceptions_NoPartIdException('Nebylo zadáno ID součásti.');
    } 

    $this->_part = new Tobacco_Parts_Part($part_id);
    
    $batches = $this->_part->getBatches();
    if (!$batches) {
      return;
    }

    $it = 0;
    foreach ($batches as $item) {
      $element = new Zend_Form_Element_Hidden('batch_number_' . $it);
      $element->setValue($item['batch']);
      $element->setOrder(500 + 2*$it);
      $this->addElement($element);
      
      $element = new Zend_Form_Element_Text('batch_amount_' . $it);
      $element->setLabel($item['batch']);
      $element->addValidator(new Zend_Validate_LessThan($item['amount']+1));
      $element->addValidator(new Zend_Validate_Int());
      $element->setValue($item['amount']);
      $element->setOrder(501 + 2*$it);
      $this->addElement($element);
      
      $it++;
    }

    if ($this->_part->getCtg() == Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
      $mixId = $this->_part->getMix();
      $mix = new Tobacco_Mixes_Mix($mixId['id_mixes']);
      $recipes = $mix->getRecipes();
      foreach ($recipes as $idx => $recipe) {
        $recipeId = $recipe['id_recipes'];
        $macerations = new Table_Macerations();
        if ($barrels = $macerations->getBarrels($recipeId, TRUE)) {
          $element = new Meduse_Form_Element_Select('barrel_' . $idx);
          $element->setLabel('Složka ' . $recipe['name']);
          foreach($barrels as $barrel) {
            $element->addMultiOption($barrel['id'], $barrel['batch']);
          }
          $element->setOrder(600 + (2 * $idx));
          $this->addElement($element);
          $element = new Zend_Form_Element_Hidden('ratio_' . $idx);
          $element->setValue($recipe['ratio']);
          $element->setOrder(601 + (2 * $idx));
          $this->addElement($element);
        }
        else {
          throw new Exception('Nebyl nalezen žádný sud odpovídající příchutě.');
        }
      }

      $element = new Zend_Form_Element_Hidden('recipes_count');
      $element->setValue(count($recipes));
      $element->setOrder(700);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Checkbox('nfstowh');
      $element->setLabel('Rozložit také obal');
      $element->setOrder(705);
      $this->addElement($element);
    }
  }
  
}