<?php
  /**
   * Formulář na rezervaci šarží tabáku v objednávce
   * 
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Tobacco_Parts_ReservationBatchForm extends Meduse_FormBootstrap {
    
    public function init() {
      
      $this->setAttrib('class', 'form');
      $this->setLegend('Vyběr šarží pro rezervaci');
      
      //ID tabáku
      $element = new Zend_Form_Element_Hidden('id_part');
      $element->setRequired(true);
      $element->setOrder(1);
      $this->addElement($element);
      
      //ID objednávky
      $element = new Zend_Form_Element_Hidden('id_order');
      $element->setRequired(false);
      $element->setOrder(2);
      $this->addElement($element);
      
      //Celkové množství tabáku k rezervaci
      $element = new Zend_Form_Element_Hidden('total_amount');
      $element->setRequired(true);
      $element->setOrder(898);
      $this->addElement($element);
      
      //Součet aktuálně vybraného množství tabáku napříč šaržemi (pro kontrolu)
      $element = new Meduse_Form_Element_Text('sum_amount');
      $element->setAttrib('readonly', 'readonly');
      $element->setOrder(899);
      $element->setLabel('Součet');
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Submit('Rezervovat');
      $element->setOrder(900);
      $this->addElement($element);
      
      parent::init();
    }
    
    /**
     * Nastavení ID tabáku
     * 
     * @param string $partId ID tabáku
     */
    public function setPartId($partId) {
      $this->getElement('id_part')->setValue($partId);
    }
    
    /**
     * Nastavení ID objednávky
     * 
     * @param string|int $orderId ID objednávky
     */
    public function setOrderId($orderId) {
      $this->getElement('id_order')->setValue($orderId);
    }

    public function setTotalAmount($amount) {
      $this->getElement('total_amount')->setValue($amount);
      $validator = new Zend_Validate_Between($amount, $amount);
      $validator->setMessage('Bylo překročeno maximální množství ' . $amount . ' ks.');
      $this->getElement('sum_amount')->addValidator($validator);
    }
    
    /**
     * Vytvoření seznamu šarží s předpočítaným výběrem
     * 
     * @param array $batches Seznam šarží
     * @param int   $maxAmount Maximální množství (pro automatický výběr)
     */
    public function setBatches($batches, $maxAmount) {
      if ($batches) {
        $it = 0;
        
        //vytváření elementů pro jednotlivé šarže
        foreach (array_reverse($batches) as $item) {
          //číslo šarže
          $element = new Zend_Form_Element_Hidden('batch_number_' . $it);
          $element->setValue($item['batch']);
          $element->setOrder(500 + 2*$it);
          $this->addElement($element);
          
          //vybrané množství
          $element = new Zend_Form_Element_Text('batch_amount_' . $it);
          $element->setAttrib('class', 'batch_amounts');
          $element->setLabel($item['batch']);
          ///kontrola, jestli je dané množství na skladě
          $validator = new Zend_Validate_LessThan($item['amount']+1);
          $validator->setMessage('Maximální množství produktu s touto šarží je ' . $item['amount'] . ' ks.');
          $element->addValidator($validator);
          $element->addValidator(new Zend_Validate_Int());
          
          //nastavování automatického výběru
          $itemAmount = min($maxAmount, $item['amount']);
          $maxAmount -= $itemAmount;
          $element->setValue($itemAmount);
          $element->setOrder(501 + 2*$it);
          $this->addElement($element);
          $it++;
        }
      }
    }
  }

