<?php

  class Tobacco_Parts_MixForm extends Meduse_FormBootstrap {

    
    public function init() {
      
      $this->setAttribs(array('class' => 'form', 'id' => 'parts_mix_form'));
      
      $element = new Zend_Form_Element_Hidden('part');
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Select('mix');
      $element->setLabel('Zvol požadovaný mix');
      $element->setRequired(true);
      $tMixes = new Table_Mixes();
      $options = $tMixes->getMixesPairs();
      $element->setMultiOptions(array('' => '- zvol -'));
      $element->addMultiOptions($options);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('amount');
      $element->setRequired(true);
      $element->setLabel('Množství [kg]');
      $element->addValidator(new Zend_Validate_Int());
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Submit('Uložit');
      $this->addElement($element);

      parent::init();	      
    }
    
    public function setPartId($id) {
      $this->getElement('part')->setValue($id);
    }
  }

