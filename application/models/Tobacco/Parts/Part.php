<?php

  class Tobacco_Parts_Part extends Parts_Part {

    const WH_TYPE = 2;
    const INSPECTED_PARTS_ID = ['TS001', 'TK001', 'TK002', 'TK003', 'TK004'];

    const UNIT_DEFAULT = 'default';
    const UNIT_GRAMS = 'grams';
    const UNIT_KILOGRAMS = 'kilograms';

    /**
     * Konstruktor polozky skladu
     */
    public function __construct($arg, $amount = 1) {
      if (is_array($arg)) {
        parent::__construct($this->insert($arg));
      }
      else {
        parent::__construct($arg, $amount, self::WH_TYPE);
      }
    }

    /**
     * Vlozeni nove polozky skladu
     *
     * @param array $data
     *   udaje o vkladane skladove polozce
     */
    public function insert($data) {

      $auth = new Zend_Session_Namespace('Zend_Auth');
      $userId = $auth->user_id;

      $tParts = new Table_Parts();
      $exists = $tParts->checkIdExist($data['id']);
      if ($exists) {
        throw new Parts_Exceptions_IdAlreadyExists('V systému již existuje součást se zadaným ID = ' . $data['id']);
      }
      $part = array(
        'id' => $data['id'],
        'name' => $data['name'],
        'id_parts_ctg' => $data['category'],
        'id_wh_type' => Table_TobaccoWarehouse::WH_TYPE,
        'measure' => in_array($data['category'], [
          Table_TobaccoWarehouse::CTG_TOBACCO,
          Table_TobaccoWarehouse::CTG_MATERIALS,
          Table_TobaccoWarehouse::CTG_MATERIALS_TOBACCO,
          Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS,
          Table_TobaccoWarehouse::CTG_MATERIALS_OTHER,
        ]) ? 'weight' : 'unit',
        'product' => in_array($data['category'], array(
          Table_TobaccoWarehouse::CTG_TOBACCO_FS,
          Table_TobaccoWarehouse::CTG_TOBACCO_NFS))? 'y' : (isset($data['product'])? $data['product'] : 'n'),
        'price' => $data['price'],
        'description' => $data['description'],
      );

      $wh = array(
        'id_parts' => $data['id'],
        'id_users' => $userId,
        'amount' => 0,
        'critical_amount' => $data['critical_amount'],
      );

      $tParts = new Table_Parts();
      $tWH = new Table_TobaccoWarehouse();

      $newId = $tParts->insert($part);
      $tWH->insert($wh);

      // todo sem pridat vlozeni pocatecniho stavu.
      $tSnap = new Table_PartsWarehouseSnapshot();
      $snap_data = [
        'id_parts' => $newId,
        'id_users' => $userId,
        'amount_wh' => 0,
        'amount_real' => 0,
        'note' => 'Počáteční stav k ' . date('d.m.Y'),
        'is_fixed' => 'y',
      ];
      $tSnap->insert($snap_data);

      if (isset($data['producers'])) {
        $producer = array(
          'id_parts' => $data['id'],
          'id_producers' => $data['producers'],
          'producers_part_id' => $data['id'],
          'producers_part_name' => $data['name'],
          'price' => $data['price'],
          'id_users' => $userId,
          'inserted' => new Zend_Db_Expr('now()'),
        );
        $tProducers = new Table_ProducersParts();
        $tProducers->insert($producer);
      }

      return $newId;
    }

    /**
     * Prida k soucasti podcoucast
     *
     * @param array $subparts
     *   pole podsoucasti (id => pocet)
     */
    public function addSubParts($subparts) {
      $tSubparts = new Table_PartsSubparts();
      foreach($subparts as $sid => $amount) {
        $tSubparts->insert(array(
          'id_multipart' => $this->getID(),
          'id_parts' => $sid,
          'amount' => $amount,
        ));
      }
    }

    public function getPossibleSubparts() {

      $partCtg  = $this->getCtg();
      if (in_array($partCtg, array(
        Table_TobaccoWarehouse::CTG_TOBACCO_FS,
        Table_TobaccoWarehouse::CTG_TOBACCO_NFS,
        Table_TobaccoWarehouse::CTG_ACCESSORIES))) {
        $tParts = new Table_Parts();
        $select = $tParts->select()->setIntegrityCheck(false);
        $select->from(['p' => 'parts'], array('id', 'name', 'description'));
        $select->joinLeft(['c' => 'parts_ctg'], 'c.id = p.id_parts_ctg', [
          'ctg' => 'id', 'ctg_name' => 'name']);

        if ($partCtg == Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
          $select->where('p.id_parts_ctg IN (?)', array(
              Table_TobaccoWarehouse::CTG_PACKAGING,
              Table_TobaccoWarehouse::CTG_PACKAGING_LABELS,
              Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_BOX_010,
              Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_BOX_050,
              Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_SACHET_010,
              Table_TobaccoWarehouse::CTG_PACKAGING_SACHETS,
              Table_TobaccoWarehouse::CTG_PACKAGING_BOXES,
              Table_TobaccoWarehouse::CTG_PACKAGING_OTHER,
            ));
        }

        elseif ($partCtg == Table_TobaccoWarehouse::CTG_TOBACCO_FS) {
          $select->where('p.id_parts_ctg IN (?)', array(
              Table_TobaccoWarehouse::CTG_TOBACCO_NFS,
              Table_TobaccoWarehouse::CTG_DUTY_STAMPS,
            ));
        }

        else {
          $select->where('p.id_parts_ctg IN (?)', array(
              Table_TobaccoWarehouse::CTG_PACKAGING,
              Table_TobaccoWarehouse::CTG_ACCESSORIES,
            ));
          $select->where('p.id <> ?', $this->getID());
        }

        $select->where('p.deleted = ?', 'n');
        $select->order(['c.id ASC', 'p.id ASC']);

        return $select->query()->fetchAll(Zend_Db::FETCH_ASSOC);
      }
      return [];
    }

    public function isTobaccoNFS() {
      return $this->getCtg() == Table_TobaccoWarehouse::CTG_TOBACCO_NFS;
    }

    public function isTobaccoFS() {
      return $this->getCtg() == Table_TobaccoWarehouse::CTG_TOBACCO_FS;
    }

    public function isFlavor() {
      return $this->getCtg() == Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS;
    }

    public function getMix($asObject = FALSE) {
      $ctg = $this->getCtg();
      if ($ctg == Table_TobaccoWarehouse::CTG_TOBACCO_FS) {
        if ($subParts = $this->getSubparts()) {
          /** @var \Tobacco_Parts_Part $part */
          foreach ($subParts as $part) {
            if ($part->getCtg() == Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
              return $part->getMix($asObject);
            }
          }
        }
      }
      elseif ($ctg == Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
        $table = new Table_PartsMixes();
        $select = $table->select()->setIntegrityCheck(false);
        $select->from(array('pm' => 'parts_mixes'));
        $select->joinLeft(array('m' => 'mixes'), 'm.id = pm.id_mixes', array('id_flavor'));
        $select->where('id_parts = ?', $this->getID());
        $result = $select->query()->fetch(Zend_Db::FETCH_ASSOC);
        if ($result) {
          return $asObject ? new Tobacco_Mixes_Mix($result['id_mixes']) : $result;
        }
        else {
          throw new Tobacco_Parts_Exceptions_NoContentException();
        }
      }
      return NULL;
    }

    public function getSupParts($include_deleted = TRUE) {
      $tSubparts = new Table_PartsSubparts();
      return $tSubparts->getAllSupParts($this->getID(), $include_deleted);
    }

    public function getBatches() {
      $tPB = new Table_PartsBatch();
      return $tPB->findByPart($this->getID());
    }

    public function getParentCtg() {
      $select = Zend_Db_Table::getDefaultAdapter()->select();
      $select->from('parts_ctg', array('parent_ctg'))->where('id = ?', $this->getCtg());
      $data = $select->query(Zend_Db::FETCH_ASSOC)->fetch();
      return $data['parent_ctg'];
    }


    public function checkMix(&$amount, $barrels) {

      $mix = $this->getMix();
      $mixObj = new Tobacco_Mixes_Mix($mix['id_mixes']);
      $ingredients = $mixObj->getIngredients(true);
      $netto = (float) $mix['amount'];

      $checkArr = $this->_computeIngredients($ingredients, $barrels, $netto, $amount);
      $status = true;

      $maxAmounts = array();
      $maxAmounts[] = ($amount == 0) ? PHP_INT_MAX : $amount;
      foreach ($checkArr as $check) {
        $maxAmounts[] = $check['max_amount'];
      }
      $maxAmount = min($maxAmounts);

      foreach ($checkArr as $check) {
        $status = $status && $check['status'];
      }
      $amount = $maxAmount;
      return ($amount > 0) ? $status : false;
    }
    public function pack($amount, $barrels) {

      $mix = $this->getMix();
      $mixObj = new Tobacco_Mixes_Mix($mix['id_mixes']);
      $ingredients = $mixObj->getIngredients(true);
      $netto = (float) $mix['amount'];
      $mixArr = $this->_computeIngredients($ingredients, $barrels, $netto, $amount);

      $maxAmounts = array();
      $maxAmounts[] = $amount;
      foreach ($mixArr as $item) {
        $maxAmounts[] = $item['max_amount'];
      }
      $maxAmount = min($maxAmounts);
      if ($maxAmount < $amount) {
        throw new Tobacco_Macerations_Exceptions_LowAmountInBarrelException('Nedostatečné množství macerátu');
      }

      $partId = $this->getID();

      $tTW = new Table_TobaccoWarehouse();
      $tTW->addPart($partId, $amount, FALSE, ', ');

      $tMaceration = new Table_Macerations();
      $critical = array('subparts' => array(), 'batches' => array());
      foreach ($mixArr as $item) {
        $maceration = new Tobacco_Macerations_Maceration($item['batch']);
        $maceration->setWeight($maceration->getWeight() - $item['required']);
        $maceration->history(Table_MacerationsHistory::TYPE_PACK, -$item['required'], 'Packing: ' . $partId . ' ('
          . $amount . ' ks ~ ' . $item['required'] . 'kg)');
        $tMaceration->update(array(
          'amount_current' => $maceration->getWeight(),
        ), 'id = ' . $maceration->getId());

        //kontrola limitního množství
        $crit = $tMaceration->checkCritical($maceration->getId());
        if ($crit) {$critical['batches'][] = $crit['batch'] . ' (' . $crit['name'] . ')';}

        ///kontrola LM podsoučástí
        $tSubparts = new Table_PartsSubparts();
        foreach ($tSubparts->selectSubpartsAmount($partId)->query()->fetchAll() as $subpart) {
          if ($subpart['amount_critical']>=$subpart['amount_warehouse']) {
            $critical['subparts'][] = $subpart['name'] . ' (' . $subpart['part'] . ')';
          }
        }
      }

      ///odebrání duplikátů
      $critical['subparts'] = array_unique($critical['subparts']);

      $batch = new Tobacco_Batches_Batch();

      $batch->setPartId($partId);
      $batch->setMixes($barrels);
      $batch->setAmount($amount);
      $batch->generateBatch();
      $batch->save();

      return $critical;
    }

    private function _computeIngredients($ingredients, $barrels, $netto, $amount) {

      $total = (float) $netto * $amount;
      $mixArr = array();
      $all = 0;

      foreach ($ingredients as $item) {
        $mixArr[$item['id_recipes']] = array(
          'status' => false,
          'ratio' => (float) $item['ratio'],
          'required' => 0,
        );
        $all += $item['ratio'];
      }

      foreach ($barrels as $batch) {
        $barrel = new Tobacco_Macerations_Maceration($batch);
        $recipeId = $barrel->getRecipeId();
        $weight = (float) $barrel->getWeight();

        if (isset($mixArr[$recipeId])) {
          $mixArr[$recipeId]['batch'] = $batch;
          $required = $total * $mixArr[$recipeId]['ratio'] / $all;
          $mixArr[$recipeId]['required'] = $required;
          $mixArr[$recipeId]['status'] = ($required <= $weight);
          $mixArr[$recipeId]['max'] = $weight;
          $mixArr[$recipeId]['max_amount'] = (int) floor($weight / $mixArr[$recipeId]['ratio'] * $all / $netto);
        }
      }
      return $mixArr;
    }

    /**
     * vraci mnoztvi tabaku v produktu v kilogramech
     */
    public function getTobbacoAmount() {
      return $this->_getTobbacoAmountRecursion($this->getID());
    }

    protected function _getTobbacoAmountRecursion($partId, $table = null) {
      if (is_null($table)) {
        $table = new Table_PartsMixes();
      }
      $amount = $table->getAmount($partId);
      $supPart = new Tobacco_Parts_Part($partId);
      $subParts = $supPart->getSubparts();
      if ($subParts && count($subParts) > 0) {
        foreach ($subParts as $part) {
          $amount += $this->_getTobbacoAmountRecursion($part->getID(), $table);
        }
      }
      return $amount;
    }

    public function getWeight() {
      $macerat = $this->getCtg() == Table_TobaccoWarehouse::CTG_TOBACCO_NFS ?
        $this->getTobbacoAmount() * 1000 : 0;
      return is_null($this->_part_weight_row) ?
        $macerat : ($this->_part_weight_row->weight + $macerat);
    }

    /**
     * Funkce se pokusi provest ukony potrebne pro to, aby na sklade bylo
     * dostatecne mnozsvi produktu:
     *
     * Pokud na skladě neexistuje dostatečné množství kolkovaného produktu,
     * ale existuje dostatečné množství nekolkovaného produktu a příslušných
     * kolků, pak systém příslušné produkty okolkuje.
     *
     * Pokud neexistuje ani dostatečný počet nekolkovaného produktu, ale
     * existuje dostatečné množství macerátu, pak systém provede nabalení
     * požadovaného množství.
     */
    public function prepareForSale(int $amount, EmailFactory_Queue $queue) {

      $success = FALSE;

      $onStock = $this->getAmountOnStock();
      $subParts = $this->getSubparts();

      if ($onStock < $amount) {

        $ctg = $this->getCtg();
        switch ($ctg) {

          // v pripade kolkovaneho tabaku se musime pokusit okolkovat
          case Table_TobaccoWarehouse::CTG_TOBACCO_FS:
            // priprava podsoucasti
            if ($subParts) {
              $status = TRUE;
              $subPart = NULL;
              foreach ($subParts as $part) {
                $status = $status && $part->prepareForSale($amount * $part->getAmount(), $queue);
                if ($part->getCtg() == Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
                  $subPart = $part;
                }
              }
              if ($status && $subPart) {
                $batches = $subPart->getAvailableBatches($amount);
                foreach ($batches as $batch => $amount) {
                  try {
                    $this->stamp($subPart->getID(), $batch, $amount);
                    $msg = '[' . $this->getID().  ']: ' . 'OK: Okolkováno ' . $amount . ' množství ' . $this->getName() . '[' . $this->getID() . '] šarží ' . $batch;
                    $queue->add($msg, $queue::TYPE_TODO);
                    $success = TRUE;
                  }
                  catch (Exception $e) {
                    $msg = '[' . $this->getID().  ']: ' . 'Chyba: Nepodařilo se okolkovat ' . $amount . ' množství ' . $this->getName() . '[' . $this->getID() . '] - ' . $e->getMessage();
                    $queue->add($msg, $queue::TYPE_WARNING);
                    $success = FALSE;
                  }
                }
              }
            }
            break;

          // v ostatnich pripadech skoncime, protoze neni dostatek na sklade
          default:
            $msg = '[' . $this->getID().  ']: ' . 'Chyba: Nedostatek součásti ' . $this->getName() . '[' . $this->getID() . '] - chybí ' . ($amount - $onStock) . ' kus(ů)';
            $queue->add($msg, $queue::TYPE_WARNING);
            $success = FALSE;
            break;
        }
      }
      else {
        $success = TRUE;
      }
      return $success;
    }

    public function getAmountOnStock() {
      $tPartsWarehouse = new Table_PartsWarehouse();
      return $tPartsWarehouse->getAmount($this->getID());
    }

    public function getAvailableBatches(int $amount) {
      $ctg = $this->getCtg();
      if ($ctg != Table_TobaccoWarehouse::CTG_TOBACCO_FS && $ctg != Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
        return FALSE;
      }
      $batchArray = array();
      $batches = array_reverse($this->getBatches());
      foreach ($batches as $batch) {
        if ($amount < $batch['amount']) {
          //ochrana při více řádcích stejných šarží
          if (array_key_exists($batch['batch'], $batchArray)) {
            $batchArray[$batch['batch']] += $amount;
          } else {
            $batchArray[$batch['batch']] = $amount;
          }
          $amount = 0;
        }
        else {
          //ochrana při více řádcích stejných šarží
          if (array_key_exists($batch['batch'], $batchArray)) {
            $batchArray[$batch['batch']] += (int) $batch['amount'];
          } else {
            $batchArray[$batch['batch']] = (int) $batch['amount'];
          }
          $amount -= (int) $batch['amount'];
        }
        if ($amount == 0) {
          break;
        }
      }
      return $batchArray;
    }

    public function stamp($subPartId, $batch, $amount) {
      $tWarehouse = new Table_TobaccoWarehouse();
      $tWarehouse->stamp($subPartId, $this->getID(), $batch, $amount);
    }

    public function getFlavor() {
      $ctg = $this->getCtg();
      if ($ctg == Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS) {
        $tFlavors = new Table_PartsFlavors();
        $row = $tFlavors->select()
          ->where('id_parts = ?', $this->getID())
          ->query(Zend_Db::FETCH_OBJ)
          ->fetch();
        if ($row) {
          return $row->id_batch_flavors;
        }
        else {
          return FALSE;
        }
      }
      elseif ($ctg == Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
        try {
          $mix = $this->getMix();
          return $mix['id_flavor'];
        }
        catch (Tobacco_Parts_Exceptions_NoContentException $e) {
          return NULL;
        }
      }
      elseif ($ctg == Table_TobaccoWarehouse::CTG_TOBACCO_FS) {
        if ($subParts = $this->getSubparts()) {
          /** @var \Tobacco_Parts_Part $part */
          foreach ($subParts as $part) {
            if ($part->getCtg() == Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
              return $part->getFlavor();
            }
          }
        }
      }
      return NULL;
    }

    public function isBatchable() {
      $ctg = $this->getCtg();
      $batchable = array(
        Table_TobaccoWarehouse::CTG_TOBACCO,
        Table_TobaccoWarehouse::CTG_TOBACCO_NFS,
        Table_TobaccoWarehouse::CTG_TOBACCO_FS,
      );
      return in_array($ctg, $batchable);
    }

      /**
       * @throws Parts_Exceptions_LowAmount
       * @throws Exception
       */
      public function disassemble($batches, $barrels = null, $nfsToWH = false): array
      {

          if (!is_array($batches) || empty($batches)) {
              throw new RuntimeException('Nebylo zadáno množsví čísel šarží k rozkladu.');
          }

          $totalAmount = 0;
          foreach ($batches as $item) {
              $totalAmount += $item['amount'];
          }
          if (!$totalAmount) {
              throw new RuntimeException('Množství k rozkladu nesmí být nulové.');
          }

          $list = [];
          $parts_warehouse = new Table_TobaccoWarehouse();
          $db = $parts_warehouse->getAdapter();

          try {
              $db->beginTransaction();
              $parts_warehouse->removePart($this->getID(), $totalAmount);

              if ((int) $this->getCtg() === Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
                  if (!is_array($barrels) || empty($barrels)) {
                      $db->rollBack();
                      throw new RuntimeException('Nezvoleny sudy pro rozložení.');
                  }
                  $total = 0;
                  foreach ($barrels as $item) {
                      $total += $item['ratio'];
                  }
                  foreach ($barrels as $item) {
                      $barrel = new Tobacco_Macerations_Maceration($item['id']);
                      $add = $this->getTobbacoAmount() * $totalAmount * $item['ratio'] / $total;
                      $weight = $barrel->getWeight();
                      $tMaceration = new Table_Macerations();
                      $tMaceration->update(['amount_current' => $weight + $add], 'id = ' . $item['id']);
                      $barrel->history(Table_MacerationsHistory::TYPE_BREAK, $add, 'Rozklad produktu ' . $this->getID() . '.');
                      $list[$barrel->getBatchNumber()] = $add . ' kg';
                  }
              }

              // Odmazání vystavených čísel šarží.
              if ((int) $this->getCtg() === Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
                  $partsBatch = new Table_PartsBatch();
                  foreach ($batches as $item) {
                      $partsBatch->turn($this->getID(), null, $item['batch'], $item['amount']);
                  }
              }

              if ($nfsToWH || (int) $this->getCtg() === Table_TobaccoWarehouse::CTG_TOBACCO_FS) {
                  foreach ($this->getSubparts() as $subpart) {
                      $parts_warehouse->insertPart($subpart->getID(), $totalAmount * $subpart->getAmount());
                      $list[$subpart->getID()] = $totalAmount * $subpart->getAmount() . ' ks';
                      if ((int) $subpart->getCtg() === Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
                          $partsBatch = new Table_PartsBatch();
                          foreach ($batches as $item) {
                              $partsBatch->turn($this->getID(), $subpart->getID(), $item['batch'], $item['amount']);
                          }
                      }
                  }
              }
              $db->commit();
              return $list;
          } catch (Exception | Parts_Exceptions_LowAmount  $e) {
              $db->rollBack();
              if ($e instanceof Parts_Exceptions_LowAmount) {
                  throw new Parts_Exceptions_LowAmount('Bylo zadáno příliš vysoké číslo.');
              }

              throw $e;
          }
      }

      /**
       * Je polozka skladu omezena, tzn. je kontrolovana celni spravou.
     * @return bool
     */
    public function isInspected() {
      return TRUE;
      // return in_array($this->getID(), self::INSPECTED_PARTS_ID);
    }

    /**
     * Vraci aktualni mnozstvi nebo mnozstvi k danemu datu na sklade
     * a to v kusech nebo gramech prip. v kilech.
     * V databazi je totiz mnostvi v kusech/gramech (integer).
     */
    public function getWHAmount(Meduse_Date $date = NULL, $unit = self::UNIT_DEFAULT) {
      $tPartsWarehouse = new Table_PartsWarehouse();
      $amount = (int) $tPartsWarehouse->getAmount($this->getID(), FALSE, $date);
      if ($this->getMeasure() === self::WH_MEASURE_UNIT || $unit === self::UNIT_DEFAULT || $unit === self::UNIT_GRAMS) {
        return $amount;
      }
      else {
        return $amount / 1000;
      }
    }
    public function setCriticalAmount($amount) {
      $amount = $this->getMeasure() == 'weight' ? $amount * 1000 : $amount;
      parent::setCriticalAmount($amount);
    }

    public function getCriticalAmount() {
      $amount = parent::getCriticalAmount();
      return ($this->getMeasure() == 'weight' ? $amount / 1000 : $amount);
    }

    public function delete(): array {

      $parts_warehouse = new Table_PartsWarehouse();

      // Kontrola, zda-li neni soucast na sklade.
      if ($parts_warehouse->getAmount($this->_part_row->id) > 0) {
        $msg = 'Součást ID = ' . $this->getID() . ' nelze smazat, protože je naskladněná.';
        throw new Parts_Exceptions_OnStock($msg);
      }

      // Kontrola, zda-li neni soucast v rezervacich.
      if ($this->checkReservations()) {
        $msg = 'Součást ID = ' . $this->getID() . ' nelze smazat, protože je rezervovaná.';
        throw new Parts_Exceptions_InReservations($msg);
      }

      // Produkty nemuzeme odstranit z nadproduktu, protoze by se nam ztratily
      // vazby potrebne k sesaveni reportu prodeju.
      if ($this->isTobaccoFS() || $this->isTobaccoNFS()) {
        if ($sup_parts = $this->getSupParts(FALSE)) {
          $sups = [];
          foreach ($sup_parts as $item) {
            $sups[] = $item['id_multipart'];
          }
          $msg = 'Produkt je součástí těchto produktů: ' . implode(', ', $sups);
          throw new Tobacco_Parts_Exceptions_SupPartsExist($msg);
        }
      }
      else {
        // Odstranime pripadne vazby obaloveho materialu, na nadsoucasti.
        $this->_checkMultipartsAbove();
      }

      $parts_subparts = new Table_PartsSubparts();
      $parts = new Table_Parts();
      $db = Zend_Registry::get('db');

      try {
        $db->beginTransaction();
        $affectedOrders = $this->removePartFromActiveOrders($db);
        if (!$this->isTobacco() && !$this->isTobaccoNFS()) {
          // V pripade, ze se nejedna o tabak odstranime zapis v tabulce skladu
          $parts_warehouse->delete("id_parts LIKE '" . $this->_part_row->id . "'");
          // a odstranime veskere vazby na podsoucasti.
          $parts_subparts->delete("id_multipart LIKE '" . $this->_part_row->id . "'");
        }
        $parts->update(['deleted' => 'y'], "id = '" . $this->getID() . "'");
        $db->commit();
      }
      catch (Exception $e) {
        $db->rollback();
        throw new Exception("Součást " . $this->_part_row->id
          . " se nepodařilo odstranit. " . $e->getMessage());
      }
      return $affectedOrders;
    }

    /**
     * Vrati jednotkovou cenu za kus nebo za kilogram resp. za gram.
     * V databazi jsou ceny za kus nebo za kilogram.
     */
    public function getUnitPrice($unit = self::UNIT_DEFAULT) {
      $price = $this->getPrice(FALSE);
      if ($this->getMeasure() === self::WH_MEASURE_UNIT || $unit === self::UNIT_KILOGRAMS || $unit === self::UNIT_DEFAULT) {
        return (float) $price;
      }
      return (float) $price / 1000;
    }

    /**
     * Testuje, zda-li je mozno prepocitat vyrovni naklady soucasti.
     * Ve WH tabaku to lze u soucasti, ktere maji podsoucasti.
     * @return bool
     */
    public function reCalculatingAllowed() {
      return $this->hasSubParts() || $this->isTobaccoNFS();
    }

    /**
     * Aktualizuje cenu vsem soucastem, ve kterych figuruje dana soucast
     */
    public function refreshPrice() {

      // Aktualizace ceny soucasti - prepocet z jeden level niz.
      $this->computePrice();

      // Aktualizace ceny vsech soucasti, ve kterych je dana soucast obsazena.
      $rows = $this->getSupParts();
      if (count($rows) > 0){
        foreach ($rows as $row) {
          $superPart = new Parts_Part($row['id_multipart']);
          $superPart->refreshPrice();
        }
      }
    }

    public function computePrice() {

      // Pokud je to soucast, ktera nema podsoucasti, neni co pocitat
      // Vratime cenu zadanou primo v DB.
      if (!$this->reCalculatingAllowed()) {
        return $this->getUnitPrice();
      }

      $price = 0;

      // Scitame pouze prvni level podsoucasti!
      if ($this->isMultipart()) {
        $table = new Table_PartsSubparts();
        $rows = $table->getAllSubParts($this->getID());
        foreach($rows as $row){
          $subpart = new Tobacco_Parts_Part($row['id_parts']);
          $price += $subpart->getUnitPrice() * $row['amount'];
        }
      }

      // Pokud je to nekolkovany tabak,
      // pricteme jeste cenu obsazeneho maceratu.
      if ($this->isTobaccoNFS()) {
        /** @var \Tobacco_Mixes_Mix $mix */
        $mix = $this->getMix(TRUE);
        $tobacco_unit_price = $mix->getUnitPrice();
        $tobacco_amount = $this->getTobbacoAmount();
        $price += ($tobacco_unit_price * $tobacco_amount);
      }

      $this->_part_row->price = $price;
      $this->_part_row->save();

      return $price;
    }

    public function restore() {
      if (!$this->isDeleted()) {
        return false;
      }
      else {
        $id = $this->getID();
        $whTable = new Table_TobaccoWarehouse();
        if ($whTable->restore($id)) {
          $partTable = new Table_Parts();
          $partTable->update(['deleted' => 'n'], 'id = ' . $partTable->getAdapter()->quote($id));
          return true;
        }
        else {
          return false;
        }
      }
    }

    /**
     * @param $id
     * @param $newId
     * @param $newName
     * @param $newDescription
     * @return void
     * @throws Parts_Exceptions_IdAlreadyExists
     * @throws Parts_Exceptions_IdNotExists
     * @throws Zend_Db_Adapter_Exception
     * @throws Zend_Db_Table_Exception
     * @throws Zend_Db_Statement_Exception
     */
    public function saveAs($id, $newId, $newName, $newDescription) {
      parent::saveAs($id, $newId, $newName, $newDescription);
      $tPartsMixes = new Table_PartsMixes();
      $row = $tPartsMixes->select()->where('id_parts = ?', $id)->query(Zend_Db::FETCH_ASSOC)->fetch();
      if ($row) {
        unset($row['id']);
        $row['id_parts'] = $newId;
        $tPartsMixes->insert($row);
      }
    }
  }

