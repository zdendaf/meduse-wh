<?php

  class Tobacco_Parts_StampForm extends Meduse_FormBootstrap {
    
    public function init() {
      
      $this->setAttrib('class', 'form');
      $this->setLegend('Okolkovat tabák');
      $this->setAction('/tobacco/parts-stamp-post');
      
      $element = new Meduse_Form_Element_Select('id_multipart');
      $element->setLabel('Naskladnit: ');
      $element->setRequired(true);
      $element->setOrder(100);
      $this->addElement($element);
      
      $element = new Zend_Form_Element_Hidden('id_part');
      $element->setRequired(true);
      $element->setOrder(1);
      $this->addElement($element);
      
      $element = new Zend_Form_Element_Hidden('id_order');
      $element->setRequired(false);
      $element->setOrder(2);
      $this->addElement($element);
      
      //požadované množství k okolkování
      $element = new Zend_Form_Element_Hidden('req_amount');
      $element->setRequired(false);
      $element->setOrder(3);
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Submit('Okolkovat');
      $element->setOrder(900);
      $this->addElement($element);
      
      parent::init();
    }
    
    public function setPartId($partId) {
      $this->getElement('id_part')->setValue($partId);
    }

    public function setOrderId($orderId) {
      $this->getElement('id_order')->setValue($orderId);
    }
    
    public function setSupParts($supParts) {
      if ($supParts) {
        $element = $this->getElement('id_multipart');
        foreach ($supParts as $item) {
          $part = new Parts_Part($item['id_multipart']);
          $element->addMultiOption($part->getID(), $part->getName());
        }
      }
    }
    
    public function setBatches($batches, $maxAmount = PHP_INT_MAX) {
      if ($batches) {
        $it = 0;
        foreach ($batches as $item) {
          $element = new Zend_Form_Element_Hidden('batch_number_' . $it);
          $element->setValue($item['batch']);
          $element->setOrder(500 + 2*$it);
          $this->addElement($element);
          $element = new Zend_Form_Element_Text('batch_amount_' . $it);
          $element->setLabel($item['batch']);
          $element->addValidator(new Zend_Validate_LessThan($item['amount']));
          $element->addValidator(new Zend_Validate_Int());
          $itemAmount = min($maxAmount, $item['amount']);
          $maxAmount -= $itemAmount;
          $element->setValue($itemAmount);
          $element->setOrder(501 + 2*$it);
          $this->addElement($element);
          $it++;
        }
      }
    }
    
    /**
     * Přidává hodnotu požadovaného množství
     * 
     * @param int $amount Požadované množství
     */
    public function setRequestedAmount($amount) {
      $element = $this->getElement('req_amount');
      $element->setValue((int)$amount);
    }
    
  }

