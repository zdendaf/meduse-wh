<?php

class Tobacco_EmailTemplates_SendmailForm extends EmailTemplates_SendmailForm {
  protected $id_wh_type = 2;


  public function init() {
    parent::init();
    $this->setAction('/tobacco-orders/notify/');
  }
}
