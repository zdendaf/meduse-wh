<?php

  class Tobacco_Orders_Form extends Orders_Form {

    protected int $wh = Parts_Part::WH_TYPE_TOBACCO;

    public function init() {

      parent::init();

      $this->getElement('meduse_orders_no')->setAttrib('maxlength', '8');

      $this->removeFeesElements();

      // nejsou potreba
      $this->removeElement('meduse_orders_deadline');
      $this->removeElement('meduse_orders_deadline_internal');
      $this->removeElement('meduse_coo_requirement');
      $this->removeElement('meduse_invoice_verify_requirement');
      $this->removeElement('meduse_di');

      // pouze nova, otevrena a expedovana
      $this->removeElement('meduse_orders_status');
      $element = new Zend_Form_Element_Hidden('meduse_orders_status');
      $this->addElement($element);

      $element = $this->getElement('meduse_orders_carrier');
      $element->removeMultiOption(Table_OrdersCarriers::UNDEFINED);
      $options = array(NULL => '-- neurčen --') + $element->getMultioptions();
      $element->setMultiOptions($options);

      $element = new Meduse_Form_Element_Checkbox('default_price');
      $element->setLabel('Výchozí cena');
      $element->setValue('y');
      $this->addElement($element);

    }
  }

