<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Tobacco_Orders_ExpeditionForm extends Meduse_FormBootstrap {
    
    public function init() {
      
      $this->setMethod(Zend_Form::METHOD_POST);
      $this->setAttrib('class', 'form');
      $this->setLegend('Expedice objednávky');
      
      $element = new Zend_Form_Element_Hidden('order_id');
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_DatePicker('expedition_date');
      $element->setOptions(array(
        'jQueryParams' => array(
          'dateFormat' => 'dd.mm.yy')
       ));
      $element->setLabel('Datum expedice');
      $element->setRequired(true);
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Text('tracking_number');
      $element->setLabel('Tracking No.');
      $element->setAttrib('size', '10');
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Submit('Uložit');
      $this->addElement($element);
      
      parent::init();
    }
    
    public function populate(array $values) {
      parent::populate($values);
      $date = new Zend_Date($values['expedition_date'], 'yyyy-MM-dd');
      $this->getElement('expedition_date')->setValue($date->toString('dd.MM.yyyy'));
    }
  }

  