<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Tobacco_Orders_Order extends Orders_Order {

    const DATA_UPLOADS_SUBDIR_PREFIX = 'mdt_order_';

    public function getTobbacoAmount() {
      $amount = 0;
      $table = new Table_OrdersProducts();
      $select = $table->select()
        ->setIntegrityCheck(false)
        ->from('orders_products', array(
          'id_products', 'amount', 'free_amount'))
        ->where('id_orders = ?', $this->getID());

      $parts = $select->query()->fetchAll();
      if ($parts) {
        foreach ($parts as $row) {
          $part = new Tobacco_Parts_Part($row['id_products']);
          $amount += ((int)$row['amount'] + (int)$row['free_amount']) * $part->getTobbacoAmount();
        }
      }
      return $amount;
    }


    public function processExpedition($params = array()){
      if ($this->isReady()){

        $db = Zend_Registry::get('db');

        $orders = new Table_Orders();
        $orders_products = new Table_OrdersProducts();
        $orders_products_parts = new Table_OrdersProductsParts();
        $orders_producs_rows = $orders_products->fetchAll("id_orders = ".$this->_order->id);


        $id_orders_products_parts = '(';
        foreach($orders_producs_rows as $row){
          $id_orders_products_parts .= $row->id.',';
        }
        $id_orders_products_parts = substr($id_orders_products_parts, 0, -1); //odstraneni posledni carky
        $id_orders_products_parts .= ')';

        $db->beginTransaction();
        try{
          if (!$this->isService() && count($orders_producs_rows) > 0){ //pokud existuji polozky
            $orders_products_parts->delete("id_orders_products IN " . $id_orders_products_parts);
          }

          $data = array(
            'date_exp' => isset($params['date_exp'])?
              $params['date_exp'] : new Zend_Db_Expr("NOW()"),
            'status' => 'closed'
          );
          if ($params['tracking_no']) {

            $tableTracking = new Table_OrdersTrackingCodes();
            $row = $tableTracking
              ->select()
              ->where('id_orders = ?', $this->getID())
              ->query(Zend_Db::FETCH_ASSOC)
              ->fetch();
            if ($row) {
              $tableTracking->update(['code' => trim($params['tracking_no'])],
                'id = ' . $row['id']);
            }
            else {
              $tableTracking->insert([
                'code' => trim($params['tracking_no']),
                'id_orders' => $this->getID(),
              ]);
            }
          }
          $orders->update($data, "id = " . $this->getID());
          $db->commit();
        }
        catch (Exception $e) {
          $db->rollback();
          throw new Exception ($e->getMessage(), $e->getCode());
        }

      } else {
        throw new Exception ("Nelze expedovat nehotovou objednavku.", 2);
      }
    }

    public function isReady() {
      if ($this->isService()) {
        return TRUE;
      }
      $orders_products = new Table_OrdersProducts();
      $select = $orders_products->select();
      $select->where("id_orders = ?", $this->_order->id);
      $rows = $orders_products->fetchAll($select);
      if ($this->getStatus() != Table_Orders::STATUS_CLOSED) {
        foreach($rows as $row) {
          if($row->amount > $this->getReadyProductAmount($row->id_products)){
            return FALSE;
          }
        }
        return (count($rows) > 0);
      }
      else {
        return TRUE;
      }
    }

    public function addProduct($partId, $amount = 1, $freeAmount = 0) {

      $table = new Table_OrdersProducts();
      $db = $table->getAdapter();

      $select = $table->select()
        ->setIntegrityCheck(false)
        ->where('id_orders = ?', $this->getID())
        ->where('id_products = ?', $partId);
      // existuje jiz produkt v objednavce
      $result = $select->query()->fetch();
			if ($result) {
        // update
        if ($amount > 0 || $freeAmount > 0) {
          $data = array(
            "amount" => $amount + $result['amount'] + $freeAmount + $result['free_amount'],
            "free_amount" => $freeAmount + $result['free_amount'],
          );
          $table->update($data, "id_orders =  " . $db->quote($this->getID())
            . " AND id_products = " . $db->quote($partId));
        }
      // jinak vkladam
			} elseif ($amount > 0 || $freeAmount > 0){
				$data = array(
					"id_orders" => $this->getID(),
					"id_products" => $partId,
					"amount" => $amount + $freeAmount,
					"free_amount" => $freeAmount,
				);
				$table->insert($data);
			}
    }

    public function removeProduct($partId) {
      $table = new Table_OrdersProducts();
      $db = $table->getAdapter();
      $table->delete("id_orders = " . $db->quote($this->getID())
        . " AND id_products = " . $db->quote($partId));
    }


    public function reserveAll($idProducts, $batchArray = array()){
      $db = Zend_Registry::get('db');
      $db->beginTransaction();

      $amountSum = 0;
      $productAmount = $this->getProductAmount($idProducts);

      if ($batchArray) {
        $table = new Table_OrdersProductsBatch();
        $tablePB = new Table_PartsBatch();
        foreach ($batchArray as $batch => $amount) {
          $amountSum += $amount;
          $table->addBatch($this->getID(), $idProducts, $batch, $amount);
          $tablePB->turn($idProducts, null, $batch, $amount);
        }
        if ($productAmount != $amountSum) {
          $db->rollback();
          throw new Exception("Požadované množství šarží neodpovídá množství produktu $idProducts v objednavce.");
        }
      }
      if($productAmount > 0){
        $this->reserveProduct($idProducts, $productAmount);
      } else {
        $db->rollback();
        throw new Exception("Produkt $idProducts neni v objednavce.");
      }
      $db->commit();
    }

    public function cancelPrepared($id_products, $id_parts){

      $parts_warehouse = new Table_PartsWarehouse();
      $db = Zend_Registry::get('db');

      $select = $db->select();
      $select->from(array('op' => 'orders_products'), array('id'))
      ->joinLeft(array('opp' => 'orders_products_parts'),
        "op.id = opp.id_orders_products",
        array('opp.id', 'opp.amount', 'opp.id_parts'))
      ->where("op.id_orders = ?", $this->_order->id)
      ->where("op.id_products = ?", $id_products)
      ->where("opp.id_parts = ?", $id_parts);
      $row = $db->fetchRow($select);
      if ($row) {
        $parts_warehouse->insertPart($id_parts, $row['amount']);
        $db->delete("orders_products_parts","id = ".$row['id']);
      }

      $select = $db->select()
        ->from(array('op' => 'orders_products'), array())
        ->joinLeft(array('opb' => 'orders_products_batch'),
          "op.id = opb.id_orders_products",
          array('op.id', 'opb.amount', 'opb.batch'))
        ->where("op.id_orders = ?", $this->_order->id)
        ->where("op.id_products = ?", $id_products);
      $rows = $db->fetchAll($select);
      if ($rows) {
        $tableOPB = new Table_OrdersProductsBatch();
        $tablePB = new Table_PartsBatch();
        foreach ($rows as $row) {
          if (!is_null($row['batch']) && !is_null($row['amount'])) {
            $tableOPB->delete('id_orders_products = ' . $row['id']);
            $tablePB->turn(null, $id_products, $row['batch'], $row['amount']);
          }
        }
      }
    }

    public function cancelAllPrepared($id_products){
      $db = Zend_Registry::get('db');
      $db->setFetchMode(Zend_Db::FETCH_ASSOC);
      $select = $db->select();
      $select->from(array('op' => 'orders_products'), array())
      ->join(array('opp' => 'orders_products_parts'), "op.id = opp.id_orders_products",
        array('opp.id', 'opp.id_orders_products', 'opp.amount', 'opp.id_parts'))
      ->where("op.id_orders = ?", $this->_order->id)
      ->where("op.id_products = ?", $id_products);
      if($rows = $db->fetchAll($select)){
        foreach($rows as $row){
          $this->cancelPrepared($id_products, $id_products);
        }
      }
      return;
    }

    public function delete(): void
    {

      $db = Zend_Registry::get('db');
      $db->beginTransaction();

      $ordersProduct = new Table_OrdersProducts();
      $select = $ordersProduct->select()
        ->setIntegrityCheck(false)
        ->from($ordersProduct, array('id_products'))
        ->where('id_orders = ?', $this->getID());
      $rows = $select->query()->fetchAll();

      if ($rows) {
        foreach($rows as $row) {
          $this->cancelAllPrepared($row['id_products']);
        }
      }

      $orders = new Table_Orders();
      $data = array(
        'status' => 'trash', 'date_delete' => date("Y-m-d"),
      );
      $orders->update($data, "id = " . $this->_order->id);

      $db->commit();
    }
    /**
     * Vraci na sklad zbozi expedovane objednavky a nastavuje ji jako zavrenou
     * Ma smysl u expedovanych objednavek, ktere byly doruceny na dobirku
     */
    public function returnFromClosed() {

      // vrati stav
      if ($this->_order->status == 'closed') {


        $db = Zend_Db_Table::getDefaultAdapter();
        $db->beginTransaction();

        $select = new Zend_Db_Select($db);
        $select->from('orders_products')->where('id_orders = ?', $this->_order->id);
        $products = $select->query()->fetchAll();
        $wh = new Table_PartsWarehouse();
        foreach ($products as $product) {
          $wh->insertPart($product['id_products'], $product['amount']);
        }

        $select = new Zend_Db_Select($db);
        $select
          ->from(array('op' => 'orders_products'), array('id', 'id_products'))
          ->join(array('opb' => 'orders_products_batch'),
            "op.id = opb.id_orders_products",
            array('opb.amount', 'opb.batch'))
          ->where("op.id_orders = ?", $this->_order->id);
        $rows = $db->fetchAll($select);
        if ($rows) {
          $tableOPB = new Table_OrdersProductsBatch();
          $tablePB = new Table_PartsBatch();
          foreach ($rows as $row) {
            $tableOPB->delete('id_orders_products = ' . $row['id']);
            $part = new Tobacco_Parts_Part($row['id_products']);
            if ($part->isBatchable()) {
              $tablePB->turn(null, $row['id_products'], $row['batch'], $row['amount']);
            }
          }
        }
        $db->commit();
        $this->_order->status = 'open';
        $this->_order->save();
      }
    }

    public function validateOrderStatus($id, $status) {
      if (!$id) {
        throw new Exception('Nebylo zadáno ID objednávky');
      } else {
        $order = new Orders_Order($id);

        if ($order->getStatus() != $status) {
          throw new Exception('Objednávka není ve stavu "' . Table_Orders::$statusToString[$status] . '".');
        }
      }
      return TRUE;
    }

    /**
     * Příprava všech produktů v objednávce pro prodej.
     *
     * @return array
     */
    public function prepareAllForSale() {
      $messages = new EmailFactory_Queue();
      $orders = new Table_Orders();
      $data = $orders->getOrderParts($this->getID());
      foreach ($data as $category) {
        foreach ($category as $d) {
          $part = new Tobacco_Parts_Part($d['id']);
          $amount = $d['amount'] - $d['ready_amount'];
          if ($amount > 0) {
            $this->prepareForSale($part, $amount, $messages);
          }
        }
      }
      return $messages;
    }

    /**
     * Příprava produktu pro prodej.
     *
     * @param Tobacco_Parts_Part $part
     * @param type $amount
     * @param EmailFactory_Queue $queue
     */
    public function prepareForSale(Tobacco_Parts_Part $part, $amount, EmailFactory_Queue $queue) {
      $success = $part->prepareForSale($amount, $queue);
      if ($success) {
        $batchArray = $this->selectBatches($part, $amount);
        try {
          $this->reserveAll($part->getID(), $batchArray);
          $message = '[' . $part->getID().  ']: ' . "Všechny součásti produktu " . $part->getID() . " byly rezervovány a vyřazeny ze skladu.";
          $queue->add($message, $queue::TYPE_INFO);
        } catch (Parts_Exceptions_LowAmount $e) {
          $message = '[' . $part->getID().  ']: ' . "Na skladu neni dostatek součásti " . $e->getMessage();
          $queue->add($message, $queue::TYPE_WARNING);
          $success = FALSE;
        } catch (Exception $e) {
          $message = '[' . $part->getID().  ']: ' . "Nepodařilo se rezervovat všechny součásti produktu. " . $e->getMessage();
          $queue->add($message, $queue::TYPE_WARNING);
          $success = FALSE;
        }
      }
      return $success;
    }

    /**
     * Automatická volba šarží pro produkt.
     *
     * @param Tobacco_Parts_Part $part
     * @param type $amount
     * @return type
     */
    public function selectBatches(Tobacco_Parts_Part $part, $amount) {

      $batchArray = array();

      //práce s prodejným tabákem
      if ($part->getCtg() == Table_TobaccoWarehouse::CTG_TOBACCO_FS ||
        $part->getCtg() == Table_TobaccoWarehouse::CTG_TOBACCO_NFS) {
        $batches = array_reverse($part->getBatches());

        foreach ($batches as $batch) {
          if ($amount < $batch['amount']) {
            //ochrana při více řádcích stejných šarží
            if (array_key_exists($batch['batch'], $batchArray)) {
              $batchArray[$batch['batch']] += $amount;
            } else {
              $batchArray[$batch['batch']] = $amount;
            }
            $amount = 0;
          }
          else {
            //ochrana při více řádcích stejných šarží
            if (array_key_exists($batch['batch'], $batchArray)) {
              $batchArray[$batch['batch']] += (int) $batch['amount'];
            } else {
              $batchArray[$batch['batch']] = (int) $batch['amount'];
            }

            $amount -= (int) $batch['amount'];
          }
          if ($amount == 0) {
            break;
          }
        }
      }
      return $batchArray;
    }

    /**
     * Otestuje, jestli může být k objednávce vystavená regular faktura.
     * K objednavce lze vystavit ostrou fakturu v techto pripadech:
     *  - kdyz ma zakaznika, ktery ma dovolenou platbu na fakturu
     *  - kdyz ma k tomu uzivatel opravneni
     *  - kdyz je platba v hotovosti, dobirkou, pres PayPal nebo pres Pays
     *  - kdyz je uhrazena zalohova faktura
     * @return bool
     */
    public function checkRegularCondition(): bool
    {
      $check = FALSE;
      if ($customer = $this->getCustomer(TRUE)) {
        $check1 = $customer->isAllowedInvoicing();
        $check2 = in_array($this->getPaymentMethod(), array(Table_Orders::PM_CASH, Table_Orders::PM_CASH_ON_DELIVERY, Table_Orders::PM_PAYPAL, Table_Orders::PM_PAYSCZ), true);
        $check3 = !empty($this->getDatePayDeposit());
        $check4 = Utils::isAllowed('tobacco-customers/set-allow-invoicing');
        $check = $check1 || $check2 || $check3 || $check4;
      }
        return $check;
    }

    /**
     * Vrátí výchozí typ faktury podle stavu objenávky.
     * @return string|void
     * @throws Exception
     */
    public function getDefaultInvoiceType() {
      switch ($this->getStatus()) {
        case Table_Orders::STATUS_OFFER:
          return Table_Invoices::TYPE_OFFER;
          break;
        case Table_Orders::STATUS_NEW:
        case Table_Orders::STATUS_CONFIRMED:
          return Table_Invoices::TYPE_PROFORM;
          break;
        case Table_Orders::STATUS_OPEN:
        case Table_Orders::STATUS_CLOSED:
          return $this->checkRegularCondition() ? Table_Invoices::TYPE_REGULAR : Table_Invoices::TYPE_PROFORM;
          break;
        default:
          // HOTFIX. @todo: co se smazanou objednavkou?
          return Table_Invoices::TYPE_OFFER;
          // throw new Exception('K objednávce nelze generovat fakturu, protože její status je "' . $this->_order->getStatus() . '".');
      }
    }

    public function getPackagesSum() {
      $sum = NULL;
      if ($products = $this->getProducts()) {
        foreach ($products as $item) {
          $part = new Tobacco_Parts_Part($item['id_products']);
          if (in_array($part->getCtg(), array(Table_TobaccoWarehouse::CTG_TOBACCO_NFS, Table_TobaccoWarehouse::CTG_TOBACCO_FS))){
            $in = $part->getTobbacoAmount() * 1000;
            if (!isset($sum[$in])) {
              $sum[$in] = 0;
            }
            $part->isTobaccoNFS();
            $amount = (int) $item['amount'] + (int) $item['free_amount'];
            $sum[$in] += $amount;
          }
        }
      }
      return $sum;
    }

    public function fileDelete($fileId) {
      $table = new Table_OrdersFiles();
      try {
        $dir = APPLICATION_PATH . self::DATA_UPLOADS_DIR . self::DATA_UPLOADS_SUBDIR_PREFIX . $this->getID();
        $info = $table->find($fileId)->current()->toArray();
        if (unlink($dir . '/' . $info['filename'])) {
          $table->delete('id = ' . $fileId);
          return TRUE;
        }
        else {
          return FALSE;
        }
      }
      catch (Exception $e) {
        return FALSE;
      }
    }
  }

