<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 3.9.2018
 * Time: 19:34
 */

class Tobacco_Orders_ClipboardForm extends Meduse_FormBootstrap {

  public function init() {

    $this->setAttrib('class', 'form');
    $this->setLegend('Nová objednávka ze schránky');

    $jsonElement = new Meduse_Form_Element_Textarea('json');
    $jsonElement->setLabel('Data z eshopu');
    $jsonElement->setRequired(TRUE);
    $this->addElement($jsonElement);

    $submitElement = new Meduse_Form_Element_Submit('submit');
    $submitElement->setLabel('Vytvořit objednávku');
    $this->addElement($submitElement);

    parent::init();
  }

  public function setJsonData($data) {
    $this->getElement('json')->setValue($data);
  }

}