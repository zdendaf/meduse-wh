<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Tobacco_Orders_Filter extends Meduse_FormBootstrap {

    public function init() {

      $this->setAttrib('class', 'form');
      $this->setMethod(Zend_Form::METHOD_POST);
      $this->setLegend('Filtrace objednávek');

      $element = new Meduse_Form_Element_Text('filter_no');
      $element->setValue(Zend_Registry::get('tobacco_orders')->filter_no);
      $element->setLabel('Číslo');
      $element->addValidator(new Zend_Validate_Int());
      $this->addElement($element);

      $element = new Meduse_Form_Element_MultiCheckbox('filter_status');
      $element->setValue(Zend_Registry::get('tobacco_orders')->filter_status);
      $element->setLabel('Stav');
      $element->addMultiOptions(array(
        Table_Orders::STATUS_NEW => 'vložená',
        Table_Orders::STATUS_OPEN => 'v přípravě',
        Table_Orders::STATUS_CLOSED => 'expedovaná',
        Table_Orders::STATUS_TRASH => 'zrušená'
      ));
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('filter_title');
      $element->setValue(Zend_Registry::get('tobacco_orders')->filter_title);
      $element->setLabel('Název');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Currency('filter_expected_payment');
      $element->setValue(Zend_Registry::get('tobacco_orders')->filter_expected_payment);
      $element->setLabel('Cena');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('filter_tracking_no');
      $element->setValue(Zend_Registry::get('tobacco_orders')->filter_tracking_no);
      $element->setLabel('Tracking no.');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('filter_invoice_no');
      $element->setValue(Zend_Registry::get('tobacco_orders')->filter_invoice_no);
      $element->setLabel('Faktura');
      $this->addElement($element);

      $element = new Meduse_Form_Element_MultiCheckbox('filter_paid');
      $element->setValue(Zend_Registry::get('tobacco_orders')->filter_paid);
      $element->setLabel('Úhrada');
      $element->addMultiOptions(array(
        'n' => 'nezaplacené',
        'y' => 'zaplacené',
      ));
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('Filtrovat');
      $element->setAttrib('class', 'btn btn-primary');
      $this->addElement($element);

      parent::init();
    }

  }

