<?php

class Tobacco_Pricelists_Form extends Meduse_Form {

  public function __construct($style, $description) {

    $this->setMethod(Zend_Form::METHOD_POST);
    $this->setAttrib('class', 'form');
    $this->setName('pricelists_form');
    $this->setLegend('Vložení / editace ceníku');

    $element = new Zend_Form_Element_Hidden('id');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('name');
    $element->setRequired(TRUE);
    $element->setLabel('Název ceníku');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('abbr');
    $element->setRequired(TRUE);
    $element->setLabel('Zkratka ceníku');
    $element->setAttrib('maxlength', '5');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Select('type');
    $element->setRequired(TRUE);
    $element->setLabel('Pro zákazníky typu');
    $element->addMultiOptions([
      Table_Pricelists::TYPE_B2B => 'Prodejce / Distributor',
      Table_Pricelists::TYPE_B2C => 'Koncový spotřebitel',
    ]);
    $this->addElement($element);

    $element = new Meduse_Form_Element_DatePicker('valid');
    $element->setRequired(TRUE);
    $element->setLabel('Platnost od');
    $element->setOptions([
      'jQueryParams' => [
        'dateFormat' => 'dd.mm.yy',
      ],
    ]);
    $element->addValidator(new Zend_Validate_Date('d.m.y'));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('default');
    $element->setLabel('Nastavit jako výchozí');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('export');
    $element->setLabel('Exportovat');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Select('assign');
    $element->setLabel('Přiřadit zákazníky');
    $element->addMultiOption('', 'Žádné');
    $element->addMultiOption('all', 'Všechny');
    $this->addElement($element);

    parent::__construct($style, $description);
  }

}