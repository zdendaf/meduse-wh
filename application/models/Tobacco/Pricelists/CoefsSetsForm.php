<?php


/**
 * Description of CoefsForm
 *
 * @author Vojtěch Mrkývka
 */
class Tobacco_Pricelists_CoefsSetsForm extends Meduse_FormBootstrap {
  public function init() {
    $this->setName('coefset');
    $this->setAttrib('class', 'form');
    $this->setMethod(Zend_Form::METHOD_POST);
    
    $this->setAction('/tobacco-pricecoefs/sets-save/');
    
    $element = new Zend_Form_Element_Hidden('id');
    $this->addElement($element);
    
    $element = new Meduse_Form_Element_Text('name');
    $element->setLabel('Název skupiny');
    $element->setRequired(true);
    $element->addValidator('stringLength', false, array(0, 50));
    $this->addElement($element);
    
    $element = new Meduse_Form_Element_Submit('Uložit');
    $this->addElement($element);
    
    parent::init();
  }
}

?>
