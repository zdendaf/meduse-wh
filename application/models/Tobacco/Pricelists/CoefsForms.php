<?php
  
/**
 * Description of CoefsForm
 *
 * @author Vojtěch Mrkývka
 */
class Tobacco_Pricelists_CoefsForms extends Meduse_FormBootstrap {
  public function init() {
    $this->setName('coefset');
    $this->setAttrib('class', 'form');
    $this->setMethod(Zend_Form::METHOD_POST);
    
    $this->setAction('/tobacco-pricecoefs/save/');
    
    $element = new Zend_Form_Element_Hidden('id');
    $this->addElement($element);
    
    $element = new Meduse_Form_Element_Text('point');
    $element->setLabel('Dolní limit [kg]');
    $element->setRequired(true);
    $element->addValidator('float');
    $this->addElement($element);
    
    $tPricelists = new Table_Pricelists();
    $pl_b2b = $tPricelists->getAllPricelists(
      Table_TobaccoWarehouse::WH_TYPE, 
      Table_Pricelists::TYPE_B2B
    );
    $pl_b2c = $tPricelists->getAllPricelists(
      Table_TobaccoWarehouse::WH_TYPE, 
      Table_Pricelists::TYPE_B2C
    );
    
    $element = new Meduse_Form_Element_Select('pricelist_b2b');
    $element->setLabel('B2B ceník');
    $element->setRequired(true);
    foreach ($pl_b2b as $item) {
      $element->addMultiOption($item['id'], $item['name']);
    }
    $this->addElement($element);
        
    $element = new Meduse_Form_Element_Select('pricelist_b2c');
    $element->setLabel('B2C ceník');
    $element->setRequired(true);
    foreach ($pl_b2c as $item) {
      $element->addMultiOption($item['id'], $item['name']);
    }
    $this->addElement($element);
    
    $element = new Meduse_Form_Element_Submit('Uložit');
    $this->addElement($element);
    
    parent::init();
  }

  public function populate(array $values) {
    parent::populate($values);      
    $this->getElement('pricelist_b2b')->setValue($values['id_b2b']);
    $this->getElement('pricelist_b2c')->setValue($values['id_b2c']);
  }
}
