<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Tobacco_Invoice_Invoice extends Invoice_Invoice {

    public function __construct($invoiceId = null){
      if (is_null($invoiceId)) {
        return;
      }
      $invoices = new Table_Invoices();
      if (!($row = $invoices->find($invoiceId)->current())) {
        throw new Exception("Neexistuje faktura s ID = '$invoiceId'.");
      }
      $this->_row = $row;
      $this->_order = new Tobacco_Orders_Order($this->_row->id_orders);
      $this->_customer = $this->_order->getCustomer();       ;

      // TODO: tento test odstranit po kontrole, ze zadna faktura nema
      // hodnotu NULL ve sloupci id_pricelists. Je potreba upravit i databazi.
      if (is_null($this->_row->id_pricelists)) {
        $tPricelist = new Table_Pricelists();
        $pricelist_id = $tPricelist->getCustomersValidPricelist($this->_customer->id);
        if (is_null($pricelist_id)) {
          throw new Exception('Není přiřazen platný ceník');
        }
        $this->_row->id_pricelists = $pricelist_id;
        $invoices->update(array('id_pricelists' => $pricelist_id), 'id = ' . $invoiceId);
      }

      $this->generateProductRows();
      $this->generateCustomRows();

      $this->setVersions();
    }

    public function getTobaccoAmount() {
      return $this->_order->getTobbacoAmount();
    }

    public function getCustomerSeedId() {
      return $this->_customer->seed_id;
    }

    /**
     * Vraci vysi spotrebni dane.
     *
     * V pripade expedovane objednavky se bere v potaz datum expedice.
     * V ostatnich pripadech se bere datum vystaveni faktury.
     *
     * @return int
     */
    public function getExciseDutyRate() {
      $tExcises = new Table_ApplicationExcise();
      $date = $this->getOrder()->getStatus() == Table_Orders::STATUS_CLOSED ? $this->getOrder()->getDateExp() : $this->getIssueDate();
      return $tExcises->getExcise($date);
    }

    public function getSupplierAddress() {
      $tAddress = new Table_Addresses();
      $addrObj = $tAddress->getMediteAddress();
      $address = self::prepareAddress($addrObj->id);
      unset($address['person']);
      unset($address['country']);
      unset($address['phone']);
      return $address;
    }

    public function getSupplierVatNumber() {
      $tCustomers = new Table_Customers();
      $row = $tCustomers->fetchRow("id = " . Table_Customers::MEDITE_ID);
      return $row->vat_no;
    }

    public function getSupplierIDNumber() {
      $tCustomers = new Table_Customers();
      $row = $tCustomers->fetchRow("id = " . Table_Customers::MEDITE_ID);
      return $row->ident_no;
    }

    public function getSupplierSeedId() {
      $tCustomers = new Table_Customers();
      $row = $tCustomers->fetchRow("id = " . Table_Customers::MEDITE_ID);
      return $row->seed_id;
    }

    public function getShowExcise() {
      return $this->_row->show_excise;
    }

    /**
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     * @throws Zend_Pdf_Exception
     * @throws Zend_Translate_Exception
     * @throws Invoice_Exceptions_ZeroProductPrice
     */
    public function generatePdf($reduction = 0) {

      // Test nenulovych cen pro objednavky mimo EU
      if ($this->getRegion() === Table_Addresses::REGION_NON_EU) {
        $this->testZeroProductPrices(TRUE);
      }

      $t = new Zend_Translate('array', Tobacco_Invoice_Invoice::$translationEng, 'en');
      $t->addTranslation(Tobacco_Invoice_Invoice::$translationCze, 'cs');
      $t->setLocale($this->getLanguage());

      $data = array();

      $data['parts'] = $this->prepareParts();
      $data['custom'] = $this->getCustomItems();
      $data['supplier'] = $this->getSupplierAddress();
      $data['supplier'][] = $this->getSupplierIDNumber() . ' / ' . $this->getSupplierVatNumber();
      $data['supplier'][] = $this->getSupplierSeedId();

      $data['billing'] = $this->getCustomerBillingAddress();
      $data['delivery_adr'] = $this->getCustomerDeliveryAddress();
      if (is_null($data['delivery_adr'])) {
        $data['delivery_adr'] = $data['billing'];
      }
      $data['billing']['phone'] = $this->getCustomerPhone();
      if ($this->getRegion() != Table_Addresses::REGION_NON_EU
        && $this->getCustomerType() != Table_Customers::TYPE_B2C) {
        $data['billing']['reg_no'] = $this->getCustomerIDNumber();
      }
      else {
        unset($data['billing']['vat']);
        unset($data['billing']['reg_no']);
      }
      $data['billing']['vat'] = $this->getCustomerVatNumber();
      $data['billing']['seed_id'] = $this->getCustomerSeedId();

      $data['payment'] = $this->getLanguage() == 'cs' ?
        Table_Orders::$paymentToString[$this->_order->getRow()->payment_method] :
        Table_Orders::$paymentToStringEn[$this->_order->getRow()->payment_method];
      $data['carrier'] = $this->_order->getCarrier();
      $data['invoice'] = $this->_row->toArray();

      $data['deductions'] = $this->getPaymentsForDeduction();

      $data['duty']['tobacco_amount'] = $this->getTobaccoAmount();
      $data['duty']['rate'] = $this->getExciseDutyRate();
      $data['duty']['total_czk'] = round($data['duty']['tobacco_amount'] * $data['duty']['rate'], 2);
      $data['duty']['total_eur'] = round($data['duty']['total_czk'] / $this->getRate(), 2);

      $data['delivery'] = $this->getDelivery();
      $data['shipping'] = $this->getShipping();

      $data['packages'] = NULL;
      if ($packagesSum = $this->getOrder()->getPackagesSum()) {
        $pkgs = array();
        foreach ($packagesSum as $package => $amount) {
          $pkgs[] = $amount . ' ' . $t->translate('pcs') . ' ' . $package . 'g';
        }
        $data['packages'] = implode(' / ', $pkgs);
      }

        $cType = $this->getCustomerType();
      $region = $this->getRegion();

      $pdf = $this->getPdf($region);

      // nastaveni cisla faktury
      switch ($this->getType()) {
        case Table_Invoices::TYPE_PROFORM:
          $no = $t->translate('pagination_number') . ' '
            . $this->_order->getProformaNo() . ' – '
            . $t->translate('order_number') . ' ' . $this->_order->getNO();
          break;
        case Table_Invoices::TYPE_REGULAR:
        case Table_Invoices::TYPE_CREDIT:
          $no = $t->translate('pagination_number') . ' '
            . $this->getNo() . ' – '
            . $t->translate('order_number') . ' ' . $this->_order->getNO();
          break;
      }
      $pdf->setNo($no);

      // nastaveni vyspupu
      if ($region === Table_Addresses::REGION_NON_EU) {
        // NON-EU B2B / B2C:
        $pdf->showNetPrices = true;
        $pdf->showNetPricesLabel = false;
        $pdf->showTotalNetPrice = true;
        $pdf->showTotalVatPrice = false;
        $pdf->showVatSummary = false;
        $pdf->setShowHs(true);
      } elseif ($this->getCountry() !== Table_Addresses::COUNTRY_CZ) {
        if ($cType === Table_Customers::TYPE_B2B) {
          // EU B2B:
          $pdf->showNetPrices = true;
          $pdf->showNetPricesLabel = true;
          $pdf->showTotalNetPrice = true;
          $pdf->showTotalVatPrice = !$this->isVatVerified();
          $pdf->showVatSummary = !$this->isVatVerified();
        } else {
          // EU B2C:
          $pdf->showNetPrices = true;
          $pdf->showNetPricesLabel = true;
          $pdf->showTotalNetPrice = true;
          $pdf->showTotalVatPrice = !$this->isVatVerified();
          $pdf->showVatSummary = !$this->isVatVerified();
        }
      } else {
        if ($cType === Table_Customers::TYPE_B2B) {
          // CZ B2B:
          $pdf->showNetPrices = true;
          $pdf->showNetPricesLabel = true;
          $pdf->showTotalNetPrice = true;
          $pdf->showTotalVatPrice = TRUE;
          $pdf->showVatSummary = TRUE;
        } else {
          // CZ B2C:
          $pdf->showNetPrices = true;
          $pdf->showNetPricesLabel = true;
          $pdf->showTotalNetPrice = true;
          $pdf->showTotalVatPrice = TRUE;
          $pdf->showVatSummary = TRUE;
        }
      }
      $pdf->showSubtotal = TRUE;
      $pdf->setLanguage($this->getLanguage());
      $pdf->setCurrency($this->getCurrency(), $t->translate('currency_name_' . $this->_row->currency));
      $pdf->setRate($this->getRate());
      $pdf->setDiscount($this->getAdditionalDiscount(), $this->includeDiscount());
      $pdf->setItemDiscount($this->getCustomDiscount(), $this->getCustomDiscountText());
      $pdf->setIssueDate($this->_row['issue_date']);
      if (!$this->isProform()) {
        $pdf->setDUZP($this->_row['duzp']);
      }
      $pdf->setDueDate($this->_row['due_date']);

      $pdf->setDeductions($data['deductions']);

      if ($this->orderIsService()) {
        $pdf->prepare($this->_order->getServices(), $this->customRows);
      }
      else {
        $pdf->prepare($this->productRows, $this->customRows);
      }

      $pdf->setProductTotal($this->getPriceTotal());
      $pdf->setDeposit($this->_row->invoice_price);
      $pdf->setRounding($this->_row->rounding == 'y');

      $pdf->setShipping($data['shipping']);
      $pdf->setDelivery($data['delivery']);

      $packaging = $this->getPackaging();
      $pdf->setPackaging(array(
        'text' => $data['invoice']['packaging'],
        'weight' => $data['invoice']['weight'],
        'weight_unit' => 'kg',
        'cost' => $packaging['cost']
      ));
      $pdf->setAddress($data['billing'], $data['delivery_adr']);

      $tDph = new Table_ApplicationDph();
      $pdf->setVat($tDph->getActualDph($this->_row['issue_date']) / 100);

      $pdf->showZeroShipping = TRUE;
      $pdf->setShowHs($this->getRegion() == Table_Addresses::REGION_NON_EU);

      // titulek faktury
      switch ($this->getType()) {
        case Table_Invoices::TYPE_PROFORM:
          $text = $t->translate('title_proform');
          break;
        case Table_Invoices::TYPE_OFFER:
          $text = $t->translate('title_offer');
          break;
        case Table_Invoices::TYPE_CREDIT:
          $text = $t->translate('title_credit');
          break;
        case Table_Invoices::TYPE_REGULAR:
        default:
          $text = $t->translate('title_invoice');
          break;
      }
      $pdf->fillTitle($text, array(447, 800), Meduse_Pdf::TEXT_ALIGN_CENTER);

      // box dodavatel
      $pdf->fillTitle($t->translate('title_supplier'), array(35, 800), Meduse_Pdf::TEXT_ALIGN_LEFT);
      $pdf->fillTextBold(array(
        $t->translate('company_name'),
        $t->translate('address'),
        $t->translate('city_zip'),
        $t->translate('country'),
        $t->translate('reg_no_vat'),
        $t->translate('seed_id')), array(45, 780), Meduse_Pdf::TEXT_ALIGN_LEFT, 2);

      $pdf->fillAddress($data['supplier'], array(120, 780), 2);

      $deliveryCoords = null;
      // box odberatel - dodaci adresa
      if (!$this->orderIsService()) {
        $pdf->fillTitle($t->translate('delivery_address'), array(344, 672), Meduse_Pdf::TEXT_ALIGN_LEFT);
        if ($this->_customer["id_addresses2"] && $this->allowDeliveryAddress()) {
          $deliveryCoords = array(350, 655);
        } else {
          $deliveryCoords = null;
          $text = $this->_row['delivery_text'] ?
            $this->_row['delivery_text'] : $t->translate('same_as_consignee');
          $pdf->fillText($text, array(350, 650), Meduse_Pdf::TEXT_ALIGN_LEFT, 2);
        }
      }
      // box odberatel - fakturace
      $pdf->fillTitle($t->translate('title_consignee'), array(35, 672), Meduse_Pdf::TEXT_ALIGN_LEFT);

      $transArray = array(
        'company' => $this->getCustomerType() == 'B2B' ? $t->translate('company_name') : $t->translate('name'),
        'person' => $t->translate('contact_person'),
        'address' => $t->translate('address'),
        'city_zip' => $t->translate('city_zip'),
        'country' => $t->translate('country'),
        'phone' => $t->translate('phone'),
        'reg_no' => $t->translate('reg_no'),
        'vat_no' => $t->translate('vat_no'),
        'seed_id' => $t->translate('seed_id'),
      );
      if (isset($data['billing']['additional_row']) || isset($data['delivery']['additional_row'])) {
        $head = $this->getAddAddressRowHead();
        $transArray['additional_row'] = $head ? $head : '';
      }
      $pdf->fillCustomerAddress($transArray, array(45, 655), array(120, 655), $deliveryCoords);

      // box datum vystaveni, DUZP, splatnost a box zpusobu platby
      $pdf->fillTextBold(array(
        $this->getDateLabel($t),
        $t->translate('payment_method')), array(45, 545), Meduse_Pdf::TEXT_ALIGN_LEFT, 2.3);
      $pdf->fillDate(array(156, 545));
      $pdf->fillText($data['payment'], array(156, 526), Meduse_Pdf::TEXT_ALIGN_LEFT, 2.3);

      // box cislo faktury
      if ($this->getType() == Table_Invoices::TYPE_PROFORM) {
        $pdf->fillTitle($t->translate('proform_no') . ': ' . $this->_order->getProformaNo(), array(344, 562), Meduse_Pdf::TEXT_ALIGN_LEFT);
      }
      if ($this->getType() == Table_Invoices::TYPE_REGULAR) {
        $pdf->fillTitle($t->translate('invoice_no') . ': ' . $this->getNo(), array(344, 562), Meduse_Pdf::TEXT_ALIGN_LEFT);
      }
      if ($this->getType() == Table_Invoices::TYPE_CREDIT) {
        $tInvoice = new Table_Invoices;
        $source = $tInvoice->getInvoiceByOrder($this->getOrderId());
        if ($source and !is_null($source['no'])) {
          $pdf->fillTitle($t->translate('invoice_no') . ': ' . $source['no'], array(344, 580), Meduse_Pdf::TEXT_ALIGN_LEFT);
        }
        $pdf->fillTitle($t->translate('credit_no') . ': ' . $this->getNo(), array(344, 562), Meduse_Pdf::TEXT_ALIGN_LEFT);
      }

      if (!$this->orderIsService()) {
        // box baleni
        $pdf->fillPackaging(array(
          $t->translate('packaging'), '',
          $t->translate('total_weight')), array(344, 545), array(550, 545));
      }
      // hlavicka polozek
      $pdf->printHeader($t);

      // vypis polozek
      $pdf->printItems();

      $pdf->fillNonItemsPrice(array(
        'subtotal' => $t->translate('subtotal'),
        'item_discount' => $pdf->getItemDiscountText(),
        'shipping' => $t->translate('shipping_costs_1'),
        'additional_discount' => sprintf($t->translate('additional_discount'), $pdf->getDiscount()),
      ), array(35, 227), array(505, 227), array(561, 227));

      $pdf->fillTotalPrice(array(
        'total' => ($this->_row->show_vat_summary == 'y' || $this->getCountry() == Table_Addresses::COUNTRY_CZ) ?
          $t->translate('total_base') : $t->translate('total_amount'),
        'vat_tax' => sprintf($t->translate('vat_tax'), $pdf->getVat() * 100),
        'deduction' => $t->translate('deduction'),
        'deduction_vat' => $t->translate('deduction_vat'),
        // 'rounding' => $t->translate('rounding'),
        'total_vat' => ($this->getRegion() == 'non-eu') ?
          $t->translate('total_amount') : $t->translate('total_vat'),
        'deposit'     => sprintf($t->translate('deposit'), $pdf->getDepositPercentage()),
        'currency' => $t->translate('currency')), array(339, 188), array(500, 188), array(561, 188));

      // dodatecne informace
      $info = array();
      if ($this->_row->currency == Table_Invoices::CURRENCY_EUR) {
        $info[] = sprintf($t->translate('rate_by_cnb'), $this->getRate());
      }
      if ($data['duty']['tobacco_amount'] > 0) {
        if (!$this->isExport() && $this->getShowExcise()=='y') {
          if ($this->_row->currency == Table_Invoices::CURRENCY_EUR) {
            $duty = $data['duty']['total_eur'];
          }
          else {
            $duty = $data['duty']['total_czk'];
          }
          $duty = $this->getType() == Table_Invoices::TYPE_CREDIT ? -$duty : $duty;
          $info[] = ($this->_row->currency == Table_Invoices::CURRENCY_EUR) ?
           sprintf($t->translate('excise_duty'), Meduse_Currency::format($duty, NULL, 2, FALSE), Meduse_Currency::format($data['duty']['total_czk'], NULL, 2, FALSE))
            : sprintf($t->translate('excise_duty_cze'), Meduse_Currency::format($duty, NULL, 2, FALSE));
        }
        $info[] = sprintf($t->translate('net_tobacco_weight'), $data['duty']['tobacco_amount']);
        if ($data['packages']) {
          $info[] = $data['packages'];
        }
      }
      $pdf->fillText($info, array(561, 112), Meduse_Pdf::TEXT_ALIGN_RIGHT);

      $pdf->fillNotice($this->getUserText(), array(35, 178));

      $pdf->fillText($t->translate('signature'), array(35, 100), Meduse_Pdf::TEXT_ALIGN_LEFT);

      // paticka
      $account = $this->getAccountInfo();
      if ($account) {
        $pdf->fillFooter(
          $t->translate('bank') . ': ' . $account['name'] . ', ' . $account['address']
          . ' / IBAN: ' . $account['iban'] . ' / BIC: ' . $account['bic']
          . ' / ' . $t->translate('account_no') . ': ' . $account['account_prefix']
          . '-' . $account['account_number'] . '/' . $account['bank_number'], array(300, 52));
      }

      $filePath = $this->_saveFile($pdf);
      $this->_addVersion($filePath);
      $this->setValid();
      return $pdf;
    }

    protected function _prepareParts() {
      $t = new Zend_Translate('array', Tobacco_Invoice_Invoice::$translationEng, 'en');
      $t->addTranslation(Tobacco_Invoice_Invoice::$translationCze, 'cs');
      $t->setLocale($this->getLanguage());
      $parts = $this->getParts()->toArray();
      // slouceni a preklad kategorii pro potreby fakturace
      foreach ($parts as $key => $part) {
        switch ($part['ctg_id']) {
          case Table_TobaccoWarehouse::CTG_TOBACCO_NFS:
            $parts[$key]['ctg_name'] = $t->translate('ctg_tobacco_nostamp');
            break;
          case Table_TobaccoWarehouse::CTG_TOBACCO_FS:
            $parts[$key]['ctg_name'] = $t->translate('ctg_tobacco');
            break;
          default:
            $parts[$key]['ctg_name'] = $t->translate('ctg_tobacco_accessories');
            $parts[$key]['ctg_id'] = 9999; //dummy id
            break;
        }
      }
      return $parts;
    }

    public function setOrder(int $orderId, string $type = NULL): int
    {
      $invoices = new Table_Invoices();
      $order = new Tobacco_Orders_Order($orderId);
      $row = $invoices->createRow(array(
        'id_orders' => $order->getID(),
        'id_wh_type' => $order->getWhType(),
        'show_excise' => 'y',
        'registered' => 'n',
        'use_actual_rate' => $order->isExpectedPaymentRateDefault() ? 'y' : 'n',
        'rate' => $order->getExpectedCurrency() === Table_Orders::CURRENCY_EUR ? $order->getExpectedPaymentRate() : 1,
        'issue_date' => date('Y-m-d'),
        'duzp' => date('Y-m-d'),
        'export' => $order->getCountry() === Table_Addresses::COUNTRY_CZ ? 'n' : 'y',
      ));
      $this->_row = $row;
      $this->_order = $order;
      $this->_customer = $this->_order->getCustomer();

      if (is_null($this->_customer)) {
        throw new Orders_Exceptions_NoCustomer('Nelze generovat fakturu, k objednávce není přiřazen zákazník.');
      }

      $this->_row->id_users = Utils::getCurrentUserId();
      $this->_row->inserted = new Zend_Db_Expr('now()');
      $this->_row->last_change = new Zend_Db_Expr('now()');

      if (!is_null($type)) {
        if ($type === Table_Invoices::TYPE_REGULAR) {
          $this->_row->type = $this->_order->checkRegularCondition() ? Table_Invoices::TYPE_REGULAR : Table_Invoices::TYPE_PROFORM;
        }
        else {
          $this->_row->type = $type;
        }
      }
      else {
        $this->_row->type = $this->_order->getDefaultInvoiceType();
      }

      // podle typu zvolíme číslo
      switch ($this->_row->type) {
        case Table_Invoices::TYPE_PROFORM:
          $this->_order->generateProformaNo();
          $this->_row->no = $this->_order->getProformaNo();
          break;
        case Table_Invoices::TYPE_REGULAR:
          $this->_row->no = $invoices->getNextNo($order);
          break;
      }

      // zjistíme, jestli existuje proforma - pouze pro ostré faktury
      $proform = $this->getPrevious();
      // pokud existuje, přeneseme náležitosti z proformy
      if ($proform instanceof Invoice_Invoice) {
        $this->copySettingsFrom($proform);
      }
      // pokud ne, vezmeme data z objednávky
      else {

        $this->_row->currency = $this->_order->getExpectedCurrency();

        $tPricelist = new Table_Pricelists();
        $this->_row->id_pricelists = $tPricelist->getCustomersValidPricelist($this->_customer->id);

        $tDph = new Table_ApplicationDph();
        $this->_row->vat = ($tDph->getActualDph($this->_row['issue_date']) / 100);

        // automaticke prednastaveni zobrazeni DPH dle země nebo regionu
        if ($this->getCountry() === Table_Addresses::COUNTRY_CZ) {
          $this->_row->show_vat_summary = 'y';
        }
        elseif ($this->getRegion() === Table_Addresses::REGION_NON_EU) {
          $this->_row->show_vat_summary = 'n';
        }
        else {
          $this->_row->show_vat_summary = $order->getCustomer(TRUE)->isVerifiedVatNo() ? 'n' : 'y';
        }

        $tAddresses = new Table_Addresses();
        $address = $tAddresses->getAddress($this->_customer['id_addresses']);
        $country = $address['country'];

        $this->_row->language = $country == 'CZ' ? Table_Invoices::LANGUAGE_CS : Table_Invoices::LANGUAGE_EN;

        $expected = $this->_order->getExpectedPayment();
        $this->_row->invoiced_total = is_null($expected) ? 0 : $expected;
        $this->_row->id_accounts = $this->_order->getDefaultAccount();

      }

      if (($this->_row->type === Table_Invoices::TYPE_REGULAR) && !$this->_row->due_date) {

        $due = $order->getDue();

        $date = new Meduse_Date();
        $date->addDay($due);
        $this->_row->due_date = $date->get('Y-m-d');
      }

      $this->_row->save();
      if ($proform instanceof Invoice_Invoice) {
        $this->copyCustomItemsFrom($proform);
        $this->copyProductsItemsFrom($proform);
      }
      else if ($this->_order->getCarrier()) {
        $data = [
          'type' => Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING,
          'description' => $this->_order->getCarrierMethodFullName(),
          'price' => $this->_order->getCarrierPrice() / $this->_row->rate,
        ];
        $this->setCustomItem($data);
      }

      return $this->getId();
    }

    /**
     * Rutina generujici nazev PDF souboru (overriden)
     *
     * @return string
     */
    protected function _generateFilename(): string {
      $invoiceNo = $this->getNo();
      if ($this->isCredit()) {
        $invoiceNo = 'C' . $invoiceNo;
      }
      $orderId = $this->getOrderId();
      $tOrders = new Table_Orders();
      try {
        $rOrder = $tOrders->find($orderId)->current();
        $orderTitle = $rOrder->title ?? '';
        $orderNo = $rOrder->no ?? '';
      }
      catch (Zend_Db_Table_Exception $e) {
        Utils::logger()->warn("Unable to get the order ID = $orderId from database. " . $e->getMessage());
        $orderTitle = '';
        $orderNo = '';
      }
      setlocale(LC_CTYPE, 'cs_CZ.utf-8');
      $orderName = strtr(iconv('UTF-8', 'ASCII//TRANSLIT', $orderTitle), array(' ' => '-', "'" => '', ',' => '', '"' => ''));
      $version = count($this->versions) + 1;
      return implode('_', array($invoiceNo, $orderNo, $orderName, $version));
    }

    public function getCustomerDeliveryAddress() {
      if (isset($this->_customer["id_addresses2"])) {
        $address = self::prepareAddress($this->_customer["id_addresses2"], $this->_customer["type"]);
        unset($address['country']);
        if ($value = $this->getAddAddressRow2nd()) {
          $address['additional_row'] = $value;
        }
        return $address;
      }
      return [];
    }

    static protected $translationEng = array(
        'title_proform' => "P R O - F O R M A   I N V O I C E",
        'title_offer' => "O F F E R",
        'title_invoice' => "I N V O I C E",
        'title_credit' => "C R E D I T   N O T E",
        'title_supplier' => "SELLER",
        'company_name' => "Company name",
        'name' => "Name",
        'address' => "Address",
        'city_zip' => "City/Postal Code",
        'country' => "Country",
        'vat_no' => "VAT No.",
        'reg_no' => "Reg.No.",
        'reg_no_vat' => 'Reg.No. / VAT No.',
        'title_consignee' => "BUYER",
        'delivery_address' => "Consignee",
        'date_issue' => "Issued",
        'date_ts' => "TS",
        'date_due' => "Due",
        'date_prefix' => "",
        'date_suffix' => " date",
        'payment_method' => "Payment method",
        'invoice_no' => "Invoice No.",
        'proform_no' => "Pro-forma invoice No.",
        'credit_no'  => "Credit Note No.",
        'packaging' => "Packaging",
        'total_weight' => "Total weight brutto",
        'header_id_no' => "ID No.",
        'header_full_description' => "Full description of goods / Subject of invoicing",
        'header_serial_no' => "Serial No.",
        'header_packages' => "Packages\n(Pcs)",
        'header_unit' => "Unit Value",
        'header_subtotal' => "Sub Total\nValue",
        'net_vat' => "net of VAT",
        'shipping_costs_1' => "Shipping costs by %c",
        'shipping_costs_2' => "Shipping costs (incl. %d) by %c",
        'total_amount' => "Total amount",
        'vat_tax' => 'VAT tax %1$s %%',
        'total_vat' => "Total incl. VAT tax",
        'total_base' => 'Tax base',
        'issued' => "Invoice issued by",
        'signature' => "Signature",
        'bank' => "Bank",
        'czech_republic' => "Czech Republic",
        'account_no' => "Account No",
        'phone' => 'Phone',
        'header_hs_code' => 'HS code',
        'notice_b2c' => 'Purchaser (consignee) declares that the goods on this invoice is for private use only and is familiar with the fact that its placement into business sector for commercial use is strictly prohibited.',
        'currency' => 'Currency',
        'currency_name_CZK' => 'Czech Crown',
        'currency_name_EUR' => 'Euro',
        'notice_b2b_world' => 'We certify that the goods exported to the %1$s are of pure national origin of Czech Republic. They contain Czech materials and they are being exported from Czech Republic. The goods were manufactured by MEDUSE DESIGN',
        'notice_b2b_eu' => 'Delivery of goods to EU Member States is exempted from VAT according to § 64 of Act 235/2004 Coll. The person liable to declare and pay VAT (including entitlement to the deduction) is the purchaser of the goods.',
        'same_as_consignee' => 'Same as buyer',
        'additional_discount' => 'Additional discount %1$s %%',
        'ctg_pipe_sets' => 'Pipe sets',
        'ctg_presentation_tools' => 'Presentation tools',
        'ctg_extra_accessories' => 'Extra accessories',
        'rate_by_cnb' => 'Price converted from EUR using the exchange rate of the ČNB: 1 EUR = %s CZK',
        'net_tobacco_weight' => 'Net tobacco weight: %s kg',
        'excise_duty' => 'Excise duty: %s EUR = %s CZK',
        'excise_duty_cze' => 'Excise duty: %s Kč',
        'ctg_tobacco' => 'Tobacco',
        'ctg_tobacco_nostamp' => 'Unstamped tobacco',
        'ctg_tobacco_accessories' => 'Accesories',
        'rounding' => 'Rounding',
        'deposit' => '%s %% deposit',
        'seed_id' => 'SEED ID',
        'pagination_number' => 'ivn.no.',
        'header_subject' => 'Subject of invoicing',
        'header_price' => 'Price',
        'header_qty' => "Quantity",
        'with_vat' => "with VAT",
        'pcs' => 'pcs',
        'deduction' => 'Deduction of deposit',
        'deduction_vat' => 'Deduction VAT of deposit',
        'order_number' => 'ord.no.',
        'page_number' => 'pg.',
        'contact_person' => 'Contact person',
        'subtotal' => 'Subtotal',
    );

    static protected $translationCze = array(
        'title_proform' => "P R O - F O R M A   F A K T U R A",
        'title_offer' => "N A B Í D K A",
        'title_invoice' => "F A K T U R A",
        'title_credit' => "O P R A V N Ý   D A Ň O V Ý   D O K L A D",
        'title_supplier' => "DODAVATEL",
        'company_name' => "Firma",
        'name' => "Jméno",
        'address' => "Adresa",
        'city_zip' => "Město/PSČ",
        'country' => "Země",
        'vat_no' => "DIČ",
        'reg_no' => "IČ",
        'reg_no_vat' => 'IČ / DIČ',
        'title_consignee' => "ODBĚRATEL",
        'delivery_address' => "Dodací adresa",
        'date_issue' => "Vystaveno",
        'date_ts' => "DUZP",
        'date_due' => "Splatnost",
        'date_prefix' => "",
        'date_suffix' => "",
        'payment_method' => "Způsob platby",
        'invoice_no' => "Číslo faktury",
        'proform_no' => "Číslo pro-forma faktury",
        'credit_no'  => "Číslo opravného daň. dokladu",
        'packaging' => "Balení",
        'total_weight' => "Celková hmotnost brutto",
        'header_id_no' => "ID číslo",
        'header_full_description' => "Celkový popis zboží / Předmět fakturace",
        'header_serial_no' => "Seriové číslo",
      	'header_packages' => "Balení\n(ks)",
        'header_unit' => "Jedn. cena",
        'header_subtotal' => "Celkem",
        'net_vat' => "bez DPH",
        'shipping_costs_1' => "Přepravní náklady pomocí %c",
        'shipping_costs_2' => "Přepravní náklady (vč. %d) pomocí %c",
        'total_amount' => "Celková částka",
        'total_base' => 'Základ',
        'vat_tax' => 'DPH %1$s %%',
      	'total_vat' => "Celkem vč. DPH",
        'issued' => "Vystavil",
        'signature' => "Podpis",
        'bank' => "Banka",
        'czech_republic' => "Česká republika",
        'account_no' => "Číslo účtu",
        'phone' => 'Telefon',
        'header_hs_code' => 'HS kód',
        'notice_b2c' => 'Kupující (příjemce), prohlašuje, že zboží uvedené na této faktuře je pouze pro soukromé použití a je obeznámen s tím, že jeho umístění do podnikatelského sektoru pro komerční účely je přísně zakázáno.',
        'currency' => 'Měna',
        'currency_name_CZK' => 'Česká koruna',
        'currency_name_EUR' => 'Euro',
        'same_as_consignee' => 'Stejná jako odběratel',
        'additional_discount' => 'Dodatečná sleva %1$s %%',
        'ctg_pipe_sets' => 'Sety dýmek',
        'ctg_presentation_tools' => 'Prezentační nástroje',
        'ctg_extra_accessories' => 'Extra příslušenství',
        'rate_by_cnb' => 'Ceny přepočítány z EUR dle kurzu vyhlášeného ČNB: 1 EUR = %s CZK',
        'net_tobacco_weight' => 'Čistá hmotnost tabáku: %s kg',
        'excise_duty' => 'Spotřební daň: %s EUR = %s CZK',
        'excise_duty_cze' => 'Spotřební daň: %s Kč',
        'ctg_tobacco' => 'Tabák',
        'ctg_tobacco_nostamp' => 'Tabák nekolkovaný',
        'ctg_tobacco_accessories' => 'Doplňky',
        'rounding' => 'Zaokrouhlení',
        'deposit' => '%s %% záloha',
        'seed_id' => 'Celní sklad',
        'pagination_number' => 'fakt.č.',
        'header_subject' => 'Předmět fakturace',
        'header_price' => 'Cena',
        'header_qty' => "Počet",
        'with_vat' => "s DPH",
        'pcs' => 'ks',
        'deduction' => 'Odpočet zálohy',
        'deduction_vat' => 'Odpočet DPH ze zálohy',
        'order_number' => 'obj.č.',
        'page_number' => 'str.',
        'contact_person' => 'Kontaktní osoba',
        'subtotal' => 'Mezisoučet',
    );
    public function getShipping() {
      $cost = 0;
      $text = array();
      $tItems = new Table_InvoiceCustomItems();
      $data = $tItems->getInvoiceItems($this->getId(), Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING);
      if ($data) {
        foreach ($data as $row) {
          $cost += (float) $row['price'];
          $text[] = $row['description'];
        }
      }
      return array(
        'show' => !empty($text),
        'cost' => $cost,
        'text' => implode(", ", $text)
      );
    }
    public function isTransferredTaxLiability($wh = Parts_Part::WH_TYPE_TOBACCO) {
      return parent::isTransferredTaxLiability($wh);
    }

    protected function getPdf($region) {
      // volba sablony
      if ($this->orderIsService()) {
        $pdf = $this->showVatSummary() ?
          Pdf_InvoiceServices::load(Pdf_InvoiceServices::TEMPLATE_SERVICES_TOBACCO_VAT)
          : Pdf_InvoiceServices::load(Pdf_InvoiceServices::TEMPLATE_SERVICES_TOBACCO);
        $pdf->type = $this->getType();
      }
      else {
        $pdf = $this->showVatSummary() ?
          Pdf_Invoice::load(Meduse_Pdf_Invoice::TEMPLATE_TOBACCO_VAT)
          : Pdf_Invoice::load(Meduse_Pdf_Invoice::TEMPLATE_TOBACCO);
      }
      return $pdf;
    }
  }

