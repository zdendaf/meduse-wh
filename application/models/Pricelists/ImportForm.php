<?php

class Pricelists_ImportForm extends Zend_Form {

  function init() {

    $this->setMethod(Zend_Form::METHOD_POST);
    $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    $this->setAction('/pricelists/import');
    $this->setLegend('Import cen do ceníku');
    $this->setAttrib('id', 'pricelistImportForm');

    $element = new Zend_Form_Element_Hidden('id_pricelists');
    $this->addElement($element);

    $element = new Zend_Form_Element_File('datafile');
    $element->setLabel('Soubor s cenami');
    $element->addValidator(new Zend_Validate_File_Count([
      'min' => 1,
      'max' => 1,
    ]));
    $element->addValidator(new Zend_Validate_File_Size(['max' => 102400]));
    $element->addValidator(new Zend_Validate_File_Extension(['csv']));
    $element->setRequired(TRUE);
    $this->addElement($element);


    $element = new Zend_Form_Element_Checkbox('update_only');
    $element->setLabel('Pouze aktualizovat');
    $this->addElement($element);

    $element = new Zend_Form_Element_Submit('import');
    $element->setLabel('Importovat');
    $this->addElement($element);

    parent::init();
  }
}
