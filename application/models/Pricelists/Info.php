<?php
/** TODO Zrychlit!!! */
class Pricelists_Info {
  private $_names = array();
  private $_categories = array();
  private $_prices = array();
  private $_column_names = array();
  private $_margins = array();

  public function __construct($result, $ids) {
    $pricelists = array();
    $margins = array();
    $acl = Zend_Registry::get('acl');
    
    $tRate = new Table_ApplicationRate();
    $rate = $tRate->getRate();
    
    foreach ($result as $row) {
      
      
      $id_parts = $row['id_parts'];
      $part = new Parts_Part($id_parts);
      $this->_categories[$id_parts] = $part->getSuperCtg();
      
      $pricelist_id = $row['id'];
      $pricelist_abbr = $row['abbr'];
      $pricelists[$id_parts][$pricelist_abbr] = $row['price'];
      
      $this->_names[$id_parts] = $part->getName();

      if ($acl->isAllowed('pricelists/show-margins')) {
        $cost = $part->getCost() / $rate;
        $margins[$id_parts][$pricelist_abbr] = $cost == 0 ? 
          0 : round(($row['price'] - $cost) / $cost * 100, 2);
      }
      if (!in_array($pricelist_abbr, $this->_column_names)) {
        $this->_column_names[$pricelist_id] = $pricelist_abbr;
      }
    }
    //Zend_Debug::dump($this->_categories); die;

      // nahrat tak, jak jsme je vybrali
    foreach ($pricelists as $id_parts => $pricelist) {
      foreach ($ids as $id) {
        if (isset($this->_column_names[(int) $id])) {
          $abbr = $this->_column_names[(int) $id];

          if (isset($pricelist[$abbr])) {
            $this->_prices[$id_parts][$abbr] = $pricelist[$abbr];
          } else {
            $this->_prices[$id_parts][$abbr] = null;
          }
          
          if (isset($margins[$id_parts][$abbr])) {
            $this->_margins[$id_parts][$abbr] = $margins[$id_parts][$abbr]; 
          } else {
            $this->_margins[$id_parts][$abbr] = null;
          }          
        }
      }
    }
  }
  
  public function getNames() { return $this->_names; }
  public function getCategories() { return $this->_categories; }  
  public function getColumnNames() { return array_reverse($this->_column_names, TRUE); }
  public function getPrices() { return $this->_prices; }
  public function getMargins() { return $this->_margins; }
}