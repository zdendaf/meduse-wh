<?php

class Pricelists_CtgDefaultForm extends Meduse_FormBootstrap {

  function init() {

    $this->setAttrib('class', 'form');
    $this->setLegend('Výchozí slevy');

    $weight = 0;
    $defaults = Pricelists::getCtgDefaults();

    for ($idx = 1; $idx <= Pricelists::CTG_NUM; $idx++) {
      $discount = $defaults[$idx] ?? 0;
      $element = new Meduse_Form_Element_Text('discount_' . $idx);
      $element->setLabel('kategorie ' . $idx);
      $element->setValue($discount);
      $element->addValidator(new Zend_Validate_Int());
      $element->addValidator(new Zend_Validate_GreaterThan(-1));
      $element->addValidator(new Zend_Validate_LessThan(100));
      $element->setOrder($weight++);
      $this->addElement($element);
    }

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Uložit');
    $element->setOrder($weight);
    $this->addElement($element);

    parent::init();
  }

}