<?php

class Pricelists_MappingForm extends Meduse_FormBootstrap
{

  /**
   * @throws Zend_Form_Exception
   */

  protected $wh;

  public function __construct($options = null)
  {
    if (isset($options['wh'])) {
      $this->wh = $options['wh'];
      unset($options['wh']);
    }
    parent::__construct($options);
  }

  /**
   * @throws Zend_Form_Exception
   */
  public function init(): void
    {
        $this->setMethod('post');
        $this->setAttrib('id', 'mappingForm');
        $this->setAttrib('class', 'form');

        $this->setLegend('Mapování ceníků');

        $table = new Table_Pricelists();
        $lists = $table->getAllPricelists($this->wh);
        $indexes = Pricelists::getAllPriceListIds();

        $options = [null => '-- zvolit --'];
        foreach ($lists as $list) {
          $options[$list['id']] = $list['name'];
        }

        $mappings = Pricelists::getMapping(null, true);

        foreach ($indexes as $idx) {
          $elementId = 'price_list_' . $idx;
          $element = new Meduse_Form_Element_Select($elementId);
          $element->setLabel($idx);
          $element->addMultiOptions($options);
          $element->setRequired();
          $element->setValue($mappings[$elementId] ?? NULL);
          $this->addElement($element);

          $elementId = 'price_list_draft_' . $idx;
          $element = new Meduse_Form_Element_Select($elementId);
          $element->setLabel($idx . ' draft');
          $element->addMultiOptions($options);
          $element->setRequired();
          $element->setValue($mappings[$elementId] ?? NULL);
          $this->addElement($element);
        }

        $element = new Meduse_Form_Element_Submit('save');
        $element->setLabel('Uložit');
        $this->addElement($element);

        parent::init();
    }

}
