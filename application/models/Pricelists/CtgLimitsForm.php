<?php

class Pricelists_CtgLimitsForm extends Meduse_FormBootstrap {

  function init() {

    $this->setAttrib('class', 'form');
    $this->setLegend('Limity');

    $weight = 0;
    $limits = Pricelists::getCtgLimits();

    for ($idx = 1; $idx <= Pricelists::CTG_NUM; $idx++) {
      $limit = $limits[$idx] ?? 0;
      $element = new Meduse_Form_Element_Text('limit_' . $idx);
      $element->setLabel('kategorie ' . $idx);
      $element->setValue($limit);
      $element->addValidator(new Zend_Validate_Int());
      $element->addValidator(new Zend_Validate_GreaterThan(-1));
      $element->setOrder($weight++);
      $this->addElement($element);
    }

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Uložit');
    $element->setOrder($weight);
    $this->addElement($element);

    parent::init();
  }

}