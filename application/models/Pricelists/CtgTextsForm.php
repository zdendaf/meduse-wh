<?php

class Pricelists_CtgtextsForm extends Meduse_FormBootstrap {

  function init() {

    $this->setAttrib('class', 'form');
    $this->setLegend('Texty');

    $weight = 0;
    $texts = Pricelists::getCtgTexts();
    $labels = Pricelists::TEXTS_IDX;

    foreach ($texts as $idx => $value) {
      $element = new Meduse_Form_Element_Textarea($idx);
      $element->setLabel($labels[$idx]);
      $element->setAttrib('rows', 3);
      $element->setAttrib('maxlength', 200);
      $element->setValue($value);
      $element->setOrder($weight++);
      $this->addElement($element);
    }

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Uložit');
    $element->setOrder($weight);
    $this->addElement($element);

    parent::init();
  }

}