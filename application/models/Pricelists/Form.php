<?php

class Pricelists_Form extends Meduse_Form {
    
    public function __construct($style, $description) {
        
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setAttrib('class', 'form');
        $this->setName('pricelists_form');
        $this->setLegend('Vložení / editace ceníku');
        
        $element = new Zend_Form_Element_Hidden('id');
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Text('name');
        $element->setRequired(true);
        $element->setLabel('Název ceníku');
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Text('abbr');
        $element->setRequired(true);
        $element->setLabel('Zkratka ceníku');
        $element->setAttrib('maxlength', '5');
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Select('type');
        $element->setRequired(true);
        $element->setLabel('Pro zákazníky typu');
        $element->addMultiOptions(array(
            Table_Pricelists::TYPE_B2B => 'Prodejce / Distributor',
            Table_Pricelists::TYPE_B2C => 'Koncový spotřebitel'
        ));
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Select('region');
        $element->setRequired(true);
        $element->setLabel('Region');
        $element->addMultiOptions(array(
            Table_Pricelists::REGION_EU     => 'Evropská unie',
            Table_Pricelists::REGION_NON_EU => 'Mimo evropskou unii'
        ));
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_DatePicker('valid');
        $element->setRequired(true);
        $element->setLabel('Platnost od');
        $element->setOptions(array(
            'jQueryParams' => array(
                'dateFormat' => 'dd.mm.yy')
            ));
        $element->addValidator(new Zend_Validate_Date('d.m.y'));
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Select('assign');
        $element->setLabel('Přiřadit zákazníky');
        $element->addMultiOption('', 'Žádné');
        $element->addMultiOption('all', 'Všechny');
        $this->addElement($element);

        parent::__construct($style, $description);
    }
    
    
    
}
