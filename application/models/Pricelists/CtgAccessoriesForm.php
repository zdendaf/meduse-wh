<?php

class Pricelists_CtgAccessoriesForm extends Meduse_FormBootstrap {

  /**
   * @throws \Zend_Validate_Exception
   * @throws \Zend_Db_Statement_Exception
   * @throws \Zend_Form_Exception
   */
  public function init() {

    $this->setAttrib('class', 'form');
    $this->setLegend('Slevy pro kategorii Accessories');

    $defaults = Pricelists::getCtgPartsCtg(Table_PartsCtg::TYPE_ACCESSORIES);
    $ctg = Pricelists::getCtgPartsCtg(Table_PartsCtg::TYPE_ACCESSORIES, FALSE);
    $weight = 0;

    for ($idx = 1; $idx <= Pricelists::CTG_NUM; $idx++) {
      $discount = $ctg[$idx] ?? NULL;
      $element = new Meduse_Form_Element_Text('discount_' . $idx);
      $element->setLabel('kategorie ' . $idx);
      $element->setValue($discount);
      $element->setAttrib('placeholder', $defaults[$idx]);
      $element->addValidator(new Zend_Validate_Int());
      $element->addValidator(new Zend_Validate_GreaterThan(-1));
      $element->addValidator(new Zend_Validate_LessThan(100));
      $element->setOrder($weight++);
      $this->addElement($element);
    }

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Uložit');
    $element->setOrder($weight);
    $this->addElement($element);

    parent::init();
  }

}