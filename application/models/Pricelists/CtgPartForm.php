<?php

class Pricelists_CtgPartForm extends Meduse_FormBootstrap {

  protected $part;

  /**
   * @throws \Exception
   */
  public function __construct($options = NULL) {
    if (is_array($options) && isset($options['id_part'])) {
      $this->part = new Parts_Part($options['id_part']);
      unset($options['id_part']);
      parent::__construct($options);
    }
    else {
      throw new Exception('Nebylo zadano ID produktu.');
    }
  }

  /**
   * @throws \Zend_Validate_Exception
   * @throws \Zend_Db_Statement_Exception
   * @throws \Zend_Form_Exception
   */
  public function init() {

    $this->setAttrib('class', 'form');
    $this->setLegend('Slevy pro produkt ' . $this->part->getID());

    $defaults = Pricelists::getCtgParts($this->part->getID(), TRUE);
    $ctg = Pricelists::getCtgParts($this->part->getID(), FALSE);
    $weight = 0;

    for ($idx = 1; $idx <= Pricelists::CTG_NUM; $idx++) {
      $discount = $ctg[$idx] ?? NULL;
      $element = new Meduse_Form_Element_Text('discount_' . $idx);
      $element->setLabel('kategorie ' . $idx);
      $element->setValue($discount);
      $element->setAttrib('placeholder', $defaults[$idx]);
      $element->addValidator(new Zend_Validate_Int());
      $element->addValidator(new Zend_Validate_GreaterThan(-1));
      $element->addValidator(new Zend_Validate_LessThan(100));
      $element->setOrder($weight++);
      $this->addElement($element);
    }

    $element = new Zend_Form_Element_Hidden('part_id');
    $element->setValue($this->part->getID());
    $element->setOrder($weight++);

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Uložit');
    $element->setOrder($weight);
    $this->addElement($element);

    parent::init();
  }

}