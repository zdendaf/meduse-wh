<?php

class Pricelists_CtgCollectionForm extends Meduse_FormBootstrap {

  protected $collectionId;
  protected $collectionAbbr;

  /**
   * @throws \Exception
   */
  public function __construct($options = NULL) {
    if (is_array($options) && isset($options['id_collection'])) {
      $this->collectionId = $options['id_collection'];
      unset($options['id_collection']);
      $collections = Parts::getCollections();
      if (key_exists($this->collectionId, $collections)) {
        $this->collectionAbbr = $collections[$this->collectionId]['abbr'];
      }
      else {
        throw new Exception('Kolekce ID = ' . $this->collectionId . ' neexistuje.');
      }
      parent::__construct($options);
    }
    else {
      throw new Exception('Nebylo zadano ID kolekce.');
    }
  }

  /**
   * @throws \Zend_Validate_Exception
   * @throws \Zend_Db_Statement_Exception
   * @throws \Zend_Form_Exception
   */
  public function init() {

    $this->setAttrib('class', 'form');
    $this->setLegend('Slevy pro kolekci ' . $this->collectionAbbr);

    $defaults = Pricelists::getCtgCollections($this->collectionId, TRUE);
    $ctg = Pricelists::getCtgCollections($this->collectionId, FALSE);
    $weight = 0;

    for ($idx = 1; $idx <= Pricelists::CTG_NUM; $idx++) {
      $discount = $ctg[$idx] ?? NULL;
      $element = new Meduse_Form_Element_Text('discount_' . $idx);
      $element->setLabel('kategorie ' . $idx);
      $element->setValue($discount);
      $element->setAttrib('placeholder', $defaults[$idx]);
      $element->addValidator(new Zend_Validate_Int());
      $element->addValidator(new Zend_Validate_GreaterThan(-1));
      $element->addValidator(new Zend_Validate_LessThan(100));
      $element->setOrder($weight++);
      $this->addElement($element);
    }

    $element = new Zend_Form_Element_Hidden('collection_id');
    $element->setValue($this->collectionId);
    $element->setOrder($weight++);

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Uložit');
    $element->setOrder($weight);
    $this->addElement($element);

    parent::init();
  }

}