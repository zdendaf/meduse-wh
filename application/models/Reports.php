<?php

class Reports {

  public function getSalesForm($params = []): Reports_SalesForm {
    $form = new Reports_SalesForm();
    $form->populate($params);
    return $form;
  }

  public function getPipesSales($options): array {
    $top = new Table_OrdersProducts();
    $data = $top->getTotalPipesSales(Table_PartsCtg::TYPE_SETS, $options);
    return $this->transformPipesSalesData($data);
  }

  public function getAccessoriesSales($options): array {
    $top = new Table_OrdersProducts();
    $data = $top->getTotalPipesSalesExclude(array(Table_PartsCtg::TYPE_SETS), $options);
    return $this->transformAccessoriesSalesData($data);
  }

  public function getServicesSales($options): array {
    $tos = new Table_OrdersServices();
    $data = $tos->getTotalSales($options);
    return $this->transformServicesData($data);
  }

  /**
   * Vrátí data pro kontingenční tabulku prodejů.
   */
  protected function transformPipesSalesData($data): array {

    $row = ['pcs' => 0, 'free' => 0, 'price' => 0.0];
    $total_row = [
      Table_Customers::TYPE_B2B => $row,
      Table_Customers::TYPE_B2C => $row,
      Table_Customers::TYPE_ALL => $row
    ];
    $cls_row = ['parts' => []];

    $t = [
      'sum' => ['cls' => [], 'total' => $total_row],
      'cls' => [],
    ];

    foreach ($data as $i) {

      if (!key_exists($i['cls_id'], $t['cls'])) {
        $t['cls'][$i['cls_id']] = ['name' => $i['cls_abbr']] + $cls_row;
        $t['sum']['cls'][$i['cls_id']] = ['name' => $i['cls_abbr']] + $total_row;
      }

      $cls_point = &$t['cls'][$i['cls_id']];
      $sum_point = &$t['sum']['cls'][$i['cls_id']];

      if (!key_exists($i['id'], $cls_point['parts'])) {
        $cls_point['parts'][$i['id']] = $total_row + ['name' => $i['name']];
      }

      $parts_point = &$cls_point['parts'][$i['id']];

      // Částečný řádek produktu přes typ zákazníka.
      $parts_point[$i['customers_type']] = [
        'pcs' => (int) $i['pcs'],
        'free' => (int) $i['pcs_free'],
        'price' => (float) $i['total'],
      ];

      // Suma řádku produktu přes typ zákazníka
      $parts_point[Table_Customers::TYPE_ALL]['pcs'] += (int) $i['pcs'];
      $parts_point[Table_Customers::TYPE_ALL]['free'] += (int) $i['pcs_free'];
      $parts_point[Table_Customers::TYPE_ALL]['price'] += (float) $i['total'];

      // Suma přes kolekce.
      $sum_point[$i['customers_type']]['price'] += (float) $i['total'];
      $sum_point[$i['customers_type']]['pcs'] += (int) $i['pcs'];
      $sum_point[$i['customers_type']]['free'] += (int) $i['pcs_free'];

      // Suma celková přes kolekce.
      $sum_point[Table_Customers::TYPE_ALL]['price'] += (float) $i['total'];
      $sum_point[Table_Customers::TYPE_ALL]['pcs'] += (int) $i['pcs'];
      $sum_point[Table_Customers::TYPE_ALL]['free'] += (int) $i['pcs_free'];

      // Suma přes typy zakákazíků.
      $t['sum']['total'][$i['customers_type']]['price'] += (float) $i['total'];
      $t['sum']['total'][$i['customers_type']]['pcs'] += (int) $i['pcs'];
      $t['sum']['total'][$i['customers_type']]['free'] += (int) $i['pcs_free'];

      // Suma celková.
      $t['sum']['total'][Table_Customers::TYPE_ALL]['price'] += (float) $i['total'];
      $t['sum']['total'][Table_Customers::TYPE_ALL]['pcs'] += (int) $i['pcs'];
      $t['sum']['total'][Table_Customers::TYPE_ALL]['free'] += (int) $i['pcs_free'];
    }

    return $t;
  }

  protected function transformAccessoriesSalesData($data): array {

    $row = ['pcs' => 0, 'free' => 0, 'avg' => 0.0, 'price' => 0.0];
    $total_row = [
      Table_Customers::TYPE_B2B => $row,
      Table_Customers::TYPE_B2C => $row,
      Table_Customers::TYPE_ALL => $row
    ];

    $t = [
      'sum' => $total_row,
      'parts' => [],
    ];

    foreach ($data as $i) {

      if (!key_exists($i['id'], $t['parts'])) {
        $t['parts'][$i['id']] = ['name' => $i['name']] + $total_row;
      }

      $parts_point = &$t['parts'][$i['id']];

      // Částečný řádek produktu přes typ zákazníka.
      $parts_point[$i['customers_type']] = [
        'pcs' => (int) $i['pcs'],
        'free' => (int) $i['pcs_free'],
        'avg' => (float) $i['price_avg'],
        'price' => (float) $i['total'],
      ];

      // Suma řádku produktu přes typ zákazníka
      $parts_point[Table_Customers::TYPE_ALL]['pcs'] += (int) $i['pcs'];
      $parts_point[Table_Customers::TYPE_ALL]['free'] += (int) $i['pcs_free'];
      $parts_point[Table_Customers::TYPE_ALL]['avg'] += (int) $i['price_avg'];
      $parts_point[Table_Customers::TYPE_ALL]['price'] += (float) $i['total'];


      // Suma přes typy zakákazíků.
      $t['sum'][$i['customers_type']]['pcs'] += (int) $i['pcs'];
      $t['sum'][$i['customers_type']]['free'] += (int) $i['pcs_free'];
      $t['sum'][$i['customers_type']]['avg'] += (float) $i['price_avg'];
      $t['sum'][$i['customers_type']]['price'] += (float) $i['total'];
      $t['sum'][Table_Customers::TYPE_ALL]['pcs'] += (int) $i['pcs'];
      $t['sum'][Table_Customers::TYPE_ALL]['free'] += (int) $i['pcs_free'];
      $t['sum'][Table_Customers::TYPE_ALL]['avg'] += (float) $i['price_avg'];
      $t['sum'][Table_Customers::TYPE_ALL]['price'] += (float) $i['total'];
    }

    return $t;
  }

  const REQUESTED_PRODUCTS_STATES = [
    Table_Orders::STATUS_NEW,
    Table_Orders::STATUS_CONFIRMED,
    Table_Orders::STATUS_OPEN,
    'all',
  ];

  public static function translateOrderStates($state) {
    if (isset(Table_Orders::$statusToString[$state])) {
      return  Table_Orders::$statusToString[$state];
    }
    elseif ($state === 'all') {
      return 'Všechny';
    }
    else {
      return NULL;
    }
  }

  public function getRequestedProducts(): array {
    $table = new Table_OrdersProducts();
    $data = $table->getTotalRequested();
    return $this->transformRequestedProductsData($data);
  }

  protected function transformRequestedProductsData($data): array {

    if (empty($data)) {
      return [];
    }

    $row = array_fill_keys(self::REQUESTED_PRODUCTS_STATES, 0);

    $t = ['parts' => [], 'sum' => $row];

    foreach ($data as $item) {
      if (!key_exists($item['part_id'], $t['parts'])) {
        $t['parts'][$item['part_id']] = $row + ['name' => $item['part_name']];
      }

      $ptr = &$t['parts'][$item['part_id']];

      $ptr[$item['order_status']] += $item['pcs'];
      $ptr['all'] += $item['pcs'];
      $t['sum'][$item['order_status']] += $item['pcs'];
      $t['sum']['all'] += $item['pcs'];
    }

    return $t;
  }

  public function getRequestedServices(): array {
    $table = new Table_OrdersServices();
    $data = $table->getTotalRequested();
    return $this->transformRequestedServicesData($data);
  }

  protected function transformRequestedServicesData($data): array {
    if (empty($data)) {
      return [];
    }
    $row = array_fill_keys(self::REQUESTED_PRODUCTS_STATES, 0);
    $t = ['services' => [], 'sum' => $row];

    foreach ($data as $item) {
      $hash = md5($item['desc']);
      if (!key_exists($hash, $t['services'])) {
        $t['services'][$hash] = $row + ['desc' => $item['desc']];
      }
      $ptr = &$t['services'][$hash];
      $ptr[$item['order_status']] += $item['qty'];
      $ptr['all'] += $item['qty'];

      $t['sum'][$item['order_status']] += $item['qty'];
      $t['sum']['all'] += $item['qty'];
    }

    return $t;
  }

  protected function transformServicesData($data): array {
    $row = ['qty' => 0, 'price' => 0.0];
    $t = ['services' => [], 'sum' => [
      Table_Customers::TYPE_B2B => $row,
      Table_Customers::TYPE_B2C => $row,
      Table_Customers::TYPE_ALL => $row,
    ]];

    if (!empty($data)) {
      foreach ($data as $item) {
        $t['services'][] = $item;
        $t['sum'][$item['customer_type']]['qty'] += (int) $item['qty'];
        $t['sum'][Table_Customers::TYPE_ALL]['qty'] += (int) $item['qty'];
        $t['sum'][$item['customer_type']]['price'] += (float) $item['total'];
        $t['sum'][Table_Customers::TYPE_ALL]['price'] += (float) $item['total'];
      }
    }
    return $t;
  }

  /**
   * @throws Parts_Exceptions_BadWH
   * @throws PHPExcel_Exception
   */
  public static function generateCompleteTobaccoExport() {

    $price_sum = 0;

    // Macerace.
    $tMacaretions = new Table_Macerations();
    $barrels = $tMacaretions->getBarrels(NULL, FALSE, TRUE);

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator('Medite - Warehouse');
    $objPHPExcel->getProperties()->setLastModifiedBy('Medite - Warehouse');
    $objPHPExcel->getProperties()->setTitle('EXPORT SKLADU K DATU ' . date('d.m.Y'));
    $objPHPExcel->getProperties()->setSubject('EXPORT SKLADU K DATU ' . date('d.m.Y'));
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setTitle('Simple');

    $total_weight = 0;
    $total_price = 0;

    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(0, 1, 'EXPORT SKLADU K DATU '
        . date('d.m.Y') . ' (celkový stav)');

    $row = 3;

    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(0, $row, "KATEGORIE: MACERÁTY");
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow($col = 0, ++$row, "Šarže")
      ->setCellValueByColumnAndRow(++$col, $row, "Akt. váha")
      ->setCellValueByColumnAndRow(++$col, $row, "Jedn. cena")
      ->setCellValueByColumnAndRow(++$col, $row, "Celk. cena")
      ->setCellValueByColumnAndRow(++$col, $row, "Název");

    foreach ($barrels as $barrel) {
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow($col = 0, ++$row, $barrel['batch'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['weight'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['unit'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['barrel_current'])
        ->setCellValueByColumnAndRow(++$col, $row, $barrel['name']);

      $total_weight += $barrel['weight'];
      $total_price += $barrel['barrel_current'];
    }

    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(0, ++$row, "Celkem: ");
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(1, $row, $total_weight);
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(3, $row, $total_price);


    $price_sum += $total_price;

    // Suroviny, material, kolky, produkty.
    $wh = Table_TobaccoWarehouse::WH_TYPE;
    $tParts = new Table_Parts();
    $parts = $tParts->getPartsForExport($wh);
    $tPartsCtg = new Table_PartsCtg();


    $total_price = 0;

    $last_ctg = NULL;

    foreach ($parts as $part) {
      $part_obj = new Parts_Part($part['id']);
      if ($last_ctg === null || $last_ctg != $part['id_parts_ctg']) {

        if ($last_ctg) {
          $objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(0, ++$row, "Celkem: ");
          $objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(1, $row, $category_amount);
          $objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(3, $row, $category_price);

          if ($last_ctg !== Table_TobaccoWarehouse::CTG_DUTY_STAMPS) {
            $total_price += $category_price;
          }
        }

        $category_price = 0;
        $category_amount = 0;

        $row += 2;
        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow(0, $row, "KATEGORIE: ".$tPartsCtg->getCategoryName($part['id_parts_ctg']));
        $parentCtg = $tPartsCtg->getParentCategory($part['id_parts_ctg']);
        $unit = ($part['id_parts_ctg'] == Table_TobaccoWarehouse::CTG_MATERIALS || $parentCtg == Table_TobaccoWarehouse::CTG_MATERIALS) ? 'kg' : 'ks';
        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow($col = 0, ++$row, "ID")
          ->setCellValueByColumnAndRow(++$col, $row, "Skladem [" . $unit . "]");

        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow(++$col, $row, "Cena [Kč/" . $unit . "]")
          ->setCellValueByColumnAndRow(++$col, $row, "Cena");

        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow(++$col, $row, "Název");
      }

      $price = $part_obj->getPrice();
      $partAmount = ($part['measure'] == Tobacco_Parts_Part::WH_MEASURE_WEIGHT) ? $part['amount'] * 0.001 :  $part['amount'];
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow($col = 0, ++$row, $part['id'])
        ->setCellValueByColumnAndRow(++$col, $row, $partAmount);

      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(++$col, $row, $price)
        ->setCellValueByColumnAndRow(++$col, $row, $partAmount * $price);

    $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(++$col, $row, $part['name']);

      $category_price += $partAmount*$price;
      $category_amount += $partAmount;
      $last_ctg = $part['id_parts_ctg'];
    }

    if ($category_price) {
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(0, ++$row, "Celkem: ");
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(1, $row, $category_amount);
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(3, $row, $category_price);

      if ($last_ctg !== Table_TobaccoWarehouse::CTG_DUTY_STAMPS) {
        $total_price += $category_price;
      }
    }

    $row += 2;

    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(0, $row, "Hodnota skladu (bez kolků):");
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(3, $row, $price_sum + $total_price);


    $category_price = 0;
    $category_amount = 0;

    $row += 2;
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(0, $row, "KATEGORIE: KOLKY");
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow($col = 0, ++$row, "ID")
      ->setCellValueByColumnAndRow(++$col, $row, "Skladem [ks]");

    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(++$col, $row, "Cena [Kč/ks]")
      ->setCellValueByColumnAndRow(++$col, $row, "Cena");

    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(++$col, $row, "Název");

    $parts = $tParts->getPartsByCategory(Table_TobaccoWarehouse::CTG_DUTY_STAMPS, FALSE, FALSE, Table_TobaccoWarehouse::WH_TYPE);
    foreach ($parts as $part) {
      $part_obj = new Parts_Part($part['id']);
      $price = $part_obj->getPrice();
      $partAmount = $part['amount'];
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow($col = 0, ++$row, $part['id'])
        ->setCellValueByColumnAndRow(++$col, $row, $partAmount);

      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(++$col, $row, $price)
        ->setCellValueByColumnAndRow(++$col, $row, $partAmount * $price);

      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(++$col, $row, $part['name']);

      $category_price += $partAmount*$price;
      $category_amount += $partAmount;
    }

    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(0, ++$row, "Celkem: ");
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(1, $row, $category_amount);
    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(3, $row, $category_price);


    $objPHPExcel->getActiveSheet()->getStyle('B1:B' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.000');
    $objPHPExcel->getActiveSheet()->getStyle('C1:C' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('D1:D' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.00');

    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

    return $objPHPExcel;
  }
}