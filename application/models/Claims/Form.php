<?php
class Claims_Form extends Meduse_FormBootstrap {
	
    public function init() {
    	
		$this->setMethod(Zend_Form::METHOD_POST)
				->setAttrib('class', 'form')
				->setLegend('Nová reklamace');
    	
    	$element = new Meduse_Form_Element_Text('meduse_claim_name');
		$element->setRequired(true);
		$element->setLabel('Název firmy');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Text('meduse_claim_desc');
		$element->setLabel('Poznámka');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Text('meduse_claim_id_parts');
		$element->setRequired(true);
		$element->setLabel('ID součásti');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Text('meduse_claim_amount');
		$element->setRequired(true);
		$element->setLabel('Počet');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_DatePicker(
			'meduse_claim_date_from',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->setRequired(true);
		$element->setLabel('Od (DD.MM.RRRR)');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_DatePicker(
			'meduse_claim_date_to',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->setLabel('Do (DD.MM.RRRR)');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);
		
		parent::init();
    }
    
}
