<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Employees_RewardForm extends Meduse_FormBootstrap {

    public function setReward(Employees_Reward $reward) {
      $this->getElement('id_employees_rewards')->setValue($reward->getEmployee()->getId());
      $this->getElement('year_employees_rewards')->setValue($reward->getDate()->get('Y'));
      $this->getElement('month_employees_rewards')->setValue($reward->getDate()->get('m'));
    }

    /**
     * Inicializace formulare.
     */
    public function init() {

      $this->_isModalForm = TRUE;

      $eUserId = new Zend_Form_Element_Hidden('id_users');
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $eUserId->setValue($authNamespace->user_id);
      $this->addElement($eUserId);

      $eEmployeeId = new Zend_Form_Element_Hidden('id_employees_rewards');
      $this->addElement($eEmployeeId);

      $eYear = new Zend_Form_Element_Hidden('year_employees_rewards');
      $this->addElement($eYear);

      $eMonth = new Zend_Form_Element_Hidden('month_employees_rewards');
      $this->addElement($eMonth);

      $eDescription = new Meduse_Form_Element_Text('description');
      $eDescription->setLabel('Popis');
      $this->addElement($eDescription);

      $eValue = new Meduse_Form_Element_Currency('value');
      $eValue->setLabel('Částka');
      $this->addElement($eValue);

      parent::init();
    }
  }
