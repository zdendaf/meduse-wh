<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Employees_AbsenceForm extends Meduse_FormBootstrap {
    
    public function init() {

      $eId = new Zend_Form_Element_Hidden('id_employees');
      
      $eStart = new Meduse_Form_Element_DatePicker('start');
      $eStart
        ->setRequired(TRUE)
        ->setLabel('Začátek');
      
      $eEnd = new Meduse_Form_Element_DatePicker('end');
      $eEnd
        ->setRequired(TRUE)
        ->setLabel('Konec');
      
      $eType = new Meduse_Form_Element_Select('type');
      $eType
        ->setRequired(TRUE)
        ->setLabel('Typ')
        ->addMultiOptions(array(
          NULL => '-- zvolte --',
          Table_EmployeesAbsence::TYPE_ILLNESS => 'nemoc',
          Table_EmployeesAbsence::TYPE_FREE => 'neplacené volno',
          Table_EmployeesAbsence::TYPE_BANNED => 'neomluvená absence',
          Table_EmployeesAbsence::TYPE_OTHER => 'jiná',
        ));
      
      $eDescription = new Meduse_Form_Element_Text('description');
      $eDescription
        ->setLabel('Důvod');
      
      $eSubmit = new Meduse_Form_Element_Submit('Odeslat');
      
      $this
        ->addAttribs(array('class' => 'form'))
        ->addElement($eId)
        ->addElement($eStart)
        ->addElement($eEnd)
        ->addElement($eType)
        ->addElement($eDescription)
        ->addElement($eSubmit);
      
      parent::init();
      
    }
    
  }

  