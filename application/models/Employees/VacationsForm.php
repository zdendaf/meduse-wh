<?php

class Employees_VacationsForm extends Meduse_FormBootstrap
{

  protected ?Employees_Reward $_reward = NULL;
  public function __construct($options = null) {
    if (isset($options['reward']) && $options['reward'] instanceof Employees_Reward) {
      $this->_reward = clone $options['reward'];
    }
    unset($options['reward']);
    parent::__construct($options);
  }

  /**
   * @throws Zend_Validate_Exception
   * @throws Zend_Form_Exception
   * @throws Zend_Db_Table_Exception
   */
  public function init()
  {

    $this->_isModalForm = TRUE;

    $year = (int) $this->_reward->getDate()->get('Y');
    $employee = $this->_reward->getEmployee();
    $vacationSum = $this->_reward->getVacation();
    $rest = $employee->getVacationRest($year) - $vacationSum;
    $rest = ($rest > 0) ? $rest : 0;

    $claimed = $employee->getVacation($year + 1, 'claimed');
    $transferred = $employee->getVacation($year + 1, 'transferred');

    $element = new Zend_Form_Element_Hidden('worker_id');
    $element->setValue($this->_reward->getEmployee()->getId());
    $this->addElement($element);

    $element = new Zend_Form_Element_Hidden('year');
    $element->setValue($year);
    $this->addElement($element);

    $element = new Zend_Form_Element_Hidden('month');
    $element->setValue('12');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Float('claimed');
    $element->setLabel('Sjednaný nárok na rok ' . ($year + 1));
    $element->setValue($claimed);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Float('transferred');
    $element->setLabel('Převést z roku ' . $year . ' (max. ' . $rest  . ' d)');
    $element->setValue($transferred ?? 0);
    $element->addValidator(new Zend_Validate_LessThan(['max' => $rest + 0.5]));
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('save');
    $element->setLabel('Nastavit');
    $this->addElement($element);

    parent::init();
  }
}