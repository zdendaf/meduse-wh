<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Employees_EditForm extends Meduse_FormBootstrap {

    protected $_onlyForEmployee = array();

    public function init() {

      $this->setAttrib('class', 'form');

      $element = new Zend_Form_Element_Hidden('type');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('id_users');
      $element->setLabel('Uživatel');

      $table = new Table_Users();
      $users = array('0' => 'bez návaznosti');
      foreach($table->getList(FALSE, FALSE) as $user) {
        $users[$user['id']] = $user['name_full'];
      }
      $element->setMultiOptions($users);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('alveno');
      $element->addValidator(new Zend_Validate_Regex('/^[0-9]{1,3}$/i'));
      $element->setLabel('Alveno ID');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('first_name');
      $element->setRequired(TRUE);
      $element->setLabel('Křestní jméno');
      $element->addValidator(new Zend_Validate_StringLength(0, 50));
      $element->setAttrib('maxlength', 50);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('last_name');
      $element->setRequired(TRUE);
      $element->setLabel('Příjmení jméno');
      $element->addValidator(new Zend_Validate_StringLength(0, 50));
      $element->setAttrib('maxlength', 50);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Checkbox('active');
      $element->setLabel('Aktivní');
      $this->addElement($element);

      $element = new Meduse_Form_Element_DatePicker('birthday');
      $element->setLabel('Datum narození:');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('birthplace');
      $element->addValidator(new Zend_Validate_StringLength(0, 50));
      $element->setAttrib('maxlength', 50);
      $element->setLabel('Místo narození');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Text('insurance_code');
      $element->addValidator(new Zend_Validate_StringLength(3, 3));
      $element->setAttrib('maxlength', 3);
      $element->setLabel('Kód. zdr. pojišťovny');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Text('account_number');
      $element->addValidator(new Zend_Validate_StringLength(0, 50));
      $element->setAttrib('maxlength', 50);
      $element->setLabel('Číslo bankovního účtu');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Phone('private_phone');
      $element->addValidator(new Zend_Validate_StringLength(0, 20));
      $element->setAttrib('maxlength', 20);
      $element->setLabel('Soukromý telefon');
      $this->addElement($element);


      $element = new Meduse_Form_Element_Text('private_email');
      $element->addValidator(new Zend_Validate_EmailAddress());
      $element->addValidator(new Zend_Validate_StringLength(0, 50));
      $element->setAttrib('maxlength', 50);
      $element->setLabel('Soukromý email');
      $this->addElement($element);

      $tDepartments = new Table_Departments();
      $options = $tDepartments->getSelectOptions('── zvol ──');
      $element = new Meduse_Form_Element_Select('id_departments');
      $element->setRequired(TRUE);
      $element->setMultiOptions($options);
      $element->setLabel('Oddělení / Firma');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('position');
      $element->setLabel('Pozice');
      $element->addValidator(new Zend_Validate_StringLength(0, 50));
      $element->setAttrib('maxlength', 50);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('business_email');
      $element->addValidator(new Zend_Validate_EmailAddress());
      $element->addValidator(new Zend_Validate_StringLength(0, 50));
      $element->setAttrib('maxlength', 50);
      $element->setLabel('Firemní email');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Phone('business_phone');
      $element->addValidator(new Zend_Validate_StringLength(0, 20));
      $element->setAttrib('maxlength', 20);
      $element->setLabel('Firemní telefon');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Phone('business_phone_pin');
      $element->setLabel('PIN');
      $element->addValidator(new Zend_Validate_Int());
      $element->addValidator(new Zend_Validate_StringLength(4, 4));
      $element->setAttrib('maxlength', 4);
      $this->addElement($element);


      $element = new Meduse_Form_Element_Text('computer_name');
      $element->setLabel('Název PC');
      $element->addValidator(new Zend_Validate_StringLength(0, 20));
      $element->setAttrib('maxlength', 20);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('computer_login');
      $element->setLabel('OS login');
      $element->addValidator(new Zend_Validate_StringLength(0, 20));
      $element->setAttrib('maxlength', 20);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('computer_password');
      $element->setLabel('OS heslo');
      $element->addValidator(new Zend_Validate_StringLength(0, 20));
      $element->setAttrib('maxlength', 20);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('work_class');
      $element->addMultiOptions(array(
        '1' => 'třída I.', '2' => 'třída II.', '3' => 'třída III.',
      ));
      $element->setRequired(TRUE);
      $element->setLabel('Pracovní třída');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Checkbox('insurance');
      $element->setLabel('Pojištění odpovědnosti');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Checkbox('nda');
      $element->setLabel('Dohoda o mlčenlivosti');
      $this->addElement($element);

      $element = new Meduse_Form_Element_DatePicker('start');
      $element->setLabel('Začátek PP:');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Checkbox('not_end');
      $element->setLabel('Smlouva na neurčito');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_DatePicker('end');
      $element->setLabel('Konec PP:');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('obligation');
      $element->setLabel('Měsíční úvazek');
      $element->addMultiOptions(array(
        '1/1' => 'plný',
        '1/2' => 'poloviční',
        '4/5' => 'čtyřpětinový',
      ));
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Checkbox('part_time');
      $element->setLabel('Zkrácený úvazek');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Currency('wage');
      $element->setLabel('Základ mzdy [Kč]');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('bonus');
      $element->setLabel('Bonus [Kč]');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Currency('transport_allowance');
      $element->setLabel('Příspěvek na dopravu [Kč]');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Currency('diets');
      $element->setLabel('Výše zahr. diet [Eur]');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Currency('vacation');
      $element->setLabel('Nárok dovolené [dnů/rok]');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Checkbox('reimburse_holidays');
      $element->setLabel('Proplácet svátky');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Currency('overtime_rate');
      $element->setLabel('Přesčas sazba [Kč]');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Currency('overtime_extra_rate');
      $element->setLabel('Přesčas extra sazba [Kč]');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Select('rounding');
      $element->setLabel('Způsob zaokrouhlování');
      $element->addMultiOptions(Table_Employees::$roundingNames);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('Uložit');
      $this->addElement($element);

      parent::init();
    }

    public function populate(array $values) {
      parent::populate($values);
    }

    protected function setOnlyForEmployee(Zend_Form_Element $element) {
      $this->_onlyForEmployee[] = $element->getName();
    }

    public function setType($type) {
      $this->getElement('type')->setValue($type);
      if ($type === Table_Employees::TYPE_SUPPLIER) {
        foreach ($this->_onlyForEmployee as $name) {
          $this->removeElement($name);
        }
        $this->getElement('wage')->setLabel('Hodinová sazba [Kč]');
        $this->getElement('vacation')->setLabel('Nárok placeného volna [dnů/rok]');
        $this->getElement('start')->setLabel('Platnost smlouvy od:');
        $this->getElement('end')->setLabel('Platnost smlouvy do:');
      }
      else {
        $this->removeElement('reimburse_holidays');
      }
    }
  }

