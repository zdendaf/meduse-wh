<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Employees_WorkSheet {

    const DAY_STATUS_EMPTY = 0;
    const DAY_STATUS_FAIL = 1;
    const DAY_STATUS_OK = 2;

    const ROUNDING_PRECISION = 15;
    const ROUNDING_TOLERANCE = 4;

    const STATUS_START = 'start';
    const STATUS_END = 'end';

    const MAX_OVERTIME = 40;

    const UNIT_DAY = 'day';
    const UNIT_HOUR = 'hour';
    const UNIT_MINUTE = 'minute';

    const LUNCH_LIMIT = 6;

    /**
     * Pocet hodin v pracovnim dnu.
     */
    const HOURS_IN_DAY = 8;

    protected $_work = NULL;
    protected $_date = NULL;
    protected $_calendar = array();
    protected $_view = NULL;

    protected $_holidays = array();
    protected $_changes = array();

    /**
     * Pocet spravne vykazanych hodin
     * @var float
     */
    protected $_total = 0;

    protected $_overtime = 0;

    protected $_vacation = 0;

    protected $_sickness = 0;

    protected $_fmc = 0;

    protected $_unpaid_vacation = 0;

    /**
     * Status celeho worksheetu.
     * @var int
     */
    protected $_status = self::DAY_STATUS_EMPTY;

    /**
     * Pocet hodin v mesici.
     * @var int
     */
    protected $_hours = 0;

    /**
     * @var int Pocet najetych kilometru v mesici
     */
    protected $_km = 0;


    /**
     * @var float Diety za sluzebni ceste v CR/SR [CZE]
     */
    protected $_diets = 0;

    /**
     * @var float Diety za sluzebni cesty mimo CR/SR [EUR]
     */
    protected $_diets_frgn = 0;

    public $options = array(
      'table_class' => 'table table-bordered table-hover table-condensed',
      'show_overtime' => TRUE,
    );

    public function __construct(Employees_Work $work) {
      $this->_work = $work;
      $this->_date = $work->getDate();
      $this->_view = Zend_Layout::getMvcInstance()->getView();
      $this->init();
    }

    public function init() {

      $settings = new Table_Settings();

      // Nastaveni dnu --------------------------------------------------------
      switch ($this->_date->get('m')) {
        case '04': case '06': case '09': case '11':
          $numDays = 30;
          break;
        case '02':
          $numDays = $this->_date->isLeapYear()? 29 : 28;
          break;
        default:
          $numDays = 31;
      }

      // zjisteni celofiremnich volnych dnu a presunu
      $tHolidays = new Table_EmployeesHolidays();
      $holidays = $tHolidays->getHolidays($this->_date->get('Y'), FALSE);

      if ($holidays) {
        foreach ($holidays as $idx => $day) {
          $this->_holidays[] = $idx;
          if ($day['change']) {
            $this->_changes[$day['change']] = $idx;
          }
        }
      }

      $date = new Meduse_Date($this->_date->toString('Y-m-01'));
      $employeeId = $this->_work->getEmployeeId();
      $employee = new Employees_Employee($employeeId);
      for ($day = 1; $day <= $numDays; $day++) {
        $change = array_key_exists($date->get('Y-m-d'), $this->_changes);
        $holiday = ($date->isHoliday() || in_array($date->get('Y-m-d'), $this->_holidays)) && !$change;
        $freeday = $date->isFreeday();
        if (!$holiday && !$freeday) {
          $this->_hours += self::HOURS_IN_DAY;
        }
        $idx = $date->toString('Y-m-d');
        $this->_calendar[$idx] = array(
          'day' => (int) $date->toString('N'),
          'date' => $date->toString('D d.m.'),
          'holiday' => $holiday,
          'freeday' => $freeday,
          'work' => array(),
          'lunch_disabled' => FALSE,
          'status' => self::DAY_STATUS_EMPTY,
          'total' => 0,
          'overtime' => 0,
          'km' => 0,
          'diets' => 0,
          'diets_frgn' => 0,
          'vacation' => 0,
          'sickness' => 0,
          'fmc' => 0,
          'unpaid_vacation' => 0,
          'placeholder' => $change ? 'pracovní den nahrazený ' . Meduse_Date::dbToForm($this->_changes[$idx]) : NULL,
          'reimbused' => FALSE,
        );
        if ($holiday && $date->get('w') != 0) {
          $this->_calendar[$idx]['placeholder'] =
            (isset($holidays[$date->get('Y-m-d')]) && !is_null($holidays[$date->get('Y-m-d')]['change'])) ? 'volno jako náhrada za ' . Meduse_Date::dbToForm($holidays[$date->get('Y-m-d')]['change']) :
            (in_array($date->get('Y-m-d'), $this->_holidays) ? 'firemní dovolená' : 'státní svátek');
        }
        $date->addDay(1);
      }

      // Vyplneni pracovnimi zaznamy ------------------------------------------
      foreach ($this->_work->getWork() as $work) {
        $idx = $work['date'];
        $this->_calendar[$idx]['work'] = array(
          'id' => $work['id'],
          'start' => $work['start'] ? $work['start'] : NULL,
          'original_start' => NULL,
          'end' => $work['end'] ? $work['end'] : NULL,
          'lunch' => $work['lunch'] == 'y' ? TRUE : FALSE,
          'original_end' => NULL,
          'type' => $work['type'],
          'km' => $work['km'] ? (float) $work['km'] : '',
          'description' => $work['description'],
          'user' => $work['id_users'],
        );
      }


      // Vyplneni originalnimi zaznamy ze ctecky ------------------------------
      $tRecords = new Table_EmployeesRecords();
      $originals = $tRecords->getRecords($employeeId, $this->_date);

      foreach ($originals as $idx => $record) {
        if (empty($this->_calendar[$idx]['work'])) {
          $this->_calendar[$idx]['work'] = array (
          'id' => NULL,
          'start' => NULL,
          'original_start' => NULL,
          'end' => NULL,
          'lunch' => FALSE,
          'original_end' => NULL,
          'type' => NULL,
          'km' => NULL,
          'description' => NULL,
          'user' => NULL,
          );
        }
        if (is_null($this->_calendar[$idx]['work']['start'])) {
          $this->_calendar[$idx]['work']['start'] = $record['start'] ? $this->round($record['start'], self::STATUS_START, $employee->getRounding()) : NULL;
        }
        if (is_null($this->_calendar[$idx]['work']['end'])) {
          $this->_calendar[$idx]['work']['end'] = $record['end'] ? $this->round($record['end'], self::STATUS_END, $employee->getRounding()) : NULL;
        }
        if (is_null($this->_calendar[$idx]['work']['type'])) {
          $this->_calendar[$idx]['work']['type'] = Table_EmployeesRecords::TYPE_WORK;
        }
        $this->_calendar[$idx]['work']['original_start'] = $record['start'];
        $this->_calendar[$idx]['work']['original_end'] = $record['end'];
      }

      // Vypocet pro kazdy den ------------------------------------------------
      foreach ($this->_calendar as $idx => $day) {

        $duration = 0;
        $overtime = 0;

        // pokud neni volno, musi u kazdeho dne byt neco vyplneneho
        $this->_calendar[$idx]['status'] = self::DAY_STATUS_FAIL;

        if (!empty($day['work'])) {

          if ((empty($day['work']['start']) || empty($day['work']['end']))) {
            if ((isset($day['work']['type']) && in_array($day['work']['type'], array(
                Table_EmployeesRecords::TYPE_VACATION,
                Table_EmployeesRecords::TYPE_SICKNESS,
                Table_EmployeesRecords::TYPE_FMC,
                Table_EmployeesRecords::TYPE_FUNERAL,
                Table_EmployeesRecords::TYPE_OVERTIME_DRAW,
                Table_EmployeesRecords::TYPE_HOLIDAY,
              )))
              ||
              ($employee->isSupplier() && isset($day['work']['type']) && $day['work']['type'] === Table_EmployeesRecords::TYPE_HALF_VACATION )) {
              $this->_calendar[$idx]['status'] = self::DAY_STATUS_OK;
            }
          }

          // zjisteni delky prace
          if (isset($day['work']['start']) && isset($day['work']['end'])) {
            $start = new Meduse_Date($day['work']['start']);
            $end = new Meduse_Date($day['work']['end']);
            $duration = $end->getTimestamp() - $start->getTimestamp();
            if ($duration >= self::LUNCH_LIMIT * 3600) {
              $this->_calendar[$idx]['lunch_disabled'] = TRUE;
              $day['lunch_disabled'] = TRUE;
            }
          }

          // vypocet ciste doby a prescasu a kontrola dle typu prace
          switch ($day['work']['type']) {

            // neplacene volno
            case Table_EmployeesRecords::TYPE_UNPAID_VACATION:
              $this->_calendar[$idx]['lunch_disabled'] = TRUE;
              $this->_calendar[$idx]['work']['lunch'] = FALSE;

              if ($this->_calendar[$idx]['freeday'] || $this->_calendar[$idx]['holiday']){
                $this->_calendar[$idx]['total'] = - self::HOURS_IN_DAY;;
              }
              else {
                $this->_calendar[$idx]['total'] = 0;
              }
              $this->_calendar[$idx]['work']['start'] = NULL;
              $this->_calendar[$idx]['work']['end'] = NULL;
              $this->_calendar[$idx]['work']['km'] = NULL;
              $this->_calendar[$idx]['unpaid_vacation'] = self::HOURS_IN_DAY;
              $this->_calendar[$idx]['status'] = self::DAY_STATUS_OK;
              break;

            // dovolena
            case Table_EmployeesRecords::TYPE_VACATION:
            case Table_EmployeesRecords::TYPE_FUNERAL:
              $this->_calendar[$idx]['lunch_disabled'] = TRUE;
              $this->_calendar[$idx]['work']['lunch'] = FALSE;
              $minutes = $duration;
              if (!$this->_calendar[$idx]['freeday'] && !$this->_calendar[$idx]['holiday']) {
                $minutes += self::HOURS_IN_DAY * 3600;
              }
              $this->_calendar[$idx]['total'] = round($minutes / 3600, 2);
              $this->_calendar[$idx]['status'] = self::DAY_STATUS_OK;
              // prace pri nemoci, dovolene nebo pri cerpanim prescasu je prescasem
              if (isset($day['work']['start']) && isset($day['work']['end'])) {
                $this->_calendar[$idx]['overtime'] = $duration / 3600;
              }
              // pohreb se nezapocitava do celkove doby dovolene
              if ($day['work']['type'] === Table_EmployeesRecords::TYPE_VACATION) {
                $this->_calendar[$idx]['vacation'] = self::HOURS_IN_DAY;
              }
              break;

            // nemocenska
            case Table_EmployeesRecords::TYPE_SICKNESS:
              $this->_calendar[$idx]['lunch_disabled'] = TRUE;
              $this->_calendar[$idx]['work']['lunch'] = FALSE;
              $minutes = $duration;
              $this->_calendar[$idx]['total'] = round($minutes / 3600, 2);
              $this->_calendar[$idx]['status'] = self::DAY_STATUS_OK;
              // prace pri nemoci je prescasem
              if (isset($day['work']['start']) && isset($day['work']['end'])) {
                $this->_calendar[$idx]['overtime'] = $duration / 3600;
              }
              if ($day['work']['type'] === Table_EmployeesRecords::TYPE_SICKNESS) {
                $this->_calendar[$idx]['sickness'] = self::HOURS_IN_DAY;
              }
              else {
                $this->_calendar[$idx]['fmc'] = self::HOURS_IN_DAY;
              }
              break;

            // OCR
            case Table_EmployeesRecords::TYPE_FMC:
              $this->_calendar[$idx]['lunch_disabled'] = TRUE;
              $this->_calendar[$idx]['work']['lunch'] = FALSE;
              $minutes = $duration;
              if (!$this->_calendar[$idx]['freeday'] && !$this->_calendar[$idx]['holiday']) {
                $minutes += self::HOURS_IN_DAY * 3600;
              }
              $this->_calendar[$idx]['total'] = round($minutes / 3600, 2);
              $this->_calendar[$idx]['status'] = self::DAY_STATUS_OK;
              // prace pri ocr je prescasem
              if (isset($day['work']['start']) && isset($day['work']['end'])) {
                $this->_calendar[$idx]['overtime'] = $duration / 3600;
              }
              if ($day['work']['type'] === Table_EmployeesRecords::TYPE_SICKNESS) {
                $this->_calendar[$idx]['sickness'] = self::HOURS_IN_DAY;
              }
              else {
                $this->_calendar[$idx]['fmc'] = self::HOURS_IN_DAY;
              }
              break;

            // cerpani prescasu
            case Table_EmployeesRecords::TYPE_OVERTIME_DRAW:
              $this->_calendar[$idx]['lunch_disabled'] = TRUE;
              $this->_calendar[$idx]['work']['lunch'] = FALSE;
              $minutes = $duration;
              $this->_calendar[$idx]['overtime'] = - self::HOURS_IN_DAY;
              $this->_calendar[$idx]['total'] = round($minutes / 3600, 2);
              $this->_calendar[$idx]['status'] = self::DAY_STATUS_OK;
              // prace pri cerpani prescasu je prescasem
              if (isset($day['work']['start']) && isset($day['work']['end'])) {
                $this->_calendar[$idx]['overtime'] += $duration / 3600;
              }
              break;

            case Table_EmployeesRecords::TYPE_HOLIDAY:
              $this->_calendar[$idx]['lunch_disabled'] = TRUE;
              $this->_calendar[$idx]['work']['lunch'] = FALSE;

            case Table_EmployeesRecords::TYPE_BUSINESS_TRIP:
            case Table_EmployeesRecords::TYPE_BUSINESS_TRIP_FOREIGN:
            case Table_EmployeesRecords::TYPE_WORK:
            case Table_EmployeesRecords::TYPE_HOMEOFFICE:
            case Table_EmployeesRecords::TYPE_HALF_VACATION:

              // odecteni 1/2 hodiny na obed
              if ($day['work']['lunch'] || $day['lunch_disabled']) {
                $duration = $duration > 1800 ? $duration - 1800 : 0;
              }

              // celkova doba ze zaznamu
              $this->_calendar[$idx]['total'] = round($duration / 3600, 2);

              // pul pracovni doby pri polovine dne placeneho volna
              if ($day['work']['type']  == Table_EmployeesRecords::TYPE_HALF_VACATION) {
                $this->_calendar[$idx]['total'] += self::HOURS_IN_DAY/2;
              }
              // cela pracovni doba pri statnim svatku pro OSVC
              if ($day['work']['type'] == Table_EmployeesRecords::TYPE_HOLIDAY) {
                $this->_calendar[$idx]['total'] += self::HOURS_IN_DAY;
              }

              if ($duration) {
                $this->_calendar[$idx]['status'] = self::DAY_STATUS_OK;
                // byla (zahr) sluzebni cesta?
                if ($day['work']['type'] == Table_EmployeesRecords::TYPE_BUSINESS_TRIP) {
                  if ($duration >= 18 * 3600) {
                    $this->_calendar[$idx]['diets'] = $settings->get('employees_diets_rate18');
                  }
                  elseif ($duration >= 12 * 3600) {
                    $this->_calendar[$idx]['diets'] = $settings->get('employees_diets_rate12');
                  }
                  elseif ($duration >= 5 * 3600) {
                    $this->_calendar[$idx]['diets'] = $settings->get('employees_diets_rate5');
                  }
                }

                // pri rucne doplnenem casu prace musi byt vyplnena poznamka
                if (!$employee->isSupplier() && $day['work']['user'] && !$day['work']['description'] && $day['work']['type'] != Table_EmployeesRecords::TYPE_HOMEOFFICE) {
                  $this->_calendar[$idx]['status'] = self::DAY_STATUS_FAIL;
                }

                // prace ve volnu je vzdy prescasem
                if (($this->_calendar[$idx]['freeday'] || $this->_calendar[$idx]['holiday'])
                  && in_array($this->_calendar[$idx]['work']['type'], array(
                    Table_EmployeesRecords::TYPE_WORK,
                    Table_EmployeesRecords::TYPE_BUSINESS_TRIP,
                    Table_EmployeesRecords::TYPE_BUSINESS_TRIP_FOREIGN,
                    Table_EmployeesRecords::TYPE_HOMEOFFICE,
                  ))) {
                  $overtime = $duration / 3600;
                }
                else {
                  $overtime = ($duration / 3600) - ($day['work']['type'] == Table_EmployeesRecords::TYPE_HALF_VACATION ? self::HOURS_IN_DAY/2 : self::HOURS_IN_DAY);
                }
                $this->_calendar[$idx]['overtime'] = $overtime;

                if ($day['work']['type'] == Table_EmployeesRecords::TYPE_BUSINESS_TRIP_FOREIGN) {
                  $diets = $employee->getDiets();
                  $this->_calendar[$idx]['diets_frgn'] += $diets ? $diets : $settings->get('employees_diets_ratef');
                }
              }
              if ($day['work']['type'] == Table_EmployeesRecords::TYPE_HALF_VACATION) {
                $this->_calendar[$idx]['vacation'] = self::HOURS_IN_DAY/2;
              }
              break;
            default:

              $this->_calendar[$idx]['status'] = self::DAY_STATUS_FAIL;
              break;
          }

          $this->_calendar[$idx]['km'] = (float) $day['work']['km'];
        }
        else {
          // pokud neni volno, musi u kazdeho dne byt neco vyplneneho
          // toto pravidlo neplati pro osvc a pro zkraceny uvazek
          if ($employee->getType() == Table_Employees::TYPE_SUPPLIER || $employee->isPartTime()) {
            $this->_calendar[$idx]['status'] = self::DAY_STATUS_OK;
          }
          else {
            $this->_calendar[$idx]['status'] = ($day['holiday'] || $day['freeday']) ? self::DAY_STATUS_OK : self::DAY_STATUS_FAIL;
          }
        }

        // pokud je pracovni den svatkem nebo volnem a pokud ma zivnostnik bonus,
        // pak se mu tyto dny proplaci
        if ($day['holiday'] && $day['day'] != 7 && $day['day'] != 6 && $employee->isSupplier() && $employee->isReimbursedHolidays()) {
          $this->_calendar[$idx]['reimbused'] = TRUE;
          $this->_calendar[$idx]['total'] += self::HOURS_IN_DAY;
        }

        if ($this->_calendar[$idx]['status'] === self::DAY_STATUS_OK) {
          $this->_total += $this->_calendar[$idx]['total'];
          $this->_overtime += $this->_calendar[$idx]['overtime'];
          $this->_vacation += $this->_calendar[$idx]['vacation'];
          $this->_sickness += $this->_calendar[$idx]['sickness'];
          $this->_fmc += $this->_calendar[$idx]['fmc'];
          $this->_unpaid_vacation += $this->_calendar[$idx]['unpaid_vacation'];
          $this->_km += $this->_calendar[$idx]['km'];
          $this->_diets += $this->_calendar[$idx]['diets'];
          $this->_diets_frgn += $this->_calendar[$idx]['diets_frgn'];
          if ($this->_status !== self::DAY_STATUS_FAIL) {
            $this->_status = self::DAY_STATUS_OK;
          }
        } elseif ($this->_calendar[$idx]['status'] === self::DAY_STATUS_FAIL) {
          $this->_status = self::DAY_STATUS_FAIL;
        }
      }
      //Zend_Debug::dump($this->_calendar);
    }

    public function __toString() {
      return $this->render();
    }

    public function render(array $options = array()) {

      $options += $this->options;

      $html  = '<div id="employees_worksheet">';
      $html .= '<table class="' . $options['table_class'] . '">';
      $html .= '<thead><tr>';
      $html .= '<th class="text-center">den</th>';
      $html .= '<th>příchod</th>';
      $html .= '<th>odchod</th>';
      $html .= '<th>celkem</th>';
      $html .= '<th>oběd</th>';
      $html .= $options['show_overtime'] ? '<th>přesčas</th>' : '';
      $html .= '<th>typ</th>';
      $html .= '<th>poznámka</th>';
      $html .= '<th>km</th>';
      $html .= '</tr></thead><tbody>';
      $html .= '<input type="hidden" name="employee" value="' . $this->_work->getEmployeeId() . '">';

      foreach ($this->_calendar as $idx => $day) {

        $rowClassArray = array();

        if ($day['freeday']) {
          $rowClassArray[] = 'freeday';
          $rowClassArray[] = 'warning';
        }
        if ($day['holiday']) {
          $rowClassArray[] = 'holiday';
          $rowClassArray[] = 'warning';
        }
        if ($day['status'] == self::DAY_STATUS_FAIL) {
          $rowClassArray[] = 'error';
        }
        if ($day['reimbused']) {
          $rowClassArray[] = 'reimbused';
        }

        $tdWorkStart = '';
        $tdWorkEnd = '';
        $tdLunch = '';
        $tdType = '';
        $tdDescription = '';
        $tdKm = '';


        if ($day['work']) {
          if (isset($day['work']['type'])) {
            switch ($day['work']['type']) {
              case Table_EmployeesRecords::TYPE_VACATION:
              case Table_EmployeesRecords::TYPE_FUNERAL:
              case Table_EmployeesRecords::TYPE_OVERTIME_DRAW:
              case Table_EmployeesRecords::TYPE_UNPAID_VACATION:
                $rowClassArray[] = 'type-vacation';
                break;
              case Table_EmployeesRecords::TYPE_HALF_VACATION:
                $rowClassArray[] = 'type-half-vacation';
                break;
              case Table_EmployeesRecords::TYPE_HOLIDAY:
                $rowClassArray[] = 'type-holiday';
                break;
              case Table_EmployeesRecords::TYPE_SICKNESS:
              case Table_EmployeesRecords::TYPE_FMC:
                $rowClassArray[] = 'type-sickness';
                break;
              case Table_EmployeesRecords::TYPE_BUSINESS_TRIP:
              case Table_EmployeesRecords::TYPE_BUSINESS_TRIP_FOREIGN:
                $rowClassArray[] = 'type-bustrip';
                break;
              case Table_EmployeesRecords::TYPE_HOMEOFFICE:
              case Table_EmployeesRecords::TYPE_WORK:
                $rowClassArray[] = 'type-work';
                break;
            }
          }

          $classes = '';
          $id = $day['work']['id'];
          if ($day['work']['start']) {
            $date = new Meduse_Date($day['work']['start']);
            $value = $date->toString('H:i');
            $classes = $day['work']['user'] && $day['work']['original_start'] != $day['work']['start'] ? 'start changed' : 'start';
          } else {
            $value = '';
          }
          $tdWorkStart .= '<input class="' . $classes . '" data-row-id="' . $id . '" type="text" name="start_' . $idx . '" value="' . $value . '">';
          if ($day['work']['original_start']) {
            $date = new Meduse_Date($day['work']['original_start']);
            $value = $date->toString('H:i');
          }
          else {
            $value = '&ndash;';
          }
          $tdWorkStart .= '<div class="original-data text-right">' . $value . '</div>';
          $classes = '';
          if ($day['work']['end']) {
            $date = new Meduse_Date($day['work']['end']);
            $value = $date->toString('H:i');
            $classes = $day['work']['user'] && $day['work']['original_end'] != $day['work']['end'] ? 'end changed' : 'end';
          } else {
            $value = '';
          }
          $tdWorkEnd .= '<input class="' . $classes . '" data-row-id="' . $id . '" type="text" name="end_' . $idx . '" value="' . $value . '">';
          if ($day['work']['original_end']) {
            $date = new Meduse_Date($day['work']['original_end']);
            $value = $date->toString('H:i');
          }
          else {
            $value = '&ndash;';
          }
          $tdWorkEnd .= '<div class="original-data text-right">' . $value . '</div>';
          $tdLunch .= '<input data-row-id="' . $id . '"type="checkbox" name="lunch_' . $idx . '" ' . ($day['lunch_disabled']  ? ' disabled="disabled" ' : '') . ((isset($day['work']['lunch']) && $day['work']['lunch']) ? ' checked="checked" ' : '') .'>';
          $tdDescription .= '<input class="description" maxlength="150" data-row-id="' . $id . '" type="text" name="description_' . $idx . '" value="' . (isset($day['work']['description']) ? $day['work']['description'] : '') . '" placeholder="' . $day['placeholder'] . '">';
          $tdKm .= '<input type="text" data-row-id="' . $id . '" name="km_' . $idx . '" value="' . (isset($day['work']['km']) ? $day['work']['km'] : '') . '">';
          $tdType .= '<select data-row-id="' . $id . '" name="type_' . $idx . '">' . $this->_getTypeOptions(isset($day['work']['type']) ? $day['work']['type'] : '') . '</select>';
        }
        else {
          $tdWorkStart .= '<input data-row-id="" type="text" name="start_' . $idx . '">';
          $tdWorkEnd .= '<input data-row-id="" type="text" name="end_' . $idx . '">';
          $tdLunch .= '<input data-row-id="" type="checkbox" name="lunch_' . $idx . '">';
          $tdType .= '<select data-row-id="" name="type_' . $idx . '">' . $this->_getTypeOptions() . '</select>';
          $tdDescription .= '<input type="text" maxlength="150" data-row-id="" name="description_' . $idx . '" placeholder="' . $day['placeholder'] . '">';
          $tdKm .= '<input type="text" data-row-id="" name="km_' . $idx  . '">';
        }

        $rowClass = " class='" . implode(' ', $rowClassArray) . "'";

        $html .= '<tr' . $rowClass .'>';
        $html .= '<td class="text-center date"><strong>' . $day['date'] . '</strong></td>';
        $html .= '<td class="start time">' . $tdWorkStart . '</td>';
        $html .= '<td class="end time">' . $tdWorkEnd . '</td>';
        $html .= '<td class="duration time"><input type="text" readonly="readonly" name="total_' . $idx . '" value="' . $day['total'] . '"></td>';
        $html .= '<td class="lunch">' . $tdLunch .'</td>';
        $html .= $options['show_overtime'] ? '<td class="overtime time"><input type="text" readonly="readonly" name="overtime_' . $idx . '" value="' . $day['overtime'] . '"></td>' : '';
        $html .= '<td class="type">' . $tdType . '</td>';
        $html .= '<td class="description">' . $tdDescription . '</td>';
        $html .= '<td class="km">' . $tdKm . '</td>';
        $html .= '</tr>';
      }
      $html .= '</tbody></table></div>';
      return $html;
    }

    public function getCalendar() {
      return $this->_calendar;
    }

    public function getTotal() {
      return $this->_total;
    }

    public function getOvertime($withDebt = FALSE) {
      $overtime = $this->_overtime;
      if ($withDebt) {
        $reward = new Employees_Reward($this->_work->getEmployee(), $this->_work->getDate());
        $overtime -= $reward->getDebt();
      }
      return $overtime;
    }

    public function getVacation($unit = self::UNIT_DAY) {
      switch($unit) {
        case self::UNIT_HOUR: return $this->_vacation;
        case self::UNIT_MINUTE: return $this->_vacation * 60;
        default: return $this->_vacation / self::HOURS_IN_DAY;
      }
    }

    public function getVacationDates() {
      $dates = array();
      foreach ($this->_calendar as $idx => $day) {
        if (isset($day['work']) && isset($day['work']['type']) && in_array($day['work']['type'], array(
            Table_EmployeesRecords::TYPE_VACATION,
            Table_EmployeesRecords::TYPE_HALF_VACATION,
          ))) {
          $dates[] = array(
            'date' => new Meduse_Date($idx, 'Y-m-d'),
            'type' => $day['work']['type'],
          );
        }
      }
      return $dates;
    }

    public function getSickness($unit = self::UNIT_DAY) {
      switch($unit) {
        case self::UNIT_HOUR: return $this->_sickness;
        case self::UNIT_MINUTE: return $this->_sickness * 60;
        default: return $this->_sickness / self::HOURS_IN_DAY;
      }
    }

    public function getSicknessDates() {
      $dates = array();
      foreach ($this->_calendar as $idx => $day) {
        if (isset($day['work']) && isset($day['work']['type']) && $day['work']['type'] == Table_EmployeesRecords::TYPE_SICKNESS) {
          $dates[] = new Meduse_Date($idx, 'Y-m-d');
        }
      }
      return $dates;
    }

    public function getFMC($unit = self::UNIT_DAY) {
      switch($unit) {
        case self::UNIT_HOUR: return $this->_fmc;
        case self::UNIT_MINUTE: return $this->_fmc * 60;
        default: return $this->_fmc / self::HOURS_IN_DAY;
      }
    }

    public function getFMCDates() {
      $dates = array();
      foreach ($this->_calendar as $idx => $day) {
        if (isset($day['work']) && isset($day['work']['type']) && $day['work']['type'] == Table_EmployeesRecords::TYPE_FMC) {
          $dates[] = new Meduse_Date($idx, 'Y-m-d');
        }
      }
      return $dates;
    }

    public function getUnpaidVacation($unit = self::UNIT_DAY) {
      switch($unit) {
        case self::UNIT_HOUR: return $this->_unpaid_vacation;
        case self::UNIT_MINUTE: return $this->_unpaid_vacation * 60;
        default: return $this->_unpaid_vacation / self::HOURS_IN_DAY;
      }
    }

    public function getUnpaidVacationDates() {
      $dates = array();
      foreach ($this->_calendar as $idx => $day) {
        if (isset($day['work']) && isset($day['work']['type']) && $day['work']['type'] == Table_EmployeesRecords::TYPE_UNPAID_VACATION) {
          $dates[] = new Meduse_Date($idx, 'Y-m-d');
        }
      }
      return $dates;
    }
    public function getTripsDates() {
      $dates = array();
      foreach ($this->_calendar as $idx => $day) {
        if (isset($day['work']) && isset($day['work']['type'])
          && ($day['work']['type'] == Table_EmployeesRecords::TYPE_BUSINESS_TRIP || $day['work']['type'] == Table_EmployeesRecords::TYPE_BUSINESS_TRIP_FOREIGN)) {
          $dates[] = new Meduse_Date($idx, 'Y-m-d');
        }
      }
      return $dates;
    }
    public function getTripsDatesLocal() {
      $dates = array();
      foreach ($this->_calendar as $idx => $day) {
        if (isset($day['work']) && isset($day['work']['type'])
          && ($day['work']['type'] == Table_EmployeesRecords::TYPE_BUSINESS_TRIP)) {

          $timestamp1 = strtotime($day['work']['start']);
          $timestamp2 = strtotime($day['work']['end']);
          $duration = abs($timestamp2 - $timestamp1)/(60*60);
          $dates[$idx] = [
            'date' => new Meduse_Date($idx, 'Y-m-d'),
            'duration' => $duration,
          ];

        }
      }
      return $dates;
    }
    public function getTripsDatesForeign() {
      $dates = array();
      foreach ($this->_calendar as $idx => $day) {
        if (isset($day['work']) && isset($day['work']['type'])
          && ($day['work']['type'] == Table_EmployeesRecords::TYPE_BUSINESS_TRIP_FOREIGN)) {

          $timestamp1 = strtotime($day['work']['start']);
          $timestamp2 = strtotime($day['work']['end']);
          $duration = abs($timestamp2 - $timestamp1)/(60*60);
          $dates[$idx] = [
            'date' => new Meduse_Date($idx, 'Y-m-d'),
            'duration' => $duration,
          ];

        }
      }
      return $dates;
    }
    public function getKm() {
      return $this->_km;
    }

    public function getWorkingHours() {
      return $this->_hours;
    }

    public function getStatus() {
      return $this->_status;
    }

    public function getDayStatus(Meduse_Date $day) {
      $end = new Meduse_Date($this->_work->getDate());
      $end->addMonth(1);
      if ($day < $this->_work->getDate() || $day >= $end) {
        throw new Employees_Exceptions_DayNotInRange();
      }
      $idx = $day->toString('Y-m-d');
      return $this->_calendar[$idx]['status'];
    }

    public function getDiets() {
      return $this->_diets;
    }

    public function getDietsFrgn() {
      return $this->_diets_frgn;
    }

    private function _getTypeOptions($default = '') {
      $opTypes = '<option value="">...</option>';

      $employeeType = $this->_work->getEmployee()->getType();
      $reimburseHoliday = $employeeType == Table_Employees::TYPE_EMPLOYEE || $this->_work->getEmployee()->isReimbursedHolidays();

      $list = $employeeType == Table_Employees::TYPE_EMPLOYEE ?
        Table_EmployeesRecords::$typeNames : Table_EmployeesRecords::$typeNames2;

      if ($employeeType === Table_Employees::TYPE_SUPPLIER && !$reimburseHoliday) {
        unset($list[Table_EmployeesRecords::TYPE_HOLIDAY]);
      }

      foreach ($list as $val => $type) {
        if ($default == $val) {
          $opTypes .= '<option selected="selected" value="' . $val . '">' . $type . '</option>';
        }
        else {
          $opTypes .= '<option value="' . $val . '">' . $type . '</option>';
        }
      }
      return $opTypes;
    }

    public function saveToDB() {
      $employeeId = $this->_work->getEmployeeId();
      $tWorks = new Table_EmployeesWorks();
      foreach ($this->_calendar as $key => $day) {
        if (!empty($day['records'])) {
          foreach ($day['records'] as $item) {
            if ($item['deleted']) {
              continue;
            }
            $dateStart = isset($item['start']) ? new Meduse_Date($item['start']) : NULL ;
            $dateEnd = isset($item['end']) ? new Meduse_Date($item['end']) : NULL;
            $row = $tWorks->createRow(array(
              'id_users' => isset($item['user']) ? $item['user'] : NULL,
              'id_employees' => $employeeId,
              'date' => $key,
              'type' => isset($item['type']) ? $item['type'] : 'work',
              'start' => $dateStart ? $dateStart->toString('H:i:s') : NULL,
              'end' => $dateEnd ? $dateEnd->toString('H:i:s') : NULL,
              'km' => isset($item['km']) ? (float) $item['km'] : NULL,
              'description' => $item['description'] ? $item['description'] : NULL,
            ));
            $row->save();
          }
        }
      }
    }

    protected function round($timestamp, $status = self::STATUS_START, $type = Table_Employees::ROUNDING_DEFAULT) {
      // bez zaokrouhleni u typu ROUNDING_NONE
      if ($type == Table_Employees::ROUNDING_NONE) {
        return $timestamp;
      }

      $time = new Meduse_Date($timestamp, 'Y-m-d H:i:s');
      switch ($status) {

        // zaokrouhlovani nahoru na presnost s toleranci
        case Table_EmployeesRecords::STATE_CHECKIN:
        case Table_EmployeesRecords::STATE_BREAKOUT:
        case self::STATUS_START:

          switch ($type) {
            case Table_Employees::ROUNDING_30_15:
              $precision = ((int) $time->toString('H') < 8) ? self::ROUNDING_PRECISION * 2 : self::ROUNDING_PRECISION;
              $tolerance = self::ROUNDING_TOLERANCE;
              break;

            case Table_Employees::ROUNDING_05:
              $precision = (int) self::ROUNDING_PRECISION / 3;
              $tolerance = 0;
              break;

            default:
              $precision = self::ROUNDING_PRECISION;
              $tolerance = self::ROUNDING_TOLERANCE;
          }

          $time->subMinute($tolerance);
          $minute = (int) $time->toString('i');
          $modulo = $minute % $precision;
          $time->addMinute($precision - $modulo);
          break;

        // zaokrouhlovani dolu na presnost s toleranci
        case Table_EmployeesRecords::STATE_CHECKOUT:
        case Table_EmployeesRecords::STATE_BREAKIN:
        case self::STATUS_END:

          switch ($type) {
            case Table_Employees::ROUNDING_05:
              $precision = (int) self::ROUNDING_PRECISION / 3;
              $tolerance = 0;
              break;

            default:
              $precision = self::ROUNDING_PRECISION;
              $tolerance = self::ROUNDING_TOLERANCE;
          }

          $time->addMinute($tolerance);
          $minute = (int) $time->toString('i');
          $modulo = $minute % $precision;
          $time->subMinute($modulo);
          break;
      }

      return $time->toString('Y-m-d H:i:s');
    }
  }

