<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Employees_Employee {

    protected $_table = NULL;
    protected $_row = NULL;
    protected $_status = NULL;

    /**
     * Vytvori instanci zamestnance.
     *
     * @param mixed $employee
     *  NULL - nacteni prihlaseneho
     *  array - vytvoreni noveho
     *  int - nacteni dle ID
     */
    public function __construct($employee = NULL) {
      $this->_table = new Table_Employees();
      if (is_null($employee)) {
        $authNamespace = new Zend_Session_Namespace('Zend_Auth');
        $select = $this->_table->select();
        $select->where('id_users = ?', $authNamespace->user_id)->limit(1);
        $this->_row = $select->query(Zend_Db::FETCH_OBJ)->fetch();
        if (!$this->_row) {
          throw new Employees_Exceptions_NonExists('Pracovník neexistuje');
        }
      }
      elseif (is_array($employee)) {
        if (!Zend_Date::isDate($employee['start'], 'YYYY-mm-dd')) {
          unset($employee['start']);
        }
        if (!Zend_Date::isDate($employee['end'], 'YYYY-mm-dd')) {
          unset($employee['end']);
        }
        $this->_row = $this->_table->createRow($employee)->save();
      }
      else {
        $this->_row = $this->_table->find($employee)->getRow(0);
      }
    }

    public function getId() {
      return $this->_row->id;
    }

    public function getUserId() {
      return $this->_row->id_users;
    }

    public function getFirstName() {
      return $this->_row->first_name;
    }

    public function getLastName() {
      return $this->_row->last_name;
    }

    public function getFullName($firstLast = FALSE) {
      return $firstLast ? $this->getLastName() . ' ' . $this->getFirstName() :
        $this->getFirstName() . ' ' . $this->getLastName();
    }

    public function getStart() {
      return $this->_row->start;
    }

    public function getEnd() {
      return $this->_row->end;
    }

    public function getPosition() {
      return $this->_row->position;
    }

    public function getDepartmentId() {
      return $this->_row->id_departments;
    }

    public function getFirm() {
      $tDepartments = new Table_Departments();
      $dept = $tDepartments->find($this->_row->id_departments)->current();
      return Table_Departments::$firm_name[$dept->id_wh_type];
    }

    public function getFirmId() {
      $tDepartments = new Table_Departments();
      $dept = $tDepartments->find($this->_row->id_departments)->current();
      return $dept->id_wh_type;
    }

    public function getDepartmentName() {
      $tDepartments = new Table_Departments();
      $dept = $tDepartments->find($this->_row->id_departments)->current();
      return $dept->name;
    }

    public function getObligation($readable = FALSE) {
      $oblig = '';
      if ($readable) {
        $number = $this->_row->obligation;
        if ($number) {
          switch ($number) {
            case '1/1': $oblig = 'plný'; break;
            case '1/2': $oblig = 'poloviční'; break;
            case '4/5': $oblig = 'čtyřpětinový'; break;
          }
        }
      }
      else {
        $oblig = $this->_row->obligation;
      }
      return $oblig;
    }

    public function getPartTime() {
      return $this->_row->part_time;
    }

    public function isPartTime() {
      return $this->_row->part_time == 'y';
    }


    public function getWage() {
      return $this->_row->wage;
    }

    public function getBonus() {
      return $this->_row->bonus;
    }

    /**
     * @throws Zend_Db_Table_Exception
     */
    public function getVacation(?int $year = null, string $type = 'total'): ?float
    {
      if ($type !== 'setting') {
        if (! $year) {
          $year = (int) date('Y');
        }
        $tClaims = new Table_EmployeesVacationClaims();
        if ($row = $tClaims->find($this->getId(), $year)->current()) {
          switch ($type) {
            case 'total':
              return ((float) $row->claimed) + ((float) $row->transferred);
            case 'claimed':
              return (float) $row->claimed;
            case 'transferred':
              return (float) $row->transferred;
          }
        }
      }
      return ($type === 'transferred') ? null : (float) $this->_row->vacation;
    }

    public function getReimburseHolidays() {
      return $this->_row->reimburse_holidays;
    }

    public function isReimbursedHolidays() {
      return $this->_row->reimburse_holidays == 'y';
    }

    public function isActive() {
      return $this->_row->active == 'y';
    }

    public function getBirthday($raw = TRUE) {
      if ($raw) {
        return $this->_row->birthday;
      }
      else {
        return is_null($this->_row->birthday) ? NULL : new Meduse_Date($this->_row->birthday, 'Y-m-d');
      }
    }

    public function getBirthplace() {
      return $this->_row->birthplace;
    }

    public function getInsuranceCode() {
      return $this->_row->insurance_code;
    }

    public function getAccountNumber() {
      return $this->_row->account_number;
    }

    public function getPrivatePhone() {
      return $this->_row->private_phone;
    }

    public function getPrivateEmail() {
      return $this->_row->private_email;
    }

    public function getBusinessPhone() {
      return $this->_row->business_phone;
    }

    public function getBusinessPhonePin() {
      return $this->_row->business_phone_pin;
    }

    public function getComputerName() {
      return $this->_row->computer_name;
    }

    public function getComputerLogin() {
      return $this->_row->computer_login;
    }

    public function getComputerPassword() {
      return $this->_row->computer_password;
    }

    public function getBusinessEmail() {
      return $this->_row->business_email;
    }

    public function getWorkClass() {
      return $this->_row->work_class;
    }

    public function getInsurance() {
      return $this->_row->insurance;
    }

    public function getNDA() {
      return $this->_row->nda;
    }

    public function getActive() {
      return $this->_row->active;
    }



    public function getTransportAllowance() {
      return $this->_row->transport_allowance;
    }

    public function getDiets() {
      return $this->_row->diets;
    }

    public function getVacationSum($year = NULL, $month = NULL): float
    {
      $table = new Table_EmployeesRewards();
      return (float) $table->getVacation($this->getId(), $year, $month);
    }

    /**
     * @throws Zend_Db_Table_Exception
     */
    public function getVacationRest(?int $year = NULL, ?int $month = NULL): float
    {
      if (!$year) {
        $date = new Meduse_Date();
        $year = (int) $date->get('Y');
      }
      $vacation = $this->getVacation($year);
      $sum = $this->getVacationSum($year, $month);
      return $vacation - $sum;
    }

    public function getStatus() {
      if (is_null($this->_status)) {
        $table = new Table_EmployeesRecords();
        $status = $table->getStatus($this->getId());
        $this->_status = ($status == Table_EmployeesRecords::STATE_BREAKOUT) ?
          Table_EmployeesRecords::STATE_CHECKIN : $status;
      }
      return $this->_status? $this->_status : Table_EmployeesRecords::STATE_CHECKOUT;
    }

    public function setStatus($status) {
      if (!is_null($status)) {
        $states = Table_EmployeesRecords::statesToString();
        if (key_exists($status, $states)) {
          $this->_status = $status;
          $table = new Table_EmployeesRecords();
          $table->insert(array(
            'id_employees' => $this->getId(),
            'status' => $status,
          ));
        }
      }
    }

    public function getType() {
      return $this->_row->type;
    }

    public function isSupplier() {
      return $this->_row->type == Table_Employees::TYPE_SUPPLIER;
    }

    public function getAlvenoId() {
      return $this->_row->alveno;
    }

    public function getStatesToChoose() {
      $status = $this->getStatus();
      $states = Table_EmployeesRecords::statesToString();
      unset($states[$status]);
      if ($status == Table_EmployeesRecords::STATE_CHECKOUT) {
        unset($states[Table_EmployeesRecords::STATE_BREAKIN]);
        unset($states[Table_EmployeesRecords::STATE_BREAKOUT]);
      } elseif ($status == Table_EmployeesRecords::STATE_BREAKIN) {
        unset($states[Table_EmployeesRecords::STATE_CHECKIN]);
      } elseif ($status == Table_EmployeesRecords::STATE_CHECKIN) {
        unset($states[Table_EmployeesRecords::STATE_BREAKOUT]);
      }
      return $states;
    }

    /**
     * @throws Zend_Date_Exception
     * @throws Employees_Exceptions_IncompleteContructor
     * @throws Employees_Exceptions_NonExists
     */
    public function getRewards($year = NULL, $month = NULL, $limit = NULL): array
    {
      $rewards = array();
      $table = new Table_EmployeesRewards();
      $rows = $table->getRewards($this->getId(), $year, $month, $limit);
      if ($rows) {
        foreach($rows as $row) {
          $date = new Meduse_Date($row->year . $row->month, 'Ym');
          $rewards[] = new Employees_Reward($this, $date);
        }
      }
      return $rewards;
    }

    /**
     * @throws Zend_Date_Exception
     * @throws Employees_Exceptions_IncompleteContructor
     * @throws Zend_Db_Statement_Exception
     */
    public function getLastClosedReward(): ?Employees_Reward
    {
      $table = new Table_EmployeesRewards();
      if ($row = $table->getLastClosedReward($this->getId())) {
        $date = new Meduse_Date($row->year . $row->month, 'Ym');
        return new Employees_Reward($this, $date);
      }
      return null;
    }

    public function getVacations($year = NULL, $limit = NULL) {
     $table = new Table_EmployeesAbsence();
     $select = $table->select();
     $select->where('id_employees = ?', $this->_row->id);
     if (!is_null($year)) {
      $select->where('start BETWEEN "' . $year . '-01-01" AND "' . $year . '-12-31"');
      $select->where('end BETWEEN "' . $year . '-01-01" AND "' . $year . '-12-31"');
      $select->where('type = ?', Table_EmployeesAbsence::TYPE_VACATION);
     }
     if (!is_null($limit)) {
       $select->limit($limit);
     }
     return $select->query()->fetchAll();
    }

    public function getAbsence(Meduse_Date $date) {
      $start = new Meduse_Date($date->toString('Y-m-01'));
      $end = new Meduse_Date($date->toString('Y-m-01'));
      $end->addMonth(1);
      $table = new Table_EmployeesAbsence();
      $select = $table->select();
      $select->where('id_employees = ?', $this->_row->id);
      $select->where('start BETWEEN "' . $start->get('Y-m-d') . '" AND "' . $end->get('Y-m-d') . '"');
      $select->where('type <> ?', Table_EmployeesAbsence::TYPE_VACATION);
      return $select->query()->fetchAll();
    }

    public function getWork(Meduse_Date $date, $asObject = false) {
      $eWork = new Employees_Work($this, $date);
      return $asObject ? $eWork : $eWork->getWork();
    }

    public function getOvertimeRate($extra = FALSE) {
      return $extra ? $this->_row->overtime_extra_rate : $this->_row->overtime_rate;
    }

    public function getRounding() {
      return $this->_row->rounding;
    }

    /**
     * @throws Zend_Db_Table_Exception
     */
    public function getFormData() {
      $data = array(
        'id_users' => $this->getUserId(),
        'active' => $this->getActive(),
        'alveno' => $this->getAlvenoId(),
        'birthday' => Meduse_Date::dbToForm($this->getBirthday()),
        'birthplace' => $this->getBirthplace(),
        'insurance_code' => $this->getInsuranceCode(),
        'account_number' => $this->getAccountNumber(),
        'private_phone' => $this->getPrivatePhone(),
        'private_email' => $this->getPrivateEmail(),
        'business_phone' => $this->getBusinessPhone(),
        'business_phone_pin' => $this->getBusinessPhonePin(),
        'computer_name' => $this->getComputerName(),
        'computer_login' => $this->getComputerLogin(),
        'computer_password' => $this->getComputerPassword(),
        'business_email' => $this->getBusinessEmail(),
        'work_class' => $this->getWorkClass(),
        'insurance' => $this->getInsurance(),
        'nda' => $this->getNDA(),
        'transport_allowance' => $this->getTransportAllowance(),
        'diets' => $this->getDiets(),
        'first_name' => $this->getFirstName(),
        'last_name' => $this->getLastName(),
        'start' => Meduse_Date::dbToForm($this->getStart()),
        'end' => Meduse_Date::dbToForm($this->getEnd()),
        'id_departments' => $this->getDepartmentId(),
        'position' => $this->getPosition(),
        'obligation' => $this->getObligation(),
        'part_time' => $this->getPartTime(),
        'wage' => $this->getWage(),
        'bonus' => $this->getBonus(),
        'vacation' => $this->getVacation(null, 'setting'),
        'reimburse_holidays' => $this->getReimburseHolidays(),
        'overtime_rate' => $this->getOvertimeRate(FALSE),
        'overtime_extra_rate' => $this->getOvertimeRate(TRUE),
        'rounding' => $this->getRounding(),
        'type' => $this->getType(),
      );
      return $data;
    }

    public function update($data) {
      $this->_table->update($data, 'id = ' . $this->getId());
      $this->_row = $this->_table->find($this->getId())->getRow(0);
    }

    /**
     * Testuje, zda-li je worksheet v danem obdobi spravne vyplnen.
     *
     * @param Meduse_Date $start
     *    Zacatek obdobi (volitelne).
     *    Vychozi hodnotou je aktualni minuly tyden den .
     *
     * @param int $days
     *    Pocet dni ke kontrole (volitelne).
     *    Vychozi hodnotou je 7 dni.
     *
     * @return bool
     *    TRUE vraci v pripade, ze veskere pracovni dny jsou vyplneny a to spravne
     *    a veskere vyplnene nepracovni dny jsou vyplnene spravne. FALSE vraci v pripade
     *    nevyplneni pracovniho dne nebo dne, ktery je vyplneny chybne.
     */
    public function verifyWorkSheet(Meduse_Date $start = NULL, $days = 7) {
      // vychozi hodnota pro zacatek testovaneho intervalu
      if (is_null($start)) {
        $day = new Meduse_Date();
        $day->subWeek(1);
      }
      else {
        $day = new Meduse_Date($start);
      }
      // inicializace
      $status = TRUE;
      $work = new Employees_Work($this, $day);
      $sheet = new Employees_WorkSheet($work);
      $count = 0;
      // prochazime postupne vsechny dny od zadaneho data
      while($count < $days) {
        try {
          // testujeme den ve worksheetu, test muze vyhodit vyjimku,
          // ze jsme se dostali do dalsiho mesice
          $dayStatus = $sheet->getDayStatus($day);
        }
        // pokud testujeme pres hranice mesice,
        // musime nacist novy worksheet a provest test na nem
        catch (Employees_Exceptions_DayNotInRange $e) {
          $work = new Employees_Work($this, $day);
          $sheet = new Employees_WorkSheet($work);
          $dayStatus = $sheet->getDayStatus($day);
        }
        // pokud neni den vyplnen, je to ok jen v pripade nepracovniho dne
        if ($dayStatus === Employees_WorkSheet::DAY_STATUS_EMPTY) {
          $status = $status && ($day->isFreeday() || $day->isHoliday());
        }
        // pokud je den vyplnen, chceme, aby byl bez chyby
        else {
          $status = $status && ($dayStatus === Employees_WorkSheet::DAY_STATUS_OK);
        }
        // jakmile se objevi chyba, dal uz netestujeme
        if (!$status) {
          break;
        }
        $day->addDay(1);
        $count++;
      }
      return $status;
    }


    public static function getDeactivated($wh_type = NULL, $asObjects = FALSE) {

      $table = new Table_Employees();
      $select = $table->select()->setIntegrityCheck(FALSE);
      if ($wh_type) {
        $select->where('wh_type = ?', $wh_type);
      }
      $select->where('active = ?', 'n')->order(array('last_name', 'first_name', 'id'));
      $result = $select->query(Zend_Db::FETCH_ASSOC);
      $return = array();
      while ($row = $result->fetch()) {
        if ($asObjects) {
          $return[] = new Employees_Employee($row['id']);
        }
        else {
          $return[] = $row;
        }
      }
      return $return;
    }
  }

