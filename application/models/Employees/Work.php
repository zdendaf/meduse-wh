<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Employees_Work {

    protected $_table = NULL;
    protected $_employee = NULL;
    protected $_date = NULL;
    protected $_work = array();

    /**
     * Konstruktor pracovních záznamů pracovníka za daný měsíc.
     *
     * @param Employees_Employee $employee
     *    objekt pracovníka
     * @param Meduse_Date $date
     *    objekt data práce
     */
    public function __construct(Employees_Employee $employee, Meduse_Date $date) {
      $this->_table = new Table_EmployeesWorks();
      $this->_employee = $employee;
      $this->_date = new Meduse_Date($date->toString('Y-m'), 'Y-m');
      $this->computeWork();
    }

    public function getDate() {
      return new Meduse_Date($this->_date);
    }

    public function getEmployeeId() {
        return $this->_employee ? $this->_employee->getId() : NULL;
    }

    public function getEmployee() {
        return $this->_employee;
    }

    protected function computeWork() {
      $date = new Meduse_Date($this->_date->toString('Y-m') . '-01 00:00:00');
      $date1str = $date->toString('Y-m-d');
      $select = $this->_table->select();
      if ($this->_employee) {
        $select->where('id_employees = ?', $this->_employee->getId());
      }
      $select->where('date >= :date1');

      $date->addMonth(1);
      $date2str = $date->toString('Y-m-d');
      $select->where('date < :date2');
      $select->order('date ASC');

      $params = array('date1' => $date1str, 'date2' => $date2str);

      $this->_work = $this->_table->getAdapter()->fetchAll($select, $params);
    }

    public function getWork($recalculate = FALSE) {
      if ($recalculate) {
        $this->computeWork();
      }
      return $this->_work;
    }

    public function getSheet($recalculate = FALSE) {
      if ($recalculate) {
        $this->computeWork();
      }
      return new Employees_WorkSheet($this);
    }

    public function update(&$records) {
      if ($records) {
        foreach ($records as $idx => $record) {
          if ($record['row_id']) {
            $this->_table->update(array(
              'id_employees' => $this->_employee->getId(),
              'date' => $record['timestamp'],
              'type' => $record['type'] === 'null' ? NULL : $record['type'],
              'description' => $record['description'] === 'null' ? NULL : $record['description'],
              'km' => $record['km'] === 'null' ? NULL : round((float) $record['km'], 2),
              'id_users' => $record['id_users'],
            ), 'id = ' . $record['row_id']);
          }
          elseif (!empty($record['timestamp'])) {
            $records[$idx]['row_id'] = $this->_table->insert(array(
              'id_employees' => $this->_employee->getId(),
              'date' => $record['timestamp'],
              'status' => $record['status'],
              'type' => $record['type'] === 'null' ? NULL : $record['type'],
              'description' => $record['description'] === 'null' ? NULL : $record['description'],
              'km' => $record['km'] === 'null' ? NULL : round((float) $record['km'], 2),
              'id_users' => $record['id_users'],
              'deleted' => 'n',
            ));
          }
        }
      }
    }

  }

