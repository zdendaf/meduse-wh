<?php


  class Employees_BustripsForm extends Meduse_FormBootstrap {


    public function init() {

      $this
        ->setAttrib('class', 'form')
        ->setMethod(Zend_Form::METHOD_POST)
        ->setLegend('Služební cesta');

      $idEmloyeesElem = new Zend_Form_Element_Hidden('name');

      $startTimeElem = new Meduse_Form_Element_Text('start_time');
      $startTimeElem
        ->setLabel('Čas')
        ->setRequired(TRUE);
      $startDateElem = new Meduse_Form_Element_DatePicker('start_date');
      $startDateElem
        ->setLabel('Datum')
        ->setRequired(TRUE);

      $endTimeElem = new Meduse_Form_Element_Text('end_time');
      $endTimeElem
        ->setLabel('Čas')
        ->setRequired(TRUE);
      $endDateElem = new Meduse_Form_Element_DatePicker('end_date');
      $endDateElem
        ->setLabel('Datum')
        ->setRequired(TRUE);

      $regionElem = new Meduse_Form_Element_Select('region');
      $regionElem
        ->setLabel('Region')
        ->setRequired(TRUE);
      $regionElem->setMultiOptions(array(
        'cz/sk' => 'česko/slovensko',
        'other' => 'jiný region',
      ));

      $descriptionElem = new Meduse_Form_Element_Textarea('description');
      $descriptionElem
        ->setAttrib('rows', 4)
        ->setLabel('Poznámka');

      $submitElem = new Meduse_Form_Element_Submit('send');
      $submitElem->setLabel('Odeslat');

      $this->addElements(array(
        $idEmloyeesElem,
        $startTimeElem,
        $startDateElem,
        $endTimeElem,
        $endDateElem,
        $regionElem,
        $descriptionElem,
        $submitElem,
      ));

      $this->addDisplayGroup(array('start_time', 'start_date'), 'start', array(
        'legend' => 'Začátek',
      ));
      $this->addDisplayGroup(array('end_time', 'end_date'), 'end', array(
        'legend' => 'Konec',
      ));
      $this->addDisplayGroup(array('region', 'description'), 'descript', array(
        'legend' => 'Popis',
      ));
      parent::init();
    }







  }