<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Employees_HolidaysForm extends Meduse_FormBootstrap {

    public function init() {
      $this->_isModalForm = TRUE;

      $element = new Meduse_Form_Element_DatePicker('date');
      $element->setLabel('Datum volna');
      $element->setRequired(TRUE);
      $this->addElement($element);

      $element = new Meduse_Form_Element_DatePicker('change');
      $element->setLabel('Náhrada za');
      $this->addElement($element);

      parent::init();
    }


  }
