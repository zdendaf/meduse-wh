<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Employees_SettingsForm extends Meduse_FormBootstrap {

    public function init() {

      $eRate5 = new Meduse_Form_Element_Currency('employees_diets_rate5');
      $eRate5
        ->setRequired(TRUE)
        ->setLabel('Sazba 5-12h [Kč]');

      $eRate12 = new Meduse_Form_Element_Currency('employees_diets_rate12');
      $eRate12
        ->setRequired(TRUE)
        ->setLabel('Sazba 12-18h [Kč]');

      $eRate18 = new Meduse_Form_Element_Currency('employees_diets_rate18');
      $eRate18
        ->setRequired(TRUE)
        ->setLabel('Sazba >18h [Kč]');

      $eRateF = new Meduse_Form_Element_Currency('employees_diets_ratef');
      $eRateF
        ->setRequired(TRUE)
        ->setLabel('Sazba zahr. [Eur]');

      $eRateCar = new Meduse_Form_Element_Currency('employees_car_rate');
      $eRateCar
        ->setRequired(TRUE)
        ->setLabel('Sazba [Kč/km]');

      $eSubmit = new Meduse_Form_Element_Submit('Uložit');

      $this->_isModalForm = TRUE;
      $this
        ->addAttribs(array('class' => 'form'))
        ->addElement($eRate5)
        ->addElement($eRate12)
        ->addElement($eRate18)
        ->addElement($eRateF)
        ->addElement($eRateCar)

        ->addDisplayGroup(array(
          'employees_diets_rate5',
          'employees_diets_rate12',
          'employees_diets_rate18',
          'employees_diets_ratef'), 'diets', array(
            'legend' => 'Nastavení sazeb diet'))

        ->addDisplayGroup(array(
          'employees_car_rate'), 'car', array(
            'legend' => 'Nastavení sazby za auto'))

        ->addElement($eSubmit);

      parent::init();

    }
  }

