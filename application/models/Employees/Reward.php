<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Employees_Reward {

    const REWARD_STATUS_EMPTY = 0;
    const REWARD_STATUS_OPEN = 2;
    const REWARD_STATUS_CLOSED = 1;
    const REWARD_STATUS_FAIL = 3;

    protected $_table = NULL;
    protected $_row = NULL;
    protected $_date = NULL;
    protected $_employee = NULL;

    public function __construct(Employees_Employee $employee = NULL, Meduse_Date $date = NULL, $create = TRUE) {

      if (is_null($employee) || is_null($date)) {
        throw new Employees_Exceptions_IncompleteContructor('Nebylo zadano ID pracovnika nebo datum.');
      }

      $this->_table = new Table_EmployeesRewards();
      $this->_employee = $employee;
      $this->_date = $date;

      $this->_row = $this->_table->find(
        $this->_employee->getId(),
        $this->_date->get('Y'),
        $this->_date->get('m'))->current();

      if (!$this->_row) {
        $this->_row = $this->_table->createRow(array(
          'id_employees' => $this->_employee->getId(),
          'year' => $this->_date->get('Y'),
          'month' => $this->_date->get('m'),
        ));

        if ($create) {
          $this->_row->save();
        }
      }

    }

    public function recalculate() {

      if ($this->_row->status === self::REWARD_STATUS_CLOSED) {
        throw new Employees_Exceptions_RewardAlreadyClosed('Měsíc je již uzavřený.');
      }

      $work = new Employees_Work($this->_employee, $this->_date);
      $sheet = new Employees_WorkSheet($work);

      // zaokrouhleni celkove odpracovane doby na 0,25 hodiny
      $this->_row->total = floor($sheet->getTotal()*4)/4;

      // ziskani mesicniho zavazku
      $hours = $this->getObligation($sheet->getWorkingHours());
      $this->_row->hours = $hours;

      // ziskani neplaceneho volna
      $unpaid = $sheet->getUnpaidVacation($sheet::UNIT_HOUR);
      $this->_row->unpaid = $unpaid;

      // ziskani prescasu (+/-)
      $this->_row->debt = $this->getDebt();

      // vykazane kilometry za svoje auto
      $this->_row->km = $sheet->getKm();

      // pocet dni dovolene
      $this->_row->vacation = $sheet->getVacation();

      // pocet dni nemoci
      $this->_row->sickness = $sheet->getSickness();

      // pocet diet ceskych
      $this->_row->diets = $sheet->getDiets();

      // pocet diet zahranicnich
      $this->_row->diets_frgn = $sheet->getDietsFrgn();

      // status mesice
      switch ($sheet->getStatus()) {
        case Employees_WorkSheet::DAY_STATUS_EMPTY:
          $this->_row->status = self::REWARD_STATUS_EMPTY;
          break;
        case Employees_WorkSheet::DAY_STATUS_FAIL:
          $this->_row->status = self::REWARD_STATUS_FAIL;
          break;
        case Employees_WorkSheet::DAY_STATUS_OK:
          $this->_row->status = self::REWARD_STATUS_OPEN;
          break;
      }

      // ulozeni do db
      $this->_row->save();
    }

    /**
     * Vrátí hodinový dluh nebo přebytek z minulého měsíce.
     */
    public function getDebt($positive = TRUE) {

      $select = $this->_table->select()->setIntegrityCheck(FALSE);
      $select->from($select->getTable(), array('debt'));
      $select->where('id_employees = ?', $this->_employee->getId());
      $select->where('status = ?', self::REWARD_STATUS_CLOSED);
      $select->where('CONCAT(year, month) < ?', $this->_date->get('Ym'));
      $select->order(array('year DESC', 'month DESC'));
      //Zend_Debug::dump($select . ''); die;
      $debt = $select->query()->fetchColumn();

      return $debt === FALSE ? 0 : ($positive ? $debt : -$debt);
    }

    public function getOvertime($withDebt = FALSE) {
      $sickness = $this->getSickness() * Employees_WorkSheet::HOURS_IN_DAY;
      $overtime = $this->getTotal() + $sickness - $this->getHours()  + $this->getUnpaid();
      return $withDebt ? $overtime - $this->getDebt() : $overtime;
    }

    public function getOvertimeCash() {
      return $this->_row->overtime_cash;
    }

    public function setOvertimeCash($value) {

      if ($value < 0) {
        return FALSE;
      }

      $rate = $this->getEmployee()->getOvertimeRate();
      $overtime = (float) $value;

      // zaporny bonus - pokuta za "podcas"
      if ($this->getOvertime(TRUE) < 0) {
        $this->_row->overtime_cash = -$overtime;
        $this->_row->overtime_bonus = -$overtime * $rate;
      }

      // odmena za prescas
      else {
        $this->_row->overtime_cash = $overtime;
        $rateExtra = $this->getEmployee()->getOvertimeRate(TRUE);
        $overtime_extra = 0;
        if ($overtime > 20) {
          $overtime_extra = $overtime - 20;
          $overtime = 20;
        }
        $this->_row->overtime_bonus =
          $overtime * $rate + $overtime_extra * ($rateExtra ? $rateExtra : $rate);
      }
      return $this->_row->save();
    }

    /**
     * @return string
     */
    public function getOvertimeBank() {
      return $this->_row->overtime_bank;
    }

    public function setOvertimeBank($value) {

      if ($value < 0) {
        return FALSE;
      }

      $rate = $this->getEmployee()->getOvertimeRate();
      $overtime = (float) $value;

      $this->_row->overtime_bank = $overtime;

      $rateExtra = $this->getEmployee()->getOvertimeRate(TRUE);
      $overtime_extra = 0;
      if ($overtime > 20) {
        $overtime_extra = $overtime - 20;
        $overtime = 20;
      }
      $this->_row->overtime_bank_bonus =
        $overtime * $rate + $overtime_extra * ($rateExtra ? $rateExtra : $rate);

      return $this->_row->save();
    }

    public function getObligation($total = NULL) {

      $obligation = $this->_employee->getObligation();
      if ($this->_employee->getType() === Table_Employees::TYPE_EMPLOYEE) {
        return (float) round(eval('return ' . $obligation . '*' . $total . ';'));
      }
      else {
        return (float) $obligation;
      }
    }

    public function getStatus() {
      return $this->_row->status;
    }

    public function isClosed() {
      return $this->getStatus() == self::REWARD_STATUS_CLOSED;
    }

    public function setStatus($status) {
      if (!in_array($status, array(
        self::REWARD_STATUS_EMPTY,
        self::REWARD_STATUS_CLOSED,
        self::REWARD_STATUS_OPEN,
        self::REWARD_STATUS_FAIL,
      ))) {
        throw new Employees_Exceptions_RewardUnknownStatus('Požadavek na změnu do neznámého stavu.');
      };
      $this->_row->status = $status;
      $this->_row->save();
    }

    public function getCloseForm($foredit = TRUE) {
      $form = new Employees_CloseMonthForm();
      $form->setType($this->_employee->getType());
      if ($this->_employee->getType() === Table_Employees::TYPE_EMPLOYEE) {
        $form->setEmployeeReward($this, $foredit);
      }
      else {
        $form->setSupplierReward($this, $foredit);
      }
      return $form;
    }

    public function getTotal() {
      return $this->_row->total;
    }

    public function getUnpaid() {
      return $this->_row->unpaid;
    }

    public function getVacation(): float
    {
      return (float) $this->_row->vacation;
    }

    public function getVacationSum($year = NULL) {
      return $this->_table->getVacation($this->_employee->getId(), $year);
    }

    public function getSickness() {
      return $this->_row->sickness;
    }

    public function getKm() {
      return $this->_row->km;
    }

    public function getCarBonus() {
      if ($this->getStatus() == self::REWARD_STATUS_CLOSED) {
        return (float) $this->_row->car_bonus;
      }
      else {
        return (float) $this->getKm() * Table_Settings::get('employees_car_rate');
      }
    }

    public function getBase() {
      return $this->isClosed() ? $this->_row->base : $this->getEmployee()->getWage();
    }

    public function getPersonalBonus() {
      return $this->isClosed() ? $this->_row->personal_bonus : $this->getEmployee()->getBonus();
    }

    public function getTransportBonus() {
      return $this->isClosed() ? $this->_row->transport_bonus : $this->getEmployee()->getTransportAllowance();
    }

    public function getDiets() {
      return $this->_row->diets;
    }

    public function getDietsFrgn() {
      return $this->_row->diets_frgn;
    }


    public function getDate() {
      return $this->_date;
    }

    public function getEmployee() {
      return $this->_employee;
    }

    public function getHours() {
      return $this->_row->hours;
    }

    public function getMainReward() {
      return $this->_row->main_reward;
    }

    public function getDeptRewards() {
      $table = new Table_EmployeesRewardsDept();
      return $table->getRewards($this->getEmployee()->getId(), $this->getDate()->get('Y'), $this->getDate()->get('m'));
    }

    public function getDeptReward() {
      $table = new Table_EmployeesRewardsDept();
      return $table->getTotal($this->getEmployee()->getId(), $this->getDate()->get('Y'), $this->getDate()->get('m'));
    }

    public function getTotalReward() {

      if ($this->getEmployee()->getType() == Table_Employees::TYPE_EMPLOYEE) {
        $total = $this->getBase()
          + $this->getOvertimeBonus()
          + $this->getOvertimeBankBonus()
          + $this->getDiets()
          + $this->getPersonalBonus()
          + $this->getTransportBonus()
          + $this->getCarBonus()
          + $this->getDeptReward();
      }
      else {
        $total = $this->getBase() * $this->getTotal()
          + $this->getCarBonus()
          + $this->getDeptReward();
      }
      return (float) $total;
    }

    public function getOvertimeBonus() {
      return $this->_row->overtime_bonus;
    }

    public function getOvertimeBankBonus() {
      return $this->_row->overtime_bank_bonus;
    }

    public function closeMonth() {
      if ($this->_employee->getType() == Table_Employees::TYPE_EMPLOYEE) {
        $this->_row->base = $this->getEmployee()->getWage();
        $this->_row->personal_bonus = (float) $this->getEmployee()->getBonus();
        $this->_row->transport_bonus = $this->getEmployee()->getTransportAllowance();
        $this->_row->car_bonus = $this->getKm() * Table_Settings::get('employees_car_rate');
        $this->_row->main_reward = $this->getTotalReward();
        if ($this->_employee->isPartTime()) {
          $this->_row->debt = 0;
        }
        else {
          $this->_row->debt = $this->getOvertimeCash() + $this->getOvertimeBank() - $this->getOvertime() + $this->getDebt();
        }
      }
      else {
        $this->_row->base = $this->getEmployee()->getWage();
        $this->_row->main_reward = $this->getTotalReward();
      }
      $this->_row->status = self::REWARD_STATUS_CLOSED;
      $this->_row->save();
    }

    /**
     * @throws Zend_Db_Table_Exception
     */
    public function getAccountData(): array
    {
      $status = $this->getStatus();
      if ($status == self::REWARD_STATUS_CLOSED) {
        $worker = $this->getEmployee();
        $date = $this->getDate();
        $work = new Employees_Work($worker, $date);
        $sheet = new Employees_WorkSheet($work);
        $vacation = array(
          'total' => $sheet->getVacation(),
          'rest' => $worker->getVacationRest((int) $date->get('Y'), (int) $date->get('m')),
          'dates' => $sheet->getVacationDates()
        );
        $sickness = array(
          'total' => $sheet->getSickness(),
          'dates' => $sheet->getSicknessDates()
        );
        $fmc = array(
          'total' => $sheet->getFMC(),
          'dates' => $sheet->getFMCDates()
        );
        $unpaid = array(
          'total' => $sheet->getUnpaidVacation(),
          'dates' => $sheet->getUnpaidVacationDates()
        );
        $tripsLocal = $sheet->getTripsDatesLocal();
        $dietsLocal = $sheet->getDiets();
        $tripsForeign = $sheet->getTripsDatesForeign();
        $dietsForeign = $sheet->getDietsFrgn();
      }
      else {
        $vacation = NULL;
        $sickness = NULL;
        $fmc = NULL;
        $unpaid = NULL;
        $tripsLocal = NULL;
        $dietsLocal = NULL;
        $tripsForeign = NULL;
        $dietsForeign = NULL;
      }
      return array(
        'status' => $status,
        'employee' => $this->_employee,
        'vacation' => $vacation,
        'sickness' => $sickness,
        'fmc' => $fmc,
        'unpaid' => $unpaid,
        'trips_local' => $tripsLocal,
        'diets_local' => $dietsLocal,
        'trips_foreign' => $tripsForeign,
        'diets_foreign' => $dietsForeign,
        'overtime' => ['time' => $this->getOvertimeBank(), 'bonus' => $this->getOvertimeBankBonus()]
      );
    }

    public function __toString() {
      try {
        return $this->_render();
      }
      catch (Exception $e) {
        return $e->getMessage();
      }
    }

    /**
     * @throws Zend_Db_Table_Exception
     */
    protected function _render(): string
    {
      $employee = $this->getEmployee();
      $date = $this->getDate();
      $vacation = $this->getVacation();
      $vacationRest = $employee->getVacationRest((int) $date->get('Y'), (int) $date->get('m'));
      if (!$this->isClosed()) {
        $vacationRest -= $vacation;
      }

      $view = new Zend_View();
      $view->setScriptPath(APPLICATION_PATH . '/views/scripts');
      $view->reward = $this;
      $view->employee = $employee;
      $view->year = (int) $this->getDate()->get('Y');
      $view->month = (int) $this->getDate()->get('m');

      $view->vacation = Meduse_Currency::format($vacation);
      $view->vacationRest = Meduse_Currency::format($vacationRest);

      return $employee->getType() == Table_Employees::TYPE_EMPLOYEE ?
        $view->render('employees/reward-employee.phtml') : $view->render('employees/reward-supplier.phtml');
    }

  }

