<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Employees_OvertimeForm extends Meduse_FormBootstrap {

    protected $_reward = NULL;

    /**
     * Konstruktor.
     */
    public function __construct($options = null) {
      if (isset($options['reward']) && $options['reward'] instanceof Employees_Reward) {
        $this->_reward = clone $options['reward'];
      }
      unset($options['reward']);
      parent::__construct($options);
    }

    /**
     * Inicializace formulare.
     */
    public function init() {
      $this->_isModalForm = TRUE;

      $eEmployeeId = new Zend_Form_Element_Hidden('id_employees_rewards');
      $this->addElement($eEmployeeId);

      $eYear = new Zend_Form_Element_Hidden('year_employees_rewards');
      $this->addElement($eYear);

      $eMonth = new Zend_Form_Element_Hidden('month_employees_rewards');
      $this->addElement($eMonth);

      $eOvertime = new Meduse_Form_Element_Float('overtime_cash');
      $eOvertime->setDescription('<div><small>Pokud přesčasy nechcete zpeněžit, zadejte hodnotu 0<br>a daný stav přesčasů se automaticky převede do dalšího měsíce.</small></div>');

      $eOverBank = new Meduse_Form_Element_Float('overtime_bank');
      $eOverBank->setLabel('Hodiny ke zpeněžení na bank. účet');
      $eOverBank->setDescription('<div><small>Pokud nechcete zpeněžit, zadejte 0.</small></div>');

      if ($this->_reward) {

        $overtime = $this->_reward->getOvertime(TRUE);
        if ($overtime < 0) {
          $eOvertime->setLabel('Hodiny k odečtu (max. ' . -$overtime . ' h)');
          $eOvertime->addValidator(new Zend_Validate_Between(['min' => 0, 'max' => -$overtime, 'inclusive' => true]));
          $eOverBank->setAttrib('disabled', 'disabled');
        }
        else {
          $eOvertime->setLabel('Hodiny ke zpeněžení (max. ' . $overtime . ' h)');
          $eOvertime->addValidator(new Zend_Validate_Between(['min' => 0, 'max' => $overtime, 'inclusive' => true]));
          $eOverBank->addValidator(new Zend_Validate_Between(['min' => 0, 'max' => $overtime, 'inclusive' => true]));
        }

        $eOvertime->setValue(abs($this->_reward->getOvertimeCash()));
        $eOverBank->setValue(abs($this->_reward->getOvertimeBank()));

        $eEmployeeId->setValue($this->_reward->getEmployee()->getId());
        $eYear->setValue($this->_reward->getDate()->get('Y'));
        $eMonth->setValue($this->_reward->getDate()->get('m'));

      }

      $this->addElement($eOvertime);
      $this->addElement($eOverBank);
      $this->addDisplayGroup(array('overtime_cash', 'overtime_bank'), 'tocash');

      $eSave = new Meduse_Form_Element_Submit('save');
      $eSave->setLabel('Zpeněžit');
      $this->addElement($eSave);
      $this->addDisplayGroup(array('save'), 'save_action');

      parent::init();
    }
  }
