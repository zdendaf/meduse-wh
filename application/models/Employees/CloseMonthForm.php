<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Employees_CloseMonthForm extends Meduse_FormBootstrap {

    protected $_onlyForEmployee = array();

    public function init() {

      $this->setAttrib('class', 'form');

      $element = new Zend_Form_Element_Hidden('type');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Float('hours');
      $element->setLabel('Závazek [h/měs]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('total');
      $element->setAttrib('readonly', 'readonly');
      $element->setLabel('Odpracováno [h]');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Float('debt');
      $element->setLabel('Převedeno [h]');
      $element->setAttrib('class', 'text-right');
      $element->setAttrib('readonly', 'readonly');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('overtime');
      $element->setLabel('Přesčas [h]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('km');
      $element->setLabel('Najeto [km]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('overtime_money');
      $element->setLabel('Zpeněžit přesčas [h]');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('overtime_rate');
      $element->setLabel('Přesč. sazba [Kč/h]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('overtime_extra_rate');
      $element->setLabel('Turbo sazba [Kč/h]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('base');
      $element->setLabel('Mzdový základ [Kč]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Float('overtime_bonus');
      $element->setLabel('Přesčas odměna [Kč]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Float('diets');
      $element->setLabel('Diety [Kč]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('diets_frgn');
      $element->setLabel('Zahr. diety [€]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('personal_bonus');
      $element->setLabel('Bonus [Kč]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('transport_bonus');
      $element->setLabel('Příp. na dopravu [Kč]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('car_bonus');
      $element->setLabel('Použití os. auta [Kč]');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);
      $this->setOnlyForEmployee($element);

      $element = new Meduse_Form_Element_Float('dept_reward');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $element->setLabel('Os. ohodnocení [Kč]');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Float('main_reward');
      $element->setLabel('Odměna/Penále [Kč]');
      $element->setAttrib('class', 'text-right');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('sum');
      $element->setAttrib('readonly', 'readonly');
      $element->setAttrib('class', 'text-right');
      $element->setLabel('Celkem');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Textarea('note');
      $element->setLabel('Poznámka');
      $element->setAttrib('rows', '3');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('close');
      $element->setLabel('Uzavřít');
      $this->addElement($element);

      $this->_isModalForm = TRUE;
      
      parent::init();
    }

    protected function setOnlyForEmployee(Zend_Form_Element $element) {
      $this->_onlyForEmployee[] = $element->getName();
    }

    public function setType($type) {
      $this->getElement('type')->setValue($type);
      if ($type === Table_Employees::TYPE_SUPPLIER) {
        foreach ($this->_onlyForEmployee as $name) {
          $this->removeElement($name);
        }
        $this->getElement('base')->setLabel('Hodinová sazba [Kč]');
        $this->getElement('overtime_bonus')->setLabel('Základ [Kč]');
      }
    }

    public function setEmployeeReward(Employees_Reward $reward, $foredit = TRUE) {

      $this->setLegend('Uzavření měsíce '
        . $reward->getDate()->toString(' m/Y – ')
        . $reward->getEmployee()->getFullName());

      $hours = $reward->getHours();
      $total = $reward->getTotal();
      $debt = $reward->getDebt();

      $this->getElement('total')->setValue($total);
      $this->getElement('hours')->setValue($hours);
      $this->getElement('debt')->setValue(-$debt);
      $this->getElement('overtime')->setValue(($total - $debt) - $hours);
      $this->getElement('km')->setValue($reward->getKm());

      $this->getElement('overtime_bonus')->addValidator(new Zend_Validate_LessThan(max(array(-$debt, 0)), FALSE));
      $this->getElement('overtime_rate')->setValue($reward->getEmployee()->getOvertimeRate(FALSE));
      $this->getElement('overtime_extra_rate')->setValue($reward->getEmployee()->getOvertimeRate(TRUE));

      $diets = (float) $reward->getDiets();
      $this->getElement('diets')->setValue($diets);
      $diets_frgn = (float) $reward->getDietsFrgn();
      $this->getElement('diets_frgn')->setValue($diets_frgn);

      $base = $reward->getEmployee()->getWage();
      $this->getElement('base')->setValue($base);
      $personal_bonus = $reward->getEmployee()->getBonus();
      $this->getElement('personal_bonus')->setValue($personal_bonus);
      $transport_bonus = $reward->getEmployee()->getTransportAllowance();
      $this->getElement('transport_bonus')->setValue($transport_bonus);
      $car_bonus = $foredit ? $reward->getKm() * Table_Settings::get('employees_car_rate') : $reward->getCarBonus();
      $this->getElement('car_bonus')->setValue($car_bonus);
      $dept_reward = $reward->getDeptReward();
      $this->getElement('dept_reward')->setValue($dept_reward);
      $main_reward = $reward->getMainReward();
      $this->getElement('main_reward')->setValue($main_reward);

      $sum_czk = ($base + $personal_bonus + $diets + $transport_bonus + $car_bonus + $dept_reward + $main_reward) . ' Kč';
      $sum_eur = $diets_frgn . ' €';
      $this->getElement('sum')->setValue($sum_czk . ' + ' . $sum_eur);

      if (!$foredit) {
        $this->setReadOnly();
      }
    }

    public function setSupplierReward(Employees_Reward $reward, $foredit = TRUE) {

      $this->setLegend('Uzavření měsíce '
        . $reward->getDate()->toString(' m/Y – ')
        . $reward->getEmployee()->getFullName());

      $hours = $reward->getHours();
      $total = $reward->getTotal();

      $this->getElement('total')->setValue($total);

      $wage = $reward->getEmployee()->getWage();
      $this->getElement('base')->setValue($wage);

      $base = $wage * $total;
      $this->getElement('overtime_bonus')->setValue($base);

      $main_reward = $reward->getMainReward();
      $this->getElement('main_reward')->setValue($main_reward);

      $dept_reward = $reward->getDeptReward();
      $this->getElement('dept_reward')->setValue($dept_reward);

      $this->getElement('sum')->setValue($base + $main_reward + $dept_reward);

      if (!$foredit) {
        $this->setReadOnly();
      }
    }

    public function setReadonly() {
      $this->setLegend('Přehled ' . substr($this->getLegend(), 10));
      foreach ($this->getElements() as $element) {
        $value =  $element->getValue();
        $name = $element->getName();
        if (!$value && in_array($name, array('personal_bonus', 'transport_bonus', 'car_bonus'))) {
          $this->removeElement($element->getName());
        }
        if ($name != 'close') {
          $element->setAttrib('readonly', 'readonly');
        }
        else {
          $element->setLabel('Zpět');
        }
      }

    }
  }

