<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Employees_Absence {
    
    protected $_table;
    protected $_employee;
    protected $_date;
    
    public function __construct(Employees_Employee $employee, Meduse_Date $date) {
      
      $this->_table = new Table_EmployeesAbsence();
      $this->_employee = $employee;
      $this->_date = $date;
      
      $select = $this->_table->select()->setIntegrityCheck(FALSE);
      
      // potrebujeme vybrat vsechny terminy, ktere spadaji do prislusneho mesice
      
      $nextMonth = $this->_date->addMonth(1);
      
      $select->where('id_employees = ?', $this->_employee->getId());
      $select->where('start <= ?', $this->_date->get('Y-m-01'));
      
    }
    
    /**
     * Vrati data dovolené
     * @param bool $approved
     *    (volitelne)
     */
    public function getVacation($approved = TRUE) {}
    
    
  }

  