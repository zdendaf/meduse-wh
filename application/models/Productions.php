<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 17.12.2018
 * Time: 17:14
 */

class Productions {

  public static function getProductionsRows($wh, $status = NULL, $categories = []) {
    $table = new Table_Productions();
    $data = $table->getAllProductions($wh, $status, $categories);
    return $data;
  }

  public static function create($wh, $data) {
    if ($wh == Table_TobaccoWarehouse::WH_TYPE) {
      $production  = new Tobacco_Productions_Production();
      $production->setProducer($data['id_producers']);
      $production->setDateOrdering($data['date_ordering']);
      $production->setParts($data['parts']);
      if ($production->save()) {
        return $production;
      }
    }
    throw new Exception('Není implementováno pro sklad Meduse.');
  }
}