<?php

class Pricelists {

  const CTG_NUM = 8;
  const PRICE_LIST_PREFIX = 'W';
  const PRICE_LIST_MASTER_ID = 'RTL';
  const PRICE_LIST_COSTS_ID = 'COSTS';
  const TEMPLATE_PATH = '../public/pdf/pricelist.pdf';
  const ROUNDING_PRECISION = [
    'default' => 2,
    Table_PartsCtg::TYPE_SETS => 0,
  ];

  const TEXTS_IDX = [
      'pricelists_footer_text' => 'Text na konci',
    ];

  const MAPPING_PREFIX  = 'pricelists_mapping_';

  public static function getCtgDefaults(): array {
    $table = new Table_PricelistsCtg();
    $select = $table->select()->where('id <= ?', self::CTG_NUM);
    $defaults = [];
    $result = $select->query(Zend_Db::FETCH_ASSOC);
    try {
      while ($row = $result->fetch()) {
        $defaults[$row['id']] = (int) $row['discount'];
      }
    }
    catch (Zend_Db_Statement_Exception $e) {}
    return $defaults;
  }

  public static function setCtgDefaults($data) {
    $table = new Table_PricelistsCtg();
    if ($data) {
      foreach ($data as $item) {
        try {
          $table->insert($item);
        }
        catch (Zend_Db_Statement_Exception $e) {
          $table->update($item, 'id = ' . $item['id']);
        }
      }
    }
  }

  public static function setCtgCollection($collectionId, $data) {
    $table = new Table_CollectionsPricelistsCtg();
    if ($data) {
      foreach ($data as $item) {
        $item['id_collections'] = $collectionId;
        $where = 'id_collections = ' . $item['id_collections'] . ' AND id_pricelists_ctg = ' . $item['id_pricelists_ctg'];
        if (!is_null($item['discount'])) {
          try {
            $table->insert($item);
          }
          catch (Zend_Db_Statement_Exception $e) {
            $table->update($item, $where);
          }
        }
        else {
          $table->delete($where);
        }
      }
    }
  }

  public static function setCtgPartsCtg($ctgId, $data) {
    $table = new Table_PartsCtgPricelistsCtg();
    if ($data) {
      foreach ($data as $item) {
        $item['id_parts_ctg'] = $ctgId;
        $where = 'id_parts_ctg = ' . $item['id_parts_ctg'] . ' AND id_pricelists_ctg = ' . $item['id_pricelists_ctg'];
        if (!is_null($item['discount'])) {
          try {
            $table->insert($item);
          }
          catch (Zend_Db_Statement_Exception $e) {
            $table->update($item, $where);
          }
        }
        else {
          $table->delete($where);
        }
      }
    }
  }

  public static function setCtgPart($partId, $data) {
    $table = new Table_PartsPricelistsCtg();
    if ($data) {
      foreach ($data as $item) {
        $item['id_parts'] = $partId;
        $where = 'id_parts = ' . $table->getAdapter()->quote($item['id_parts']) . ' AND id_pricelists_ctg = ' . $item['id_pricelists_ctg'];
        if (!is_null($item['discount'])) {
          try {
            $table->insert($item);
          }
          catch (Zend_Db_Statement_Exception $e) {
            $table->update($item, $where);
          }
        }
        else {
          $table->delete($where);
        }
      }
    }
  }

  /**
   * Vrátí asociativní pole se slevovými kategoriemi pro kolekce.
   *
   * @param null $idCollection
   * @param bool $fillDefaults
   *
   * @return array
   * @throws \Zend_Db_Statement_Exception
   */
  public static function getCtgCollections($idCollection = NULL, bool $fillDefaults = TRUE): array {

    $collCtg = [];
    $defaults = self::getCtgDefaults();
    $collections = Parts::getCollections();

    // Naplnění výchozími hodnotami.
    if ($fillDefaults) {
      foreach ($collections as $collection) {
        $collCtg[$collection['id']] = [];
        for ($ctgId = 1; $ctgId <= self::CTG_NUM; $ctgId++) {
          $collCtg[$collection['id']][$ctgId] = $defaults[$ctgId];
        }
      }
    }

    // Změna překrytých hodnot pro kolekce.
    $table = new Table_CollectionsPricelistsCtg();
    $select = $table->select()->where('id_pricelists_ctg <= ?', self::CTG_NUM);
    if ($collResult = $select->query(Zend_Db::FETCH_ASSOC)) {
      while ($row = $collResult->fetch()) {
        if (!key_exists($row['id_collections'], $collCtg)) {
          $collCtg[$row['id_collections']] = [];
        }
        $collCtg[$row['id_collections']][$row['id_pricelists_ctg']] = (int) $row['discount'];
      }
    }

    // Vrácení kategorií pro požadovanou kolekci nebo všech kolekcí.
    if ($idCollection) {
        return key_exists($idCollection, $collCtg) ? $collCtg[$idCollection] : [];
    }
    return $collCtg;
  }

  /**
   * Vrátí asociativní pole se slevovými kategoriemi pro produktové kategorie.
   *
   * @param null $idPartCtg
   * @param bool $fillDefaults
   *
   * @return array
   * @throws \Zend_Db_Statement_Exception
   */
  public static function getCtgPartsCtg($idPartCtg = NULL, bool $fillDefaults = TRUE): array {

    $partCtg = [];
    $defaults = self::getCtgDefaults();
    $categories = Parts::getCategories();

    // Naplnění výchozími hodnotami.
    if ($fillDefaults) {
      foreach ($categories as $idx => $category) {
        $partCtg[$idx] = [];
        for ($ctgId = 1; $ctgId <= self::CTG_NUM; $ctgId++) {
          $partCtg[$idx][$ctgId] = $defaults[$ctgId];
        }
      }
    }

    // Změna překrytých hodnot pro kategorii.
    $table = new Table_PartsCtgPricelistsCtg();
    $select = $table->select()->where('id_pricelists_ctg <= ?', self::CTG_NUM);
    if ($collResult = $select->query(Zend_Db::FETCH_ASSOC)) {
      while ($row = $collResult->fetch()) {
        if (!key_exists($row['id_parts_ctg'], $partCtg)) {
          $partCtg[$row['id_parts_ctg']] = [];
        }
        $partCtg[$row['id_parts_ctg']][$row['id_pricelists_ctg']] = (int) $row['discount'];
      }
    }

    // Vrácení kategorií pro požadovanou kategorii nebo všech.
    if ($idPartCtg) {
      return key_exists($idPartCtg, $partCtg) ? $partCtg[$idPartCtg] : [];
    }
    return $partCtg;
  }

  /**
   * Vrátí asociativní pole se slevovými kategoriemi pro produkty.
   *
   * @param $idParts
   * @param bool $fillDefaults
   *
   * @return array
   * @throws \Zend_Db_Statement_Exception
   */
  public static function getCtgParts($idParts = NULL, bool $fillDefaults = TRUE): array {

    $categories = [];
    $parts = Parts::getProducts(['withCollections' => TRUE]);

    try {
      $defCats = self::getCtgDefaults();
      $colCats = self::getCtgCollections();
      $accCats = self::getCtgPartsCtg(Table_PartsCtg::TYPE_ACCESSORIES);
    }
    catch(Exception $e) {
      Utils::logger()->warn($e->getMessage());
      $colCats = [];
      $accCats = [];
      $defCats = [];
    }

    // Naplnění výchozími hodnotami.
    if ($fillDefaults && $parts && $colCats && $accCats) {
      foreach ($parts as $item) {
        if ($idParts && $idParts !== $item['id']) {
          continue;
        }
        $categories[$item['id']] = [];
        for ($ctgId = 1; $ctgId <= self::CTG_NUM; $ctgId++) {

          // Výchozí hodnoty pro produkt z kategorie Accessories.
          if ($item['ctg_parent_id'] == Table_PartsCtg::TYPE_ACCESSORIES) {
            if (isset($accCats[$ctgId])) {
              $categories[$item['id']][$ctgId] = $accCats[$ctgId];
            }
          }

          // Jinak se díváme na kolekce.
          else {
            if (isset($item['collections'])) {
              $discounts = [];
              foreach ($item['collections'] as $collection) {
                if (isset($colCats[$collection['collection_id']][$ctgId])) {
                  $discounts[] = $colCats[$collection['collection_id']][$ctgId];
                }
              }
              $categories[$item['id']][$ctgId] = $discounts ? min($discounts) : 0;
            }
            else {
              // Produkt není v žádné kolekci.
              $categories[$item['id']][$ctgId] = $defCats[$ctgId];
            }

          }
        }
      }
    }

    // Změna překrytých hodnot pro produkty.
    $table = new Table_PartsPricelistsCtg();
    $select = $table->select()->where('id_pricelists_ctg <= ?', self::CTG_NUM);
    if ($idParts) {
      $select->where('id_parts = ?', $idParts);
    }
    if ($collResult = $select->query(Zend_Db::FETCH_ASSOC)) {
      while ($row = $collResult->fetch()) {
        if (!key_exists($row['id_parts'], $categories)) {
          $categories[$row['id_parts']] = [];
        }
        $categories[$row['id_parts']][$row['id_pricelists_ctg']] = (int) $row['discount'];
      }
    }

    // Vrácení kategorií pro požadovanou kolekci nebo všech kolekcí.
    if ($idParts) {
      return key_exists($idParts, $categories) ? $categories[$idParts] : [];
    }
    return $categories;
  }

  public static function getCtgDefaultsForm(): Zend_Form {
    return new Pricelists_CtgDefaultForm();
  }

  public static function getCtgLimitsForm(): Zend_Form {
    return new Pricelists_CtgLimitsForm();
  }

  /**
   * @throws \Exception
   */
  public static function getCtgCollectionForm($collectionId): Zend_Form {
    return new Pricelists_CtgCollectionForm(['id_collection' => $collectionId]);
  }

  public static function getCtgAccessoriesForm(): Zend_Form {
    return new Pricelists_CtgAccessoriesForm();
  }

  /**
   * @throws \Exception
   */
  public static function getCtgPartForm($partId): Zend_Form {
    return new Pricelists_CtgPartForm(['id_part' => $partId]);
  }

  /**
   * TODO vraci to samé jako computePrices, ale bere data z uložených cen ceníku. Neprovádí výpočet
   *
   * @param null $ctgId
   * @return array
   * @throws Zend_Db_Statement_Exception
   */
  public static function loadPrices($ctgId = NULL): array
  {
    $pricesArray = [];

    $mapping = self::getMapping();
    $tPrices = new Table_Prices();

    $table = new Table_Pricelists();
    if (!$plInfo = $table->getPricelists([$mapping['price_list_' . self::PRICE_LIST_MASTER_ID]])) {
      Utils::logger()->warn('Není definovaný master ceník');
      return [];
    }
    $plCodes = $plInfo->getColumnNames();
    $plCode = array_shift($plCodes);
    $mainPrices = $plInfo->getPrices();
    $vat = Utils::getActualVat(TRUE);
    $vatCoef = 1 + $vat / 100;

    // Globální krycí náklad.
    $table = new Table_ApplicationCoverCost();
    $cover_cost_global = (float) $table->getCoverCost();

    // Zadaný kurz CZK/EUR (ceníky jsou v EUR, náklady v CZK).
    $table = new Table_ApplicationRate();
    $rate = (float) $table->getRate();

    if ($products = Parts::getProducts(['withCollections' => TRUE])) {
      foreach ($products as $partId => $item) {

        $precision = self::ROUNDING_PRECISION[$item['ctg_id']] ?? self::ROUNDING_PRECISION['default'];

        // Vypočet výrobních nákladů.
        $cover_cost = (float) ($item['cover_cost'] ?? $cover_cost_global);
        $cost_nett = round($item['price'] / $rate, 2);
        $cost = round($item['price'] * (1 + $cover_cost / 100) / $rate, 2);

        $prices = [];
        // $margins = [];

        // Vypočet prodejní ceny a marží.
        $mPrice = array_key_exists($partId, $mainPrices) ? (float) $mainPrices[$partId][$plCode] : NULL;
        if ($discounts = self::getCtgParts($partId)) {
          foreach ($discounts as $idx => $discount) {
            $prices[$idx] = $tPrices->getProductPrice($partId, $mapping['price_list_' . self::PRICE_LIST_PREFIX . $idx]);
          }
        }

        // Vložení do návratového asociativního pole.
        $pricesArray[$partId] = [
          'id' => $partId,
          'designated' => $item['designated'],
          'name' => $item['name'],
          'category_id' => (int) $item['ctg_id'],
          'category_name' => $item['ctg_name'],
          'category_order' => (int) $item['ctg_order'],
          'parent_id' => (int) $item['ctg_parent_id'],
          'parent_name' => $item['ctg_parent_name'],
          'parent_order' => (int) $item['ctg_parent_order'],
          'cost' => $cost,
          'cost_nett' => $cost_nett,
          'master_price' => round($mPrice, $precision),
          'vat' => $vat,
          'master_price_vat' => round($mPrice * $vatCoef, $precision),
          'master_margin' => $cost === 0 ? '–' : round(($mPrice - $cost) / $cost * 100),
          // 'discounts' => $discounts,
          'prices' => $prices,
          // 'margins' => $margins,
          'collections' => $item['collections'] ?? [],
        ];
      }
    }

    uasort($pricesArray, ['self', 'sortProducts']);

    return $pricesArray;
  }

  /**
   * Vypočítá ceny na základě master ceníku a zadaných slevových kategorií.
   * Součástí vráceného asiciatvního pole jsou výrobní náklady včetně krycích nákladů
   * a marže vůči jednotlivým slevovým kategoriím (počítáno z nákladů včetně krytí).
   *
   * @throws Zend_Db_Statement_Exception
   */
  public static function computePrices($ctgId = NULL): array {

    $mapping = self::getMapping(0, true);
    $masterId = reset($mapping);
    $pricesArray = [];

    $table = new Table_Pricelists();
    if (!$plInfo = $table->getPricelists([$masterId])) {
      Utils::logger()->warn('Není definovaný master ceník');
      return [];
    }
    $plCodes = $plInfo->getColumnNames();
    $plCode = array_shift($plCodes);
    $mainPrices = $plInfo->getPrices();
    $vat = Utils::getActualVat(TRUE);
    $vatCoef = 1 + $vat / 100;

    // Globální krycí náklad.
    $table = new Table_ApplicationCoverCost();
    $cover_cost_global = (float) $table->getCoverCost();

    // Zadaný kurz CZK/EUR (ceníky jsou v EUR, náklady v CZK).
    $table = new Table_ApplicationRate();
    $rate = (float) $table->getRate();

    if ($products = Parts::getProducts(['withCollections' => TRUE])) {
      foreach ($products as $partId => $item) {

        $precision = self::ROUNDING_PRECISION[$item['ctg_id']] ?? self::ROUNDING_PRECISION['default'];

        // Vypočet výrobních nákladů.
        $cover_cost = (float) ($item['cover_cost'] ?? $cover_cost_global);
        $cost_nett = round ($item['price'] / $rate, 2);
        $cost = round($item['price'] * (1 + $cover_cost / 100) / $rate, 2);

        $prices = [];
        $margins = [];

        // Vypočet prodejní ceny a marží.
        $mPrice = array_key_exists($partId, $mainPrices) ? (float) $mainPrices[$partId][$plCode] : NULL;
        if ($discounts = self::getCtgParts($partId)) {
          foreach ($discounts as $idx => $discount) {
            $prices[$idx] = round($mPrice * ((100 - $discount) / 100), $precision);
            $margins[$idx] = $cost === 0 ? '–' : round(($prices[$idx] - $cost) / $cost * 100);
          }
        }

        // Vložení do návratového asociativního pole.
        $pricesArray[$partId] = [
          'id' => $partId,
          'designated' => $item['designated'],
          'name' => $item['name'],
          'category_id' => (int) $item['ctg_id'],
          'category_name' => $item['ctg_name'],
          'category_order' => (int) $item['ctg_order'],
          'parent_id' => (int) $item['ctg_parent_id'],
          'parent_name' => $item['ctg_parent_name'],
          'parent_order' => (int) $item['ctg_parent_order'],
          'cost' => $cost,
          'cost_nett' => $cost_nett,
          'master_price' => $mPrice,
          'vat' => $vat,
          'master_price_vat' => round($mPrice * $vatCoef, $precision),
          'master_margin' => ($cost === 0) ? '–' : round(($mPrice - $cost) / $cost * 100),
          'discounts' => $discounts,
          'prices' => $prices,
          'margins' => $margins,
          'collections' => $item['collections'] ?? [],
        ];
      }
    }

    uasort($pricesArray, ['self', 'sortProducts']);

    return $pricesArray;
  }

  /**
   * Callback pro řazení produktových položek v ceníku.
   */
  protected static function sortProducts($a, $b): int {
    if ($a['parent_order'] === $b['parent_order']) {
      if ($a['category_order'] === $b['category_order']) {
        if ($a['category_id'] == Table_PartsCtg::TYPE_SETS) {
          $collA = reset($a['collections']);
          $collB = reset($b['collections']);
          if ($collA['collection_id'] === $collB['collection_id']) {
            if ($a['id'] === $b['id']) return 0;
            else return $a['name'] < $b['name'] ? -1 : 1;
          }
          else {
            return $collA['collection_name'] < $collB['collection_name'] ? -1 : 1;
          }
        }
        else {
          if ($a['id'] === $b['id']) {
            return 0;
          }
          else {
            return $a['name'] < $b['name'] ? -1 : 1;
          }
        }
      }
      else {
        return $a['category_order'] < $b['category_order'] ? -1 : 1;
      }
    }
    else {
      return (int) $a['parent_order'] < (int) $b['parent_order'] ? -1 : 1;
    }
  }

  public static function getAllCategoriesIdx($ctgId = NULL, $addRetail = false, $addCosts = false): array {
    $categories = [];
    if (!$ctgId) {
      for ($idx = 1; $idx <= self::CTG_NUM; $idx++) {
        $categories[$idx] = $idx;
      }
    }
    elseif (is_array($ctgId)) {
      $categories = $ctgId;
    }
    else {
      $categories[] = $ctgId;
    }

    if ($addRetail) {
      array_unshift($categories, 0);
    }

    if ($addCosts) {
      array_unshift($categories, -1);
    }

    return $categories;
  }

  public static function getAllPriceListIds(): array
  {
    $indexes = [
      -1 => self::PRICE_LIST_COSTS_ID,
      0 => self::PRICE_LIST_MASTER_ID,
    ];
    for ($idx = 1; $idx <= self::CTG_NUM; $idx++) {
      $indexes[$idx] = self::PRICE_LIST_PREFIX . $idx;
    }
    return $indexes;
  }



  public static function generate($ctgId = NULL, $draft = false, &$problems = [], $wh = Table_PartsWarehouse::WH_TYPE): array {
    $generated = [];
    $categories = self::getAllCategoriesIdx($ctgId);

    // Když je draft == false, pak vygenerované ceny ukladam do ostrých ceníků.
    // Je proto nutné smazat cache.
    if (!$draft) {
      $cache = self::getCache();
      $cache->remove(self::getCacheId(false));
    }

    // tady procujeme jen s vypočítanými cenami
    $prices = self::getPrices(NULL, true);

    $tPricelists = new Table_Pricelists();
    $tPrices = new Table_Prices();
    foreach ($categories as $idx) {
      if ($draft && $idx === 0) {
        continue;
      }
      $mapping = Pricelists::getMapping($idx, $draft);
      $pl = $tPricelists->find(reset($mapping))->current()->toArray();
      $plId = $pl['id'];
      $generated[$plId] = $pl['abbr'];

      $tPrices->delete('id_pricelists = ' . $plId);
      foreach ($prices as $partId => $item) {
        if ($idx === -1) {
          $price = $item['cost_nett'];
        }
        elseif ($idx === 0) {
          $price = $item['master_price'];
        }
        else {
          $price = $item['prices'][$idx];
        }

        if (!$price) {
          if ($idx === -1) {
            $msg = 'Produkt ' . $partId . ' má nulové výrobní náklady.';
          }
          else {
            $msg = 'Pro produkt ' . $partId . ' není v ceníku ' . ($idx === 0 ? 'RTL' : 'W' . $idx) . ' definovaná cena.';
          }
          $problems[] = $msg;
          Utils::logger()->warn($msg);
          continue;
        }
        $data = [
          'id_parts' => $partId,
          'id_pricelists' => $plId,
          'price' => $price,
        ];
        $tPrices->insert($data);
      }
    }
    Utils::logger()->info('Byly generovany ceniky: ' . implode(',', $generated));
    return $generated;
  }

  /**
   * Rutina pro generování záhlaví stránek ceníku.
   *
   * @param $row
   * @param $pageWidth
   * @param $marginTop
   * @param $marginBottom
   * @param $page
   * @param $doc
   * @param $pageHeight
   * @param $lineHeight
   * @param $tabs
   * @param $plCodes
   * @param $limits
   * @param $numPartsInSection
   * @param $draft
   * @return void
   * @throws Zend_Pdf_Exception
   */
  private static function generatePdfHeader(&$row, $pageWidth, $marginTop, $marginBottom, &$page, $doc, $pageHeight, $lineHeight, $tabs, $plCodes, $limits, $numPartsInSection, $draft) {
    if ($row < $marginBottom || ($numPartsInSection === 0 && $row < $marginBottom + $lineHeight * 4)) {
      if ($page !== 0) {
        $doc->pages[] = new Zend_Pdf_Page($doc->pages[0]);
        $doc->setPageNumber($page);
      }
      $page = $doc->getPageNumber() + 1;
      $doc->fillText('page ' . $page, [$pageWidth / 2, $marginBottom - $lineHeight * 6.5]);

      $row = $pageHeight - $marginTop;
      $text = 'Meduse Design Price List' . ($draft ? ' DRAFT' : '');
      $doc->fillTitle($text, [$tabs['1'], $row]);
      foreach ($plCodes as $plCode) {
        $idx = $plCode === 0 ? 'RTL' : 'W' . $plCode;
        $doc->fillTextBold($idx === 'RTL' ? 'RETAIL' : $idx . ($draft ? 'D' : ''), [$tabs[$idx], $row], $doc::TEXT_ALIGN_RIGHT);
      }
      if (count($plCodes) >= 2) {
        $row -= $lineHeight * 4;
        $doc->fillText('number of pieces per one-shot order', [$tabs['1'], $row]);
        foreach ($plCodes as $plCode) {
          if ($plCode > 0) {
            $idx = 'W' . $plCode;
            $doc->fillText($limits[$plCode], [$tabs[$idx], $row], $doc::TEXT_ALIGN_RIGHT);
          }
        }
      }
      $row -= $lineHeight * 6;
    }
  }

  /**
   * Rutina pro vlozeni uživatelského textu na poslední stránku ceníku.
   *
   * @param $margin
   * @param $marginBottom
   * @param $doc
   * @param $lineHeight
   * @return void
   */
  private static function generatePdfFooter($margin, $marginBottom, $doc, $lineHeight)
  {
     $footerText = self::getFooterText();
     $doc->fillText($footerText, [$margin, $marginBottom - $lineHeight * 6.5]);
  }

  /**
   * Generuje PDF automatických wholesale ceníků nebo master retail ceníku.
   *
   * @param array|null $ctgIds
   * @param bool $draft
   * @param array $filter
   *
   * @return Meduse_Pdf|null
   * @throws Zend_Pdf_Exception
   */
  public static function generatePdf(array $ctgIds = NULL, bool $draft = false, array $filter = []): ?Meduse_Pdf
  {

    $filterCollections = $filter['collections'] ?? [];
    $filterCategories = $filter['categories'] ?? [];
    $filterExcluded = $filter['excluded'] ?? [];

    $plCodes = array_reverse(self::getAllCategoriesIdx($ctgIds));
    $prices = self::getPrices(NULL, $draft);

    $doc = Meduse_Pdf::load(self::TEMPLATE_PATH);

    $margin = 28;
    $marginTop = 80;
    $marginBottom = 50;
    $pageHeight = 600;
    $pageWidth = 840;
    $pricesWidth = 530;
    $maxPriceWidth = 100;
    $priceWidth = min(round($pricesWidth / count($plCodes)), $maxPriceWidth);
    $skuWidth = 60;
    $lineHeight = 3;
    $page = 0;

    $row = 0;

    $it = 0;
    $tabs = [ '1' => $margin, '2' => $margin + $skuWidth ];
    foreach ($plCodes as $plCode) {
      $idx = $plCode === 0 ? 'RTL' : 'W' . $plCode;
      $tabs[$idx] = $pageWidth - ($margin + ($it * $priceWidth));
      $it++;
    }

    $limits = self::getCtgLimits();

    $last_category_id = NULL;
    $last_collection_id = NULL;
    $numPartsInSection = 0;

    foreach ($prices as $partId => $item) {
      if ($filterExcluded && in_array($partId, $filterExcluded)) {
        continue;
      }
      if ($filterCategories && !in_array($item['category_id'], $filterCategories)) {
        continue;
      }
      if ($filterCollections) {
        $itemCollection = array_map(function ($i){
          return $i['collection_id'];
        }, $item['collections']);
        if (!array_intersect($itemCollection, $filterCollections)) {
          continue;
        }
      }

      self::generatePdfHeader($row, $pageWidth, $marginTop, $marginBottom, $page, $doc, $pageHeight, $lineHeight, $tabs, $plCodes, $limits, $numPartsInSection, $draft);

      if (($item['category_id'] !== Table_PartsCtg::TYPE_SETS && $last_category_id !== $item['category_id'])
        || ($item['category_id'] === Table_PartsCtg::TYPE_SETS && $last_collection_id !== $item['collections'][0]['collection_id'])) {
        $numPartsInSection = 0;
        self::generatePdfHeader($row, $pageWidth, $marginTop, $marginBottom, $page, $doc, $pageHeight, $lineHeight, $tabs, $plCodes, $limits, $numPartsInSection, $draft);
        $row -= $lineHeight * 4;
        $section_name = $item['category_id'] === Table_PartsCtg::TYPE_SETS ?
          $item['category_name']  . ' - ' . $item['collections'][0]['collection_name'] :
          ($item['parent_name'] ? $item['parent_name'] . ' - ' : '') . $item['category_name'];
        $doc->fillTextBold(strtoupper($section_name), [$tabs['1'], $row]);
        $y = round($row - $lineHeight * 1.5);
        $doc->pages[$doc->getPageNumber()]->drawLine($tabs['1'], $y, $pageWidth - $margin, $y);
        list(, $row) = $doc->getCoords();
        $row -= $lineHeight;
        if ($item['category_id'] === Table_PartsCtg::TYPE_SETS) {
         $last_collection_id =  $item['collections'][0]['collection_id'];
        }
        else {
          $last_category_id = $item['category_id'];
        }
      }

      $textPrices = [];
      foreach ($plCodes as $plCode) {
        $price = $plCode === 0 ? $item['master_price_vat'] : $item['prices'][$plCode];
        $textPrices[$plCode] = ($plCode === 0 && strcasecmp($item['designated'], Table_Customers::TYPE_B2B) === 0) ?
          '—' : Meduse_Currency::format($price, '€', 2, FALSE);
      }
      $doc->fillText($partId, [$tabs['1'], $row]);
      $doc->fillText($item['name'], [$tabs['2'], $row]);
      foreach ($textPrices as $plCode => $textPrice) {
        $idx = $plCode === 0 ? 'RTL' : 'W' . $plCode;
        $doc->fillText($textPrice, [$tabs[$idx], $row], $doc::TEXT_ALIGN_RIGHT);
      }
      $y = round($row - $lineHeight * 1.5);
      $doc->pages[$doc->getPageNumber()]->drawLine($tabs['1'], $y, $pageWidth - $margin, $y);
      list(, $row) = $doc->getCoords();
      $row -= $lineHeight;
      $numPartsInSection++;
    }

    self::generatePdfFooter($margin, $marginBottom, $doc, $lineHeight);

    return $doc;
  }

  /**
   * @param $row
   * @param $sheet PHPExcel_Worksheet
   * @param $plCodes
   * @param $limits
   * @param $draft
   * @return int
   * @throws PHPExcel_Exception
   */
  private static function generateXlsxHeader($row, PHPExcel_Worksheet $sheet, $plCodes, $limits, $draft): int {
      $col = 2;
      foreach ($plCodes as $plCode) {
        $idx = $plCode === 0 ? 'RTL' : 'W' . $plCode . ($draft ? 'D' : '');
        $cell = $sheet->getCellByColumnAndRow($col++, $row);
        $cell->setValue($idx === 'RTL' ? 'RETAIL' : $idx);
        $style = $cell->getStyle();
        $style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $style->getFont()->setBold(TRUE);
      }
      if (count($plCodes) >= 2) {
        $row++;
        $sheet->getCellByColumnAndRow(0, $row)->setValue('number of pieces per one-shot order');
        $sheet->mergeCellsByColumnAndRow(0, $row, 1, $row);
        $col = 3;
        foreach ($plCodes as $plCode) {
          if ($plCode > 0) {
            $sheet->getCellByColumnAndRow($col++, $row)->setValue($limits[$plCode]);
          }
        }
      }
      return ++$row;
  }


  /**
   * @throws PHPExcel_Exception
   */
  public static function generateXlsx(array $ctgIds = NULL, bool $draft = false, array $filter = []): PHPExcel {

    $filterCollections = $filter['collections'] ?? [];
    $filterCategories = $filter['categories'] ?? [];
    $filterExcluded = $filter['excluded'] ?? [];

    $plCodes = self::getAllCategoriesIdx($ctgIds);
    $prices = self::getPrices(NULL, $draft);


    $doc = new PHPExcel();
    $doc->getProperties()
      ->setCreator('Meduse Design - Warehouse')
      ->setLastModifiedBy('Meduse Design - Warehouse');
    $sheet = $doc->setActiveSheetIndex(0);
    $sheet->setTitle('Meduse Design Price List' . ($draft ? ' DRAFT' : ''));

    $row = 1;
    $col = 0;

    $value = 'TEST';
    $sheet->getCellByColumnAndRow($col, $row)->setValue($value);

    $limits = self::getCtgLimits();
    $last_category_id = NULL;
    $last_collection_id = NULL;

    foreach ($prices as $partId => $item) {
      if ($filterExcluded && in_array($partId, $filterExcluded)) {
        continue;
      }
      if ($filterCategories && !in_array($item['category_id'], $filterCategories)) {
        continue;
      }
      if ($filterCollections) {
        $itemCollection = array_map(function ($i){
          return $i['collection_id'];
        }, $item['collections']);
        if (!array_intersect($itemCollection, $filterCollections)) {
          continue;
        }
      }

      if (($item['category_id'] !== Table_PartsCtg::TYPE_SETS && $last_category_id !== $item['category_id'])
        || ($item['category_id'] === Table_PartsCtg::TYPE_SETS && $last_collection_id !== $item['collections'][0]['collection_id'])) {

        if ($row !== 1) {
          $row++;
        }

        $section_name = $item['category_id'] === Table_PartsCtg::TYPE_SETS ?
          $item['category_name']  . ' - ' . $item['collections'][0]['collection_name'] :
          ($item['parent_name'] ? $item['parent_name'] . ' - ' : '') . $item['category_name'];

        $sheet->getCellByColumnAndRow(0, $row)->setValue($section_name)->getStyle()->getFont()->setBold(TRUE);
        $sheet->mergeCellsByColumnAndRow(0, $row, 1, $row);

        if ($item['category_id'] === Table_PartsCtg::TYPE_SETS) {
          $last_collection_id =  $item['collections'][0]['collection_id'];
        }
        else {
          $last_category_id = $item['category_id'];
        }

        $row = self::generateXlsxHeader($row, $sheet, $plCodes, $limits, $draft);
      }

      $textPrices = [];
      foreach ($plCodes as $plCode) {
        $price = $plCode === 0 ? $item['master_price_vat'] : $item['prices'][$plCode];
        $textPrices[$plCode] = ($plCode === 0 && strcasecmp($item['designated'], Table_Customers::TYPE_B2B) === 0) ?
          '—' : $price;
      }
      $sheet->getCellByColumnAndRow($col, $row)->setValue($partId);
      $sheet->getCellByColumnAndRow($col + 1, $row)->setValue($item['name']);
      $pricesIdx = 0;
      foreach ($textPrices as $textPrice) {
        $pricesIdx++;
        $cell = $sheet->getCellByColumnAndRow($col + 1 + $pricesIdx, $row);
        $cell->setValue($textPrice);
        $style = $cell->getStyle();
        $style->getNumberFormat()->setFormatCode('#,##0.00');
        $style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
      }
      $row++;
    }

    $sheet->getColumnDimension('A')->setAutoSize(true);
    $sheet->getColumnDimension('B')->setAutoSize(true);

    $row++;
    $sheet->getCellByColumnAndRow(0, ++$row)->setValue(self::getFooterText())
      ->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->mergeCellsByColumnAndRow(0, $row, 1 + count($plCodes), $row);
    $sheet->getCellByColumnAndRow(0, ++$row)->setValue('https://www.meduse-experience.com/')
      ->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->mergeCellsByColumnAndRow(0, $row, 1 + count($plCodes), $row);

    return $doc;
  }


  /**
   * Master price list je DRAFT Retail price list.
   */
  public static function getMasterPricelist($draft = false): array {
    $mapping = self::getMapping(0, $draft);
    $id = reset($mapping);
    $tLists = new Table_Pricelists();
    return $tLists->find($id)->current()->toArray();
  }

  public static function getPriceLists($wh = Table_PartsWarehouse::WH_TYPE): array {
    $tLists = new Table_Pricelists();
    return $tLists->fetchAll('deleted = "n" AND id_wh_type = ' . $wh)->toArray();
  }

  public static function getPricelistsForm(): Pricelists_Form {
    $pricelistForm = new Pricelists_Form(NULL, NULL);
    $pricelistForm->setAction('/pricelists/edit');
    return $pricelistForm;
  }

  public static function getImportForm(): Pricelists_ImportForm {
    $importForm = new Pricelists_ImportForm();
    $importForm->removeElement('import');
    return $importForm;
  }

  public static function getPrices($ctgId = NULL, $draft = true, $useCache = TRUE) {

    $cache = self::getCache();
    $cacheId = self::getCacheId($draft);

    $data = $cache->load($cacheId);

    if ($data === FALSE || $useCache === FALSE) {
      try {
        // Když není draft, ceny načítáme z databáze z ceníků
        $data = $draft ? self::computePrices($ctgId) : self::loadPrices($ctgId);
        $cache->save($data, $cacheId);
      }
      catch (Exception $e) {
        Utils::logger()->err($e->getMessage());
      }
    }
    return $data;
  }

  public static function getCtgLimits(): array {
    $limits = [];
    for ($idx = 1; $idx <= Pricelists::CTG_NUM; $idx++) {
      $limits[$idx] = Utils::getSetting('pricelists_limit_' . $idx);
    }
    return $limits;
  }

  public static function getFooterText() {
    return Utils::getSetting('pricelists_footer_text');
  }

  public static function getCtgTexts(): array
  {
    $texts = self::TEXTS_IDX;
    array_walk($texts, static function(&$value, $idx){
      return $value = Utils::getSetting($idx);
    });
    return $texts;
  }

  public static function getCtgTextsForm(): Pricelists_CtgTextsForm
  {
    return new Pricelists_CtgTextsForm();
  }

  public static function getAllPriceLists($includingMaster = TRUE): array {
    $table = new Table_Pricelists();
    $select = $table->select()->where('id IN (?)', self::getMapping());
    if ($includingMaster) {
      $select->orWhere('master = ?', TRUE);
    }
    return $select->query(Zend_DB::FETCH_ASSOC)->fetchAll();
  }


  public static function getMappingForm($wh = null): Pricelists_MappingForm
  {
    $form = new Pricelists_MappingForm(['wh' => $wh]);
    $form->setAction('/pricelists/mapping');
    return $form;
  }

  public static function getMapping(int $id = null, bool $draft = false): array
  {
    $ids = self::getAllPriceListIds();
    if ($id !== null) {
      if (array_key_exists($id, $ids)) {
        $key = ($draft ? 'price_list_draft_' : 'price_list_') . $ids[$id];
        return [$key => (int) Utils::getSetting(self::MAPPING_PREFIX . $key)];
      }

      return [];
    }

    $mapping = [];
    foreach ($ids as $idx) {
      $key = 'price_list_' . $idx;
      $mapping[$key] = (int) Utils::getSetting(self::MAPPING_PREFIX . $key);
      if ($draft) {
        $key = 'price_list_draft_' . $idx;
        $mapping[$key] = (int) Utils::getSetting(self::MAPPING_PREFIX . $key);
      }
    }
    return $mapping;
  }

  protected static function getCache(): Zend_Cache_Core
  {
    /** @var  Zend_Cache_Core $cache */
    $cache = NULL;
    try {
      $cache = Zend_Registry::get('cache');
    }
    catch (Zend_Exception $e) {
      Utils::logger()->err($e->getMessage());
    }
    return $cache;
  }

  protected static function getCacheId(bool $draft): string
  {
    return 'meduse_prices' . ($draft ? '_draft' : '_current');
  }
}