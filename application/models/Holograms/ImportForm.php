<?php

/**
 * Description of ImportForm
 *
 * @author Zdeněk
 */
class Holograms_ImportForm extends EasyBib_Form {
    
    public function init() {
        $this->setAction('/holograms/import');
        $this->setMethod(Zend_Form::METHOD_POST);

    	//kolekce
        $db = Zend_Registry::get('db');
        $rows = $db->fetchAll($db->select()->from('collections'));
        $element = new Zend_Form_Element_Select('collections');
        $element->setLabel('Kolekce');
        foreach ($rows as $row) {
            $element->addMultiOption($row['id'], $row['name']);
        }
        $this->addElement($element);       
        
        $element = new Zend_Form_Element_Textarea('serials');
        $element->setLabel('Sériová čísla');
        $element->setDescription('Jedno sériové číslo na řádek');
        $this->addElement($element);    
        
        // odesilaci button
        $element = new Zend_Form_Element_Submit('send');
        $element->setLabel('Importovat');
        $element->setAttrib('class', 'btn btn-primary');
        $this->addElement($element);    

                
        EasyBib_Form_Decorator::setFormDecorator(
                $this, EasyBib_Form_Decorator::BOOTSTRAP_MINIMAL, 'send'
        );
        }
}
