<?php

/**
 * Description of FilterForm
 *
 * @author Zdeněk
 */
class Holograms_FilterForm extends EasyBib_Form {
    
    public function init() {
    	
    	$this->setMethod(Zend_Form::METHOD_POST);
        $this->setAction('/holograms');
        
    	$db = Zend_Registry::get('db');

    	//kolekce
        $rows = $db->fetchAll($db->select()->from('collections'));
        $element = new Zend_Form_Element_MultiCheckbox('collection');
        $element->setLabel('Kolekce');
    	foreach($rows as $row){
            $element->addMultiOption($row['id'], " ".$row['name']);
    	}
    	$this->addElement($element);
        
        // CISLO OBJEDNAVKY
        $element = new Zend_Form_Element_Text('order_no');
    	$element->setLabel('Číslo objednávky');
    	$this->addElement($element);
    	
        // CISLO SOUCASTI
    	$element = new Zend_Form_Element_Text('parts_id');
    	$element->setLabel('ID součásti');
    	$this->addElement($element);

        //pouze nepřiřazené
        $checkBox = new Zend_Form_Element_Checkbox('unassigned');
        $checkBox->setLabel("Pouze nepřiřazené");
        $checkBox->setAttrib('class', 'clearfix');
        $this->addElement($checkBox);
        
        $element = new Zend_Form_Element_Button('send');
        $element->setLabel('Filtrovat');
        
        $this->addElement($element);      
        
        
                // add display group
        $this->addDisplayGroup(array('collection', 'order_no', 'parts_id'),
            'hologram_filter'    
        );
        $this->addDisplayGroup(array('unassigned'), 'hologram_checkbox');
        $this->addDisplayGroup(array('send'), 'hologram_send');
        
        EasyBib_Form_Decorator::setFormDecorator(
                $this, EasyBib_Form_Decorator::BOOTSTRAP_MINIMAL, 'send'
        );
        
        
    }
}
