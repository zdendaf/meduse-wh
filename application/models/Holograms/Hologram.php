<?php

/**
 * Description of Hologram
 * @author Zdeněk
 */
class Holograms_Hologram {

    private $_hologram;

    /**
     * Hologramy uložené v databázi z možností filtrace 
     * @param type $params parametry pro filtraci hologramů
     *                     [collection, unassigned, order_no, parts_id]
     * @return type array
     */
    public static function getAllHolograms($params = array()) {
        $db = Zend_Registry::get('db');        
        $where = array();       
        if (!empty($params['collection'])) {
            $where[] = '(c.id IN (' . implode(', ', $params['collection']) . ')'
            . (in_array('0', $params['collection'])? 'OR c.id IS NULL)' : ')');
        }
        if ($params['unassigned'] == '1') {
            $where[] = '(o.id IS NULL AND p.id IS NULL)';
        } else {
            $order_no = trim($params['order_no']); 
            if (!empty($order_no)) {
                $where[] = $db->quoteInto('o.no = ?', $order_no);
            }
            $part_id = trim($params['parts_id']); 
            if (!empty($part_id)) {
                $where[] = $db->quoteInto('p.id = ?', $part_id);
            }            
        }
        $query = 'SELECT h.id AS hologram_id, h.id_orders AS order_id, h.id_parts AS part_id, 
                    h.hologram, h.inserted, h.changed, c.abbr, 
                    o.title AS order_name, o.no AS order_no, p.name AS part_name 
                 FROM holograms AS h
                 LEFT JOIN collections AS c ON h.id_categories = c.id
                 LEFT JOIN orders AS o ON h.id_orders = o.id
                 LEFT JOIN parts AS p ON h.id_parts = p.id';
        
        if (!empty($where)) {
            $query .= "\nWHERE " . implode("\nAND ", $where);
        }
        $query .= "\nORDER BY h.hologram ASC";
        return $db->fetchAll($query);  
    }
    
    public function __construct($params = array()) {
        $this->_hologram = array();
        
        if  (isset($params['id']) && !isset($params['category'])) {
            $holograms = new Table_Holograms();
            if(!($row = $holograms->find($params['id'])->current())){
                throw new Exception(
                        'Neexistuje hologram s id = '. $params['id']. '.'
                );
            }
            $this->_hologram = $row;            
            return;
        }     
        
        if (isset($params['category']) && !isset($params['id']) && !isset($params['serial'])) {
            die('fuck');
            $holograms = new Table_Holograms();
            $select = $holograms->select()
                    ->where("id_categories = ?", $params['category'])
                    ->where("id_orders = NULL")->where("id_parts = NULL");
            $this->_hologram = $holograms->fetchRow($select);
            return;
        }
        
        if (isset($params['serial'])) {
            $holograms = new Table_Holograms();
            $id = $holograms->insert(array(
                'hologram' => $params['serial'],
                'id_categories' => $params['category'],
                'inserted' => date('Y-m-d h:i:s')
            ));
            $this->_hologram = $holograms->find($id)->current();
            return;
        }
    }

    public function getRow() {
        return $this->_hologram;
    }
  
    /** 
     * Je hologram volný nebo obsazený.
     * @return boolen
     */
    public function isFree() {
        return is_null($this->_hologram["id_orders"]) 
        && is_null($this->_hologram["id_parts"]);
    }
    
    /**
     * Vrátí ID kategorie hologramu ~ kolekce.
     * @return integer
     */
    public function getCategory() {
        return $this->_hologram["id_categories"];
    }
    
    /**
     * Přiřadí objednávku k hologramu
     * @param integer $id_order ID objednávky
     * @param integer $id_part ID produktu/části
     * @return boolean Úspěch/neúspěch
     */
    public function assignTo($id_order, $id_part) {       
        $part = new Parts_Part($id_part);
        if ($part->getCollection() != $this->getCategory()) {
            throw new Zend_Exception(
                    "Kategorie hologramu se neshoduje s kolekcí produktu."
            );
            return false;
        }
        $holograms = new Table_Holograms();
        $where = $holograms->getAdapter()
                ->quoteInto("id = ?", $this->_hologram["id"]);
        $holograms->update(array('id_orders' => $id_order,
                                 'id_parts' => $id_part), $where);
        return true;
    }
	
	public function getCode() {
		$tCollections = new Table_Collections();
		$category = $tCollections->find($this->_hologram['id_categories'])->current();
		return $category['abbr'] . ' ' . $this->_hologram['hologram'];
	}
}

