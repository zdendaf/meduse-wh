<?php

/**
 * @author Zdeněk Filipec <zdendaf@gmail.com>
 */
class EmailFactory_Queue {

  const SENDING_EMAILS_ENABLE = FALSE;

  const SESSION_NAME = 'queues';
  const DEFAULT_EXPIRATION = 86400; // 24 hodin
  const TYPE_INFO = 'informace';
  const TYPE_WARNING = 'upozornění';
  const TYPE_TODO = 'úkol';

  protected $id = NULL;
  protected $user_id = NULL;
  protected $created = NULL;
  protected $expiration = 0;
  protected $messages = array();
  protected $subject = '';
  protected $description = '';
  protected $recipients = array();
  protected $new_messages = 0;
  protected $email_type = Table_Emails::EMAIL_ORDER_PREPARE;

  public static function createSession() {
    $queues = new Zend_Session_Namespace(EmailFactory_Queue::SESSION_NAME);
    if (!isset($queues->data)) {
      $queues->data = array();
    }
    else {
      //EmailFactory_Queue::destroyExpiredQueues();
    }
  }

  public static function destroyExpiredQueues($send = TRUE) {
    $queues = new Zend_Session_Namespace(EmailFactory_Queue::SESSION_NAME);
    foreach ($queues->data as $queue) {
      if ($queue instanceof EmailFactory_Queue && $queue->isExpired()) {
        if ($send) {
          $queue->send(TRUE);
        }
        else {
          $queue->delete();
        }
      }
    }
  }

  /**
   * Vytvoří frontu
   */
  public static function createQueue($user_id = NULL) {
    return new EmailFactory_Queue($user_id);
  }

  public static function getQueue($queue_id) {
    $queues = new Zend_Session_Namespace(self::SESSION_NAME);
    if (isset($queues->data[$queue_id])) {
      $queue = $queues->data[$queue_id];
      return $queue;
    }
    else {
      throw new Exception('Queue ID = ' . $queue_id . ' not exists.');
    }
  }

  public function __construct($user_id = NULL) {
    if (is_null($user_id)) {
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $user_id = $authNamespace->user_id;
    }
    $this->init($user_id);
  }

  protected function init($user_id) {
    $this->addRecipients($user_id);
    $this->expiration = self::DEFAULT_EXPIRATION + time();
    $this->user_id = $user_id;
    $this->id = uniqid();
    $this->setSubject('Nová fronta ' . $this->id);
    $this->save();
  }

  public function save() {
    $this->new_messages = 0;
    $queues = new Zend_Session_Namespace(self::SESSION_NAME);
    $queues->data[$this->id] = $this;
    return $this;
  }

  public function delete() {
    $queues = new Zend_Session_Namespace(self::SESSION_NAME);
    unset($queues->data[$this->id]);
  }

  public function setSubject($subject) {
    $this->subject = $subject;
    return $this;
  }

  public function getSubject() {
    return $this->subject;
  }

  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  public function getDescription() {
    return $this->description;
  }

  public function setExpiration($seconds) {
    $this->expiration = $seconds;
    return $this;
  }

  public function getExpiration() {
    return $this->expiration;
  }

  public function isExpired() {
    return (time() > $this->expiration);
  }

  public function setEmailType($type) {
    $this->email_type = $type;
  }

  public function getEmailType() {
    return $this->email_type;
  }

  public function addRecipients($users) {
    if (!is_array($users)) {
      $users = array($users);
    }
    $tUser = new Table_Users();
    foreach ($users as $user_id) {
      $data = $tUser->getUser($user_id, FALSE);
      if (!$data) {
        throw new Exception('User ID = ' . $user_id . ' not exists');
      }
      $this->recipients[$user_id] = $data['email'];
    }
    return $this;
  }

  public function getRecipients() {
    return $this->recipients;
  }

  public function send($delete = TRUE) {
    if (self::SENDING_EMAILS_ENABLE) {
      $tEmails = new Table_Emails();
      $params = array(
        'subject' => $this->getSubject(),
        'body' => $this->getDescription() . $this->getMessages(0, TRUE, '<br>'),
        'recipients' => $this->getRecipients(),
      );
      $tEmails->sendEmail($this->email_type, $params);
    }
    if ($delete) {
      $this->delete();
    }
    else {
      return $this;
    }
  }

  public function add($text, $type = self::TYPE_INFO) {
    $message = new stdClass();
    $message->inserted = date('Y-m-d H:i:s');
    $message->type = $type;
    $message->text = $text;
    $this->messages[] = $message;
    $this->new_messages++;
    return $this;
  }

  public function getMessages($offset = 0, $asstring = FALSE, $delimiter = "\n") {
    if (!$asstring) {
      return array_slice($this->messages, $offset);
    }
    $messages = array();
    foreach (array_slice($this->messages, $offset) as $message) {
      $messages[] = '[' . $message->inserted . '] ' . $message->type . ': ' . $message->text;
    }
    return implode($delimiter, $messages);
  }

  public function getMessagesCount() {
    return count($this->messages);
  }

  public function getNewMessages($asstring = FALSE, $delimiter = "\n") {
    $offset = $this->getMessagesCount() - $this->new_messages;
    return $this->getMessages($offset, $asstring, $delimiter);
  }

  public function getId() {
    return $this->id;
  }

}
