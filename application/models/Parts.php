<?php

/**
 * @author Zdeněk Filipec <zdendaf@gmail.com>
 */
class Parts {

  /**
   * @param bool $price_flag
   * @param false $reservations
   * @param int $wh
   *
   * @return \PHPExcel
   * @throws \Exception
   */
  public static function getPartsXLSX($price_flag = TRUE, $reservations = FALSE, $thrownParts = FALSE, $wh = Table_PartsWarehouse::WH_TYPE): PHPExcel {
    if ($wh === Table_PartsWarehouse::WH_TYPE) {
      return self::getPartsXLSX_pipes($price_flag, $reservations, $thrownParts);
    }
    elseif ($wh === Table_TobaccoWarehouse::WH_TYPE) {
      return self::getPartsXLSX_tobacco($price_flag);
    }
    else {
      throw new Exception('Unknown WH type');
    }
  }

  protected static function getPartsXLSX_tobacco($price_flag = TRUE): PHPExcel {

    $wh = Table_TobaccoWarehouse::WH_TYPE;
    $tParts = new Table_Parts();
    $parts = $tParts->getPartsForExport($wh);
    $tPartsCtg = new Table_PartsCtg();

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator('Medite - Warehouse');
    $objPHPExcel->getProperties()->setLastModifiedBy('Medite - Warehouse');
    $objPHPExcel->getProperties()->setTitle('EXPORT SKLADU SOUČÁSTÍ K DATU ' . date('d.m.Y'));
    $objPHPExcel->getProperties()->setSubject('EXPORT SKLADU SOUČÁSTÍ K DATU ' . date('d.m.Y'));
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setTitle('Simple');

    $total_price = 0;

    $objPHPExcel->getActiveSheet()
      ->setCellValueByColumnAndRow(0, 1, 'EXPORT SKLADU SOUČÁSTÍ K DATU '
        . date('d.m.Y') . ' (celkový stav)');
    $row = 1;
    $last_ctg = NULL;

    foreach ($parts as $part) {
      $part_obj = new Parts_Part($part['id']);
      if ($last_ctg === null || $last_ctg != $part['id_parts_ctg']) {
        $row += 2;
        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow(0, $row, "KATEGORIE: ".$tPartsCtg->getCategoryName($part['id_parts_ctg']));
        $parentCtg = $tPartsCtg->getParentCategory($part['id_parts_ctg']);
        $unit = ($part['id_parts_ctg'] == Table_TobaccoWarehouse::CTG_MATERIALS || $parentCtg == Table_TobaccoWarehouse::CTG_MATERIALS) ? 'kg' : 'ks';
        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow($col = 0, ++$row, "ID")
          ->setCellValueByColumnAndRow(++$col, $row, "Skladem [" . $unit . "]");
        if ($price_flag) {
          $objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(++$col, $row, "Cena [Kč/" . $unit . "]")
            ->setCellValueByColumnAndRow(++$col, $row, "Cena");
        }
        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow(++$col, $row, "Název");
      }
      $price = $part_obj->getPrice();
      $partAmount = ($part['measure'] == Tobacco_Parts_Part::WH_MEASURE_WEIGHT) ? $part['amount'] * 0.001 :  $part['amount'];
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow($col = 0, ++$row, $part['id'])
        ->setCellValueByColumnAndRow(++$col, $row, $partAmount);
      if ($price_flag) {
        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow(++$col, $row, $price)
          ->setCellValueByColumnAndRow(++$col, $row, $partAmount * $price);
      }
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(++$col, $row, $part['name']);
      $total_price += $partAmount*$price;
      $last_ctg = $part['id_parts_ctg'];
    }

    if ($price_flag) {
      $row += 2;
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(0, $row, "Hodnota skladu: ");
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(3, $row, $total_price);
    }


    $objPHPExcel->getActiveSheet()->getStyle('C1:D' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.00');
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle('B' . $row)
      ->getNumberFormat()->setFormatCode('# ##0.00');

    return $objPHPExcel;
  }

  protected static function getPartsXLSX_pipes($price_flag = TRUE, $reservations = FALSE, $thrownParts = FALSE) {

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator('Meduse Design - Warehouse');
    $objPHPExcel->getProperties()->setLastModifiedBy('Meduse Design - Warehouse');
    if ($thrownParts) {
      $objPHPExcel->getProperties()->setTitle('EXPORT SKLADU VYŘAZENÝCH SOUČÁSTÍ K DATU ' . date('d.m.Y'));
      $objPHPExcel->getProperties()->setSubject('EXPORT SKLADU VYŘAZENÝCH SOUČÁSTÍ K DATU ' . date('d.m.Y'));
    }
    else {
      $objPHPExcel->getProperties()->setTitle('EXPORT SKLADU SOUČÁSTÍ K DATU ' . date('d.m.Y'));
      $objPHPExcel->getProperties()->setSubject('EXPORT SKLADU SOUČÁSTÍ K DATU ' . date('d.m.Y'));
    }
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setTitle('Simple');

    $tReservations = NULL;
    if ($reservations) {
      $tReservations = new Table_OrdersProductsParts();
    }

    $total_price = 0;
    $total_priceOp = 0;
    if ($thrownParts) {
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(0, 1, 'EXPORT SKLADU VYŘAZENÝCH SOUČÁSTÍ K DATU '
          . date('d.m.Y'));
    }
    else {
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(0, 1, 'EXPORT SKLADU SOUČÁSTÍ K DATU '
          . date('d.m.Y') . ' (celkový stav)');
    }
    $row = 1;
    $last_ctg = NULL;

    $tParts = new Table_Parts();
    $tPartsCtg = new Table_PartsCtg();

    foreach ($tParts->getPartsForExport(Table_PartsWarehouse::WH_TYPE, $thrownParts) as $part) {
      $part_obj = new Parts_Part($part['id']);
      if ($last_ctg === null || $last_ctg != $part['id_parts_ctg']) {
        $row += 2;
        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow(0, $row, "KATEGORIE: ".$tPartsCtg->getCategoryName($part['id_parts_ctg']));
        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow($col = 0, ++$row, "ID")
          ->setCellValueByColumnAndRow(++$col, $row, "Skladem [ks]");
        if ($price_flag) {
          $objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(++$col, $row, "Cena [Kč/ks]")
            ->setCellValueByColumnAndRow(++$col, $row, "Meduse operace [Kč/ks]")
            ->setCellValueByColumnAndRow(++$col, $row, "Cena")
            ->setCellValueByColumnAndRow(++$col, $row, "Meduse operace");
        }
        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow(++$col, $row, "Název");
      }
      $price = $part_obj->getPrice() - $part_obj->getOwnOperationSum();
      $priceOp = $part_obj->getOwnOperationSum();
      if ($tReservations !== NULL) {
        $partAmount = $part['amount'] + $tReservations->getTotalAmount($part['id']);
      }
      else {
        $partAmount = $part['amount'];
      }
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow($col = 0, ++$row, $part['id'])
        ->setCellValueByColumnAndRow(++$col, $row, $partAmount);
      if ($price_flag) {
        $objPHPExcel->getActiveSheet()
          ->setCellValueByColumnAndRow(++$col, $row, $price)
          ->setCellValueByColumnAndRow(++$col, $row, $priceOp)
          ->setCellValueByColumnAndRow(++$col, $row, $partAmount*$price)
          ->setCellValueByColumnAndRow(++$col, $row, $partAmount*$priceOp);
      }
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(++$col, $row, $part['name']);
      $total_price += $partAmount*$price;
      $total_priceOp += $partAmount*$priceOp;
      $last_ctg = $part['id_parts_ctg'];
    }

    if ($price_flag) {
      $row += 2;
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(0, $row, "Hodnota skladu: ");
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(3, $row, $total_price);
      $row += 1;
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(0, $row, "Součet operací: ");
      $objPHPExcel->getActiveSheet()
        ->setCellValueByColumnAndRow(3, $row, $total_priceOp);
    }
    $objPHPExcel->getActiveSheet()->getStyle('C1:D' . $row)
      ->getNumberFormat()->setFormatCode('#,##0.00');
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle('B' . $row)
      ->getNumberFormat()->setFormatCode('#0.00');

    return $objPHPExcel;
  }




  public static function getPartsCSV($price_flag = TRUE, $reservations = FALSE, $wh = Table_PartsWarehouse::WH_TYPE) {
    if ($wh === Table_PartsWarehouse::WH_TYPE) {
      return self::getPartsCSV_pipes($price_flag, $reservations);
    }
    elseif ($wh === Table_TobaccoWarehouse::WH_TYPE) {
      return self::getPartsCSV_tobacco();
    }
    else {
      throw new Exception('Unknown WH type');
    }
  }

  protected static function getPartsCSV_tobacco() {

    $tParts = new Table_Parts();
    $parts = $tParts->getPartsForExport(Table_TobaccoWarehouse::WH_TYPE);
    $tPartsCtg = new Table_PartsCtg();

    // Cena soucasti
    $tReservations = null;
    if(Zend_Registry::get('acl')->isAllowed('parts/detail/price')) {
      $price_flag = true;
    } else {
      $price_flag = false;
    }
    $total_price = 0;

    $text_delimiter = '"';
    $field_delimiter = ';';
    $line_delimiter = "\n";

    $export_string  = ($price_flag ? $text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.$text_delimiter.$field_delimiter : '').$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.'EXPORT SKLADU SOUČÁSTÍ K DATU '.date('d.m.Y').$text_delimiter.$field_delimiter.$line_delimiter;
    $export_string .= ($price_flag ? $text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.$text_delimiter.$field_delimiter : '').$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.'(celkový stav)'.$text_delimiter.$field_delimiter.$line_delimiter.$line_delimiter;

    $last_ctg = null;
    foreach($parts as $part){

      $part_obj = new Tobacco_Parts_Part($part['id']);
      $ctg = $part_obj->getParentCtg();
      $unit = ($ctg == Table_TobaccoWarehouse::CTG_MATERIALS) ? 'kg' : 'ks';
      if($last_ctg === null || $last_ctg != $part['id_parts_ctg']){
        $export_string .= $line_delimiter.
          $text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter."KATEGORIE: ".$tPartsCtg->getCategoryName($part['id_parts_ctg']).$text_delimiter.$field_delimiter.
          $line_delimiter;
        $export_string .=
          $text_delimiter."ID".$text_delimiter.$field_delimiter.
          $text_delimiter."Skladem [".$unit."]".$text_delimiter.$field_delimiter.
          ($price_flag ? $text_delimiter."Cena [Kč/".$unit."]".$text_delimiter.$field_delimiter : '').
          ($price_flag ? $text_delimiter."Cena".$text_delimiter.$field_delimiter : '').
          $text_delimiter."Název".$text_delimiter.$field_delimiter.
          $line_delimiter;
      }

      $price = $part_obj->getPrice();

      if($tReservations !== null){
        $partAmount = $part['amount'] + $tReservations->getTotalAmount($part['id']);
      } else {
        $partAmount = $part['amount'];
      }
      if ($ctg == Table_TobaccoWarehouse::CTG_MATERIALS) {
        $partAmount = $partAmount * 0.001;
      }

      $export_string .=
        $text_delimiter.$part['id'].$text_delimiter.$field_delimiter;
      $export_string .= ($ctg == Table_TobaccoWarehouse::CTG_MATERIALS) ?
        $text_delimiter.number_format($partAmount, 2, ',', ' ').$text_delimiter.$field_delimiter
        : $text_delimiter.$partAmount.$text_delimiter.$field_delimiter;
      $export_string .=
        ($price_flag ? $text_delimiter.number_format($price, 2, ',', ' ').$text_delimiter.$field_delimiter : '').
        ($price_flag ? $text_delimiter.number_format($partAmount*$price, 2, ',', ' ').$text_delimiter.$field_delimiter : '').
        $text_delimiter.$part['name'].$text_delimiter.$field_delimiter.
        $line_delimiter;

      $total_price += $partAmount*$price;

      $last_ctg = $part['id_parts_ctg'];
    }

    $export_string .= ($price_flag ? $line_delimiter.$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter."Hodnota skladu: ".$text_delimiter.$field_delimiter.$text_delimiter.number_format($total_price, 2, ',', ' ').$text_delimiter.$field_delimiter : '');

    return $export_string;
  }

  public static function getPartsCSV_pipes($price_flag = TRUE, $reservations = FALSE): string {

    $tParts = new Table_Parts();
		$tPartsCtg = new Table_PartsCtg();

		$tReservations = NULL;
    if($reservations){
      $tReservations = new Table_OrdersProductsParts();
    }
    
		$total_price = 0;
		$total_priceOp = 0;

		$text_delimiter = '"';
		$field_delimiter = ';';
		$line_delimiter = "\n";

		$export_string  = ($price_flag ?
      $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . $text_delimiter . $field_delimiter : '')
      . $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . 'EXPORT SKLADU SOUČÁSTÍ K DATU ' . date('d.m.Y')
      . $text_delimiter . $field_delimiter . $line_delimiter;
		$export_string .= ($price_flag ?
      $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . $text_delimiter . $field_delimiter : '')
      . $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . '(celkový stav)' . $text_delimiter . $field_delimiter
      . $line_delimiter . $line_delimiter;

		$last_ctg = NULL;

		foreach($tParts->getPartsForExport() as $part){

			$part_obj = new Parts_Part($part['id']);

			if($last_ctg === NULL || $last_ctg != $part['id_parts_ctg']){
				$export_string .= $line_delimiter
          . $text_delimiter . $text_delimiter . $field_delimiter
          . $text_delimiter . $text_delimiter . $field_delimiter
          . $text_delimiter . $text_delimiter . $field_delimiter
          . $text_delimiter . "KATEGORIE: "
          . $tPartsCtg->getCategoryName($part['id_parts_ctg'])
          . $text_delimiter . $field_delimiter . $line_delimiter;
				$export_string .=
					$text_delimiter . "ID" . $text_delimiter . $field_delimiter
          . $text_delimiter . "Skladem [ks]"
            . $text_delimiter . $field_delimiter
          . ($price_flag ? $text_delimiter . "Cena [Kč/ks]"
            . $text_delimiter . $field_delimiter : '')
          . ($price_flag ? $text_delimiter . "Meduse operace [Kč/ks]"
            . $text_delimiter . $field_delimiter : '')
          . ($price_flag ? $text_delimiter . "Cena"
            . $text_delimiter . $field_delimiter : '')
          . ($price_flag ? $text_delimiter . "Meduse operace"
            . $text_delimiter . $field_delimiter : '')
          . $text_delimiter . "Název" . $text_delimiter . $field_delimiter
          . $line_delimiter;
			}

			$price = $part_obj->getPrice() - $part_obj->getOwnOperationSum();
      $priceOp = $part_obj->getOwnOperationSum();

			if($tReservations !== NULL){
				$partAmount = $part['amount'] + $tReservations->getTotalAmount($part['id']);
			} else {
				$partAmount = $part['amount'];
			}


			$export_string .=
				$text_delimiter . $part['id'] . $text_delimiter . $field_delimiter
        . $text_delimiter . $partAmount . $text_delimiter . $field_delimiter
        . ($price_flag ? $text_delimiter . number_format($price, 2, ',', ' ')
          . $text_delimiter . $field_delimiter : '')
        . ($price_flag ? $text_delimiter . number_format($priceOp, 2, ',', ' ')
          . $text_delimiter . $field_delimiter : '')
        . ($price_flag ? $text_delimiter . number_format($partAmount*$price, 2, ',', ' ')
         . $text_delimiter . $field_delimiter : '')
        . ($price_flag ? $text_delimiter . number_format($partAmount*$priceOp, 2, ',', ' ')
          . $text_delimiter . $field_delimiter : '')
        . $text_delimiter . $part['name'] . $text_delimiter . $field_delimiter
        . $line_delimiter;

			$total_price += $partAmount*$price;
			$total_priceOp += $partAmount*$priceOp;
			$last_ctg = $part['id_parts_ctg'];
		}

		$export_string .= ($price_flag ? $line_delimiter
      . $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . "Hodnota skladu: " . $text_delimiter . $field_delimiter
      . $text_delimiter . number_format($total_price, 2, ',', ' ')
      . $text_delimiter . $field_delimiter : '');
    $export_string .= ($price_flag ? $line_delimiter
      . $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . "Součet operací: " . $text_delimiter . $field_delimiter
      . $text_delimiter . number_format($total_priceOp, 2, ',', ' ')
      . $text_delimiter . $field_delimiter : '');

    return $export_string;
  }

  public static function getTrashData($id_wh_type = Parts_Part::WH_TYPE_PIPES): array {
    $tParts = new Table_Parts();
    return $tParts->getTrashData($id_wh_type);
  }

  public static function findBySKU($sku) {
    $table = new Table_Parts();
    $select = $table->select()->from('parts', array('id', 'id_wh_type'))->where('sku LIKE ?', $sku);
    try {
      if ($row = $select->query(Zend_Db::FETCH_OBJ)->fetch()) {
        if ($row->id_wh_type == Table_PartsWarehouse::WH_TYPE) {
          return new Parts_Part($row->id);
        }
        else {
          return new Tobacco_Parts_Part($row->id);
        }
      }
      else {
        return NULL;
      }
    }
    catch (Exception $e) {
      $log = new Zend_Log();
      $log->err($e->getMessage());
      return NULL;
    }
  }

  public static function findByProducer($id_producers): array {
    $table = new Table_Parts();
    return $table->findByProducer($id_producers);
  }

  public static function getProducts(array $options = []): array {

    $wh = $options['wh'] ?? Table_PartsWarehouse::WH_TYPE;

    $filterCollections = $options['filterCollections'] ?? [];
    $filterCategories = $options['filterCategories'] ?? [];
    $withCollections = !empty($filterCollections) || (($options['withCollections'] ?? FALSE));

    $products = [];

    $tParts = new Table_Parts();
    $select = $tParts->getProducts($wh, TRUE);
    $select->setIntegrityCheck(FALSE);
    $select->join(['pc' => 'parts_ctg'], 'pc.id = p.id_parts_ctg', [
      'ctg_id' => 'id',
      'ctg_name' => new Zend_Db_Expr('ifnull(pc.name_eng, pc.name)'),
      'ctg_description' => 'description',
      'ctg_order' => 'ctg_order',
    ]);
    $select->joinLeft(['pc2' => 'parts_ctg'], 'pc.parent_ctg = pc2.id', [
      'ctg_parent_id' => 'id',
      'ctg_parent_name' => 'name',
      'ctg_parent_order' => 'ctg_order',
    ]);

    if ($filterCategories) {
      $select->where('p.id_parts_ctg IN (?)', $filterCategories);
    }

    if ($result = $select->query(Zend_Db::FETCH_ASSOC)) {
      while ($row = $result->fetch()) {
        $products[$row['id']] = $row;
      }
    }

    if ($products && $wh === Table_PartsWarehouse::WH_TYPE && $withCollections) {
      $tpc = new Table_PartsCollections();
      $select = $tpc->select()
        ->setIntegrityCheck(FALSE)
        ->from(['pc' => 'parts_collections'], ['part_id' => 'id_parts'])
        ->join(['c' => 'collections'], 'c.id = pc.id_collections', [
          'collection_id' => 'id',
          'collection_name' => 'name',
          'collection_abbr' => 'abbr',
        ]);

      if ($result = $select->query(Zend_Db::FETCH_ASSOC)) {
        while ($row = $result->fetch()) {
          if (key_exists($row['part_id'], $products)) {
            if (!key_exists('collections', $products[$row['part_id']])) {
              $products[$row['part_id']]['collections'] = [];
            }
            if ($filterCollections && !in_array($row['collection_id'], $filterCollections)) {
              continue;
            }
            $products[$row['part_id']]['collections'][] = $row;
          }
        }
      }
    }

    if ($filterCollections) {
      $products = array_filter($products, function($item){
        return isset($item['collections']) && !empty($item['collections']);
      });
    }

    return $products;
  }

  public static function getParts($wh = Table_PartsWarehouse::WH_TYPE, $asSelect = FALSE) {
    $tParts = new Table_Parts();
    return $tParts->getParts($wh, $asSelect);
  }

  /**
   * Vratí asociativni pole s kolekcemi.
   *
   * @return array
   */
  public static function getCollections(): array {
    $tCollections = new Table_Collections();
    $collections = [];
    $rows = $tCollections->fetchAll()->toArray();
    while ($row = array_shift($rows)) {
      $collections[$row['id']] = $row;
    }
    return $collections;
  }

  /**
   * Vratí asociativni pole s kategoriemi.
   *
   * @return array
   */
  public static function getCategories(): array {
    $table = new Table_PartsCtg();
    return $table->getCategories();
  }

  /**
   * Nastaví výchozí hodnotu krycích nákladů všem produktům.
   *
   * @param $wh int Typ WH
   *
   * @return int Počet změněných produktů.
   */
  public static function setDefaultCoverCost(int $wh = Table_PartsWarehouse::WH_TYPE): int {
    $table = new Table_Parts();
    return $table->update(['cover_cost' => NULL], [
      'cover_cost IS NOT NULL',
      'product = ' . $table->getAdapter()->quote('y'),
      'id_wh_type = ' . $table->getAdapter()->quote($wh)
    ]);
  }
}
