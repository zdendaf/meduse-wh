<?php
class BugReportForm extends Zend_Form {

	public function __construct ($options = null){

        parent::__construct($options);

        $this->setAction('/index/report-bug');
        $this->setMethod('post');
        $this->setAttrib('id', 'bug_report_form');

        $title = new Zend_Form_Element_Text('bug_title');
        $title->setLabel('Shrnutí problému (krátce)');
        $title->setAttrib('class', 'input-block-level');
        $this->addElement($title);

        $desc = new Zend_Form_Element_Textarea('bug_desc');
        $desc->setLabel('Popis problému (jak k němu došlo, jak se dá zopakovat)');
        $desc->setAttrib('class', 'input-block-level');
        $desc->setAttrib('rows', '5');
        $desc->setAttrib('cols', '40');
        $this->addElement($desc);

        $element = new Zend_Form_Element_Hidden('bug_page');
        $element->setValue('REQUEST_URI: ' . $_SERVER['REQUEST_URI'] . ' , QUERY_STRING:' . $_SERVER['QUERY_STRING']);
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('bug_browser');
        $element->setValue($_SERVER['HTTP_USER_AGENT']);
        $this->addElement($element);

        $authNamespace = new Zend_Session_Namespace('Zend_Auth');
        $element = new Zend_Form_Element_Hidden('bug_username');
        $element->setValue($authNamespace->user_name_full);
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('bug_usr_role');
        $element->setValue(implode(', ', $authNamespace->user_role));
        $this->addElement($element);
    }
}