<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Departments_BudgetForm extends Meduse_FormBootstrap {
    
    public function init() {
      
      $this->setAttrib('class', 'form');
      
      $element = new Meduse_Form_Element_DatePicker('from');
      $element->setRequired(TRUE);
      $element->setLabel('Datum od');
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_DatePicker('to');
      $element->setRequired(TRUE);
      $element->setLabel('Datum do');
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Currency('amount');
      $element->setRequired(TRUE);
      $element->setLabel('Částka');
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Submit('Uložit');
      $this->addElement($element);
      
      parent::init();
    }

    /**
     * @throws Zend_Date_Exception
     */
    public function populate(array $values) {
      parent::populate($values);
      
      Zend_Date::setOptions(array('format_type' => 'php'));
      
      if (isset($values['from'])) {
        $from = new Zend_Date($values['from'], 'Y-m-d');
        $this->getElement('from')->setValue($from->toString('d.m.Y'));
      }
      
      if (isset($values['to'])) {
        $to = new Zend_Date($values['to'], 'Y-m-d');
        $this->getElement('to')->setValue($to->toString('d.m.Y'));
      }
            
    }
  }

  