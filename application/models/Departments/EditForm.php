<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Departments_EditForm extends Meduse_FormBootstrap {

    public function init() {
      $this->setAttrib('class', 'form');

      $element = new Meduse_Form_Element_Text('name');
      $element->setRequired(TRUE);
      $element->setLabel('Název oddělení');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('id_wh_type');
      $element->setRequired(TRUE);
      $element->setLabel('Firma');
      $element->addMultiOptions(array(
        '-- zvol firmu --',
        Parts_Part::WH_TYPE_PIPES => 'Meduse Design',
        Parts_Part::WH_TYPE_TOBACCO => 'Medite',
      ));
      $this->addElement($element);

      $users = array();
      $users[] = '-- zvol uživatele --';
      $tUsers = new Table_Users();
      foreach ($tUsers->getList() as $user) {
        $users[$user['id']] = $user['name_full'];
      }
      $element = new Meduse_Form_Element_Select('id_users');
      $element->setRequired(TRUE);
      $element->setLabel('Vedoucí');
      $element->setMultiOptions($users);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('Uložit');
      $this->addElement($element);

      parent::init();
    }
  }

