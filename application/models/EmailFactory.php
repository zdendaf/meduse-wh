<?php

  class EmailFactory {

    const EMAIL_BOSS = 'stary@meduse-experience.com';
    const EMAIL_MANAGER = 'info@meduse-experience.com';
    const EMAIL_WAREHOUSE = 'kubacak@meduse-experience.com';
    const EMAIL_TOBACCO_WAREHOUSE = 'hybler@medite-tobacco.com';
    const EMAIL_TOBACCO_OPERATOR = 'dlapkova@medite-tobacco.com';
    const EMAIL_TOBACCO_SALES = 'info@medite-tobacco.com';
    const EMAIL_SALES = 'novackova@meduse-experience.com';
    const EMAIL_OFFICE = 'pavlik@meduse-experience.com';
    const EMAIL_INFO = 'info@meduse-experience.com';
    const EMAIL_ACCOUNTANT = 'drabikova@meduse-experience.com';

    const USER_ID_WAREHOUSE = 4; // kubacakr

    public static $subjectPrefix = '';
    public static $url = 'https://warehouse.medusepipes.com/';
    public static $devUrl = 'http://dev.warehouse.medusepipes.com/';
    public static $header = '<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		</head>
		<body style="font-family:Arial,Helvetica,sans-serif;">';
    public static $footer = "<br /><hr style='border: 0; border-top: 1px solid silver; margin-bottom: 2px; margin-top: 10px;'><span style='color: gray; font-size: 13px;'>Tato zpráva má pouze informativní charakter a byla automaticky vygenerována skladovým informačním systémem <span><img src='http://warehouse.medusepipes.com/public/images/favicon.ico' style='vertical-align:text-bottom;'/> <span style='font-size: 13px;'>MEDUSE WAREHOUSE</span></span>.<br />Prosím neodpovídejte na ni.<br /></div></html>";

    public static function orderConfirmEmail($orderID) {

      $order = new Orders_Order($orderID);

      // podle toho, zda-li ma ci nema objednavka produkty, posleme email do skladu
      // nebo primo obchodnikum Medusy nebo Medite
      if (!$order->isService()) {
        $to = $order->getWhType() == Parts_Part::WH_TYPE_PIPES ? self::EMAIL_WAREHOUSE : self::EMAIL_TOBACCO_WAREHOUSE . ', ' . self::EMAIL_TOBACCO_SALES;
      } else {
        $to = $order->getWhType() == Parts_Part::WH_TYPE_PIPES ? self::EMAIL_SALES : self::EMAIL_TOBACCO_SALES;
      }
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $to;
      $email->type = 'confirmEmail';
      $email->subject = EmailFactory::$subjectPrefix . "Potvrzení objednávky č. " . $order->getNO();
      $url = (Zend_Registry::get('version') == 'TEST' ? EmailFactory::$devUrl : EmailFactory::$url) . ($order->getWhType() == Parts_Part::WH_TYPE_PIPES ? 'orders' : 'tobacco-orders' ) . '/detail/id/' . $order->getID();
      $email->body = EmailFactory::$header
        . "Objedávka číslo <strong>" . $order->getNO() . " " . $order->getTitle() . "</strong> byla potvrzena. "
        . "Nyní je možné jí otevřít.<br/>Odkaz: <a href='" . $url . "'>Detail objednávky</a>"
        . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function orderProductionEmail($productionID) {
      $productions = new Table_Productions;
      $details = $productions->getDetails($productionID);
      $parts = $productions->getParts($productionID);
      $table = "<table><tr><th>Součást</th><th>Počet</th></tr>";
      foreach ($parts as $part) {
        $table .= "<tr> <td>" . $part['id_parts'] . " " . $part['name'] . "</td> <td>" . $part['amount'] . " ks</td> </tr>";
      }
      $table .= "</table>";
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_WAREHOUSE;
      $email->type = 'productionEmail';
      $email->subject = EmailFactory::$subjectPrefix . "Nová výroba " . $details['name'];
      $email->body = EmailFactory::$header . "Výroba <strong>" . $details['name'] . "</strong> byla vložena do systému." . $table . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function newLocationEmail($ip, $hash) {
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $tUsersIp = new Table_UsersIp();
      $rUser = $tUsersIp->getUserByHash($hash);
      if ($rUser === null) {
        throw new Exception('User not found!');
      }
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->type = 'newLocationEmail';
      $email->subject = EmailFactory::$subjectPrefix . 'Přihlášení z neznámého místa';

      if (Zend_Registry::get('version') == 'TEST') {
        EmailFactory::$url = EmailFactory::$devUrl;
      }

      if (in_array('manager', $authNamespace->user_role) || in_array('admin', $authNamespace->user_role)) {
        $email->to = $authNamespace->user_email;
        $email->body = EmailFactory::$header . "Na vašem účtu '" . $rUser->username . "' došlo k přihlášení z neznámé IP adresy: <br /><br /><strong>" . $ip . "</strong><br /><br />pro povolení výše uvedené adresy klikněte na tento odkaz:<br /><a href='" . EmailFactory::$url . "login?allow=$hash'>" . EmailFactory::$url . "login?allow=$hash</a><br/><br/>zjištění vlastní adresy: <a href='http://www.whatismyip.com/'>http://www.whatismyip.com/</a>" . EmailFactory::$footer;
      } else {
        $email->to = self::EMAIL_MANAGER;
        $email->body = EmailFactory::$header . "Na účtu '" . $rUser->username . "' došlo k přihlášení z neznámé IP adresy: <br /><br /><strong>" . $ip . "</strong><br /><br />Pro povolení výše uvedené adresy klikněte na tento odkaz:<br /><a href='" . EmailFactory::$url . "login?allow=$hash'>" . EmailFactory::$url . "login?allow=$hash</a>" . EmailFactory::$footer;
      }
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function orderOpenEmail($orderId) {
      $order = new Orders_Order($orderId);
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $order->getWhType() == Parts_Part::WH_TYPE_PIPES ? self::EMAIL_WAREHOUSE : self::EMAIL_TOBACCO_WAREHOUSE . ', ' . self::EMAIL_TOBACCO_SALES;
      $email->type = 'orderOpenNoticeEmail';
      $email->subject = EmailFactory::$subjectPrefix . 'Objednávka č.' . $order->getNO() . ' "' . $order->getTitle() . '" byla otevřena.';
      $url = (Zend_Registry::get('version') == 'TEST' ? EmailFactory::$devUrl : EmailFactory::$url) . ($order->getWhType() == Parts_Part::WH_TYPE_PIPES ? 'orders' : 'tobacco-orders' ) . '/detail/id/' . $order->getID();
      $email->body = EmailFactory::$header
        . "Objedávka číslo <strong>" . $order->getNO() . " " . $order->getTitle() . "</strong> byla otevřena. "
        . "Nyní je možné jí začít připravovat.<br/>Odkaz: <a href='" . $url . "'>Detail objednávky</a>"
        . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function tobaccoOrderOpenEmail($orderId) {
      $order = new Orders_Order($orderId);
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = implode(',', [self::EMAIL_TOBACCO_WAREHOUSE, self::EMAIL_TOBACCO_OPERATOR]);
      $email->type = 'tobaccoOrderOpenNoticeEmail';
      $email->subject = EmailFactory::$subjectPrefix . 'Objednávka č.' . $order->getNO() . ' "' . $order->getTitle() . '" byla otevřena.';
      $url = (Zend_Registry::get('version') == 'TEST' ? EmailFactory::$devUrl : EmailFactory::$url) . ($order->getWhType() == Parts_Part::WH_TYPE_PIPES ? 'orders' : 'tobacco-orders' ) . '/detail/id/' . $order->getID();
      $email->body = EmailFactory::$header
        . "Objedávka číslo <strong>" . $order->getNO() . " " . $order->getTitle() . "</strong> byla otevřena. "
        . "Nyní je možné jí začít připravovat.<br/>Odkaz: <a href='" . $url . "'>Detail objednávky</a>"
        . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function tobaccoOrderIsReadyEmail($orderId) {
      $order = new Orders_Order($orderId);
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_TOBACCO_SALES;
      $email->type = 'tobaccoOrderOpenIsReadyEmail';
      $email->subject = EmailFactory::$subjectPrefix . 'Objednávka č.' . $order->getNO()
        . ' "' . $order->getTitle() . '" je připravena k expedici.';
      $url = (Zend_Registry::get('version') == 'TEST' ? EmailFactory::$devUrl : EmailFactory::$url) . ($order->getWhType() == Parts_Part::WH_TYPE_PIPES ? 'orders' : 'tobacco-orders' ) . '/detail/id/' . $order->getID();
      $email->body = EmailFactory::$header
        . "Objedávka číslo <strong>" . $order->getNO() . " " . $order->getTitle() . "</strong> je připravena a je možno ji expedovat. "
        . "<br/>Odkaz: <a href='" . $url . "'>Detail objednávky</a>"
        . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function tobaccoOrderExpeditionEmail($orderID) {
      $order = new Orders_Order($orderID);
      $tracking_no = $order->getTrackingNo();
      if (empty($tracking_no)) {
        $tracking_no = '-';
      }
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_TOBACCO_SALES;
      $email->type = 'tobaccoOrderExpeditionNoticeEmail';
      $email->subject = EmailFactory::$subjectPrefix . "Expedice objednávky č. " . $order->getNO();
      $db = Zend_Registry::get('db');
      $country = $db->fetchOne('SELECT name FROM c_country WHERE code = ' . $db->quote($order->getCountry()));
      $url = (Zend_Registry::get('version') == 'TEST' ? EmailFactory::$devUrl : EmailFactory::$url) . ($order->getWhType() == Parts_Part::WH_TYPE_PIPES ? 'orders' : 'tobacco-orders' ) . '/detail/id/' . $order->getID();
      $email->body = EmailFactory::$header
        . "Objednávka č. " . $order->getNO() . ' "' . $order->getTitle() . '"'
        . (!empty($tracking_no) ? ', Tr. No.: ' . $tracking_no : '')
        . " byla vyexpedována do " . $country
        . ", zašlete zákazníkovi upozornění."
        . "<br/>Odkaz: <a href='" . $url . "'>Detail objednávky</a>"
        . EmailFactory::$footer;
      return $email;
    }

    public static function orderExpeditionEmail($orderID) {
      $order = new Orders_Order($orderID);
      $tracking_no = $order->getTrackingNo();
      if (empty($tracking_no)) {
        $tracking_no = '-';
      }
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = implode(', ', [self::EMAIL_WAREHOUSE, self::EMAIL_SALES, self::EMAIL_OFFICE, self::EMAIL_MANAGER]);
      $email->type = 'orderExpeditionNoticeEmail';
      $email->subject = EmailFactory::$subjectPrefix . "Expedice objednávky č. " . $order->getNO();
      $db = Zend_Registry::get('db');
      $country = $db->fetchOne('SELECT name FROM c_country WHERE code =' . $db->quote($order->getCountry()));
      $url = (Zend_Registry::get('version') == 'TEST' ? EmailFactory::$devUrl : EmailFactory::$url) . ($order->getWhType() == Parts_Part::WH_TYPE_PIPES ? 'orders' : 'tobacco-orders' ) . '/detail/id/' . $order->getID();
      $email->body = EmailFactory::$header
        . "Objednávka č. " . $order->getNO() . ' "' . $order->getTitle() . '"'
        . (!empty($tracking_no) ? ', Tr. No.: ' . $tracking_no : '')
        . " byla vyexpedována do " . $country
        . ", zašlete zákazníkovi upozornění."
        . "<br/>Odkaz: <a href='" . $url . "'>Detail objednávky</a>"
        . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function orderExpeditionEmailExWorks($orderID) {
      $order = new Orders_Order($orderID);
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = implode(', ', [self::EMAIL_WAREHOUSE, self::EMAIL_SALES, self::EMAIL_OFFICE, self::EMAIL_MANAGER]);
      $email->type = 'orderExpeditionNoticeEmailExWorks';
      $email->subject = EmailFactory::$subjectPrefix . "Expedice objednávky č. " . $order->getNO();
      $url = (Zend_Registry::get('version') == 'TEST' ? EmailFactory::$devUrl : EmailFactory::$url) . ($order->getWhType() == Parts_Part::WH_TYPE_PIPES ? 'orders' : 'tobacco-orders' ) . '/detail/id/' . $order->getID();
      $email->body = EmailFactory::$header
        . "Objednávka č. " . $order->getNO() . ' "' . $order->getTitle() . '" byla vyexpedována.'
        . "<br/>Odkaz: <a href='" . $url . "'>Detail objednávky</a>"
        . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function orderNewCustomerEmail($orderID, $subject, $body, $to) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $to;
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_ORDER_NEW_CUSTOMER_NOTICE];
      $email->subject = EmailFactory::$subjectPrefix . $subject;
      $email->body = EmailFactory::$header . nl2br($body);
      $email->id_orders = $orderID;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function orderOpenCustomerEmail($orderID, $subject, $body, $to) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $to;
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_ORDER_OPEN_CUSTOMER_NOTICE];
      $email->subject = EmailFactory::$subjectPrefix . $subject;
      $email->body = EmailFactory::$header . nl2br($body);
      $email->id_orders = $orderID;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function orderExpeditionCustomerEmail($orderID, $subject, $body, $to) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $to;
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE];
      $email->subject = EmailFactory::$subjectPrefix . $subject;
      $email->body = EmailFactory::$header . nl2br($body);
      $email->id_orders = $orderID;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function preOrder14DaysNoticeEmail($orderID) {
      $order = new Orders_Order($orderID);
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_SALES . ', ' . self::EMAIL_INFO;
      $email->type = 'preOrder14DaysNoticeEmail';
      $email->subject = EmailFactory::$subjectPrefix . "Objednávka č. " . $order->getNO() . ' není potvrzená';

      $url = (Zend_Registry::get('version') == 'TEST' ? EmailFactory::$devUrl : EmailFactory::$url) . ($order->getWhType() == Parts_Part::WH_TYPE_PIPES ? 'orders' : 'tobacco-orders' ) . '/detail/id/' . $order->getID();
      $email->body = EmailFactory::$header
        . "Objednávka č. " . $order->getNO() . " není potvrzená více než 14 dní. "
        . "Ujistěte se, že je s ní vše v pořádku."
        . "<br/>Odkaz: <a href='" . $url . "'>Detail objednávky</a>"
        . EmailFactory::$footer;
      $email->id_orders = $orderID;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function preOrder21DaysNoticeEmail($orderID) {
      $order = new Orders_Order($orderID);
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_SALES . ', ' . self::EMAIL_INFO;
      $email->type = 'preOrder21DaysNoticeEmail';
      $email->subject = EmailFactory::$subjectPrefix . "Objednávka č. " . $order->getNO() . ' stále není potvrzená';

      $url = (Zend_Registry::get('version') == 'TEST' ? EmailFactory::$devUrl : EmailFactory::$url) . ($order->getWhType() == Parts_Part::WH_TYPE_PIPES ? 'orders' : 'tobacco-orders' ) . '/detail/id/' . $order->getID();
      $email->body = EmailFactory::$header
        . "Objednávka č. " . $order->getNO() . " není potvrzená více než 3 týdny. "
        . "Kontaktujte zákazníka zda-li je vše v pořádku."
        . "<br/>Odkaz: <a href='" . $url . "'>Detail objednávky</a>"
        . EmailFactory::$footer;
      $email->id_orders = $orderID;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function preOrderFullNoticeEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_SALES . ', ' . self::EMAIL_INFO;
      $email->type = 'preOrderFullNoticeEmail';
      $email->subject = EmailFactory::$subjectPrefix . "Nepotvrzené objednávky";
      $email->body = $data['body'];
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function bugEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_MANAGER;
      $email->type = 'bugEmail';
      $email->subject = 'Hlášení problému';
      $email->body = EmailFactory::$header
        . "Shrnutí: " . $data['bug_title'] . "<hr/>"
        . "Popis: " . $data['bug_desc'] . "<hr/>"
        . "Stránka: " . $data['bug_page'] . "<hr/>"
        . "Uživatel: " . $data['bug_username'] . "<hr/>"
        . "Role: " . $data['bug_usr_role'] . "<hr/>"
        . "Prohlížeč: " . $data['bug_browser'] . "<hr/>"
        . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function issueCloseEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $data['mailto'];
      $email->type = 'issueClose';
      $email->subject = 'Uzavření problému #' . $data['id_issues'];
      $email->body = EmailFactory::$header
        . "Problém #" . $data['id_issues'] . " byl uzavřen<br>"
        . "Sumarizace problému: " . $data['summary']
        . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function issueCommentEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = implode(',', $data['mailto']);
      $email->type = 'issueComment';
      $email->subject = 'Nový komentář k problému #' . $data['id_issues'];
      $email->body = EmailFactory::$header
        . "Uživatel " . $data['username'] . " vložil nový komentář k problému #" . $data['id_issues'] . "<br>"
        . "Sumarizace problému: " . $data['summary'] . "<br>"
        . "Komentář:<br>" . $data['body'] . "<hr/>"
        . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function missingPartsSummaryEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_WAREHOUSE;
      $email->type = 'missingPartsSummaryEmail';
      $email->subject = 'Denní přehled chybějících součástí';
      $email->body = EmailFactory::$header . $data['body'] . EmailFactory::$footer;
      ;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function delayedProductionEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_WAREHOUSE;
      $email->type = 'delayedProductionEmail';
      $email->subject = 'Pozor! Překročena lhůta dodání zadané výroby';
      $email->body = EmailFactory::$header
        . 'Výroba <b>' . $data['name'] . '</b> č. ' . $data['id'] . ' nebyla dodána v termínu ' . $data['date_delivery'] . '.<br><br>'
        . 'Nedodané položky výroby: ' . '<br><table border="0" cellpadding="3"><tr><th colspan="2">Součást</th><th align="right">Počet</th></tr>';
      foreach ($data['parts'] as $part) {
        if ($part['amount_remain']) {
          $email->body .= '<tr><td>' . $part['id_parts']
            . '</td><td>' . $part['name']
            . '</td><td align="right">' . $part['amount_remain'] . ' ks</td></tr>';
        }
      }
      $email->body .= '</table><br>' . EmailFactory::$footer;
      ;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function noticeCriticalAmountEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_TOBACCO_WAREHOUSE;
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_NOTICE_CRITICAL_AMOUNT];
      $email->subject = 'Report kritického množství';
      $email->body = EmailFactory::$header . $data['body'] . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function noticeZeroCostsEmail($body) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_WAREHOUSE;
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_NOTICE_ZERO_COSTS];
      $email->subject = 'Upozornění na nulové náklady podsoučástí produktů';
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function verifyWorksheetEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $data['email'];
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_VERIFY_WORKSHEET_NOTICE];
      $email->subject = 'Upozornění na špatně vyplněný worksheet';
      $email->body = EmailFactory::$header . $data['body'] . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function endEmployeeContractEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_MANAGER;
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_VERIFY_WORKSHEET_NOTICE];
      $email->subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_END_EMP_CONTRACT_NOTICE] . ' – ' . $data['full_name'];
      $email->body = EmailFactory::$header . $data['body'] . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function birthdayEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_MANAGER . ($data['head_mail'] ? (',' . $data['head_mail']) : '');
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_BIRTHDAY_NOTICE];
      $email->subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_BIRTHDAY_NOTICE] . ' – ' . $data['full_name'];
      $email->body = EmailFactory::$header . $data['body'] . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function orderPrepareEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = implode(',', array_merge($data['recipients'], array(self::EMAIL_TOBACCO_WAREHOUSE)));
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_ORDER_PREPARE];
      $email->subject = $data['subject'];
      $email->body = EmailFactory::$header . $data['body'] . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function orderChangeEmail($data) {
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = implode(',', $data['recipients']);
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_NOTICE_ORDER_CHANGE];
      $email->subject = $data['subject'];
      $email->body = EmailFactory::$header . $data['body'] . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function invoiceChangeEmail($data) {
      $invoice = $data['invoice'];

      /** @var $invoice Invoice_Invoice */
      $body  = 'Byla upravena faktura č. ' . $invoice->getNo() . ' – ' . $invoice->getCustomer()->getName() . '.<br>';
      $body .= 'Aktuální verze je ke stažení na adrese http://warehouse.medusepipes.com/';
      $body .= $invoice->getWHType() == Parts_Part::WH_TYPE_PIPES ? 'invoice' : 'tobacco-invoice';
      $body .= '/download/id/' . $invoice->getId();
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_INVOICE_CHANGE];
      $email->subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_INVOICE_CHANGE] . ' č. ' . $invoice->getNo();
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function paymentAddEmail(Invoice_Payment $payment) {
      $tUsers = new Table_Users();
      $user = $tUsers->getUser($payment->getOrder()->getOwnerId(), FALSE);
      $order = $payment->getOrder();
      $invoice = $payment->getInvoice();
      $baseUrl = $invoice->getWHType() == Table_TobaccoWarehouse::WH_TYPE ? 'https://warehouse.medusepipes.com/tobacco-' : 'https://warehouse.medusepipes.com/';
      $subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_PAYMENT_NOTICE_ADD] . ' č. ' . $order->getNO();
      $body = 'Byla přidána nová úhrada faktury č. ' . $invoice->getNo()
        . ', ' . ' která přísluší objednávce č. ' . $order->getNO() . ' ' . $order->getTitle() . '<br><br>'
        . 'Detail objednávky: ' . $baseUrl . 'orders/detail/id/' . $order->getId() . '<br>'
        . 'Faktura: ' . $baseUrl . 'invoice/download/id/' . $invoice->getId();
      if ($payment->hasNo() && $invoice->getType() == Table_Invoices::TYPE_PROFORM) {
        $body .= '<br>Doklad o úhradě: ' . $baseUrl . 'invoice/payment-get-recipe/id/' . $payment->getId();
      }

      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $user['email'];
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_PAYMENT_NOTICE_ADD];
      $email->subject = $subject;
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function paymentChangeEmail(Invoice_Payment $payment) {
      $tUsers = new Table_Users();
      $user = $tUsers->getUser($payment->getOrder()->getOwnerId(), FALSE);
      $order = $payment->getOrder();
      $invoice = $payment->getInvoice();
      $baseUrl = $invoice->getWHType() == Table_TobaccoWarehouse::WH_TYPE ? 'https://warehouse.medusepipes.com/tobacco-' : 'https://warehouse.medusepipes.com/';
      $subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_PAYMENT_NOTICE_ADD] . ' č. ' . $order->getNO();
      $body = 'Byla změněna úhrada faktury č. ' . $invoice->getNo()
        . ', ' . ' která přísluší objednávce č. ' . $order->getNO() . ' ' . $order->getTitle() . '<br><br>'
        . 'Detail objednávky: ' . $baseUrl . 'orders/detail/id/' . $order->getId() . '<br>'
        . 'Faktura: ' . $baseUrl . 'invoice/download/id/' . $invoice->getId();
      if ($invoice->getType() == Table_Invoices::TYPE_PROFORM) {
        $body .= '<br>Doklad o úhradě: ' . $baseUrl . 'invoice/payment-get-recipe/id/' . $payment->getId();
      }

      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $user['email'];
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_PAYMENT_NOTICE_CHANGE];
      $email->subject = $subject;
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function paymentRemoveEmail(Invoice_Payment $payment) {
      $tUsers = new Table_Users();
      $user = $tUsers->getUser($payment->getOrder()->getOwnerId(), FALSE);
      $order = $payment->getOrder();
      $invoice = $payment->getInvoice();
      $baseUrl = $invoice->getWHType() == Table_TobaccoWarehouse::WH_TYPE ? 'https://warehouse.medusepipes.com/tobacco-' : 'https://warehouse.medusepipes.com/';
      $subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_PAYMENT_NOTICE_ADD] . ' č. ' . $order->getNO();
      $body = 'Byla odstraněna úhrada faktury č. ' . $invoice->getNo()
        . ', ' . ' která přísluší objednávce č. ' . $order->getNO() . ' ' . $order->getTitle() . '<br><br>'
        . 'Detail objednávky: ' . $baseUrl . 'orders/detail/id/' . $order->getId() . '<br>'
        . 'Faktura: ' . $baseUrl . 'invoice/download/id/' . $invoice->getId();

      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $user['email'];
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_PAYMENT_NOTICE_CHANGE];
      $email->subject = $subject;
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function unpaidInvoicesEmail($data) {
      $now = new Meduse_Date();

      if (isset($data['wh'])) {
        $wh_body = $data['wh'] == Table_PartsWarehouse::WH_TYPE ? 'MEDUSE ' : 'MEDITE ';
        $wh_subj = $data['wh'] == Table_PartsWarehouse::WH_TYPE ? ' - MEDUSE' : ' - MEDITE';
      }
      else {
        $wh_body = '';
        $wh_subj = '';
      }

      $body = 'Ke dni ' . date('d.m.Y') . ' nebyly uhrazeny následující faktury ' . $wh_body . 'po splatnosti:<br><ol>';
      foreach ($data['stats'] as $row) {
        $baseUrl = $row->wh == Table_TobaccoWarehouse::WH_TYPE ? 'https://warehouse.medusepipes.com/tobacco-' : 'https://warehouse.medusepipes.com/';
        $due = new Meduse_Date($row->due_date);
        $type = ucfirst(Table_Invoices::$typeToString[$row->type]);
        $days = round($now->getDate()->sub($due->getDate())->getTimestamp() / (60*60*24));
        switch($days) {
          case 1:
            $over = $days . ' den';
            break;
          case 2:
          case 3:
          case 4:
            $over = $days . ' dny';
            break;
          default:
            $over = $days . ' dnů';
        }
        $body .= '<li><a href="' . $baseUrl . 'invoice/download/id/' . $row->id
          .'">' . $type .  ' č. ' . $row->invoice_no . '</a> – ' . $over . ' po splatnosti, částka: ' . $row->invoiced_total
          . ' ' . $row->currency . ', uhrazeno: ' . $row->payments . ' ' . $row->currency
          . ' (<a href="' . $baseUrl . 'orders/detail/id/' . $row->order_id
          . '">obj.: ' . $row->order_no . ' ' . $row->order_title . '</a>)</li>';
      }
      $body .= '</ol>';
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = $data['recipient'];
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_NOTICE_UNPAID_INVOICES];
      $email->subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_NOTICE_UNPAID_INVOICES] . $wh_subj;
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function partsExportEmail($data) {

      $body  = 'Byly automaticky vygenerovány exporty skladu dýmek.<br>';
      $body .= '<ul>';
      foreach($data['filenames'] as $filename) {
        $body .= '<li><a href="https://warehouse.medusepipes.com/export/download/filename/' . $filename . '">' . $filename . '</a></li>';
      }
      $body .= '</ul>';
      $body .= 'Veškeré exporty jsou dostupné na přehledu vygenerovaných exportů<br>';
      $body .= 'https://warehouse.medusepipes.com/export/';

      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_MANAGER;
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_NOTICE_PARTS_EXPORT];
      $email->subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_NOTICE_PARTS_EXPORT];
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");

      return $email;
    }

    public static function partsExportEmailMedite($data) {

      $body  = 'Byly automaticky vygenerovány exporty skladu tabáku.<br>';
      $body .= '<ul>';
      foreach($data['filenames'] as $filename) {
        $body .= '<li><a href="https://warehouse.medusepipes.com/tobacco/export-download/filename/' . $filename . '">' . $filename . '</a></li>';
      }
      $body .= '</ul>';
      $body .= 'Veškeré exporty jsou dostupné na přehledu vygenerovaných exportů<br>';
      $body .= '<a href="https://warehouse.medusepipes.com/tobacco/export/">' .
        'https://warehouse.medusepipes.com/tobacco/export/</a>';

      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = implode(',', [self::EMAIL_MANAGER, self::EMAIL_TOBACCO_WAREHOUSE]);
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_NOTICE_PARTS_EXPORT_MEDITE];
      $email->subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_NOTICE_PARTS_EXPORT_MEDITE];
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");

      return $email;
    }


    public static function invoiceInCashEmail(Invoice_Invoice $invoice) {
      $baseUrl = $invoice->getWHType() == Table_TobaccoWarehouse::WH_TYPE ? 'https://warehouse.medusepipes.com/tobacco-' : 'https://warehouse.medusepipes.com/';
      $subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_INVOICE_IN_CASH_NOTICE];
      $order = $invoice->getOrder();
      $body = 'Byla vytvořená nová faktura č. ' . $invoice->getNo() . ' hrazená v hotovosti,<br>'
        . 'která přísluší objednávce č. ' . $order->getNO() . ' ' . $order->getTitle() . '<br><br>'
        . 'Detail objednávky: ' . $baseUrl . 'orders/detail/id/' . $order->getId() . '<br>'
        . 'Faktura: ' . $baseUrl . 'invoice/download/id/' . $invoice->getId();
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_BOSS;
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_INVOICE_IN_CASH_NOTICE];
      $email->subject = $subject;
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function noticeOrderDeadlineEmail($data) {
      $url = "https://warehouse.medusepipes.com/orders/detail/id/{$data['id']}";
      $subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_ORDER_DEADLINE_NOTICE] . ' č. ' . $data['no'];
      $body = "Byl změněný deadline u objenávky č. {$data['no']} – {$data['title']}<br><br>" .
        "Původní deadline: {$data['deadlineOld']}<br>" .
        "Nový deadline: {$data['deadlineNew']}<br><br>" .
        "Deatil objednávky: <a href='{$url}'>{$url}</a><br>";
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = self::EMAIL_WAREHOUSE;
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_ORDER_DEADLINE_NOTICE];
      $email->subject = $subject;
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }

    public static function noticeOrderPackagingEmail(array $params) {

      /** @var Orders_Order $order */
      $order = isset($params['order']) ? $params['order'] : NULL;
      $type = isset($params['type']) ? $params['type'] : 'default';

      if (!($order instanceof Orders_Order)) {
        return;
      }

      $baseUrl = $order->getWHType() == Table_TobaccoWarehouse::WH_TYPE ? 'https://warehouse.medusepipes.com/tobacco-' : 'https://warehouse.medusepipes.com/';
      $id = $order->getID();
      $no = $order->getNO();
      $title = $order->getTitle();
      $url = "${baseUrl}orders/detail/id/$id#infos";

      $subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_NOTICE_ORDER_PACKAGING] . ' č. ' . $no;

      switch ($type) {

        // Polozky.
        case 'items':
          $subject .= ' — položky balení';
          $body = "<p>Pozor, u objednávky č. $no — $title byly změněny položky balení.</p>";
          if ($packaging = $order->getPackaging(TRUE)) {
            $it = 1;
            $body .= "<table border='1' width='100%'><tr><th>#</th><th>typ</th><th>rozměry</th><th>objem</th><th>váha</th></tr>";
            foreach ($packaging['items'] as $item) {
              $type = Table_Packaging::$translate[$item['type']];
              $body .= "<tr><td align='right'>$it.</td><td>$type</td>"
                . "<td align='right'>${item['width']}&times;${item['depth']}&times;${item['height']}&nbsp;cm</td>"
                . "<td align='right'>${item['volume']}&nbsp;m<sup>3</sup></td><td align='right'>${item['weight_brutto']}&nbsp;kg</td></tr>";
              $it++;
            }
            $body .= "<tr></tr><th colspan='3'>celkem</th><td align='right'><b>${packaging['volume']}&nbsp;m<sup>3</sup></b>"
              . "</td><td align='right'><b>${packaging['weight']}&nbsp;kg</b></td></tr></table>";

          }
          else {
            $body .= '<p>Nejsou definované žádné položky balení.</p>';
          }
          break;

        case 'tracking':
          // Tracking.
          $subject .= ' — trackovací čísla';
          $body = "<p>Pozor, u objednávky č. $no — $title byla změněna trackovací čísla.</p>";
          if ($tracking = $order->getTrackingCodes()) {
            $codes = array_column($tracking, 'code');
            $body .= '<ul><li>' . implode('</li><li>', $codes) . '</li></ul>';
          }
          else {
            $body .= '<p>Nejsou přiřazena žádná trackovací čísla.</p>';
          }
          break;

        default:
          $body = "<p>Pozor, u objednávky č. $no — $title bylo změněno balení.</p>";
      }

      $body .= "<p>Detail objednávky: <a href='$url'>$url</a></p>";

      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_NOTICE_ORDER_PACKAGING];
      $email->to = implode(', ', [self::EMAIL_WAREHOUSE, self::EMAIL_OFFICE, self::EMAIL_SALES]);
      $email->inserted = new Zend_Db_Expr("NOW()");
      $email->subject = $subject;
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      return $email;
    }

    public static function tobaccoPackingReport($params) {
      $subject = Table_Emails::$number_to_readable[Table_Emails::EMAIL_TOBACCO_PACKING_REPORT];
      $body = $params['body'];
      $tEmail = new Table_Emails();
      $email = $tEmail->createRow();
      $email->to = implode(',', [self::EMAIL_TOBACCO_WAREHOUSE, self::EMAIL_TOBACCO_OPERATOR]);
      $email->type = Table_Emails::$number_to_db_type[Table_Emails::EMAIL_TOBACCO_PACKING_REPORT];
      $email->subject = $subject;
      $email->body = EmailFactory::$header . $body . EmailFactory::$footer;
      $email->inserted = new Zend_Db_Expr("NOW()");
      return $email;
    }
  }
