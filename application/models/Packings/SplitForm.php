<?php

/**
 * Formulář na rozdělení balení při přípravě packing listů
 *
 * @author Vojtěch Mrkývka
 */
class Packings_SplitForm extends Meduse_FormBootstrap {
  public function init() {
    $this->setAttrib('id', 'splitform');
    
    $element = new Meduse_Form_Element_Text('put-aside');
    $element->setLabel('Oddělit [ks]');
    $this->addElement($element);
    
    $element = new Meduse_Form_Element_Submit('split-submit');
    $element->setLabel('Oddělit');
    $this->addElement($element);
    
    parent::init();
  }
}
