<?php

class Orders {

  const SHOP_B2B_CARRIERS_TRANSLATE = [
    'ex_works' => 270,
    'ppl' => 357,
  ];

  const SHOP_B2B_PAYMENT_METHODS_TRANSLATE = [
    'transfer' => [
      'ex_works' => Table_Orders::PM_TRANSFER,
      'ppl' => Table_Orders::PM_TRANSFER
    ],
    'cash' => [
      'ex_works' => Table_Orders::PM_CASH,
      'ppl' => Table_Orders::PM_CASH_ON_DELIVERY,
    ],
    'cod' => [
      'ex_works' => Table_Orders::PM_CASH,
      'ppl' => Table_Orders::PM_CASH_ON_DELIVERY,
    ],
  ];

  // id z tabulky orders_carriers
  const SHOP_B2B_SHIPPING_METHODS_TRANSLATE = [
    'transfer' => ['ex_works' => 42, 'ppl' => 64],
    'cash' => ['ex_works' => 42, 'ppl' => 61],
    'cod' => ['ex_works' => 42, 'ppl' => 61],
  ];

  const SHOP_B2C_CARRIERS_TRANSLATE = [
    'ppl' => 357,
    'ppl_free' => 357,
    'ppl_cash' => 357,
    'ppl_cash_ver' => 357,
    'ppl_card' => 357,
    'ppl_card_ver' => 357,
    'cp' => 328,
    'cp_cash' => 328,
  ];

  const SHOP_B2C_PAYMENT_METHODS_TRANSLATE = [
    'transfer' => [
      'ppl' => Table_Orders::PM_TRANSFER,
      'ppl_free' => Table_Orders::PM_TRANSFER,
      'cp' => Table_Orders::PM_TRANSFER,
    ],
    'pod' => [
      'ppl_free' => Table_Orders::PM_CASH_ON_DELIVERY,
      'ppl_cash' => Table_Orders::PM_CASH_ON_DELIVERY,
      'ppl_cash_ver' => Table_Orders::PM_CASH_ON_DELIVERY,
      'ppl_card' => Table_Orders::PM_CASH_ON_DELIVERY,
      'ppl_card_ver' => Table_Orders::PM_CASH_ON_DELIVERY,
      'cp_cash' => Table_Orders::PM_CASH_ON_DELIVERY,
    ],
    'uc_payscz_std' => [
      'ppl' => Table_Orders::PM_PAYSCZ,
      'ppl_free' => Table_Orders::PM_PAYSCZ,
      'cp' => Table_Orders::PM_PAYSCZ,
    ],
  ];

  const SHOP_B2C_SHIPPING_METHODS_TRANSLATE = [
    'transfer' => [
      'ppl' => 63,
      'ppl_free' => 65,
      'cp' => 44,
    ],
    'pod' => [
      'ppl_free' => 65,
      'ppl_cash' => 62,
      'ppl_cash_ver' => 66,
      'ppl_card' => 62,
      'ppl_card_ver' => 66,
      'cp_cash' => 43,
    ],
    'uc_payscz_std' => [
      'ppl' => 63,
      'ppl_free' => 65,
      'cp' => 44,
    ],
  ];

  public static function getProformDataGrid($params) {
    $view = new Zend_View();
    $prefix = isset($params['wh']) && $params['wh'] == Table_TobaccoWarehouse::WH_TYPE ? 'tobacco-' : '';
    $grid = new ZGrid(array(
      'title' => _("Proforma"),
      'allow_add_link' => false,
      'columns' => array(
        'proforma_no' => array(
          'header' => _('Proforma'),
        ),
        'invoice' => array(
          'header' => '',
          'css_style' => 'text-align: left',
          'sorting' => false,
          'renderer' => 'condition',
          'condition_value' => NULL,
          'condition_false' => array(
            'renderer' => 'Bootstrap_Button',
            'icon' => 'icon icon-download',
            'url' => $view->url(array(
              'controller' => $prefix . 'invoice',
              'action' => 'download',
              'id' => '{invoice}',
            )),
          ),
        ),
        'title' => array(
          'header' => _('Objednávka'),
          'css_style' => 'text-align: left',
          'sorting' => false,
          'renderer' => 'url',
          'url' => $view->url(array(
            'module' => 'default', 'controller' => 'orders',
            'action' => 'detail', 'id' => '{id}'
            ), null, true)
        ),
        'date_confirm' => array(
          'header' => _('Datum potvrzení'),
          'renderer' => 'date',
          'css_style' => 'text-align: left',
          'sorting' => false
        ),
        'purchaser' => array(
          'header' => _('Typ zákazníka'),
          'css_style' => 'text-align: left',
          'sorting' => false,
          'renderer' => 'replace',
          'replace_conditions' => array(
            'personal' => 'Koncový zákazník',
            'business' => 'Obchodní kontakt'
          ),
          'replace_value' => 'purchaser'
        ),
        'expected_payment' => array(
          'header' => _('Očekávaná platba'),
          'renderer' => 'currency',
          'currency_options' => array(
            'currency_value' => 'expected_payment',
            'currency_symbol' => 'expected_payment_currency'
          )
        ),
        'country' => array(
          'header' => _('Země')
        )
      )
    ));

    $table = new Table_Orders();
    $select = $table->getProformSelect($params);

    $grid->setSelect($select);
    $grid->setRequest($params);

    return $grid;
  }

  public static function getNextNo($wh_type) {
    $tOrders = new Table_Orders();
    return $tOrders->getNextNo($wh_type);
  }

  public static function createFromJson($json, $wh_type = Table_PartsWarehouse::WH_TYPE) {
    $customer = NULL;
    $products = array();
    $result = array(
      'json' => $json,
      'order' => NULL,
      'billing' => NULL,
      'shipping' => NULL,
      'pricelist' => NULL,
      'payment_method' => NULL,
      'shipping_method' => NULL,
      'errors' => array(),
    );
    $data = json_decode($json, TRUE);
    $customer = NULL;
    if (empty($data['billing']['company_id'])) {
      if (empty($data['billing']['mail'])) {
        $result['errors'][] = 'Nebyl zadán ani email ani IČ.';
      }
      elseif (!$customer = Customers::findByMail($data['billing']['mail'], $wh_type)) {
        $result['errors'][] = 'Nebyl nalezen zákazník s emailem "' . $data['billing']['mail'] . '".';
      }
    }
    else {
      if (!$customer = Customers::findByIdentNo($data['billing']['company_id'], $wh_type)) {
        $result['errors'][] = 'Nebyl nalezen zákazník s IČ = "' . $data['billing']['company_id'] . '".';
      }
    }
    $deliveries = NULL;
    if ($customer) {
      $tablePrice = new Table_Pricelists();
      if ($row = $tablePrice->getPricelistByAbbr($data['pricelist'])) {
        $pricelist = $row['id'];
      }
      else {
        if (!$pricelist = $tablePrice->getCustomersValidPricelist($customer->getId())) {
          $result['errors'][] = 'Zákazník ID = "' . $customer->getId() . '" nemá přiřazen platný ceník';
        }
      }
      $carriers = $customer->isB2B() ? self::SHOP_B2B_CARRIERS_TRANSLATE : self::SHOP_B2C_CARRIERS_TRANSLATE;
      $carrier = new Producers_Carrier($carriers[$data['shipping_method']]);
      $deliveries = $carrier->getDeliveries();

      $shipping_methods = $customer->isB2B() ? self::SHOP_B2B_SHIPPING_METHODS_TRANSLATE : self::SHOP_B2C_SHIPPING_METHODS_TRANSLATE;
      $id_orders_carriers = $shipping_methods[$data['payment_method']][$data['shipping_method']];
    }

    $carrier_price = NULL;
    if ($deliveries) {
      foreach ($deliveries as $delivery) {
        if ($delivery->id == $id_orders_carriers) {
          $carrier_price = $delivery->default_price ? $delivery->default_price : NULL;
          break;
        }
      }
    }
    foreach ($data['products'] as $sku => $amount) {
      if ((substr($sku, -5) === '_FREE')) {
        $free = TRUE;
        $sku = substr($sku, 0, strlen($sku) - 5);
      }
      else {
        $free = FALSE;
      }
      if ($product = Parts::findBySKU($sku)) {
        $products[$product->getID()] = array(
          'product' => $product,
          'amount' => $free ? 0 : $amount,
          'free' => $free ? $amount : 0);
      }
      else {
        $result['errors'][] = 'Nebyl nalezen produkt s SKU = "' . $sku . '".';
      }
    }
    if (empty($result['errors'])) {
      $payment_methods = $customer->isB2B() ? self::SHOP_B2B_PAYMENT_METHODS_TRANSLATE : self::SHOP_B2C_PAYMENT_METHODS_TRANSLATE;
      $payment_method = $payment_methods[$data['payment_method']][$data['shipping_method']];
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $now = new Zend_Db_Expr("NOW()");
      $table = new Table_Orders();
      $orderId = $table->createRow(array(
        'type' => Table_Orders::TYPE_PRODUTCS,
        'private' => 'n',
        'no' => self::getNextNo($wh_type),
        'title' => $customer->getCompany(),
        'description' => $customer->isB2B() ? 'B2B objednávka z eshopu' : 'B2C objednávka z eshopu',
        'id_wh_type' => $wh_type,
        'date_request' => $now,
        'status' => Table_Orders::STATUS_NEW,
        'owner' => $authNamespace->user_id,
        'id_customers' => $customer->getId(),
        'id_addresses' => $customer->getBillingAddressId(),
        'id_pricelists' => $pricelist,
        'expected_payment_rate' => 1,
        'payment_method' => $payment_method,
        'id_orders_carriers' => $id_orders_carriers,
        'carrier_price' => $carrier_price,
        'inserted' => $now,
      ))->save();

      if ($orderId) {
        $result['order'] = $wh_type == Table_PartsWarehouse::WH_TYPE ? new Orders_Order($orderId) : new Tobacco_Orders_Order($orderId);

        // vlozeni produktu
        if ($products) {
          foreach ($products as $productId => $product) {
            $result['order']->addProduct($productId, $product['amount'], $product['free']);
          }
          $result['order']->recalculateExpectedPayment();
        }

        $result['billing'] = '';
        foreach ($data['billing'] as $idx => $line) {
          if ($line) {
            switch ($idx) {
              case 'phone': $line = 'telefon: ' . $line; break;
              case 'company_id': $line = 'IČ: ' . $line; break;
              case 'vatid': $line = 'DIČ: ' . $line; break;
              case 'mail': $line = 'E-mail: ' . $line; break;
            }
            $result['billing'] .= $line . '<br>';
          }
        }
        $result['shipping'] = '';
        foreach ($data['shipping'] as $idx => $line) {
          if ($line) {
            $result['shipping'] .= $line . '<br>';
          }
        }

      }
      else {
        $result['errors'][] = 'Objednávku se nepodařilo založit.';
      }
    }
    return $result;
  }
}
