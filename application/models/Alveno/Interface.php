<?php

/**
 * @author Zdeněk Filipec <zdendaf@gmail.com>
 */
interface Alveno_Interface {
  public function getNewRecords();
  public function fetchRecord();
  public function fetchAllRecords();
  public function fetchAllMessages();
  public function getAffected();
  public function checkNewestFilename($return = FALSE);
  public function getLastRecord();
  public function getNewestRecord();
  public function distStates();
}

