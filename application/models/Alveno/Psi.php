<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Alveno_Psi extends Alveno_HTTP implements Alveno_Interface {

    protected $_setting_prefix = NULL;
    protected $_dist_states = FALSE;

    public function __construct($key) {
      $this->_setting_prefix = 'psi_' . $key . '_';
      parent::__construct($key);
    }

    public function getNewRecords() {

      $this->importRecords();

      if ($this->_raw) {
        $tEmployees = new Table_Employees();
        foreach ($this->_raw as $line) {
          list($e, $d, $t, $r) = explode('|', $line, 4);

          $employee = $tEmployees->findByAlvenoId($e);
          if ($employee) {

            $year  = substr($d, 0, 4);
            $month = substr($d, 4, 2);

            $timestamp = $year . '-' . $month . '-' . substr($d, 6, 2)
              . ' ' . substr($d, 8, 2) . ':' . substr($d, 10, 2) . ':00';

            // kontrola data
            $now = date('Y-m-d H:i:s');
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $timestamp);
            if ($now < $timestamp || !$date) {
              $this->_messages[] = 'Chyba v datumu ' . $timestamp . '.';
              continue;
            }

            $this->_records[] = array(
              'id_employees' => $employee->id,
              'timestamp' => $timestamp,
              'type' => Table_EmployeesRecords::TYPE_WORK,
              'status' => NULL,
              'description' => NULL,
            );

            if (!isset($this->_affected[$employee->id])) {
              $this->_affected[$employee->id] = array();
            }
            $this->_affected[$employee->id] = array_merge($this->_affected[$employee->id], array($year . '-' . $month));
          }
          else {
           $this->_messages[] = 'Nebyl rozpoznán pracovník ID = ' . $e . ' (' . $d . ' / ' . $t . ')';
          }
        }
      }
      if ($this->_records) {
        reset($this->_records);
        $this->_hasNext = TRUE;
      }
    }

  }


