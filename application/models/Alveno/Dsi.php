<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Alveno_Dsi extends Alveno_HTTP implements Alveno_Interface {

    protected $_setting_prefix = NULL;
    protected $_dist_states = TRUE;

    public function __construct($key) {
      $this->_setting_prefix = 'dsi_' . $key . '_';
      parent::__construct($key);
    }
    public function getNewRecords() {

      $this->importRecords();
      if ($this->_raw) {
        $tEmployees = new Table_Employees();
        foreach ($this->_raw as $line) {
          list($f, $d, $t, $r) = explode('|', $line, 4);

          // alveno pridava do zaznamu k id jednu nulu nakonec
          $alveno = substr($f, 0, -1);
          $employee = $tEmployees->findByAlvenoId($alveno);
          if ($employee) {

            $year  = substr($d, 0, 4);
            $month = substr($d, 4, 2);

            $timestamp = $year . '-' . $month . '-' . substr($d, 6, 2)
              . ' ' . substr($d, 8, 2) . ':' . substr($d, 10, 2) . ':00';

            if (!in_array((int)$t, array(0, 1, 5, 6))) {
              $this->_messages[] = 'Nebyl rozpoznán stav #' . $t;
              continue;
            }

            // kontrola data
            $now = date('Y-m-d H:i:s');
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $timestamp);
            if ($now < $timestamp || !$date) {
              $this->_messages[] = 'Chyba v datumu ' . $timestamp . '.';
              continue;
            }

            $this->_records[] = array(
              'id_employees' => $employee->id,
              'timestamp' => $timestamp,
              'type' => Table_EmployeesRecords::TYPE_WORK,
              'status' => $this->_status[$t],
              'description' => NULL,
            );

            if (!isset($this->_affected[$employee->id])) {
              $this->_affected[$employee->id] = array();
            }
            $this->_affected[$employee->id] = array_merge($this->_affected[$employee->id], array($year . '-' . $month));
          }
          else {
           $this->_messages[] = 'Nebyl rozpoznán pracovník ID = ' . $alveno . ' (' . $d . ' / ' . $t . ')';
          }
        }
      }
      if ($this->_records) {
        reset($this->_records);
        $this->_hasNext = TRUE;
      }
    }
  }


