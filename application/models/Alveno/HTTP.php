<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Alveno_HTTP implements Alveno_Interface {

    const ROUNDING_PRECISION = 15;
    const ROUNDING_TOLERANCE = 5;

    protected $_status = array(
      '0' => Table_EmployeesRecords::STATE_CHECKIN,
      '1' => Table_EmployeesRecords::STATE_CHECKOUT,
      '5' => Table_EmployeesRecords::STATE_BREAKIN,
      '6' => Table_EmployeesRecords::STATE_BREAKOUT,
    );

    protected $_server = NULL;
    protected $_user = NULL;
    protected $_password = NULL;

    protected $_setting_prefix = NULL;
    protected $_raw = array();
    protected $_dist_states = FALSE;

    protected $_connection = NULL;
    protected $_login = NULL;
    protected $_files = array();

    protected $_records = array();
    protected $_hasNext = FALSE;
    protected $_messages = array();

    protected $_affected = array();

    protected $_type = NULL;

    protected $_key = NULL;

    public function __construct($key) {
      if (!Zend_Registry::get('config')->alveno->enabled) {
        throw new Exception('Alveno is disabled.');
      }
      $this->_server = Zend_Registry::get('config')->alveno->http->server;
      $this->_user = Zend_Registry::get('config')->alveno->http->user;
      $this->_password = Zend_Registry::get('config')->alveno->http->password;
      $this->_key = $key;
    }

    public function getNewRecords() {
      $this->_records = array();
    }

    public function fetchRecord() {
      if ($this->_hasNext) {
        $record = current($this->_records);
        $this->_hasNext = next($this->_records) !== FALSE;
        return $record;
      }
      else {
        return FALSE;
      }
    }

    public function fetchAllRecords() {
      return $this->_records;
    }

    public function fetchAllMessages() {
      return $this->_messages;
    }

    public function getAffected() {
      return $this->_affected;
    }

    public function checkNewestFilename($return = FALSE) {
      return;
    }

    public function getLastRecord() {
      if (is_null($this->_setting_prefix)) {
        return FALSE;
      }
      $settings = new Table_Settings();
      $last = $settings->get($this->_setting_prefix . 'last');
      try {
        return new Meduse_Date($last, 'YmdHis');
      }
      catch (Exception $e) {
        return FALSE;
      }

    }

    public function getNewestRecord() {
      return NULL;
    }

    public function distStates() {
      return $this->_dist_states;
    }


    protected function importRecords() {

      $tSettings = new Table_Settings();
      $var_name = $this->_setting_prefix . 'last';
      $from = date_create_from_format('YmdHis', $tSettings->get($var_name, '20190601000000'));

      $url = $this->_server . '?c=' . $this->_key . '&from=' . $from->format('YmdHis');
      if ($this->_raw = file($url)) {
        $last_line = array_pop($this->_raw);
        $timestamp = trim($last_line);
        $to = date_create_from_format('Y-m-d H-i-s', $timestamp);
        $this->_messages[] = 'Byly importovány záznamy od z terminálu "'
          . $this->_key . '" od ' . $from->format('d.m.Y H:i')
          . ' do ' . $to->format('d.m.Y H:i') . '.';
        $tSettings->set($var_name, $to->format('YmdHis'));
      }
      else {
        $this->_messages[] = 'Nebyly nalezeny žádné nové záznamy z terminálu "'
          . $this->_key . '" od ' . $from->format('d.m.Y H:i') . '.';
      }
    }
  }


