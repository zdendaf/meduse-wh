<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Alveno_FTP implements Alveno_Interface {

    const ENABLED = TRUE;

    const SERVER = 'vpn.meduse-experience.com';
    const PORT = '34567';
    const USERNAME = 'filipec';
    const PASSWORD = 'G2QtPrCd96';

    const ROUNDING_PRECISION = 15;
    const ROUNDING_TOLERANCE = 5;

    protected $_status = array(
      '0' => Table_EmployeesRecords::STATE_CHECKIN,
      '1' => Table_EmployeesRecords::STATE_CHECKOUT,
      '5' => Table_EmployeesRecords::STATE_BREAKIN,
      '6' => Table_EmployeesRecords::STATE_BREAKOUT,
    );

    protected $_directory = NULL;
    protected $_setting_prefix = NULL;
    protected $_local_file = NULL;
    protected $_dist_states = FALSE;

    protected $_connection = NULL;
    protected $_login = NULL;
    protected $_files = array();

    protected $_records = array();
    protected $_hasNext = FALSE;
    protected $_messages = array();

    protected $_affected = array();

    protected $_type = NULL;

    public function __construct() {

      if (!Zend_Registry::get('config')->alveno->enabled) {
        throw new Exception('Alveno is disabled.');
      }

      if (!self::ENABLED) {
        throw new Exception(self::SERVER . ' is disabled.');
      }

      // connect to FTP server
      $this->_connection = ftp_connect(self::SERVER, self::PORT);
      if (!self::ENABLED || !$this->_connection) {
        throw new Exception('Could not connect to ' . self::SERVER .'.');
      }

      // login to FTP server
      $this->_login = @ftp_login($this->_connection, self::USERNAME, self::PASSWORD);
      if (!$this->_login) {
        throw new Exception('Could not log in for user ' . self::USERNAME . '.');
      }

      // set to passive mode
      ftp_pasv($this->_connection, TRUE);

      // change the current directory to php
      if ($this->_directory) {
        ftp_chdir($this->_connection, $this->_directory);
        $this->_files = ftp_nlist($this->_connection, '.');
        sort($this->_files);
      }

    }

    public function __destruct() {
      if ($this->_connection) {
        ftp_close($this->_connection);
      }
    }

    public function getNewRecords() {
      $this->_records = array();
    }

    public function fetchRecord() {
      if ($this->_hasNext) {
        $record = current($this->_records);
        $this->_hasNext = next($this->_records) !== FALSE;
        return $record;
      }
      else {
        return FALSE;
      }
    }

    public function fetchAllRecords() {
      return $this->_records;
    }


    public function fetchAllMessages() {
      return $this->_messages;
    }

    public function getAffected() {
      return $this->_affected;
    }

    public function checkNewestFilename($return = FALSE) {
      $name = $this->_files[count($this->_files)-1];
      $tSettings = new Table_Settings();
      $tSettings->set($this->_setting_prefix . 'newest_file', $name);
      if ($return) {
        return $name;
      }
    }

    public function getLastRecord() {
      if (is_null($this->_setting_prefix)) {
        return FALSE;
      }
      $settings = new Table_Settings();
      $lastName = $settings->get($this->_setting_prefix . 'last_file');
      if (!$lastName) {
        return new Meduse_Date(NULL, 'Y-m-d H-i-s');
      }
      else {
        return new Meduse_Date(substr($lastName, 0, 19), 'Y-m-d H-i-s');
      }
    }

    public function getNewestRecord() {
      $settings = new Table_Settings();
      $newestName = $settings->get($this->_setting_prefix . 'newest_file');
      if (!$newestName) {
        return new Meduse_Date(NULL, 'Y-m-d H-i-s');
      }
      else {
        return new Meduse_Date(substr($newestName, 0, 19), 'Y-m-d H-i-s');
      }
    }

    public function distStates() {
      return $this->_dist_states;
    }

    protected function getNewFiles() {

      $tSettings = new Table_Settings();
      $lastFile = $tSettings->get($this->_setting_prefix . 'last_file');

      $file = fopen($this->_local_file, 'w');
      $skip = $lastFile ? TRUE : FALSE;
      foreach ($this->_files as $filename) {
        if ($skip && $filename === $lastFile) {
          $skip = FALSE;
          continue;
        }
        elseif ($skip) {
          continue;
        }
        ftp_fget($this->_connection, $file, $filename, FTP_BINARY);
        $this->_messages[] = 'Byl importován soubor "' . $filename . '".';
      }
      fclose($file);

      $tSettings->set($this->_setting_prefix . 'last_file', $filename);
      $tSettings->set($this->_setting_prefix . 'newest_file', $filename);
    }
  }


