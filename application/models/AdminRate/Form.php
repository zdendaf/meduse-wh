<?php

class AdminRate_Form extends Meduse_FormBootstrap {
  protected $action = '/admin/rate-update';
  protected $warehouse = 1;

  public function init() {
    $this->setName('rateform');
    $this->setAction($this->action);
    $this->setMethod(Zend_Form::METHOD_POST);
    $this->setAttrib('class', 'form');
    $this->setLegend('Nastavení kurzu');

    $element = new Meduse_Form_Element_Float('rate');
    $element->setLabel('Kurz EUR/CZK');
    $element->setRequired(TRUE);
    $tRate = new Table_ApplicationRate();
    $element->setValue($tRate->getRate($this->warehouse));
    $this->addElement($element);

    $element = new Zend_Form_Element_Submit('Uložit');
    $this->addElement($element);

    parent::init();

  }
}
