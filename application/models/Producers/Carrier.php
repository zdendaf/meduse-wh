<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Producers_Carrier extends Producers_Producer {

    protected $_tDelivery = NULL;
    protected $_rDelivery = array();

    public function __construct($id = NULL) {
      $this->_tDelivery = new Table_OrdersCarriers();
      parent::__construct($id);
    }

    public function init($id) {
      parent::init($id);
      $this->_rDelivery = $this->_tDelivery
        ->select()->where('id_producers = ?', $this->_rProducers->id)
        ->query(Zend_Db::FETCH_OBJ)->fetchAll();
    }

    /**
     * Pridani zpusobu dopravy
     */
    public function addDelivery($data) {
      $data['id_producers'] = $this->_rProducers->id;
      if (!isset($data['inserted'])) {
        $data['inserted'] = date('Y-m-d H:i:s');
      }
      $this->_tDelivery->insert($data);
      $this->init($this->_rProducers->id);
    }


    /**
     * Editace zpusobu dopravy
     */
    public function updateDelivery($data) {
      $data['id_producers'] = $this->_rProducers->id;
      $this->_tDelivery->update($data, 'id = ' . $this->_tDelivery->getAdapter()->quote($data['id']));
      $this->init($this->_rProducers->id);
    }

    /**
     * Vrati pole zpusobu dopravy
     */
    public function getDeliveries() {
      return $this->_rDelivery;
    }

    public function getTrackingPattern() {
      return $this->_rProducers->tracking_pattern;
    }
  }
