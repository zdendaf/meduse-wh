<?php

class Producers_ProductionSearchForm extends Meduse_FormBootstrap {

  public function init() {
    $this->setLegend('Výroba součástí');

    $this->setAttribs(['class' => 'form'])->setMethod('get');

    $element = new Zend_Form_Element_Text('name');
    $element->setLabel('Jméno dodavatele');
    $this->addElement($element);

    $element = new Zend_Form_Element_Select('producer');
    $element->setLabel('Dodavatel');
    $element->addMultiOption('', '-- všichni dodavatelé --');
    $element->addMultiOptions(Zend_Db_Table::getDefaultAdapter()
      ->fetchPairs("SELECT id, name FROM producers WHERE id_wh_type = 1 AND id_orders_carriers IS NULL ORDER BY name"));
    $this->addElement($element);

    $element = new Zend_Form_Element_Checkbox('show');
    $element->setCheckedValue('y');
    $element->setUncheckedValue('n');
    $element->setLabel('Zobrazit historii');
    $this->addElement($element);

    $element = new Zend_Form_Element_Submit('Hledat');
    $this->addElement($element);

    parent::init();
  }
}
