<?php
/**
 * Tento formular je pouzit i v multiformu
 */
class Producers_DeliveryProductionAddForm extends Meduse_FormBootstrap
{

    public function init()
    {        
        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('post');

        $element = new Meduse_Form_Element_DatePicker('date_delivery', array(
			'jQueryParams'	=> array(
				'dateFormat' => 'dd.mm.yy',
				'firstDay' => 1
			),
			'label'			=> 'Datum dodání',
			'required'		=> true,
			'value'			=> date("d.m.Y"),
			'autocomplete'	=> 'off'
		));
		$this->addElement($element);
        
        $element = new Meduse_Form_Element_Select('status');
		$element->setLabel("Stav výroby po naskladnění");
		$element->addMultiOptions(array(
			'open' => "Otevřená - pokračuje",
			'closed' => "Uzavřená"
		));
		$this->addElement($element);

        $element = new Zend_Form_Element_Text('delivery_description');
        $element->setRequired(false);
        $element->setLabel('Poznámka');
        $this->addElement($element);        
        
        $element = new Zend_Form_Element_Submit('submit');
        $element->setLabel('Uložit');
        $this->addElement($element);
        
        parent::init();
    }

}
