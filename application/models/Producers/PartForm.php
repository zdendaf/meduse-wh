<?php
class Producers_PartForm extends Meduse_FormBootstrap {
	public function init ($options = null){

    $this->setName('parts_box');
    $this->setLegend('Úprava součásti');
    $this->setAttrib('class', 'form');
        
		$element = new Meduse_Form_Element_Text('p_part_id');
		$element->setLabel('Id součásti dodavatele');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('p_part_name');
		$element->setLabel('Jméno součásti dodavatele');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Text('p_part_min_amount');
		$element->setLabel('Minimální počet k objednání');
		$this->addElement($element);
		
    $element = new Meduse_Form_Element_Currency('p_part_price');
		$element->setLabel('Cena dodavatele');
		$this->addElement($element);

		$element = new Zend_Form_Element_Hidden('id');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);
    
     parent::init($options);
	}
}
