<?php
class Producers_SearchForm extends Meduse_FormBootstrap {
	public function init() {
        //parent::__construct('parts_box', 'Dodavatelé', $options);
		$this->setAttrib('class', 'form');
		$this->setMethod('get');
		$this->setLegend('Dodavatelé');

		$element = new Meduse_Form_Element_Text('name');
		$element->setLabel('Jméno firmy');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('category');
		$element->addMultiOption(NULL, '-- vybrat --');
		$element->addMultiOptions(Zend_Registry::get('db')
            ->fetchPairs('SELECT id,name FROM producers_ctg WHERE id NOT IN ( 21, 22 )')); //vse krome dopravcu
		$element->setLabel('Kategorie');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Hledat');
		$this->addElement($element);

		parent::init();
	}
}
