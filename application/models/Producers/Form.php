<?php

class Producers_Form extends Meduse_FormBootstrap
{

    public function init()
    {        
        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('post');

        $element = new Zend_Form_Element_Checkbox('meduse_producer_is_active');
        $element->setLabel('Status');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('meduse_producer_name');
        $element->setRequired(true);
        $element->addValidator(new Meduse_Validate_NotEmpty());
        $element->setLabel('Jméno firmy');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('meduse_producer_ident_no');
        $element->addValidator(new Zend_Validate_StringLength(0, 100));
        $element->setLabel('Identifikační číslo');
        $this->addElement($element);

        $element = new Meduse_Form_Element_SelectChosen('meduse_producer_warehouse');
        $element->addMultiOption(NULL, '- subjekt není zpracovatelem -');
        $element->addMultiOptions(Zend_Registry::get('db')->fetchPairs('SELECT id,name FROM extern_warehouses e WHERE NOT EXISTS (SELECT * FROM producers WHERE id_extern_warehouses = e.id)'));
        $element->setLabel('Externí sklad');
        $this->addElement($element);

        $element = new Meduse_Form_Element_SelectChosen('meduse_producer_category');
        $element->setRequired(true);
        $element->addMultiOption(NULL, '-- vybrat --');
        $element->addMultiOptions(Zend_Registry::get('db')->fetchPairs('SELECT id,name FROM producers_ctg ORDER BY name'));
        $element->setLabel('Kategorie');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('meduse_producer_address');
        $element->setLabel('Adresa');
        $this->addElement($element);

//		$element = new Zend_Form_Element_Text('meduse_producer_city');
//		$element->setLabel('Město');
//		$this->addElement($element);
//
//		$element = new Zend_Form_Element_Text('meduse_producer_street');
//		$element->setLabel('Ulice');
//		$this->addElement($element);
//
//		$element = new Zend_Form_Element_Text('meduse_producer_zip');
//		$element->setLabel('PSČ');
//		$this->addElement($element);

        $element = new Zend_Form_Element_Text('meduse_producer_www');
        $element->setLabel('www');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('meduse_producer_desc');
        $element->setLabel('Poznámka k firmě');
        $this->addElement($element);
        
        $element = new Zend_Form_Element_Text('meduse_producer_activity');
        $element->setLabel('Činnost firmy');
        $this->addElement($element);        
        
        $element = new Zend_Form_Element_Submit('Odeslat');
        $this->addElement($element);
        
        parent::init();
    }

}
