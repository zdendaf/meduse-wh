<?php

class Producers_PersonForm extends Meduse_FormBootstrap {

  public function init() {
    /**
     * Create Elements
     */
    $this->setAttribs(['class' => 'form'])->setMethod('post');

    $element = new Meduse_Form_Element_Text('meduse_producer_deputy');
    $element->setLabel('Kontaktní osoba');
    $element->addValidator(new Zend_Validate_StringLength(0, 60));
    $element->setRequired(TRUE);
    $element->setAttrib('class', 'span4');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Phone('meduse_producer_phone');
    $element->setLabel('Mobil');
    $element->addValidator(new Zend_Validate_StringLength(0, 20));
    $element->setAttrib('class', 'span4');
    $element->setAttrib('maxlength', 20);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Phone('meduse_producer_phone2');
    $element->setLabel('Telefon');
    $element->addValidator(new Zend_Validate_StringLength(0, 20));
    $element->setAttrib('class', 'span4');
    $element->setAttrib('maxlength', 20);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_producer_email');
    $element->setLabel('E-mail');
    $element->addValidator(new Zend_Validate_EmailAddress());
    $element->addValidator(new Zend_Validate_StringLength(0, 50));
    $element->setAttrib('class', 'span4');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_producer_position');
    $element->setLabel('Pozice kont. osoby');
    $element->setAttrib('class', 'span4');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Select('meduse_producer_position_select');
    $element->setLabel('Pozice kont. osoby');
    $element->addMultiOptions([
      '' => '- - -',
      'Obchodník' => 'Obchodník',
      'Technolog' => 'Technolog',
      'Ředitel' => 'Ředitel',
      'Vedoucí výroby' => 'Vedoucí výroby',
      'Účetní' => 'Účetní',
    ]);
    $this->addElement($element);
    $element->setAttrib('class', 'span4');

    $element = new Meduse_Form_Element_Submit('Odeslat');
    $this->addElement($element);

    parent::init();
  }

}
