<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Producers_Producer {

    protected $_tProcucers = NULL;
    protected $_rProducers = NULL;
    protected $_tPersons = NULL;
    protected $_rPersons = array();

    /**
     * Konstruktor objektu producenta
     */
    public function __construct($id = NULL) {
      $this->_tProcucers = new Table_Producers();
      $this->_rProducers = $this->_tProcucers->createRow();
      $this->_tPersons = new Table_ProducersPersons();
      if ($id) {
        $this->init($id);
      }
    }

    /**
     * Inicializace/naplneni producenta daty z databaze
     */
    public function init($id) {
      $this->_rProducers = $this->_tProcucers->find($id)->current();
      $this->_rPersons = $this->_tPersons
        ->select()->where('id_producers = ?', $this->_rProducers->id)
        ->query(Zend_Db::FETCH_OBJ)->fetchAll();
    }

    /**
     * Vytvoreni noveho producenta
     */
    public function create(array $data) {
      if ($this->_rProducers->id) {
        throw new Producers_Exceptions_AlreadyExists();
      }
      $id = $this->_tProcucers->add($data);
      $this->init($id);
    }

    /**
     * Pridani kontaktni osoby k producentovi
     */
    public function addPerson(array $data) {
      $data['id_producers'] = $this->_rProducers->id;
			$this->_tPersons->add($data);
      $this->init($this->_rProducers->id);
    }

    /**
     * Vrati ID producenta
     */
    public function getId() {
      return $this->_rProducers->id;
    }

    /**
     * Vrati nazev producenta
     */
    public function getName() {
      return $this->_rProducers->name;
    }

    public function getIdentNo() {
      return $this->_rProducers->ident_no;
    }

    /**
     * Vrati adresu producenta
     */
    public function getAddress() {
      return $this->_rProducers->address;
    }

    /**
     * Vrati web producenta
     */
    public function getWeb() {
      return $this->_rProducers->www;
    }

    /**
     * Vrati poznamku producenta
     */
    public function getDescription() {
      return $this->_rProducers->description;
    }

    /**
     * Je producent aktivni?
     */
    public function isActive() {
      return ($this->_rProducers->is_active == 'y');
    }

    public function setActive($active = true, $save = true) {
      $this->_rProducers->is_active = $active ? 'y' : 'n';
      if ($save) {
        $this->_rProducers->save();
      }
    }

    public function getPersons() {
      return $this->_rPersons;
    }
  }
