<?php
class Producers_DeliveryProductionForm extends Meduse_Form {

	public function __construct ($options = null){
        parent::__construct('parts_box', "Naskladnit výrobu");

		$element = new Meduse_Form_Element_DatePicker(
			'meduse_production_end',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->setRequired(true);
		$element->setLabel('Datum dodání');
		$element->setValue(date("d.m.Y"));
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_production_amount');
		$element->setLabel('Dodaný počet');
		$element->setRequired(true);
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('meduse_production_status');
		$element->addMultiOption('closed', "Výroba ukončena");
		$element->addMultiOption('partial', "Částečně dodáno");
		$element->setLabel('Výsledek dodání');
		$element->setRequired(true);
		$this->addElement($element);

		$element = new Zend_Form_Element_Hidden('id');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$element->setValue('Odeslat');
		$this->addElement($element);
	}
}
