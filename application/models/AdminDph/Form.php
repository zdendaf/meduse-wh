<?php
class AdminDph_Form extends Meduse_FormBootstrap {

    public function init()
    {
        $this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			 ->setMethod(Zend_Form::METHOD_POST);

        $element = new Meduse_Form_Element_Select('code_c_country');
        $element->setRequired(true);
        $element->setLabel('Země');
        $select = 'SELECT code, CONCAT(name,\' (\',code,\')\') FROM c_country WHERE region = \'eu\' ORDER BY name';
        $options = Zend_Registry::get('db')->fetchPairs($select);
        $element->addMultiOptions($options);
        $element->setValue('CZ');
        $element->setAttrib('class', 'span3');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('dph');
        $element->setRequired(true);
        $element->setLabel('Hodnota v %');
        $element->setAttrib('class', 'span1');
        $this->addElement($element);

        //$element = new Meduse_Form_Element_Text('active_from_date');
        $element = new Meduse_Form_Element_DatePicker('active_from_date',
                array('jQueryParams' => array(
                    'dateFormat' => 'dd.mm.yy'
            )));
        $element->setRequired(true);
        $element->setDescription('(dd.mm.yyyy)');
        $element->setLabel('Aktivní od');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Submit('Odeslat');
        $this->addElement($element);
        
        parent::init();
    }

}
