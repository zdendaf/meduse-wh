<?php

class Issues_CommentForm extends Zend_Form {

	public function init() {		
		$this->setDescription('Vložit nový komentář')
				->setAttrib('class', 'form')
				->setMethod(Zend_Form::METHOD_POST);
		
		$element = new Zend_Form_Element_Hidden('id_issues');
		$this->addElement($element);

		$authNamespace = new Zend_Session_Namespace('Zend_Auth');
		$element = new Zend_Form_Element_Hidden('id_users');
		$element->setValue(intval($authNamespace->user_id));
		$this->addElement($element);
		
		$element = new Zend_Form_Element_Textarea('body');
		$element->setRequired(true);
		$element->setAttribs(array(
			'class' => 'span7',
			'placeholder' => 'Zadejte text komentáře...',
			'rows' => '5'
		));
		$this->addElement($element);
		
		$element = new Zend_Form_Element_Submit('Odeslat');
		$element->setAttribs(array(
			'class' => 'btn btn-primary'
		));
		$this->addElement($element);
		
		
		parent::init();
	}
	
}

