<?php

class Issues_StatusForm extends Zend_Form {
	
	public function init() {
		
		$this->setMethod(Zend_Form::METHOD_POST)
				->setDescription('Změna stavu')
				->setAttrib('class', 'form');

		$element = new Zend_Form_Element_Hidden('id_issues');
		$this->addElement($element);		
		
		$element = new Zend_Form_Element_Select('status');
		$element->addMultiOptions(Table_Issues::$statusToString);
		$this->addElement($element);
		
		$element = new Zend_Form_Element_Submit('Změnit');
		$element->setValue('Změnit stav');
		$element->setAttrib('class', 'btn btn-primary');
		$this->addElement($element);
		
		parent::init();
	}

	
	
	
	
}

