<?php

class Documents_Document
{

  /*
  protected int $id;
  protected string $title;
  protected string $description;
  */

  protected Table_Documents $_documentsTable;
  protected Table_DocumentsFiles $_filesTable;

  protected Zend_Db_Table_Row_Abstract $_row;
  protected array $files;

  public function __construct($id = NULL)
  {
    $this->_documentsTable = new Table_Documents();
    $this->_filesTable = new Table_DocumentsFiles();
    if ($id) {
      try {
        if (is_null($this->_row = $this->_documentsTable->find($id)->current())) {
          throw new RuntimeException('Document not found.');
        }
        $this->files = $this->_filesTable->select()->setIntegrityCheck(FALSE)
          ->from(['f' => $this->_filesTable->info('name')])
          ->join(['u' => 'users'], 'u.id = f.id_users', ['user' => 'name', 'user_name' => 'name_full'])
          ->where('id_documents = ?', $this->_row->id)
          ->order('id DESC')
          ->query()->fetchAll();
      }
      catch (Zend_Db_Table_Exception $e) {
        Utils::logger()->err($e->getMessage());
        return NULL;
      }
    }
    else {
      $this->_row = $this->_documentsTable->createRow();
      $this->_row->id_users = Utils::getCurrentUserId();
    }
  }

  public function getId(): ?int
  {
    return $this->_row->id ? (int) $this->_row->id : NULL;
  }

  public function getWh(): int {
      return (int) $this->_row->wh;
  }

    public function setWh(int $wh, $save = TRUE): Documents_Document
    {
        $this->_row->wh = $wh;
        if ($save) {
            $this->_row->save();
        }
        return $this;
    }

  public function setTitle(string $title, $save = TRUE): Documents_Document
  {
    $this->_row->title = $title;
    if ($save) {
      $this->_row->save();
    }
    return $this;
  }

  public function setDeleted(bool $deleted, $save = TRUE): Documents_Document
  {
    $this->_row->deleted = $deleted ? 1 : 0;
    if ($save) {
      $this->_row->save();
    }
    return $this;
  }

  public function getTitle(): string
  {
    return $this->_row->title;
  }

  public function setDescription(string $description, $save = TRUE): Documents_Document
  {
    $this->_row->description = $description;
    if ($save) {
      $this->_row->save();
    }
    return $this;
  }

  public function getDescription(): ?string
  {
    return $this->_row->description;
  }

  public function save() {
    $this->_row->save();
  }

  public function uploadFile($fid = NULL) {

    $errors = [];
    $received = [];

    /** @var Zend_File_Transfer_Adapter_Http $transfer */
    $transfer = new Zend_File_Transfer();
    $files = $transfer->getFileInfo();

    foreach ($files as $file => $info) {
      if (!$transfer->isUploaded($file)) {
        $errors[] = 'Nebyl nahrán žádný soubor.';
        continue;
      }
      if (!$transfer->isValid($file)) {
        $errors[] = 'Soubor <em>' . $file . '</em> není přípustný.';
        continue;
      }
      $dir = Documents::generatePath($this->getId(), $fid, TRUE);
      $filename =  $dir . '/' . $info['name'];
      try {
        $transfer->addFilter('Rename', ['target' => $filename, 'overwrite' => TRUE], $file);
        if ($transfer->receive()) {
          $received[] = $info['name'];
        }
        else {
          $errors[] = 'Soubor \'' . $info['name'] . '\' se nepodařilo přidat.';
        }

      }
      catch (Zend_File_Transfer_Exception $e) {
        $errors[] = $e->getMessage();
      }
    }

    if (!$received) {
      $errors[] = 'Soubor nebyl uploadován';
    }
    else {
      foreach ($received as $filename) {
        if ($fid) {
          $this->_filesTable->update([
              'id_users' => Utils::getCurrentUserId(),
              'filename' => $filename
              ], 'id = ' . $this->_filesTable->getAdapter()->quote($fid));
        }
        else {
          $this->_filesTable->insert([
            'id_documents' => $this->getId(),
            'id_users' => Utils::getCurrentUserId(),
            'filename' => $filename,
          ]);
        }
      }
    }

    return empty($errors) ? TRUE : $errors;
  }

  public function getLastVersion(): int
  {
    $select = $this->_filesTable->select()
      ->from($this->_filesTable, [new Zend_Db_Expr('MAX(id)')])
      ->where('id_documents = ?', $this->getId());
    return (int) $select->query()->fetchColumn();
  }

  public function getFiles(): array
  {
    return $this->files;
  }

  public function getEditForm($title = NULL): Meduse_FormBootstrap
  {
    $form = Documents::getForm($title ?? 'Editace infa o dokumentu', $this->getWh());
    $form->getElement('id')->setValue($this->getId());
    $form->removeElement('file');
    $form->populate([
      'title' => $this->getTitle(),
      'description' => $this->getDescription(),
    ]);
    return $form;
  }

  function getUserId(): int
  {
    return (int) $this->_row->id_users;
  }

  function getUserName() {
    $table = new Table_Users();
    $row = $table->find($this->getUserId())->current()->toArray();
    return $row['name_full'];
  }

  public function getAddVersionForm($title = NULL, int $wh = Table_PartsWarehouse::WH_TYPE): Meduse_FormBootstrap
  {
    $form = Documents::getForm($title ?? 'Vložení nové verze', $wh);
    $form->getElement('id')->setValue($this->getId());
    $form->removeElement('title');
    $form->removeElement('description');
    return $form;
  }
}