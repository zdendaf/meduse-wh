<?php

class Documents_Form extends Meduse_FormBootstrap
{

    public const ALLOWED_EXTENSIONS = [
        'pdf',
        'odt', 'doc', 'docx',
        'ods', 'xls', 'xlsx',
        'txt',
        'jpg', 'jpeg', 'png',
    ];
    public const ALLOWED_MAX_SIZE = 5_242_880;

    public function init(): void
    {
        try {
            $this->setAttrib('class', 'form');

            $element = new Zend_Form_Element_Hidden('id');
            $this->addElement($element);

            $element = new Zend_Form_Element_Hidden('wh');
            $element->setValue(Table_PartsWarehouse::WH_TYPE);
            $this->addElement($element);

            $element = new Meduse_Form_Element_Text('title');
            $element->setLabel('Název');
            $element->setAttrib('maxlength', 50);
            $element->setRequired();
            $this->addElement($element);

            $element = new Meduse_Form_Element_File('file');
            $element->setLabel('Soubor');
            $element->setRequired();
            $element->addValidator(new Zend_Validate_File_Count(['min' => 1, 'max' => 1]));
            $element->setMultiFile(1);
            $element->addValidator(new Zend_Validate_File_Size(['max' => self::ALLOWED_MAX_SIZE]));
            $element->addValidator(new Zend_Validate_File_Extension(self::ALLOWED_EXTENSIONS));
            $this->addElement($element);

            $element = new Meduse_Form_Element_Textarea('description');
            $element->setLabel('Popis');
            $element->setAttribs(['maxlength' => 255, 'rows' => 3]);
            $this->addElement($element);

            $element = new Meduse_Form_Element_Submit('save');
            $element->setLabel('Uložit');
            $this->addElement($element);
        } catch (Zend_Form_Exception | Zend_Validate_Exception $e) {
            Utils::logger()->err('Unable to set up the form. ' . $e->getMessage());
        }

        parent::init();
    }

}