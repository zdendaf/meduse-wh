<?php
class Table_UsersIp extends Zend_Db_Table_Abstract {

	protected $_name = 'users_allowed_ip';

	protected $_primary = 'id';
    
    public function getUserByHash($hash) {
        $row = $this->fetchRow("hash = ".$this->getAdapter()->quote($hash));
        if($row) {
            $tUsers = new Table_Users();
            $rUser = $tUsers->find($row->id_users)->current();
            if($rUser) {
                return $rUser;
            }
        }
        return null;
    }
    
}
