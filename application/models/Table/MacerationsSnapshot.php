<?php

class Table_MacerationsSnapshot extends Zend_Db_Table_Abstract {

	protected $_name = 'macerations_snapshot';
	protected $_primary = 'id';

  public function setSnapshot($id_macerations, $amount_wh, $amount_real, $date, $id_users, $note = NULL, $isFixed = FALSE, $id_snapshot = NULL) {
    if ($id_snapshot) {
      $this->update([
        'id_macerations' => $id_macerations,
        'id_users' => $id_users,
        'amount_wh' => $amount_wh,
        'amount_real' => $amount_real,
        'date' => $date,
        'note' => $note,
        'is_fixed' => $isFixed ? 'y' : 'n',
      ], 'id = ' . $id_snapshot);
    }
    else {
      $id_snapshot = $this->insert([
        'id_macerations' => $id_macerations,
        'id_users' => $id_users,
        'amount_wh' => $amount_wh,
        'amount_real' => $amount_real,
        'date' => $date,
        'note' => $note,
        'is_fixed' => $isFixed ? 'y' : 'n',
      ]);
    }
    return $id_snapshot;
  }

}


