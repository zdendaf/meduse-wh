<?php
class Table_ProductsTypes extends Zend_Db_Table_Abstract{
	
	protected $_name = 'products_types';
	
	protected $_primary = 'id';
	
	public function getTypes(){
		$rows = $this->fetchAll();
		$data = array();
		foreach($rows as $row){
			$data[$row->id] = array('name' => $row->type_name, 'description' => $row->description);
		}
		return $data;
	}
	
}
