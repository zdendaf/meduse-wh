<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Table_InvoiceVersions extends Zend_Db_Table_Abstract {
    protected $_name = 'invoice_versions';
    protected $_primary = 'id';
    
    public function getVersions($invoiceId) {
      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('v' => $this->_name), array('id', 'created', 'filename'))
        ->joinLeft(array('u' => 'users'), 'u.id = v.id_users', array('username', 'name', 'name_full'))
        ->where('v.id_invoice = ?', $invoiceId)
        ->order('v.created ASC');
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }
  }

  