<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Table_EmployeesWorks extends Zend_Db_Table_Abstract {

    protected $_name = 'employees_works';
    protected $_primary = 'id';


    public function save(array $record) {

      $id = $record['id'];
      $column = $record['column'];
      $value = empty($record['value']) ? NULL : $record['value'];

      // zaznam uz v databazi je, updatujeme
      if ($id) {

        // kontrola, zda-li je pocatecni start mensi nez end
        if (($column == 'start' || $column == 'end') && !is_null($value)) {
          $timestamp = new Meduse_Date($record['date'] . ' ' . $value);
          $row = $this->find($id)->current()->toArray();

          // ukladame end
          if ($record['column'] == 'end') {
            if ($row['start']) {
              $start = new Meduse_Date($row['start']);
              if ($timestamp <= $start) {
                // zvysime hodnotu o den
                $timestamp->addDay(1);
              }
            }
            // Musime se podivat i do zaznamů ze čtečky
            else {
              $day = new Meduse_Date($record['date']);
              $tRec = new Table_EmployeesRecords();
              $records = $tRec->getRecords($record['id_employees'], $day, TRUE);
              if ($records && $records['start']) {
                $start = new Meduse_Date($records['start']);
                if ($timestamp <= $start) {
                  // zvysime hodnotu o den
                  $timestamp->addDay(1);
                }
              }
            }
          }

          // ukladame start
          else {
            if ($row['end']) {
              $end = new Meduse_Date($row['start']);
              if ($timestamp > $end) {
                $end->addDay(1);
                // musime do end pridat den a zapsat do databaze
                $this->update(array('end' => $end->toString('Y-m-d H:i:s')), 'id = ' . $id);
              }
            }
          }
          $value = $timestamp->toString('Y-m-d H:i:s');
        }

        if ($column == 'lunch') {
          $row = $this->find($id)->current()->toArray();
          if ($row['start'] && $row['end']) {
            $start = new Meduse_Date($row['start']);
            $end = new Meduse_Date($row['end']);
            $diff = $end->getTimestamp() - $start->getTimestamp();
            if ($diff >= 3600 * Employees_WorkSheet::LUNCH_LIMIT) {
              $value = 'y';
            }
          }
          else {
            $value = 'n';
          }
        }

        // samotny update
        $this->update(array(
          $column => $value,
          'id_users' => $record['id_users']
        ), 'id = ' . $id);

        // vymaz prazdnych radku
        if (is_null($value)) {
          $this->delete(array("`type` IS NULL AND `start` IS NULL AND `end` IS NULL AND `description` IS NULL AND `km` IS NULL"));
          return NULL;
        }
      }

      // zaznam v databazi neni, vkladame
      else {
        if ($column == 'start' || $column == 'end') {
          $value = $record['date'] . ' ' . $value;
        }
        $id = $this->insert(array(
          'id_employees' => $record['id_employees'],
          'id_users' => $record['id_users'],
          'date' => $record['date'],
          $column => $value,
        ));
      }

      // nakonec vratime cely radek
      return $this->find($id)->current()->toArray();
    }
  }
