<?php
//class Table_WrapsWarehouse extends Zend_Db_Table_Abstract{
//
//	protected $_name = 'wraps_warehouse';
//
//	protected $_primary = 'id';
//
//	public function getWarehouseStatus(){
//		$select = $this->select();
//		$select->setIntegrityCheck(false)
//		->from('wraps_warehouse')
//		->joinLeft(array('w' => 'wraps'), 'id_wraps = w.id')
//		->joinLeft(array('wc' => 'wraps_ctg'), "w.id_wraps_ctg = wc.id", array('ctg_name'))
//		->order(array('id_wraps_ctg', 'id_wraps'));
//		$rows = $this->fetchAll($select);
//		$data = array();
//		foreach($rows as $row){
//			$data[$row->ctg_name][] = array(
//				'id' => $row->id_wraps,
//				'name' => $row->name,
//				'description' => $row->description,
//				'amount' => $row->amount
//			);
//		}
//		return $data;
//	}
//
//	public function getWarehouseStatusQuery(){
//		$select = $this->select();
//		$select->setIntegrityCheck(false)
//		->from('wraps_warehouse', array('amount'))
//		->joinLeft(array('w' => 'wraps'), 'id_wraps = w.id', array('id', 'id_wraps_ctg', 'name', 'description'))
//		->joinLeft(array('wc' => 'wraps_ctg'), "w.id_wraps_ctg = wc.id", array('ctg_name'))
//		->order(array('id_wraps_ctg', 'id_wraps'));
//		return $select;
//	}
//
//	public function addPart($id, $amount){
//		$new_amount = (int) $amount + $this->getAmount($id);
//		$this->update(array("amount" => $new_amount), "id_wraps = '".$id."'");
//		$authNamespace = new Zend_Session_Namespace('Zend_Auth');
//		$this->getAdapter()->insert('wraps_warehouse_history', array(
//			'id_wraps' => $id,
//			'id_users' => $authNamespace->user_id,
//			'amount' => ($amount)
//		));
//	}
//
//	public function removeWrap($id, $amount){
//		$wh_amount = $this->getAmount($id);
//		if($wh_amount < $amount){
//			throw new Parts_Exceptions_LowAmount("Objednávku nelze expedovat. Na skladu chybí ".($amount - $wh_amount)." ks obalu $id");
//		} else {
//			$data = array(
//				'amount' => ($wh_amount - $amount)
//			);
//			$this->update($data, "id_wraps = ".$this->getAdapter()->quote($id));
//			$authNamespace = new Zend_Session_Namespace('Zend_Auth');
//			$this->getAdapter()->insert('wraps_warehouse_history', array(
//				'id_wraps' => $id,
//				'id_users' => $authNamespace->user_id,
//				'amount' => ($amount*-1)
//			));
//		}
//
//	}
//
//	public function getAmount($id){
//		$select = $this->select();
//		$select->where("id_wraps = ?", $id);
//		$row = $select->query()->fetch();
//		return (int) $row['amount'];
//	}
//
//}
