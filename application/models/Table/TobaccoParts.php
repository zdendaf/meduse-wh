<?php

  class Table_TobaccoParts extends Table_Parts {

    public function getProducts($params = array(), $asSelect = FALSE) {

      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('p' => 'parts'));
      $select->joinLeft(array('pc' => 'parts_ctg'),
        "p.id_parts_ctg = pc.id", array('ctg_name' => 'name'));
      $select->where("p.deleted = ?", 'n');
      $select->where("p.product = ?", 'y');
      $select->where("p.id_wh_type = ?", Table_TobaccoWarehouse::WH_TYPE);
      $select->order(array('pc.ctg_order ASC', 'p.id'));

      $name = isset($params['name'])? $params['name'] : null;
      if(!is_null($name) && $name != 'null') {
        $select->where('p.name LIKE ?', '%'. $name . '%');
      }

      $id = isset($params['id'])? $params['id'] : null;
      if (!is_null($id) && $id != 'null'){
        $select->where('p.id LIKE ?', '%'. $id . '%');
      }

      $fs_only = isset($params['fs_only'])? $params['fs_only'] : FALSE;
      if ($fs_only) {
        $select->where('pc.id <> ?', Table_TobaccoWarehouse::CTG_TOBACCO_NFS);
      }

      $other = isset($params['other']) ? $params['other'] : null;
      if ($other) {
        if (!is_array($other)) {
          $other = array($other);
        }
        foreach ($other as $where) {
          $select->where($where);
        }
      }
      if ($asSelect) {
        return $select;
      }
      else {
        $data = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
        return $data;
      }
    }

  }
