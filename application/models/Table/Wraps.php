<?php
//class Table_Wraps extends Zend_Db_Table_Abstract{
//
//	protected $_name = 'wraps';
//
//	protected $_primary = 'id';
//
//	public function getDatalist(){
//
//		$select = $this->select();
//		$select->setIntegrityCheck(false);
//		$select->from(array('p' => 'parts'), array(
//			'id',
//			'name',
//			'description'
//		))->joinLeft(
//			array('c' => 'collections'),
//			"p.id_collections = c.id",
//			array('collection_name'=>'name')
//		)->joinLeft(
//			array('pt' => 'products_types'),
//			"p.id_products_types = pt.id",
//			array('type_name')
//		)->where(
//			"p.id_parts_ctg = 22"
//		)->order(
//			array("p.id_collections", "p.id_products_types")
//		);
//
//		$rows = $this->fetchAll($select);
//
//		foreach($rows as $row){
//
//			if(empty($row->collection_name) && empty($row->type_name)){
//				$group = 'Ostatní obalový materiál';
//			} else {
//				$group = $row->collection_name.' '.$row->type_name;
//			}
//			$data[$group][] = array(
//				'id' => $row->id,
//				'name' => $row->name,
//				'description' => $row->description,
//				'ctg_name' => $group,
//				'type' => $row->collection_name.' '.$row->type_name
//			);
//		}
//		return $data;
//
//	}
//
//	public function getFiltredDatalist(){
//		$query = "
//		SELECT
//			w.id_wraps_ctg,
//			w.name,
//			pw.id_products,
//			products.id_collections as prod_coll,
//			products.id_products_types as prod_type,
//			pw.id as id_products_wraps,
//			pw.id_wraps,
//			pwp.amount,pwp.id,
//			parts.id_parts_ctg as part_ctg,
//			pwp.is_set,
//			parts.id_collections as part_coll
//		FROM `wraps` AS `w`
//		LEFT JOIN products_wraps AS pw ON w.id = pw.id_wraps
//		LEFT JOIN products ON pw.id_products = products.id
//		LEFT JOIN (
//			SELECT id_products_wraps, id_products AS id, amount, 1 AS is_set
//			FROM `products_wraps_sets`
//			UNION
//			SELECT id_products_wraps, id_parts AS id, amount, NULL AS is_set
//			FROM products_wraps_parts
//		) as pwp ON pw.id = pwp.id_products_wraps
//		LEFT JOIN parts ON pwp.id = parts.id
//		WHERE id_wraps_ctg =1 AND pw.id_products = 'C03'
//		";
//
//		$sub_select = $this->select();
//		$sub_select->setIntegrityCheck(false);
//		$sub_select->from("products_wraps_sets", array('id_products_wraps', 'id' => 'id_products', 'part_amount' => 'amount', 'is_set' => '(1)'));
//
//		$sub_select2 = $this->select();
//		$sub_select2->setIntegrityCheck(false);
//		$sub_select2->from("products_wraps_parts", array('id_products_wraps', 'id' => 'id_parts', 'part_amount' => 'amount', 'is_set' => new Zend_Db_Expr('NULL')));
//
//		$db = Zend_Registry::get('db');
//		$union = $db->select()
//		->union(array($sub_select, $sub_select2));
//
//		$select = $this->select();
//		$select->setIntegrityCheck(false);
//		$select->from(array('w' => 'wraps'), array('id_wraps_ctg', 'name'))
//		->joinLeft(array('pw' => 'products_wraps'), "w.id = pw.id_wraps", array('id_products', 'id_products_wraps' => 'id', 'id_wraps', 'wrap_amount' => '(1)'))
//		->joinLeft(array('p' => 'products'), "p.id = pw.id_products", array('prod_coll' => 'id_collections', 'prod_type' => 'id_products_types'))
//		//->joinLeft(array('pwp' => $union), "pw.id = pwp.id_products_wraps", array('part_amount', 'id', 'is_set'))
//		//->joinLeft("parts", "pwp.id = parts.id", array('part_ctg' => 'id_parts_ctg', 'part_coll' => 'id_collections'))
//		;
//
//		$select->where("p.id = ?", "C03");
//
//		echo "<pre>".$select; die();
//	}
//
//	/**
//	 * Vraci select
//	 * @param int $id_orders
//	 * @param int $id_wraps_ctg
//	 * @return string select query
//	 */
//	public function getOrdersWraps($id_orders, $id_wraps_ctg){ //(array $id_orders, $id_prod_col, $id_prod_ctg, $id_wrap_ctg){
//
//
//
//		$query = "
//		SELECT id_wraps, SUM( IFNULL(amount, 0) * wrap_amount )
//		FROM (
//			SELECT `id_products` , `id_wraps` , sum( `wrap_amount` ) AS wrap_amount
//			FROM `products_wraps`
//			GROUP BY `id_products` , `id_wraps`
//		) AS t1
//		LEFT JOIN (
//			SELECT `id_products` , SUM( `amount` ) AS amount
//			FROM `orders_products`
//			WHERE id_orders
//			IN (
//				SELECT id
//				FROM orders
//				WHERE STATUS IN (
//				'new', 'open'
//				)
//			)
//			GROUP BY `id_products`
//		) AS t2 ON t1.id_products = t2.id_products
//		GROUP BY id_wraps
//		";
//
//		//$id_orders = array(19,27,28,36,37,38,39,41,42,45,46);
//		//$id_orders = array(19,27,28,36);
//
//		if($id_orders === null || count($id_orders) < 1 || $id_orders == ''){
//			return null;
//		}
//
//		// pocet obalu v produktu
//		$sub_select1 = $this->select();
//		$sub_select1->setIntegrityCheck(false);
//		$sub_select1->from('products_wraps', array('id_products', 'id_wraps', 'wrap_amount' => "sum(wrap_amount)"))
//		->group(array('id_products', 'id_wraps'));
//
//		// pocet produktu v objednavkach
//		$sub_select2 = $this->select();
//		$sub_select2->setIntegrityCheck(false);
//		$sub_select2->from('orders_products', array('id_products', 'amount' => "sum(amount)"))
//		->where("id_orders IN (?)", $id_orders)
//		->group('id_products');
//
//		// pocet extra obalu
//		$extra_select = $this->select();
//		$extra_select->setIntegrityCheck(false);
//		$extra_select->from(
//			array('ow' => 'orders_wraps'),
//			array('id_wraps', 'amount' => "sum(amount)")
//		)->joinLeft(
//			array('w' => 'wraps'),
//			"ow.id_wraps = w.id",
//			array('name','id_wraps_ctg')
//		)->where("id_orders IN (?)", $id_orders)
//		->group('id_wraps');
//
//		$select = $this->select();
//		$select->setIntegrityCheck(false);
//		$select->from(array('t1' => $sub_select1), array('id_wraps'))
//		->joinLeft(array('t2' => $sub_select2), "t1.id_products = t2.id_products", array('amount' => "SUM( IFNULL(t2.amount, 0) * t1.wrap_amount )"))
//		->joinLeft(array('w' => 'wraps'), "w.id = t1.id_wraps", array("name","id_wraps_ctg"))
//		->group('t1.id_wraps')
//		;
//
//		$db = Zend_Registry::get('db');
//		$union = $db->select()
//		->union(array($extra_select, $select));
//
//		$top_select = $db->select()->from($union, array('id_wraps', 'amount' => 'SUM(amount)', 'id_wraps_ctg', 'name'))->group('id_wraps');
//
//
//		if($id_wraps_ctg != null && $id_wraps_ctg != ''){
//			$top_select->where("id_wraps_ctg = ?", $id_wraps_ctg);
//		}
//
//		return $top_select;
//
//		//echo '<pre>'.$select; die();
//
//
//
//
//
//
//		//$id_orders = array(19,27,28,36,37,38,39,41,42,45,46);
//
//		$select1 = $this->select();
//		$select1->setIntegrityCheck(false);
//		$select1->from("orders_products", array("id_products"))
//		->where("id_orders IN (?)", $id_orders);
//
//
//		$select = $this->select();
//		$select->setIntegrityCheck(false);
//		$select->from(array('w' => 'wraps'), array('id_wraps_ctg', 'name', 'wh_amount' => "(SELECT amount FROM wraps_warehouse WHERE id_wraps = w.id)"))
//		->joinLeft(array('pw' => 'products_wraps'), "w.id = pw.id_wraps", array('id_products', 'id_wraps', 'wrap_amount' => '(COUNT(id_wraps) * (SELECT SUM(amount) FROM orders_products WHERE id_products = pw.id_products AND id_orders IN (19,27,28,36,37,38,39,41,42,45,46)))'))
//		->joinLeft(array('p' => 'products'), "p.id = pw.id_products", array('prod_coll' => 'id_collections', 'prod_type' => 'id_products_types'))
//		->group(array('pw.id_wraps'));//'pw.id_products',
//
//		if(count($id_orders) > 0){
//			$select->where("pw.id_products IN ?", $select1);
//		}
//		/*
//		if($id_prod_col !== null){
//			$select->where("prod_coll = ?", $id_prod_col);
//		}
//
//		if($id_prod_ctg !== null){
//			$select->where("prod_type = ?", $id_prod_ctg);
//		}
//
//		if($id_wrap_ctg !== null){
//			$select->where("id_wraps_ctg = ?", $id_wrap_ctg);
//		}
//		*/
//		echo '<pre>'.$select; die();
//
//	}
//
//}
