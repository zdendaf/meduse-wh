<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Table_EmployeesRecords extends Zend_Db_Table_Abstract {

    protected $_name = 'employees_records';
    protected $_primary = 'id';

    const STATE_CHECKIN  = 'checkin';
    const STATE_BREAKIN  = 'breakin';
    const STATE_BREAKOUT = 'breakout';
    const STATE_CHECKOUT = 'checkout';

    const TYPE_WORK = 'work';
    const TYPE_BUSINESS_TRIP = 'bustrip';
    const TYPE_BUSINESS_TRIP_FOREIGN = 'bustrip_frgn';
    const TYPE_HOMEOFFICE = 'hoffice';
    const TYPE_OVERTIME_DRAW = 'overtime';
    const TYPE_VACATION = 'vacation';
    const TYPE_HALF_VACATION = 'halfvacation';
    const TYPE_SICKNESS = 'sickness';
    const TYPE_FMC = 'fmc';
    const TYPE_FUNERAL = 'funeral';
    const TYPE_UNPAID_VACATION = 'unpaid_vacation';
    const TYPE_HOLIDAY = 'holiday';

    const NOON = '13:00:00';

    public static $typeNames = array(
      self::TYPE_WORK => 'práce',
      self::TYPE_BUSINESS_TRIP => 'služ. cesta ČR/SK',
      self::TYPE_BUSINESS_TRIP_FOREIGN => 'služ. cesta ZAHR.',
      self::TYPE_HOMEOFFICE => 'homeoffice',
      self::TYPE_OVERTIME_DRAW => 'čerpání přesčasu',
      self::TYPE_VACATION => 'řádná dovolená',
      self::TYPE_HALF_VACATION => 'půlden dovolené',
      self::TYPE_SICKNESS => 'nemocenská',
      self::TYPE_FMC => 'OČR',
      self::TYPE_FUNERAL => 'pohřeb',
      self::TYPE_UNPAID_VACATION => 'neplacené volno',
    );

    public static $typeNames2 = array(
      self::TYPE_WORK => 'práce',
      self::TYPE_HOMEOFFICE => 'homeoffice',
      self::TYPE_VACATION => 'placené volno',
      self::TYPE_HALF_VACATION => 'půlden plac. volna',
      self::TYPE_UNPAID_VACATION => 'neplacené volno',
      self::TYPE_HOLIDAY => 'státní svátek',
    );

    public static $typeAbbrs = array(
      self::TYPE_WORK => 'PRAC',
      self::TYPE_BUSINESS_TRIP => 'SLUŽ',
      self::TYPE_BUSINESS_TRIP_FOREIGN => 'ZAHR',
      self::TYPE_HOMEOFFICE => 'HOME',
      self::TYPE_OVERTIME_DRAW => 'PŘES',
      self::TYPE_VACATION => 'DOVO',
      self::TYPE_HALF_VACATION => 'PULD',
      self::TYPE_SICKNESS => 'NEMO',
      self::TYPE_FMC => 'OČR',
      self::TYPE_FUNERAL => 'POHŘ',
      self::TYPE_UNPAID_VACATION => 'NEPL',
      self::TYPE_HOLIDAY => 'SVÁT',
    );

    public static function statesToString($idx = NULL) {
      $states = array(
        self::STATE_CHECKIN  => 'příchod',
        self::STATE_BREAKIN  => 'pauza',
        self::STATE_BREAKOUT => 'konec pauzy',
        self::STATE_CHECKOUT => 'odchod',
      );
      return is_null($idx)? $states : $states[$idx];
    }

    public function getStatus($idEmployee) {
      $select = $this->select();
      $select->from($this->_name, array('status'));
      $select->where('id_employees = ?', $idEmployee);
      $select->order('timestamp DESC')->limit(1);
      return $select->query()->fetchColumn();
    }

    public function getRecords($employeeId, Meduse_Date $date, $oneday = FALSE) {
      $oneday_str = NULL;
      $records = array();
      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from($this->_name, array(
            'date' => new Zend_Db_Expr('DATE(`timestamp`)'),
            'timestamp',
            'status' => 'status',
          ))
        ->where('id_employees = ?', $employeeId)
        ->where('timestamp IS NOT NULL');
      if ($oneday) {
        $oneday_str =  $date->toString('Y-m-d');
        $select->where('timestamp >= ?', $oneday_str);
      }
      else {
        $select->where('timestamp >= ?', $date->toString('Y-m-01'))
          ->where('timestamp < ?', $date->addMonth(1)->toString('Y-m-01'));
      }
      $select->group('date')->group('status')
        ->order('timestamp');
      $result = $select->query()->fetchAll();

      foreach ($result as $item) {
        $idx = $item['date'];
        if (!isset($records[$idx])) {
          $records[$idx] = array(
            'start' => NULL,
            'end' => NULL,
          );
        }
        if ($item['status'] == self::STATE_CHECKIN) {
          $records[$idx]['start'] = $item['timestamp'];
        }
        elseif ($item['status'] == self::STATE_CHECKOUT) {
          $records[$idx]['end'] = $item['timestamp'];
        }
      }
      if ($oneday) {
        return isset($records[$oneday_str]) ? $records[$oneday_str] : NULL;
      }
      else {
        return $records;
      }
    }

    public function preprocessRecords(&$records) {
      $workers = array();
      foreach ($records as $record) {
        $employee = $record['id_employees'];
        $day = substr($record['timestamp'], 0, 10);
        if (!isset($workers[$employee])) {
          $workers[$employee] = array();
        }
        if (!isset($workers[$employee][$day])) {
          $workers[$employee][$day] = array();
        }
        $workers[$employee][$day][] = $record['timestamp'];
      }
      foreach ($records as $idx => $record) {
        $status = NULL;
        $employee = $record['id_employees'];
        $day = substr($record['timestamp'], 0, 10);
        $count = count($workers[$employee][$day]);
        if ($count == 1) {
          $finded = $this->select()
            ->from('employees_records', array('status'))
            ->where('id_employees = ?', $record['id_employees'])
            ->where('timestamp LIKE ?', $day . '%')
            ->order('id DESC')
            ->limit(1)->query()->fetchColumn();
          switch($finded) {
            case self::STATE_CHECKIN:
              $status = self::STATE_CHECKOUT;
              break;
            case self::STATE_CHECKOUT:
              $status = NULL;
            default:
              $time = substr($record['timestamp'], 11, 8);
              if ($time < self::NOON) {
                $status = self::STATE_CHECKIN;
              }
              else {
                $status = self::STATE_CHECKOUT;
              }
              break;
          }
        }
        else {
          if ($workers[$employee][$day][0] == $record['timestamp']) {
            $status = self::STATE_CHECKIN;
          }
          elseif ($workers[$employee][$day][$count-1] == $record['timestamp']) {
            $status = self::STATE_CHECKOUT;
          }
        }
        if ($status) {
          $records[$idx]['status'] = $status;
        }
        else {
          unset($records[$idx]);
        }
      }
    }
  }

