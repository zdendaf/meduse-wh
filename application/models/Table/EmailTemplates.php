<?php

class Table_EmailTemplates extends Zend_Db_Table_Abstract {

	protected $_name = 'email_templates';
	protected $_primary = array('id');

	/**
	 * Vraci asociativni pole s id a subjectem sablon
	 * @return type array
	 */
	public function getList($wh = 1) {
		return $this->select()
					->from($this->_name, array('id' => 'id', 'name' => 'name'))
          ->where('id_wh_type = ?', $wh)
					->query(Zend_Db::FETCH_ASSOC)
					->fetchAll();
	}
}