<?php

  class Table_InvoiceCustomItems extends Zend_Db_Table_Abstract {

    protected $_name = 'invoice_custom_items';
    protected $_primary = 'id';

    const CUSTOM_ITEM_TYPE_ADDITIONAL = 'additional';
    const CUSTOM_ITEM_TYPE_PACKAGING  = 'packaging';
    const CUSTOM_ITEM_TYPE_DELIVERY   = 'delivery';
    const CUSTOM_ITEM_TYPE_SHIPPING   = 'shipping';
    const CUSTOM_ITEM_TYPE_DISCOUNT   = 'discount';

    public static $customItemTypeToString = array(
      self::CUSTOM_ITEM_TYPE_ADDITIONAL => 'dodatečná',
      self::CUSTOM_ITEM_TYPE_DELIVERY   => 'doručení',
      self::CUSTOM_ITEM_TYPE_PACKAGING  => 'balení',
      self::CUSTOM_ITEM_TYPE_SHIPPING   => 'doprava',
      self::CUSTOM_ITEM_TYPE_DISCOUNT   => 'sleva',
    );

    public function getInvoiceItems($invoiceId, $type = null) {
      $select = $this->select()->setIntegrityCheck(false);
      $select->where('id_invoice = ?', $invoiceId);
      if (!is_null($type)) {
        if (!is_array($type))
          $type = array($type);
        $select->where('type IN (?)', $type);
      }
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }

    public function removeAllInvoiceItems($invoiceId, $type = null) {
      $where = 'id_invoice = ' . $this->getAdapter()->quote($invoiceId);
      if ($type) {
        $where .= ' AND type = ' . $this->getAdapter()->quote($type);
      }
      return $this->delete($where) !== 0;
    }

    public function getInvoiceItemsSum($invoiceId, $type = null) {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from($this->_name, array(new Zend_Db_Expr('SUM(price)')))
        ->where('id_invoice = ?', $invoiceId);
      if (!is_null($type)) {
        if (!is_array($type)) {
          $type = array($type);
        }
        $select->where('type IN (?)', $type);
      }
      return $select->query()->fetchColumn();
    }
  }
