<?php
class Table_PartsOperations extends Zend_Db_Table_Abstract{
	protected $_name = 'parts_operations';

	protected $_primary = 'id';

	/**
	 * Vraci select pro vsechny operace nad soucasti
	 * @param string $idParts
	 * @return Zend_Db_Select
	 */
	public function getOperations($idParts){
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(
			array('po'=>$this->_name)
		)->joinLeft(
			array('p' => 'producers'),
			"p.id = po.	id_producers",
			array("producer" => "name")
		)->where(
			"id_parts = ?", $idParts
		);
		return $select;
	}

  public function getOperationsSum($idParts = NULL, $col = 'price') {
    $select = $this->select();
    $select->from($this->_name,
      array(
        'id_parts',
        'price' => new Zend_Db_Expr('IFNULL(SUM(price), 0)'),
        'time' => new Zend_Db_Expr('IFNULL(SUM(`time`), 0)'),
      ));
    if ($idParts) {
      $select->where('id_parts = ?', $idParts);
    }
    $select->group('id_parts');
    $result = $select->query(Zend_Db::FETCH_ASSOC);
    try {
      if ($idParts) {
        $row = $result->fetch();
        if (isset($row[$col])) {
          return $row[$col];
        }
        else {
          return FALSE;
        }
      }
      else {
        $sums = array();
        while ($row = $result->fetch()) {
          $sums[$row['id_parts']] = $row;
        }
        return $sums;
      }
    }
    catch (Exception $e) {
      return FALSE;
    }
  }

  public function getOperationsByProducer($producerId) {
		$select = $this->getAdapter()->select();
		$select->from(
			array('p'=>$this->_name),
			array('o_name' => 'name', 'o_price' => 'price', 'o_id' => 'id')
		)->joinLeft(
			array('pa'=>'parts'),
			"p.id_parts = pa.id"
		)->where(
			"p.id_producers = ?",$producerId
		);
		return $this->getAdapter()->fetchAll($select);
	}

  public function recalculate() {

    $result = $this->select()
      ->setIntegrityCheck(false)
      ->from($this, array('id_parts'))
      ->where('id_producers = ?', Table_Producers::MEDUSE_ID)
      ->query()
      ->fetchAll(Zend_Db::FETCH_ASSOC);

    if ($result) {

      foreach ($result as $item) {
        $part = new Parts_Part($item['id_parts']);
        $nettoPrice = $part->getPrice() - $part->getOperationSum();
        $part->setPrice($nettoPrice);
      }

      $tRate = new Table_ApplicationOperationsRate();
      $rate = $tRate->getRate();
      $this->update(array(
        'price' => new Zend_Db_Expr('time * ' . $rate . ' / 60')
        ), 'id_producers = ' . Table_Producers::MEDUSE_ID);

      foreach ($result as $item) {
        $part = new Parts_Part($item['id_parts']);
        $part->refreshOperations(TRUE);
        $part->refreshPrice();
      }
    }
  }
}
