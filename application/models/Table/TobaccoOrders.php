<?php

  class Table_TobaccoOrders extends Table_Orders {

    /**
     *
     * @param array $params parametry filtru pro select
     * @return db select objekt
     */
    public function getOrdersSelect($params = array()) {

      $tTemplates = new Table_EmailTemplates();
      $email_new = $tTemplates->fetchRow('default_for = \'' . Table_Emails::EMAIL_ORDER_NEW_CUSTOMER_NOTICE . '\' AND id_wh_type=\'' . Table_TobaccoWarehouse::WH_TYPE . '\'');
      if ($email_new) {
        $email_new = $email_new->toArray();
        $email_new = $email_new['id'];
      } else {$email_new = '0';}

      $email_open = $tTemplates->fetchRow('default_for = \'' . Table_Emails::EMAIL_ORDER_OPEN_CUSTOMER_NOTICE . '\' AND id_wh_type=\'' . Table_TobaccoWarehouse::WH_TYPE . '\'');
      if ($email_open) {
        $email_open = $email_open->toArray();
        $email_open = $email_open['id'];
      } else {$email_open = '0';}

      $email_exped = $tTemplates->fetchRow('default_for = \'' . Table_Emails::EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE . '\' AND id_wh_type=\'' . Table_TobaccoWarehouse::WH_TYPE . '\'');
      if ($email_exped) {
        $email_exped = $email_exped->toArray();
        $email_exped = $email_exped['id'];
      } else {$email_exped = '0';}

      $select = $this->getAdapter()->select();
      $select->from('orders', array(
        'id',
        'no',
        'title',
        'description',
        'date_request',
        'date_confirm',
        'date_open',
        'date_exp',
        'date_notification',
        'date_notification_new' => new Zend_Db_Expr('(SELECT MAX(inserted) FROM emails WHERE id_orders = orders.id AND type=\'' . Table_Emails::$number_to_db_type[Table_Emails::EMAIL_ORDER_NEW_CUSTOMER_NOTICE] . '\')'),
        'date_notification_open' => new Zend_Db_Expr('(SELECT MAX(inserted) FROM emails WHERE id_orders = orders.id AND type=\'' . Table_Emails::$number_to_db_type[Table_Emails::EMAIL_ORDER_OPEN_CUSTOMER_NOTICE] . '\')'),
        'date_notification_exp' => new Zend_Db_Expr('(SELECT MAX(inserted) FROM emails WHERE id_orders = orders.id AND type=\'' . Table_Emails::$number_to_db_type[Table_Emails::EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE] . '\')'),
        'date_delete',
        'date_deadline',
        'date_pay_deposit',
        'date_pay_all',
        'date_payment_accept',
        'email_new' => new Zend_Db_Expr('\'' . $email_new . '\''),
        'email_open' => new Zend_Db_Expr('\'' . $email_open . '\''),
        'email_expedition' => new Zend_Db_Expr('\'' . $email_exped . '\''),
        'expected_payment' => new Zend_Db_Expr('IFNULL(expected_payment, 0)'),
        'expected_payment_currency',
        'paid_percentage' => new Zend_Db_Expr('concat(paid_percentage, " %")'),
        'status',
        'id_addresses',
        'customer_email' => new Zend_Db_Expr("(SELECT email FROM customers WHERE id = id_customers)"),
        'id_customers',
        'strategic_start',
        'strategic_priority',
        'strategic_months',
        'tracking_no' => new Zend_Db_Expr('group_concat(distinct otc.code separator \', \')'),
        'payment_method',
        'carrier' => new Zend_Db_Expr("(SELECT name FROM orders_carriers WHERE id = id_orders_carriers)"),
        'carrier_id' => 'id_orders_carriers',
        'carrier_price',
        'country' => new Zend_Db_Expr("IF(id_addresses IS NULL, '', (SELECT country FROM addresses WHERE id = id_addresses) )"),
        'owner' => new Zend_Db_Expr("(SELECT name FROM users WHERE id = owner)")
        )
      );

      // propojeni s fakturami u expedovanych objednavek
      $select->joinLeft(array('i' => 'invoice'),
        'orders.id = i.id_orders AND i.type = "' . Table_Invoices::TYPE_REGULAR . '"',
        array('invoice_id' => 'i.id', 'invoice_no' => 'i.no'));

      $select->joinLeft(array('v' => 'invoice_versions'),
        'v.id_invoice = i.id', array('invoice_version' => new Zend_Db_Expr('MAX(v.id)')));
      $select->group('orders.id');

      $select->where('orders.id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE);
      if (isset($params['year'])) {
        $select->where('YEAR(date_request) = ?', $params['year']);
      }

      if (!$params['view_private']) {
        $select->where('orders.private = "n" OR orders.owner = ?', $params['user']);
      }

      // filtrace - nepouziva se
      if (!empty($params['filter_no'])) {
        $select->where('orders.no = ?', $params['filter_no']);
      }
      if (!empty($params['filter_status'])) {
        $select->where("status IN (?)", $params['filter_status']);
      }
      if (!empty($params['tab'])) {
        $select->where("status = ?", $params['tab']);
      }
      if (!empty($params['filter_title'])) {
        $select->where("title LIKE ?", '%' . $params['filter_title'] . '%');
      }
      if (isset($params['filter_expected_payment']) && (float) $params['filter_expected_payment'] > 0) {
        $select->where("expected_payment = ?", $params['filter_expected_payment']);
      }
      if (!empty($params['filter_tracking_no'])) {
        $select->where("tracking_no LIKE ?", $params['filter_tracking_no']);
      }
      if (!empty($params['filter_invoice_no'])) {
        $select->where("i.no = ?", $params['filter_invoice_no']);
      }
      if (isset($params['filter_paid'])) {
        if (in_array('n', $params['filter_paid']) && !in_array('y', $params['filter_paid'])) {
          $select->where("date_payment_accept IS NULL");
        }
        if (!in_array('n', $params['filter_paid']) && in_array('y', $params['filter_paid'])) {
          $select->where("date_payment_accept IS NOT NULL");
        }
      }

      // trackovaci kody
      $select->joinLeft(['otc' => 'orders_tracking_codes'], 'orders.id = otc.id_orders', []);

      return $select;
    }

    public function getDataGrid($params = array()) {

    $select = $this->getOrdersSelect($params);

    // nastaveni data gridu
    $settings = array(
        'css_class' => 'table table-bordered table-hover table-striped',
        'curr_sort' => 'no',
        'curr_sort_order' => 'desc',
        'columns' => array(
            'no' => array(
                'header' => _('Č.obj.'),
                'css_style' => 'text-align: left',
                'sorting' => true,
                'renderer' => 'Bootstrap_Button',
                'icon' => 'icon-edit',
                'class' => 'btn btn-mini',
                'text' => '{no}',
                'alt' => 'Detail',
                'url' => '/tobacco-orders/detail/id/{id}',
            ),
            'title' => array(
                'header' => _('Název'),
                'css_style' => 'text-align: left',
                'sorting' => true,
            ),
            'expected_payment' => array(
                'header' => _('Cena'),
                'renderer' => 'bootstrap_currency',
                'currency_options' => array(
                    'currency_value' => 'expected_payment',
                ),
                'css_style' => 'text-align: right',
                'sorting' => true,
            ),
          'paid_percentage' => array(
            'header' => _('Uhrazeno'),
            'renderer' => 'bootstrap_currency',
            'currency_options' => array(
              'currency_value' => 'paid_percentage',
            ),
            'css_style' => 'text-align: right',
            'sorting' => true,
          ),
            'expected_payment_currency' => array(
                'header' => _('Měna'),
                'css_style' => 'text-align: left',
                'sorting' => true,
            ),
            'date_request' => array(
                'header' => _('Vloženo'),
                'css_style' => 'text-align: left',
                'renderer' => 'date',
                'sorting' => true
            ),
            'date_open' => array(
                'header' => _('Otevřeno'),
                'css_style' => 'text-align: center',
                'renderer' => 'condition',
                'condition_value' => NULL,
                'condition_false' => array(
                  'db_row_name' => 'date_open',
                  'renderer' => 'date',
                ),
                'condition_true' => array(
                  'renderer' => 'bootstrap_button',
                  'icon' => 'icon-shopping-cart',
                  'class' => 'btn btn-mini',
                  'text' => 'Otevřít',
                  'url' => '/tobacco-orders/open/id/{id}',
                ),
                'sorting' => true
            ),
            'date_exp' => array(
                'header' => _('Expedováno'),
                'css_style' => 'text-align: left',
                'renderer' => 'condition',
                'condition_value' => NULL,
                'condition_false' => array(
                  'renderer' => 'date',
                  'db_row_name' => 'date_exp',
                ),
                'condition_true' => array(
                  'renderer' => 'bootstrap_button',
                  'icon' => 'icon-road',
                  'class' => 'btn btn-mini',
                  'text' => 'expedovat',
                  'url' => '/tobacco-orders/expedition/id/{id}',
                ),
                'sorting' => true
            ),
            'tracking_no' => array(
                'header' => _('Track. no.'),
                'css_style' => 'text-align: left',
                'sorting' => true,
            ),
            'invoice_version' => array(
                'header' => _('Faktura'),
                'css_style' => 'text-align: center',
                'renderer' => 'condition',
                'condition_value' => NULL,
                'condition_true' => array(
                  'db_row_name' => 'invoice_id',
                  'renderer' => 'condition',
                  'condition_value' => NULL,
                  'condition_false' => array(
                    'renderer' => 'bootstrap_button',
                    'icon' => 'icon-file',
                    'class' => 'btn btn-mini',
                    'text' => 'generovat',
                    'url' => '/tobacco-invoice/prepare/id/{invoice_id}',
                  ),
                  'condition_true' => array(
                    'renderer' => 'bootstrap_button',
                    'icon' => 'icon-edit',
                    'class' => 'btn btn-mini',
                    'text' => 'vystavit',
                    'url' => '/tobacco-invoice/prepare/order/{id}',
                  ),
                ),
                'condition_false' => array(
                  'db_row_name' => 'invoice_no',
                  'renderer' => 'Url',
                  'url' => '/tobacco-invoice/download/id/{invoice_id}',
                ),
                'sorting' => true,
            ),
            'date_payment_accept' => array(
                'header' => _('Zaplaceno'),
                'css_style' => 'text-align: center',
                'renderer' => 'condition',
                'condition_value' => NULL,
                'condition_true' => array(
                  'renderer' => 'bootstrap_button',
                  'icon' => 'icon-plus',
                  'class' => 'btn btn-mini',
                  'text' => 'úhrada',
                  'url' => '/tobacco-orders/detail/id/{id}#invoices',
                ),
                'condition_false' => array(
                  'db_row_name' => 'date_payment_accept',
                  'renderer' => 'date',
                ),
                'sorting' => true
            ),
            'date_notification_exp' => array(
                'header' => _('Upozornění'),
                'css_style' => 'text-align: center',
                'renderer' => 'condition',
                'sorting' => true,
                'condition_value' => null,
                'condition_true' => array(
                    'db_row_name' => 'date_exp',
                    'renderer' => 'condition',
                    'condition_value' => null,
                    'condition_true' => array(
                        'db_row_name' => 'date_notification_open',
                        'renderer' => 'condition',
                        'condition_value' => null,
                        'condition_true' => array(
                            'db_row_name' => 'date_open',
                            'renderer' => 'condition',
                            'condition_value' => null,
                            'condition_true' => array(
                                'db_row_name' => 'date_notification_new',
                                'renderer' => 'condition',
                                'condition_value' => null,
                                'condition_true' => array(
                                    'db_row_name' => 'date_request',
                                    'renderer' => 'condition',
                                    'condition_value' => null,
                                    'condition_false' => array(
                                        'db_row_name' => 'id_customers',
                                        'renderer' => 'condition',
                                        'condition_value' => null,
                                        'condition_true' => array(
                                            'renderer' => 'bootstrap_button',
                                            'icon' => 'icon-user',
                                            'text' => 'Bez uživatele',
                                            'class' => 'btn btn-mini',
                                            'alt' => 'detail',
                                            'url' => '/tobacco-orders/detail/id/{id}',
                                        ),
                                        'condition_false' => array(
                                            'renderer' => 'bootstrap_button',
                                            'class' => 'btn btn-mini',
                                            'text' => 'nezasláno!',
                                            'url' => '#',
                                            'on_click' => 'showModal([[id]], [[id_customers]], \'[[customer_email]]\', [[email_new]]);',
                                            'icon' => 'icon-envelope'
                                        ),
                                    ),
                                ),
                                'condition_false' => array(
                                    'renderer' => 'date'
                                ),
                            ),
                            'condition_false' => array(
                                'db_row_name' => 'id_customers',
                                'renderer' => 'condition',
                                'condition_value' => null,
                                'condition_true' => array(
                                    'renderer' => 'bootstrap_button',
                                    'icon' => 'icon-user',
                                    'text' => 'bez uživatele',
                                    'class' => 'btn btn-mini',
                                    'alt' => 'Detail',
                                    'url' => '/tobacco-orders/detail/id/{id}',
                                ),
                                'condition_false' => array(
                                    'renderer' => 'bootstrap_button',
                                    'class' => 'btn btn-mini',
                                    'text' => 'nezasláno!',
                                    'url' => '#',
                                    'on_click' => 'showModal([[id]], [[id_customers]], \'[[customer_email]]\', [[email_open]]);',
                                    'icon' => 'icon-envelope'
                                ),
                            ),
                        ),
                        'condition_false' => array(
                            'renderer' => 'date'
                        ),
                    ),
                    'condition_false' => array(
                        'db_row_name' => 'id_customers',
                        'renderer' => 'condition',
                        'condition_value' => null,
                        'condition_true' => array(
                            'renderer' => 'bootstrap_button',
                            'icon' => '',
                            'text' => 'bez uživatele',
                            'class' => 'btn btn-mini',
                            'alt' => 'detail',
                            'url' => '/tobacco-orders/detail/id/{id}',
                        ),
                        'condition_false' => array(
                            'renderer' => 'bootstrap_button',
                            'class' => 'btn btn-mini',
                            'text' => 'nezasláno!',
                            'url' => '#',
                            'on_click' => 'showModal([[id]], [[id_customers]], \'[[customer_email]]\', [[email_expedition]]);',
                            'icon' => 'icon-envelope'
                        ),
                    ),
                ),
                'condition_false' => array(
                    'renderer' => 'date',
                ),
            ),
            'date_delete' => array(
                'header' => _('Smazáno'),
                'css_style' => 'text-align: left',
                'renderer' => 'date',
                'sorting' => true
            ),
            'delete' => array(
                'header' => _('Smazat'),
                'sorting' => FALSE,
                'renderer' => 'bootstrap_button',
                'icon' => 'icon-remove',
                'class' => 'btn btn-mini remove',
                'text' => 'do koše',
                'alt' => 'Přesunout objednávku do koše',
                'url' => '/tobacco-orders/delete/id/{id}',
            ),
        ),
    );

    // odstraneni sloupce s upozorninim pro ty, kteri nemaji opravneni
    $acl = Zend_Registry::get('acl');
    if (!$acl->isAllowed('tobacco-orders/pay')) {
      unset($settings['columns']['date_payment_accept']);
    }
    if (!$acl->isAllowed('tobacco-orders/notify')) {
      unset($settings['columns']['date_notification_exp']);
    }
    if (!$acl->isAllowed('tobacco-orders/delete')) {
      unset($settings['columns']['date_delete']);
      unset($settings['columns']['delete']);
    }
    // odstraneni sloupcu dle stavu objednavky
    switch ($params['tab']) {
        case 'new':
          unset($settings['columns']['date_exp']);
          unset($settings['columns']['date_payment_accept']);
          unset($settings['columns']['date_delete']);
          unset($settings['columns']['paid_percentage']);
        case 'open':
          unset($settings['columns']['tracking_no']);
          unset($settings['columns']['date_notification_exp']);
          unset($settings['columns']['date_delete']);
          break;
        case 'closed':
          unset($settings['columns']['date_request']);
          unset($settings['columns']['date_open']);
          unset($settings['columns']['date_delete']);
          break;
        case 'trash':
          unset($settings['columns']['date_notification_exp']);
          unset($settings['columns']['date_payment_accept']);
          unset($settings['columns']['delete']);
          unset($settings['columns']['paid_percentage']);
          break;
      }

      // vytvoreni data gridu
    $grid = new ZGrid($settings);
    $grid->setSelect($select);
    $grid->setRequest($params);

    return $grid->render();
  }

  public function getYears($status = 'closed', $id_wh_type = 2) {
    return parent::getYears($status, $id_wh_type);
  }
  
  }

