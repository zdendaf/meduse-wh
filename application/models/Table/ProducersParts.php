<?php
class Table_ProducersParts extends Zend_Db_Table_Abstract{

	protected $_name = 'producers_parts';

	protected $_primary = 'id';

	public function getProducer($idPart){
		$select = $this->select();
		$select->where("id_parts = ?", $idPart);
		if($row = $this->fetchRow($select)){
			return $row->id_producers;
		} else return null;
	}

	public function getOperationProducer($idOperation) {
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(
			'parts_operations'
		)->where("id = ?", $idOperation);
		if($row = $this->fetchRow($select)){
			return $row->id_producers;
		} else return null;
	}

	public function getParts($id_producer, $id_wh_type = 1){

    $subSelect = $this->select()->setIntegrityCheck(false);
    $subSelect->from(
      'parts_operations', 
      array('id_parts', 'operations_price' => new Zend_Db_Expr('SUM(price)')));
    $subSelect->group('id_parts');
    
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(
			array('pp' => $this->_name),
			array('pp_id'=>'pp.id','producers_part_id','producers_part_name','min_amount','producer_price' => 'price'));
    $select->joinLeft(
			array('p' => "parts"),
			"pp.id_parts = p.id",
			array('p.id', 'p.name','p.price'));
    $select->joinLeft(
      array('po' => $subSelect),
      'pp.id_parts = po.id_parts',
      array(
        'purchase_price' => new Zend_Db_Expr('p.price - IFNULL(po.operations_price, 0) - pp.price'), 
        'operations_price' => new Zend_Db_Expr('IFNULL(po.operations_price, 0)')));
    $select
      ->where("pp.id_producers = ?", $id_producer)
      ->where("is_deleted = 0");
    if ($id_wh_type) {
      $select->where('p.id_wh_type = ?', $id_wh_type);
    }
		return $this->fetchAll($select);
	}

}