<?php
class Table_Actualities extends Zend_Db_Table_Abstract{

	protected $_name = 'actualities';

	protected $_primary = 'id';

	public function getActualities($count, $offset = null){
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array('a' => 'actualities'), array('date' => 'DATE(`date`)', 'text'))
		->joinLeft(array('u' => 'users'), "a.id_users = u.id", array('username'))
		->order("a.date DESC");

		if($offset !== null){
			$select->limit($count, $offset);
		}
		else {
			$select->limit($count, 0);
		}
		return $this->fetchAll($select);
	}

	/**
	 * prida aktualitu
	 * @param string $text
	 */
	public function add($text, $id_wh_type = Parts_Part::WH_TYPE_PIPES){
		$authNamespace = new Zend_Session_Namespace('Zend_Auth');
		$this->insert(array(
			'id_users' => $authNamespace->user_id,
      'id_wh_type' => $id_wh_type,
			'text' => $text
		));
	}
}
