<?php
class Table_Eans extends Zend_Db_Table_Abstract {

	protected $_name = 'eans';
	protected $_primary = array('prefix', 'code');
	
	// prefix přiřazený společností GS1
	const PREFIX = 8592936;

	public function getCode($partId) {
		$part = new Parts_Part($partId);
		if (!$part->isProduct()) {
			throw new Zend_Exception('Součást není produktem, není přiřazený EAN13 kód.');
		}
		return $this->fetchRow('id_parts LIKE ' . $this->getAdapter()->quote($partId));
	}
	
	public function getPart($code, $prefix = self::PREFIX) {
		$db = $this->getAdapter();
		return $this->fetchRow(array(
			'prefix = ' . $db->quote($prefix),
			'code = ' . $db->quote($code)
		));		
	}
	
	public function addCode($partId, $code, $prefix = self::PREFIX) {
		$part = new Parts_Part($partId);
		if (!$part->isProduct()) {
			throw new Zend_Exception('Součást není produktem, nelze přiřadit EAN13 kód.');
		}
		$chnum = $this->getCheckNumber($code, $prefix);
		if ($this->getCode($partId)) {
			$this->update(array(
				'prefix' => $prefix,
				'code' => $code,
				'chnum' => $chnum				
			), 'id_parts LIKE ' . $this->getAdapter()->quote($partId));
		} else {		
			$this->insert(array(
				'prefix' => $prefix,
				'code' => $code,
				'chnum' => $chnum,
				'id_parts' => $partId
			));
		}
	}
	
	public function removeCode($partId) {
		$this->delete('id_parts LIKE ' . $this->getAdapter()->quote($partId));
	}
	
	public function getBarcode($partId) {
		$barcode = new Zend_Barcode_Object_Ean13();
		$code = $this->getCode($partId);
		$barcode->setText($code->prefix . $code->code);
		$renderer = new Zend_Barcode_Renderer_Svg(array('barcode' => $barcode));
		$renderer->render();
	}
	
	protected function getCheckNumber($code, $prefix = self::PREFIX) {
		$c = (string) $prefix . $code;
		if (strlen($c) != 12) {
			throw new Zend_Exception('Nesprávná délka EAN13 kódu bez kontrolní číslice');
		}
		for ($i = 0; $i < 12; $i = $i + 2) {
			$even += intval(substr($c, $i, 1)); 
		}
		for ($i = 1; $i < 12 ; $i = $i + 2) {
			$odd += intval(substr($c, $i, 1)); 
		}
		$sum = $even + 3 * $odd;
		return ($sum % 10 == 0) ? 0 : 10 - ($sum % 10);
	}
	
}