<?php

  class Table_Departments extends Zend_Db_Table_Abstract {

    protected $_name = 'departments';
    protected $_primary = 'id';

    const FIRM_MEDUSE_DESIGN = 1;
    const FIRM_TOBACOCKTAILS = 2;

    public static $firm_name = array(
      self::FIRM_MEDUSE_DESIGN => 'Meduse Design',
      self::FIRM_TOBACOCKTAILS => 'Medite',
    );

    public function getGrid($options) {
      $grid = new ZGrid($options);
      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from(array('d' => $this->_name), array('id', 'name'))
        ->joinLeft(array('u' => 'users'), 'd.id_users = u.id', array('leader' => 'name_full'));
      $grid->setSelect($select);
      return $grid;
    }

    public function getSelectOptions($first = NULL) {
      $options = array();
      if (!is_null($first)) {
        $options[NULL] = $first;
      }
      $select = $this->select()->order('id_wh_type');
      $depts = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
      foreach($depts as $dept) {
        $options[$dept->id] = $dept->name . ' / ' .  self::$firm_name[$dept->id_wh_type];
      }
      return $options;
    }

    /**
     * Vrati rowset oddeleni, ktere dany uzivatel ridi (je jejich vedoucim)
     */
    public function getManagedDepartments($userId) {
      $select = $this->select()->setIntegrityCheck(FALSE)->where('id_users = ?', $userId);
      return $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
    }



  }

