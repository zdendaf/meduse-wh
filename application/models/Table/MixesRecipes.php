<?php

  class Table_MixesRecipes extends Zend_Db_Table_Abstract {
    
    protected $_name = 'mixes_recipes';
    protected $_primary = 'id';

    public function getUnitPrice($mixId = NULL, $sql = FALSE) {

      $totalSelect = $this->select()
        ->from('mixes_recipes', [
          'id_mixes', 'total' => new Zend_Db_Expr('SUM(ratio)')])
        ->group('id_mixes');

      $tRecipes = new Table_Recipes();
      $recipeSelect = $tRecipes->getUnitPrice(NULL, TRUE);

      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from(['mr' => 'mixes_recipes'], ['id_mixes', 'unit_symbol' => new Zend_Db_Expr('"Kč/kg"')])
        ->join(['t' => $totalSelect], 't.id_mixes = mr.id_mixes', [])
        ->join(['r' => $recipeSelect], 'r.recipe_id = mr.id_recipes', [
          'unit_price' => new Zend_Db_Expr('SUM(r.unit_price * mr.ratio) / t.total'),
        ])
        ->group('mr.id_mixes');
      if ($mixId) {
        $select->where('mr.id_mixes = ?', $mixId);
      }

      if ($sql) {
        return $select;
      }
      elseif ($mixId) {
        return $select->query()->fetch();
      }
      else {
        return $select->query()->fetchAll();
      }
    }

  }
