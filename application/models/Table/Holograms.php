<?php
class Table_Holograms extends Zend_Db_Table_Abstract{

	protected $_name = 'holograms';
	protected $_primary = 'id';

  public function getAvailable($counts) {
    $serials = array();
    if ($counts) {
      $subSelects = array();
      foreach ($counts as $idColl => $count) {
        if ($count <= 0) {
          continue;
        }
        $subSelects[$idColl] = $this->select()->from($this->_name, array(
          'collection' => 'id_categories', 
          'serial' => 'hologram',
        ))->where('id_orders IS NULL')->where('id_categories = ?', $idColl)->limit($count);
      }
      if ($subSelects) {
        
        $expr = implode(') UNION (', $subSelects);
        $serials = $this->getAdapter()->query('(' . $expr . ')')->fetchAll(Zend_Db::FETCH_ASSOC);
      }
    }
    return $serials;
  }
}