<?php

  /**
   * Description of MacerationsHistory
   *
   * @author Zdenek
   */
  class Table_MacerationsHistory extends Zend_Db_Table_Abstract {

    const TYPE_CREATE = 'create';
    const TYPE_PACK = 'pack';
    const TYPE_CHECK = 'check';
    const TYPE_MERGE_FROM = 'merge_from';
    const TYPE_MERGE_TO = 'merge_to';
    const TYPE_BREAK = 'break';
    const TYPE_DESTROY = 'destroy';

    public static $typeTraslations = [
      self::TYPE_CREATE => 'založení sudu',
      self::TYPE_PACK => 'nabalení produktu',
      self::TYPE_CHECK => 'kontrola sudu',
      self::TYPE_MERGE_FROM => 'přimíchání ze sudu',
      self::TYPE_MERGE_TO => 'vmíchání do sudu',
      self::TYPE_BREAK => 'rozklad produktu',
      self::TYPE_DESTROY => 'vyprázdnění sudu',
    ];

    protected $_name = 'macerations_history';
    protected $_primary = 'id';

    public function getChanges($macerationIds, $from, $to, $sum = FALSE, $partIds = []) {

      if (!is_array($macerationIds)) {
        $macerationIds = [$macerationIds];
      }

      $cols = array();
      if ($sum) {
        $cols['amount'] = new Zend_Db_Expr('SUM(amount)');
      }
      else {
        $cols[] = 'id';
        $cols[] = 'amount';
        $cols['date'] = 'last_change';
        $cols[] = 'description';
        $cols[] = 'type';
      }
      if (!$macerationIds) {
        return $sum ? 0 : [];
      }
      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from(['mh' => 'macerations_history'], $cols)
        ->where('id_macerations IN (?)', $macerationIds)
        ->where('last_change >= ?', $from);
        if ($from == $to) {
            $select->where('last_change <= ?', $to);
        }
        else {
            $select->where('last_change <= ?', $to);
        }
        $select->order(['last_change', 'id']);
      if ($sum) {
        // TODO: overit a zmenit podminku v sumach pro prodej (detail mixu)
        $select->where('type IN (?)', [
          self::TYPE_CREATE,
          self::TYPE_PACK,
          self::TYPE_BREAK,
          self::TYPE_CHECK,
          self::TYPE_DESTROY,
        ]);
      }
      else {
        $select->join(['m' => 'macerations'], 'm.id = mh.id_macerations', ['batch']);
      }
      if ($partIds) {
        $select->where('type IN (?)', [self::TYPE_PACK, self::TYPE_BREAK]);
        $cSelect = $this->select();
        foreach ($partIds as $partId) {
          $cSelect->orWhere('description LIKE ?', '%' . $partId . '%');
        }
        $select->where(implode(' ', $cSelect->getPart(Zend_Db_Select::WHERE)));
      }
      $query = $select->query(Zend_Db::FETCH_ASSOC);
      return $sum ? $query->fetch() : $query->fetchAll();
    }

    public function checkType($type) {
      return key_exists($type, self::$typeTraslations);
    }

  }
