<?php

  /**
   * Mapper pro tabulku s cenami opreaci.
   *
   * @author Zdenek
   */
  class Table_ApplicationOperationsRate extends Zend_Db_Table_Abstract {
    
    protected $_name = 'application_operations_rate';
    protected $_primary = 'id';
    
    /**
     * Vraci posledni hodinovou sazbu za operaci.
     *  
     * @return int
     *    posledni hodinova sazba za operaci.
     */
    public function getRate() {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from($this)->columns(array('rate'))->order('valid DESC');
      $result = $select->query()->fetchColumn(1);
      return $result ? (int) $result : 0;
    }
  }
