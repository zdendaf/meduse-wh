<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Table_CustomersWarehousesProducts extends Zend_Db_Table_Abstract {

    protected $_name = 'customers_warehouses_products';
    protected $_primary = 'id';

    public function getProducts($warehouseId) {
      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from(array('cwp' => $this->_name))
        ->joinLeft(array('p' => 'parts'), 'p.id = cwp.id_parts', array('name'))
        ->joinLeft(array('o' => 'orders'), 'o.id = cwp.id_orders', array(
          'order_title' => 'title', 'order_id' => 'id', 'order_no' => 'no'))
        ->where('cwp.id_customers_warehouses = ?', $warehouseId);
      $results = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
      return $results;
    }

    public function addProduct($warehouseId, $productId, $price, $orderId = NULL, $amount = 1) {
      $now = new Meduse_Date();
      $idx = 0;
      while($idx < $amount) {
        $this->createRow(array(
          'id_customers_warehouses' => $warehouseId,
          'id_parts' => $productId,
          'id_orders' => $orderId,
          'aquisition_date' => $now->toString('Y-m-d'),
          'aquisition_price' => $price,
        ))->save();
        $idx++;
      }
    }
  }
