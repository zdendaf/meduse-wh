<?php
/**
 * Trida pracujici s objednavkou
 * @author niecj
 */
class Table_Row_Orders extends Zend_Db_Table_Row_Abstract {

    const STATUS_NEW       = 'new';

    const STATUS_CONFIRMED = 'confirmed';

    const STATUS_OPEN      = 'open';

    const STATUS_CLOSED    = 'closed';

    const STATUS_DELETED   = 'deleted';

    /**
     * Vraci soucasti na dane objednavce
     * POZOR - novym zpusobem - nezobrazi produkty, ktere nejsou soucasti
     */
    public function getParts(){
        $db = $this->getTable()->getAdapter();
        $select = $db->select();
        $select->from(
            array('op' => 'orders_products')
        )->join(
            array('p' => 'parts'),
            "op.id_products = p.id"
        )->where(
            "id_orders = ?", $this->id
        );
        return $db->fetchAll($select);
    }

}
?>
