<?php

  class Table_PartsMixes extends Zend_Db_Table_Abstract {
    
    protected $_name = 'parts_mixes';
    protected $_primary = 'id';
    
    function getMix($partId) {
      $select = $this->select()->setIntegrityCheck(false);      
      $select->from($this)->where('id_parts = ?', $partId);
      return $select->query()->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
    function getParts($recipeId) {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('pr' => $this->_name), array('id_parts', 'amount'));
      $select->joinLeft(array('p' => 'parts'), 'p.id = pr.id_parts', array('name'));
      $select->where('id_recipes = ?', $recipeId);
      return $select->query()->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
    function getAmount($partId) {
      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from($this, array('amount'))
        ->where('id_parts = ?', $partId)
        ->limit(1);
      $amount = $select->query()->fetchColumn();  
      return $amount === false ? 0 : (float) $amount;
    }
  }

