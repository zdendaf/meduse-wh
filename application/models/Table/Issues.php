<?php

class Table_Issues extends Zend_Db_Table_Abstract {
	
	protected $_name = 'issues';
	protected $_primary = 'id';
	
	const STATUS_NEW		= 'new';
	const STATUS_SCHEDULED  = 'scheduled';
	const STATUS_CLOSED     = 'closed';
	const STATUS_REJECTED   = 'rejected';

	static $statusToString = array(
		self::STATUS_NEW		=> "nový",
		self::STATUS_SCHEDULED  => "naplánovaný",
		self::STATUS_CLOSED     => "uzavřený",
		self::STATUS_REJECTED   => "zamítnutý"
	);

	static $statusToClass = array(
		self::STATUS_NEW		=> "warning",
		self::STATUS_SCHEDULED  => "success",
		self::STATUS_CLOSED     => "info",
		self::STATUS_REJECTED   => "important"
	);	
	
	public function getAllIssues() {		
		$subSelect = $this->_db->select()
				->from(array('c' => 'issues_comments'), array('id_issues', 'comments' => 'COUNT(id_issues)'))
				->group(array('id_issues'));
		$select = $this->_db->select()
				->from(array('i' => $this->_name))
				->joinLeft(array('u' => 'users'), 'u.id = i.id_users', array('username' => 'u.name_full'))
				->joinLeft(array('c' => new Zend_Db_Expr('(' . $subSelect . ')')), 'c.id_issues = i.id', array('c.comments'))
				->order(array('i.changed DESC'));
		return $select->query()->fetchAll();
	}
	
	public function getIssue($id) {
		$select = $this->_db->select()
				->from(array('i' => $this->_name))
				->joinLeft(array('u' => 'users'), 'u.id = i.id_users', array('username' => 'u.name_full'))
				->where('i.id = ?', $id);
		return $select->query()->fetch();
	}
}


