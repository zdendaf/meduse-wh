<?php
class Table_ApplicationDph extends Zend_Db_Table_Abstract{
	
	protected $_name = 'application_dph';
	
	protected $_primary = 'id';
  
  const DEFAULT_DPH = 21;
        
        
        /**
         * Get data
         * 
         * for aquitec grid
         * 
         * @return \Zend_Db_Select object
         */
        public function getData()
        {
            $select = $this->getAdapter()->select();
            return $select->from($this->_name)->order(['code_c_country ASC', 'active_from_date ASC']);
        }
        

        /**
         * Get Actutal DPH value
         * 
         * @return string
         */
        public function getActualDph($date = null, $country = 'CZ')
        {
            if (is_null($date)) {
              $date = date('Y-m-d');
            }
          
            $data = $this->fetchRow('code_c_country = ' . $this->getAdapter()->quote($country) .
              ' AND active_from_date < ' . "'" . $date . "'", 'inserted DESC');
            
            if (is_null($data)) {
              return self::DEFAULT_DPH;
            }
            else {
              $out = $data->toArray();
              return $out['value'];
            }
        }


    /**
     * Add new DPH value
     *
     * @param int $value
     * @param string $active_from_date
     * @param string $code
     */
    public function addDphValue($value, $active_from_date, $code) {
        $data = [
            'value' => $value,
            'active_from_date' => $active_from_date,
            'code_c_country' => $code,
        ];
        $this->insert($data);
    }


    /**
         * Delete value
         *  
         * @param int $id 
         */
        public function delDphValue($id) 
        {
            $this->delete('id = ' . $id);
        }
        
}