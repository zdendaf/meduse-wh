<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Table_ApplicationCoverCost extends Zend_Db_Table_Abstract {
    
    const DEFAULT_COVER_COST = 10;
    
    protected $_name = 'application_cover_cost';
    protected $_primary = 'id';
    
    public function getCoverCost($wh = 1) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, array('cover_cost'))
          ->where('id_wh_type = ' . $wh)
          ->order(array('inserted DESC'))->limit(1);
        $rate = $select->query(Zend_Db::FETCH_ASSOC)->fetchColumn();
        return $rate == false ? self::DEFAULT_COVER_COST : $rate;
    }    
    
  }

  