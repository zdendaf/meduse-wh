<?php

class Table_InvoiceCustomTexts extends Zend_Db_Table_Abstract {
    
    protected $_name = 'invoice_custom_texts';
    protected $_primary = 'id';
    
    const TYPE_B2B = 'B2B';
    const TYPE_B2C = 'B2C';
    
    const REGION_EU = 'eu';
    const REGION_NON_EU = 'non-eu';
    
    const LANGUAGE_CZECH = 'cs';
    const LANGUAGE_ENGLISH = 'en';
    
    const LENGTH_TEXT = 500;
    const LENGTH_DESCRIPTION = 50;

    const INVOICE_TYPE_ALL = 'all';
    const INVOICE_TYPE_PROFORMA = 'proforma';
    const INVOICE_TYPE_REGULAR = 'regular';

    public static function getInvoiceTypes() {
       return [
        self::INVOICE_TYPE_ALL => 'vše',
        self::INVOICE_TYPE_PROFORMA => 'proforma',
        self::INVOICE_TYPE_REGULAR => 'faktura',
      ];
    }

    public static function getAllText($wh = 1) {
      $tTexts = new Table_InvoiceCustomTexts();
      return $tTexts->fetchAll('id_wh_type = ' . $wh)->toArray();
    }
}

