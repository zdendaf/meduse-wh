<?php
class Table_PartsCtg extends Zend_Db_Table_Abstract{

	protected $_name = 'parts_ctg';
	protected $_primary = 'id';

  public const PRESENTATION_TOOLS = 20;
  public const TYPE_SETS = 23; //typ soucasti sety dymek
  public const TYPE_SETS_PACKING = 24;
  public const TYPE_ACCESSORIES = 58;
  public const EXTRA_ACCESSORIES = 7;
  public const TRANSPORT_BOXES = 44;
  public const PALETTES = 45;

  public static array $ctgToString = array(
    self::EXTRA_ACCESSORIES => 'Extra příslušenství',
    self::PRESENTATION_TOOLS => 'Prezentační nástroje',
    self::TYPE_SETS => 'Sety dýmek',
    self::TYPE_SETS_PACKING => 'Sety dýmek - obaly',
    self::TYPE_ACCESSORIES => 'Příslušenství',
    self::TRANSPORT_BOXES => 'Přepravní krabice',
    self::PALETTES => 'Palety',
  );

  // Kategorie, ktere se nezobrazuji v reportech
  // (soucasti jdou ucetne primo do spotreby)
  static $reportExcuded = [
    33, // nastroje
    46, // lepidla
    47, // konzervační spreje
  ];

	private static $_instance;

	public function getCategories($lang = 'cs'){
		$rows = $this->fetchAll();
		$data = array();
		foreach($rows as $row) {
			$data[$row->id] = $lang == 'cs' ?
                  array('name' => $row->name, 'description' => $row->description)
                : array('name' => $row->name_eng, 'description' => $row->description);
		}
		return $data;
	}

	public function getParentCategories() {
		return $this->fetchAll($this->select()->from($this->_name,array('id'))->where('parent_ctg = 0'))->toArray();
	}

	public function getCategoryName($idCtg, $lang = 'cs'){
		$rCtg = $this->find($idCtg)->current();
		if($rCtg){
			$name = $lang == 'cs' ? $rCtg->name : $rCtg->name_eng;
			if($rCtg->parent_ctg !== null){
				$rParent = $this->find($rCtg->parent_ctg)->current();
				if($rParent){
					$name = $lang == 'cs' ? $rParent->name . ' - ' . $name
                            : $rParent->name_eng . ' - ' . $name;
				}
			}
			return $name;
		} else {
			return '';
		}
	}

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new Table_PartsCtg();
		}
		return self::$_instance;
	}


  /**
   * Get category list without saved items
   *
   * @return array|string[]
   */
  public function getCategorySelectList(): array
  {

		$select = Zend_Db_Table::getDefaultAdapter()->select();
		$select->from(array('pc' => $this->_name), array('pc.id', 'pc.name'));
                $select->joinLeft(array('cptd' => 'customers_parts_ctg_discount'), 'cptd.id_parts_ctg = pc.id', array('cptd.id_parts_ctg'));
                $select->where('cptd.id_parts_ctg IS NULL');
                $data = Zend_Db_Table::getDefaultAdapter()->fetchPairs($select);

                if(empty($data)) {
                    return array();
                }

    return array('' => '- - - -') + $data;
	}

  public function getParentCategory($idCtg) {
      $row = $this->find($idCtg)->current();
      if ($row) {
      return $row->parent_ctg ? $row->parent_ctg : $idCtg;
      } else {
        return null;
      }
  }

  public static function getChildrenCategories($parentCtgId) {
    $childs = array();
    $self = new Table_PartsCtg();
    $query = $self->select()->where('parent_ctg = ?', $parentCtgId)->query(Zend_Db::FETCH_OBJ);
    while ($row = $query->fetch()) {
      $childs[] = $row->id;
    }
    return $childs;
  }

  public function getCategoryByIdOrName($term) {
    $id = intval($term);
    $select = $this->select()
      ->where('id = ?', $id)
      ->orWhere('name like %?%', $term);
    return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }
  
  public function getCategoriesWithPartsSum($wh = 1): array
  {
      $subSelect = $this->select()->setIntegrityCheck(FALSE);
      $subSelect->from(array('p' => 'parts'),
          array(new Zend_Db_Expr('COUNT(p.id)')))->where('p.id_parts_ctg = ctg.id');
    
      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(array('ctg' => 'parts_ctg'), array(
          'id', 'name', 'ctg_order', 'description', 'parent_ctg',
          'parts_count' => new Zend_Db_Expr('(' . $subSelect . ')'),
      ));
      $select->joinLeft(array('parent' => 'parts_ctg'), 'ctg.parent_ctg = parent.id', array(
          'parent_id' => 'id',
          'parent' => 'name',
          'p_ctg_order' => 'ctg_order'
      ));
      
      $select->where('ctg.id_wh_type = ?', $wh);
    
      $select->order(new Zend_Db_Expr('(IF(ctg.parent_ctg IS NULL,0,1)*IFNULL(parent.ctg_order,0)*10000) + ctg.ctg_order'));
      
      return $select->query()->fetchAll();
    
  }
}