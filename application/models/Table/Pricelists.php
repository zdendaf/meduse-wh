<?php

class Table_Pricelists extends Zend_Db_Table_Abstract {

    protected $_name = 'pricelists';
    protected $_primary = 'id';

    const TYPE_B2B = 'b2b';
    const TYPE_B2C = 'b2c';

    const REGION_EU     = 'eu';
    const REGION_NON_EU = 'non-eu';

    public function insert(array $data) {
      $to = $data['assign'] ?? NULL;
      unset($data['assign']);
      $type = $data['type'] ?? NULL;
      unset($data['assign']);
      $id = parent::insert($data);
      if ($to) {
        $this->_assign($id, $to, $type);
      }
      return $id;
    }

    public function update(array $data, $where) {
        $to = $data['assign'];
        unset($data['assign']);
        parent::update($data, $where);
        list( , $id) = explode('=', $where);
        $this->_assign ($id, $to, $data['type']);
    }

    public function getCustomersPricelists($idCustomer, $ignore_customer_type = FALSE) {
        $tCustomers = new Table_Customers();
        $rCustomers = $tCustomers->find($idCustomer)->current();
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('pl' => $this->_name));
        $select->joinLeft(array('cp' => 'customers_pricelists'),
                'pl.id = cp.id_pricelists AND cp.id_customers = ' . $idCustomer, array(
                    'assign' => 'cp.id'
                ));
        $select->where('pl.deleted = ?', 'n');
        $select->where('pl.id_wh_type = ?', $rCustomers->id_wh_type);
        if ($rCustomers->type && !$ignore_customer_type) {
            $select->where('pl.type = ?', $rCustomers->type);
        }
        $select->order('pl.type');
        return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }

    public function getCustomersValidPricelist($idCustomer, $ignore_customer_type = FALSE) {
        $tCustomers = new Table_Customers();
        $rCustomers = $tCustomers->find($idCustomer)->current();
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('pl' => $this->_name), array('id'));
        $select->joinLeft(array('cp' => 'customers_pricelists'),
                'pl.id = cp.id_pricelists', array());
        if ($rCustomers->type && !$ignore_customer_type) {
            $select->where('pl.type = ?', $rCustomers->type);
        }
        $select->where('pl.valid <= ?', new Zend_Db_Expr('NOW()'));
        $select->where('cp.id_customers = ?', $idCustomer);
        $select->where('pl.deleted = ?', 'n');
        $select->where('pl.id_wh_type = ?', $rCustomers->id_wh_type);
        $select->order('pl.valid ASC');
        $pdId = $select->query(Zend_Db::FETCH_ASSOC)->fetchColumn();
        return ($pdId ? $pdId : null);
    }

    private function _assign($id, $to, $type) {
        if (empty($to)) {
            return;
        }
      $tCP = new Table_CustomersPricelists();
        if ($to == 'all') {
            $tCP->assignAllCustomers($id, empty($type)? null : $type);
        } else {
            $tCP->assignCustomersFrom((int) $to, array($id));
        }
    }

    public function getPricelists($ids) {
      if (!is_array($ids)) {
        throw new InvalidArgumentException('Pricelists::getPricelists()');
      }

      $select = $this->getAdapter()->select();
      $select->from(array('pl' => 'pricelists'))
        ->join(array('p' => 'prices'), 'p.id_pricelists = pl.id', array('id_parts','price'));
      foreach ($ids as $id) {
        $select->orWhere('pl.id = ?', $id);
      }

      $result = $this->getAdapter()->fetchAll($select);
      return $result ? new Pricelists_Info($result, $ids) : NULL;
    }

    public function getAllPricelists($wh = null, $type = null): array
    {
      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from($this->_name)
        ->where('deleted = ?', 'n')
        ->where('valid <= ?', new Zend_Db_Expr('NOW()'));
      if ($wh) {
        $select->where('id_wh_type = ?', $wh);
      }
      if ($type) {
        $select->where('type = ?', $type);
      }
      return $select->query()->fetchAll();
    }

    public function getPricelistsByType($wh, $type) {
      $select = $this->select()
              ->where('id_wh_type =?', $wh)
              ->where('type = ?', $type)
              ->where('deleted = ?', 'n')
              ->order('name');
      return $select;
    }

    /**
     * Zkontroluje, zdali má uživatel přiřazen ceník. Pokud nemá, přiřadí mu výchozí
     *
     * @param int    $customer zákazníkovo ID
     * @param string $country  kód státu
     *
     * @return bool|string Vrátí zkratku ceníku, případně FALSE.
     */
    public function setDefaultPricelist($customer, $country) {
      //kontrola zdali má uživatel přiřazen ceník
      $tCustomersPricelists = new Table_CustomersPricelists();
      $pricelist_check = $tCustomersPricelists->select()->where('id_customers = ?', (int)$customer)->query()->fetch();
      if ($pricelist_check) {
        return FALSE;
      //přiřazení ceníku
      } else {
        $region    = $this->select()->setIntegrityCheck(false)->from('c_country')->where('code = ?', strtoupper($country))->query()->fetch();
        $person    = $this->select()->setIntegrityCheck(false)->from('customers')->where('id = ?', (int)$customer)->query()->fetch();

        $pricelist = $this->select()->where('type = ?', $person['type'])->where('region = ?', $region['region'])->where('valid < ?', date('Y-m-d'))->query();

        if ($pricelist) {
          $pricelist = $pricelist->fetch();


          $tCustomersPricelists->assignCustomer($person['id'], array((int)$pricelist['id']));

          return $pricelist['abbr'];
        } else {
          return FALSE;
        }
      }
    }

    public function getPricelistsForExport($wh = NULL) {
      $export = array();
      $query = $this->select()
        ->from('pricelists', array('abbr'))
        ->where('export = ?', 'y')
        ->query();
      while ($abbr = $query->fetchColumn()) {
        $cond = 'pl.id = pr.id_pricelists AND pl.abbr = ' . $this->getAdapter()->quote($abbr);
        $select = $this->select()->setIntegrityCheck(FALSE)
          ->from(array('p' => 'parts'), array('sku'))
          ->join(array('pr' => 'prices'), 'pr.id_parts = p.id', array('price'))
          ->join(array('pl' => 'pricelists'), $cond, array())
          ->where('p.product = \'y\'')
          ->where('p.sku IS NOT NULL');
        if ($wh) {
          $select->where('p.id_wh_type = ?', $wh);
        }
        $export[$abbr] = $select->query()->fetchAll();
      }
      return $export;
    }


    public function getPricelistsAssignsForExport($wh = NULL) {
      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from(array('cp' => 'customers_pricelists'), array())
        ->join(array('c' => 'customers'), 'cp.id_customers = c.id', array('ic' => 'ident_no'))
        ->join(array('pl' => 'pricelists'), 'cp.id_pricelists = pl.id', array('pl' => 'abbr'))
        ->where('c.ident_no IS NOT NULL')
        ->where('pl.deleted = ?','n')
        ->where('pl.export = ?','y');
        if ($wh) {
          $select->where('c.id_wh_type = ?', $wh);
        }
        $export = $select->query()->fetchAll();
        return $export;
    }

    public function getPricelistByAbbr($abbr) {
      return $this->select()->where('abbr = ?', $abbr)->query()->fetch();
    }

    /**
     * Vráti ID master ceníku.
     */
    public function getMasterId() {
      if ($row = $this->getMaster()) {
        return (int) $row['id'];
      }
      else {
        return NULL;
      }
    }

    /**
     * Vrátí informace o master ceníku.
     */
    public function getMaster() {
      try {
        return $this->select()
          ->where('master = ?', 1)
          ->query(Zend_Db::FETCH_ASSOC)
          ->fetch();
      }
      catch (Zend_Db_Statement_Exception $e) {
        Utils::logger()->err($e->getMessage());
        return NULL;
      }
    }

  /**
   * Vrátí master ceník.
   * @return \Pricelists_Info|null
   */
    public function getMasterPricelist() {
      if ($masterId = $this->getMasterId()) {
        return $this->getPricelists([$masterId]);
      }
      return NULL;
    }

  /**
   * @throws Zend_Db_Statement_Exception
   */
  public function getPriceListByCtg($ctgId, $wh_type = Table_PartsWarehouse::WH_TYPE) {
      if ( ! $mapped = Pricelists::getMapping($ctgId)) {
        return false;
      }
      $select = $this->select()->where('id = ?', reset($mapped));
      return $select->query(Zend_DB::FETCH_ASSOC)->fetch();
    }
}
