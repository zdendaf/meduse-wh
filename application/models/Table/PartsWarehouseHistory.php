<?php

class Table_PartsWarehouseHistory extends Zend_Db_Table_Abstract {

  protected $_name = 'parts_warehouse_history';
  protected $_primary = 'id';


  public function getChanges($partId, Meduse_Date $from, Meduse_Date $to, $sum = FALSE) {
    /** @var Zend_Db_Adapter_Abstract $db */
    $db = Zend_Registry::get('db');
    $sub = $db->select()
      ->from(array('h' => 'parts_warehouse_history'), array(
        'date' => new Zend_Db_Expr('DATE_FORMAT(h.last_change, \'%Y-%m-%d\')'),
        'amount_add' => new Zend_Db_Expr('IF (amount > 0, amount, 0)'),
        'amount_remove' => new Zend_Db_Expr('IF (amount < 0, ABS(amount), 0)'),
      ))
      ->join(array('p' => 'parts'), 'p.id = h.id_parts AND p.id = \'' . $partId . '\'', array())
      ->where('h.last_change >= ?', $from->get('Y-m-d 00:00:00'))
      ->where('h.last_change <= ?', $to->get('Y-m-d 23:59:59'));
    $select = $db->select()
      ->from(array('sub' => $sub), array(
        'date',
        'amount_add' => new Zend_Db_Expr('SUM(amount_add)'),
        'amount_remove' => new Zend_Db_Expr('SUM(amount_remove)'),
      ));
    if (!$sum) {
      $select
        ->group('date')
        ->order('date');
    }
    $query = $select->query(Zend_Db::FETCH_ASSOC);
    return $sum ? $query->fetch() : $query->fetchAll();
  }

}