<?php

/**
 * Rozšíření Table_Customers pro sklad Medite
 *
 * @author Vojtěch Mrkývka
 */
class Table_TobaccoCustomers extends Table_Customers{
  
  public function saveForm($data) {
		//zpracování výchozího ceníku v závislosti na typu zákazníka
    if ($data['customer_type'] == 'B2B') {
      $pricelist = $data['pricelist_B2B'];
    } else {
      $pricelist = $data['pricelist_B2C'];
    }
    
    //nový nebo stávající zákazník
    if(isset($data['id'])) {
			$rCustomer = $this->find($data['id'])->current();
      //kontrola změny ceníku u stávajícího zákazníka
      $rPricelists = new Table_Pricelists();
      if ($data['customer_type'] != $rCustomer->type) {
        $addPricelist = TRUE;
        // ošetření proti přiřazení ceníků jiného typu
        $deletePricelists = TRUE;
      }
      elseif ($rPricelists->getCustomersValidPricelist($data['id']) != $pricelist) {
        $addPricelist = TRUE;
      }
		} else {
			$rCustomer = $this->createRow();
                        
      $rCustomer->customer_no = $this->generateCustomerNumber();
      $rCustomer->inserted = new Zend_Db_Expr('NOW()');
					
			$tAddress = new Table_Addresses();
			$rAddress = $tAddress->createRow();
			$idAddr = $rAddress->save();
			$rCustomer->id_addresses = $idAddr;
      // přiřazení nového ceníku - níže
      $addPricelist = TRUE;
		}
    
    
    //ukládání dat do tabulky zákazníků
		$rCustomer->company = $data['customer_company'];
		$rCustomer->first_name = $data['customer_first_name'];
		$rCustomer->last_name = $data['customer_last_name'];
		$rCustomer->phone = $data['customer_phone'];
    $rCustomer->email = $data['customer_email'];
		$rCustomer->type = $data['customer_type'];
    $rCustomer->ident_no = empty($data['customer_ident_no'])? null : $data['customer_ident_no'];
		$rCustomer->vat_no = $data['customer_vat_no'];
		$rCustomer->discount = $data['customer_discount'];
		$rCustomer->discount_type = $data['customer_discount_type'];
    
    $rCustomer->id_wh_type = Table_TobaccoWarehouse::WH_TYPE;  
		
    $rCustomer->save();
    
    //přiřazení výchozího ceníku
    if ($addPricelist === TRUE) {
      $rPricelists = new Table_CustomersPricelists();
      
      //mazání přiřazených ceníků v případě změny typu zákazníka
      if ($deletePricelists === TRUE) {
        $rPricelists->delete('id_customers = ' . $rCustomer->id);
      }
      
      $rPricelists->insert(array(
          'id_customers' => $rCustomer->id,
          'id_pricelists' => $pricelist,
      ));
    }
		
    //výstup - id
    return $rCustomer->id;
	}
}
