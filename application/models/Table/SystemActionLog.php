<?php

class Table_SystemActionLog extends Zend_Db_Table_Abstract {
  protected $_name = 'system_action_log';
	protected $_primary = 'id';
  
  const ACTION_PART_ADD = 'add part';
  const ACTION_PART_REMOVE = 'remove part';
  
  public function addAction($action, $params, $user_action = NULL) {
    if (is_null($user_action)) {
      $tUAL = new Table_UsersActionLog();
      $select = $tUAL->select()
        ->order('id DESC')
        ->limit(1)
        ->query()
        ->fetch();
      $user_action = $select['id'];
    }    
    
    $this->insert(array(
        'id_users_action_log' => $user_action,
        'action'              => $action,
        'params'              => $params,
    ));
  }
}
