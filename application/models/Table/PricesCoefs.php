<?php

/**
 * Sady ceníků v závislosti na objednanem množství
 */
class Table_PricesCoefs extends Zend_Db_Table_Abstract {
    protected $_name = 'prices_coefs';
    protected $_primary = 'id';

    /**
     * Vrati vsechny zadefinovane body.
     * @return array Udaje o vsech bodech
     */
    public function getPoints() {
      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('pc' => $this->_name), array('id', 'point'))
        ->joinLeft(array('plb' => 'pricelists'), 'plb.id = pc.id_pricelists_b2b', 
          array('name_b2b' => 'name'))
        ->joinLeft(array('plc' => 'pricelists'), 'plc.id = pc.id_pricelists_b2c', 
          array('name_b2c' => 'name'))
        ->order('point ASC');
      return $select->query()->fetchAll();
    } 
    
    /**
     * Vrati bod podle ID.
     * @param int $id
     * @return array Udaje o nalezenem bodu
     */
    public function getPoint($id) {
      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('pc' => $this->_name), array('id', 'point'))
        ->joinLeft(array('plb' => 'pricelists'), 'plb.id = pc.id_pricelists_b2b', 
          array('name_b2b' => 'name', 'id_b2b' => 'id'))
        ->joinLeft(array('plc' => 'pricelists'), 'plc.id = pc.id_pricelists_b2c', 
          array('name_b2c' => 'name', 'id_b2c' => 'id'))
        ->where('pc.id = ?', $id);
      return $select->query()->fetch();
    } 
    
    /**
     * Najde bod s ceniky podle vahy.
     * @param float $point Vaha
     * @param bool $exactly Hledani presne nebo nejblizsi nizsi
     * @return array Udaje o nalezenem bodu
     */
    public function findPoint($point, $exactly = false) {
      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('pc' => $this->_name), array('id', 'point'))
        ->joinLeft(array('plb' => 'pricelists'), 'plb.id = pc.id_pricelists_b2b', 
          array('name_b2b' => 'name', 'id_b2b' => 'id'))
        ->joinLeft(array('plc' => 'pricelists'), 'plc.id = pc.id_pricelists_b2c', 
          array('name_b2c' => 'name', 'id_b2c' => 'id'))
        ->order('point DESC');
      if ($exactly) {
        $select->where('pc.point = ?', $point);
      } else {
        $select->where('pc.point <= ?', $point);
      }
      return $select->query()->fetch();
    }
}

