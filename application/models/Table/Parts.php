<?php
class Table_Parts extends Zend_Db_Table_Abstract{

	protected $_name = 'parts';

	protected $_primary = 'id';

	public function getDatalist($asSelect = false){
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(
            array('p' => $this->_name),
            array(
                'id',
                'name',
                'description',
                'id_parts_ctg',
                'version',
                'multipart' => new Zend_Db_Expr("IF(multipart = 'y', 'ano', 'ne')"),
				'product' => new Zend_Db_Expr("IF(product = 'y', 'ano', 'ne')"),
				'export',
                'price',
                'operations_cost'
            )
        )->join(
            array('pc' => 'parts_ctg'),
            "p.id_parts_ctg = pc.id",
            array(
                "ctg_name" => "name"
            )
        )->where(
            "deleted = 'n'"
        )->order(
            array('id_parts_ctg', 'id')
        );
		return ($asSelect ? $select : $this->fetchAll($select));
	}

	/**
	 * Vraci podsoucasti soucasti podle id
	 *
	 * @param $id_part
	 *
	 * @return \Zend_Db_Table_Rowset_Abstract
   */
	public function getSubpartsById($id_part){
		$select = $this->select();
		$select
    ->setIntegrityCheck(false)
    ->from(array('ps' => 'parts_subparts'), array('*'))
		->joinLeft(array('p' => 'parts'),
      "ps.id_parts = p.id",
      array('id_parts_ctg', 'name', 'price', 'operations_cost'))
    ->joinLeft(array('pc' => 'parts_collections'),
      'ps.id_parts = pc.id_parts',
       array('id_collections' => new Zend_Db_Expr('GROUP_CONCAT(pc.id_collections)')))
		->where("id_multipart = ?", $id_part)
    ->group('p.id');
		return $this->fetchAll($select);
	}

	/**
	 * Vraci soucasti, ktere se mohou stat podsoucastmi
	 * @return array
	 */

	public function getSubparts($collection = null){

    $select = $this->select();

    $subSelect = new Zend_Db_Select($select->getAdapter());
    $subSelect
      ->setIntegrityCheck(false)
      ->from('producers_parts')
      ->where('id_producers = p.id');

    $select
      ->setIntegrityCheck(false)
      ->from(array('p' => 'parts'), array(
        'id', 'id_wh_type', 'id_parts_ctg', 'name', 'description', 'measure',
        'version', 'end_price', 'price', 'old_price', 'hs', 'product',
        'multipart', 'semifinished', 'set', 'deleted', 'export', 'last_change',
        'hasParts' => new Zend_Db_Expr("EXISTS($subSelect)")
      ))
      ->joinLeft(array('pc' => 'parts_ctg'),
        "p.id_parts_ctg = pc.id",
        array('ctg_name' => 'name'))
      ->joinLeft(array('pcl' => 'parts_collections'),
        'p.id = pcl.id_parts', array('id_collections'))
      ->joinLeft(array('co' => 'collections'),
        'co.id = pcl = id_collections', array('c_name' => 'name'))
      ->where("p.deleted = 'n'")
      ->group('p.id');

		if($collection !== null){

      $subSelect2 = new Zend_Db_Select($select->getAdapter());
      $subSelect2
        ->setIntegrityCheck(false)
        ->from('parts_collections')
        ->where('id_parts = p.id');
      $select->where("? IN ($subSelect2) OR pc.id_collections IS NULL OR pc.id_collections = 0");
		}

		$rows = $this->fetchAll($select);
		$data = array();
		foreach($rows as $row){
			$data[$row->ctg_name][] = array(
				'id' => $row->id,
				'name' => $row->name,
				'description' => $row->description,
				'version' => $row->version,
				'multipart' => $row->multipart,
				'product' => $row->product,
				'hasParts' => $row->hasParts
			);
		}
		return $data;
	}

	/**
	 * Vraci pole vsech id soucasti
	 * @return array pole[id_part] = 0
	 */
	public function getPartsArray(){
		$rows = $this->fetchAll($this->select());
		$data = array();
		foreach($rows as $row){
			$data[$row->id] = 0;
		}
		return $data;
	}

	/**
	 * Vraci pole neslozenych soucasti
	 * @return array
	 */
	public function getSimplePartsArray(){
		$rows = $this->fetchAll($this->select()->where("multipart = 'n'"));
		$data = array();
		foreach($rows as $row){
			$data[$row->id] = 0;
		}
		return $data;
	}

	/**
         * Vraci pole soucasti podle id nebo nazvu
         */
        public function getProductByIdOrName($term){

		$select = $this->select();
		$select->from($this->_name, array('id', 'name'));
                $select->where('id LIKE "' . $term . '%" OR name LIKE "' . $term . '%"');
                $select->where('product = "y"');

		return $this->fetchAll($select);
	}

    /**
     * Vraci posledni id podle prefixu
     */
    public function getLatestPartID($term)
    {
        $select = $this->select();
        $select->from($this->_name, array('id'))
               ->where('id REGEXP "^' . $term . '+[[:digit:]]+"')
               ->order('id DESC')
               ->limit(1);

        return $this->fetchAll($select);
    }

    /**
     * Check ID exist
     * @param string $id
     * @return bool
     */
    public function checkIdExist($id)
    {
        $result = $this->fetchRow('id = "' . $id . '"');

        if(!$result) {
            return false;
        }

        return true;
    }

    /**
     * Get trash data
     * @return array
     */
    public function getTrashData($id_wh_type = Parts_Part::WH_TYPE_PIPES)
    {
        $select = $this->select();
        $select->from($this->_name)
               ->where("deleted = 'y'")
               ->where('id_wh_type = ?', $id_wh_type)
               ->order('id ASC');
        $data = $this->fetchAll($select);

        return $data->toArray();
    }

    public function getProductsSelect($collections = false) {

    $subSelect = $this->select()
      ->setIntegrityCheck(false)
      ->from('collections', array('name'))
      ->where('id IN ?', $this->select()
        ->setIntegrityCheck(false)
        ->from('parts_collections', array('id_collections'))
        ->where('id_parts = p.id'))
      ->limit(1);

    $subSelect1 = $this->select()
      ->setIntegrityCheck(false)
      ->from('collections', array('abbr'))
      ->where('id IN ?', $this->select()
        ->setIntegrityCheck(false)
        ->from('parts_collections', array('id_collections'))
        ->where('id_parts = p.id'))
      ->limit(1);

		$select = $this->select()
      ->setIntegrityCheck(false)
      ->from(array('p' => 'parts'))
      ->joinLeft(array('pt' => 'products_types'),
        "p.id_products_types = pt.id",
        array('type_name'))
      ->joinLeft(array('pa_ctg' => 'parts_ctg'),
        "p.id_parts_ctg = pa_ctg.id", array(
          'ctg_id' => 'id',
          'ctg_name' => 'name',
          'coll_name' => $this->getAdapter()->quote($subSelect),
          'coll_abbr' => $this->getAdapter()->quote($subSelect1)
          ))
      ->where("p.product = ?", 'y')
      ->where("p.deleted = ?", 'n')

      ->order(array('pa_ctg.ctg_order', 'p.id'));

      $where = '';
      if (is_array($collections)) {
        foreach ($collections as $coll) {
          if ($where == '') {
            $where = "EXISTS (SELECT id FROM parts_collections WHERE id_parts = p.id
              AND id_collections = ".$this->getAdapter()->quote($coll).")";
          } else {
            $where .= "OR EXISTS(SELECT id FROM parts_collections WHERE id_parts = p.id
              AND id_collections = ".$this->getAdapter()->quote($coll).")";
          }
        }
      }
			if(!empty($where)){
				$select->where($where);
			}


    //Zend_Debug::dump($select . ''); die;
      return $select;

    }

    private function ProductsWasteReport($ctg) {
      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(array('p' => $this->_name), array());
      $select->joinLeft(array('pw' => 'parts_warehouse'), 'p.id = pw.id_parts', array());
      $select->joinInner(array('pm' => 'parts_mixes'), 'p.id = pm.id_parts', array());
      $select->joinLeft(array('mr' => 'mixes_recipes'), 'pm.id_mixes = mr.id_mixes', array());
      $select->columns(array(
          'amount' => new Zend_Db_Expr('pw.amount * pm.amount'),
          'recipe_ratio' => new Zend_Db_Expr('GROUP_CONCAT(CONCAT(mr.id_recipes,",",mr.ratio) SEPARATOR ";")'),
          'recipe_ratio_sum' => new Zend_Db_Expr('SUM(mr.ratio)')
      ));
      $select->where('p.id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE);
      $select->where('p.id_parts_ctg = ?', $ctg);
      $select->group('p.id');

      return $select;
    }

    public function getProductsWasteReport($ctg) {
      $select = $this->ProductsWasteReport($ctg);
      $report = array();
      foreach ($select->query()->fetchAll() as $result) {
        if(empty($result['recipe_ratio_sum'])) {continue;}

        foreach (explode(';', $result['recipe_ratio']) as $ratio) {
          $ratio = explode(',', $ratio);

          if (array_key_exists($ratio[0], $report)) {
            $report[$ratio[0]] += $result['amount'] / $result['recipe_ratio_sum'] * $ratio[1];
          } else {
            $report[$ratio[0]] = $result['amount'] / $result['recipe_ratio_sum'] * $ratio[1];
          }
        }

      }

      return $report;
    }

    public function getProductsByCategory($ctg, $wh = Table_PartsWarehouse::WH_TYPE) {
      return $this->getPartsByCategory($ctg, TRUE, TRUE, $wh);
    }

    public function getPartsByCategory($ctg, $productsOnly = TRUE, $asSelect = TRUE, $wh = Table_PartsWarehouse::WH_TYPE) {
      $select = $this->select()->setIntegrityCheck(false)
        ->from(array('p' => $this->_name))
        ->joinLeft(array('pw' => 'parts_warehouse'), 'p.id=pw.id_parts', array('amount'))
        ->where('id_parts_ctg = ?', $ctg)
        ->where('id_wh_type = ?', $wh)
        ->where('deleted = ?', "n");
      if ($productsOnly) {
        $select->where('product = ?', "y");
      }
      return $asSelect ? $select : $select->query()->fetchAll();
    }

    public function getProducts($wh = Table_PartsWarehouse::WH_TYPE, $asSelect = FALSE) {
      $select = $this->select();
      $select->from(['p' => $this->_name]);
      $select->where('p.id_wh_type = ?', $wh);
      $select->where('p.product = ?', "y");
      $select->where('p.deleted = ?', "n");
      return $asSelect ? $select : $this->fetchAll($select);
    }

  /**
   * @throws \Parts_Exceptions_BadWH
   * @throws \Zend_Exception
   */
  public function checkCosts($wh = 1): array {
    $allParts = Parts::getParts($wh);
    $checked = [];
    foreach ($allParts as $product) {
      $part = new Parts_Part($product['id']);
      $part->checkCosts($checked);
    }
    uasort($checked, function ($a, $b) {
      if ($a['price'] === $b['price']) {
        return $a['id'] < $b['id'] ? -1 : 1;
      }
      else {
        return $a['price'] < $b['price'] ? -1 : 1;
      }
    });
    $parts = [];
    foreach ($checked as $id => $item) {
      $diff = round($item['costs'] - ($item['price'] + $item['inputs'] + $item['operations']), 2);
      if ($item['costs'] <= 0.0 || (($item['price'] !== 0.0 || $item['inputs'] !== 0.0 || $item['operations'] !== 0.0) && $diff !== 0.0)) {
        $parts[$id] = $item;
      }
    }
    return $parts;
  }

  public function getPartsForExport($wh = Table_PartsWarehouse::WH_TYPE, $thrownParts = FALSE) {

    // sklad dýmek
    if ($wh === Table_PartsWarehouse::WH_TYPE) {

      $opSelect = $this->select()->setIntegrityCheck(FALSE);
      $opSelect->from(['po' => 'parts_operations'], [
        'id_parts',
        'parts_operations' => new Zend_Db_Expr('SUM(price)'),
      ])->where('time <> 0')->group('id_parts');

      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(['pw' => 'parts_warehouse'], ['amount'])
        ->joinLeft(['pwe' => 'parts_warehouse_extern'], 'pwe.id_parts = pw.id_parts', ['id_extern_warehouses'])
        ->joinLeft(['p' => 'parts'], "pw.id_parts = p.id", [
          'id',
          'id_parts_ctg',
          'name',
          'description',
          'price' => new Zend_Db_Expr('p.price - IFNULL(ops.parts_operations, 0)'),
          'deleted',
        ])
        ->join(['pctg' => 'parts_ctg'],
          "p.id_parts_ctg = pctg.id " .
          "AND pctg.id NOT IN (" . $select->getAdapter()->quote(Table_PartsCtg::$reportExcuded) . ")", [
          'ctg_name' => 'name',
        ])
        ->joinLeft(['ops' => $opSelect], "p.id = ops.id_parts", [
          'meduse_operations' => new Zend_Db_Expr('IFNULL(ops.parts_operations, 0)'),
        ])
        ->where('p.id_wh_type = 1')
        ->where('p.deleted != ?', 'y')
        ->order(['p.id_parts_ctg', 'p.id']);

      // vyřazené součásti
      if ($thrownParts) {
        $select->having('pwe.id_extern_warehouses = ?',
          Table_PartsWarehouseExtern::THROWN_PARTS_WAREHOUSE_ID);
      }

      // nevyřazené součásti
      else {
        $select->having('pwe.id_extern_warehouses IS NULL')
          ->orHaving('pwe.id_extern_warehouses != ?',
            Table_PartsWarehouseExtern::THROWN_PARTS_WAREHOUSE_ID);
      }

      return $select->query()->fetchAll();
    }
    // sklad tabáku
    elseif ($wh === Table_TobaccoWarehouse::WH_TYPE) {

      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(['pw' => 'parts_warehouse'], ['amount'])
        ->joinLeft(['p' => 'parts'], "pw.id_parts = p.id", [
          'id',
          'id_parts_ctg',
          'name',
          'description',
          'price',
          'measure',
          'deleted',
        ])
        ->joinLeft(['pctg' => 'parts_ctg'], "p.id_parts_ctg = pctg.id", [
          'ctg_name' => 'name',
        ])
        ->where('p.id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE)
        ->where('p.id_parts_ctg != ?', Table_TobaccoWarehouse::CTG_DUTY_STAMPS)
        ->where('p.deleted != ?', 'y')
        ->order(['p.id_parts_ctg', 'p.id']);

      return $select->query()->fetchAll();
    }
    else {
      throw new Exception('Unknown WH type');
    }
  }


  public function getTree($partId, $withOperations = TRUE) {
    $subparts = $this->getSubpartsById($partId)->toArray();
    $tree = array();
    if ($subparts) {
      foreach($subparts as $subpart){
        $tree[$subpart['id_parts']] = array(
          'id' => $subpart['id_parts'],
          'name' => $subpart['name'],
          'amount' => $subpart['amount'],
          'price' => $withOperations ? $subpart['price'] : $subpart['price'] - $subpart['operations_cost'],
          'subparts' => $this->getTree($subpart['id_parts'], $withOperations)
        );
      }
    }
    return $tree;
  }

  public function findByProducer($id_producers) {
    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(array('p' => 'parts'),
      array('id', 'name', 'description', 'measure'));
    $select->join(array('pp' => 'producers_parts'),
      'p.id = pp.id_parts AND pp.is_deleted = 0 AND pp.id_producers = ' . $id_producers,
      array('producers_part_id', 'producers_part_name', 'price', 'min_amount' => new Zend_Db_Expr('IFNULL(pp.min_amount, 0)')));
    $select->where('p.deleted = ?', 'n');
    $result = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
    return $result;
  }

  /**
   * Vrati vlastni naklady na porizani a na operace k danym soucastem.
   * @param $partId
   *
   * @return array|null
   */
  public function getCostOperationSums($partId) {
    if (!$partId) {
      return NULL;
    }
    if (!is_array($partId)) {
      $partId = array($partId);
    }
    $select = $this->select()->setIntegrityCheck(FALSE)
      ->from(array('p' => 'parts'), array('id'))
      ->joinLeft(array('pp' => 'producers_parts'), 'pp.id_parts = p.id', array(
        'costs' => new Zend_Db_Expr('IFNULL(SUM(pp.price), 0)')
      ))
      ->joinLeft(array('po' => 'parts_operations'), 'po.id_parts = p.id', array(
        'operations' => new Zend_Db_Expr('IFNULL(SUM(po.price), 0)')
      ))
      ->where('p.id IN (?)', $partId)
      ->group('p.id');
    $query = $select->query(Zend_Db::FETCH_ASSOC);
    return $query->fetchAll();
  }

  public function getParts($wh = Table_PartsWarehouse::WH_TYPE, $asSelect = FALSE) {
    $select = $this->select();
    $select->setIntegrityCheck(FALSE);
    $select->from(['p' => 'parts']);
    $select->where('p.id_wh_type = ?', $wh);
    $select->where('deleted = ?', 'n');
    $select->join(['pc' => 'parts_ctg'], 'pc.id = p.id_parts_ctg', [
      'ctg_id' => 'id',
      'ctg_name' => 'name',
      'ctg_description' => 'description',
    ]);
    if ($asSelect) {
      return $select;
    }
    else {
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }
  }

  public function getClosingReport($year) {

    // Vsechny soucasti ve skladu tabaku s kategorii. Nesmazane.
    $select = $this->getParts(Tobacco_Parts_Part::WH_TYPE, TRUE);
    $select->order('name');

    // Omezeni reportu na hlidane polozky
    $select->where('p.id_parts_ctg IN (37, 38, 39) OR p.id = \'TS001\'');

    // Soucasny stav ve WH
    $select->join(['w' => 'parts_warehouse'], 'w.id_parts = p.id', ['amount']);

    // Historie zmen.
    $selectHistory = $select->getAdapter()->select();
    $selectHistory->from('parts_warehouse_history', [
      'id_parts',
      'amount_add' => new Zend_Db_Expr('SUM(IF(amount > 0, amount, 0))'),
      'amount_remove' => new Zend_Db_Expr('SUM(IF(amount < 0, -amount, 0))'),
    ]);
    $selectHistory->where('YEAR(last_change) = ?', $year);
    $selectHistory->group('id_parts');
    $select->joinLeft(['h' => $selectHistory], 'h.id_parts = p.id', [
      'amount_add' => new Zend_Db_Expr('IFNULL(amount_add, 0)'),
      'amount_remove' => new Zend_Db_Expr('IFNULL(amount_remove, 0)'),
    ]);

    // Pocatecni stav.
    $selectMin = $select->getAdapter()->select();
    $selectMin->from('parts_warehouse_snapshot', [
      'id_parts', 'date' => new Zend_Db_Expr('MIN(`date`)')
    ]);
    $selectMin->where('YEAR(date) = ?', $year);
    $selectMin->group('id_parts');
    $selectStart = $select->getAdapter()->select();
    $selectStart->from(['s' => 'parts_warehouse_snapshot'], ['id_parts', 'amount_real']);
    $selectStart->join(['m' => $selectMin], 'm.id_parts = s.id_parts AND m.date = s.date', []);
    $select->join(['s' => $selectStart], 's.id_parts = p.id', [
      'snap_start' => new Zend_Db_Expr('IFNULL(s.amount_real, 0)')
    ]);

    // Koncovy stav
    $selectMax = $select->getAdapter()->select();
    $selectMax->from('parts_warehouse_snapshot', ['id_parts', 'date' => new Zend_Db_Expr('MAX(`date`)')]);
    $selectMax->where('YEAR(date) = ?', $year);
    $selectMax->where('date != ?', $year . '-01-01');
    $selectMax->group('id_parts');
    $selectEnd = $select->getAdapter()->select();
    $selectEnd->from(['s' => 'parts_warehouse_snapshot'], ['snap_id' => 'id', 'id_parts', 'amount_real', 'date']);
    $selectEnd->join(['m' => $selectMax], 'm.id_parts = s.id_parts AND m.date = s.date', []);
    $select->joinLeft(['e' => $selectEnd], 'e.id_parts = p.id', [
      'snap_id',
      'amount_real',
      'amount_real_date' => 'date',
    ]);

    return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }

}