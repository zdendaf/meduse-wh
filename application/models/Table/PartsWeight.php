<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Table_PartsWeight extends Zend_Db_Table_Abstract {
    
    protected $_name = 'parts_weight';
    protected $_primary = 'id';
    
    public function insertUpdate(array $data) {
      
      if(empty($data['weight']) || $data['weight'] == '0') {
        $this->delete('id_parts = "' . $data['id_parts'] . '"');
        return;
      }
      
      $exists = $this->select()
        ->where('id_parts = ?', $data['id_parts'])
        ->query(Zend_Db::FETCH_ASSOC)
        ->fetch();
      if ($exists) {
        $this->update($data, 'id = ' . $exists['id']);
        return $exists['id'];
      } else {
        return $this->insert($data);
      }
    }
  }

  