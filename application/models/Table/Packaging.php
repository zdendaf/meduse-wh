<?php

class Table_Packaging extends Zend_Db_Table_Abstract {

  protected $_name = 'packings';
  protected $_primary = 'id';

  const TYPE_BOX = 'box';
  const TYPE_PALETTE = 'pallete';

  public static $translate = [self::TYPE_BOX => 'box', self::TYPE_PALETTE => 'paleta'];

}