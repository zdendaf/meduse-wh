<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Table_InvoiceItems extends Zend_Db_Table_Abstract {
    protected $_name = 'invoice_items';
    protected $_primary = 'id';    
  
    public function insertUpdate($data, $new_only = false) {
      if (!isset($data['id_parts'])) {
        throw new Exception('ID part must be given.');
      }
      if (!isset($data['id_invoice'])) {
        throw new Exception('ID invoice must be given.');
      }
      $row = $this->fetchRow('id_invoice = ' . $data['id_invoice'] 
        . ' AND id_parts ="' . $data['id_parts'] . '"');
      if ($row) {
        if (!$new_only) {
          $this->update($data, 'id = ' . $row->id);
        }
      } else {
        $tInvoice = new Table_Invoices();
        $data['id_orders'] = $tInvoice->find($data['id_invoice'])->current()->id_orders;
        $this->insert($data);
      }
    }
  }
  
  

  