<?php

class Table_Macerations extends Zend_Db_Table_Abstract {

	protected $_name = 'macerations';
	protected $_primary = 'id';


  public function getSelect(): Zend_Db_Select
  {
    return $this->select()->setIntegrityCheck(false);
  }

  public function getDataGrid($params, $type = 'active'): string
  {
      $select = $this->getSelect();

      switch ($type) {
          case 'zero':
              $where = 'amount_current <= 0 and discarded = \'n\''; break;
          case 'discarded':
              $where = 'discarded = \'y\''; break;
          default:
              $where = 'amount_current > 0 and discarded = \'n\''; break;
      }

      $select->where($where);
      $grid = new ZGrid(array(
        'css_class' => 'table table-hover table-condensed sticky',
        'add_link_url' => '/tobacco/macerations-add',
        'allow_add_link' => Zend_Registry::get('acl')->isAllowed('tobacco/macerations-add'),
        'curr_items_per_page' => -1,
        'add_link_title' => _('Nová macerace'),
        'columns' => array(
          'batch' => array(
            'header' => _('Šarže'),
            'css_style' => 'text-align: left',
            'sorting' => true,
            'renderer' => 'url',
            'url' => '/tobacco/macerations-detail/id/{id}'
          ),
          'name'   => array(
            'header' => _('Název'),
            'css_style' => 'text-align: left',
          ),
          'amount_in' => array(
            'header' => _('Nabaleno [kg]'),
            'css_style' => 'text-align: center; width: 5em;',
            'sorting' => false
          ),
          'amount_current' => array(
            'header' => _('Hmotnost nyní [kg]'),
            'css_style' => 'text-align: center; width: 5em;',
            'sorting' => false
          ),
          'start' => array(
            'header' => _('Založeno'),
            'css_style' => 'text-align: left',
            'renderer' => 'date',
            'sorting' => true
          ),
          'end' => array(
            'header' => _('Připraveno'),
            'css_style' => 'text-align: left',
            'renderer' => 'date',
            'sorting' => true
          ),
          'checked' => array(
            'header' => _('Kontrola'),
            'css_style' => 'text-align: left',
            'renderer' => 'date',
            'sorting' => true
          ),
          'check' => array(
              'header' => '',
              'css_style' => 'text-align: center',
              'renderer' => 'bootstrap_action',
              'type' => 'check',
              'url' => '/tobacco/macerations-snapshot-add/id/{id}',
          ),
          'storno' => array(
              'header' => '',
              'css_style' => 'text-align: center',
              'renderer' => 'bootstrap_action',
              'type' => 'remove',
              'url' => 'javascript:openStornoDialog(\'{batch}\')',
          ),

        )
      ));
      $grid->setSelect($select);
      $grid->setRequest($params);
      return $grid->render();
    }

    public function getTotalWeight() {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('m' => $this->_name),
        array('total_weight' => new Zend_Db_Expr('SUM(m.amount_current)')));
      $row = $select->query(Zend_Db::FETCH_ASSOC)->fetch();
      return $row['total_weight'];
    }

    public function getBarrelsCount() {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from($this->_name, array('count' => new Zend_Db_Expr('COUNT(id)')));
      $select->where('amount_current > 0');
      $row = $select->query(Zend_Db::FETCH_ASSOC)->fetch();
      return $row['count'];
    }

    public function getBarrelsByRecipes() {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('m' => $this->_name),
        array(
          'id_recipes',
          'count' => new Zend_Db_Expr('COUNT(m.id)'),
          'batches' => new Zend_Db_Expr('GROUP_CONCAT(m.batch SEPARATOR ", ")'),
          'weight' => new Zend_Db_Expr('SUM(m.amount_current)'),
        ));
      $select->joinLeft(array('r' => 'recipes'), 'r.id = m.id_recipes', array('name'));
      $select->where('m.amount_current > 0');
      $select->group('m.id_recipes')->order(array('r.name', 'm.batch'));
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }

    public function getBarrels($recipeIds = NULL, $empty = FALSE, $prices = FALSE) {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('m' => $this->_name),
        array(
          'id' => 'm.id',
          'batch' => 'm.batch',
          'weight' => 'm.amount_current',
          'start' => 'm.start',
          'end' => 'm.end',
          'days_left' => new Zend_Db_Expr('TIMESTAMPDIFF(DAY, NOW(), m.`end`)'),
          'name' => 'm.name',
          'amount_in' => 'm.amount_in',
          'amount_current' => 'm.amount_current',
        ));
      $select->joinLeft(array('r' => 'recipes'), 'r.id = m.id_recipes', array('recipe_name' => 'name'));
      // $select->where('m.discarded = ?', 'n');
      if ($recipeIds) {
        $recipeIds = is_array($recipeIds) ? $recipeIds : [$recipeIds];
        $select->where('id_recipes IN (?)', $recipeIds);
      }
      if ($prices) {
        $priceSelect = $this->getPrice(NULL, TRUE);
        $select->join(['p' => $priceSelect], 'p.maceration_id = m.id', ['barrel_in', 'barrel_current', 'unit']);
      }
      if (!$empty) {
        $select->where('m.amount_current > 0');
      }
      $select->order(array('r.name', 'm.batch'));

      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }

    /**
     * Kontrola limitního množství
     *
     * @param int $id ID
     */
    public function checkCritical($id) {
      $crit = $this->select()->setIntegrityCheck(false)
              ->from($this->_name)
              ->where('id = ?', $id)
              ->query()->fetch();

      if ($crit['amount_current']<=$crit['amount_critical']) {
        return array(
            'name' => $crit['name'],
            'batch' => $crit['batch']);
      } else {
        return FALSE;
      }
    }
    /**
     * Získání macerátů pod limitem
     *
     * @param int $id ID
     */
    public function getCritical() {
      $crit = $this->select()->setIntegrityCheck(false)
              ->from($this->_name)
              ->where('amount_current <= amount_critical')
              ->where('amount_current > 0')
              ->query()->fetchAll();
      return $crit;
    }

    public function getRelatedParts($macerationId) {

      // mixy, ktere obsahuji macerat
      $mSelect = $this->getAdapter()->select()
        ->from(['mr' => 'mixes_recipes'], [])
        ->join(['m' => 'mixes'], 'm.id = mr.id_mixes', ['id'])
        ->where('mr.id_recipes = ?', $macerationId);

      // nekolkovane produkty zjistenych mixu
      $taSelect = $this->getAdapter()->select()
        ->from(['pm' => 'parts_mixes'], [])
        ->join(['p' => 'parts'], 'p.id_wh_type = 2 AND p.id = pm.id_parts', ['id', 'name'])
        ->join(['m' => 'mixes'], 'm.id = pm.id_mixes', ['mix_id' => 'id', 'mix_name' => 'name'])
        ->join(['bf' => 'batch_flavors'], 'm.id_flavor = bf.id', ['flavor_id' => 'id', 'flavor_name' => 'name'])
        ->where('pm.id_mixes IN ?', $mSelect);

      // kolkovane produkty zjistenych mixu
      $tkSelect = $this->getAdapter()->select()
        ->from(['pm' => 'parts_mixes'], [])
        ->join(['ps' => 'parts_subparts'], ' ps.id_parts = pm.id_parts', [])
        ->join(['p' => 'parts'], 'p.id_wh_type = 2 AND p.id = ps.id_multipart', ['id', 'name'])
        ->join(['m' => 'mixes'], 'm.id = pm.id_mixes', ['mix_id' => 'id', 'mix_name' => 'name'])
        ->join(['bf' => 'batch_flavors'], 'm.id_flavor = bf.id', ['flavor_id' => 'id', 'flavor_name' => 'name'])
        ->where('pm.id_mixes IN ?', $mSelect);

      // spojeni selectu a vysledek
      $select = $this->getAdapter()->select()
        ->union([$taSelect, $tkSelect])
        ->order('flavor_name ASC')->order('id ASC');

      // $s = $select . '';

      $result = $select->query();
      $parts = [];
      while($row = $result->fetch()) {
        $parts[$row['id']] = $row;
      }
      return $parts;
    }

    public function getClosingReport($year) {

      // Maceraty a soucasny stav.
      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(['m' => 'macerations'], ['id', 'name', 'batch', 'amount' => 'amount_current']);
      $select->order('name');


      // Historie zmen sudu.
      $selectHistory = $select->getAdapter()->select();
      $selectHistory->from('macerations_history', [
        'id_macerations',
        'amount_add' => new Zend_Db_Expr('SUM(IF(amount > 0, amount, 0))'),
        'amount_remove' => new Zend_Db_Expr('SUM(IF(amount < 0, -amount, 0))'),
      ]);
      $selectHistory->where('YEAR(last_change) = ?', $year);
      $selectHistory->where('type != ? ', Table_MacerationsHistory::TYPE_CREATE);
      $selectHistory->group('id_macerations');
      $select->joinLeft(['h' => $selectHistory], 'h.id_macerations = m.id', [
        'amount_add' => new Zend_Db_Expr('IFNULL(amount_add, 0)'),
        'amount_remove' => new Zend_Db_Expr('IFNULL(amount_remove, 0)'),
      ]);

      // Vsechny zmeny maceratu dle receptu.
      $selectTotal = $select->getAdapter()->select();
      $selectTotal->from(['mh' => 'macerations_history'], [
        'remove_total' => new Zend_Db_Expr('SUM(-mh.amount)'),
      ]);
      $selectTotal->join(['m' => 'macerations'], 'mh.id_macerations = m.id', ['id_recipes']);
      $selectTotal->where('YEAR(mh.last_change) = ?', $year);
      $selectTotal->where('mh.type IN (?)', [Table_MacerationsHistory::TYPE_PACK, Table_MacerationsHistory::TYPE_BREAK]);
      $selectTotal->group('m.id_recipes');
      $select->joinLeft(['t' => $selectTotal], 't.id_recipes = m.id_recipes', [
        'remove_total' => new Zend_Db_Expr('IFNULL(remove_total, 0)'),
      ]);

      // Pocatecni stav.
      $selectMin = $select->getAdapter()->select();
      $selectMin->from('macerations_snapshot', [
        'id_macerations', 'date' => new Zend_Db_Expr('MIN(`date`)')
      ]);
      $selectMin->where('YEAR(date) = ?', $year);
      $selectMin->group('id_macerations');
      $selectStart = $select->getAdapter()->select();
      $selectStart->from(['s' => 'macerations_snapshot'], ['id_macerations', 'amount_real']);
      $selectStart->join(['m' => $selectMin], 'm.id_macerations = s.id_macerations AND m.date = s.date', []);
      $select->joinLeft(['s' => $selectStart], 's.id_macerations = m.id', [
        'snap_start' => new Zend_Db_Expr('IFNULL(s.amount_real, 0)')
      ]);

      // Koncovy stav
      $selectMax = $select->getAdapter()->select();
      $selectMax->from('macerations_snapshot', [
        'id_macerations', 'date' => new Zend_Db_Expr('MAX(`date`)')
      ]);
      $selectMax->where('YEAR(date) = ?', $year);
      $selectMax->where('date != ?', $year . '-01-01');
      $selectMax->group('id_macerations');
      $selectEnd = $select->getAdapter()->select();
      $selectEnd->from(['s' => 'macerations_snapshot'], ['snap_id' => 'id', 'id_macerations', 'amount_real', 'date']);
      $selectEnd->join(['m' => $selectMax], 'm.id_macerations = s.id_macerations AND m.date = s.date', []);
      $select->joinLeft(['e' => $selectEnd], 'e.id_macerations = m.id', [
        'snap_id',
        'amount_real',
        'amount_real_date' => 'date',
      ]);
      $select->having('snap_start > 0');
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }

    public function getPrice($maceration_id = NULL, $sql = FALSE) {

      $recipes = new Table_Recipes();
      $recipesSelect = $recipes->getUnitPrice(NULL, TRUE);

      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(['m' => 'macerations'], ['maceration_id' => 'id']);
      $select->join(['r' => $recipesSelect], 'r.recipe_id = m.id_recipes', [
        'unit' => 'unit_price',
        'barrel_in' => new Zend_Db_Expr('r.unit_price * m.amount_in'),
        'barrel_current' => new Zend_Db_Expr('r.unit_price * m.amount_current')
      ]);
      $select->group('m.id');

      if ($maceration_id) {
        $select->where('m.id = ?', $maceration_id);
      }

      if ($sql) {
        return $select;
      }
      elseif ($maceration_id) {
        return $select->query()->fetch();
      }
      else {
        return $select->query()->fetchAll();
      }
    }
}


