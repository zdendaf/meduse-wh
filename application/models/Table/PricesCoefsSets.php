<?php

/**
 * Sady koeficientů na změnu cen (v závislosti na obj. množství)
 *
 * @author Vojtěch Mrkývka
 */
class Table_PricesCoefsSets extends Zend_Db_Table_Abstract {
    protected $_name = 'prices_coefs_sets';
    protected $_primary = 'id';
}

?>
