<?php
/**
 * MISTO TETO TRIDY POUZIVAME 'Table_Parts'
 * TABULKA 'product' JE 'deprecated' A DO BUDOUCNA JI CHCEME ZE SYSTEMU UPLNE ODSTRANIT !
 */
class Table_Products extends Zend_Db_Table_Abstract{
	
	protected $_name = 'products';
	
	protected $_primary = 'id';
	
	public function addProduct($id, $name, $type, $collection, $desc){
		$data = array(
			"id" => $id,
			"name" => $name,
			"id_products_types" => $type,
			"id_collections" => $collection
		);
		if($desc != '') $data['description'] = $desc;
		$this->insert($data);
	}

	public function getProductsSelect($ctg) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array('p' => 'parts'))
		->joinLeft(array('pt' => 'products_types'), "p.id_products_types = pt.id", array('type_name'))
		->joinLeft(array('pa_ctg' => 'parts_ctg'), "p.id_parts_ctg = pa_ctg.id", array('ctg_name' => 'name'))
		->where("p.product = 'y'")
		->where("p.deleted = 'n'");

		if($ctg !== null && is_array($ctg)){
			$select->where("p.id_parts_ctg IN (?)", $ctg);
		} else {
			$select->where("p.id_parts_ctg NOT IN (?)", array(24, 25, 26, 27, 28)); //BEZ OBALU
		}
		return $select;
	}
	
	/**
	 * Vraci produkty podle kolekce nebo vsechny pri nevyplenem parametru
	 * pouziva tabulku parts a priznak product
	 * @param int $idColl kolekce
	 * @return multitype:
	 */
	public function getProducts($idColl = null, array $ctg = null, array $notCtg = null){
		
		$select = $this->getProductsSelect($ctg);
		$select->order(array('pa_ctg.name ASC','p.id'));

		if($idColl !== null){
			$select->where("EXISTS(SELECT id FROM parts_collections WHERE id_parts = p.id AND id_collections = ?)", $idColl);
		}

		if($notCtg !== null){
			$select->where("p.id_parts_ctg NOT IN (?)", $notCtg);
		}

		$rows = $this->fetchAll($select);
		$data = array();
		foreach($rows as $row){
//			$group = empty($row->ctg_name) ? $row->ctg_name : $row->type_name;
			$data[$row->ctg_name][] = array(
				'id' => $row->id,
				'name' => $row->name, 
				'description' => $row->description,
				'ctg_name' => $row->ctg_name,
				'type' => $row->id_products_types
			);
		} //Zend_Debug::dump($data); die();
		return $data;
	}
	
	public function getDatalist($collection){
		$part_table = new Table_Parts();
		$rows = $part_table->fetchAll($part_table->select()->where("product = 'y'")->where("deleted = 'n'")->where("EXISTS (SELECT * FROM parts_collections WHERE parts.id = parts_collections.id_parts AND id_collections IN (?))", $collection)->order(array('id_products_types', 'id_parts_ctg','id')));
		$data = array();
		foreach($rows as $row){
			if($row->id_products_types === null || strstr($row->id,"B")){
				$data['others'][] = array(
					'id' => $row->id,
					'name' => $row->name,
					'description' => $row->description,
					'ctg_name' => $part_table->getAdapter()->fetchOne("SELECT name FROM parts_ctg WHERE id = :id", array('id' => $row->id_parts_ctg))
				);
			} else {
				$data[$row->id_products_types][] = array(
					'id' => $row->id,
					'name' => $row->name,
					'description' => $row->description,
					'ctg_name' => null
				);
			}
		}
//		Zend_Debug::dump($data); die();
		return $data;
	}
	
	/**
	 * Vraci pole s id produktu
	 * @param $id_products_types
	 * @return array pole[id_products] = 0
	 */
	public function getProductsArray(){
		$select = $this->select();
		$rows = $this->fetchAll($select);
		$data = array();
		foreach($rows as $row){
			$data[$row->id] = 0;
		}
		return $data;
	}
	
	/**
	 * Vraci soucasti produktu
	 * @param $id
	 * @return unknown_type
	 */
	public function getParts($id){
		$select = $this->select();
		$select->setIntegrityCheck(false)
		->from(array('pp' => 'products_parts'))
		->joinLeft(array('p' => 'parts'), "pp.id_parts LIKE p.id", array('name', 'description', 'version', 'multipart', 'product' => new Zend_Db_Expr("IF(EXISTS( SELECT * FROM products WHERE id = p.id),'y','n')")))
		->joinLeft(array('pc' => 'parts_ctg'), "p.id_parts_ctg = pc.id", array('ctg_name' => 'name'))
		->where("pp.id_products LIKE ?", $id)
		//->orWhere("pp.id_products IN (SELECT id_parts_set FROM products_parts_set WHERE id_products LIKE '".$id."')") //soucasti setu, ktery je prirazen produktu
		->order("pp.id_parts","p.id_parts_ctg");
		$rows = $this->fetchAll($select);
		$data = array();
		foreach($rows as $row){
			$data[$row->ctg_name][] = array(
				'id' => $row->id_parts,
				'name' => $row->name, 
				'description' => $row->description,
				'version' => $row->version,
				'multipart' => $row->multipart,
				'product' => $row->product
			);
		}
		return $data;
	}
}
