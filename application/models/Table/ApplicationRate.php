<?php

class Table_ApplicationRate extends Zend_Db_Table_Abstract {
    
    const DEFAULT_RATE = 25;
  
    protected $_name = 'application_rate';
    protected $_primary = 'id';
    
    public function getRate($wh = 1) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, array('rate'))
          ->where('id_wh_type = ' . $wh)
          ->order(array('inserted DESC'))->limit(1);
        $rate = $select->query(Zend_Db::FETCH_ASSOC)->fetchColumn();
        return $rate == false ? self::DEFAULT_RATE : $rate;
    }
    
}


