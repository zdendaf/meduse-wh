<?php

class Table_Recipes extends Zend_Db_Table_Abstract {
	
  const STATE_ACTIVE = 'active';      // SHOW LEVEL = 0
  const STATE_INACTIVE = 'inactive';  // SHOW LEVEL = 1
  const STATE_HIDDEN = 'hidden';      // SHOW LEVEL = 2
  
	protected $_name = 'recipes';
	protected $_primary = 'id';
  
  protected $_data = array(
    
  );

  public function getSelect() {
    $select = $this->select()->setIntegrityCheck(false);
    $select->from($this->_name, array());
    return $select;
  }

    public function getRecipesPairs($showLevel = 0)
    {
        $states = [];
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, ['id', 'name']);
        $select->order('name ASC');
        switch ($showLevel) {
            case 2:
                $states[] = self::STATE_HIDDEN;
            case 1:
                $states[] = self::STATE_INACTIVE;
            case 0:
                $states[] = self::STATE_ACTIVE;
        }
        $select->where('state IN (?)', $states);
        return Utils::getDb()->fetchPairs($select);
    }

    public function getRecipeItems($recipeId, $totalWeight = 1000) {
    $select = $this->select()->setIntegrityCheck(false);
    $select->from(array('rp' => 'recipes_parts'), array('id', 'id_parts', 'ratio'));
    $select->joinLeft(array('p' => 'parts'), 'p.id = rp.id_parts', array('name'));
    $select->where('rp.id_recipes = ?', $recipeId);
    $data = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    if ($data) {
      $total = 0;
      foreach ($data as $item) {
        $total += $item['ratio']; 
      }
      foreach ($data as $key => $item) {
        $data[$key]['amount'] = round($data[$key]['ratio'] / $total * $totalWeight, 3); 
      }
    }
    return $data;    
  }

 	public function getRecipeParts($recipe_id) {
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('rp' => 'recipes_parts'), array('id', 'ratio', 'id_parts'))
				->joinLeft(array('p' => 'parts'), 'p.id = rp.id_parts', array('name'))
        ->joinLeft(array('pw' => 'parts_warehouse'), 'p.id = pw.id_parts', array('amount' => new Zend_Db_Expr('amount / 1000')))
				->where('rp.id_recipes = ?', $recipe_id);
		$data = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    $fixed = Tobacco_Recipes_Recipe::getFixedParts();
    foreach ($data as &$part) {
      $obj = new Tobacco_Parts_Part($part['id_parts']);
      $part['fixed'] = in_array($part['id_parts'], $fixed);
      $part['flavor'] = $obj->isFlavor();
    }

    return $data;
	}

  public function get($id) {
	  $select = $this->select()->where('id = ?', $id);
	  $data = $this->fetchRow($select);

	  $recipe = new Tobacco_Recipes_Recipe();
	  $recipe->setId($id)
        ->setFlavorId($data['id_flavor'])
			  ->setName($data['name'])
			  ->setDescription($data['description'])
			  ->setDue($data['due'])
			  ->setCreatedAt($data['created_at'])
			  ->setLastUpdate($data['last_update'])
			  ->setState($data['state']);

	  return $recipe;
  }

  public function insertPart($recipe_id, $part_id, $ratio) {
		$data = array(
			'id_recipes' => $recipe_id,
			'id_parts' => $part_id,
			'ratio' => $ratio
		);

		$this->getAdapter()->insert('recipes_parts', $data);
  }

  public function deletePart($entry_id) {
	  $adapter = $this->getAdapter();
	  $where = $adapter->quoteInto('id = ?', $entry_id);
	  $adapter->delete('recipes_parts', $where);
  }

  public function deleteAllParts($recipe_id) {
    $tRecipesParts = new Table_RecipesParts();
    $where = $tRecipesParts->getAdapter()->quoteInto('id_recipes = ?', $recipe_id);
    $tRecipesParts->delete($where);
  }

  public function getRecipesGrid($params) {

    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(['r' => 'recipes']);
    $select->join(['p' => $this->getUnitPrice(NULL, TRUE)], 'r.id = p.recipe_id');
    if (isset($params['status'])) {
      $select->where('r.state = ?', $params['status']);
    }
    else {
      $select->where('r.state in (?)', ['active', 'inactive']);
    }

    $grid = new ZGrid([
      'css_class' => 'table table-condensed table-hover sticky',
      'add_link_url' => '/tobacco/recipe-add',
      'allow_add_link' => Zend_Registry::get('acl')
        ->isAllowed('tobacco/recipe-add'),
      'add_link_title' => _('Přidat položku'),
      'columns' => [
        'name' => [
          'header' => 'Název',
          'sorting' => true,
          'renderer' => 'url',
          'url' => '/tobacco/recipe-detail/id/{id}',
        ],
        'created_at' => [
          'header' => 'Vytvořeno',
          'sorting' => true,
          'renderer' => 'date',
        ],
        'last_update' => [
          'header' => 'Upraveno',
          'renderer' => 'date',
          'sorting' => true,
        ],
        'unit_price' => [
          'header' => 'Jedn. cena',
          'renderer' => 'currency',
          'currency_options' => [
            'currency_value' => 'unit_price',
            'currency_symbol' => 'unit_symbol',
            'rounding_precision' => 2,
          ],
          'sorting' => true,
        ],
        'edit' => [
          'header' => '',
          'renderer' => 'bootstrap_Action',
          'type' => 'edit',
          'url' => '/tobacco/recipe-edit/id/{id}',
        ],
        'saveAs' => [
          'header' => '',
          'renderer' => 'bootstrap_Action',
          'type' => 'saveAs',
          'url' => '/tobacco/recipe-save-as/id/{id}',
        ],
        'delete' => [
          'header' => '',
          'renderer' => 'bootstrap_Action',
          'type' => 'delete',
          'url' => '/tobacco/recipe-delete/id/{id}',
        ],
      ],
    ]);
    $grid->setSelect($select);
    $grid->setRequest($params);
    return $grid->render();
  }

  public function getMacerationsWasteReport() {
    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(array('r' => $this->_name), array('id', 'name'));
    $select->joinLeft(
            array('m' => 'macerations'), 
            'r.id = m.id_recipes', 
            array('amount_in' => new Zend_Db_Expr('SUM(m.amount_in)'), 'amount_current' => new Zend_Db_Expr('SUM(m.amount_current)'))
    );
    $select->group('r.id');
    
    return $select;
  }

  public function getUnitPrice($recipe_id = NULL, $sql = FALSE) {

    $totalSelect = $this->select()->setIntegrityCheck(FALSE);
    $totalSelect->from('recipes_parts', ['id_recipes',
      'total' => new Zend_Db_Expr('SUM(ratio)')
    ]);
    $totalSelect->group('id_recipes');

    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(['rp' => 'recipes_parts'], ['recipe_id' => 'id_recipes']);
    $select->join(['p' => 'parts'], 'p.id = rp.id_parts', []);
    $select->join(['t' => $totalSelect], 't.id_recipes = rp.id_recipes', [
      'unit_price' => new Zend_Db_Expr('SUM(rp.ratio * p.price) / t.total'),
      'unit_symbol' => new Zend_Db_Expr('"Kč/kg"'),
    ]);
    $select->group('rp.id_recipes');

    if ($recipe_id) {
      $select->where('rp.id_recipes = ?', $recipe_id);
    }

    if ($sql) {
      return $select;
    }
    elseif ($recipe_id) {
      return $select->query()->fetch();
    }
    else {
      return $select->query()->fetchAll();
    }
  }
}


