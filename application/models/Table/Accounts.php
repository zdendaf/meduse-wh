<?php

class Table_Accounts extends Zend_Db_Table_Abstract {

    protected $_name = 'accounts';
    protected $_primary = 'id';

  /**
   * Vrati ID vychoziho bankovniho uctu zavisleho na WH, mene a pripadne i platebni metode.
   *
   * @param $id_wh_type ID skladu (1 nebo 2)
   * @param $currency (CZK nebo EUR)
   * @param null $payment_medhod (ID platebni metody, nepovinne)
   *
   * @return int|null ID platebni metody nebo NULL pri nenalezeni.
   */
    public function getDefaultAccountId($id_wh_type, $currency, $payment_medhod = null) {
      $id = NULL;
      // defaultni
      $select = $this->select()->setIntegrityCheck(false);
      $select->from($this, array('id'));
      $select->where('id_wh_type = ?', $id_wh_type);
      $select->where('currency = ?', $currency);
      $select->order('is_default DESC');
      // svazana platebni metoda
      if ($payment_medhod) {
        $selectPM = clone($select);
        $selectPM->where('payment_method = ?', $payment_medhod);
        $id = $selectPM->query()->fetchColumn();
      }
      if (!$id) {
        $id = $select->query()->fetchColumn();
      }
      return $id ? (int) $id : null;
    }

}

