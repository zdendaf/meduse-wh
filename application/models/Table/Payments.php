<?php
class Table_Payments extends Zend_Db_Table_Abstract{

	protected $_name = 'payments';
	protected $_primary = 'id';

  public function getAllPayments($wh = NULL, $params = array(), $only_select = FALSE) {
    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(array('p' => $this->_name), array(
      'id',
      'amount' => new Zend_Db_Expr('ROUND(amount, 2)'),
      'currency', 'rate', 'date',
      'invoice_no' => new Zend_Db_Expr('IF(i.type = "regular", i.no, o.proforma_no)'),
      'no',
      ));
    $select->joinLeft(array('i' => 'invoice'), 'p.id_invoice = i.id', array(
      'invoice_id' => 'id', 'invoice_type' => 'type'));
    $select->joinLeft(array('o' => 'orders'),  'o.id = i.id_orders', array(
      'order_id' => 'o.id', 'order_no' => 'o.no', 'order_title' => 'o.title'));
    if ($wh) {
      $select->where('i.id_wh_type = ?', $wh);
    }
    if ($params['sort']) {
      $sort = $params['sort'] . ' ' . ($params['sorder'] ? $params['sorder'] : 'ASC');
      $select->order($sort);
    }
    else {
      $select->order('date DESC');
    }
    return $only_select ? $select : $select->query()->fetchAll();
  }

  public function getPaymentsForInvoice($invoiceId) {
    return $this->select()
      ->where('id_invoice = ?', $invoiceId)
      ->query(Zend_Db::FETCH_OBJ)
      ->fetchAll();
  }

  public function getPaymentsSumForInvoice($invoiceId) {
    $select = $this->select()
      ->from('payments', array(new Zend_Db_Expr('SUM(amount)')))
      ->where('id_invoice = ?', $invoiceId)
      ->group('id_invoice');
    return (float) $select->query()->fetchColumn();
  }


  public static function getNextNo($wh = Table_PartsWarehouse::WH_TYPE) {
    $prefix = date('9y');
    $select = self::$_defaultDb->select()->from('payments', array('no'))->order('no desc')
      ->where('no LIKE ?', $prefix . '%')->where('id_wh_type = ?', $wh);
    $max = (int) $select->query()->fetchColumn();
    return ($max) ? $max + 1 : $prefix . '0001';
  }

  public static function checkNo($no, $wh = Table_PartsWarehouse::WH_TYPE) {
    $select = self::$_defaultDb->select()->from('payments')->order('no desc')
      ->where('no = ?', $no)->where('id_wh_type = ?', $wh);
    return FALSE === $select->query()->fetchColumn();
  }

  /**
   * Vraci seznam možných uhrad k odpočtu pro konkrétního zákazníka.
   *
   * @param $customerId
   * @return array
   * @throws Zend_Db_Statement_Exception
   */
  public function getPaymentsForDeduction($customerId): array
  {
    $subSelect = Utils::getDb()->select()
      ->from(['pd' => 'payments_deductions'], ['id_payments', 'deducted' => new Zend_Db_Expr('SUM(pd.amount)')])
      ->group('id_payments');

    $select = Utils::getDb()->select()
      ->from(['p' => $this->_name], ['id', 'amount', 'rate', 'currency', 'date', 'remain' => new Zend_Db_Expr('IFNULL(p.amount - s.deducted, p.amount)')])
      ->join(['i' => 'invoice'], 'p.id_invoice = i.id and i.type = \'proforma\'', ['id_invoice' => 'id'])
      ->join(['o' => 'orders'], 'i.id_orders = o.id and o.proforma_no is not null and o.id_customers = ' . Utils::getDb()->quote($customerId), ['proforma_no'])
      ->joinLeft(['s' => $subSelect], 's.id_payments = p.id', ['deducted' => new Zend_Db_Expr('IFNULL(deducted, 0)')])
      ->where('p.no is not null')
      ->where('p.date >= \'2019-01-01\'');

    $result = $select->query();
    $payments = [];
    while ($row = $result->fetch()) {
      $payments[$row['id']] = $row;
    }

    return $payments;
  }

}