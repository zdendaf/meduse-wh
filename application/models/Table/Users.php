<?php
class Table_Users extends Zend_Db_Table_Abstract {

	protected $_name = 'users';

	protected $_primary = 'id';


        public function getList($asSelect = false, $exceptAdmin = true)
        {
            $select = $this->select()->setIntegrityCheck(false);
            $select->from(
                array('u' => $this->_name),
                array(
                    'id',
                    'check_ip',
                    'is_active',
                    'username',
                    'name',
                    'name_full',
                    'email',
                    'phone',
              )
            )->join(
                array('up' => 'users_permissions'),
                'up.id_users = u.id',
                array()
            )->join(
                array('ur' => 'users_roles'),
                'ur.id = up.id_users_roles',
                array('role_translation' => new Zend_Db_Expr('GROUP_CONCAT(ur.role_translation SEPARATOR "<br>")'))
            )->where("u.is_delete = 0") //nesmazane
             ->group('u.id'); 
            if ($exceptAdmin) {
              $select->where("u.id != 1");  //vsichni krom admina
            }

            
            // filter by role
            $authNamespace = new Zend_Session_Namespace('Zend_Auth');
            if (!in_array('admin', $authNamespace->user_role)) {
              if (in_array('manager', $authNamespace->user_role)) {
                $select->where('ur.role_name <> ?', 'admin');
              } 
              else {
                $select->where('u.id = ?', $authNamespace->user_id);
              }              
            }
            return ($asSelect ? $select : $this->fetchAll($select));
        }

      /**
       * Vrati informace o uzivateli.
       * @param int $id
       *  ID uzivatele.
       * @param bool $filter
       *  Pokud je TRUE (vychozi hodnota), vrati se pozadovane udaje pouze Adminovi nebo Manazerovi.
       *  Ostatnim rolim se vrati data prihlaseneho uzivatele!
       *  Pokud je FALSE, vrati se data pozadovaneho uzivatele
       * @return mixed|null
       */
        public function getUser($id, $filter = TRUE) {

            $select = $this->select()
              ->setIntegrityCheck(false)
              ->from(array('u' => 'users'))
              ->joinLeft(array('up' => 'users_permissions'), 'up.id_users = u.id', array())
              ->joinLeft(array('ur' => 'users_roles'), 'ur.id = up.id_users_roles', array(
                'role_ids' => new Zend_Db_Expr('GROUP_CONCAT(ur.id)')))
              ->group('u.id');

            if ($filter) {
              $authNamespace = new Zend_Session_Namespace('Zend_Auth');

              // filter by role
              if (in_array('admin', $authNamespace->user_role)) {
                // show all
                $select
                  ->where('u.is_delete = 0')
                  ->where('u.id = ?', $id);
              }
              elseif(in_array('manager', $authNamespace->user_role)) {
                // show without admins
                $select
                  ->where('u.is_delete = 0')
                  ->where('ur.role_name <> ?', "admin")
                  ->where('u.id = ?', $id);
              }
              else {
                // show current user
                $select
                  ->where('u.is_delete = 0')
                  ->where('u.id = ?', $authNamespace->user_id);


              }
            } else {
                $select->where('u.id = ?', $id);
            }
            $data = $select->query(Zend_Db::FETCH_ASSOC)->fetch();
            // fix is null
            if(!$data) {
                return null;
            }
            return $data;
        }

    public function editUser($uid, $check_ip, $is_active, $username, $name, $name_full, $email, $phone, $role, $description, $expiration = 60) {
      $data = array(
        'username' => $username,
        'check_ip' => $check_ip,
        'name' => $name,
        'name_full' => $name_full,
        'email' => $email,
        'phone' => $phone,
        'description' => $description,
        'is_active' => $is_active,
        'expiration' => $expiration,
      );
      $this->update($data, 'is_delete = 0 AND id = ' . $uid);
      if (!empty($role)) {
        $tPerm = new Table_UsersPermissions();
        $tPerm->delete('id_users = ' . $uid);
        foreach ($role as $perm) {
          $tPerm->insert(array(
            'id_users' => $uid,
            'id_users_roles' => $perm
          ));
        }
      }
    }


    public function addUser($username, $name, $name_full, $email, $phone, $role, $description, $password, $is_active, $expiration = 60) {
      $salt = uniqid();
      $data = array(
        'username' => $username,
        'name' => $name,
        'password' => sha1($salt . $password),
        'password_hash' => $salt,
        'password_expiration' => '0000-00-00',
        'name_full' => $name_full,
        'email' => $email,
        'phone' => $phone,
        'description' => $description,
        'is_active' => $is_active == 'y',
        'expiration' => $expiration,
      );
      $uid = $this->insert($data);
      if (!empty($role)) {
        $tPerm = new Table_UsersPermissions();
        foreach ($role as $perm) {
          $tPerm->insert(array(
            'id_users' => $uid,
            'id_users_roles' => $perm
          ));
        }
      }      
    }


        public function changePwd($uid, $password) {

			$salt =  uniqid();
			$date = new DateTime('+' . Zend_Registry::get('config')->users->password_lifetime . ' days');

            $this->update(array(
				'password' => sha1($salt . $password),
				'password_expiration' => $date->format('Y-m-d'),
				'password_hash' => $salt
			), 'id = ' . $uid);
        }

        public function setDelete($uid)
        {
            $this->update(array('is_delete' => 1, 'is_active' => 0), 'id = ' . $uid);
        }

		public function getUserByName($username) {
			$user = $this->select()
				->from($this->_name)
				->where('username = ' . $this->getAdapter()->quote($username))
				->query(Zend_Db::FETCH_ASSOC)->fetchAll();
			return $user[0];
		}

		public function getSalt($userName) {
			return  $this->select()
					->from($this->_name, array('password_hash'))
					->where('username = ' . $this->getAdapter()->quote($userName))
					->query()->fetchColumn(0);
		}
}