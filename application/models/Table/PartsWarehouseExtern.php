<?php
class Table_PartsWarehouseExtern extends Zend_Db_Table_Abstract{
    
    protected $_name = 'parts_warehouse_extern';
    
    protected $_primary = 'id';

    const THROWN_PARTS_WAREHOUSE_ID = 1;

    /**
     * Vlozi do daneho externiho skladu pocet urcene soucasti
     * @param int $id_extern_warehouse
     * @param string $id_parts
     * @param int $amount
     * @return bool
     */
    public function insertPart($id_extern_warehouse, $id_parts, $amount){
        $authNamespace = new Zend_Session_Namespace('Zend_Auth');
        $old_amount = $this->getAdapter()->fetchOne("SELECT amount FROM parts_warehouse_extern WHERE id_parts = '".$id_parts."' AND id_extern_warehouses = $id_extern_warehouse");
        if(
            $old_amount
            || // nebo pokud zaznam existuje a pocet je 0
            $this->getAdapter()->fetchAll("SELECT * FROM parts_warehouse_extern WHERE id_parts = '".$id_parts."' AND id_extern_warehouses = $id_extern_warehouse")
        ) {
            $this->update(
                array(
                    "amount" => ($old_amount + $amount),
                    'id_users' => $authNamespace->user_id
                ),
                "id_parts = '".$id_parts."' AND id_extern_warehouses = $id_extern_warehouse"
            );
        } else {
            $this->insert(array(
                'id_extern_warehouses' => $id_extern_warehouse,
                'id_parts' => $id_parts,
                'amount' => $amount,
                'id_users' => $authNamespace->user_id
            ));
            $this->getAdapter()->insert('parts_warehouse_extern_history',array(
                'id_extern_warehouses' => $id_extern_warehouse,
                'id_parts' => $id_parts,
                'amount' => $amount,
                'id_users' => $authNamespace->user_id
            ));
        }
    }
    
    public function removePart($id_extern_warehouse, $id_parts, $amount){
        $authNamespace = new Zend_Session_Namespace('Zend_Auth');
        if($old_amount = $this->getAdapter()->fetchOne("SELECT amount FROM parts_warehouse_extern WHERE id_parts = '".$id_parts."' AND id_extern_warehouses = $id_extern_warehouse")){
            $this->update(array(
                "amount" => ($old_amount - $amount),
                'id_users' => $authNamespace->user_id
                ), "id_parts = '".$id_parts."' AND id_extern_warehouses = $id_extern_warehouse"
            );
            if(($old_amount - $amount) <= 0){
                $this->delete("id_parts = '".$id_parts."' AND id_extern_warehouses = $id_extern_warehouse");
            }
        } else throw new Exception("Pokus o odebrani $amount ks $id_parts z externiho skladu $id_extern_warehouse se nezdaril.");
    }
    
    /**
     * Updates existing rows. -- nahrada triggeru
     *
     * @param  array        $data  Column-value pairs.
     * @param  array|string $where An SQL WHERE clause, or an array of SQL WHERE clauses.
     * @return int          The number of rows updated.
     */
    public function update(array $data, $where){
        $authNamespace = new Zend_Session_Namespace('Zend_Auth');
        if(array_key_exists('amount', $data)) $new_amount = $data['amount']; else $new_amount = 0;
        $history_data = $this->fetchRow($where)->toArray();
        $data['id_users'] = $authNamespace->user_id;
        parent::update($data, $where);
        $data['amount'] = $new_amount - $history_data['amount'];
        $data['id_extern_warehouses'] = $history_data['id_extern_warehouses'];
        $data['id_parts'] = $history_data['id_parts'];
        $this->getAdapter()->insert('parts_warehouse_extern_history', $data);
    }
    
    public function getOverview($id_parts){
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('pwe' => 'parts_warehouse_extern'), array('amount'))
        ->joinLeft(array('ew' => 'extern_warehouses'), "pwe.id_extern_warehouses = ew.id", array('name', 'id'))
        ->where("id_parts = ?", $id_parts);
        return $this->getAdapter()->fetchAll($select);
    }
        
    /**
     * Vraci pocet soucasti v danem externim skladu
     * @param unknown_type $id_extern_warehouse
     * @param unknown_type $id_parts
     * @return unknown_type
     */
    public function getAmount($id_extern_warehouse, $id_parts){
        $select = $this->select();
        $select->where("id_extern_warehouses = ?", $id_extern_warehouse)
        ->where("id_parts = ?", $id_parts);
        $row = $this->getAdapter()->fetchRow($select);
        return $row['amount'];
    }

    public function getWarehouseByProducer($producerId) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from('producers', array('id_extern_warehouses'))->where("id = ?", $producerId);
        $row = $this->getAdapter()->fetchRow($select);
        if($row) {
            return $row['id_extern_warehouses'];
        } else return null;
    }

    public function getWhName($idWh) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from('extern_warehouses')->where("id = ?", $idWh);
        $row = $this->getAdapter()->fetchRow($select);
        return $row['name'];
    }

    public function getPartsForThrow($partsIds = [], $wh = Table_PartsWarehouse::WH_TYPE, $sql = FALSE) {
      /*
select p.name, pw.id_parts, pw.amount, pw.amount - (ifnull(sum(pwe.amount), 0)) as throw_amount
from parts p
         join parts_warehouse pw on p.id = pw.id_parts
         left join parts_warehouse_extern pwe on pw.id_parts = pwe.id_parts
where p.id_wh_type = 1
  and p.id in (?)
group by p.name, pw.id_parts, pw.amount;
       */

      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(['p' => 'parts'], ['name']);
      $select->join(['pw' => 'parts_warehouse'], 'p.id = pw.id_parts', ['id_parts', 'amount']);
      $select->joinLeft(['pwe' => 'parts_warehouse_extern'], 'pw.id_parts = pwe.id_parts', [
        'throw_amount' => new Zend_Db_Expr('pw.amount - (IFNULL(SUM(pwe.amount), 0))'),
      ]);
      $select->where('p.id_wh_type = ?', $wh);
      if ($partsIds) {
        $select->where('p.id IN (?)', $partsIds);
      }
      $select->group(['p.name', 'pw.id_parts', 'pw.amount']);

      if ($sql) {
        return $select;
      }
      else {
        $rows = [];
        $resource = $select->query();
        while($row = $resource->fetch()) {
          $rows[$row['id_parts']] = $row;
        }
        return $rows;
      }
    }

}
