<?php

class Table_Settings extends Zend_Db_Table_Abstract {

    const TYPE_INT = 'int';
    const TYPE_FLOAT = 'float';
    const TYPE_BOOL = 'bool';
    const TYPE_STRING = 'string';

    protected $_name = 'settings';
    protected $_primary = 'name';

    public function find() {
      $args = func_get_args();
      $row = parent::find($args)->current();
      if (!$row) {
        return NULL;
      } else {
        switch ($row->type) {
          case self::TYPE_INT: return (int) $row->value;
          case self::TYPE_FLOAT: return (float) $row->value;
          case self::TYPE_BOOL: return (bool) $row->value;
          default: return $row->value;
        }
      }
    }

    public static function set($name, $value, $type = NULL, $description = NULL) {
      $self = new Table_Settings();
      $data = array(
        'name' => $name,
        'value' => (string) $value,
        'type' => $type,
        'description' => $description,
      );
      if (!is_null($self->find($name))) {
        unset($data['name']);
        if (!$data['type']) {
          unset($data['type']);
        }
        if (!$data['description']) {
          unset($data['description']);
        }
        $self->update($data, 'name = "' . $name . '"');
      } else {
        $self->insert($data);
      }
    }

    public static function get($name, $default = NULL) {
      $self = new Table_Settings();
      $value = $self->find($name);
      return !$value ? $default : $value;
    }
}