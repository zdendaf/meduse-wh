<?php
  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Table_PartsBatch extends Zend_Db_Table_Abstract {
    
    protected $_name = 'parts_batch';
    protected $_primary = 'id';
  
    public function findByBatch($batch) {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from($this->_name)->where('batch = ?', $batch);
      $result = $select->query()->fetchAll(Zend_Db::FETCH_ASSOC);
      return $result;
    }
    
    public function findByPart($partId) {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from($this->_name)->where('id_parts = ?', $partId);
      $result = $select->query()->fetchAll(Zend_Db::FETCH_ASSOC);
      return $result;      
    }
    
    /**
     * Získává šarže v určeném množství podle ID parts.
     * 
     * @param int $partId ID parts
     * @param int $amount požadované množství
     * 
     * @return array Pole s šaržemi/množstvím.
     */
    public function getAmountByPart($partId, $amount) {
      //získání šarží
      $batches = $this->findByPart($partId);
      
      //určení šarží
      $batches_list = array();
      foreach ($batches as $batch) {
        if ($amount < $batch['amount']) {
          $batches_list[] = array('batch' => $batch['batch'], 'amount' => $amount);
          $amount = 0;
        } else {
          $batches_list[] = array('batch' => $batch['batch'], 'amount' => (int) $batch['amount']);
          $amount -= $batch['amount'];
        }
        // kontrola limitu
        if ($amount == 0) {
          break;
        }
      }
      
      //vrácení výsledku
      return $batches_list;
    }
    
    /**
     * Přidání množství kolků bez ovlivnění dalších součástí
     * 
     * @param string $part   ID parts
     * @param string $batch  šarže
     * @param int    $amount množství ku přidání
     */
    public function addBatchAmount($part, $batch, $amount) {
      $select = $this->select()
                     ->where('id_parts = ?', $part)
                     ->where('batch = ?', $batch)
                     ->query()->fetch(Zend_Db::FETCH_ASSOC);
      if ($select) {
        $this->update(array('amount' => ($amount + $select['amount'])), 'id = "' . $select['id'] . '"');
      } else {
        $this->insert(array(
              'id_parts' => $part,
              'batch'    => $batch,
              'amount'   => $amount,
            ));
      }
    }
    
    
    public function turn($partFrom, $partTo, $batch, $amount) {
      
      if (!is_null($partFrom)) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name);
        $select->where('id_parts = ?', $partFrom);
        $select->where('batch = ?', $batch);
        $result = $select->query()->fetch(Zend_Db::FETCH_ASSOC);

        if (!$result) {
          throw new Exception('Nebylo nalezen produkt ' 
            . $partFrom . ' se šarží ' . $batch);
        }
        if ((int)$result['amount'] < $amount) {
          throw new Exception('Nedostatené množství produktu ' 
            . $partFrom . ' se šarží ' . $batch 
            . '. Maximum je ' . $result['amount'] . '.');        
        }
      
      $this->update(array('amount' => (int)$result['amount'] - $amount), 
        'id = ' . $result['id']);
      }
      if (!is_null($partTo)) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name);
        $select->where('id_parts = ?', $partTo);
        $select->where('batch = ?', $batch);
        $result = $select->query()->fetch(Zend_Db::FETCH_ASSOC);

        if ( $result) {
          $this->update(array('amount' => (int)$result['amount'] + $amount), 
            'id = ' . $result['id']);        
        } else {
          $this->insert(array(
            'id_parts' => $partTo,
            'batch' => $batch,
            'amount' => (int)$amount,
          ));
        }
      }
      $this->delete('amount = 0');
    }
  }
