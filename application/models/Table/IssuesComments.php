<?php
class Table_IssuesComments extends Zend_Db_Table_Abstract {
	
	protected $_name = 'issues_comments';
	protected $_primary = 'id';
	
	public function getComments($id_issues) {
		
		$select = $this->_db->select()
			->from(array('c' => $this->_name))
			->joinLeft(array('u' => 'users'), 'u.id = c.id_users', array('username' => 'u.name_full'))
			->where('c.id_issues = ?', $id_issues);
		return $select->query()->fetchAll();				
	}
}

