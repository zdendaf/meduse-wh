<?php
class Table_BatchFlavors extends Zend_Db_Table_Abstract {

	protected $_name = 'batch_flavors';

	protected $_primary = 'id';
  
  public function getAssocArray() {
    $select = $this->select();    
    $data = $this->fetchAll($select);

    $result = array();
    foreach ($data as $it) {
      if ($it['active'] == 'y') {
        $result[$it['id']] = $it['name'] . ($it['is_mix'] == 'y' ? ' (mix)' : '');
      }
    }
    return $result;
  }
  
  public function getName($id) {
    $select = $this->select();
    $select->from($this->_name, array('name'));
    $where = $this->getAdapter()->quoteInto('id=?', $id);
    $select->where($where);
    return $this->getAdapter()->fetchOne($select);
  }

}
