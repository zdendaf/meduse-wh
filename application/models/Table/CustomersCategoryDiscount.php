<?php

class Table_CustomersCategoryDiscount extends Zend_Db_Table_Abstract {

    protected $_name = 'customers_parts_ctg_discount';
    protected $_primary = 'id';

    
    /**
     * Add new discount
     * 
     * @param int $customer_id
     * @param string $category_id
     * @param int $discount 
     */
    public function addDiscount($customer_id, $category_id, $discount)
    {       
        $data = array(
            'id_customers' => $customer_id,
            'id_parts_ctg' => $category_id,
            'discount' => $discount,
            'inserted' => null
        );
        
        $this->insert($data);
    }
    
    
    /**
     * Get category discount list
     * 
     * @param int $customer_id
     * @param bool $asSelect
     */
    public function getCategoryDiscountList($customer_id, $asSelect = false)
    {
        $select = $this->select();
	$select->setIntegrityCheck(false);
	$select->from(array('ccd' => $this->_name))
               ->joinLeft(array('pc' => 'parts_ctg'), "pc.id = ccd.id_parts_ctg", array('name'))
               ->where("ccd.id_customers = ?", $customer_id);
        
        return ($asSelect ? $select : $this->fetchAll($select));
    }
    
    
    
    /**
     * Check if record exist
     * 
     * @param int $customer_id
     * @param string $category_id
     * @return bool
     */
    public function checkExist($customer_id, $category_id)
    { 
        $data = $this->fetchRow('id_customers = ' . $customer_id . ' AND id_parts_ctg = ' . $category_id);

        if(!$data) {
            return false;
        }
        
        return true;
    }
}
