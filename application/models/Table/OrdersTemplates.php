<?php
class Table_OrdersTemplates extends Zend_Db_Table_Abstract{

	protected $_name = 'orders_templates';

	protected $_primary = 'id';

	public function getList() {
		$select = $this->select()->setIntegrityCheck(false);
		$select->from($this->_name);
		$select->joinLeft('addresses', "addresses.id = orders_templates.id_addresses", array('city','country'));
		return $this->fetchAll($select);
	}

	public function mapFromDb($form, $idOrder) {
		$row = $this->getAdapter()->fetchRow("SELECT * FROM orders WHERE id = :id", array('id' => $idOrder));
		if($row) {
			$form->meduse_orders_name->setValue($row['title']);
			$form->meduse_orders_desc->setValue($row['description']);
			$form->meduse_orders_carrier->setValue($row['id_orders_carriers']);
			$form->meduse_orders_payment_method->setValue($row['payment_method']);
      $form->meduse_orders_type->setValue($row['type']);
			$form->order->setValue($idOrder);
		}
	}

	public function mapAddressFromDb($form, $idOrder) {
		$order = new Orders_Order($idOrder);
		if (!is_null($order->getAddress())){
			$row = Zend_Registry::get('db')
        ->fetchRow('SELECT * FROM addresses WHERE id = ' . $order->_address);
			$form->getElement('street')->setValue($row['street']);
			$form->getElement('pop_number')->setValue($row['pop_number']);
			$form->getElement('orient_number')->setValue($row['orient_number']);
			$form->getElement('city')->setValue($row['city']);
			$form->getElement('zip')->setValue($row['zip']);
			$form->getElement('country')->setValue($row['country']);
		}
		return $order->_address;
	}
}
