<?php

class Table_Prices extends Zend_Db_Table_Abstract {

  protected $_name = 'prices';

  protected $_primary = 'id';

  function savePrice($id_parts, $name_part, $id_pricelists, $price, $includingVat = FALSE) {

    $price = $includingVat ? $price / (1 + Utils::getActualVat()) : $price;

    $part = new Parts_Part($id_parts);
    if ($part->isProduct()) {
      $select = $this->select();
      $select->from($this->_name, ['id']);
      $select->where('id_parts = ?', $id_parts);
      $select->where('id_pricelists = ?', $id_pricelists);
      $id = $select->query()->fetchColumn();

      if ($id) {
        $this->update(['price' => $price], 'id = ' . $id);
      }
      else {
        $this->insert([
          'id_parts' => $id_parts,
          'id_pricelists' => $id_pricelists,
          'price' => $price,
        ]);
      }
    }
    else {
      throw new Exception('Součást id=' . $id_parts . ' není produktem.');
    }
  }

  public function getProductPrices($idProduct, bool $includingVat = false): array
  {
    $part = new Parts_Part($idProduct);
    $vat = $includingVat ? 1 + Utils::getActualVat() : 1;
    $pid = $this->getAdapter()->quote($idProduct);
    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(['l' => 'pricelists']);
    $select->joinLeft(['p' => 'prices'], 'p.id_pricelists = l.id AND p.id_parts = ' . $pid, [
      'price' => new Zend_Db_Expr('price * ' . $vat),
    ]);
    $select->where('l.valid <= NOW()');
    $select->where('l.deleted = ?', 'n');
    $select->where('l.id_wh_type = ?', $part->getWarehouseTypeId());

    return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }

  public function getProductPrice($idProduct, $idPricelist, bool $includingVat = false) {
    $vat = $includingVat ? 1 + Utils::getActualVat() : 1;
    $pid = $this->getAdapter()->quote($idProduct);
    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(['p' => $this->_name], ['price'  => new Zend_Db_Expr('price * ' . $vat)]);
    $select->where('id_parts = ?', $idProduct);
    $select->where('id_pricelists = ?', $idPricelist);
    return $select->query(Zend_Db::FETCH_ASSOC)->fetchColumn();
  }

  public function getProductPricesAllPricelists($idProduct) {
    $product = new Parts_Part($idProduct);
    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(['l' => 'pricelists']);
    $select->joinLeft(['p' => 'prices'], 'p.id_pricelists = l.id AND p.id_parts = "' . $idProduct . '"', ['price']);
    $select->order(['l.valid DESC', 'l.id']);
    $select->where('l.deleted = ?', 'n');
    $select->where('l.id_wh_type = ?', $product->getWarehouseTypeId());
    return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }

  /**
   * Vrátí ceny z master ceníku.
   *
   */
  public function getProductMasterPrice($idProduct, $includingVat = FALSE): array {

    $vat = $includingVat ? 1 + Utils::getActualVat() : 1;

    $pid = $this->getAdapter()->quote($idProduct);
    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(['l' => 'pricelists']);
    $select->joinLeft(['p' => 'prices'], 'p.id_pricelists = l.id AND p.id_parts = ' . $pid, [
      'price' => new Zend_Db_Expr('price * ' . $vat),
    ]);
    $select->order(['l.valid DESC', 'l.id']);
    $select->where('l.deleted = ?', 'n');
    $select->where('l.master = ?', 1);
    return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }



}

