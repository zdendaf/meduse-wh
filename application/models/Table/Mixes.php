<?php
  class Table_Mixes extends Zend_Db_Table_Abstract {
    
    protected $_name = 'mixes';
    protected $_primary = 'id';

    const STATE_ACTIVE = 'active';      // SHOW LEVEL = 0
    const STATE_INACTIVE = 'inactive';  // SHOW LEVEL = 1
    const STATE_HIDDEN = 'hidden';      // SHOW LEVEL = 2    
    
    static public $state_names = array(
      self::STATE_ACTIVE   => 'Aktivní',
      self::STATE_INACTIVE => 'Neaktivní',
      self::STATE_HIDDEN   => 'N/A'
    );




    public function getSelect($params) {

      $tRecipes = new Table_MixesRecipes();
      $priceSelect = $tRecipes->getUnitPrice(NULL, TRUE);

      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(array('m' => 'mixes'), array(
        'id',
        'flavor' => new Zend_Db_Expr('CONCAT(LPAD(m.id_flavor, 3, "0"), ": ", bf.name)'),
        'name',
        'description',
        'create',
        'update',
        'state',
      ));
      $select->joinLeft(array('bf' => 'batch_flavors'), 'm.id_flavor = bf.id', array(
        'flavor_id' => 'id',
        'flavor_name' => 'name',
        ));
      $select->join(['p' => $priceSelect], 'p.id_mixes = m.id', ['unit_price', 'unit_symbol']);
      $select->order('bf.id');
      if (isset($params['status'])) {
        $select->where('state = ?', $params['status']);
      }
      else {
        $select->where('state <> ?', self::STATE_HIDDEN);
      }

      return $select;
    }    
    
    public function getDataGrid($params) {
      $select = $this->getSelect($params);
      $acl = Zend_Registry::get('acl');
      $grid = new ZGrid(array(
        'css_class' => 'table table-condensed table-hover sticky',
        'add_link_url' => '/tobacco/mixes-add',
        'allow_add_link' => $acl->isAllowed('tobacco/mixes-add'),
        'add_link_title' => _('Nový mix'),
        'columns' => array(
          'flavor' => array(
            'header' => _('Příchuť'),
            'sorting' => true,
          ),
          'name' => array(
            'header' => _('Název'),
            'sorting' => true,
            'renderer' => 'url',
            'url' => '/tobacco/mixes-detail/id/{id}'
          ),
          'create' => array(
            'header' => _('Vytvořeno'),
            'sorting' => true,
            'renderer' => 'date'
          ),
          'update' => array(
            'header' => _('Upraveno'),
            'renderer' => 'date',
            'sorting' => true
          ),
          'unit_price' => [
            'header' => 'Jedn. cena',
            'renderer' => 'currency',
            'currency_options' => [
              'currency_value' => 'unit_price',
              'currency_symbol' => 'unit_symbol',
              'rounding_precision' => 2,
            ],
            'sorting' => true,
          ],
        'edit' => array(
          'header' => '',
          'renderer' => 'bootstrap_Action',
          'type' => 'edit',
          'url' => '/tobacco/mixes-edit/id/{id}'
        ),
        'delete' => array(
          'header' => '',
          'renderer' => 'bootstrap_Action',
          'type' => 'delete',
          'url' => '/tobacco/mixes-delete/id/{id}'
        )),
      ));
      $grid->setSelect($select);
      $grid->setRequest($params);
      return $grid->render();      
    }
    
    public function insertRecipe($mix_id, $recipe_id, $ratio) {
      $data = array(
        'id_mixes' => $mix_id,
        'id_recipes' => $recipe_id,
        'ratio' => $ratio
      );
      $this->getAdapter()->insert('mixes_recipes', $data);      
    }
    
    public function deletePart($delete_id) {
      $adapter = $this->getAdapter();
      $where = $adapter->quoteInto('id = ?', $delete_id);
      $adapter->delete('mixes_recipes', $where);      
    }
    
    public function getMixRecipes($mixId) {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('mr' => 'mixes_recipes'), array('id', 'ratio', 'id_recipes'))
          ->joinLeft(array('r' => 'recipes'), 'r.id = mr.id_recipes', array('name'))
          ->where('mr.id_mixes = ?', $mixId);
      $data = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();    
      return $data;    
    }
    
    public function getMixesPairs($showLevel = 0) {    
      $select = $this->select()->setIntegrityCheck(false);
      $select->from($this->_name, array('id', 'name'));
      $select->order('name ASC');
      switch ($showLevel) {
        case 2:
          $states[] = self::STATE_HIDDEN;
        case 1:
          $states[] = self::STATE_INACTIVE;
        case 0:
          $states[] = self::STATE_ACTIVE;
      }
      $select->where('state IN (?)', $states);
      $pairs = Zend_Registry::get('db')->fetchPairs($select);
      return $pairs;    
    }


    public function getRelatedParts($mixId) {

      // nekolkovane produkty zjistenych mixu
      $taSelect = $this->getAdapter()->select()
        ->from(['pm' => 'parts_mixes'], [])
        ->join(['p' => 'parts'], 'p.id_wh_type = 2 AND p.id = pm.id_parts', ['id', 'name'])
        ->join(['m' => 'mixes'], 'm.id = pm.id_mixes', ['mix_id' => 'id', 'mix_name' => 'name'])
        ->join(['bf' => 'batch_flavors'], 'm.id_flavor = bf.id', ['flavor_id' => 'id', 'flavor_name' => 'name'])
        ->where('pm.id_mixes = ?', $mixId);

      // kolkovane produkty zjistenych mixu
      $tkSelect = $this->getAdapter()->select()
        ->from(['pm' => 'parts_mixes'], [])
        ->join(['ps' => 'parts_subparts'], ' ps.id_parts = pm.id_parts', [])
        ->join(['p' => 'parts'], 'p.id_wh_type = 2 AND p.id = ps.id_multipart', ['id', 'name'])
        ->join(['m' => 'mixes'], 'm.id = pm.id_mixes', ['mix_id' => 'id', 'mix_name' => 'name'])
        ->join(['bf' => 'batch_flavors'], 'm.id_flavor = bf.id', ['flavor_id' => 'id', 'flavor_name' => 'name'])
        ->where('pm.id_mixes = ?', $mixId);

      // spojeni selectu a vysledek
      $select = $this->getAdapter()->select()
        ->union([$taSelect, $tkSelect])
        ->order('flavor_name ASC')->order('id ASC');

      // $s = $select . '';

      $result = $select->query();
      $parts = [];
      while($row = $result->fetch()) {
        $parts[$row['id']] = $row;
      }
      return $parts;
    }

    public function getPartsForPacking($allowedAmounts = [0.01, 0.05, 0.25], $asSelect = FALSE) {
      $pmCondition = 'pm.id_mixes = mi.id AND pm.amount IN (' . implode(', ', $allowedAmounts) . ')';
      $select = $this->select()->setIntegrityCheck(FALSE);
      $select
        ->from(['mi' => 'mixes'], ['name_mixes' => 'name'])
        ->join(['pm' => 'parts_mixes'], $pmCondition, ['id_mixes', 'id_parts', 'amount'])
        ->join(['p' => 'parts'], 'p.id = pm.id_parts AND p.deleted = \'n\'', [])
        ->where('mi.state = ?', self::STATE_ACTIVE)
        ->order(['mi.name', 'pm.amount']);
      return $asSelect ? $select : $select->query()->fetchAll();
    }

    public function getBarrelsForPacking($asSelect = FALSE) {
      $mCondition = 'm.id_recipes = mr.id_recipes AND m.amount_current > 0 AND m.start <= NOW()';
      $select = $this->select()->setIntegrityCheck(FALSE);
      $select
        ->from(['mi' => 'mixes'], ['name_mixes' => 'name'])
        ->join(['mr' => 'mixes_recipes'], 'mr.id_mixes = mi.id', ['id_mixes', 'ratio'])
        ->join(['m' => 'macerations'], $mCondition, ['id_macerations' => 'id', 'batch', 'amount_current'])
        ->join(['r' => 'recipes'], 'r.id = m.id_recipes', ['id_flavor', 'name_flavor' => 'name'])
        ->where('mi.state = ?', self::STATE_ACTIVE)
        ->order(['mi.id', 'm.batch ASC']);
      return $asSelect ? $select : $select->query()->fetchAll();
    }

  }

