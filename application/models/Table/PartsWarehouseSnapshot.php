<?php

class Table_PartsWarehouseSnapshot extends Zend_Db_Table_Abstract {

  protected $_name = 'parts_warehouse_snapshot';
  protected $_primary = 'id';

  public function setSnapshot($id_parts, $amount_wh, $amount_real, $measure, $date, $id_users, $note = NULL, $isFixed = FALSE, $id_snapshot = NULL) {
    if ($id_snapshot) {
      $this->update([
        'id_parts' => $id_parts,
        'id_users' => $id_users,
        'amount_wh' => $measure == 'weight' ? round($amount_wh * 1000) : $amount_wh,
        'amount_real' => $measure == 'weight' ? round($amount_real * 1000) : $amount_real,
        'date' => $date,
        'note' => $note,
        'is_fixed' => $isFixed ? 'y' : 'n',
      ], 'id = ' . $id_snapshot);
    }
    else {
      $id_snapshot = $this->insert([
        'id_parts' => $id_parts,
        'id_users' => $id_users,
        'amount_wh' => $measure == 'weight' ? round($amount_wh * 1000) : $amount_wh,
        'amount_real' => $measure == 'weight' ? round($amount_real * 1000) : $amount_real,
        'date' => $date,
        'note' => $note,
        'is_fixed' => $isFixed ? 'y' : 'n',
      ]);
    }
    return $id_snapshot;
  }
}