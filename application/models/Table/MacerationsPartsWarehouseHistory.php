<?php


class Table_MacerationsPartsWarehouseHistory extends Zend_Db_Table_Abstract {
  protected $_name = 'macerations_parts_warehouse_history';
  protected $_primary = 'id';

  public function getPartsHistoryInfo($partId = NULL, Meduse_Date $date = NULL) {
    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(['h' => 'parts_warehouse_history'], ['id', 'amount', 'note']);
    $select->join(['mp' => 'macerations_parts_warehouse_history'], 'h.id = mp.id_parts_warehouse_history', ['id_macerations']);
    $select->join(['m' => 'macerations'], 'mp.id_macerations = m.id', ['batch', 'name']);
    $select->order('h.last_change ASC');
    if ($partId) {
      $select->where('id_parts = ?', $partId);
    }
    if ($date) {
      $from = $date->get('Y-m-d 00:00:00');
      $to = $date->get('Y-m-d 23:59:59');
      $select->where('h.last_change >= ?', $from);
      $select->where('h.last_change <= ?', $to);
    }
    return $select->query()->fetchAll();
  }
}