<?php

class Table_TobaccoWarehouse extends Table_PartsWarehouse {
	protected $_name = 'parts_warehouse';
	protected $_primary = 'id';

  /* ID z tabulky parts_ctg */
  const CTG_TOBACCO            = 34;

  const CTG_MATERIALS          = 35;
  const CTG_MATERIALS_TOBACCO  = 40;
  const CTG_MATERIALS_FLAVORS  = 41;
  const CTG_MATERIALS_OTHER    = 42;

  const CTG_PACKAGING          = 36;

  const CTG_PACKAGING_LABELS               = 49;
  const CTG_PACKAGING_LABELS_BOX_010       = 53;
  const CTG_PACKAGING_LABELS_BOX_050       = 54;
  const CTG_PACKAGING_LABELS_SACHET_010   = 55;

  const CTG_PACKAGING_SACHETS  = 50;
  const CTG_PACKAGING_BOXES    = 51;
  const CTG_PACKAGING_OTHER    = 52;

  const CTG_DUTY_STAMPS        = 38;

  const CTG_TOBACCO_NFS        = 37;
  const CTG_TOBACCO_FS         = 39;

  const CTG_ACCESSORIES        = 43;
  
  const WH_TYPE = 2;
	
  static $ctgToString = array(
    self::CTG_TOBACCO           => 'Tabák',

    self::CTG_MATERIALS         => 'Suroviny',
    self::CTG_MATERIALS_TOBACCO => 'Surový tabák',
    self::CTG_MATERIALS_FLAVORS => 'Příchutě',
    self::CTG_MATERIALS_OTHER   => 'Ostatní suroviny',

    self::CTG_PACKAGING         => 'Obaly',
    self::CTG_PACKAGING_LABELS  => 'Etikety',
    self::CTG_PACKAGING_LABELS_SACHET_010  => 'etikety na 10g sáčky',
    self::CTG_PACKAGING_LABELS_BOX_010  => 'etikety na 10g krabičky',
    self::CTG_PACKAGING_LABELS_BOX_050  => 'etikety na 50g krabičky',

    self::CTG_PACKAGING_SACHETS => 'Sáčky',
    self::CTG_PACKAGING_BOXES   => 'Krabičky',
    self::CTG_PACKAGING_OTHER   => 'Ostatní obalový materiál',

    self::CTG_DUTY_STAMPS       => 'Kolky',

    self::CTG_TOBACCO_NFS       => 'Produkty nekolkované',
    self::CTG_TOBACCO_FS        => 'Okolkované zboží',

    self::CTG_ACCESSORIES       => 'Doplňky',
  );
  
	public function init(){
		parent::init();
		$this->getAdapter()->setFetchMode(Zend_Db::FETCH_ASSOC);
	}
	
	public function getFiltredTobaccoWarehouseStatus(    
    $id_ctg = null, 
    $name = null, 
    $id = null, 
    $prod = null, 
    $producer = null, 
    $multi = null
  ) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
    
    $select->from('parts_warehouse', array(
      'amount' => $id_ctg == self::CTG_MATERIALS ? new Zend_Db_Expr('amount * 0.001') : 'amount',
      'critical_amount' => $id_ctg == self::CTG_MATERIALS ? new Zend_Db_Expr('critical_amount * 0.001') : 'critical_amount',
    ));
		$select->joinLeft(array('p' => 'parts'), 'id_parts LIKE p.id', array(
      'id',
      'id_parts_ctg',
      'name',
      'description',
      'multipart',
      'set',
      'product',
      'semifinished',
      'unit_price' => 'price',
      'unit_symbol' => new Zend_Db_Expr('IF(p.measure = "unit", "Kč/ks", "Kč/kg")')))
		->joinLeft(array('pc' => 'parts_ctg'), "p.id_parts_ctg = pc.id", array(
      'ctg_name' => 'name'))
		->joinLeft(array('pc2' => 'parts_ctg'), "pc2.id = pc.parent_ctg", array(
      'parent_ctg_name' => 'name'))
		->joinLeft(array('prod' => 'producers_parts'), "prod.id_parts = p.id", array(
      "id_producers", "producer_name" => 
      new Zend_Db_Expr("(SELECT name FROM producers AS tmp WHERE tmp.id = prod.id_producers)") ))
		->order(array('id_parts_ctg', 'p.id'));

		if(!empty($id_ctg) && $id_ctg != 'null'){
			$select->where("p.id_parts_ctg = ? OR (pc2.id = $id_ctg)", $id_ctg);
		}
		
		if(!empty($name) && $name != 'null'){
			$select->where("p.name LIKE ?", "%".$name."%");
		}
		
		if(!empty($id) && $id != 'null'){
			if(count(explode(",", $id)) > 1){
				$ids = explode(",", $id);
				$whereIds = array();
				foreach($ids as $tmpId){
					$whereIds[] = "p.id LIKE ".$this->getAdapter()->quote("%".$tmpId."%");
				}
				$where = implode(" OR ", $whereIds);
				$select->where($where);
			} else {
				$select->where("p.id LIKE ?", "%".$id."%");
			}
		}

		if(!empty($prod) && $prod != 'null' && $prod == 'y') {
			$select->where("p.product = 'y'");
		}

		if(!empty($producer) && $producer != 'null') {
			$select->where("prod.id_producers = ?", $producer);
		}

		if(!empty($multi) && $prod != 'null' && $multi == 'y') {
			$select->where("NOT EXISTS(SELECT * FROM parts_subparts WHERE id_multipart = p.id)");
		}
		$select->where("p.id_wh_type = ?", Parts_Part::WH_TYPE_TOBACCO);
    $select->where('p.deleted <> ?', 'y');

		return $select;
	}
  
  public function getFiltredWarehouseStatusAmountSum(
    $id_ctg = null, 
    $name = null, 
    $id = null, 
    $prod = null, 
    $producer = null, 
    $multi = null
  ) {
    $select = $this->getFiltredTobaccoWarehouseStatus($id_ctg, $name, $id, $prod, $producer, $multi);
    
    $select->columns(array('amount_sum' => new Zend_Db_Expr('SUM(amount) * 0.001')));
    
    return $select;
  }

  public function getDataGridMaterials($params) {
    $reg = Zend_Registry::get('tobacco_warehouse');
    $select = $this->getFiltredTobaccoWarehouseStatus(self::CTG_MATERIALS, $reg->filter_name, $reg->filter_id, NULL, $reg->filter_producer);
    $grid = new ZGrid([
      'css_class' => 'table table-condensed table-hover sticky',
      'add_link_url' => '/tobacco/parts-add/ctg/' . self::CTG_MATERIALS,
      'allow_add_link' => Zend_Registry::get('acl')
        ->isAllowed('tobacco/parts-add'),
      'add_link_title' => _('Přidat položku'),
      'columns' => [
        'id' => [
          'header' => _('ID'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'bootstrap_urlAnchor',
          'anchor' => '{id}',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'name' => [
          'header' => _('Název'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'url',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'description' => [
          'header' => _('Popis'),
          'css_style' => 'text-align: left; font-size: .85em;',
          'sorting' => FALSE,
        ],
        'unit_price' => [
          'header' => 'Jedn. cena',
          'css_style' => 'text-align: right; white-space: nowrap;',
          'renderer' => 'currency',
          'currency_options' => [
            'currency_value' => 'unit_price',
            'currency_symbol' => 'unit_symbol',
            'rounding_precision' => 2,
          ],
          'sorting' => TRUE,
        ],
        'amount' => [
          'header' => _('Množství&nbsp;[kg]'),
          'css_style' => 'text-align: right; width: 5em;',
          'sorting' => TRUE,
        ],
        'critical_amount' => [
          'header' => _('Kritické&nbsp;[kg]'),
          'css_style' => 'text-align: right; width: 5em;',
          'renderer' => 'bootstrap_InputText',
          'input_id' => 'ca_{id}',
          'input_class' => 'critical-amount',
          'input_size' => 5,
          'input_data' => [
            'part-id' => '{id}',
          ],
          'sorting' => TRUE,
        ],
        'producer_name' => [
          'header' => _('Dodavatel'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
        ],
        'add' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'bootstrap_Action',
          'type' => 'add',
          'url' => 'javascript:openAddDialog(\'{id}\', \'{name}\')',
        ],
      ],
    ]);
    $grid->setSelect($select);
    $grid->setRequest($params);
    return $grid->render();
  }

  public function getDataGridDutyStamps($params) {
    $reg = Zend_Registry::get('tobacco_warehouse');
    $select = $this->getFiltredTobaccoWarehouseStatus(self::CTG_DUTY_STAMPS, $reg->filter_name, $reg->filter_id, NULL, $reg->filter_producer);
    $grid = new ZGrid([
      'css_class' => 'table table-condensed table-hover sticky',
      'add_link_url' => '/tobacco/parts-add/ctg/' . self::CTG_DUTY_STAMPS,
      'allow_add_link' => Zend_Registry::get('acl')
        ->isAllowed('tobacco/parts-add'),
      'columns' => [
        'id' => [
          'header' => _('ID'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'bootstrap_urlAnchor',
          'anchor' => '{id}',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'name' => [
          'header' => _('Název'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'url',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'unit_price' => [
          'header' => 'Jedn. cena',
          'css_style' => 'text-align: right; white-space: nowrap;',
          'renderer' => 'currency',
          'currency_options' => [
            'currency_value' => 'unit_price',
            'currency_symbol' => 'unit_symbol',
            'rounding_precision' => 2,
          ],
          'sorting' => TRUE,
        ],
        'amount' => [
          'header' => _('Množství&nbsp;[ks]'),
          'css_style' => 'text-align: right; width: 5em;',
          'sorting' => TRUE,
        ],
        'critical_amount' => [
          'header' => _('Kritické&nbsp;[ks]'),
          'css_style' => 'text-align: right; width: 5em;',
          'renderer' => 'bootstrap_InputText',
          'input_id' => 'ca_{id}',
          'input_class' => 'critical-amount',
          'input_size' => 5,
          'input_data' => [
            'part-id' => '{id}',
          ],
          'sorting' => TRUE,
        ],
        'add' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'bootstrap_Action',
          'type' => 'add',
          'url' => 'javascript:openAddDialog(\'{id}\', \'{name}\')',
        ],
      ],
    ]);
    $grid->setSelect($select);
    $grid->setRequest($params);
    return $grid->render();
  }

  public function getDataGridPackaging($params, $subcategory = NULL) {
    $reg = Zend_Registry::get('tobacco_warehouse');
    $select = $this->getFiltredTobaccoWarehouseStatus(
      $subcategory ? $subcategory : self::CTG_PACKAGING,
      $reg->filter_name,
      $reg->filter_id,
      NULL,
      $reg->filter_producer
    );
    $s = $select. '';
    $grid = new ZGrid([
      'css_class' => 'table table-condensed table-hover sticky',
      'add_link_url' => '/tobacco/parts-add/ctg/' . self::CTG_PACKAGING,
      'allow_add_link' => Zend_Registry::get('acl')
        ->isAllowed('tobacco/parts-add'),
      'add_link_title' => _('Přidat položku'),
      'columns' => [
        'id' => [
          'header' => _('ID'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'bootstrap_urlAnchor',
          'anchor' => '{id}',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'name' => [
          'header' => _('Název'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'url',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'description' => [
          'header' => _('Popis'),
          'css_style' => 'text-align: left; font-size: .85em;',
          'sorting' => FALSE,
        ],
        'unit_price' => [
          'header' => 'Jedn. cena',
          'css_style' => 'text-align: right; white-space: nowrap;',
          'renderer' => 'currency',
          'currency_options' => [
            'currency_value' => 'unit_price',
            'currency_symbol' => 'unit_symbol',
            'rounding_precision' => 2,
          ],
          'sorting' => TRUE,
        ],
        'amount' => [
          'header' => _('Množství&nbsp;[ks]'),
          'css_style' => 'text-align: right; width: 5em;',
          'sorting' => TRUE,
        ],
        'critical_amount' => [
          'header' => _('Kritické&nbsp;[ks]'),
          'css_style' => 'text-align: right; width: 5em;',
          'renderer' => 'bootstrap_InputText',
          'input_id' => 'ca_{id}',
          'input_class' => 'critical-amount',
          'input_size' => 5,
          'input_data' => [
            'part-id' => '{id}',
          ],
          'sorting' => TRUE,
        ],
        'producer_name' => [
          'header' => _('Dodavatel'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
        ],
        'add' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'bootstrap_Action',
          'type' => 'add',
          'url' => 'javascript:openAddDialog(\'{id}\', \'{name}\')',
        ],
      ],
    ]);
    $grid->setSelect($select);
    $grid->setRequest($params);
    return $grid->render();
  }

  public function getDataGridTobaccoNFS($params) {
    $reg = Zend_Registry::get('tobacco_warehouse');
    $select = $this->getFiltredTobaccoWarehouseStatus(self::CTG_TOBACCO_NFS, $reg->filter_name, $reg->filter_id, NULL, $reg->filter_producer);
    $grid = new ZGrid([
      'allow_paginator' => FALSE,
      'curr_items_per_page' => 0,
      'css_class' => 'table table-condensed table-hover sticky',
      'add_link_url' => '/tobacco/parts-add/ctg/' . self::CTG_TOBACCO_NFS,
      'allow_add_link' => Zend_Registry::get('acl')
        ->isAllowed('tobacco/parts-add'),
      'add_link_title' => _('Přidat položku'),
      'columns' => [
        'id' => [
          'header' => _('ID'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'bootstrap_urlAnchor',
          'anchor' => '{id}',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'name' => [
          'header' => _('Název'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'url',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'description' => [
          'header' => _('Popis'),
          'css_style' => 'text-align: left; font-size: .85em;',
          'sorting' => FALSE,
        ],
        'unit_price' => [
          'header' => 'Jedn. cena',
          'css_style' => 'text-align: right; white-space: nowrap;',
          'renderer' => 'currency',
          'currency_options' => [
            'currency_value' => 'unit_price',
            'currency_symbol' => 'unit_symbol',
            'rounding_precision' => 2,
          ],
          'sorting' => TRUE,
        ],
        'amount' => [
          'header' => _('Množství&nbsp;[ks]'),
          'css_style' => 'text-align: right; width: 5em;',
          'sorting' => TRUE,
        ],
        'pack' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'bootstrap_action',
          'type' => 'add',
          'url' => '/tobacco/parts-pack/id/{id}',
        ],
        'stamp' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'bootstrap_action',
          'type' => 'check',
          'url' => '/tobacco/parts-stamp/id/{id}',
        ],
      ],
    ]);
    $grid->setSelect($select);
    $grid->setRequest($params);

    return $grid->render();
  }

  public function getDataGridTobaccoFS($params) {
    $reg = Zend_Registry::get('tobacco_warehouse');
    $select = $this->getFiltredTobaccoWarehouseStatus(self::CTG_TOBACCO_FS, $reg->filter_name, $reg->filter_id, NULL, $reg->filter_producer);
    $grid = new ZGrid([
      'allow_paginator' => FALSE,
      'curr_items_per_page' => 0,
      'css_class' => 'table table-condensed table-hover sticky',
      'add_link_url' => '/tobacco/parts-add/ctg/' . self::CTG_TOBACCO_FS,
      'allow_add_link' => Zend_Registry::get('acl')
        ->isAllowed('tobacco/parts-add'),
      'add_link_title' => _('Přidat položku'),
      'columns' => [
        'id' => [
          'header' => _('ID'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'bootstrap_urlAnchor',
          'anchor' => '{id}',
          'attribs' => ['id' => '{id}', 'class' => 'jump'],
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'name' => [
          'header' => _('Název'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'url',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'description' => [
          'header' => _('Popis'),
          'css_style' => 'text-align: left; font-size: .85em;',
          'sorting' => FALSE,
        ],
        'unit_price' => [
          'header' => 'Jedn. cena',
          'css_style' => 'text-align: right; white-space: nowrap;',
          'renderer' => 'currency',
          'currency_options' => [
            'currency_value' => 'unit_price',
            'currency_symbol' => 'unit_symbol',
            'rounding_precision' => 2,
          ],
          'sorting' => TRUE,
        ],
        'amount' => [
          'header' => _('Množství&nbsp;[ks]'),
          'css_style' => 'text-align: right; width: 5em;',
          'sorting' => TRUE,
        ],
      ],
    ]);
    $grid->setSelect($select);
    $grid->setRequest($params);
    return $grid->render();
  }

  public function getDataGridAccessories($params) {
    $reg = Zend_Registry::get('tobacco_warehouse');
    $select = $this->getFiltredTobaccoWarehouseStatus(self::CTG_ACCESSORIES, $reg->filter_name, $reg->filter_id, NULL, $reg->filter_producer);
    $grid = new ZGrid([
      'css_class' => 'table table-condensed table-hover sticky',
      'add_link_url' => '/tobacco/parts-add/ctg/' . self::CTG_ACCESSORIES,
      'allow_add_link' => Zend_Registry::get('acl')
        ->isAllowed('tobacco/parts-add'),
      'add_link_title' => _('Přidat položku'),
      'columns' => [
        'id' => [
          'header' => _('ID'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'bootstrap_urlAnchor',
          'anchor' => '{id}',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'product' => [
          'header' => _('Produkt'),
          'css_style' => 'text-align: center',
          'sorting' => TRUE,
          'renderer' => 'bootstrap_Flag',
          'states' => ['n', 'y'],
        ],
        'name' => [
          'header' => _('Název'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'url',
          'url' => '/tobacco/parts-detail/id/{id}',
        ],
        'unit_price' => [
          'header' => 'Jedn. cena',
          'css_style' => 'text-align: right; white-space: nowrap;',
          'renderer' => 'currency',
          'currency_options' => [
            'currency_value' => 'unit_price',
            'currency_symbol' => 'unit_symbol',
            'rounding_precision' => 2,
          ],
          'sorting' => TRUE,
        ],
        'amount' => [
          'header' => _('Množství&nbsp;[ks]'),
          'css_style' => 'text-align: right; width: 5em;',
          'sorting' => TRUE,
        ],
        'critical_amount' => [
          'header' => _('Kritické&nbsp;[ks]'),
          'css_style' => 'text-align: right; width: 5em;',
          'renderer' => 'bootstrap_InputText',
          'input_id' => 'ca_{id}',
          'input_class' => 'critical-amount',
          'input_size' => 5,
          'input_data' => [
            'part-id' => '{id}',
          ],
          'sorting' => TRUE,
        ],
        'producer_name' => [
          'header' => _('Dodavatel'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
        ],
        'add' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'bootstrap_Action',
          'type' => 'add',
          'url' => 'javascript:openAddDialog(\'{id}\', \'{name}\')',
        ],
      ],
    ]);
    $grid->setSelect($select);
    $grid->setRequest($params);
    return $grid->render();
  }

  public function stamp($partId, $multiPartId, $batch, $amount) {
      $this->addPart($multiPartId, $amount);
      $tPB = new Table_PartsBatch();
      $tPB->turn($partId, $multiPartId, $batch, $amount);
    }
    
    public function getProductsOverviewNFS() {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('p' => 'parts'), array());
      $select->joinLeft(array('pw' => 'parts_warehouse'), 'p.id = pw.id_parts',
        array('amount' => new Zend_Db_Expr('SUM(pw.amount)')));
      $select->joinInner(array('pr' => 'parts_mixes'), 'p.id = pr.id_parts',
        array('total_weight' => new Zend_Db_Expr('SUM(pw.amount * pr.amount)')));
      $select->where(' p.id_parts_ctg = ?', self::CTG_TOBACCO_NFS);
      return $select->query(Zend_Db::FETCH_ASSOC)->fetch();
    }
    
    public function getProductsOverviewFS() {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('p' => 'parts'), array());
      $select->joinLeft(array('pw' => 'parts_warehouse'), 'p.id = pw.id_parts',
        array('amount' => new Zend_Db_Expr('pw.amount')));
      $select->joinLeft(array('ps' => 'parts_subparts'), 'p.id = ps.id_multipart', array());      
      $select->joinInner(array('pr' => 'parts_mixes'), 'ps.id_parts = pr.id_parts',
        array('total_weight' => new Zend_Db_Expr('pw.amount * pr.amount')));
      $select->where(' p.id_parts_ctg = ?', self::CTG_TOBACCO_FS);
      return $select->query(Zend_Db::FETCH_ASSOC)->fetch();
    }
    
    public function getProductsByMixNFS() {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('pb' => 'parts_batch'), array('batch' => new Zend_Db_Expr('GROUP_CONCAT(pb.batch SEPARATOR ", ")'), 'amount' => new Zend_Db_Expr('SUM(pb.amount)')));
      $select->joinLeft(array('p' => 'parts'), 'pb.id_parts = p.id', array('id', 'name'));
      $select->joinInner(array('pr' => 'parts_mixes'), 'p.id = pr.id_parts',
        array('weight' => 'amount', 'total_weight' => new Zend_Db_Expr('SUM(pb.amount * pr.amount)')));
      $select->joinLeft(array('r' => 'mixes'), 'pr.id_mixes = r.id',
        array('mix' => 'name'));
      $select->where('p.id_parts_ctg = ?', self::CTG_TOBACCO_NFS);
      $select->where('pb.amount > 0'); 
      $select->group('r.id');
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }
    
    public function getProductsByMixFS() {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('pb' => 'parts_batch'), array('batch' => new Zend_Db_Expr('GROUP_CONCAT(pb.batch SEPARATOR ", ")'), 'amount' => new Zend_Db_Expr('SUM(pb.amount)')));
      $select->joinLeft(array('p' => 'parts'), 'pb.id_parts = p.id', array('id', 'name'));
      $select->joinInner(array('pr' => 'parts_mixes'), 'p.id = pr.id_parts',
        array('weight' => 'amount', 'total_weight' => new Zend_Db_Expr('SUM(pb.amount * pr.amount)')));
      $select->joinLeft(array('r' => 'mixes'), 'pr.id_mixes = r.id',
        array('mix' => 'name'));
      $select->where('p.id_parts_ctg = ?', self::CTG_TOBACCO_FS);
      $select->where('pb.amount > 0'); 
      $select->group('r.id');
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }    
    
    public function getProductsByPackNFS() {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('pb' => 'parts_batch'), array('batch' => new Zend_Db_Expr('GROUP_CONCAT(pb.batch SEPARATOR ", ")'), 'amount' => new Zend_Db_Expr('SUM(pb.amount)')));
      $select->joinLeft(array('p' => 'parts'), 'pb.id_parts = p.id', array('id', 'name'));
      $select->joinInner(array('pr' => 'parts_mixes'), 'p.id = pr.id_parts',
        array('weight' => 'amount', 'total_weight' => new Zend_Db_Expr('SUM(pb.amount * pr.amount)')));
      $select->joinLeft(array('r' => 'mixes'), 'pr.id_mixes = r.id',
        array('mix' => 'name'));
      $select->where('p.id_parts_ctg = ?', self::CTG_TOBACCO_NFS);
      $select->where('pb.amount > 0'); 
      $select->group('p.id');
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }

    public function getProductsByPackFS() {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('pb' => 'parts_batch'), array('batch' => new Zend_Db_Expr('GROUP_CONCAT(pb.batch SEPARATOR ", ")'), 'amount' => new Zend_Db_Expr('SUM(pb.amount)')));
      $select->joinLeft(array('p' => 'parts'), 'pb.id_parts = p.id', array('id', 'name'));
      $select->joinInner(array('pr' => 'parts_mixes'), 'p.id = pr.id_parts',
        array('weight' => 'amount', 'total_weight' => new Zend_Db_Expr('SUM(pb.amount * pr.amount)')));
      $select->joinLeft(array('r' => 'mixes'), 'pr.id_mixes = r.id',
        array('mix' => 'name'));
      $select->where('p.id_parts_ctg = ?', self::CTG_TOBACCO_FS);
      $select->where('pb.amount > 0'); 
      $select->group('p.id');
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }
    
    public function getProductsReport($category) {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('pb' => 'parts_batch'), array('batch', 'amount'));
      $select->joinLeft(array('p' => 'parts'), 'pb.id_parts = p.id', array('id', 'name'));
      $select->joinInner(array('pr' => 'parts_mixes'), 'p.id = pr.id_parts',
        array('weight' => 'amount', 'total_weight' => new Zend_Db_Expr('pb.amount * pr.amount')));
      $select->joinLeft(array('r' => 'mixes'), 'pr.id_mixes = r.id',
        array('mix' => 'name'));
      $select->where('p.id_parts_ctg = ?', $category);
      $select->where('pb.amount > 0');
      
      return $select;
    }
    
    public function getProductsReportNFS() {
      $select = $this->getProductsReport(self::CTG_TOBACCO_NFS);
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }
    
    public function getProductsReportFS() {
      $select = $this->getProductsReport(self::CTG_TOBACCO_FS);
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }
    
    public function getProductReportAmount($category) {

    $select = $this->getProductsReport($category);
    
    $sub_select = $this->select()->from(array('bm' => 'batch_mixes'))
            ->setIntegrityCheck(false)
            ->group('id_mixes')
            ->group('id_macerations')
            ->group('batch');
    
    //mixy a jejich prvočinitele
    $select->joinLeft(array('bm' => $sub_select), 'pb.batch=bm.batch');
    $select->joinLeft(array('m' => 'macerations'), 'bm.id_macerations = m.id');
    $select->joinLeft(array('mr' => 'mixes_recipes'), 'm.id_recipes = mr.id_recipes AND bm.id_mixes = mr.id_mixes');
    $select->joinLeft(array('mi' => 'mixes'), 'bm.id_mixes = mi.id');

    $select->columns(new Zend_Db_Expr('SUM(pb.amount*pr.amount) AS "amount_batch_sum"'));
    $select->columns(new Zend_Db_Expr('GROUP_CONCAT(CONCAT(m.batch, ",", mr.ratio) SEPARATOR ";") AS "mixes_batches_ratio"'));
    $select->columns(new Zend_Db_Expr('SUM(mr.ratio) AS "mixes_total_ratio"'));

    $select->group('bm.batch');

    $allbatches = $select->query()->fetchAll();

    $batch_array = array();

    foreach ($allbatches as $batch) {
      $batch_array[$batch['batch']] = $batch['amount_batch_sum'];

      // rozbití mixu na prvočinitele
      foreach (explode(';', $batch['mixes_batches_ratio']) as $batches_ratio) {
        $batch_ratio = explode(',', $batches_ratio);
        $batch_coef[$batch_ratio[0]] = ((isset($batch_coef[$batch_ratio[0]])) ? ($batch_coef[$batch_ratio[0]]+$batch_ratio[1]) : $batch_ratio[1]);
        
      }
      
      foreach ($batch_coef as $batch_curr=>$coef) {
        $batch_array[$batch_curr] = $batch['amount_batch_sum'] / $batch['mixes_total_ratio'] * $coef;
      }
    }

    return $batch_array;
  }
  
  public function getProductReportAmountNFS() {
      return $this->getProductReportAmount(self::CTG_TOBACCO_NFS);
  }

  
  public function getProductReportAmountFS() {
      return $this->getProductReportAmount(self::CTG_TOBACCO_FS);
  }
  
  public function getCriticalAmount($id) {
	  $row = $this->fetchRow('id_parts = ' . $this->getAdapter()->quote($id));
	  if (!$row) {
	    return null;
    }
	  $ca = $row->toArray();
    return $ca['critical_amount'];
  }

  public function restore($partId) {
    $auth = new Zend_Session_Namespace('Zend_Auth');
    $data = array(
      'id_parts' => $partId,
      'id_users' => $auth->user_id,
      'amount' => 0,
      'critical_amount' => 0,
    );
    try {
      $this->insert($data);
      return TRUE;
    }
    catch (Zend_Db_Statement_Exception $e) {
      Utils::logger()->notice($e->getMessage());
      return ($e->getCode() === 23000); // V pripade tabaku se po vymazu zaznam neodstranil.
    }
    catch (Exception $e) {
      Utils::logger()->warn($e->getMessage());
      return FALSE;
    }
  }
}

