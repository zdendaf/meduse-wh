<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Table_EmployeesAbsence extends Zend_Db_Table_Abstract {
    
    const COND_ALL          = 0;
    const COND_RUNNING      = 1;
    const COND_RUNNING_NEXT = 2;
    const COND_NEXT         = 3;
    const COND_PAST         = 4;
    
    const TYPE_VACATION = 'vacation';
    const TYPE_ILLNESS  = 'illness';
    const TYPE_OTHER    = 'other';
    const TYPE_FREE     = 'free';
    const TYPE_BANNED   = 'banned';
    
    protected $_name = 'employees_absence';
    protected $_primary = 'id';
  
    public static $translate = array(
      self::TYPE_VACATION => 'dovolená',
      self::TYPE_ILLNESS => 'nemoc',
      self::TYPE_OTHER => 'placené volno',
      self::TYPE_FREE => 'neplacené volno',
      self::TYPE_BANNED => 'neomluvená absence',
    );
    
    public function getGrid($options, $condition = Table_EmployeesAbsence::COND_ALL) {
      
      $select = $this->select()
        ->setIntegrityCheck(FALSE)
        ->from(array('v' => $this->_name), array(
          'id', 
          'id_employees', 
          'start', 
          'end',
          'total',
          'approved',
          ))
        ->joinLeft(array('e' => 'employees'), 'e.id = v.id_employees', array(
          'last_name', 
          'first_name',
        ))
        ->joinLeft(array('d' => 'departments'), 'd.id = e.id_departments', array(
          'department' => 'name',
        ))
        ->order(array('v.start', 'v.end'));
      
      switch ($condition) {
        case self::COND_RUNNING:
          $select->where('v.start <= NOW()');
        case self::COND_RUNNING_NEXT:
          $select->where('v.end >= NOW()');
          break;
        case self::COND_NEXT:
          $select->where('v.start > NOW()');
          break;
        case self::COND_PAST:
          $select->where('v.end < NOW()');
          break;
      }
      
      $grid = new ZGrid($options);
      $grid->setSelect($select);
      
      return $grid;
    }
    
    /**
     * Vraci typ nepritomnosti nebo FALSE, pokud nepritomnost nebyla.
     */
    public function testAbsence($employeeId, Meduse_Date $date, $approved = TRUE) {
      $select = $this->select()->from($this, array('type', 'description', 'approved'));
      $select->where('id_employees = ?', $employeeId);
      $select->where('? BETWEEN start AND end', $date->get('Y-m-d'));
      if ($approved) {
        $select->where('approved = ?', 'y');
      }
      return $select->query()->fetch();
    }
  }
  