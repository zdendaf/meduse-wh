<?php
class Table_OrdersFees extends Zend_Db_Table_Abstract {

	protected $_name = 'orders_fees';
	protected $_primary = 'id';

  public function setFee($feeId, $price, $orderId): bool
  {
    try {
      $id = $this->insert(['id_orders' => $orderId, 'id_fees' => $feeId, 'price' => $price]);
      return (bool) $id;
    }
    catch (Exception $e) {
      $updated = $this->update(['price' => $price], [
        'id_orders = ' . $this->getAdapter()->quote($orderId),
        'id_fees = ' . $this->getAdapter()->quote($feeId),
      ]);
      return $updated === 1;
    }
  }

  public function unsetFee($feeId, $orderId): bool
  {
    $deleted = $this->delete( [
      'id_orders = ' . $this->getAdapter()->quote($orderId),
      'id_fees = ' . $this->getAdapter()->quote($feeId),
    ]);
    return $deleted === 1;
  }

  public function getOrderFees($orderId): array
  {
    $select = $this->select()->setIntegrityCheck(false);
    $select->from(['of' => $this->_name], ['id_fees', 'price']);
    $select->join(['f' => 'application_fees'], 'f.id = of.id_fees', ['name', 'text_cs', 'text_en', 'is_coo', 'is_iv']);
    $select->where('of.id_orders = ?', $orderId);
    $orderFees = [];
    if ($fees = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll()) {
      foreach ($fees as $fee) {
        $orderFees[$fee['id_fees']] = $fee;
      }
    }
    return $orderFees;
  }

}
