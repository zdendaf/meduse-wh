<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Table_Works extends Zend_Db_Table_Abstract {
    protected $_name = 'works';
    protected $_primary = 'id';
    
    const STATUS_CHECKIN  = 1;
    const STATUS_BREAKIN  = 2;
    const STATUS_BREAKOUT = 3;
    const STATUS_CHECKOUT = 4;
    
  }

  