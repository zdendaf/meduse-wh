<?php

class Table_CustomersProductsDiscount extends Zend_Db_Table_Abstract {

    protected $_name = 'customers_products_discount';
    protected $_primary = 'id';

    
    /**
     * Add new discount
     * 
     * @param int $customer_id
     * @param string $product_id
     * @param int $discount 
     */
    public function addDiscount($customer_id, $product_id, $discount)
    {       
        $data = array(
            'id_customers' => $customer_id,
            'id_products' => $product_id,
            'discount' => $discount,
            'inserted' => null
        );
        
        $this->insert($data);
    }
    
    
    /**
     * Get product discount list
     * 
     * @param int $customer_id
     * @param bool $asSelect
     */
    public function getProductDiscountList($customer_id, $asSelect = false)
    {
        $select = $this->select();
	$select->setIntegrityCheck(false);
	$select->from(array('cpd' => $this->_name))
               ->joinLeft(array('p' => 'products'), "p.id = cpd.id_products", array('name'))
               ->where("cpd.id_customers = ?", $customer_id)
               ->where("p.deleted = 'n'");
        
        return ($asSelect ? $select : $this->fetchAll($select));
    }
    
    
    
    /**
     * Check if record exist
     * 
     * @param int $customer_id
     * @param string $product_id
     * @return bool
     */
    public function checkExist($customer_id, $product_id)
    { 
        $data = $this->fetchAll('id_customers = ' . $customer_id . ' AND id_products = "' . $product_id . '" ');

        if($data->toArray()) {
            return true;
        }
        
        return false;
    }
    
    
    /**
     * Get product discount
     * 
     * @param int $customer_id
     * @return array
     */
    public function getProductDiscount($record_id)
    {
        $select = $this->select();
	$select->setIntegrityCheck(false);
	$select->from(array('cpd' => $this->_name))
               ->joinLeft(array('p' => 'products'), "p.id = cpd.id_products", array('name'))
               ->where("cpd.id = ?", $record_id)
               ->where("p.deleted = 'n'");
        
        $data = $this->fetchAll($select);
        
        return $data[0]->toArray();
    }
    
    
    /**
     * Edit product discount
     * 
     * @param int $row_id
     * @param int $discount 
     */
    public function editProductDiscount($row_id, $discount)
    {
        $data = array(
            'discount' => $discount
        );
        
        $this->update($data, 'id = ' . $row_id);
    }
}
