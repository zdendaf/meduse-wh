<?php
class Table_ProductsAllParts extends Zend_Db_Table_Abstract{
	
	protected $_name = 'products_all_parts';
	
	protected $_primary = 'id';
	
	public function addProduct($id){
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from('products_parts')
		->where('id_products = ?',$id);
		$rows = $this->fetchAll($select);
		foreach($rows as $row){
			$part = new Parts_Part($row->id_parts);
			$part->insertSubParts($id, $row->amount);
		}
	}
	
}
