<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Table_UsersPermissions extends Zend_Db_Table_Abstract {
    
    protected $_name = 'users_permissions';
    protected $_primary = 'id';
    
  }

  