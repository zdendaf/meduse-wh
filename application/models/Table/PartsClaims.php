<?php
class Table_PartsClaims extends Zend_Db_Table_Abstract{
	
	protected $_name = 'parts_claims';
	
	protected $_primary = 'id';
	
	public function getDatalist(){
		$rows = $this->fetchAll($this->select());
		$data = array();
		foreach($rows as $row){
			$date_from = new Zend_Date($row->date_from, Zend_Date::ISO_8601);
			if($row->date_to){
				$date_to = new Zend_Date($row->date_to, Zend_Date::ISO_8601);
				$date_to = $date_to->toString("d.M.yyyy");
			} else {
				$date_to = '';
			}
			$data[$row->id] = array(
				'id' => $row->id,
				'id_parts' => $row->id_parts,
				'name' => $row->name, 
				'description' => $row->description,
				'amount' => $row->amount,
				'date_from' => $date_from->toString("d.M.yyyy"),
				'date_to' =>  $date_to,
			);
		}
		return $data;
	}
	
	/**
	 * Vytvori reklamaci na soucast
	 * @param $id_parts
	 * @param $amount
	 * @param $name
	 * @param $description
	 * @param $date_from
	 * @param $date_to
	 * @return unknown_type
	 */
	public function add($id_parts, $amount, $name, $description, $date_from, $date_to){
		$this->insert(array(
			'id_parts' => $id_parts,
			'name' => $name,
			'description' => $description,
			'amount' => $amount,
			'date_from' => $date_from,
			'date_to' => $date_to,			
		));
	}
	
	public function remove($id_claim){
		$select = $this->select()->where("id = ?", $id_claim);
		$this->delete($select->getPart("WHERE"));
	}
	
}