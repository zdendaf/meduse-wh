<?php
class Table_Addresses extends Zend_Db_Table_Abstract {

  	protected $_name = 'addresses';
    protected $_primary = 'id';

    const REGION_EU = 'eu';
    const REGION_NON_EU = 'non-eu';
    const COUNTRY_CZ = 'CZ';

    const TYPE_BILLING = 1;
    const TYPE_DELIVERY = 2;

    public function getMeduseAddress() {
      $tCustomers = new Table_Customers();
      $rCustomerMeduse = $tCustomers->fetchRow("id = ".Table_Customers::MEDUSE_ID);
      return $this->fetchRow("id = " . $rCustomerMeduse->id_addresses);
    }

    public function getMediteAddress() {
      $tCustomers = new Table_Customers();
      $rCustomerMeduse = $tCustomers->fetchRow("id = ".Table_Customers::MEDITE_ID);
      return $this->fetchRow("id = " . $rCustomerMeduse->id_addresses);
    }

    public function getAddress($id) {
        if (is_null($id)) {
          return null;
        }
        $select = $this->getAdapter()->select();
        $select->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'c_country'), 'c.code = a.country', array('country_name' => 'name', 'region'))
            ->where('id = ?', $id);
        return $select->query()->fetch();
    }

    public function saveForm(array $data) {
      $row = $this->createRow($data);
      $row->save();
      return $row;
    }
}