<?php
class Table_WrapsParts extends Zend_Db_Table_Abstract{
	
	protected $_name = 'wraps_parts';
	
	protected $_primary = 'id';
	
	public function getWraps($id_product){
		$rows = $this->fetchAll($this->select()->where("id_products = ?", $id_product)->order(array('id_products')));
		$data = array();
		foreach($rows as $row){
			$data[$row->id_wraps][] = $row->id_part;
		}
		return $data;
	}
	
}
