<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Table_EmployeesRewards extends Zend_Db_Table_Abstract {
    protected $_name = 'employees_rewards';
    protected $_primary = array('id_employees', 'year', 'month');


    // dotaz na soucet dovolene za dany rok pro daneho zamestanace
    public function getVacation($id_employees, $year = NULL, $month = NULL, $onlyClosed = TRUE): float
    {
      if (is_null($year)) {
        $year = date('Y');
      }
      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from($this->_name, array('vacation' => new Zend_Db_Expr('SUM(vacation)')))
        ->where('id_employees = ?', $id_employees)
        ->where('year = ?', $year)
        ->group('year');
      if ($month) {
        $select->where('month <= ?', $month);
      }
      if ($onlyClosed) {
        $select->where('status = ?', Employees_Reward::REWARD_STATUS_CLOSED);
      }
      $vacation = (float) $select->query()->fetchColumn();
      return $year == '2016' ? $vacation + $this->getInitalVacation($id_employees) : $vacation;
    }

    /**
     * Pocatecni stav dovolenych pri spusteni dochazkoveho modulu dle udaju od ucetni
     */
    private function getInitalVacation($id_employees) {
      $inital = array(
        6  =>  4.5,
        1  =>  9.0,
        4  => 13.0,
        8  =>  4.0,
        3  => 11.0,
        12 => 10.0,
        7  =>  5.0,
        5  =>  7.0,
      );
      return isset($inital[$id_employees]) ? $inital[$id_employees] : 0;
    }

    public function getRewards(int $idWorker, $year = NULL, $month = NULL, $limit = NULL): array
    {
      $select = $this->select()->from($this->_name, array('id_employees', 'year', 'month'));
      $select->where('id_employees = ?', $idWorker);
      if (!is_null($year)) {
        $select->where('year = ?', $year);
      }
      if (!is_null($month)) {
        $select->where('month = ?', $month);
      }
      $select->order(array('year DESC', 'month DESC'));
      if (!is_null($limit)) {
        $select->limit($limit);
      }
      return $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
    }

    /**
     * @throws Zend_Db_Statement_Exception
     */
    public function getLastClosedReward(int $idWorker) {
      $select = $this->select()->from($this->_name, array('id_employees', 'year', 'month'));
      $select->where('id_employees = ?', $idWorker);
      $select->where('status = ?', 1);
      $select->order(array('year DESC', 'month DESC'));
      return $select->query(Zend_Db::FETCH_OBJ)->fetch();
    }
  }

