<?php

class Table_OrdersServices extends Zend_Db_Table_Abstract {

  protected $_name = 'orders_services';
  protected $_primary = 'id';

  public function getTotalSales(array $options) {

    $wh = isset($options['wh']) ? $options['wh'] : Table_PartsWarehouse::WH_TYPE;

    // Zakladni dotaz bez filtrace.
    $select = $this->select()->setIntegrityCheck(false);
    $select
      ->from(['os' => 'orders_services'], [
        'description',
        'qty' => new Zend_Db_Expr('os.qty'),
        'avg' => new Zend_Db_Expr('ROUND(IF (i.currency = \'' . Table_Invoices::CURRENCY_EUR . '\', os.price *  i.rate * os.qty, os.price * os.qty) / os.qty, 2)'),
        'total' => new Zend_Db_Expr('ROUND(IF (i.currency = \'' . Table_Invoices::CURRENCY_EUR . '\', os.price *  i.rate * os.qty, os.price * os.qty), 2)'),
        'total_czk' => new Zend_Db_Expr('ROUND(IF (i.currency = \'' . Table_Invoices::CURRENCY_CZK . '\', os.price * os.qty, 0), 2)'),
        'total_eur' => new Zend_Db_Expr('ROUND(IF (i.currency = \'' . Table_Invoices::CURRENCY_EUR . '\', os.price * os.qty, 0), 2)'),
      ])
      ->join(['o' => 'orders'], 'o.id = os.id_orders and o.id_wh_type = ' . $wh .
        ' and o.status = \'' . Table_Orders::STATUS_CLOSED . '\'', [])
      ->join(['i' => 'invoice'], 'i.id_orders = o.id ' .
        'and i.type = \'' . Table_Invoices::TYPE_REGULAR . '\'', ['invoice_id' => 'id', 'invoice_no' => 'no'])
      ->join(['c' => 'customers'], 'c.id = o.id_customers', ['customer_type' => 'type'])
      ->join(['a' => 'addresses'], 'a.id = o.id_addresses', [])
      ->join(['cc' => 'c_country'], 'cc.code = a.country', [])
      ->order('o.id');

    // Filtrace - datum od.
    if (isset($options['from']) && !empty($options['from'])) {
      $date = new Zend_Date($options['from'], 'dd.MM.yyyy');
      if ($wh == Table_PartsWarehouse::WH_TYPE) {
        $select->where('o.date_request >= ?', $date->toString('yyyy-MM-dd'));
      }
      else {
        $select->where('o.date_exp >= ?', $date->toString('yyyy-MM-dd'));
      }
    }

    // Filtrace - datum do.
    if (isset($options['to']) && !empty($options['to'])) {
      $date = new Zend_Date($options['to'], 'dd.MM.yyyy');
      if ($wh == Table_PartsWarehouse::WH_TYPE) {
        $select->where('o.date_request <= ?', $date->toString('yyyy-MM-dd'));
      }
      else {
        $select->where('o.date_exp <= ?', $date->toString('yyyy-MM-dd'));
      }

    }

    // Filtrace - zeme.
    if (isset($options['country']) && !empty($options['country'])) {
      switch ($options['country']) {
        case 'EU':
          $select->where('cc.region = ?', 'eu');
          break;
        case 'EU_NOT_CZ':
          $select->where('cc.region = ?', 'eu');
          $select->where('a.country != ?', 'CZ');
          break;
        case 'NOT_CZ':
          $select->where('a.country != ?', 'CZ');
          break;
        default:
          $select->where('a.country = ?', $options['country']);
          break;
      }
    }

    // Filtrace - zakaznik.
    if (isset($options['customer']) && !empty($options['customer'])) {
      $select->where('o.id_customers = ?', $options['customer']);
    }

    // Filtrace - typ zakaznika.
    if (strtoupper($options['type']) !== 'ALL') {
      $select->where('c.type = ? ', $options['type']);
    }

    // Filtrace - jen uhrazene
    if ((isset($options['onlypaid']) && $options['onlypaid'] == 'y') || $wh == Table_PartsWarehouse::WH_TYPE) {
      $select->where('o.date_payment_accept IS NOT NULL OR o.date_pay_all IS NOT NULL');
    }

    if (isset($options['order'])) {
      switch ($options['order']) {
        case 'pcs':
          $select->order('qty desc');
          break;
        case 'price':
            $select->order('total desc');
          break;
        case 'name':
        default:
          $select->order('description');
          break;
      }
    }
    else {
      $select->order('description');
    }

    return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }

  /**
   * Vrátí data o objednaných službách v objednávkách předběžných, potvrzených a otevřených.
   * @return array
   */
  public function getTotalRequested(): array {
    $select = $this->select()->setIntegrityCheck(FALSE)
      ->from(['os' => 'orders_services'], ['qty' => new Zend_Db_Expr('SUM(os.qty)'), 'desc' => 'description'])
      ->join(['o' => 'orders'], 'o.id = os.id_orders AND o.id_wh_type = 1', ['order_status' => 'status'])
      ->where('o.status IN (?)', [Table_Orders::STATUS_NEW, Table_Orders::STATUS_CONFIRMED, Table_Orders::STATUS_OPEN])
      ->group(['os.description', 'o.status'])
      ->order('os.description');
    return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }

}
