<?php
class Table_Producers extends Zend_Db_Table_Abstract{

	const CTG_CARRIERS = 21;
  const MEDUSE_ID = 1;
  const MEDUSE_ELEKTRO_ID = 265;

	protected $_name = 'producers';

	protected $_primary = 'id';

	protected $_forbidden = array('"', "--", ";", "\\", "'", "<", ">");

	/**
	 * Prida radek do tabulky + udela kontrolu poli, ktere maji byt vyplnene
	 * @param array $values
	 */
	public function add(array $values){
		$data = array();

		if(!isset($values['name'])) throw new Exception("Nejsou vyplneny vsechny povinne sloupce.");

		//povinne hodnoty
		$data['name'] = $values['name'];

		//volitelne hodnoty
		foreach($values as $name => $value){
			if(!empty($value)){
				$data[$name] = trim(str_replace($this->_forbidden, "", $value));
			}
		}

		return $this->insert($data);
	}

	public function edit(array $values, $id){
		$data = array();

		if(!isset($values['name'])) throw new Exception("Nejsou vyplneny vsechny povinne sloupce.");

		//povinne hodnoty
		$data['name'] = $values['name'];

		//volitelne hodnoty
		foreach($values as $name => $value){
			if(!empty($value)){
				$data[$name] = trim(str_replace($this->_forbidden, "", $value));
			}else {
        $data[$name] = null;
      }
		}
		if (!isset($data["is_active"])) {
			$data["is_active"] = 'n';
		}

		if ($data["id_extern_warehouses"] == 'NULL') unset($data["id_extern_warehouses"]);

		$this->update($data, "id = ".$this->getAdapter()->quote($id));

		if ($this->isCarrier($id)) {
			$tableOC = new Table_OrdersCarriers();
			$tableOC->setName($values['name'], $this->getCarrierID($id), false);
		}
	}

	public function getDataList($asString = false, $wh = 1){
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(
			array("p" => "producers")
		)->joinLeft(
			"producers_ctg",
			"id_producers_ctg = producers_ctg.id",
			array(
				"producer_ctg" => 'name',
				"hasParts" => "EXISTS(SELECT * FROM producers_parts WHERE id_producers = p.id)",
				"hasOperations" => "EXISTS(SELECT * FROM parts_operations WHERE id_producers = p.id)"
			)
		)->where("p.id_orders_carriers IS NULL")
      ->where('id_wh_type = ?', $wh);
		$select->order(array("id_producers_ctg","EXISTS(SELECT * FROM producers_parts WHERE id_producers = p.id) DESC","EXISTS(SELECT * FROM parts_operations WHERE id_producers = p.id) DESC","name"));

		if($asString){
			return $select;
		} else {
			return $this->fetchAll($select);
		}
	}

	public function getPartId($producerId, $partId) {
		$row = $this->getAdapter()->fetchRow('SELECT producers_part_id FROM producers_parts WHERE id_producers = :p AND id_parts = :ip',array('p'=>$producerId,'ip'=>$partId));
		if($row){
			return $row['producers_part_id'];
		} else {
			return '';
		}
	}

	public function getPartName($producerId, $partId) {
		$row = $this->getAdapter()->fetchRow('SELECT producers_part_name FROM producers_parts WHERE id_producers = :p AND id_parts = :ip',array('p'=>$producerId,'ip'=>$partId));
		if($row){
			return $row['producers_part_name'];
		} else {
			return '';
		}
	}

	public function isCarrier($id) {
    $tOrdersCarriers = new Table_OrdersCarriers();
		$row = $tOrdersCarriers->fetchRow('id_producers = ' . $this->getAdapter()->quote($id));
		return ($row != FALSE);
	}

	public function getCarrierID($id) {
		$row = $this->fetchRow('id = ' . $this->getAdapter()->quote($id))->toArray();
		return $row['id_orders_carriers'];
	}

  /**
   * Vrati seznam dopravcu
   *
   * @param $wh typ skladu (default wh dymek)
   */
  public function getCarriers($status = 'all', $wh = Parts_Part::WH_TYPE_PIPES): array
  {
    $select = $this->select()->setIntegrityCheck(FALSE)
      ->from(array('p' => $this->_name), array('id', 'name'))
      ->join(array('c' => 'orders_carriers'), 'p.id = c.id_producers', array())
      ->where('id_wh_type = ?', $wh)
      ->group('p.id')->order('p.name');

    if ($status === 'active') {
      $select->where('p.is_active = ?', 'y');
    }
    elseif ($status === 'inactive') {
      $select->where('p.is_active = ?', 'n');
    }
    return $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
  }


  public function getProducers($wh = Parts_Part::WH_TYPE_PIPES, $onlyActive = TRUE, $conditions = array()) {
    $select = $this->select()->setIntegrityCheck(FALSE)->distinct(TRUE);
    $select->from(array('p' => 'producers'));
    $select->joinLeft(array('oc' => 'orders_carriers'), 'oc.id_producers = p.id', array());
    if ($onlyActive) {
      $select->where('p.is_active = ?', 'y');
    }
    $select->where('p.id_wh_type = ?', $wh);
    $select->where('oc.id IS NULL');
    $select->order('p.name ASC');
    if ($conditions) {
      foreach ($conditions as $cond => $value) {
        $select->where($cond, $value);
      }
    }
    return $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
  }

}
