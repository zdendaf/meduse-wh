<?php
class Table_OrdersCarriers extends Zend_Db_Table_Abstract{

	protected $_name = 'orders_carriers';
	protected $_primary = 'id';

	const EX_WORKS = 4;
  const EX_WORKS_TOBACCO = 42;
  const UNDEFINED = 5;
  const DEF_METHOD_NAME = 'výchozí';

	public function setName($name, $id, $updateProducer = true) {
		$this->update(array(
				'name' => $name
			), 'id = ' . $this->getAdapter()->quote($id));
		if ($updateProducer) {
			$tableProducers = new Table_Producers();
			$tableProducers->update(array(
					'name' => $this->getAdapter()->quote($name)
				), 'id_orders_carriers = ' . $this->getAdapter()->quote($id));
		}
	}

  /**
   * Vrati asociativni pole pro formularovy prvek select s volbou dodavatelu
   * @return array
   */
  public function getOptionsPairs($wh = Parts_Part::WH_TYPE_PIPES) {
    $select = $this->select()->setIntegrityCheck(FALSE)
      ->from(array('oc' => $this->_name), array(
        'id',
        'name' => new Zend_Db_Expr('IF(p.name <> oc.name, CONCAT(p.name, " - ", oc.name), oc.name)')
        ))
      ->joinLeft(array('p' =>'producers'), 'p.id = oc.id_producers AND p.is_active = "y"', array())
      ->where('oc.id <> ?', self::UNDEFINED)
      ->where('oc.id_producers IS NOT NULL')
      ->where('oc.is_active = ?', 'y')
      ->where('p.id_wh_type = ?', $wh)
      ->order('name ASC');
    return $this->getAdapter()->fetchPairs($select);
  }
}
