<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Table_Employees extends Zend_Db_Table_Abstract {

    const TYPE_EMPLOYEE = 'employee';
    const TYPE_SUPPLIER = 'supplier';

    const ROUNDING_NONE = 0;
    const ROUNDING_DEFAULT = 1;
    const ROUNDING_30_15 = 2;
    const ROUNDING_05 = 3;

    public static $roundingNames = array(
      self::ROUNDING_NONE => 'bez zaokrouhlování',
      self::ROUNDING_DEFAULT => '15 minut s tolerancí 2 min.',
      self::ROUNDING_30_15 => '30/15 minut s tolerancí 2 min.',
      self::ROUNDING_05 => '5 minut bez tolerance',
    );

    protected $_name = 'employees';
    protected $_primary = 'id';

    public function getAdminGrid($options, array $params = array(), $wh_type = NULL, $onlyActive = TRUE) {
      $grid = new ZGrid($options);
      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from(array('e' => $this->_name), array(
          'id', 'first_name', 'last_name',
          'typ' => new Zend_Db_Expr('IF(e.type LIKE \'employee\', \'zam.\', \'živ.\')')
        ))
        ->joinLeft(array('d' => 'departments'), 'e.id_departments = d.id', array(
          'department' => 'name'));
      if (!is_null($wh_type)) {
        $select->where('d.id_wh_type = ?', $wh_type);
      }
      if (isset($params['sort']) && $params['sort'] == 'first_name') {
        $select->order('e.first_name');
      }
      else {
        $select->order('e.last_name');
      }
      if ($onlyActive) {
        $select->where('e.active = ?', 'y');
      }
      $grid->setSelect($select);
      return $grid;
    }

    /**
     * Vrátí asociativní pole pracovníků ve tvaru id => celé jméno pro použití ve formuláři.
     *
     * @param type $departmentId
     *    nepovinný parametr pro filtraci dle oddělení
     *
     * @return array
     */
    public function getOptionsPairs($departmentId = NULL) {
      $pairs = array();
      $select = $this->select($this->_name, array('id', 'first_name', 'last_name'));
      $select->order(array('last_name', 'first_name'));
      if ($departmentId) {
        $select->where('id_departments = ', $departmentId);
      }
      $result = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
      if ($result) {
        foreach ($result as $row) {
          $pairs[$row->id] = $row->last_name . ' ' . $row->first_name;
        }
      }
      return $pairs;
    }

    public function findByAlvenoId($alvenoId) {
      return $this->select()->setIntegrityCheck(FALSE)
        ->where('alveno = ?', $alvenoId)
        ->query(Zend_Db::FETCH_OBJ)->fetch();
    }

    public function getDeptsEmployees(array $deptsIds) {
      return $this->select()->setIntegrityCheck(FALSE)
        ->from(array('e' => 'employees'))
        ->joinLeft(array('d' => 'departments'), 'd.id = e.id_departments', array('department' => 'name'))
        ->where('id_departments IN (?)', $deptsIds)
        ->where('e.active = ?', 'y')
        ->order(array('last_name ASC', 'first_name ASC'))
        ->query(Zend_Db::FETCH_OBJ)->fetchAll();
    }

    public function getEmployees($active_only = FALSE) {
      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from(array('e' => $this->_name))
        ->where('e.type = ?', self::TYPE_EMPLOYEE)
        ->order('e.last_name');
      if ($active_only) {
        $select->where('e.active = ?', 'y');
      }
      return $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
    }
  }

