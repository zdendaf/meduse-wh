<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 29.6.2018
 * Time: 15:52
 */

class Table_CustomersBudgets extends Zend_Db_Table_Abstract {

  protected $_name = 'customers_budgets';
  protected $_primary = 'id';

  public function getCustomerBudgets($id_customers) {
    $select = $this->select()->from($this->_name, array('id'))->where('id_customers = ?', $id_customers);
    return $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
  }

}