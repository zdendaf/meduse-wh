<?php

class Table_CustomersPricelists extends Zend_Db_Table_Abstract {
    
    protected $_name = 'customers_pricelists';
    protected $_primary = 'id';
    
    
    public function assignCustomer($idCustomer, array $idPricelists) {
        $this->delete('id_customers = ' . $idCustomer);
        if ($idPricelists) {
            foreach ($idPricelists as $id) {
              if ($id) {
                $this->insert(['id_customers' => $idCustomer, 'id_pricelists' => $id]);
              }
            }
        }
    }
    
    public function assignCustomersFrom($idFromPricelist, array $idPricelists) {
        $this->delete('id_pricelists IN (' . implode(', ', $idPricelists) . ')');
        if ($idPricelists) {
            foreach ($idPricelists as $id) {
                $select = $this->select()->setIntegrityCheck(false);
                $select->from(array($this->_name), array(
                    'id_customers', 
                    'id_pricelist' => new Zend_Db_Expr($id)));
                $select->where('id_pricelists = ?', $idFromPricelist);
                $data = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
                $this->insert($data);
            }
        }
    }
    
    public function assignAllCustomers($idPricelist, $customerType = null) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from('customers', array(
            'id_customers' => 'id', 
            'id_pricelists' => new Zend_Db_Expr($idPricelist)));
        if (!is_null($customerType)) {
            $select->where('type = ?', $customerType);    
        }
        $data = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
        foreach($data as $row) {
            $this->insert($row); 
        }
    }
}
