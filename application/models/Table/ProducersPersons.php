<?php
class Table_ProducersPersons extends Zend_Db_Table_Abstract{

	protected $_name = 'producers_persons';

	protected $_primary = 'id';

	protected $_forbidden = array('"', "--", ";", "\\", "'", "<", ">");

	/**
	 * Prida radek do tabulky + udela kontrolu poli, ktere maji byt vyplnene
	 * @param array $values
	 */
	public function add(array $values){
		$data = array();

		$empty = true;
		foreach($values as $name => $value){
			$empty = $empty && empty($value);
		}
		if($empty) return false;

		foreach($values as $name => $value){
			if(!empty($value)){
				$data[$name] = trim(str_replace($this->_forbidden, "", $value));
			}
		}

		return $this->insert($data);
	}

	public function edit(array $values, $id){
		$data = array();

		//volitelne hodnoty
		foreach($values as $name => $value){
			//if(!empty($value)){
				$data[$name] = trim(str_replace($this->_forbidden, "", $value));
			//}
		}

		$this->update($data, "id = ".$this->getAdapter()->quote($id));
	}

}
