<?php

// Ke smazani.

class Table_ProductsParts extends Zend_Db_Table_Abstract{

	//ZRUSIT, PRESUNOUT JINAM
	protected $_name = 'parts_subparts';
	
	protected $_primary = 'id';
	
	/**
	 * Vraci pocet soucasti produktu daych polem idecek
	 * @param $id_array
	 * @return unknown_type
	 */
	public function getParts($idPart){
		$select = $this->select();
		$select->setIntegrityCheck(false);
//		$select->from(array("pp" => "products_parts"), array('amount' => 'SUM(pp.amount)', 'id_parts'))
//		->joinLeft(array("p" => "parts"), "p.id = pp.id_parts", array("id_parts_ctg", "name"))
//		->where("pp.id_products IN (?)",$id_array) //soucasti prirazene primo produktu
//		//->orWhere("pp.id_products IN (SELECT id_parts_set FROM products_parts_set WHERE id_products IN (?))",$id_array) //soucasti setu, ktery je prirazen produktu
//		->order(array("id_parts_ctg", "pp.id_parts"))
//		->group('pp.id_parts'); //slouceni soucasti produktu a soucasti setu produktu

		$select->from(
			array('p' => 'parts'),
			array()
		)->joinLeft(
			array('ps' => 'parts_subparts'),
			"p.id = ps.id_multipart",
			array(
				"id_parts",
				"amount" => new Zend_Db_Expr("SUM(ps.amount)"),
				"name"   => new Zend_Db_Expr("(SELECT name FROM parts WHERE id = ps.id_parts)"),
				"id_parts_ctg"    => new Zend_Db_Expr("(SELECT id_parts_ctg FROM parts WHERE id = ps.id_parts)"),
			)
		)->where(
			"p.id = ?", $idPart
		)->group(
			"ps.id_parts"
		);
		
		$parts = array();
		$rows = $this->fetchAll($select);
		if(count($rows) > 1){
			foreach($rows as $row){
				$parts[] = array(
					'id' => $row['id_parts'],
					'name' => $row['name'],
					'amount' => $row['amount'],
					'ctg' => $row['id_parts_ctg']
				);
			}
			return $parts;
		} else {
			$row = $this->getAdapter()->fetchRow("SELECT * FROM parts WHERE id = :id", array('id' => $idPart));
			return array( array(
				'id' => $row['id'],
				'name' => $row['name'],
				'amount' => 1,
				'ctg' => $row['id_parts_ctg']
			));
		}
	}
	
}
