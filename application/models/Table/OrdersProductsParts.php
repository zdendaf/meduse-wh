<?php
/**
 * Rezervace soucasti
 */
class Table_OrdersProductsParts extends Zend_Db_Table_Abstract{
	
	protected $_name = 'orders_products_parts';
	
	protected $_primary = 'id';
	
	/**
	 * Vraci pocet hotovych soucasti pro danou polozku objednavky (produkt)
	 * @param $id_order
	 * @param $id_part
	 * @return unknown_type
	 */
	public function getAmount($id_order, $id_product, $id_part){

		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array("op" => "orders_products"))
		->joinLeft(array("opp" => "orders_products_parts"), "op.id = opp.id_orders_products", array("ready_amount" => "amount"))
		->where("id_orders = ?", $id_order)
		->where("id_products LIKE ?", $id_product);


		if($this->getAdapter()->fetchAll($select)){
			$row = $this->fetchRow($select);
			return (int) $row['ready_amount'];
		} else {
			return 0;
		}
	}
	
	/**
	 * Vraci celkovy pocet nachystanych soucasti pro vsechny polozky vsech objednavek
	 * @param $id_part
	 * @return unknown_type
	 */
	public function getTotalAmount($id_part){
		$select = $this->select();
		$select->from(array("opp" => "orders_products_parts"), array("part_amount" => "SUM(amount)"))
		->where("id_parts LIKE ?", $id_part)
		->group("opp.id_parts");
		
		if(($row = $this->fetchRow($select)) !== NULL)
			return (int) $row['part_amount'];
		else
			return 0;
		
	}
	
	public function getAllAmounts(){
		$parts = new Table_Parts();
		$parts_array = $parts->getPartsArray();
		foreach($parts_array as $id => $value){
			$parts_array[$id] = $this->getTotalAmount($id);
		}
		return $parts_array;
	}

	/**
	 * Vraci pocet rezervaci k vybranym objednavkam
	 * @param array $orders
	 * @return <type>
	 */
	public function getOrdersReservations(array $orders){
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(
			array("opp" => "orders_products_parts"),
			array("id_parts","part_amount" => "SUM(opp.amount)")
		)->join(
			array("op" => "orders_products"),
			"op.id = opp.id_orders_products",
			array()
		)->where(
			"op.id_orders IN (?)", $orders
		)->group(
			"opp.id_parts"
		);

		$rows = $this->fetchAll($select);
		$data = array();
		foreach($rows as $row){
			$data[$row['id_parts']] = $row['part_amount'];
		}
		return $data;
	}

  /**
   * Vrati asociativni pole s mnozstvim rezervovane soucasti (hodnota) v objednavce (klic).
   *
   * @return array
   */
	public function getReservationsAmount($partId) {
	  $amount = array();
	  $select = $this->select()->setIntegrityCheck(FALSE);
	  $select->from(array('opp' => 'orders_products_parts'), array());
	  $select->joinLeft(array('op' => 'orders_products'), 'opp.id_orders_products = op.id', array(
	    'id_orders', 'amount' => new Zend_Db_Expr('SUM(op.amount)'),
    ));
	  $select->where('opp.id_parts LIKE ?', $partId);
    $select->group('op.id_orders');
    if ($results = $select->query(Zend_Db::FETCH_OBJ)->fetchAll()) {
      foreach ($results as $result) {
        $amount[$result->id_orders] = $result->amount;
      }
    }
    return $amount;
  }
}
