<?php

class Table_PaymentsDeductions extends Zend_Db_Table_Abstract {
  protected $_name = 'payments_deductions';
  protected $_primary = 'id';


  /**
   * Returns sum of already placed deductions from given payment.
   *
   * @param int $paymentId
   * @return float
   */
  public function getPlacedAmount(int $paymentId): float {

    $sum = $this->select()
      ->from($this->_name, [new Zend_Db_Expr('SUM(amount)')])
      ->where('id_payments = ?', $paymentId)
      ->query()->fetchColumn();

    return (float) $sum;
  }

  /**
   * Returns sum of deductions for given invoice.
   *
   * @param int $invoiceId
   * @return float
   */
  public function getDeductionAmout(int $invoiceId): float {

    $sum = $this->select()
      ->from($this->_name, [new Zend_Db_Expr('SUM(amount)')])
      ->where('deduct_from = ?', $invoiceId)
      ->query()->fetchColumn();

    return (float) $sum;
  }

}
