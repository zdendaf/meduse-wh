<?php
  class Table_Budgets extends Zend_Db_Table_Abstract {
  	
    protected $_name = 'budgets';
    protected $_primary = 'id';
    
    public function getGrid($options, $departmentId = NULL, $fromDate = NULL, $toDate = NULL) {
      $grid = new ZGrid($options);
      $select = $this->select()
        ->setIntegrityCheck(FALSE)
        ->from(array('b' => $this->_name), array('id', 'from', 'to', 'amount'));
      if ($departmentId) {
        $select->where('id_departments = ?', $departmentId);
      }
      if ($fromDate) {
        $select->where('from >= ?', $fromDate);
      }
      if ($toDate) {
        $select->where('to <= ?', $toDate);
      }
      $grid->setSelect($select);      
      return $grid;
    }
  }  