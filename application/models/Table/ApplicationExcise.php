<?php

class Table_ApplicationExcise extends Zend_Db_Table_Abstract {

  protected $_name = 'application_excise';
  protected $_primary = 'id';

  public function getExcise($date) {
    if (is_null($date)) {
      $date = date('Y-m-d');
    }
    $select = $this->select()
      ->from($this->_name, array('value'))
      ->where('active_from_date <= ?', $date)
      ->order(array('active_from_date DESC'))
      ->limit(1);
    $rate = $select->query(Zend_Db::FETCH_ASSOC)->fetchColumn();
    if (!$rate) {
      throw new Exception('Nenalezena sazba spotrebni dane.');
    }
    else {
      return (int) $rate;
    }
  }
}


