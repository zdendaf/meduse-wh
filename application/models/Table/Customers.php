<?php
class Table_Customers extends Zend_Db_Table_Abstract{

	const MEDUSE_ID = 80;
  const MEDITE_ID = 416;

  const TYPE_B2B = 'B2B';
  const TYPE_B2C = 'B2C';
  const TYPE_ALL = 'ALL';

  const VAT_INVOICED_AUTO = 'a';
  const VAT_INVOICED_ALWAYS = 'y';
  const VAT_INVOICED_NEVER = 'n';

	protected $_name = 'customers';
	protected $_primary = 'id';

	public function getDatalist($asSelect = true, $idWHType = 1, $params = array()) {

    $subSelect = $this->select()->setIntegrityCheck(false);
    $subSelect->from(array('cp' => 'customers_pricelists'), array());
    $subSelect->join(array('p' => 'pricelists'), 'cp.id_pricelists = p.id AND p.id_wh_type = ' . $idWHType,
            array('GROUP_CONCAT(abbr SEPARATOR ", ")'));
    $subSelect->where('cp.id_customers = c.id');
    $subSelect->where('p.deleted = "n"');
    $subSelect->group(array('cp.id_customers'));
    $subSelect->order(array('p.valid', 'p.id'));

    $select = $this->select()->setIntegrityCheck(false);
    $select->from(array("c" => "customers"), array('*',
        'pricelists' => new Zend_Db_Expr('(' . $subSelect . ')')));
    $select->joinLeft(array('a' => 'addresses'), 'c.id_addresses = a.id', array('country', 'billing_company' => 'company', 'person'));
    $select->where('c.deleted = 0')->where('id_wh_type = ?', $idWHType);


    if (isset($params['q']) && !empty($params['q'])) {
      $q = $params['q'];
      $select->where("c.company LIKE '%$q%' OR c.first_name LIKE '%$q%' OR c.last_name LIKE '%$q%' OR c.phone LIKE '%$q%'");
    }

    if($asSelect){
			return $select;
		}

    return $this->fetchAll($select);
  }

  public function enrichDataList_InvoiceLastIssueDate(Zend_Db_Select &$select) {
    $lastSelect = $this->getAdapter()->select();
    $lastSelect->from(['i' => 'invoice'], ['invoice_last_issue_date' => new Zend_Db_Expr('max(i.issue_date)')]);
    $lastSelect->join(['o' => 'orders'], 'i.id_orders = o.id and i.type = \'regular\'', 'id_customers');
    $lastSelect->group('o.id_customers');
    $select->joinLeft(['ls' => $lastSelect], 'ls.id_customers = c.id', ['invoice_last_issue_date']);
  }

	public function saveForm($data) {

		if (isset($data['id'])) {
      // existujici zaznam uzivatele
			$rCustomer = $this->find($data['id'])->current();
		} else {
      // vytvoreni noveho
			$rCustomer = $this->createRow();

      // ke kteremu skladu zakaznik patri - default k wh dymek
      $rCustomer->id_wh_type = isset($data['id_wh_type']) ? (int) $data['id_wh_type'] : Pipes_Parts_Part::WH_TYPE;

      // vygenerovani unikatniho cisla
      $rCustomer->customer_no = $this->generateCustomerNumber();
      $rCustomer->inserted = new Zend_Db_Expr('NOW()');

      // vytvoreni prazdne fakturacni/dodaci adresy
			$tAddress = new Table_Addresses();
			$rAddress = $tAddress->createRow(array(
			  'company' => trim($data['customer_company']),
        'person' => trim(implode(' ', array($data['customer_first_name'], $data['customer_last_name']))),
        'phone' => trim($data['customer_phone']),
        'street' => trim($data['address_street']),
        'pop_number' => trim($data['address_pop_number']),
        'orient_number' => trim($data['address_orient_number']),
        'city' => trim($data['address_city']),
        'zip' => trim($data['address_zip']),
        'country' => $data['address_country'],
      ));
			$idAddr = $rAddress->save();
			$rCustomer->id_addresses = $idAddr;
      $rCustomer->id_addresses2 = $idAddr;

      if (!$data['customer_type']) {
        $data['customer_type'] = 'B2B';
      }
      $rCustomer->type = $data['customer_type'];
		}

    // zaznamy spolecene pro oba wh

		$rCustomer->company = $data['customer_company'];
		$rCustomer->first_name = $data['customer_first_name'];
		$rCustomer->last_name = $data['customer_last_name'];
		$rCustomer->phone = $data['customer_phone'];
    $rCustomer->email = $data['customer_email'];
		$rCustomer->type = $data['customer_type'];
    $rCustomer->ident_no = empty($data['customer_ident_no'])? null : $data['customer_ident_no'];
		$rCustomer->vat_no = $data['customer_vat_no'];
		$rCustomer->vat_checked = $data['customer_vat_checked'] ? $data['customer_vat_checked'] : NULL;
    $rCustomer->vat_invoice = $data['customer_vat_invoice'] ? $data['customer_vat_invoice'] : self::VAT_INVOICED_AUTO;
    // seed id
    $rCustomer->seed_id = empty($data['customer_seed_id']) ? NULL : $data['customer_seed_id'];


    // zaznamy jen pro wh dymek
    if ($rCustomer->id_wh_type == Pipes_Parts_Part::WH_TYPE) {
      // slevy
      $rCustomer->discount = $data['customer_discount'] ?? 0;
      $rCustomer->discount_type = $data['customer_discount_type'] ?? NULL;
      $rCustomer->coo_requirement = $data['coo_requirement'];
      $rCustomer->invoice_verify_requirement = $data['invoice_verify_requirement'];
      $rCustomer->seller = $data['seller'] === 'y' ? 1 : 0;
      $rCustomer->id_pricelists_ctg = $data['customer_type'] === Table_Customers::TYPE_B2B ? $data['id_pricelists_ctg'] : NULL;
    }
    else {
      // dovoleni fakturace
      $rCustomer->allow_invoicing = $data['allow_invoicing'];
      $rCustomer->due = $data['due'] ? (int) $data['due'] : NULL;
    }

    // ulozeni zaznamu do databaze
    $rCustomer->save();

    // pouze pro zakazniky wh tabaku
    if ($rCustomer->id_wh_type == Table_TobaccoWarehouse::WH_TYPE) {

      // vychozi ceniky
      $pricelists = array();
      if ($data['customer_type'] == 'B2B' && isset($data['pricelist_B2B'])) {
        $pricelists[] =  (int) $data['pricelist_B2B'];
      }
      elseif (isset($data['pricelist_B2C'])) {
        $pricelists[] = (int) $data['pricelist_B2C'];
      }
      $tCustPricelists = new Table_CustomersPricelists();
      $tCustPricelists->assignCustomer($rCustomer->id, $pricelists);

    }

		return $rCustomer;
	}

     /**
     * Generuje a vrací náhodné 5ti místné zákaznické číslo.
     */

    protected function generateCustomerNumber($tries = 50) {
      $rndNo = null;
      //$select->from('customers', array('id'));
      for ($it = 0; $it < $tries; $it++) {
        $rndNo = rand(10000, 99999);
        $select = $this->select();
        $select->where('customer_no = ?', $rndNo);
        $list = $this->fetchRow($select);
        if (!$list) {
          return $rndNo;
        }
      }
      throw new Zend_Exception('Nepodařilo se vygenerovat
                náhodné zákaznické číslo');
    }

    public function getPossibleNames($term = null, $wh_type = null) {

      $unionSelect2 = $this->select()
        ->setIntegrityCheck(false)
        ->from('customers', array(
          'id' => 'id',
          'id_wh_type' => 'id_wh_type',
          'name' => 'company'))
        ->where('company NOT LIKE ""')
        ->order('name ASC');
        if ($wh_type) {
          $unionSelect2->where("id_wh_type = ?", $wh_type);
        }
      $unionSelect1 = $this->select()
        ->setIntegrityCheck(false)
        ->from('customers', array(
          'id' => 'id',
          'id_wh_type' => 'id_wh_type',
          'name' => new Zend_Db_Expr("CONCAT(IFNULL(first_name, ''), ' ', IFNULL(last_name, ''))")));
        if ($wh_type) {
          $unionSelect1->where("id_wh_type = ?", $wh_type);
        }
      $subSelect = $this->getAdapter()->select()
        ->union(array($unionSelect1, $unionSelect2));

      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('t' => $subSelect), array('id', 'name'));

      if ($term) {
        $select->where("name LIKE ?", "%" . trim($term) . "%");
      }

      $tmpRes = $select->query()->fetchAll();
      $results = array();
      foreach ($tmpRes as $key => $value) {
        if(!empty($value)){
          $results[] = array('id'    => $value['id'], 'value' => trim($value['name']));
        }
      }
      return $results;
    }

    public function getPossibleCompanyNames($term = null, $wh_type = null) {
      $select = $this->select()->from($this->_name, array('company'))->group('company');

      if (!is_null($term)) {
        $select->where('company LIKE \'' . trim($term) . '%\'');
      }

      if (!is_null($wh_type)) {
        $select->where('id_wh_type=\'' . $wh_type . '\'');
      }

      return $select;
    }

    public function getPossibleCompanyNamesInvoice($term = null, $wh_type = null) {
      $select = $this->getPossibleCompanyNames($term, $wh_type);

      $str = array();
      foreach ($select->query()->fetchAll() as $s) {
        $str[] = $s['company'];
      }

      return $str;
    }

    public function getOptionsArray($conditions = array()) {

      $options = array();

      $conditions += array(
        'id_wh_type = ?' => 1,
        'type = ?' => 'B2B',
      );

      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from($this->_name, array('id', 'company', 'last_name', 'first_name'))
        ->order(array('company', 'last_name', 'first_name'));

      if ($conditions) {
        foreach ($conditions as $cond => $value) {
          $select->where($cond, $value);
        }
      }


      $results = $select->query(Zend_Db::FETCH_OBJ);

      while($result = $results->fetch()) {

        $chunks = array();
        $chunks[] = $result->company;
        if ($result->company && ($result->last_name || $result->first_name)) {
          $chunks[] = '-';
        }
        $chunks[] = $result->last_name;
        $chunks[] = $result->first_name;
        if ($name = trim(implode(' ', $chunks))) {
          $options[$result->id] = $name;
        }
      }
      return $options;
    }

    public function getSellers(int $wh_type = null): array
    {
        $select = $this->select()
            ->from($this->_name, ['id', 'company'])
            ->where('seller = 1')
            ->where('id_wh_type = ?', $wh_type);
        return $this->getAdapter()->fetchPairs($select);
    }

}
