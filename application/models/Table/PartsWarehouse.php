<?php
class Table_PartsWarehouse extends Zend_Db_Table_Abstract{

	protected $_name = 'parts_warehouse';

	protected $_primary = 'id';

	protected $_part_wh_amounts = array();

	protected $_part_base_wh_amounts = array();

  const WH_TYPE = 1;

	public function init(){
		parent::init();
		$this->getAdapter()->setFetchMode(Zend_Db::FETCH_ASSOC);
	}

	/**
	 * Soucasti, ktere nejsou produkty a maji byt tucne - NA POZADAVEK klienta 13.2.2010
	 * @var unknown_type
	 */
	protected static $_bold_parts = array('AS26','AS27','AS28','AS29','A30','AS30');

	public static function getBoldParts(){
		return self::$_bold_parts;
	}

    /**
     * Retrieves the filtered warehoused parts based on given parameters.
     *
     * @param string $warehouse The warehouse type ('null', 'base', or external warehouse ID)
     * @param array|null $id_coll An array of collection IDs to filter by
     * @param string $id_ctg The ID of the parts category to filter by
     * @param string $name The name of the parts to filter by
     * @param string $id The ID of the parts to filter by
     * @param string $prod The product flag to filter by ('y' for products, 'n' for all)
     * @param string $producer The ID of the producer to filter by
     * @param string $multi The multipart flag to filter by ('y' for non-multi parts, 'n' for all)
     * @return Zend_Db_Table_Rowset_Abstract The filtered warehouse parts
     */
    public function getFilteredWarehouseStatus(string $warehouse, ?array $id_coll, string $id_ctg, string $name, string $id, string $prod, string $producer, string $multi): Zend_Db_Table_Rowset_Abstract
    {
        $select = $this->select();
        $select->setIntegrityCheck(false);

        if ($warehouse === 'null' || $warehouse === 'base') {
            // All or central WH
            $select->from('parts_warehouse', [
                'amount_base' => "(amount - IFNULL((SELECT SUM(amount) FROM parts_warehouse_extern WHERE id_parts = p.id GROUP BY id_parts), 0))",
                'amount_all' => 'amount']);
        } else {
            // External WH
            // Old simple select:
            $select->from(['whe' => 'parts_warehouse_extern'], ['amount_ext' => 'amount'])->where("id_extern_warehouses = ?", $warehouse);
            //*

            /* // Compound select with joined to WH history:
            $authNamespace = new Zend_Session_Namespace('Zend_Auth');
            $fSelectA = $this->getAdapter()->select();
            $fSelectA
                ->from('parts_warehouse_extern_history', array('id_parts', 'amount_ext' => new Zend_Db_Expr(0), 'id_extern_warehouses'))
                ->where('id_users = ?', $authNamespace->user_id)
                ->group('id_parts');
            $fSelectB = $this->getAdapter()->select();
            $fSelectB
                ->from('parts_warehouse_extern', array('id_parts', 'amount_ext' => 'amount', 'id_extern_warehouses'));
            $fSelect  = $this->getAdapter()->select();
            $fSelect
                ->from(array('whe_uni' => new Zend_Db_Expr('((' . $fSelectA . ') UNION (' . $fSelectB .'))')),
                    array('id_parts', 'amount_ext' => new Zend_Db_Expr('MAX(amount_ext)'), 'id_extern_warehouses'))
                ->group('id_parts');
            $select
                ->from(array('whe' => new Zend_Db_Expr('(' . $fSelect . ')')))
                ->where("id_extern_warehouses = ?", $warehouse);
            //*/
        }

        $select->joinLeft(['p' => 'parts'], 'id_parts LIKE p.id', ['id', 'id_parts_ctg', 'name', 'multipart', 'set', 'product', 'semifinished'])
            ->joinLeft(['pc' => 'parts_ctg'], "p.id_parts_ctg = pc.id", ['ctg_name' => 'name'])
            ->joinLeft(['pc2' => 'parts_ctg'], "pc2.id = pc.parent_ctg", ['parent_ctg_name' => 'name'])
            ->joinLeft(['prod' => 'producers_parts'], "prod.id_parts = p.id", ["id_producers", "producer_name" => new Zend_Db_Expr("(SELECT name FROM producers AS tmp WHERE tmp.id = prod.id_producers)")])
            ->order(['id_parts_ctg', 'p.id']);

        // External WH
        if ($warehouse !== 'null' && $warehouse !== 'base') {
            $select->joinLeft(
                ['whc' => 'parts_warehouse'],
                'whe.id_parts = whc.id_parts',
                ['amount_base' => "(whc.amount - whe.amount)", 'amount_all' => 'amount']
            );
        }

        if (is_array($id_coll)) {
            $where = '';
            foreach ($id_coll as $coll) {
                if ($where === '') {
                    $where = "EXISTS(SELECT id FROM parts_collections WHERE id_parts = p.id AND id_collections = " . $this->_db->quote($coll) . ")";
                } else {
                    $where .= "OR EXISTS(SELECT id FROM parts_collections WHERE id_parts = p.id AND id_collections = " . $this->_db->quote($coll) . ")";
                }
            }
            if (!empty($where)) {
                $select->where($where);
            }
        }

        if (!empty($id_ctg) && $id_ctg !== 'null') {
            $select->where("p.id_parts_ctg = ? OR (pc2.id = $id_ctg)", $id_ctg);
        }

        if (!empty($name) && $name !== 'null') {
            $select->where("p.name LIKE ?", "%" . $name . "%");
        }

        if (!empty($id) && $id !== 'null') {
            if (count(explode(",", $id)) > 1) {
                $ids = explode(",", $id);
                $whereIds = [];
                foreach ($ids as $tmpId) {
                    $whereIds[] = "p.id LIKE " . $this->getAdapter()->quote("%" . $tmpId . "%");
                }
                $where = implode(" OR ", $whereIds);
                $select->where($where);
            } else {
                $select->where("p.id LIKE ?", "%" . $id . "%");
            }
        }

        if (!empty($prod) && $prod === 'y') {
            $select->where("p.product = 'y'");
        }

        if (!empty($producer) && $producer !== 'null') {
            $select->where("prod.id_producers = ?", $producer);
        }

        if (!empty($multi) && $prod !== 'null' && $multi === 'y') {
            $select->where("NOT EXISTS(SELECT * FROM parts_subparts WHERE id_multipart = p.id)");
        }
        $select->where("p.id_wh_type = ?", Parts_Part::WH_TYPE_PIPES);
        $select->where('p.deleted = ?', 'n');

        return $this->fetchAll($select);
    }

    /**
     * vklada do skladu zadany pocet soucasti a upravuje stav podsoucasti na skladu
	 * @param $id
	 * @param $amount
	 * @return string
   * @throws Parts_Exceptions_LowAmount
	 */
	public function addPart($id, $amount, $preferCentral = false, $errorMsgGlue = '<br>', $date = NULL, $msg = NULL, &$last_history_id = NULL) {
		$error = false;
    $tmp_part = new Parts_Part($id, $amount);
		$message = '';
    $message_error = array();
		if($tmp_part->isMultipart()){
      foreach($tmp_part->getSubparts() as $subpart){
        try {
          $message .= $this->removePart($subpart->getID(), $subpart->getAmount(), $preferCentral);
        } catch (Parts_Exceptions_LowAmount $e) {
          $error = true;
          $message_error[] = $e->getMessage();
        }
      }
    }
    if ($error) {
      throw new Parts_Exceptions_LowAmount(implode($errorMsgGlue, $message_error));
    } else {
      $added_amount = (int) $amount;
      $amount = (int) $amount + (int) $this->getAmount($id, true);
      $data = array(
        'amount' => $amount,
        'date' => $date,
        'description' => $msg,
      );
      $last_history_id = $this->update($data, "id_parts LIKE '".$id."'");

      $tSAL = new Table_SystemActionLog();
      $tSAL->addAction(Table_SystemActionLog::ACTION_PART_ADD, 'id=' . $id . ', amount=' . $added_amount);
      return $message;
    }
	}

	/**
	 * Vklada soucast do skladu, aniz by odecitala stavy podsoucasti
	 * @param $id
	 * @param $amount
	 */
	public function insertPart($id, $amount, &$last_history_id = NULL){
		$amount = (int) $amount + (int) $this->getAmount($id);
    $last_history_id = $this->update(array("amount" => $amount), "id_parts LIKE '".$id."'");
	}

	/**
	 * Vyrazeni soucasti ze skladu. Pokud je $preferCentral = TRUE
   * vyrazuje se nejprve z centralniho skladu a pak teprve z externiho.
   *
	 * @param string $id soucast
	 * @param int $amount pocet
   * @param boolean $preferCentral optional preferovat odecet nejprve z centralnio skladu
	 * @return string zprava o pridani
	 */
	public function removePart($id, $amount, $preferCentral = false, $date = NULL, $msg = NULL, &$last_history_id = NULL) {

    $total_amount = $this->getAmount($id, true);
    $base_amount = $this->getBaseAmount($id);
    $db = Zend_Registry::get('db');
    $message = '';
    $id_extern = null;

    // 1) kontrola dostatecneho mnoztvi
    if ($amount > $total_amount) {
      throw new Parts_Exceptions_LowAmount("Chybí "
        . ($amount - $total_amount) . " ks " . $id);
    }

    // 2) kontrola, zda-li je v pripade odpoctu z externiho skladu,
    // soucast pouze v jedinem externim skladu
    if (!$preferCentral || $amount > $base_amount) {
      $select = new Zend_Db_Select($db);
      $select
        ->from('parts_warehouse_extern', array('id_extern_warehouses', 'amount'))
        ->where('id_parts = ?', $id);
      $rows = $select->query()->fetchAll();

      if (count($rows) > 1) {

				$externi_sklady = '';
				foreach($rows as $row) {
          $select = new Zend_Db_Select($db);
          $select
            ->from('extern_warehouses', array('name'))
            ->where('id = ?', $row['id_extern_warehouses']);

          $name = $select->query()->fetchColumn();
					$externi_sklady .= $name.', ';
				}
				throw new Parts_Exceptions_MultipleExternWH("Součást " . $id
          . " je obsažena ve více externích skladech ($externi_sklady) <br />"
          . " a není jasné, ze kterého z nich se má odečíst stav. <br /><br />"
          . " Možné řešení: Všechny součásti " . $id
          . " musí být při odečítání maximálné v jednom externím skladu, takže "
          . " je z ostatních externích skladu přeneste do centrálního.");
      } elseif (count($rows) == 1) {
        $id_extern = $rows[0]['id_extern_warehouses'];
      }
    }

    // 3) stanoveni poctu odectu z centralniho a externiho skladu dle preference
    if ($preferCentral || is_null($id_extern)) {
      $from_base = min(array($amount, $base_amount));
      $from_ext = $amount - $from_base;
    } else {
      $from_ext = min(array($amount, $total_amount - $base_amount));
      $from_base = $amount - $from_ext;
    }

    // 4a) odecet z centralniho skladu
    $data = array(
      'amount' => $total_amount - $amount,
      'date' => $date,
      'description' => $msg,
    );
    $last_history_id = $this->update($data, 'id_parts LIKE ' . $db->quote($id));

    $tSAL = new Table_SystemActionLog();

    if ($from_base) {
      $message .= $from_base . ' ks ' . $id
        . ' bylo odečteno z <b>centrálního skladu</b>.<br />';
      //logování operace
      $tSAL->addAction(Table_SystemActionLog::ACTION_PART_REMOVE, 'id=' . $id . ', amount=' . $from_base . ', extern=0');
    }

    // 4b) odecet z externiho skladu
    if ($from_ext) {
      $pwe = new Table_PartsWarehouseExtern();

      $pwe->removePart($id_extern, $id, $from_ext);
      $select = new Zend_Db_Select($db);
      $select
        ->from('extern_warehouses', array('name'))
        ->where('id = ?', $id_extern);

      $name = $select->query()->fetchColumn();
      $message .= $from_ext . ' ks ' . $id
        . ' bylo odečteno z externího skladu <b>' . $name . '</b>.';
      //logování operace
      $tSAL->addAction(Table_SystemActionLog::ACTION_PART_REMOVE, 'id=' . $id . ', amount=' . $from_ext . ', extern=' . $id_extern);
    }

    return '<br />' . $message;
	}

  /**
   * Kontrola limitního množství
   *
   * @param int $id ID parts
   */
  public function checkCritical($id) {
    // limitní množství
    $crit = $this->select()->setIntegrityCheck(false)
            ->from($this->_name)
            ->where('id_parts = ?', $id)
            ->joinLeft('parts', 'parts.id = ' . $this->_name . '.id_parts', 'name')
            ->query()->fetch();

    if ($crit['amount']<=$crit['critical_amount']) {
      return $crit['name'];
    } else {
      return FALSE;
    }
  }

	/**
	 * Odstrani ze skladu podsoucasti soucasti podle uvedeneho mnozstvi
	 * @param $id
	 * @param $amount
	 */
	private function reduceSubparts($id, $amount, &$last_history_id = NULL){
		$tmp_part = new Parts_Part($id);
		foreach($tmp_part->getSubparts() as $part){

		}
		$amount = (int) $amount + $this->getAmount($id);
    $last_history_id = $this->update(array("amount" => $amount), "id_parts LIKE '".$id."'");
	}

	/**
	 * Vyrazovani soucasti z centralniho skladu
	 * @param <type> $id
	 * @param <type> $amount
	 * @return <type>
	 */
	public function reduceCentralAmount($id, $amount, &$last_history_id = NULL){
		if($amount > $this->getBaseAmount($id)){
			throw new Parts_Exceptions_LowAmount('V centrálním skladu je méně kusů součásti, než má být vyřazeno.');
		} else {
			$new_amount = (int) $this->getAmount($id) - (int) $amount;
			$last_history_id = $this->update(array("amount" => $new_amount), "id_parts LIKE '".$id."'");
			return true;
		}
	}

	/**
	 * Vyrazovani soucasti z centralniho skladu
	 * @param <type> $id
	 * @param <type> $amount
	 * @return <type>
	 */
	public function reduceExternAmount($id_part, $amount, $id_wh){

		$extern_wh = new Table_PartsWarehouseExtern();
		if($extern_wh->getAmount($id_wh, $id_part) < $amount){
			throw new Parts_Exceptions_LowAmount('V centrálním skladu je méně kusů součásti, než má být vyřazeno.');
		} else {
			$new_amount = $extern_wh->getAmount($id_wh, $id_part) - $amount;
			$db = Zend_Registry::get('db');
			try{
				$db->beginTransaction();
				$extern_wh->removePart($id_wh, $id_part, $amount);
				$this->reduceCentralAmount($id_part, $amount);
				$db->commit();
			} catch (Exception $e) {
				$db->rollback();
				throw $e;
			}
		}

	}

	/**
	 * Vraci mnozstvi soucasi skladem
	 *
	 * @param $idPart
   *
	 * @return unknown_type
	 */
	public function getAmount($idPart, $from_db = FALSE, Meduse_Date $date = NULL) {
    $to = new Meduse_Date();
    $this->getAdapter()->setFetchMode(Zend_Db::FETCH_ASSOC);
    if(!isset($this->_part_wh_amounts[$idPart]) || $from_db){
      $select = $this->select();
      $select->from('parts_warehouse')->where("id_parts LIKE ?", $idPart);
      $row = $select->query()->fetch();
      $this->_part_wh_amounts[$idPart] = (int) $row['amount'];
    }
    $amount = $this->_part_wh_amounts[$idPart];

    // Bez datumu se ptame na soucasne mnozstvi.
    if (is_null($date) || $date >= $to) {
      return $amount;
    }

    $wh = new Tobacco_Warehouse();
    if ($snaps = $wh->getSnapshots($idPart, $date, $to, 'ASC')) {
      // Pokud najdeme inventurni stav, zmeny pocitame od tohoto okamziku.
      $snap = array_shift($snaps);
      $to = new Meduse_Date($snap['date'], 'Y-m-d H:i:s');
      $amount = $snap['amount_wh'];
    }
    $tHistory = new Table_PartsWarehouseHistory();
    $changes = $tHistory->getChanges($idPart, $date, $to, TRUE);
    return $amount - $changes['amount_add'] + $changes['amount_remove'];

	}

	/**
	 * Vraci pocet soucasti skladem - bez soucasti v externich skladech
	 * @return unknown_type
	 */
	public function getBaseAmount($id){
                //die($id);
		if(!isset($this->_part_base_wh_amounts[$id])){
			$amount_base = $this->getAmount($id);
			$select = $this->select();
	    	$select->setIntegrityCheck(false);
	    	$select->from('parts_warehouse_extern', array('amount' => 'SUM(amount)'))->where("id_parts = ?", $id)->group('id_parts');
	    	$amount_extern = $this->getAdapter()->fetchOne($select);
	    	$this->_part_base_wh_amounts[$id] = ($amount_base - $amount_extern);
		}
    	return $this->_part_base_wh_amounts[$id];
	}

	 /**
     * Updates existing rows.
     *
     * @param  array        $data  Column-value pairs.
     * @param  array|string $where An SQL WHERE clause, or an array of SQL WHERE clauses.
     * @return int          The ID of last inserted rows in history table
    *  @throws \Exception
     */
    public function update(array $data, $where){
      $update = TRUE;
    	$authNamespace = new Zend_Session_Namespace('Zend_Auth');
    	if (array_key_exists('amount', $data)) {
        $new_amount = $data['amount'];
      }
      else {
        $new_amount = 0;
    	}
      /* Od 29.06.2020 zde nastavala chyba */
      $history_data = [];
      if ($row = $this->fetchRow($where)) {
        $history_data = $row->toArray();
      }
    	if (empty($history_data)) {
    	  throw new Exception('Položka ve skladu nebyla nalezena.');
      }
    	unset($history_data['id']);
    	if ($new_amount >= 0) {
    		$history_data['amount'] = $new_amount - $history_data['amount'];
    	}
    	$history_data['id_users'] = $authNamespace->user_id;
    	if (isset($data['description'])) {
        $history_data['note'] = $data['description'];
        unset($data['description']);
      }
    	else {
        unset($history_data['note']);
      }
    	if (isset($data['critical_amount'])) {
    	  $history_data['critical_amount'] = $data['critical_amount'];
      }
    	if (isset($data['date'])) {
        $history_data['last_change'] = $data['date'];
        // Pokud neexistuje inventarni zaznam s datem vyssim nez zadane datum,
        // pak provedeme i update aktualniho stavu skladu
        if (!is_null($data['date'])) {
          $tSnap = new Table_PartsWarehouseSnapshot();
          if ($snap = $tSnap->fetchRow($where . ' AND date > "' . $data['date'] . '"', 'date DESC')) {
            $update = FALSE;
          }
        }
        unset($data['date']);
      }
    	else {
        unset($history_data['last_change']);
      }
    	// Update aktualniho stavu skladu.
    	if ($update) {
    	  unset($data['date']);
    	  unset($data['description']);
        parent::update($data, $where);
      }
    	$tHistory = new Table_PartsWarehouseHistory();
    	return $tHistory->insert($history_data);
    }

	public function getHistory($idParts = null, $id_wh_type = Parts_Part::WH_TYPE_PIPES) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(
			array('pwh' => 'parts_warehouse_history'),
			array('id','amount','last_change','id_parts','note')
		)->join(
			array('p' => 'parts'),
			"pwh.id_parts = p.id",
			array('name')
		)->join(
			array('u' => 'users'),
			"pwh.id_users = u.id",
			array('username' => 'description')
		)->order("pwh.id DESC");

		if($idParts !== null){
			$select->where("pwh.id_parts = ?",$idParts);
			$select->limit(30);
		} else {
			$select->limit(100);
		}
    $select->where('id_wh_type = ?', $id_wh_type);
		return $this->fetchAll($select);
	}


        public function getPartAmountNumber($id, $isArray = false) {
            $data = $this->fetchRow('id = "' . $id . '"');

            if(!$data) {
                if($isArray) {
                    return array($id => 0);
                }

                return 0;
            }
            else {
                if($isArray) {
                    return array($data['id'] => $data['amount']);
                }

                return (int)$data['amount'];
            }
	}

  public function getCritical(): Zend_Db_Select
  {
    return $this->select()
      ->from($this->_name, array('amount', 'id_parts'))
      ->setIntegrityCheck(false)
      ->joinLeft('parts', 'parts.id = ' . $this->_name . '.id_parts AND parts.deleted = \'n\'', ['name'])
      ->where($this->_name . '.amount <= ' . $this->_name . '.critical_amount')
      ->where($this->_name . '.critical_amount > 0');
  }

  public function getCriticalTobaccoNFS(): array
  {
    $select = $this->getCritical();
    $select->where('parts.id_parts_ctg = ' . Table_TobaccoWarehouse::CTG_TOBACCO_NFS . ' OR parts.id_parts_ctg = ' . Table_TobaccoWarehouse::CTG_TOBACCO_FS);
    return $select->query()->fetchAll();
  }

  public function getCriticalMaterialsTobacco(): array
  {
    $select = $this->getCritical();
    $select->where('parts.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_MATERIALS_TOBACCO);
    return $select->query()->fetchAll();
  }

  public function getCriticalMaterialsFlavors(): array
  {
    $select = $this->getCritical();
    $select->where('parts.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS);
    return $select->query()->fetchAll();
  }

  public function getCriticalMaterialsOther(): array
  {
    $select = $this->getCritical();
    $select->where('parts.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_MATERIALS_OTHER);
    return $select->query()->fetchAll();
  }

  public function getCriticalPackaging($subcategory = Table_TobaccoWarehouse::CTG_PACKAGING): array
  {
    $select = $this->getCritical();
    $select->where('parts.id_parts_ctg = ?', $subcategory);
    return $select->query()->fetchAll();
  }

  public function getCriticalStamps(): array
  {
    $select = $this->getCritical();
    $select->where('parts.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_DUTY_STAMPS);
    return $select->query()->fetchAll();
  }

  public function getCriticalAccessories(): array
  {
    $select = $this->getCritical();
    $select->where('parts.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_ACCESSORIES);
    return $select->query()->fetchAll();
  }

  public function batchConsolidate(bool $dryRun = false): stdClass {

        $result = new stdClass();

        $db = $this->getAdapter();
        $db->beginTransaction();

        try {
            $db->query(<<<SQL
                CREATE TEMPORARY TABLE batch_repair
                    SELECT pw.id_parts,
                           s.batch,
                           pw.amount            AS amount,
                           s.amount             AS batch_amount,
                           s.amount - pw.amount AS diff
                    FROM parts_warehouse pw
                             JOIN (
                        SELECT pb.id_parts, SUM(pb.amount) as amount, MAX(pb.batch) AS batch
                        FROM parts_batch pb
                        GROUP BY pb.id_parts
                    ) s ON s.id_parts = pw.id_parts;
            SQL
            );

            $querySelect = $db->query(<<<SQL
                SELECT * FROM batch_repair;
            SQL
            );

            $repaired = $querySelect->fetchAll(Zend_Db::FETCH_ASSOC);

            if (!$dryRun) {
                $db->query(<<<SQL
                                TRUNCATE TABLE parts_batch;
                            SQL
                );

                $db->query(<<<SQL
                                INSERT INTO parts_batch (id_parts, batch, amount)
                                SELECT br.id_parts, br.batch, br.amount
                                FROM batch_repair br;
                            SQL
                );
            }
            $db->query(<<<SQL
                DROP TABLE batch_repair;
            SQL
            );

            $db->commit();

            $result->error = false;
            $result->repaired = $repaired;
            $result->dryRun = $dryRun;

        } catch (Zend_Db_Statement_Exception $e) {
            $db->rollBack();
            Utils::logger()->err($e->getMessage());
            $result->error = true;
            $result->message = $e->getMessage();
        }

      return $result;
  }
}
