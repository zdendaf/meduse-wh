<?php

class Table_PackingsSaves extends Zend_Db_Table_Abstract {

  protected $_name = 'packings_saves';
  protected $_primary = 'id';
  
  /**
   * Uložení rozpracované podoby PL
   * 
   * @param int $order ID obj.
   * @param string $json JSON data s produkty
   * @param string $autosave zdali se jedná o automatické uložení (y/n)
   */
  public function save($order, $json, $autosave = 'n') {
    $this->insert(array(
        'id_orders' => $order,
        'json'      => $json,
        'autosave'  => $autosave,
    ));
  }
  
  /**
   * Nahrání poslední uložené podoby PL
   * 
   * @param type $order ID obj.
   * @return array Poslední rozložení s dodatečnými daty
   */
  
  public function loadLast($order) {
    return $this->select()->where('id_orders = ?', $order)->order('time DESC')->query();
  }
}
