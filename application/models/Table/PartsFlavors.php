<?php

class Table_PartsFlavors extends Zend_Db_Table_Abstract {

  protected $_name = 'parts_flavors';
  protected $_primary = 'id';

  public function merge($data, $where) {
    $select = $this->select()->where($where);
    if ($select->query()->fetch()) {
      $this->update($data, $where);
    }
    else {
      $this->insert($data);
    }


  }

}