<?php
class Table_Productions extends Zend_Db_Table_Abstract{

	protected $_name = 'productions';

	protected $_primary = 'id';

	const STATUS_OPENED = 'open';
	const STATUS_PARTIAL = 'parial';
	const STATUS_CLOSED = 'closed';

	public function getProductions($idParts){
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(
			"w_productions_current",
			array('id_productions','amount','amount_remain', 'date_delivery')
		)->where(
			"id_parts = ?", $idParts
		);
		return $this->fetchAll($select);
	}

	public function getProductionsByProducer($idProducer, $operation = false){
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(
			"w_productions_current",
			array('id_productions','date_ordering', 'date_delivery')
		)->joinLeft(
			"producers",
			"producers.id = w_productions_current.id_producers"
		)->where(
			"id_producers = ?", $idProducer
		)->group('id_productions');
    
    if ($operation) {
      $select->where('id_parts_operations IS NOT NULL');
    }
		return $this->fetchAll($select);
	}

	public function getParts($idProductions) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select
			->from(array('pp' => 'productions_parts'),array('*','id_productions_parts' => 'id','p_id' => new Zend_Db_Expr("''"),'p_name' => new Zend_Db_Expr("''")))
			->joinLeft(array('p' => 'parts'), "p.id = pp.id_parts")
      ->joinLeft(array('pdp' => 'producers_parts'), 'pdp.id_parts = p.id', array('producer_price' => 'price'))
			->where('id_productions = ?', $idProductions);
		return $this->fetchAll($select);
	}

  public function getDetails($idProductions) {
    $select = $this->select();
    $select->setIntegrityCheck(FALSE);
    $select->from(['prod' => $this->_name])
      ->joinLeft(['producer' => 'producers'], "prod.id_producers = producer.id", ['name'])
      ->joinLeft(['users' => 'users'], 'prod.id_users = users.id', ['username' => 'users.name_full'])
      ->where('prod.id = ?', $idProductions);
    return $this->fetchRow($select);
  }

  public function getDeliveries($idProductions) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select
			->from(array('pd' => 'productions_deliveries'))
			->joinLeft(array('pdp' => 'productions_deliveries_parts'),
                "pdp.id_productions_deliveries = pd.id"
                ,
                array(
                    'delivery_amount' => new Zend_Db_Expr('SUM(delivery_amount)'),
                    'id_productions_deliveries'
                )
            )
      ->joinLeft(['u' => 'users'], 'pd.id_users = u.id', ['name'])
			->joinLeft(array('pp' => 'productions_parts'), "pdp.id_productions_parts = pp.id", array('id_parts'))
			->where('pd.id_productions = ?', $idProductions)
			->order(array('pd.delivery_date','id_parts'))
            ->group(array('pd.delivery_date','id_parts'));
		$rows =  $this->fetchAll($select);
    
    $data = array();
		foreach ($rows as $row) {

		  if (!isset($data[$row['delivery_date']])) {
		    $data[$row['delivery_date']] = ['user' => $row['name'], 'parts' => []];
      };

      if (!isset($data[$row['delivery_date']]['parts'][$row['id_parts']])) {
        $data[$row['delivery_date']]['parts'][$row['id_parts']] = 0;
      };

      $data[$row['delivery_date']]['parts'][$row['id_parts']] += (int) $row['delivery_amount'];
    }    
    return $data;
	}

	public function getDelayedProduction($wh = Table_PartsWarehouse::WH_TYPE) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select
      ->where('status LIKE ?', Table_Productions::STATUS_OPENED)
			->where('date_delivery < ?', new Zend_Db_Expr('DATE_ADD(NOW(), INTERVAL -1 DAY)'))
		  ->where('id_wh_type = ?', $wh);
		return $select->query()->fetchAll();
	}

	public function notifyDelayedProduction($productionId) {
		$detail = $this->getDetails($productionId)->toArray();
		$parts = $this->getParts($productionId)->toArray();
		$data = $detail;
		$data['parts'] = $parts;
		$tEmails = new Table_Emails();
		$tEmails->sendEmail(Table_Emails::EMAIL_PRODUCTION_DELAY, $data);
		$this->update(array('date_notify' => date('Y-m-d')), 'id = ' . $productionId);
	}

  public function getAllProductions($wh = Table_PartsWarehouse::WH_TYPE, $status = NULL, $categories = []) {

	  $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(['pn' => 'productions']);
    $select->join(['pds' => 'producers'], 'pds.id = pn.id_producers', ['name']);
    $select->joinLeft(['pd' => 'productions_deliveries'], 'pd.id_productions = pn.id', ['invoice_id']);
    $select->where('pn.id_wh_type = ?', $wh);
    $select->order('pn.date_ordering DESC');

    // Filtrace pres stav objednavky.
    if ($status && in_array($status, array(Table_Productions::STATUS_OPENED, Table_Productions::STATUS_PARTIAL, Table_Productions::STATUS_CLOSED))) {
      $select->where('pn.status LIKE ?', Table_Productions::STATUS_OPENED);
    }

    // Filtrace pres kategorie objenavanych produktu.
    if (!empty($categories)) {
      $select
        ->join(['pp' => 'productions_parts'], 'pp.id_productions = pn.id', [])
        ->join(['p' => 'parts'], 'p.id = pp.id_parts AND p.id_parts_ctg IN (' . implode(', ', $categories) . ')', [])
        ->group('pn.id');
    }

    return $select->query()->fetchAll();
  }
}
