<?php
/**
 * Trida pro praci s tabulkou exter_warehouses
 *
 * @author Jakub Niec
 */
class Table_ExternWarehouses extends Zend_Db_Table_Abstract {

	protected $_name = 'extern_warehouses';

	/**
	 * Vraci select, ktery zobrazujeme v datalistu
	 * @param int $id_extern_warehouse
	 * @param int $id_user
	 */
    public function getDataList($id_extern_warehouse, $id_user) {
		$select = $this->getAdapter()->select();
		$select->from(
			array('ew' => $this->_name)
		)->joinLeft(
			array('pwe' => 'parts_warehouse_extern'),
			'ew.id = pwe.id_extern_warehouses'
		)->where(
			'ew.id = ?',$id_extern_warehouse
		)->where(
			'users IS NOT NULL'
		)->where(
			'? IN (ew.users)'
		);
	}

	/**
	 * Vraci seznam externich skladu, ke kterym ma dany uzivatel pristup
	 * @param <type> $id_user
	 * @return <type>
	 */
	public function getWarehouses($id_user) {
		$select = $this->getAdapter()->select();
		$select->from(
			array('ew' => $this->_name),
			array('id')
		)->where(
			'users IS NOT NULL'
		)->where(
			'? IN (ew.users)'
		);
		return $this->getAdapter()->fetchOne($select);
	}
	
}
