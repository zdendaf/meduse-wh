<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
class Table_OrdersProductsBatch extends Zend_Db_Table_Abstract{

	protected $_name = 'orders_products_batch';
	protected $_primary = 'id';

  public function addBatch($order_id, $part_id, $batch, $amount) {

    if ($amount == 0) {
      return;
    }

    $select = $this->select()
      ->setIntegrityCheck(false)
      ->from('orders_products', 'id')
      ->where('id_orders = ?', $order_id)
      ->where('id_products LIKE ?', $part_id);

    $id = $select->query()->fetchColumn();
    if (!$id) {
      throw new Exception('Nepodařilo se najít příslušný řádek objednávky.');
    }

    $this->insert(array(
      'id_orders_products' => $id,
      'batch' => $batch,
      'amount' => $amount,
    ));

  }

}

