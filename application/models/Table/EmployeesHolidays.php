<?php

  class Table_EmployeesHolidays extends Zend_Db_Table_Abstract {

    protected $_name = 'employees_holidays';
    protected $_primary = 'date';

    public function getHolidays($year = NULL, $asObject = TRUE) {
      $select = $this->select()->order('date ASC');
      if ($year) {
        $select
        ->where('date >= ?', $year . '-01-01')
        ->where('date <= ?', $year . '-12-31');
      }
      $data = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
      $holidays = array();
      if ($data) {
        foreach ($data as $row) {
          $holidays[$row->date] = array(
            'date' => $asObject ? new Meduse_Date($row->date, 'Y-m-d') : $row->date,
            'change' => is_null($row->change) ? NULL : ($asObject ? new Meduse_Date($row->change, 'Y-m-d') : $row->change)
          );
        }
      }
      ksort($holidays);
      return $holidays;
    }
  }
