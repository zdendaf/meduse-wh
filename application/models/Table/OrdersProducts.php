<?php

class Table_OrdersProducts extends Zend_Db_Table_Abstract {

  protected $_name = 'orders_products';
  protected $_primary = 'id';

  public function updateAmount($paramsObject) {
    $data = array(
        'amount' => $paramsObject->newVal
    );

    $this->update($data, "id_orders = '{$paramsObject->oid}' AND id_products = '{$paramsObject->pid}'");
  }

  public function getEkokomSales($options) {
    $select = $this->select()
      ->setIntegrityCheck(false)
      ->from(array('op' => 'orders_products'), array(
        'id' => 'id_products',
        'amount' => new Zend_Db_Expr('SUM(op.amount + op.free_amount)')
      ))
      ->joinLeft(array('o' => 'orders'), 'o.id = op.id_orders', array())
      ->where('o.id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE)
      ->where('o.status = ?', Table_Orders::STATUS_CLOSED)
      ->group('id');
    if (isset($options['from']) && !empty($options['from'])) {
      $date = new Zend_Date($options['from'], 'dd.MM.yyyy');
      $select->where('o.date_request >= ?', $date->toString('yyyy-MM-dd'));
    }
    if (isset($options['to']) && !empty($options['to'])) {
      $date = new Zend_Date($options['to'], 'dd.MM.yyyy');
      $select->where('o.date_request <= ?', $date->toString('yyyy-MM-dd'));
    }

    return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }


  /**
   * Report celkoveho prodeje tabaku
   * @param array $options asociativni pole nepovinnych parametru reportu:
   *    - from      ... pocatecni datum reportu
   *    - to        ... koncove datum reportu
   *    - country   ... kod zeme (fakturacni adresa)
   *    - customer  ... id zakaznika
   *    - flavor    ... id prichute
   */
  public function getTotalTobaccoSales(array $options) {

    $tobaccoSales = $this->getTotalTobaccoSalesData($options);

    $summary = new stdClass();
    $summary->total = NULL;
    $summary->mixes = array();
    $summary->packages = array();

    if ($tobaccoSales) {

      $summary->total = new stdClass();
      $summary->total->count = 0;
      $summary->total->weight = 0;
      $summary->total->price_czk = 0;
      $summary->total->price_eur = 0;
      $summary->total->price = 0;

      foreach ($tobaccoSales as $item) {
        // total
        $summary->total->count += $item['pcs'];
        $summary->total->weight += $item['weight'];
        $summary->total->price_czk += $item['total_czk'];
        $summary->total->price_eur += $item['total_eur'];
        $summary->total->price += $item['total'];

        // pres prichute
        if (!isset($summary->mixes[$item['name']])) {
          $summary->mixes[$item['name']] = new StdClass();
          $summary->mixes[$item['name']]->name = $item['name'];
          $summary->mixes[$item['name']]->count = 0;
          $summary->mixes[$item['name']]->count_b2b = 0;
          $summary->mixes[$item['name']]->count_b2c = 0;
          $summary->mixes[$item['name']]->weight = 0;
          $summary->mixes[$item['name']]->weight_b2b = 0;
          $summary->mixes[$item['name']]->weight_b2c = 0;
          $summary->mixes[$item['name']]->price_czk = 0;
          $summary->mixes[$item['name']]->price_czk_b2b = 0;
          $summary->mixes[$item['name']]->price_czk_b2c = 0;
          $summary->mixes[$item['name']]->price_eur = 0;
          $summary->mixes[$item['name']]->price_eur_b2b = 0;
          $summary->mixes[$item['name']]->price_eur_b2c = 0;
          $summary->mixes[$item['name']]->price = 0;
          $summary->mixes[$item['name']]->price_b2b = 0;
          $summary->mixes[$item['name']]->price_b2c = 0;
          $summary->mixes[$item['name']]->data = array();
        }

        if (!isset($summary->mixes[$item['name']]->data[$item['pkg']])) {
          $summary->mixes[$item['name']]->data[$item['pkg']] = new stdClass();
          $summary->mixes[$item['name']]->data[$item['pkg']]->count = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->count_b2b = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->count_b2c = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->weight = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->weight_b2b = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->weight_b2c = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_czk = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_czk_b2b = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_czk_b2c = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_eur = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_eur_b2b = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_eur_b2c = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_b2b = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_b2c = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_avg_b2b = 0;
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_avg_b2c = 0;
        }

        $summary->mixes[$item['name']]->count += $item['pcs'];
        $summary->mixes[$item['name']]->weight += $item['weight'];
        $summary->mixes[$item['name']]->price_czk += $item['total_czk'];
        $summary->mixes[$item['name']]->price_eur += $item['total_eur'];
        $summary->mixes[$item['name']]->price += $item['total'];

        $summary->mixes[$item['name']]->data[$item['pkg']]->count += $item['pcs'];
        $summary->mixes[$item['name']]->data[$item['pkg']]->weight += $item['weight'];
        $summary->mixes[$item['name']]->data[$item['pkg']]->price_czk += $item['total_czk'];
        $summary->mixes[$item['name']]->data[$item['pkg']]->price_eur += $item['total_eur'];
        $summary->mixes[$item['name']]->data[$item['pkg']]->price += $item['total'];

        if ($item['customer_type'] === Table_Customers::TYPE_B2B) {
          $summary->mixes[$item['name']]->count_b2b += $item['pcs'];
          $summary->mixes[$item['name']]->weight_b2b += $item['weight'];
          $summary->mixes[$item['name']]->price_czk_b2b += $item['total_czk'];
          $summary->mixes[$item['name']]->price_eur_b2b += $item['total_eur'];
          $summary->mixes[$item['name']]->price_b2b += $item['total'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->count_b2b += $item['pcs'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->weight_b2b += $item['weight'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_czk_b2b += $item['total_czk'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_eur_b2b += $item['total_eur'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_b2b += $item['total'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_avg_b2b = $item['price_avg'];
        }
        else {
          $summary->mixes[$item['name']]->count_b2c += $item['pcs'];
          $summary->mixes[$item['name']]->weight_b2c += $item['weight'];
          $summary->mixes[$item['name']]->price_czk_b2c += $item['total_czk'];
          $summary->mixes[$item['name']]->price_eur_b2c += $item['total_eur'];
          $summary->mixes[$item['name']]->price_b2c += $item['total'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->count_b2c += $item['pcs'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->weight_b2c += $item['weight'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_czk_b2c += $item['total_czk'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_eur_b2c += $item['total_eur'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_b2c += $item['total'];
          $summary->mixes[$item['name']]->data[$item['pkg']]->price_avg_b2c = $item['price_avg'];
        }

        // pres baleni
        if (!isset($summary->packages[$item['pkg']])) {
          $summary->packages[$item['pkg']] = new StdClass();
          $summary->packages[$item['pkg']]->count = 0;
          $summary->packages[$item['pkg']]->count_b2b = 0;
          $summary->packages[$item['pkg']]->count_b2c = 0;
          $summary->packages[$item['pkg']]->weight = 0;
          $summary->packages[$item['pkg']]->weight_b2b = 0;
          $summary->packages[$item['pkg']]->weight_b2c = 0;
          $summary->packages[$item['pkg']]->price_czk = 0;
          $summary->packages[$item['pkg']]->price_czk_b2b = 0;
          $summary->packages[$item['pkg']]->price_czk_b2c = 0;
          $summary->packages[$item['pkg']]->price_eur = 0;
          $summary->packages[$item['pkg']]->price_eur_b2b = 0;
          $summary->packages[$item['pkg']]->price_eur_b2c = 0;
          $summary->packages[$item['pkg']]->price = 0;
          $summary->packages[$item['pkg']]->price_b2b = 0;
          $summary->packages[$item['pkg']]->price_b2c = 0;
        }
        $summary->packages[$item['pkg']]->count += $item['pcs'];
        $summary->packages[$item['pkg']]->weight += $item['weight'];
        $summary->packages[$item['pkg']]->price_czk += $item['total_czk'];
        $summary->packages[$item['pkg']]->price_eur += $item['total_eur'];
        $summary->packages[$item['pkg']]->price += $item['total'];

        if ($item['customer_type'] === Table_Customers::TYPE_B2B) {
          $summary->packages[$item['pkg']]->count_b2b += $item['pcs'];
          $summary->packages[$item['pkg']]->weight_b2b += $item['weight'];
          $summary->packages[$item['pkg']]->price_czk_b2b += $item['total_czk'];
          $summary->packages[$item['pkg']]->price_eur_b2b += $item['total_eur'];
          $summary->packages[$item['pkg']]->price_b2b += $item['total'];
        }
        else {
          $summary->packages[$item['pkg']]->count_b2c += $item['pcs'];
          $summary->packages[$item['pkg']]->weight_b2c += $item['weight'];
          $summary->packages[$item['pkg']]->price_czk_b2c += $item['total_czk'];
          $summary->packages[$item['pkg']]->price_eur_b2c += $item['total_eur'];
          $summary->packages[$item['pkg']]->price_b2c += $item['total'];
        }
      }
      $this->tobaccoSalesSort($options['order'], $summary->mixes);
      $this->tobaccoSalesSort($options['order'], $summary->packages);
    }
    return $summary;
  }

  protected function tobaccoSalesSort($orderBy, &$data) {
    $method_name = 'tobaccoSalesSort_' . $orderBy;
    if (method_exists($this, $method_name)) {
      $this->$method_name($data);
    }
  }

  private function tobaccoSalesSort_pcs(&$data) {
    uasort($data, function($a, $b) {
      if ($a->count == $b->count) {
        return 0;
      }
      else {
        return $a->count < $b->count ? 1 : -1;
      }
    });
    foreach ($data as $name => $item) {
      if (isset($data[$name]->data)) {
        uasort($data[$name]->data, function($a, $b){
          if ($a->count == $b->count) {
            return 0;
          }
          else {
            return $a->count < $b->count ? 1 : -1;
          }
        });
      }
    }
  }

  private function tobaccoSalesSort_weight(&$data) {
    uasort($data, function($a, $b) {
      return $a->weight < $b->weight;
    });
    foreach ($data as $name => $item) {
      if (isset($data[$name]->data)) {
        uasort($data[$name]->data, function($a, $b){
          return $a->weight < $b->weight;
        });
      }
    }
  }

  private function tobaccoSalesSort_price(&$data) {
    uasort($data, function($a, $b) {
      if (isset($a->price) && isset($b->price)) return $a->price < $b->price;
      else return NULL;
    });
    foreach ($data as $name => $item) {
      if (isset($data[$name]->data)) {
        uasort($data[$name]->data, function($a, $b){
          if (isset($a->price) && isset($b->price)) return $a->price < $b->price;
          else return NULL;
        });
      }
    }
  }

  protected function getTotalTobaccoSalesData(array $options) {
    $selects = array();
    if ($options['type'] == 'fs' || $options['type'] == 'all') {
      $selects[] = '(' . $this->tobbacoSalesSelect('fs', $options) . ')';
    }
    if ($options['type'] == 'nfs' || $options['type'] == 'all') {
      $selects[] = '(' . $this->tobbacoSalesSelect('nfs', $options) . ')';
    }
    $uSelect = new Zend_Db_Select(Zend_Registry::get('db'));
    $uSelect->union($selects, Zend_Db_Select::SQL_UNION_ALL);
    $select = new Zend_Db_Select(Zend_Registry::get('db'));
    $select->from(array('t' => $uSelect), array(
      'pcs' => new Zend_Db_Expr('SUM(pcs)'),
      'total_czk' => new Zend_Db_Expr('ROUND(SUM(total_czk), 2)'),
      'total_eur' => new Zend_Db_Expr('ROUND(SUM(total_eur), 2)'),
      'total' => new Zend_Db_Expr('ROUND(SUM(total), 2)'),
      'pkg',
      'weight' => new Zend_Db_Expr('ROUND(SUM(weight), 3)'),
      'name',
      'price_avg' => new Zend_Db_Expr('ROUND(AVG(price), 2)'),
      'customer_type'
    ));
    $select
      ->group(array('t.name', 't.pkg', 't.customer_type'))
      ->order(array('t.name', 't.pkg', 't.customer_type'));
    // $s = $select . '';
    return $select->query()->fetchAll();
  }

  private function tobbacoSalesSelect($type, $options) {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from(array('op' => $this->_name), array(
          'pcs' => 'amount',
          'total_czk' => new Zend_Db_Expr('IF(i.currency = "CZK", (op.amount - op.free_amount) * IFNULL(ii.price * (1 - i.additional_discount / 100), 0) * i.rate, 0)'),
          'total_eur' => new Zend_Db_Expr('IF(i.currency = "EUR", (op.amount - op.free_amount) * IFNULL(ii.price * (1 - i.additional_discount / 100), 0), 0)'),
          'total' => new Zend_Db_Expr('(op.amount - op.free_amount) * IFNULL(ii.price * (1 - i.additional_discount / 100), 0) * i.rate'),
      ))
      ->joinLeft(array('o' => 'orders'), 'o.id = op.id_orders', array());

      if ($type == 'fs') {
        $select->joinLeft(array('ps' => 'parts_subparts'), 'ps.id_multipart = op.id_products', array())
        ->joinLeft(array('pm' => 'parts_mixes'), 'pm.id_parts = ps.id_parts', array(
            'pkg' => new Zend_Db_Expr('ROUND (pm.amount * 1000, 0)'),
            'weight' => new Zend_Db_Expr('pm.amount * op.amount'),
        ));
      } elseif ($type == 'nfs') {
        $select->joinLeft(array('pm' => 'parts_mixes'), 'pm.id_parts = op.id_products', array(
            'pkg' => new Zend_Db_Expr('ROUND (pm.amount * 1000, 0)'),
            'weight' => new Zend_Db_Expr('pm.amount * op.amount'),
        ));
      }
      $select->joinLeft(array('m' => 'mixes'), 'pm.id_mixes = m.id', array(
          'name',
      ))
      ->joinLeft(array('ii' => 'invoice_items'), 'op.id_orders = ii.id_orders AND op.id_products = ii.id_parts', array(
          'price' => new Zend_Db_Expr('IFNULL(ii.price * (1 - i.additional_discount / 100) * i.rate, 0)'),
      ))
      ->joinLeft(array('i' => 'invoice'), 'i.id = ii.id_invoice', array('currency', 'rate'))
      ->joinLeft(array('c' => 'customers'), 'c.id = o.id_customers', array('customer_type' => 'type'))
      ->joinLeft(array('a' => 'addresses'), 'a.id = o.id_addresses', array())
      ->join(array('cc' => 'c_country'), 'cc.code = a.country', array())
      ->where('o.status = ?', Table_Orders::STATUS_CLOSED)
      ->where('o.id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE)
      ->where('pm.id_mixes IS NOT NULL')
      ->where('i.type = ?', Table_Invoices::TYPE_REGULAR);

      if (isset($options['from']) && !empty($options['from'])) {
        $date = new Zend_Date($options['from'], 'dd.MM.yyyy');
        $select->where('o.date_exp >= ?', $date->toString('yyyy-MM-dd'));
      }
      if (isset($options['to']) && !empty($options['to'])) {
        $date = new Zend_Date($options['to'], 'dd.MM.yyyy');
        $select->where('o.date_exp  <= ?', $date->toString('yyyy-MM-dd'));
      }

      // Volba země, bere se v potaz atribut exportního prodeje
      if (isset($options['country']) && !empty($options['country'])) {
        if ($options['country'] == 'NOT_CZ') {
          $select->where('a.country <> \'CZ\' AND (i.export = \'y\' OR i.export IS NULL)');
        }
        elseif($options['country'] == 'CZ') {
          $select->where('a.country = \'CZ\' OR (a.country <> \'CZ\' AND i.export = \'n\')');
        }
        elseif($options['country'] == 'EU') {
          $select->where('cc.region = \'eu\' OR (cc.region = \'non-eu\' AND i.export = \'n\')');
        }
        elseif ($options['country'] == 'EU_NOT_CZ') {
          $select->where('cc.region = \'eu\' AND a.country <> \'CZ\' AND (i.export = \'y\' OR i.export IS NULL)');
        }
        else {
          $select->where('a.country = ? AND (i.export = \'y\' OR i.export IS NULL)', $options['country']);
        }
      }
      if (isset($options['customer']) && !empty($options['customer'])) {
        $select->where('o.id_customers = ?', $options['customer']);
      }
      if (isset($options['flavor']) && !empty($options['flavor'])) {
        $select->where('m.id_flavor = ?', $options['flavor']);
      }
      if (isset($options['pkg']) && !empty($options['pkg'])) {
        $select->where('pm.amount = ?', $options['pkg'] / 1000);
      }
      if (isset($options['onlypaid']) && $options['onlypaid'] == 'y') {
        $select->where('o.date_payment_accept IS NOT NULL OR o.date_pay_all IS NOT NULL');
      }
      return $select;
  }

  private function reportsBatches() {

    $select = $this->select()->setIntegrityCheck(false);
    $select->from(array('op' => $this->_name), array('products' => 'id_products', 'amount_total' => 'amount', 'amount_free' => 'free_amount'));

    $select->join(array('o' => 'orders'), 'o.id=op.id_orders');
    $select->joinLeft(array('i' => 'invoice'), 'op.id_orders = i.id_orders AND i.type = "' . Table_Invoices::TYPE_REGULAR . '"', array('additional_discount'));
    $select->join(array('p' => 'parts'), 'op.id_products = p.id', array('name'));
    $select->join(array('pm' => 'parts_mixes'), 'p.id = pm.id_parts', array('amount_unit' => 'amount'));
    $select->join(array('opb' => 'orders_products_batch'), 'op.id = opb.id_orders_products', array('batch', 'amount_batch' => 'amount'));

    //mixy a jejich prvočinitele
    $select->joinLeft(array('bm' => 'batch_mixes'), 'opb.batch=bm.batch');
    $select->joinLeft(array('m' => 'macerations'), 'bm.id_macerations = m.id');
    $select->joinLeft(array('mr' => 'mixes_recipes'), 'm.id_recipes = mr.id_recipes AND bm.id_mixes = mr.id_mixes');
    $select->joinLeft(array('mi' => 'mixes'), 'bm.id_mixes = mi.id');

    $select->joinLeft(array('ii' => 'invoice_items'), 'p.id=ii.id_parts AND i.id=ii.id_invoice', array('price'));

    $select->columns(new Zend_Db_Expr('SUM(opb.amount*pm.amount) AS "amount_batch_sum"'));
    $select->columns(new Zend_Db_Expr('GROUP_CONCAT(CONCAT(m.batch, ",", mr.ratio) SEPARATOR ";") AS "mixes_batches_ratio"'));
    $select->columns(new Zend_Db_Expr('SUM(mr.ratio) AS "mixes_total_ratio"'));

    $select->where('o.id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE);

    return $select;
  }

  public function getAmountReportBatchesExpd() {
    $select = $this->reportsBatches();

    $select->where('o.status ="closed"');
    $select->group('bm.batch');

    $allbatches = $select->query()->fetchAll();

    $batch_array = array();

    foreach ($allbatches as $batch) {
      $batch_array[$batch['batch']] = $batch['amount_batch_sum'];

      // rozbití mixu na prvočinitele
      foreach (explode(';', $batch['mixes_batches_ratio']) as $batches_ratio) {
        $batch_ratio = explode(',', $batches_ratio);
        $batch_array[$batch_ratio[0]] = $batch['amount_batch_sum'] / $batch['mixes_total_ratio'] * $batch_ratio[1];
      }
    }

    return $batch_array;
  }

  public function getAmountReportBatchesNonExpd() {
    $select = $this->reportsBatches();

    $select->where('o.status != "closed"');
    $select->group('bm.batch');

    $allbatches = $select->query()->fetchAll();

    $batch_array = array();

    foreach ($allbatches as $batch) {
      $batch_array[$batch['batch']] = $batch['amount_batch_sum'];

      // rozbití mixu na prvočinitele
      foreach (explode(';', $batch['mixes_batches_ratio']) as $batches_ratio) {
        $batch_ratio = explode(',', $batches_ratio);
        $batch_array[$batch_ratio[0]] = $batch['amount_batch_sum'] / $batch['mixes_total_ratio'] * $batch_ratio[1];
      }
    }
    return $batch_array;
  }

  public function getAmountReportFlavours($status) {
    $select = $this->select()->setIntegrityCheck(false);
    $select->from(array('op' => $this->_name), array());

    $select->join(array('o' => 'orders'), 'o.id=op.id_orders', array());
    $select->join(array('opb' => 'orders_products_batch'), 'op.id = opb.id_orders_products', array());
    $select->join(array('pm' => 'parts_mixes'), 'op.id_products = pm.id_parts', array());
    $select->join(array('mr' => 'mixes_recipes'), 'pm.id_mixes=mr.id_mixes', array());

    $select->columns(array(
        'amount_sum' => new Zend_Db_Expr('SUM(opb.amount * pm.amount)'),
        'ratio' => new Zend_Db_Expr('GROUP_CONCAT(CONCAT(mr.id_recipes, ",", mr.ratio) SEPARATOR ";")'),
        'ratio_total' => new Zend_Db_Expr('SUM(mr.ratio)'),
    ));

    $select->where('o.id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE);
    $select->where('o.status = ?', $status);
    $select->group('mr.id_mixes');

    $report = array();
    foreach ($select->query()->fetchAll() as $result) {
      if (empty($result['ratio_total'])) {
        continue;
      }

      foreach (explode(';', $result['ratio']) as $ratio) {
        $ratio = explode(',', $ratio);

        if (array_key_exists($ratio[0], $report)) {
          $report[$ratio[0]] += $result['amount_sum'] / $result['ratio_total'] * $ratio[1];
        } else {
          $report[$ratio[0]] = $result['amount_sum'] / $result['ratio_total'] * $ratio[1];
        }
      }
    }

    return $report;
  }

    public function getTotalCoalSales(array $options) {
      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('op' => $this->_name), array(
          'amount',
          'total_czk' => new Zend_Db_Expr('IF(i.currency = "CZK", (op.amount - op.free_amount) * IFNULL(ii.price * (1 - i.additional_discount / 100) * i.rate, 0), 0)'),
          'total_eur' => new Zend_Db_Expr('IF(i.currency = "EUR", (op.amount - op.free_amount) * IFNULL(ii.price * (1 - i.additional_discount / 100), 0), 0)'),
          ))
        ->joinLeft(array('o' => 'orders'), 'o.id = op.id_orders', array())
        ->joinLeft(array('p' => 'parts'), 'p.id = op.id_products', array(
          'name',
          ))
        ->joinLeft(array('ii' => 'invoice_items'), 'op.id_orders = ii.id_orders AND op.id_products = ii.id_parts', array(
          'price' => new Zend_Db_Expr('IFNULL(ii.price * (1 - IFNULL(i.additional_discount / 100, 0)) * i.rate, 0)'),
        ))
        ->joinLeft(array('i' => 'invoice'), 'i.id = ii.id_invoice', array('currency', 'rate'))
        ->joinLeft(array('c' => 'customers'), 'c.id = o.id_customers', array())
        ->joinLeft(array('a' => 'addresses'), 'a.id = o.id_addresses', array())
        ->where('o.status = ?', Table_Orders::STATUS_CLOSED)
        ->where('o.id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE)
        ->where('p.id_parts_ctg = ?', Table_TobaccoWarehouse::CTG_ACCESSORIES)
        ->where('i.type = ?', Table_Invoices::TYPE_REGULAR);

      if (isset($options['onlypaid']) && $options['onlypaid'] == 'y') {
        $select->where('o.date_payment_accept IS NOT NULL OR o.date_pay_all IS NOT NULL');
      }
      if (isset($options['from']) && !empty($options['from'])) {
        $date = new Zend_Date($options['from'], 'dd.MM.yyyy');
        $select->where('o.date_request >= ?', $date->toString('yyyy-MM-dd'));
      }
      if (isset($options['to']) && !empty($options['to'])) {
        $date = new Zend_Date($options['to'], 'dd.MM.yyyy');
        $select->where('o.date_request <= ?', $date->toString('yyyy-MM-dd'));
      }
      if (isset($options['country']) && !empty($options['country'])) {
        if ($options['country'] == 'NOT_CZ') {
          $select->where('a.country <> ?', 'CZ');
        }
        else {
          $select->where('a.country = ?', $options['country']);
        }
      }
      if (isset($options['customer']) && !empty($options['customer'])) {
        $select->where('o.id_customers = ?', $options['customer']);
      }
      $xSelect = new Zend_Db_Select(Zend_Registry::get('db'));
      $xSelect->from(array('t' => $select), array(
        'pcs' => new Zend_Db_Expr('SUM(amount)'),
        'total_czk' => new Zend_Db_Expr('ROUND(SUM(total_czk), 2)'),
        'total_eur' => new Zend_Db_Expr('ROUND(SUM(total_eur), 2)'),
        'name',
        'price_avg' => new Zend_Db_Expr('ROUND(AVG(price), 2)'),
      ));
      $xSelect
        ->group('t.name')
        ->order('t.name');
      return $xSelect->query()->fetchAll();
    }

  /**
   * Report celkoveho prodeje ve skladu dýmek
   *
   * @param int $ctg Kategorie produktů
   * @param array $options asociativni pole nepovinnych parametru reportu:
   *    - from      ... pocatecni datum reportu
   *    - to        ... koncove datum reportu
   *    - country   ... kod zeme (fakturacni adresa)
   *    - customer  ... id zakaznika
   */
  public function getTotalPipesSales($ctg, array $options) {
    $select = $this->select()
      ->setIntegrityCheck(false)->from(['op' => $this->_name], [
        'pcs' => new Zend_Db_Expr('SUM(op.amount)'),
        'pcs_free' => new Zend_Db_Expr('SUM(op.free_amount)'),
        'total' => new Zend_Db_Expr('ROUND (SUM((op.amount - op.free_amount) * IFNULL(ii.price * (1 - IFNULL(i.additional_discount / 100, 0)) * i.rate, 0)), 2)'),
      ])
      ->joinLeft(['o' => 'orders'], 'o.id = op.id_orders', [])
      ->joinLeft(['p' => 'parts'], 'p.id = op.id_products', ['id', 'name'])
      ->joinLeft(['pcls' => 'parts_collections'], 'pcls.id_parts = p.id', [])
      ->join(['cls' => 'collections'], 'cls.id = pcls.id_collections', [
        'cls_id' => 'id',
        'cls_name' => 'name',
        'cls_abbr' => 'abbr',
      ])
      ->joinLeft(['ii' => 'invoice_items'], 'op.id_orders = ii.id_orders AND op.id_products = ii.id_parts', [
        'price_avg' => new Zend_Db_Expr('ROUND(AVG(IFNULL(ii.price * (1 - IFNULL(i.additional_discount / 100, 0)) * i.rate, 0)), 2)'),
      ])
      ->joinLeft(['i' => 'invoice'], 'i.id = ii.id_invoice', [])
      ->joinLeft(['c' => 'customers'], 'c.id = o.id_customers', [
        'customers_type' => new Zend_Db_Expr('IFNULL(c.type, \'B2B\')'),
      ])
      ->joinLeft(['a' => 'addresses'], 'a.id = o.id_addresses', [])
      ->join(['cc' => 'c_country'], 'cc.code = a.country', [])
      ->where('o.status = ?', Table_Orders::STATUS_CLOSED)
      ->where('o.id_wh_type = ?', 1)
      ->where('p.id_parts_ctg = ?', $ctg)
      ->where('i.type = ?', Table_Invoices::TYPE_REGULAR)
      ->group(['p.id', 'c.type'])
      ->order(array('p.id'));

    if (isset($options['from']) && !empty($options['from'])) {
      $date = new Zend_Date($options['from'], 'dd.MM.yyyy');
      $select->where('o.date_request >= ?', $date->toString('yyyy-MM-dd'));
    }
    if (isset($options['to']) && !empty($options['to'])) {
      $date = new Zend_Date($options['to'], 'dd.MM.yyyy');
      $select->where('o.date_request <= ?', $date->toString('yyyy-MM-dd'));
    }
    if (isset($options['country']) && !empty($options['country'])) {
      switch ($options['country']) {
        case 'EU':
          $select->where('cc.region = ?', 'eu');
          break;
        case 'EU_NOT_CZ':
          $select->where('cc.region = ?', 'eu');
          $select->where('a.country != ?', 'CZ');
          break;
        case 'NOT_CZ':
          $select->where('a.country != ?', 'CZ');
          break;
        default:
          $select->where('a.country = ?', $options['country']);
          break;
      }
    }
    if (isset($options['customer']) && !empty($options['customer'])) {
      $select->where('o.id_customers = ?', $options['customer']);
    }
    if ($options['type'] !== 'ALL') {
      $select->where('c.type = ? ', $options['type']);
    }
    return $select->query()->fetchAll();
  }

  /**
   * Report celkoveho prodeje ve skladu dýmek s vyloučenými kategoriemi produktů
   *
   * @param array $excl_cats Vyloučené kategorie produktů
   * @param array $options asociativni pole nepovinnych parametru reportu:
   *    - from      ... pocatecni datum reportu
   *    - to        ... koncove datum reportu
   *    - country   ... kod zeme (fakturacni adresa)
   *    - customer  ... id zakaznika
   */
  public function getTotalPipesSalesExclude(array $excl_cats, array $options) {
    $select = $this->select()
      ->setIntegrityCheck(false)
      ->from(['op' => $this->_name], [
        'pcs' => new Zend_Db_Expr('SUM(op.amount)'),
        'pcs_free' => new Zend_Db_Expr('SUM(op.free_amount)'),
        'total' => new Zend_Db_Expr('ROUND (SUM((op.amount - op.free_amount) * IFNULL(ii.price * (1 - IFNULL(i.additional_discount / 100, 0)) * i.rate, 0)), 2)'),
      ])->joinLeft(['o' => 'orders'], 'o.id = op.id_orders', [])
      ->joinLeft(['p' => 'parts'], 'p.id = op.id_products', ['id', 'name'])
      ->joinLeft(['pcls' => 'parts_collections'], 'pcls.id_parts = p.id', [])
      ->join(['cls' => 'collections'], 'cls.id = pcls.id_collections', [
        'cls_id' => 'id',
        'cls_name' => 'name',
        'cls_abbr' => 'abbr',
      ])
      ->joinLeft(['ii' => 'invoice_items'], 'op.id_orders = ii.id_orders AND op.id_products = ii.id_parts', [
        'price_avg' => new Zend_Db_Expr('ROUND(AVG(IFNULL(ii.price * (1 - IFNULL(i.additional_discount / 100, 0)) * i.rate, 0)), 2)'),
      ])
      ->joinLeft(['i' => 'invoice'], 'i.id = ii.id_invoice', [
        'rate',
        'additional_discount'
      ])
      ->joinLeft(['c' => 'customers'], 'c.id = o.id_customers', [
        'customers_type' => new Zend_Db_Expr('IFNULL(c.type, \'B2B\')'),
      ])
      ->joinLeft(['a' => 'addresses'], 'a.id = o.id_addresses', [])
      ->join(['cc' => 'c_country'], 'cc.code = a.country', [])
      ->where('o.status = ?', Table_Orders::STATUS_CLOSED)
      ->where('o.id_wh_type = ?', 1);

    foreach($excl_cats as $ctg) {
      $select->where('p.id_parts_ctg != ?', $ctg);
    }

    $select
      ->where('i.type = ?', Table_Invoices::TYPE_REGULAR)
      ->group(['p.id', 'c.type'])
      ->order(['p.id']);

    if (isset($options['from']) && !empty($options['from'])) {
      $date = new Zend_Date($options['from'], 'dd.MM.yyyy');
      $select->where('o.date_request >= ?', $date->toString('yyyy-MM-dd'));
    }
    if (isset($options['to']) && !empty($options['to'])) {
      $date = new Zend_Date($options['to'], 'dd.MM.yyyy');
      $select->where('o.date_request <= ?', $date->toString('yyyy-MM-dd'));
    }
    if (isset($options['country']) && !empty($options['country'])) {
      switch ($options['country']) {
        case 'EU':
          $select->where('cc.region = ?', 'eu');
          break;
        case 'EU_NOT_CZ':
          $select->where('cc.region = ?', 'eu');
          $select->where('a.country != ?', 'CZ');
          break;
        case 'NOT_CZ':
          $select->where('a.country != ?', 'CZ');
          break;
        default:
          $select->where('a.country = ?', $options['country']);
          break;
      }
    }
    if (isset($options['customer']) && !empty($options['customer'])) {
      $select->where('o.id_customers = ?', $options['customer']);
    }
    if (isset($options['type']) && $options['type'] !== 'ALL') {
      $select->where('c.type = ? ', $options['type']);
    }
    return $select->query()->fetchAll();
  }

  public function getProductSales($productId, $from, $to, $sum = FALSE) {
    $select = $this->select()->setIntegrityCheck(FALSE);
    if ($sum) {
      $select->from(array('op' => 'orders_products'), array(
        'amount' => new Zend_Db_Expr('SUM(op.amount)'),
      ));
    }
    else {
      $select->from(array('op' => 'orders_products'), array(
        'amount',
      ));
    }
    $select
      ->join(array('o' => 'orders'), 'op.id_orders = o.id '
        . ' AND o.status = \'' . Table_Orders::STATUS_CLOSED . '\'', array(
          'title' => 'title',
          'order_id' => 'id',
          'order_no' => 'no',
          'date_open',
          'date_exp',
      ))
      ->join(array('i' => 'invoice'), 'i.id_orders = o.id '
        . 'AND i.type = \'' . Table_Invoices::TYPE_REGULAR . '\''
        . 'AND o.date_exp >= \'' . $from . '\' AND o.date_exp < \'' . $to . '\'', array(
          'invoice_id' => 'id',
          'invoice_no' => 'no',
      ))
      ->where('op.id_products = ?', $productId)
      ->order(array('o.date_exp', 'i.no'));
    $query = $select->query(Zend_Db::FETCH_ASSOC);
    if ($sum) {
      return $query->fetchColumn(0);
    }
    else {
      return $query->fetchAll();
    }
  }

  public function getCosts($idOrder = NULL, $types = ['strategic', 'new', 'confirmed', 'open'], $wh = Table_PartsWarehouse::WH_TYPE) {
    $select = $this->select()->setIntegrityCheck(FALSE)
      ->from(['op' => 'orders_products'], ['id_orders'])
      ->join(['o' => 'orders'], 'o.id = op.id_orders', [])
      ->join(['p' => 'parts'], 'p.id = op.id_products', [])
      ->join(['pw' => 'parts_warehouse'], 'pw.id_parts = op.id_products', [])
      ->joinLeft(['opp' => 'orders_products_parts'], 'opp.id_orders_products = op.id AND opp.id_parts = op.id_products', [])
      ->where('o.id_wh_type = ?', $wh)
      ->group('op.id_orders');
    try {
      $select->columns([
        'operation_time_total' => new Zend_Db_Expr('SUM(p.operations_time * op.amount)'),
        'operation_cost_total' => new Zend_Db_Expr('SUM(p.operations_cost * op.amount)'),
        'price_total' => new Zend_Db_Expr('SUM(p.price * op.amount)'),
        'operation_time_missing' => new Zend_Db_Expr('SUM(p.operations_time * (IF (op.amount - pw.amount - IFNULL(opp.amount,0) < 0, 0, op.amount - pw.amount - IFNULL(opp.amount,0))))'),
        'operation_cost_missing' => new Zend_Db_Expr('SUM(p.operations_cost * (IF (op.amount - pw.amount - IFNULL(opp.amount,0) < 0, 0, op.amount - pw.amount - IFNULL(opp.amount,0))))'),
        'price_missing' => new Zend_Db_Expr('SUM(p.price * (IF (op.amount - pw.amount - IFNULL(opp.amount,0) < 0, 0, op.amount - pw.amount - IFNULL(opp.amount,0))))'),
        'amount_sets' => new Zend_Db_Expr('SUM(IF (p.id_parts_ctg = 23, op.amount, 0))'),
        'price_sets' => new Zend_Db_Expr('SUM(IF (p.id_parts_ctg = 23, op.amount * p.price, 0))'),
        'price_acc' => new Zend_Db_Expr('SUM(IF (p.id_parts_ctg <> 23, op.amount * p.price, 0))'),
      ]);
    }
    catch (Zend_Db_Select_Exception $e) {
      die($e->getTraceAsString());
    }
    if ($idOrder) {
      $select->where('op.id_orders = ?', $idOrder);
    }
    else {
      $select->where('o.status IN (?)', $types);
    }
    $data = [];
    if ($result = $select->query(Zend_DB::FETCH_ASSOC)) {
      while ($row = $result->fetch()) {
        $data[$row['id_orders']] = $row;
      }
    }
    return $data;
  }

  /**
   * Vrátí data o objednaných produktech v objednávkách předběžných, potvrzených a otevřených.
   *
   * @return array
   */
  public function getTotalRequested(): array {
    $select = $this->select()->setIntegrityCheck(FALSE)
      ->from(['op' => 'orders_products'], ['pcs' => new Zend_Db_Expr('SUM(op.amount)')])
      ->join(['o' => 'orders'], 'o.id = op.id_orders AND o.id_wh_type = 1', ['order_status' => 'status'])
      ->join(['p' => 'parts'], 'p.id = op.id_products', ['part_name' => 'name', 'part_id' => 'id'])
      ->where('o.status IN (?)', [Table_Orders::STATUS_NEW, Table_Orders::STATUS_CONFIRMED, Table_Orders::STATUS_OPEN])
      ->group(['p.id', 'o.status'])
      ->order('p.id');
    return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }

    /**
     * Retrieve active orders that contain a specific part.
     *
     * @param string $partId The ID of the part to search for in the orders.
     *
     * @return array An array of active orders that contain the specified part. Each order is represented as an associative array with the following keys:
     *   - id: The ID of the order.
     *   - title: The title of the order.
     *   - no: The order number.
     *   - amount: The amount of the specified part in the order.
     */
  public function retrieveActiveOrdersContainsPart(string $partId): array
  {
      $select = $this->getAdapter()->select()
          ->from(['op' => 'orders_products'], ['amount'])
          ->join(['p' => 'parts'], 'p.id = op.id_products', [])
          ->join(['o' => 'orders'], 'o.id = op.id_orders', ['id', 'title', 'no'])
          ->where('o.status not in (?)', [Table_Orders::STATUS_CLOSED, Table_Orders::STATUS_TRASH, Table_Orders::STATUS_DELETED])
          ->where('p.id = ?', $partId)
          ->order('o.id');

        return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
  }

}

