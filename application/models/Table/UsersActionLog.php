<?php
class Table_UsersActionLog extends Zend_Db_Table_Abstract {
	protected $_name = 'users_action_log';
	protected $_primary = 'id';
  
  public function getDataForReportGrid() {
    $select = $this->select();
    
    $select->orWhere('action = ?', 'email-templates/save');
    $select->orWhere('action = ?', 'tobacco-order-edit/process');
    $select->orWhere('action = ?', 'tobacco-orders/delete-item');
    $select->orWhere('action = ?', 'tobacco-orders/stamp-all');
    $select->orWhere('action = ?', 'tobacco/add-item');
    $select->orWhere('action = ?', 'tobacco/macerations-check');
    $select->orWhere('(action = ? AND params LIKE "%merge_from=%")', 'tobacco/macerations-merge');
    $select->orWhere('action = ?', 'tobacco/parts-pack-process');
    $select->orWhere('action = ?', 'tobacco/macerations-save');
    $select->orWhere('action = ?', 'tobacco/macerations-storno');
    
    return $select;
  }
  
  public function splitControllerAndAction($controlleraction) {
    $controlleraction = explode('/', $controlleraction);
    
    $output['controller'] = $controlleraction[0];
    $output['action']     = $controlleraction[1];
    
    return $output;
  }
  
  public function splitParamsToArray($params_string) {
    $params = array();
    
    if (trim($params_string)) {
      foreach (explode(', ', $params_string) as $param) {
        $par = explode('=', $param);
        $params[$par[0]] = isset($par[1]) ? $par[1] : NULL;
      }
    }
    
    return $params;
  }

  public function getLastURL($userName) {
    $lastRecors = $this->select()
      ->from($this->_name, array('action', 'params'))
      ->order('date DESC')
      ->where('username = ?', $userName)
      ->where('action <> ?', 'index/index')
      ->where('action <> ?', 'index/extend')
      ->where('action <> ?', 'login/index')
      ->limit(1)->query()->fetch();
    if (!$lastRecors || $lastRecors['action'] == 'login/clear') {
      return NULL;
    }
    list($controller, $action) = explode('/', $lastRecors['action']);
    $url = array('controller' => $controller, 'action' => $action);
    $params = explode(',', $lastRecors['params']);
    foreach ($params as $param) {
      list($key, $value) = explode('=', $param);
      if ($key && !in_array($key, array('tab'))) {
        $url[$key] = $value;
      }
    }
    $view = new Zend_View();
    return $view->url($url);
  }
}