<?php

class Table_Orders extends Zend_Db_Table_Abstract {

  protected $_name = 'orders';
  protected $_primary = 'id';
  protected $_rowClass = "Table_Row_Orders";

  //payment methods
  const PM_CASH_ON_DELIVERY = 'cash_on_delivery';
  const PM_CASH = 'cash';
  const PM_CARD = 'card';
  const PM_TRANSFER = 'transfer';
  const PM_FREE_PRESENTATION = 'free_presentation';
  const PM_FREE_CLAIM = 'free_claim';
  const PM_PAYPAL = 'paypal';
  const PM_PAYSCZ = 'payscz';
  const PM_UNKNOWN = 'unknown';

  static $paymentToString = [
    self::PM_CASH_ON_DELIVERY => "na dobírku",
    self::PM_CASH => "v hotovosti",
    self::PM_CARD => "kartou",
    self::PM_TRANSFER => "převodem",
    self::PM_FREE_PRESENTATION => "zdarma - prezentace",
    self::PM_FREE_CLAIM => "zdarma - reklamace",
    self::PM_PAYPAL => "PayPal",
    self::PM_PAYSCZ => "Pays",
    self::PM_UNKNOWN => 'n/a',
  ];

  static $paymentToStringEn = [
    self::PM_CASH_ON_DELIVERY => "cash on delivery",
    self::PM_CASH => "in cash",
    self::PM_CARD => "payment by card",
    self::PM_TRANSFER => "bank transfer",
    self::PM_FREE_PRESENTATION => "free - presentation",
    self::PM_FREE_CLAIM => "free - claim",
    self::PM_PAYPAL => "PayPal",
    self::PM_PAYSCZ => "Pays",
    self::PM_UNKNOWN => 'n/a',
  ];

  const STATUS_OFFER = 'offer';
  const STATUS_NEW = 'new';
  const STATUS_CONFIRMED = 'confirmed';
  const STATUS_STRATEGIC = 'strategic';
  const STATUS_OPEN = 'open';
  const STATUS_CLOSED = 'closed';
  const STATUS_TRASH = 'trash';
  const STATUS_DELETED = 'deleted';

  static $statusToString = [
    self::STATUS_OFFER => 'Nabídka',
    self::STATUS_NEW => 'Předběžná',
    self::STATUS_CONFIRMED => 'Potvrzená',
    self::STATUS_STRATEGIC => 'Strategická',
    self::STATUS_OPEN => 'Otevřená',
    self::STATUS_CLOSED => 'Expedovaná',
    self::STATUS_TRASH => 'V koši',
    self::STATUS_DELETED => 'Smazaná',
  ];

  const TYPE_PRODUTCS = 'products';
  const TYPE_SERVICE = 'service';
  const CURRENCY_CZK = 'CZK';
  const CURRENCY_EUR = 'EUR';

  /**
   * Vlozi objednavku do databaze
   *
   * @param <type> $no
   * @param <type> $title
   * @param <type> $deadline
   * @param <type> $desc
   * @param <type> $carrier
   * @param <type> $expPayment
   * @param <type> $expPaymentDeposit
   * @param <type> $expPaymentCur
   * @param <type> $paymentMeth
   *
   * @return int - The primary key of the row inserted.
   */
  public function addOrder($no, $title, $deadline, $desc, $carrier, $carrierPrice, $purchaser, $expPayment, $expPaymentDeposit, $expPaymentCur, $paymentMeth, $type, $wh) {
    if (empty ($deadline)) {
      $zend_deadline = new Zend_Db_Expr("NULL");
    }
    else {
      $zend_deadline = new Zend_Date($deadline, "d.MM.yyyy");
      $zend_deadline = $zend_deadline->toString("yyyy-MM-dd");
    }
    $data = [
      "no" => $no,
      "title" => $title,
      "date_request" => new Zend_Db_Expr("NOW()"),
      "date_deadline" => $zend_deadline,
      "id_orders_carriers" => $carrier,
      "purchaser" => $purchaser,
      "carrier_price" => empty($carrierPrice) ? new Zend_Db_Expr('NULL') : $carrierPrice,
      'expected_payment' => empty($expPayment) ? new Zend_Db_Expr('NULL') : $expPayment,
      'expected_payment_currency' => $expPaymentCur,
      'payment_method' => $paymentMeth,
      'type' => $type,
    ];
    $authNamespace = new Zend_Session_Namespace('Zend_Auth');
    $data['owner'] = $authNamespace->user_id;
    $data['inserted'] = new Zend_Db_Expr("NOW()");
    if ($desc != '') {
      $data['description'] = $desc;
    }

    if (!empty($expPaymentDeposit)) {
      $data['expected_payment_deposit'] = $expPaymentDeposit;

    }

    $data['proforma_no'] = $this->getNextProformaNo($wh);

    return $this->insert($data);
  }

  public function getNextProformaNo($wh = 1) {
    $p = "P";
    $y = date('y');
    $tOrders = new Table_Orders();
    $select = $tOrders->select()->setIntegrityCheck(FALSE);
    $select->from('orders', new Zend_Db_Expr("SUBSTR(IFNULL(MAX(proforma_no), '{$p}{$y}0000'), 2)"));
    $select->where('proforma_no LIKE ?', "{$p}{$y}%");
    $select->where('id_wh_type = ?', $wh);
    $no = (int) $select->query()->fetchColumn();
    return $p . ($no + 1);
  }

  /**
   *
   * @param <type> $status
   * @param array $params
   *
   * @return Zend_Db_Select
   */
  public function getOrdersNew($status = NULL, $params = []) {
    $select = $this->select()
      ->setIntegrityCheck(FALSE)
      ->from(['orders' => 'orders'], [
        'id',
        'no',
        'type',
        'title',
        'description',
        'date_request',
        'date_confirm',
        'date_open',
        'date_exp',
        'date_notification',
        'date_delete',
        'date_deadline',
        'date_deadline_internal',
        'date_pay_deposit',
        'date_pay_all',
        'date_payment_accept',
        'expected_payment',
        'paid_percentage',
        'expected_payment_currency',
        'status',
        'di',
        'id_addresses',
        'customer_email' => $this->getAdapter()->quote($this->select()
          ->setIntegrityCheck(FALSE)
          ->from('customers', ['email'])
          ->where('id = id_customers')),
        'id_customers',
        'strategic_start',
        'strategic_priority',
        'strategic_months',
        'tracking_no' => new Zend_Db_Expr('group_concat(distinct otc.code separator \', \')'),
        'payment_method',
        'carrier' => $this->getAdapter()->quote($this->select()
          ->setIntegrityCheck(FALSE)
          ->from('orders_carriers', ['name'])
          ->where('id = id_orders_carriers')),
        'carrier_id' => 'id_orders_carriers',
        'carrier_price',
        'country' => new Zend_Db_Expr("IF(id_addresses IS NULL, '', (" . $this->select()
            ->setIntegrityCheck(FALSE)
            ->from('addresses', ['country'])
            ->where('id = id_addresses') . "))"),
        'owner' => $this->getAdapter()->quote($this->select()
          ->setIntegrityCheck(FALSE)
          ->from('users', ['name'])
          ->where('id = owner')),
      ]);
    $select->where("orders.id_wh_type = ?", 1);
    if (!$params['view_private']) {
      $select->where('orders.private = "n" OR orders.owner = ?', $params['user']);
    }
    if ($status !== NULL) {
      $select->where("status = ?", $status);
    }
    if (isset($params['where'])) {
      foreach ($params['where'] as $where) {
        $select->where($where[0], $where[1]);
      }
    }

    // propojeni s fakturami u expedovanych objednavek
    if ($status == Table_Orders::STATUS_CLOSED) {
      $select->joinLeft(['i' => 'invoice'], 'orders.id = i.id_orders AND i.type = "' . Table_Invoices::TYPE_REGULAR . '"', [
          'invoice_id' => 'i.id',
          'invoice_no' => 'i.no',
        ]);
      $select->joinLeft(['v' => 'invoice_versions'], 'v.id_invoice = i.id', ['invoice_version' => new Zend_Db_Expr('MAX(v.id)')]);
    }
    if (isset($params['order'])) {
      $select->order($params['order']);
    }
    $select->group('orders.id');
    if (isset($params['service']) && $params['service'] == FALSE) {
      $select->where('orders.type <> ?', self::TYPE_SERVICE);
    }

    // trackovaci kody
    $select->joinLeft(['otc' => 'orders_tracking_codes'], 'orders.id = otc.id_orders', []);

    return $select->query()->fetchAll();
  }

  public function getOrders() {
    $select = $this->select();
    $select->from($this->_name, [
        'id',
        'no',
        'title',
        'description',
        'date_request',
        'date_confirm',
        'date_open',
        'date_exp',
        'date_deadline',
        'date_pay_deposit',
        'date_pay_all',
        'status',
        'owner',
        'id_addresses',
        'country' => new Zend_Db_Expr("IF(id_addresses IS NULL, '', (SELECT country FROM addresses WHERE id = id_addresses) )"),
        'owner' => new Zend_Db_Expr("(SELECT name FROM users WHERE id = owner)"),
      ])->order('date_deadline');
    $select->where("id_wh_type = ?", 1);
    $rows = $this->fetchAll($select);
    $data = [];
    foreach ($rows as $row) {
      $data[$row['status']][] = $row;
    }
    return $data;
  }

  /**
   * Vraci vsechny new a open objednavky bez rozdilu
   *
   * @return unknown_type
   */
  public function getAllOrders() {
    $select = $this->select();
    $select->from($this->_name)->where("status IN ('new','open')");
    $rows = $this->fetchAll($select);
    $data = [];
    foreach ($rows as $row) {
      $data[] = $row;
    }
    return $data;
  }

  public function openOrder($id) {
    $where = $this->getAdapter()->quoteInto('id = ?', $id);
    $data = [
      'status' => 'open',
      'date_open' => new Zend_Db_Expr('NOW()'),
    ];
    $this->update($data, $where);
  }

  public function confirmOrder($id) {
    $where = $this->getAdapter()->quoteInto('id = ?', $id);
    $data = [
      'status' => 'confirmed',
      'date_confirm' => new Zend_Db_Expr('CURDATE()'),
    ];
    $this->update($data, $where);
  }

  public function setNewOrder($id) {
    $where = $this->getAdapter()->quoteInto('id = ?', $id);
    $data = [
      'status' => 'new',
    ];
    $this->update($data, $where);
  }

  public function getLastId() {
    return $this->getAdapter()->lastInsertId();
  }

  private function getOrderPartsSelect($idOrder) {
    $select = $this->select();
    $select->setIntegrityCheck(FALSE)
      ->from(['op' => 'orders_products'], [
        'id_products',
        'hs_code',
        'amount',
        'free_amount',
        'c_name' => new Zend_Db_Expr('(SELECT GROUP_CONCAT(code SEPARATOR ", ") FROM collections WHERE id IN (SELECT id_collections FROM parts_collections WHERE id_parts = p.id))'),
      ])
      ->joinLeft(['o' => 'orders'], "op.id_orders = o.id", [])
      ->joinLeft(['p' => 'parts'], "op.id_products = p.id", [
          'name',
          'description',
          'end_price',
          'end_price_eur',
          'hs',
        ])
      ->joinLeft(['pc' => 'parts_collections'], "op.id_products = pc.id_parts", ['id_collections'])
      ->joinLeft(['ctg' => 'parts_ctg'], "ctg.id = p.id_parts_ctg", [
          'ctg_name' => 'ctg.name',
          'ctg_id' => 'ctg.id',
        ])
      ->joinLeft(['cu' => 'customers'], 'cu.id = o.id_customers', [
          'discount_global' => new Zend_Db_Expr('cu.discount / 100'),
          'discount_type',
        ])
      ->joinLeft(['ccd' => 'customers_parts_ctg_discount'], 'ccd.id_customers = o.id_customers AND ccd.id_parts_ctg = ctg.id', ['discount_category' => new Zend_Db_Expr('IFNULL (ccd.discount, 0) / 100')])
      ->joinLeft(['cpd' => 'customers_products_discount'], 'cpd.id_customers = o.id_customers AND cpd.id_products = op.id_products', ['discount_product' => new Zend_Db_Expr('IFNULL (cpd.discount, 0) / 100')])
      ->where("op.id_orders = ?", $idOrder)
      ->group(['op.id_products'])
      ->order(['ctg.ctg_order', 'pc.id_collections', 'p.id']);
    return $select;
  }

  public function getOrderParts($idOrder, $idCollection = NULL) {
    $order = new Orders_Order($idOrder);

    $select = $this->getOrderPartsSelect($idOrder);


    if ($idCollection !== NULL) {
      $select->where("pc.id_collections = ? OR pc.id_collections = 0", $idCollection);
    }

    $rows = $this->fetchAll($select);
    $data = [];
    foreach ($rows as $row) {
      $data[$row->ctg_name][] = [
        'id' => $row->id_products,
        'name' => $row->name,
        'description' => $row->description,
        'amount' => $row->amount,
        'free_amount' => $row->free_amount,
        'ready_amount' => $order->getReadyProductAmount($row->id_products),
        'coll' => $row->id_collections,
        'c_name' => $row->c_name,
        'ctg_id' => (int) $row->ctg_id,
      ];
    }
    return $data;
  }

  /**
   * Vrátí pole produktů z konkrétní objednávky, spadajících do určité
   * kategorie. Navazuje na fci getOrderParts.
   *
   * @param string $ctg_id ID kategorie
   * @param int $idOrder ID objednávky
   * @param int $idCollection Kolekce zboží (volitelné)
   */
  public function getOrderPartsCtg($ctgId, $idOrder, $idCollection = NULL) {
    $data = $this->getOrderParts($idOrder, $idCollection);

    $output = [];
    foreach ($data as $category) {
      foreach ($category as $d) {
        if ($d['ctg_id'] == $ctgId) {
          $output[] = $d;
        }
      }
    }

    return $output;
  }

  public function getOrderPartsDetailed($idOrder) {
    $order = new Orders_Order($idOrder);

    $select = $this->select()
      ->setIntegrityCheck(FALSE)
      ->from(['op' => 'orders_products'], ['id_products', 'amount'])
      ->joinLeft(['p' => 'parts'], 'p.id = op.id_products', [
        'name',
        'id_parts_ctg',
      ])
      ->joinLeft(['pc' => 'parts_ctg'], 'p.id_parts_ctg=pc.id', ['ctg_name' => 'name'])
      ->joinLeft(['opb' => 'orders_products_batch'], 'op.id=opb.id_orders_products', ['batches' => new Zend_Db_Expr('GROUP_CONCAT(CONCAT(opb.batch, ":", opb.amount) SEPARATOR ",")')])
      ->where('op.id_orders = ?', $idOrder)
      ->group('op.id_products')
      ->order(['p.id_parts_ctg', 'op.id_products']);

    $rows = $this->fetchAll($select);
    $data = [];


    foreach ($rows as $row) {
      $data[$row->ctg_name][] = [
        'id' => $row->id_products,
        'name' => $row->name,
        'amount' => $row->amount,
        'ready_amount' => $order->getReadyProductAmount($row->id_products),
        'batches' => $row->batches,
      ];
    }
    return $data;
  }

  public function getSortedOrderParts($idOrder, $idCollection = NULL) {
    $select = $this->getOrderPartsSelect($idOrder);
    return $this->fetchAll($select);
  }

  /**
   * Vraci pocet dymek na objednavce ( grupovani dle kolekce )
   */
  public function getPipeSummary($idOrder) {
    $select = $this->select();
    $select->setIntegrityCheck(FALSE)
      ->from(['op' => 'orders_products'], ['amount' => new Zend_Db_Expr('sum(amount)')])
      ->joinLeft(['p' => 'parts'], "op.id_products = p.id", [])
      ->joinLeft(['pc' => 'parts_collections'], "pc.id_parts = p.id")
      ->joinLeft(['c' => 'collections'], "pc.id_collections = c.id", ['c_name' => 'name'])
      ->where("op.id_orders = ?", $idOrder)
      ->where('id_parts_ctg = 23')
      ->group('pc.id_collections', 'p.id')
      ->order(['pc.id_collections', 'p.id']);

    return $this->fetchAll($select);
  }

  //////////////   db pristup   /////////////////////


  /**
   * vraci soucasti vsech produktu zadanych v objednavce - sumy poctu
   *
   * @param $id_array
   *
   * @return unknown_type
   */
  public function getAllOrderPartsL0($id_array) {

    $select = $this->select();
    $select->setIntegrityCheck(FALSE)
      ->from(['o' => 'orders'], [])
      ->joinLeft(['op' => 'orders_products'], "o.id = op.id_orders", [
        'product' => 'op.id_products',
        'product_amount' => 'op.amount',
      ])
      ->joinLeft(['pp' => 'products_parts'], "op.id_products = pp.id_products", [
        'part' => 'pp.id_parts',
        'part_amount' => 'sum(op.amount * pp.amount)',
      ])
      ->joinLeft(['p' => 'parts'], "pp.id_parts = p.id", ['part_name' => 'p.name'])
      ->where("o.id IN (?)", $id_array)
      ->group('pp.id_parts');
    die($select);
    return $select;
  }

  /**
   * vraci id vsech produktu a k nim prislusnych soucasti pro zapis do tab.
   * orders_product_parts (rezervace)
   *
   * @param $id_array
   *
   * @return unknown_type
   */
  public function getAllOrderProductParts($id_array) {
    $select = $this->select();
    $select->setIntegrityCheck(FALSE);

    $select = $this->_db->select();
    $select->from(['op' => 'orders_products'], [
        "part_amount" => 'amount',
      ])->joinLeft(['p' => 'parts'], "p.id = op.id_products", [
        "part" => "id",
        "product" => "id",
        "part_name" => "name",
      ])->where("op.id_orders IN (?)", $id_array);

    return $select;
  }


  public function getOrderPartReportOld($id_array) {

    $sub_select = $this->select();
    $sub_select->setIntegrityCheck(FALSE);
    $sub_select->from(['o' => 'orders'], [])
      ->joinLeft(['op' => 'orders_products'], "o.id = op.id_orders", [])
      ->joinLeft(['p' => 'parts'], "p.id = op.id_products", [
          "name",
          "id_parts_ctg",
          "id_collections",
          "id",
          "extra_amount" => 'SUM(IF(p.id_products_types = 3, op.amount, 0))',
          //SUM(IF(p.id_products_types = 3 OR p.id_products_types = 2, op.amount * ps.amount, 0))
          "extra_sets_amount" => 'SUM(IF(p.id_products_types = 2, op.amount, 0))',
          "sets_amount" => 'SUM(IF(p.id_products_types = 1, op.amount, 0))',
          "total_amount" => "SUM(op.amount)",
        ])
      ->joinLeft(['pctg' => 'parts_ctg'], "pctg.id = p.id_parts_ctg", ["ctg_name" => "name"])
      ->where("o.id IN (?)", $id_array)
      ->group("p.id");

    return $this->fetchAll($sub_select);
  }

  /**
   * Pocty objednanych soucasti pro objednavky dane ID
   *
   * @param $idOrders
   *
   * @return \Zend_Db_Table_Rowset_Abstract
   */
  public function getOrderPartReport($orderBy, array $idOrders, $idColl = 'null', $idCtg = 'null') {

    $subSelectOrders = $this->select();
    $subSelectOrders->setIntegrityCheck(FALSE);
    $subSelectOrders->from(['op' => 'orders_products'], ['prod_amount =>'])
      ->where("id_orders IN (?)", $idOrders)
      ->group("id_products");

    $subSelect = $this->select();
    $subSelect->setIntegrityCheck(FALSE);
    $subSelect->from(['p' => 'parts'], [
        "name",
        "id_parts_ctg",
        "id",
        "product",
        "extra_amount" => 'IFNULL(SUM(IF(p.id_parts_ctg != 23, op.amount, 0)),0)',
        "sets_amount" => 'IFNULL(SUM(IF(p.id_parts_ctg = 23, op.amount, 0)),0)',
      ])
      ->join(['pw' => 'parts_warehouse'], "p.id = pw.id_parts", ['wh_amount' => 'amount'])
      ->joinLeft(['op' => 'orders_products'], "p.id = op.id_products AND op.id_orders IN (" . $this->getAdapter()
          ->quote($idOrders) . ")", [])
      ->joinLeft(['pctg' => 'parts_ctg'], "pctg.id = p.id_parts_ctg", [])
      ->joinLeft(['parentCtg' => 'parts_ctg'], "pctg.parent_ctg = parentCtg.id", ["ctg_name" => new Zend_Db_Expr("IFNULL(CONCAT(parentCtg.name,' - ',pctg.name),pctg.name)")])
      ->joinLeft(['pp' => 'producers_parts'], "pp.id_parts = p.id", ['id_producers'])
      ->joinLeft(['prod' => 'producers'], "pp.id_producers = prod.id", ['producer_name' => 'name'])
      ->joinLeft(['prod_ctg' => 'producers_ctg'], "prod.id_producers_ctg = prod_ctg.id", ["prod_ctg_name" => "prod_ctg.name"])
      ->group("p.id");

    if ($orderBy == Orders_PartsReport::ORDER_PRODUCERS) {
      $subSelect->order(["prod.id", "p.id"]);
    }
    else {
      $subSelect->order([
          "IFNULL(parentCtg.name, pctg.name)",
          "pctg.name",
          "p.id",
        ]);
    }


    $select = $this->select();
    $select->setIntegrityCheck(FALSE);
    $select->from(['report' => $subSelect], [
        'report.*',
        "reservation" => new Zend_Db_Expr("IF(EXISTS(SELECT * FROM orders_products_parts AS opp WHERE opp.id_parts = report.id), 1, 0)"),
        //priznak existence rezervace
        "extern" => new Zend_Db_Expr("IF(EXISTS(SELECT * FROM parts_warehouse_extern AS pwe WHERE pwe.id_parts = report.id), 1, 0)"),
        //priznak existence soucasti v externim skladu
        "production" => new Zend_Db_Expr("IF(EXISTS(SELECT * FROM w_productions_current AS wp WHERE wp.id_parts = report.id AND amount_remain > 0), 1, 0)")
        //priznak existence vyroby
      ]);

    if ($idCtg != 'null') {
      $select->where("id_parts_ctg = ?", $idCtg);
    }

    if ($idColl != 'null') {
      $select->where("EXISTS(SELECT * FROM parts_collections AS pc WHERE pc.id_parts = report.id AND pc.id_collections = ?)", $idColl);
    }


    return $this->fetchAll($select);
  }

  /**
   * Pocita pocet vsech podsoucasti soucasti dane id
   *
   * @param string $idPart id soucasti napr. 'AC01'
   * @param array $amounts mnozstvi soucasti pro ktere se pocita stav napr.
   *   array('sets' => 1, 'extra' => 1, 'warehouse' => 1)
   * @param array $orderAmounts pole s pocty objednanych soucasti napr.
   *   array('AC01' => array('sets' => 1, 'extra' => 2))
   * @param int $depth hloubka zanoreni
   *
   * @return array order_amounts upravene pole s pocty objednanych soucasti
   */
  public function getPartAmounts($idPart, array $amounts, array $reservations, array $orderAmounts, $depth = 1) {
    if ($depth > 10) {
      throw new Exception ("Algoritmus pocitani poctu soucasti dosahl 10. stupne zanoreni. Soucast: $idPart");
    }

    if (!isset($orderAmounts[$idPart])) {
      $orderAmounts[$idPart]['sets'] = 0;
      $orderAmounts[$idPart]['extra'] = 0;

      $tWarehouse = new Table_PartsWarehouse();
      $orderAmounts[$idPart]['warehouse'] = $tWarehouse->getAmount($idPart);
    }

    if (!isset($reservations[$idPart])) {
      $reservations[$idPart] = 0;
    }

    $orderTotalAmount = $orderAmounts[$idPart]['sets'] + $orderAmounts[$idPart]['extra'];// + $orderAmounts[$idPart]['extra_sets'];
    $actualPartAmount = $amounts['sets'] + $amounts['extra'];// + $amounts['extra_sets'];
    $whAmount = $orderAmounts[$idPart]['warehouse'] + $reservations[$idPart]; //pocet soucasti skladem se navysuje o rezervace

    //zvyseni celkoveho poctu o aktualni
    $orderAmounts[$idPart]['sets'] += $amounts['sets'];
    $orderAmounts[$idPart]['extra'] += $amounts['extra'];

    if ($actualPartAmount > 0 && ($orderTotalAmount + $actualPartAmount) > $whAmount) {

      if ($orderTotalAmount >= $whAmount) {
        $actualMissingSets = $amounts['sets'];
        $actualMissingExtra = $amounts['extra'];
      }
      else {
        //sety
        if (($whAmount - $orderTotalAmount) >= $amounts['sets']) {
          $actualMissingSets = 0;
        }
        else {
          $actualMissingSets = ($orderTotalAmount + $amounts['sets']) - $whAmount;
          if ($actualMissingSets > $amounts['sets']) {
            $actualMissingSets = $amounts['sets'];
          }
        }

        //extra acc.
        if (($whAmount - ($orderTotalAmount + $amounts['sets'])) >= $amounts['extra']) {
          $actualMissingExtra = 0;
        }
        else {
          $actualMissingExtra = ($orderTotalAmount + $amounts['sets'] + $amounts['extra']) - $whAmount;
          if ($actualMissingExtra > $amounts['extra']) {
            $actualMissingExtra = $amounts['extra'];
          }
        }
      }

      $table_parts = new Table_Parts();
      $subParts = $table_parts->getSubpartsById($idPart);

      if (count($subParts) > 0) { //pokud je soucast slozena
        foreach ($subParts as $subpart) {
          $actAmounts = [
            'sets' => ($actualMissingSets * $subpart['amount']),
            'extra' => ($actualMissingExtra * $subpart['amount']),
          ];
          $orderAmounts = $this->getPartAmounts($subpart['id_parts'], $actAmounts, $reservations, $orderAmounts, $depth + 1);
        }
      }

    }
    return $orderAmounts;
  }

  public function getCollectionsReport($p_queryStatus = '', $p_queryColl = FALSE, $p_dateFrom, $p_dateTo = FALSE, $p_customerId = FALSE) {

    // priprava parametru
    $queryStatus = "'strategic', 'offer', 'new', 'confirmed', 'open', 'closed'";
    if ($p_queryStatus != 'all') {
      $queryStatus = Zend_Db_Table::getDefaultAdapter()->quote($p_queryStatus);
    }

    $queryColl = '';
    if (is_array($p_queryColl) && count($p_queryColl) > 0) {
      $colls = implode(',', $p_queryColl);
      $queryColl = ' AND pc.id_collections IN (' . $colls . ')';
    }

    $dateFrom = '';
    if ($p_dateFrom) {
      $zCloseDate = new Zend_Date($p_dateFrom, 'dd.MM.yyyy');
      $dateFrom = ' AND date_exp >= "' . $zCloseDate->toString('yyyy-MM-dd') . '"';
    }

    $dateTo = '';
    if ($p_dateTo) {
      $zCloseDate = new Zend_Date($p_dateTo, 'dd.MM.yyyy');
      $dateTo = ' AND date_exp <= "' . $zCloseDate->toString('yyyy-MM-dd') . '"';
    }

    // databazovy dotaz

    $subSelect1 = $this->getAdapter()->select();
    $subSelect1->from('orders_products', [
      'id_orders',
      'id_products',
      'amount',
    ]);

    $subSelect2 = $this->getAdapter()->select();
    $subSelect2->from('orders_products_history', [
      'id_orders',
      'id_products',
      'amount',
    ]);

    $subSelect3 = $this->getAdapter()->select();
    $subSelect3->from('orders', ['id'])
      ->where("status IN ($queryStatus) $dateFrom $dateTo");

    $select = $this->getAdapter()->select();
    $select->from(['op' => new Zend_Db_Expr('(' . $subSelect1 . ' UNION ' . $subSelect2 . ')')], ['amount' => 'IFNULL(SUM(op.amount),0)'])
      ->join(['p' => 'parts'], 'p.id = op.id_products', [])
      ->joinLeft(['pc' => 'parts_collections'], 'p.id = pc.id_parts', [])
      ->joinLeft(['c' => 'collections'], 'pc.id_collections = c.id', [
        'id',
        'name',
      ])
      ->joinLeft(['o' => 'orders'], 'op.id_orders = o.id', [])
      ->joinLeft(['cu' => 'customers'], 'o.id_customers = cu.id', ['type'])
      ->where("id_orders IN ($subSelect3) AND p.id_parts_ctg = 23 $queryColl")
      ->group(['c.id', 'cu.type']);
    $db = $this->getAdapter();

    if ($p_customerId) {
      $select->where("cu.id = ?", $p_customerId);
    }
    $rows = $db->fetchAll($select);

    // sestaveni reportu
    $report = [
      'total' => 0,
      'b2b' => 0,
      'b2c' => 0,
      'cols' => [],
    ];
    foreach ($rows as $row) {
      if (!isset($report['cols'][$row['id']])) {
        $report['cols'][$row['id']] = [
          'name' => 'n/a',
          'total' => 0,
          'b2b' => 0,
          'b2c' => 0,
        ];
      }
      $report['total'] += $row['amount'];
      $report['cols'][$row['id']]['total'] += $row['amount'];
      $report['cols'][$row['id']]['name'] = $row['name'];
      if ($row['type'] == 'B2B') {
        $report['b2b'] += $row['amount'];
        $report['cols'][$row['id']]['b2b'] += $row['amount'];
      }
      else {
        $report['b2c'] += $row['amount'];
        $report['cols'][$row['id']]['b2c'] += $row['amount'];
      }
    }
    return $report;
  }

  public function getPartsReport($p_queryStatus, $p_queryColl, $p_dateFrom, $p_dateTo, $p_customerId) {

    // priprava parametru
    $queryStatus = "'strategic', 'offer', 'new', 'confirmed', 'open', 'closed'";
    if ($p_queryStatus != 'all') {
      $queryStatus = Zend_Db_Table::getDefaultAdapter()->quote($p_queryStatus);
    }

    $queryColl = '';
    if (is_array($p_queryColl) && count($p_queryColl) > 0) {
      $colls = implode(',', $p_queryColl);
      $queryColl = ' AND pc.id_collections IN (' . $colls . ')';
    }

    $dateFrom = '';
    if ($p_dateFrom) {
      $zCloseDate = new Zend_Date($p_dateFrom, 'dd.MM.yyyy');
      $dateFrom = ' AND date_exp >= "' . $zCloseDate->toString('yyyy-MM-dd') . '"';
    }

    $dateTo = '';
    if ($p_dateTo) {
      $zCloseDate = new Zend_Date($p_dateTo, 'dd.MM.yyyy');
      $dateTo = ' AND date_exp <= "' . $zCloseDate->toString('yyyy-MM-dd') . '"';
    }

    // databazovy dotaz

    $subSelect1 = $this->getAdapter()->select();
    $subSelect1->from('orders_products', [
      'id_orders',
      'id_products',
      'amount',
    ]);

    $subSelect2 = $this->getAdapter()->select();
    $subSelect2->from('orders_products_history', [
      'id_orders',
      'id_products',
      'amount',
    ]);

    $subSelect3 = $this->getAdapter()->select();
    $subSelect3 = $this->getAdapter()->select();
    $subSelect3->from('orders', ['id'])
      ->where("status IN ($queryStatus) $dateFrom $dateTo");

    $select = $this->getAdapter()->select();
    $select->from(['op' => new Zend_Db_Expr("($subSelect1 UNION $subSelect2)")], ['amount' => 'IFNULL(SUM(op.amount), 0)'])
      ->join(['p' => 'parts'], 'op.id_products = p.id', ['id', 'name'])
      ->joinLeft(['pc' => 'parts_collections'], 'p.id = pc.id_parts', [])
      ->joinLeft(['c' => 'collections'], 'pc.id_collections = c.id', [])
      ->joinLeft(['o' => 'orders'], 'op.id_orders = o.id', [])
      ->joinLeft(['cu' => 'customers'], 'o.id_customers = cu.id', ['type'])
      ->where("id_orders IN ($subSelect3) AND p.id_parts_ctg = 23 $queryColl")
      ->group(['p.id', 'cu.type']);
    $db = $this->getAdapter();

    if ($p_customerId) {
      $select->where("cu.id = ?", $p_customerId);
    }
    $rows = $db->fetchAll($select);

    // sestaveni reportu
    $report = [
      'total' => 0,
      'b2b' => 0,
      'b2c' => 0,
      'parts' => [],
    ];
    foreach ($rows as $row) {
      if (!isset($report['parts'][$row['id']])) {
        $report['parts'][$row['id']] = [
          'name' => 'n/a',
          'total' => 0,
          'b2b' => 0,
          'b2c' => 0,
        ];
      }
      $report['total'] += $row['amount'];
      $report['parts'][$row['id']]['total'] += $row['amount'];
      $report['parts'][$row['id']]['name'] = $row['name'];
      if ($row['type'] == 'B2B') {
        $report['b2b'] += $row['amount'];
        $report['parts'][$row['id']]['b2b'] += $row['amount'];
      }
      else {
        $report['b2c'] += $row['amount'];
        $report['parts'][$row['id']]['b2c'] += $row['amount'];
      }
    }
    return $report;
  }

  /**
   * celkovy soucet objednanych setu dymek
   */
  public function getAllSetsCounts() {
    $select = $this->_getSetsCountsSelect()
      ->where('id_orders IN ?', $this->select()
        ->from('orders', ['id'])
        ->where('status IN (?)', [
          self::STATUS_NEW,
          self::STATUS_OPEN,
          self::STATUS_CONFIRMED,
        ]))
      ->group('pc.id_collections');
    return $select->query()->fetchAll();
  }

  /**
   * celkovy soucet objednanych dymek
   */
  public function getAllPipesCounts() {
    $select = $this->_getSetsCountsSelect()
      ->where('id_orders IN ?', $this->select()
        ->from('orders', ['id'])
        ->where('status IN (?)', [
          self::STATUS_NEW,
          self::STATUS_OPEN,
          self::STATUS_CONFIRMED,
        ]))
      ->group('p.id');
    return $select->query()->fetchAll();
  }

  /**
   * celkovy soucet objednanych setu dymek v kosi
   */
  public function getAllSetsTrashCounts() {
    $select = $this->_getSetsCountsSelect()
      ->where('id_orders IN ?', $this->select()
        ->from('orders', ['id'])
        ->where('status = ?', self::STATUS_TRASH))
      ->group('pc.id_collections');
    return $select->query()->fetchAll();
  }

  private function _getSetsCountsSelect() {
    return $this->select()
      ->setIntegrityCheck(FALSE)
      ->from(['op' => 'orders_products'], [
        'amount' => new Zend_Db_Expr('IFNULL(SUM(op.amount), 0)'),
      ])
      ->join(['p' => 'parts'], 'op.id_products = p.id', ['p.id', 'p.name'])
      ->joinLeft(['pc' => 'parts_collections'], 'pc.id_parts = op.id_products', ['pc.id_collections'])
      ->joinLeft(['c' => 'collections'], 'pc.id_collections = c.id', ['c.name'])
      ->where('p.id_parts_ctg = ?', Table_PartsCtg::TYPE_SETS);
  }


  public function getNextNo($wh = NULL) {
    $db = $this->getAdapter();

    $select = $db->select();
    $select->from('orders', [new Zend_Db_Expr('MAX(id)')]);
    if ($wh) {
      $select->where('id_wh_type = ?', $wh);
    }
    $last_id = $select->query()->fetchColumn();

    $select = $db->select();
    $select->from('orders', ['no'])->where('id = ?', $last_id);
    $no = $select->query()->fetchColumn();

    $inc = 0;
    $fetch = TRUE;
    while ($fetch) {
      $inc++;
      $select = $db->select();
      $select->from('orders', ['no'])->where('no = ?', $no + $inc);
      if ($wh) {
        $select->where('id_wh_type = ?', $wh);
      }
      $fetch = $select->query()->fetchColumn();
    }
    return $no + $inc;
  }

  public function getPartsCollections($status = NULL, $customer = NULL) {
    $select = $this->select()
      ->from(['o' => $this->_name])
      ->setIntegrityCheck(FALSE)
      ->joinLeft(['op' => 'orders_products'], 'o.id = op.id_orders')
      ->joinLeft(['p' => 'parts'], 'p.id = op.id_products')
      ->joinLeft(['pc' => 'parts_collections'], 'pc.id_parts = p.id')
      ->joinLeft(['c' => 'collections'], 'c.id = pc.id_collections');

    if (!is_null($status)) {
      $select->where('o.status = ?', $status);
    }

    if (!is_null($customer)) {
      $select->where('o.id_customers = ?', $customer);
    }

    return $select;
  }

  public function getCollectionAmountSums($customer, $ctg) {
    $select = $this->getPartsCollections(self::STATUS_CLOSED, $customer)
      ->columns([
        'sum_total' => 'SUM(op.amount)',
        'sum_free' => 'SUM(op.free_amount)',
      ])
      ->where('p.id_parts_ctg = ?', $ctg)
      ->group('c.abbr')
      ->query()
      ->fetchAll();

    $tCollections = new Table_Collections();
    $collts = $tCollections->getCollectionsSelect(TRUE)->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    $collections = [];
    foreach ($collts as $collt) {
      $collections[$collt['code']] = [
        'total' => 0,
        'free' => 0,
        'transfer' => 0,
      ];
    }

    foreach ($select as $value) {
      if (!is_null($value['abbr'])) {
        $collections[$value['abbr']]['total'] = (int) $value['sum_total'];
        $collections[$value['abbr']]['free'] = (int) $value['sum_free'];
        $collections[$value['abbr']]['transfer'] = (int) $value['sum_transfer'];

      }
    }

    return $collections;
  }

  function getYears($status = 'closed', $id_wh_type = 1) {
    $years = [];
    $select = $this->select()
      ->setIntegrityCheck(TRUE)
      ->distinct(TRUE)
      ->from($this->_name, ['year' => new Zend_Db_Expr('YEAR(date_request)')])
      ->where('id_wh_type = ?', $id_wh_type)
      ->order('date_request');
    if ($status) {
      $select->where('status = ?', $status);
    }
    $result = $select->query();
    while ($year = $result->fetchColumn()) {
      $years[] = $year;
    }
    return $years;
  }

  public function getProformSelect($params) {
    $select = $this->select()->setIntegrityCheck(FALSE);
    $select->from(['o' => 'orders'])
      ->joinLeft(['a' => 'addresses'], 'a.id = o.id_addresses', ['country' => 'a.country'])
      ->joinLeft(['i' => 'invoice'], 'o.id = i.id_orders AND i.type = "' . Table_Invoices::TYPE_PROFORM . '"', ['invoice' => 'id'])
      ->where(new Zend_Db_Expr('proforma_no IS NOT NULL'))
      ->where('status != ?', Table_Orders::STATUS_DELETED)
      ->where('status != ?', Table_Orders::STATUS_TRASH)
      ->order('proforma_no DESC');

    if (isset($params['wh'])) {
      $select->where('o.id_wh_type = ?', $params['wh']);
    }
    return $select;
  }
}
