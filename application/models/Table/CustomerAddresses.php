<?php

/**
 * @author Kryštof Košut
 */
class Table_CustomerAddresses extends Zend_Db_Table_Abstract {

  protected $_name = 'customers_addresses';
  protected $_primary = 'id';

  /**
   * Return array of customer addresses
   *
   * @param integer $userID
   * @return array (adresy uzivatele)
   */
  public function getAddress(Customers_Customer $customer, $likeObjects = FALSE, $deleted = FALSE){

    $select = $this->select()->setIntegrityCheck(false);
    $select->from(array('ca' => $this->_name), array());
    $select->joinLeft(array('a' => 'addresses') , 'a.id = ca.id_address');
    $select->where('id_customers = ?', $customer->getId());

    if (!$deleted) {
      $select->where('deleted = ?', 0);
    }

    $rows = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
    if (!$likeObjects) {
      return $rows;
    }

    $objects = array();
    if ($rows) {
      foreach ($rows as $row) {
        $addressObj = new Addresses_Address($row);
        $objects[$row->id] = $addressObj;
      }
    }
    return $objects;
  }

  /**
   * Add row to table customers_addresses
   *
   * @param integer $userID
   * @param integer $addressID
   */
  public function addAddress($customerId, $addressId){
    //Code
    $data = array(
        'id_address' => $addressId,
        'id_customers' => $customerId,
    );

   $this->insert($data);
  }

  /**
   * Get full address from database table addresses (merged with customers)
   *
   * @param type $customerId
   * @param type $addressId
   * @return array(fetched)
   */
  public function getFullAddress($customerId, $addressId){

       $row = $this->_db->select()
               ->from(array('ca' => 'customers_addresses'))
               ->join(array('aa' => 'addresses'), 'ca.id_address = aa.id')
               ->join(array('cu' => 'customers'), 'ca.id_customers = cu.id')
               ->where('ca.id_customers = ?', $customerId)
               ->where('ca.id_address = ?', $addressId)
               ->query()
               ->fetch();


       $company = $this->_db->select()
               ->from(array('ad' => 'addresses'),array('company'))
               ->where('ad.id = ?',$addressId)
               ->query()
               ->fetch();

       $row = array_merge($row,$company);

    return $row;
  }
}
