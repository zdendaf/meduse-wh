<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Table_EmployeesVacationClaims extends Zend_Db_Table_Abstract
  {
    protected $_name = 'employees_vacation_claims';
    protected $_primary = array('id_employees', 'year');
  }

