<?php
class Table_Emails extends Zend_Db_Table_Abstract {

	protected $_name = 'emails';

	protected $_id = 'id';

	protected $_from = 'warehouse@meduse-experience.com';

	protected $_from_name = 'MEDUSE WAREHOUSE';

	const EMAIL_ORDER_CONFIRM = 1; // email odeslany na sklad

	const EMAIL_PRODUCTION_CREATION = 2; // email odeslany na sklad + vyroba

	const EMAIL_NEW_LOCATION = 3; // email odeslany na sklad + vyroba

	const EMAIL_ORDER_EXPEDITION_NOTICE = 4; // email upozorneni

	const EMAIL_PRE_ORDER_14_DAYS_NOTICE = 5; // 14 dni upozorneni - predbezne objednavky

	const EMAIL_PRE_ORDER_21_DAYS_NOTICE = 6; // 21 dni upozorneni - predbezne objednavky

	const EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE = 7; // upozorneni o expedici zakaznikovi

  const EMAIL_BUG_REPORT = 8; // hlaseni chyby

	const EMAIL_MISSING_PARTS_DAILY_SUMMARY = 9; // denni prehled chybejicich soucasti

	const EMAIL_ORDER_EXPEDITION_NOTICE_EX_WORKS = 10; // email upozorneni - EX WORKS

	const EMAIL_ISSUE_COMMENT = 11; // upozornění na nový komentář

	const EMAIL_ISSUE_CLOSE = 12; // oznámění při ukončení problému

	const EMAIL_PRODUCTION_DELAY = 13; // oznameni pri prekroceni data dodani vyroby

  const EMAIL_PRE_ORDER_FULL_NOTICE = 14; // predbezne objednavky 14/21 dny - jeden mail

  const EMAIL_ORDER_NEW_CUSTOMER_NOTICE = 15; // upozorneni o vytvoreni zakazky zakaznikovi

  const EMAIL_ORDER_OPEN_CUSTOMER_NOTICE = 16; // upozorneni o otevreni zakazky zakaznikovi

  const EMAIL_NOTICE_CRITICAL_AMOUNT = 17; //cron na limitni mnozstvi

  const EMAIL_NOTICE_ZERO_COSTS = 18; // upozorneni vedouciho skladnika na nulove naklady

  const EMAIL_ORDER_OPEN_NOTICE = 19; //upozorneni na otevreni zakazky

  const EMAIL_TOBACCO_ORDER_OPEN_NOTICE = 20; //upozorneni na otevreni zakazky

  const EMAIL_TOBACCO_ORDER_IS_READY_NOTICE = 21; // upozorneni o moznosti expedice

  const EMAIL_TOBACCO_ORDER_EXPEDITION_NOTICE = 22; // email upozorneni

  const EMAIL_VERIFY_WORKSHEET_NOTICE = 23; // info o spatne vyplnenem worksheetu

  const EMAIL_END_EMP_CONTRACT_NOTICE = 24; // info o blizicim se konci pracovni smlouvy

  const EMAIL_BIRTHDAY_NOTICE = 25; // info o narozeninach

  const EMAIL_ORDER_PREPARE = 26; // info o priprave

  const EMAIL_INVOICE_CHANGE = 27; // info o zmene faktury

  const EMAIL_PAYMENT_NOTICE_ADD = 28; // info o nove platbe

  const EMAIL_PAYMENT_NOTICE_CHANGE = 29; // info o zmene platby

  const EMAIL_PAYMENT_NOTICE_REMOVE = 30; // info o odstraneni platby

  const EMAIL_NOTICE_UNPAID_INVOICES = 31; // info o neuhrazenych fakturach po splatnosti

  const EMAIL_NOTICE_PARTS_EXPORT = 32; // info o vygenerovanem exportu skladu

  const EMAIL_NOTICE_ORDER_CHANGE = 33; // info o zmene objednavky

  const EMAIL_INVOICE_IN_CASH_NOTICE = 34; // info o vystavene fakture v hotovosti

  const EMAIL_NOTICE_ORDER_PACKAGING = 35; // info o zmene baleni objednavky

  const EMAIL_TOBACCO_PACKING_REPORT = 36; // info o hromadnem nabaleni

  const EMAIL_NOTICE_PARTS_EXPORT_MEDITE = 37; // info o vygenerovanem exportu skladu

  const EMAIL_ORDER_DEADLINE_NOTICE = 38; // info o zmene deadlinu objednavky

  static $number_to_db_type = array(
    self::EMAIL_ORDER_NEW_CUSTOMER_NOTICE => 'orderNewNoticeCustomerEmail',
    self::EMAIL_ORDER_OPEN_CUSTOMER_NOTICE => 'orderOpenNoticeCustomerEmail',
    self::EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE => 'orderExpeditionNoticeCustomerEmail',
    self::EMAIL_NOTICE_CRITICAL_AMOUNT => 'noticeCriticalAmountEmail',
    self::EMAIL_NOTICE_ZERO_COSTS => 'noticeZeroCostsEmail',
    self::EMAIL_VERIFY_WORKSHEET_NOTICE => 'noticeVerifyWorksheet',
    self::EMAIL_END_EMP_CONTRACT_NOTICE => 'noticeEndOfEmployeeContract',
    self::EMAIL_BIRTHDAY_NOTICE => 'noticeBirthday',
    self::EMAIL_ORDER_PREPARE => 'infoOrderPrepare',
    self::EMAIL_INVOICE_CHANGE => 'infoInvoiceChange',
    self::EMAIL_PAYMENT_NOTICE_ADD => 'noticePaymentAdd',
    self::EMAIL_PAYMENT_NOTICE_CHANGE => 'noticePaymentChange',
    self::EMAIL_PAYMENT_NOTICE_REMOVE => 'noticePaymentRemove',
    self::EMAIL_NOTICE_UNPAID_INVOICES => 'noticeUnpaidInvoices',
    self::EMAIL_NOTICE_PARTS_EXPORT => 'noticePartsExport',
    self::EMAIL_NOTICE_ORDER_CHANGE => 'infoOrderChange',
    self::EMAIL_INVOICE_IN_CASH_NOTICE => 'noticeInvoiceInCash',
    self::EMAIL_NOTICE_ORDER_PACKAGING => 'noticeOrderPackaging',
    self::EMAIL_TOBACCO_PACKING_REPORT => 'tobaccoPackingReport',
    self::EMAIL_NOTICE_PARTS_EXPORT_MEDITE => 'noticePartsExportMedite',
    self::EMAIL_ORDER_DEADLINE_NOTICE => 'noticeOrderDeadline',
  );

  static $number_to_readable = array(
    self::EMAIL_ORDER_NEW_CUSTOMER_NOTICE => 'Zákazník: nová objednávka',
    self::EMAIL_ORDER_OPEN_CUSTOMER_NOTICE => 'Zákazník: otevřená objednávka',
    self::EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE => 'Zákazník: expedovaná objednávka',
    self::EMAIL_END_EMP_CONTRACT_NOTICE => 'Blíží se konec pracovní smlouvy',
    self::EMAIL_BIRTHDAY_NOTICE => 'Narozeniny pracovníka',
    self::EMAIL_ORDER_PREPARE => 'Příprava objednávky',
    self::EMAIL_INVOICE_CHANGE => 'Změna evidované faktury',
    self::EMAIL_PAYMENT_NOTICE_ADD => 'Nová úhrada k objednávce',
    self::EMAIL_PAYMENT_NOTICE_CHANGE => 'Změna úhrady k objednávce',
    self::EMAIL_PAYMENT_NOTICE_REMOVE => 'Odstranění úhrady k objednávce',
    self::EMAIL_NOTICE_UNPAID_INVOICES => 'Přehled neuhrazených faktur po splatnosti',
    self::EMAIL_NOTICE_PARTS_EXPORT => 'Automaticky vygenerované exporty skladu Meduse',
    self::EMAIL_NOTICE_ORDER_CHANGE => 'Změna objednávky',
    self::EMAIL_INVOICE_IN_CASH_NOTICE => 'Nová faktura hrazená v hotovosti',
    self::EMAIL_NOTICE_ORDER_PACKAGING => 'Změna balení objednávky',
    self::EMAIL_TOBACCO_PACKING_REPORT => 'Zadání hromadného nabalení',
    self::EMAIL_NOTICE_PARTS_EXPORT_MEDITE => 'Automaticky vygenerované exporty skladu Medite',
    self::EMAIL_ORDER_DEADLINE_NOTICE => 'Změna deadlinu objednávky',
  );

	public function sendEmail($emailType, $params) {
		switch($emailType){
			case self::EMAIL_ORDER_CONFIRM:
				if(!isset($params['orderId'])) throw new Exception('"orderId" param must be send to the getEmail() function!');
				$email = EmailFactory::orderConfirmEmail($params['orderId']);
				$this->send($email);
				break;
			case self::EMAIL_PRODUCTION_CREATION:
				$email = EmailFactory::orderProductionEmail($params['productionId']);
				$this->send($email);
				break;
			case self::EMAIL_NEW_LOCATION:
				$email = EmailFactory::newLocationEmail($params['ip'],$params['hash']);
				$this->send($email);
				break;
      case self::EMAIL_ORDER_EXPEDITION_NOTICE:
				$email = EmailFactory::orderExpeditionEmail($params['orderId']);
				$this->send($email);
				break;
      case self::EMAIL_TOBACCO_ORDER_EXPEDITION_NOTICE:
				$email = EmailFactory::tobaccoOrderExpeditionEmail($params['orderId']);
				$this->send($email);
				break;
      case self::EMAIL_ORDER_EXPEDITION_NOTICE_EX_WORKS:
				$email = EmailFactory::orderExpeditionEmailExWorks($params['orderId']);
				$this->send($email);
				break;
      case self::EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE:
				$tUser = new Table_Users();
				$user = $tUser->getUserByName(Zend_Auth::getInstance()->getIdentity());
				$email = EmailFactory::orderExpeditionCustomerEmail($params['orderId'], $params['subject'], $params['body'], $params['to']);
				if ($this->send($email, $user['email'], $user['name_full'], $user['email'])) {
					$tOrders = new Table_Orders();
					$tOrders->update(array('date_notification' => date('Y-m-d')), 'id = ' . $params['orderId']);
				}
				break;
			case self::EMAIL_PRE_ORDER_14_DAYS_NOTICE:
				$email = EmailFactory::preOrder14DaysNoticeEmail($params['orderId']);
				$this->send($email);
				break;
      case self::EMAIL_PRE_ORDER_21_DAYS_NOTICE:
				$email = EmailFactory::preOrder21DaysNoticeEmail($params['orderId']);
				$this->send($email);
				break;
      case self::EMAIL_PRE_ORDER_FULL_NOTICE:
				$email = EmailFactory::preOrderFullNoticeEmail($params);
				$this->send($email);
				break;
      case self::EMAIL_BUG_REPORT:
				$email = EmailFactory::bugEmail($params);
				$this->send($email);
				break;
			case self::EMAIL_MISSING_PARTS_DAILY_SUMMARY:
				$email = EmailFactory::missingPartsSummaryEmail($params);
				$this->send($email);
				break;
      case self::EMAIL_ISSUE_CLOSE:
				$email = EmailFactory::issueCloseEmail($params);
				$this->send($email);
				break;
      case self::EMAIL_ISSUE_COMMENT:
				$email = EmailFactory::issueCommentEmail($params);
				$this->send($email);
				break;
      case self::EMAIL_PRODUCTION_DELAY:
				$email = EmailFactory::delayedProductionEmail($params);
				$this->send($email);
				break;
      case self::EMAIL_ORDER_NEW_CUSTOMER_NOTICE:
				$tUser = new Table_Users();
				$user = $tUser->getUserByName(Zend_Auth::getInstance()->getIdentity());
				$email = EmailFactory::orderNewCustomerEmail($params['orderId'], $params['subject'], $params['body'], $params['to']);
				if ($this->send($email, $user['email'], $user['name_full'], $user['email'])) {
					$tOrders = new Table_Orders();
					$tOrders->update(array('date_notification' => date('Y-m-d')), 'id = ' . $params['orderId']);
				}
				break;
      case self::EMAIL_ORDER_OPEN_CUSTOMER_NOTICE:
				$tUser = new Table_Users();
				$user = $tUser->getUserByName(Zend_Auth::getInstance()->getIdentity());
				$email = EmailFactory::orderOpenCustomerEmail($params['orderId'], $params['subject'], $params['body'], $params['to']);
				if ($this->send($email, $user['email'], $user['name_full'], $user['email'])) {
					$tOrders = new Table_Orders();
					$tOrders->update(array('date_notification' => date('Y-m-d')), 'id = ' . $params['orderId']);
				}
				break;
      case self::EMAIL_ORDER_OPEN_NOTICE:
        $email = EmailFactory::orderOpenEmail($params['orderId']);
        $this->send($email);
        break;
      case self::EMAIL_TOBACCO_ORDER_OPEN_NOTICE:
        $email = EmailFactory::tobaccoOrderOpenEmail($params['orderId']);
        $this->send($email);
        break;
      case self::EMAIL_TOBACCO_ORDER_IS_READY_NOTICE:
        $email = EmailFactory::tobaccoOrderIsReadyEmail($params['orderId']);
        $this->send($email);
        break;
      case self::EMAIL_NOTICE_CRITICAL_AMOUNT:
        $email = EmailFactory::noticeCriticalAmountEmail($params);
				$this->send($email);
				break;
      case self::EMAIL_NOTICE_ZERO_COSTS:
        $email = EmailFactory::noticeZeroCostsEmail($params);
				$this->send($email);
				break;
			case self::EMAIL_VERIFY_WORKSHEET_NOTICE:
				$email = EmailFactory::verifyWorksheetEmail($params);
				$this->send($email);
				break;
      case self::EMAIL_END_EMP_CONTRACT_NOTICE:
				$email = EmailFactory::endEmployeeContractEmail($params);
				$this->send($email);
				break;
      case self::EMAIL_BIRTHDAY_NOTICE:
				$email = EmailFactory::birthdayEmail($params);
				$this->send($email);
				break;
      case self::EMAIL_ORDER_PREPARE:
				$email = EmailFactory::orderPrepareEmail($params);
				$this->send($email);
				break;
      case self::EMAIL_INVOICE_CHANGE:
				$email = EmailFactory::invoiceChangeEmail($params);
				$this->send($email);
        break;
      case self::EMAIL_PAYMENT_NOTICE_ADD:
				$email = EmailFactory::paymentAddEmail($params);
				$this->send($email);
        break;
      case self::EMAIL_PAYMENT_NOTICE_CHANGE:
				$email = EmailFactory::paymentChangeEmail($params);
				$this->send($email);
        break;
      case self::EMAIL_PAYMENT_NOTICE_REMOVE:
				$email = EmailFactory::paymentRemoveEmail($params);
				$this->send($email);
        break;
      case self::EMAIL_NOTICE_UNPAID_INVOICES:
				$email = EmailFactory::unpaidInvoicesEmail($params);
				$this->send($email);
        break;
      case self::EMAIL_NOTICE_PARTS_EXPORT:
				$email = EmailFactory::partsExportEmail($params);
				$this->send($email);
        break;
      case self::EMAIL_NOTICE_ORDER_CHANGE:
        $email = EmailFactory::orderChangeEmail($params);
        $this->send($email);
        break;
      case self::EMAIL_INVOICE_IN_CASH_NOTICE:
        $email = EmailFactory::invoiceInCashEmail($params);
        $this->send($email);
        break;
      case self::EMAIL_NOTICE_ORDER_PACKAGING:
        $email = EmailFactory::noticeOrderPackagingEmail($params);
        $this->send($email);
        break;
      case self::EMAIL_TOBACCO_PACKING_REPORT:
        $email = EmailFactory::tobaccoPackingReport($params);
        $this->send($email);
        break;
      case self::EMAIL_NOTICE_PARTS_EXPORT_MEDITE:
        $email = EmailFactory::partsExportEmailMedite($params);
        $this->send($email);
        break;
      case self::EMAIL_ORDER_DEADLINE_NOTICE:
        $email = EmailFactory::noticeOrderDeadlineEmail($params);
        $this->send($email);
        break;
      default:
				throw new Exception('neznamy typ emailu '.$emailType);
		}
	}

	/**
	 * Odesle email a ulozi log
	 * @param Zend_Db_Table_Row_Abstract $emailRow
	 * @return int id emailu v logu
	 */
	private function send($emailRow, $from = '', $fromName = '', $bbc = '') {
		$mail = new Zend_Mail('UTF-8');
		if (isset(Zend_Registry::get('config')->mail->transport)) {
			$mail->setDefaultTransport(new Zend_Mail_Transport_Smtp(
				Zend_Registry::get('config')->mail->transport->host,
				Zend_Registry::get('config')->mail->transport->toArray()
			));
		}
		$mail->setFrom($from ? $from : $this->_from, $fromName ? $fromName : $this->_from_name);
		$mail->setBodyHtml($emailRow->body);

    if (Zend_Registry::get('version') == 'TEST') {
      $emailRow->to = '';
      if (isset(Zend_Registry::get('config')->develop->mail)) {
        $emailRow->to = Zend_Registry::get('config')->develop->mail;
        $emails = explode(',', Zend_Registry::get('config')->develop->mail);
        foreach ($emails as $email){
          $mail->addTo($email);
        }
      } else {
        throw new Exception("No developer e-mail found!");
      }
      $emailRow->subject = '[DEV] '.$emailRow->subject;
    }
    else {
      $emails = explode(',',$emailRow->to);
      foreach($emails as $email) {
        $mail->addTo($email);
      }
      if (isset(Zend_Registry::get('config')->develop->mail)) {
        $emails = explode(',', Zend_Registry::get('config')->develop->mail);
        foreach($emails as $email){
          $mail->addBcc($email);
        }
      }
    }
		$mail->setEncodingOfHeaders(Zend_Mime::ENCODING_BASE64);
    $mail->setSubject($emailRow->subject);
		$mail->addBcc($bbc);
		$mail->send();
		return $emailRow->save();
	}
}

