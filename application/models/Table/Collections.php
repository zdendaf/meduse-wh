<?php
class Table_Collections extends Zend_Db_Table_Abstract{

	protected $_name = 'collections';
	protected $_primary = 'id';

  /**
   * Get collections select.
   * @param bool $pipes_only
   *   ve vysledich budou pouze kolekce dymek.
   * @return Zend_Db_Table_Select
   */
  public function getCollectionsSelect($pipes_only = FALSE) {
    $select = $this->select();
    $select->from($this->_name);
    if ($pipes_only) {
      $select->where('is_pipes = ?', 'y');
    }
    return $select;
  }

  /**
   * Vraci IDs kolekci dymek.
   * @return array
   */
  public function getAllCollectionsIds() {
    $ids = [];
    $select = $this->getCollectionsSelect(FALSE);
    if ($data = $select->query()->fetchAll()) {
      foreach ($data as $row) {
        $ids[] = $row['id'];
      }
    }
    return $ids;
  }

  /**
   * Vraci IDs kolekci dymek.
   * @return array
   */
  public function getPipesCollectionsIds() {
    $ids = [];
    $select = $this->getCollectionsSelect(TRUE);
    if ($data = $select->query()->fetchAll()) {
      foreach ($data as $row) {
        $ids[] = $row['id'];
      }
    }
    return $ids;
  }
}
