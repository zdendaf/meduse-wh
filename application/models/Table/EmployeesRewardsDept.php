<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Table_EmployeesRewardsDept extends Zend_Db_Table_Abstract {

    protected $_name = 'employees_rewards_dept';
    protected $_primary = 'id';


    public function getRewards($id_employees, $year, $month) {

      $select = $this->select()
        ->where('id_employees_rewards = ?', $id_employees)
        ->where('year_employees_rewards = ?', $year)
        ->where('month_employees_rewards = ?', $month);

      $rows = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();

      return $rows;
    }

    public function getTotal($id_employees, $year, $month) {
      $select = $this->select()
        ->from($this->_name, array(new Zend_Db_Expr('SUM(value)')))
        ->where('id_employees_rewards = ?', $id_employees)
        ->where('year_employees_rewards = ?', $year)
        ->where('month_employees_rewards = ?', $month);
      $total = $select->query()->fetchColumn();
      return $total;
    }
  }

