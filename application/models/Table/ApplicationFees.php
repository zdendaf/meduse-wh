<?php
class Table_ApplicationFees extends Zend_Db_Table_Abstract {

	protected $_name = 'application_fees';
	protected $_primary = 'id';

  public function list(bool $enabled = null, $asSelect = false) {
    $select = $this->select();
    if (!is_null($enabled)) {
      $select->where('enabled = ?', $enabled);
    }
    if ($asSelect) {
      return $select;
    }
    $return = [];
    if ($fees = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll()) {
      foreach ($fees as $item) {
        $return[$item['id']] = $item;
      }
    }

    return $return;
  }

  public function setEnabled(int $feeId, $enable = true): bool
  {
    $count = $this->update(['enabled' => $enable], 'id = ' . $this->getAdapter()->quote($feeId));
    return $count === 1;
  }
}
