<?php

  class Table_Invoices extends Zend_Db_Table_Abstract {

    protected $_name = 'invoice';
    protected $_primary = 'id';

    const TYPE_OFFER = 'offer';
    const TYPE_PROFORM = 'proforma';
    const TYPE_REGULAR = 'regular';
    const TYPE_CREDIT = 'credit'; # dobropis

    public static $typeToString = array(
      self::TYPE_OFFER => 'nabídka',
      self::TYPE_PROFORM => 'proforma',
      self::TYPE_REGULAR => 'faktura',
      self::TYPE_CREDIT => 'dobropis',
    );

    const REGION_CZ = 'cz';
    const REGION_EU = 'eu';
    const REGION_WORLD = 'world';

    public static $regionToString = array(
      self::REGION_CZ => 'Česká republika',
      self::REGION_EU => 'Evropská unie',
      self::REGION_WORLD => 'svět'
    );

    public const CUSTOMER_TYPE_B2B = 'B2B';
    public const CUSTOMER_TYPE_B2C = 'B2C';
    public const LANGUAGE_EN = 'en';
    public const LANGUAGE_CS = 'cs';

    public static array $languageToString = array(
      self::LANGUAGE_EN => 'anglicky',
      self::LANGUAGE_CS => 'česky',
    );

    public const CURRENCY_CZK = 'CZK';
    public const CURRENCY_EUR = 'EUR';

    public static array $currencyToString = array(
      self::CURRENCY_CZK => 'Česká koruna',
      self::CURRENCY_EUR => 'Euro'
    );
    protected static array $currencyToSign = array(
      self::CURRENCY_CZK => 'Kč',
      self::CURRENCY_EUR => '€'
    );

      protected static array $currencyToSignEn = array(
          self::CURRENCY_CZK => 'CZE',
          self::CURRENCY_EUR => 'EUR'
      );

      public static function getCurrencySign(string $currency, string $language = self::LANGUAGE_CS) {
          if ($language === self::LANGUAGE_CS) {
              return self::$currencyToSign[$currency];
          }
          return self::$currencyToSignEn[$currency];
      }

    function getInvoiceIdByOrder($orderId, $type = NULL) {
      $order = new Orders_Order($orderId);
      if (is_null($type)) {
        switch ($order->getStatus()) {
          case Table_Orders::STATUS_OFFER:
            $type = Table_Invoices::TYPE_OFFER;
            break;
          case Table_Orders::STATUS_NEW:
            $type = Table_Invoices::TYPE_PROFORM;
            break;
          case Table_Orders::STATUS_CONFIRMED:
          case Table_Orders::STATUS_OPEN:
          case Table_Orders::STATUS_CLOSED:
            $type = Table_Invoices::TYPE_REGULAR;
            break;
          default:
            $type = '';
            break;
        }
      }
      $select = $this->select();
      $select->from($this->_name, $this->_primary)
        ->where('id_orders = ?', $orderId)
        ->where('type = ?', $type)
        ->setIntegrityCheck(false);
      return $select->query()->fetchColumn();
    }

    public function findRate($orderId, $type, $wh = 1) {
      $select = $this->select()->setIntegrityCheck(false);
      $select->from($this->_name, 'rate');
      $select->where('id_orders = ?', $orderId);
      $select->where('type = ?', $type);
      $rate = $select->query()->fetchColumn();
      if ($rate) {
        return $rate;
      } else {
        $tRate = new Table_ApplicationRate();
        return $tRate->getRate($wh);
      }
    }

    public function getAllInvoices($wh_type = 1) {
      $select = $this->getAllInvoicesSelect($wh_type);
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }

    public function getAllInvoicesSelect($wh_type = 1, $showNonNumbered = TRUE, $onlyClosed = FALSE): Zend_Db_Table_Select
    {
      $select = $this->select();
      $select
        ->setIntegrityCheck(false)
        ->from(array('i' => $this->_name), array(
          'id',
          'invoice_no' => 'no',
          'invoice_type' => 'type',
          'inserted',
          'language',
          'currency',
          'locked' => new Zend_Db_Expr("IF(registered = 'n' OR DATE_FORMAT(NOW(), '%Y-%m-%d') < DATE_FORMAT(DATE_ADD(IFNULL(duzp, NOW()), INTERVAL 1 MONTH), '%Y-%m-15'), 'n', 'y')"),
          'registered',
          'invoiced_total',
          'payment' => new Zend_Db_Expr("IF(SUM(p.amount) = i.invoiced_total, 'p', IF(i.type = 'proforma' OR i.type = 'regular', 'u', 'x'))"),
        ));

        $orderJoinCondition = 'o.id = i.id_orders' . ($onlyClosed ? ' AND o.status = \'closed\'' : '');
        $select->join(array('o' => 'orders'), $orderJoinCondition, array('order_id' => 'id', 'order_no' => 'no', 'title', 'description'))
          ->joinLeft(array('c' => 'customers'), 'c.id = o.id_customers', array('company', 'customer_id' => 'id', 'customer_type' => 'type'))
          ->joinLeft(array('a' => 'addresses'), 'c.id_addresses = a.id', array())
          ->joinLeft(array('cc' => 'c_country'), 'a.country = cc.code', array('region'))
          ->joinLeft(array('p' => 'payments'), 'p.id_invoice = i.id', array())
          ->group('i.id')
          ->where('i.id_wh_type = ?', $wh_type);

      if (!$showNonNumbered) {
        $select->where('i.type <> \'regular\' OR i.no IS NOT NULL');
      }

      return $select;
    }

    public function getFilteredInvoicesSelect(array $options): Zend_Db_Table_Select
    {

      $wh_type = $options['wh'] ?? Table_PartsWarehouse::WH_TYPE;
      $showNonNumbered = ! ($options['only_numbered'] ?? false);
      $onlyClosed = $options['only_closed'] ?? false;
      $filter = $options['filter'] ?? null;

      $select = $this->getAllInvoicesSelect($wh_type, $showNonNumbered, $onlyClosed);
      $select->where('i.type <> ?', self::TYPE_OFFER);

      $params = array(
        'title' => $filter->title ?? NULL,
        'customer_type' => $filter->customer_type ?? NULL,
        'no' => $filter->no ?? NULL,
        'type' => $filter->type ?? NULL,
        'region' => $filter->region ?? NULL,
        'language' => $filter->language ?? NULL,
        'inserted_from' => $filter->inserted_from ?? NULL,
        'inserted_to' => $filter->inserted_to ?? NULL,
      );

      // název společnosti
      if ($params['title']) {
        $name = addslashes(strip_tags(trim($params['title'])));
        $select->where("o.title LIKE ?","%$name%");
      }

      // typ zákazníka
      if ($params['customer_type']) {
        $select->where("c.type = ?", $params['customer_type']);
      }

      // číslo faktury
      if ($params['no']) {

        if ($params['no'] == 'nic') {
          $select->where("i.no IS NULL");
        } else {
          $select->where("i.no = ?", $params['no']);
        }
      }

      // typ faktury
      if ($params['type']) {
        $select->where("i.type = ?", $params['type']);
      }

      // region
      if ($params['region']) {
        $select->where("cc.region = ?", $params['region']);
      }

      // jazyk
      if ($params['language']) {
        $select->where("i.language = ?", $params['language']);
      }

      // datum vložení
      if ($params['inserted_from']) {
        $select->where("i.inserted >= ?", date('Y-m-d', strtotime($params['inserted_from'])));
      }
      if ($params['inserted_to']) {
        $select->where("i.inserted < ?", date('Y-m-d', strtotime($params['inserted_to'] . '+1day')));
      }


      return $select;
    }

    // todo: presunout do Table_InvoiceCustomItems
    public function getCustomItems($invoiceId) {
      $select = $this->select()->setIntegrityCheck(false)
        ->from('invoice_custom_items')
        ->where('id_invoice = ?', $invoiceId);
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }

    public function getCustomDiscount($invoiceId) {
      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from('invoice_custom_items', array('discount' => 'SUM(price)'))
        ->where('id_invoice = ?', $invoiceId)
        ->where('type = ?', Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_DISCOUNT);
      return $select->query()->fetchColumn();
    }

    public function getCustomDiscountText($invoiceId) {
      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from('invoice_custom_items', array('text' => 'GROUP_CONCAT(description SEPARATOR ", ")'))
        ->where('id_invoice = ?', $invoiceId)
        ->where('type = ?', Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_DISCOUNT);
      return $select->query()->fetchColumn();
    }

    /**
     *
     *
     * @param int $orderID ID objednávky
     * @param string $type Typ faktury (volitelné, výchozí klasická)
     * @return array|null Řádek tabulky s fakturou
     */
    public function getInvoiceByOrder($orderID, $type = 'regular') {
      $select = $this->select()
          ->where('id_orders = ?', $orderID)
          ->where('type = ?', $type)
          ->query()->fetch();

      return $select;
    }

    /**
     * Vrati DB zaznamy vsech faktur pridruzenych k objednavce.
     */
    public function getAllInvoicesByOrder($orderID, $numbered_only = FALSE) {
      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(array('i' => 'invoice'), array('*', 'no' => new Zend_Db_Expr('IFNULL(i.no, o.proforma_no)')));
      $select->where('i.id_orders = ?', $orderID);
      $select->joinLeft(array('o' => 'orders'), 'o.id = i.id_orders', array());
      if ($numbered_only) {
        $select->where('i.no IS NOT NULL OR i.type <> "regular"');
      }
      return $select->query()->fetchAll();
    }


    /**
     * Testuje unikatnost cisla faktury daneho typu a daneho skladu.
     *
     * Pokud je testovane cislo faktury rovno NULL, pak vrati vzdy TRUE.
     * Jinak vraci TRUE, pokud takova faktura neexistuje, id faktury kdyz uz v systemu je.
     *
     * @param string $no
     *    cislo faktury
     * @param string $type
     *    (volitelne) typ faktury, default honota je 'regular'
     * @param type $wh
     *    (volitelne) oznaceni skladu, default hodnota je 1.
     * @return mixed
     */
    public function isFree($no, $type = Table_Invoices::TYPE_REGULAR, $wh = 1) {
      if (!$no) {
        return TRUE;
      }
      $select = $this->select()
        ->where('no = ? ', $no)
        ->where('type = ?', $type)
        ->where('id_wh_type = ?', $wh);
      $result = $select->query()->fetch();
      return $result ? $result['id'] : TRUE;
    }


    /**
     * Vraci dalsi volne cislo faktury.
     *
     * @param Orders_Order $order
     *    Objednavka
     * @param array $options
     *    Další volitelné parametry
     *    'check_series' bool kontrolovat posloupnost
     *    'date' Meduse_Date datum
     *    'prefix_extra' string extra prefix
     *
     * @return string Oznaceni faktury
     */
    public function getNextNo(Orders_Order $order, array $options = []) {

      $id_wh_type = $order->getWhType();

      // @todo do admin. konfigurace
      if ($id_wh_type == Pipes_Parts_Part::WH_TYPE) {
        $options += array(
          'prefix_year' => 2,
          'prefix_month' => 0,
          'counter' => 4,
        );
      }
      elseif ($id_wh_type == Tobacco_Parts_Part::WH_TYPE) {
        $options += array(
          'prefix_year' => 2,
          'prefix_month' => 0,
          'counter' => 4,
          'prefix_extra' => $order->getExpectedCurrency() == Table_Orders::CURRENCY_EUR ? 'E' : '',
        );
      }
      else {
        return FALSE;
      }

      $this->defaultOptions($options);

      $date = $options['date'];
      $check_series = $options['check_series'];

      $next_number = 1;

      $number = NULL;

      // prohledavani ve fakturach
      if ($check_series || is_null($number)) {
        $number = $this->findFreeNo($id_wh_type, $options);
        if ($number) {
          $next_number = substr($number, -$options['counter']);
        }
      }

      // vytvoreni cisla (retezec)
      return $options['prefix_extra'] . $date->get('y') . str_pad($next_number, $options['counter'], '0', STR_PAD_LEFT);
    }

    /**
     * Vraci posledni cislo faktury.
     *
     * @param Orders_Order $order
     *    Objednavka
     * @param array $options
     *    Další volitelné parametry
     *    'check_series' bool kontrolovat posloupnost
     *    'date' Meduse_Date datum
     *    'prefix_extra' string extra prefix
     *
     * @return string Oznaceni faktury
     */
    public function getLastNo(Orders_Order $order, $options = array()) {

      $id_wh_type = $order->getWhType();

      // @todo do admin. konfigurace
      if ($id_wh_type == Pipes_Parts_Part::WH_TYPE) {
        $options += array(
          'prefix_year' => 2,
          'prefix_month' => 0,
          'counter' => 4,
        );
      }
      elseif ($id_wh_type == Tobacco_Parts_Part::WH_TYPE) {
        $options += array(
          'prefix_year' => 2,
          'prefix_month' => 0,
          'counter' => 4,
          'prefix_extra' => $order->getExpectedCurrency() == Table_Orders::CURRENCY_EUR ? 'E' : '',
        );
      }
      else {
        return FALSE;
      }

      $this->defaultOptions($options);

      $date = $options['date'];
      $check_series = $options['check_series'];

      $last_number = NULL;
      $number = NULL;

      // prohledavani ve fakturach
      if ($check_series || is_null($number)) {
        $number = $this->findLastNo($id_wh_type, $options);
        if ($number) {
          $last_number = substr($number, -$options['counter']);
        }
      }
      if ($last_number) {
        // vytvoreni cisla (retezec)
        $number_string = $options['prefix_extra'] . $date->get('y') . str_pad($last_number, $options['counter'], '0', STR_PAD_LEFT);
        return $number_string;
      }
      else {
        return NULL;
      }
    }


    protected function findFreeNo($id_wh_type, $options = array(), $find_gap = FALSE) {

      // definice hodnoty pocitadla pro pripad nenalezeni v db
      $next = 1;

      // vychozi hodnoty
      $this->defaultOptions($options);

      $range = FALSE;

      // pripadne prohledame stavajici posloupnost
      if ($find_gap) {
        $range = $this->findGaps($id_wh_type, $options);
      }

      // existuje dira
      if ($range) {
        $next = $range['starts'];
      }

      // neexituje dira najdeme nejvyssi cislo faktury daneho roku
      else {
        $select = $this->select()->setIntegrityCheck(FALSE);
        $select->from($this->_name, array(
          'next' => new Zend_Db_Expr('SUBSTR(no, ' . -$options['counter'] . ') + 1'),
        ));
        $start = strlen($options['prefix_extra']) + 1;
        $select->where('SUBSTR(no, ' . $start . ', ' . $options['prefix_year'] . ') LIKE ?', $options['date']->get('y'));
        $select->where('id_wh_type = ?', $id_wh_type);
        $select->where('type = ?', $options['type']);
        $select->where('issue_date >= ?', $options['date']->get('Y-01-01'));
        $select->order('no DESC')->limit(1);

        $result = $select->query()->fetchColumn();
        if ($result) {
          $next = $result;
        }
      }
      // vrati retezec s cilem dalsi faktury
      return $options['prefix_extra'] . $options['date']->get('y') . str_pad($next, $options['counter'], 0, STR_PAD_LEFT);

    }

    protected function findLastNo($id_wh_type, $options = array(), $find_gap = FALSE) {

      // definice hodnoty pocitadla pro pripad nenalezeni v db
      $next = 1;

      // vychozi hodnoty
      $this->defaultOptions($options);

      $range = FALSE;

      // pripadne prohledame stavajici posloupnost
      if ($find_gap) {
        $range = $this->findGaps($id_wh_type, $options);
      }

      // existuje dira
      if ($range) {
        $next = $range['starts'];
      }

      // neexituje dira najdeme nejvyssi cislo faktury daneho roku
      else {
        $select = $this->select()->setIntegrityCheck(FALSE);
        $select->from($this->_name, array(
          'last' => new Zend_Db_Expr('SUBSTR(no, ' . -$options['counter'] . ')'),
        ));
        $start = strlen($options['prefix_extra']) + 1;
        $select->where('SUBSTR(no, ' . $start . ', ' . $options['prefix_year'] . ') LIKE ?', $options['date']->get('y'));
        $select->where('id_wh_type = ?', $id_wh_type);
        $select->where('type = ?', $options['type']);
        $select->order('no DESC')->limit(1);

        $result = $select->query()->fetchColumn();
        if ($result) {
          $next = $result;
        }
      }
      // vrati retezec s cilem posledni faktury
      return $options['prefix_extra'] . $options['date']->get('y') . str_pad($next, $options['counter'], 0, STR_PAD_LEFT);

    }

    public function findGaps($id_wh_type, $options = array()) {

      $this->defaultOptions($options);

      $subMin = $this->select()->setIntegrityCheck(FALSE);
      $subMin->from(array('i3' => $this->_name), array(
        new Zend_Db_Expr('MIN(SUBSTR(i3.no, ' . -$options['counter'] . ') + 0) - 1'),
      ));
      $subMin->where('SUBSTR(i3.no, ' . -$options['counter'] . ') + 0 > SUBSTR(i1.no, ' . -$options['counter'] . ') + 0');
      $subMin->where('SUBSTR(i3.no, 1, ' . $options['prefix_year'] . ') LIKE ?', $options['date']->get('y'));
      $subMin->where('i3.id_wh_type = ?', $id_wh_type);
      $subMin->where('i3.type = ?', self::TYPE_REGULAR);

      $subWhere = $this->select()->setIntegrityCheck(FALSE);
      $subWhere->from(array('i2' => $this->_name), array(
        new Zend_Db_Expr('SUBSTR(i2.no, ' . -$options['counter'] . ') + 0'),
      ));
      $subWhere->where('SUBSTR(i2.no, ' . -$options['counter'] . ') + 0 = SUBSTR(i1.no, ' . -$options['counter'] . ') + 1');
      $subWhere->where('SUBSTR(i2.no, 1, ' . $options['prefix_year'] . ') LIKE ?', $options['date']->get('y'));
      $subWhere->where('i2.id_wh_type = ?', $id_wh_type);
      $subWhere->where('i2.type = ?', self::TYPE_REGULAR);

      $select = $this->select()->setIntegrityCheck(FALSE)->distinct(TRUE);
      $select->from(array('i1' => $this->_name), array(
        'starts' => new Zend_Db_Expr('SUBSTR(i1.no, ' . -$options['counter'] . ') + 1'),
        'ends' => new Zend_Db_Expr('(' . $subMin . ')'),
      ));
      $select->where('NOT EXISTS (?)', $subWhere);
      $select->where('SUBSTR(i1.no, 1, ' . $options['prefix_year'] . ') LIKE ?', $options['date']->get('y'));
      $select->where('i1.id_wh_type = ?', $id_wh_type);
      $select->where('i1.type = ?', self::TYPE_REGULAR);
      $select->having('ends IS NOT NULL');
      $select->order(array('starts', 'ends'));

      $range = $select->query()->fetch();

      return $range;
    }

    /**
     * Doplni pole voleb pro hledani vsemi hodnotami.
     */
    private function defaultOptions(&$options) {
      $options += array(
        'prefix_extra' => '',
        'prefix_year' => 2,
        'prefix_month' => 2,
        'counter' => 3,
        'date' => new Meduse_Date(),
        'type' => self::TYPE_REGULAR,
        'check_series' => FALSE,
      );
    }

    /**
     * Vrati statistiku uhrad faktur neuhrazenych objednavek.
     */
    public function getPaymentsStats($options = array()): array
    {
      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(array('i' => 'invoice'), array(
        'id' => 'id',
        'type',
        'wh' => 'id_wh_type',
        'issue_date',
        'due_date',
        'invoice_no' => new Zend_Db_Expr('IFNULL(i.no, o.proforma_no)'),
        'currency',
        'rate',
        'invoiced_total'
      ));
      $select->joinLeft(array('o' => 'orders'), 'o.id = i.id_orders AND (o.paid_percentage IS NULL OR o.paid_percentage < 100)', array(
        'order_id' => 'id',
        'order_title' => 'o.title',
        'order_no' => 'o.no',
      ));
      $select->joinLeft(array('p' => 'payments'), 'p.id_invoice = i.id', array(
        'payments' => new Zend_Db_Expr('IFNULL(SUM(p.amount), 0)')
      ));
      $select->joinLeft(array('u' => 'users'), 'i.id_users = u.id', array(
        'owner_id' => 'u.id',
        'owner_name' => 'u.name_full',
        'owner_email' => 'u.email',
      ));

      $select->where('i.invoiced_total > 0');
      $select->where('i.type <> "credit"');
      $select->where('i.no IS NOT NULL OR i.type <> "regular"');
      $select->where('o.status NOT IN ("trash", "deleted")');

      if (isset($options['overdue']) && $options['overdue']) {
        $select->where('i.due_date < NOW()');
      }
      if (isset($options['unpaid']) && $options['unpaid']) {
        $select->having('payments < invoiced_total');
      }
      if (isset($options['wh'])) {
        $select->where('i.id_wh_type = ?', $options['wh']);
      }
      $select->group('i.id');

      return $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
    }

    public function selectSpending($customerId = NULL, $year = NULL, $type = NULL, $execute = TRUE) {
      $select = $this->select()->setIntegrityCheck(FALSE);
      $select->from(array('i' => 'invoice'), array(
        'year' => new Zend_Db_Expr('YEAR(IFNULL(i.duzp, i.issue_date))'),
        'amount' => new Zend_Db_Expr('ROUND(SUM(IF(i.currency = "'
          . self::CURRENCY_CZK . '", i.invoiced_total / i.rate, i.invoiced_total)))'),
      ));
      $select->joinInner(array('o' => 'orders'), 'i.id_orders = o.id', array());
      $select->joinLeft(array('c' => 'customers'), 'c.id = o.id_customers', array('customer' => 'id'));
      $select->where('i.type = ?', Table_Invoices::TYPE_REGULAR);
      if ($customerId) {
        $select->where('c.id = ?', $customerId);
      }
      if ($year) {
        $select->where('YEAR(IFNULL(i.duzp, i.issue_date)) = ?', $year);
      }
      if ($type) {
        $select->where('o.type = ?', $type);
      }
      $select->group(array('customer', 'year'));
      return $execute ? $select->query(Zend_Db::FETCH_ASSOC)->fetchAll() : $select;
    }

    /**
     * @throws Zend_Db_Statement_Exception
     */
    public function getPaymentsForDeduction($invoiceId, $allCandidates = FALSE): array
    {

    // Pro kazdou uhradu suma její umístění.
    $s = $this->select()->setIntegrityCheck(FALSE)
      ->from('payments_deductions', [
        'id_payments',
        'amount' => new Zend_Db_Expr('SUM(amount)'),
      ])
      ->group('id_payments');

    // Seznam použitých úhrad k faktuře.
    $s2 = $this->select()->setIntegrityCheck(FALSE)
      ->from('payments_deductions', ['id', 'id_payments', 'amount'])
      ->where('deduct_from = ?', $invoiceId);


    // Radek s ID zakaznika
    $s3 = $this->select()->setIntegrityCheck(FALSE)
      ->from(['i' => 'invoice'], [])
      ->join(['o' => 'orders'], 'o.id = i.id_orders', 'id_customers')
      ->where('i.id = ?', $invoiceId);

    // Seznam proforem zakaznika
    $select = $this->select()->setIntegrityCheck(FALSE)
      ->from(['i' => 'invoice'], ['id_invoice' => 'id'])
      ->join(['o' => 'orders'], 'o.id = i.id_orders', ['proforma_no'])
      ->join(['p' => 'payments'],
        'p.id_invoice = i.id AND p.date >= \'2019-01-01\' AND p.no IS NOT NULL', [
          'id', 'amount', 'rate', 'currency', 'date',
          'remain' => new Zend_Db_Expr('p.amount - (IFNULL(s2.amount, 0))'),
        ])
      ->joinLeft(['s' => $s], 's.id_payments = p.id AND s.amount < p.amount', [])
      ->joinLeft(['s2' => $s2], 's2.id_payments = p.id', [
        'id_payments_deductions' => 'id',
        'deducted' => new Zend_Db_Expr('IFNULL(s2.amount, 0)')
        ])
      ->where('o.id_customers IN ?', $s3)
      ->where('i.type = ?', self::TYPE_PROFORM)
      ->group('p.id');
      if (!$allCandidates) {
        $select->where('IFNULL(s2.amount, 0) > 0');
      }

      $result = $select->query();
      $payments = [];
      while ($row = $result->fetch()) {
        $payments[$row['id']] = $row;
      }
      return $payments;
    }
  }