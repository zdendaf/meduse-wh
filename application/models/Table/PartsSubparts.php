<?php

class Table_PartsSubparts extends Zend_Db_Table_Abstract {

  protected $_name = 'parts_subparts';

  protected $_primary = 'id';

  public function selectSubpartsAmount($multipart = NULL) {
    $select = $this->select()
      ->setIntegrityCheck(FALSE)
      ->from(['ps' => $this->_name], [
        'multipart' => 'id_multipart',
        'amount_per_multipart' => 'amount',
        'part' => 'id_parts',
      ])
      ->joinLeft(['pw' => 'parts_warehouse'], 'ps.id_parts = pw.id_parts', [
        'amount_warehouse' => 'amount',
        'amount_critical' => 'critical_amount',
      ])
      ->joinLeft(['p' => 'parts'], 'ps.id_parts=p.id', ['name']);

    if (!is_null($multipart)) {
      $select->where('id_multipart = ?', $multipart);
    }

    return $select;
  }

  public function hasSubpartInCategory($multipart, $part_ctg) {
    $select = $this->selectSubpartsAmount($multipart)
      ->where('p.id_parts_ctg = ?', $part_ctg);
    return $select->query()->fetch();
  }

  /**
   * @param $partId
   * @param bool $include_deleted
   *
   * @return array
   */
  public function getAllSupParts($partId, $include_deleted = TRUE) {
    $select = $this->select()->setIntegrityCheck(FALSE)
      ->distinct(TRUE)
      ->from(['s' => 'parts_subparts'])
      ->where('s.id_parts = ?', $partId);
    if (!$include_deleted) {
      $select->join(['p' => 'parts'], 'p.id = s.id_parts AND p.deleted <> "n"', []);
    }
    return $select->query()->fetchAll(Zend_Db::FETCH_ASSOC);
  }

  /**
   * @param $partId
   *
   * @return array
   */
  public function getAllSubParts($partId) {
    $select = $this->select()
      ->where('id_multipart = ?', $partId);
    return $select->query()->fetchAll(Zend_Db::FETCH_ASSOC);
  }
}
