<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Table_OrdersFiles extends Zend_Db_Table_Abstract {
    protected $_name = 'orders_files';
    protected $_primary = 'id';
    
    public function getFiles($orderId) {
      $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('f' => $this->_name), array('id', 'created', 'filename', 'title'))
        ->joinLeft(array('u' => 'users'), 'u.id = f.id_users', array('username', 'name', 'name_full'))
        ->where('f.id_orders = ?', $orderId)
        ->order('f.created ASC');
      return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }

    public function addFiles($orderId, $data) {
      foreach ($data as $line) {
        if (!trim($line['title'])) {
          $line['title'] = NULL;
        }
        $id = $this->select()
          ->where('id_orders = ?', $orderId)
          ->where('filename = ?', $line['filename'])
          ->query()
          ->fetchColumn();
        if (!$id) {
          $line['id_orders'] = $orderId;
          $this->insert($line);
        }
        else {
          $line['created'] = new Zend_Db_Expr('NOW()');
          $this->update($line, 'id = ' . $id);
        }
      }
    }
  }

  