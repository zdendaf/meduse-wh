<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Table_CustomersWarehouses extends Zend_Db_Table_Abstract {

    protected $_name = 'customers_warehouses';
    protected $_primary = 'id';

    protected static function getDefaultGridOptions() {
      return array(
        'css_class' => 'table table-bordered table-hover',
        'allow_add_link' => TRUE,
        'add_link_title' => 'Nový zákaznický sklad',
        'columns' => array(
          'id' => array(
            'header' => _('#'),
            'css_style' => 'text-align: right',
            'sorting' => TRUE,
          ),
          'name' => array(
            'header' => _('Název skladu'),
            'renderer' => 'url',
            'url' => '',
            'sorting' => TRUE,
          ),
          'company' => array(
            'header' => _('Firma'),
            'sorting' => TRUE,
          ),
          'first_name' => array(
            'header' => _('Jméno'),
            'sorting' => TRUE,
          ),
          'last_name' => array(
            'header' => _('Příjmení'),
            'sorting' => TRUE,
          ),
          'count' => array(
            'header' => _('Položek'),
            'css_style' => 'text-align: right',
          ),
          'price' => array(
            'header' => _('Hodnota [€]'),
            'css_style' => 'text-align: right',
          ),
        ),
      );
    }

    public function getGrid(array $options = array(), $id_wh_type = Parts_Part::WH_TYPE_PIPES) {
      $options = array_replace_recursive(self::getDefaultGridOptions(), $options);
      $select = $this->getSummarySelect(array('c.id_wh_type = ?' => $id_wh_type));
      $grid = new ZGrid($options);
      $grid->setSelect($select);
      return $grid;
    }

    public function getSummary($warehouseId) {
      $select = $this->getSummarySelect(array('id_customers_warehouses = ?' => $warehouseId));
      if (!$result = $select->query(Zend_Db::FETCH_OBJ)->fetch()) {
        $result = new stdClass();
        $result->price = 0;
        $result->count = 0;
      }
      return $result;
    }

    protected function getSummarySelect($conditions = array()) {
      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from(array('w' => $this->_name), array(
          'id', 'id_customers', 'name'))
        ->joinLeft(array('c' => 'customers'), 'c.id = w.id_customers', array(
          'customer_no', 'company', 'first_name', 'last_name',
        ))
        ->joinLeft(array('p' => 'customers_warehouses_products'), 'p.id_customers_warehouses = w.id', array(
          'count' => new Zend_Db_Expr('COUNT(p.id)'),
          'price' => new Zend_Db_Expr('ROUND(SUM(p.aquisition_price), 2)'),
        ))
        ->group('w.id');
      if ($conditions) {
        foreach($conditions as $cond => $value) {
          $select->where($cond, $value);
        }
      }
      return $select;
    }

    public function getOptionsArray($conditions = array()) {
      $options = array();
      $conditions += array(
        'c.id_wh_type = ?' => 1,
      );
      $select = $this->select()->setIntegrityCheck(FALSE)
        ->from(array('w' => $this->_name), array('id', 'name'))
        ->joinLeft(array('c' => 'customers'), 'c.id = w.id_customers', array())
        ->order(array('name'));
      if ($conditions) {
        foreach ($conditions as $cond => $value) {
          $select->where($cond, $value);
        }
      }
      $results = $select->query(Zend_Db::FETCH_OBJ);
      while($result = $results->fetch()) {
        $options[$result->id] = $result->name;
      }
      return $options;
    }
  }
