<?php

class Parts_Form extends Meduse_FormBootstrap {

  public function init() {

    $this->setAttribs(['enctype' => 'multipart/form-data', 'class' => 'form'])
      ->setMethod(Zend_Form::METHOD_POST);

    $order = 0;

    //kategorie
    $element = new Meduse_Form_Element_Categories([
      'name' => 'meduse_parts_category',
      'all' => FALSE,
      'type' => 'pipes',
    ]);
    $element->setOrder(++$order);
    $this->addElement($element);


    $element = new Meduse_Form_Element_MultiCheckbox('meduse_parts_collection');
    $element->addMultiOptions(Zend_Registry::get('db')
      ->fetchPairs('SELECT id,name FROM collections'));
    $element->setLabel('Kolekce');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_parts_id');
    $element->setRequired(TRUE);
    $element->setLabel('ID');
    $element->setAttrib('html', '<span id="meduse_parts_id_check"></span>');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_parts_name');
    $element->setRequired(TRUE);
    $element->setLabel('Název');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_parts_desc');
    $element->setLabel('Poznámka');
    $element->setOrder(++$order);
    $this->addElement($element);

    // typ měrné jednotka
    $element = new Zend_Form_Element_Select('meduse_parts_wh_measure');
    $element->addMultiOptions([
      Parts_Part::WH_MEASURE_UNIT => 'kusy',
      Parts_Part::WH_MEASURE_WEIGHT => 'hmotnost',
    ]);
    $element->setLabel('Typ měrné jednotky');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_parts_version');
    $element->setLabel('Verze');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Select('meduse_parts_producer');
    $element->addMultiOption(NULL, '-- žádný --');
    $element->addMultiOptions(Zend_Registry::get('db')
      ->fetchPairs('SELECT id,name FROM producers ORDER BY name ASC'));
    $element->setLabel('Dodavatel');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Currency('meduse_parts_price');
    $element->setLabel('Pořizovací náklady');
    $element->setRequired(FALSE);
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('meduse_parts_product');
    $element->setLabel('Produkt');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_parts_hs');
    $element->setLabel('HS code');
    $element->setAttrib('readonly', 'readonly');
    $element->setOrder(++$order);
    $this->addElement($element);


    $element = new Meduse_Form_Element_Button('hs_change');
    $element->setLabel('Změnit HS code');
    $element->setAttrib('class', 'btn btn-success');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_parts_cover_cost');
    $element->setLabel('Krycí náklad [%]');
    $element->setValue('');
    $element->setAttrib('disabled', 'disabled');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('meduse_parts_cover_cost_default');
    $element->setLabel('Vložit výchozí');
    $element->setValue('y');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('meduse_parts_set');
    $element->setLabel('Set součástí');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('meduse_parts_semifinished');
    $element->setLabel('Polotovar');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('Odeslat');
    $element->setOrder(++$order);
    $this->addElement($element);

    parent::init();
  }

}