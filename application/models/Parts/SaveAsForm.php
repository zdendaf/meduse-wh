<?php
class Parts_SaveAsForm extends Meduse_FormBootstrap {

  protected ?string $nameFromExisting = null;
  protected ?string $descriptionFromExisting = null;

  public function __construct($options = null)
  {
    if (is_array($options) && isset($options['from']) && is_a($options['from'], Parts_Part::class)) {
      $this->nameFromExisting = $options['from']->getName();
      $this->descriptionFromExisting = $options['from']->getDescription();
      unset($options['from']);
    }
    parent::__construct($options);
  }


  public function init()
    {
        $this
          ->setAttribs([
            'id' => 'save_as_form',
            'enctype' => 'multipart/form-data',
            'class' => 'form',
            ])
			    ->setMethod(Zend_Form::METHOD_POST);
        
        $element = new Meduse_Form_Element_Text('meduse_parts_id');
		$element->setRequired(true);
		$element->setLabel('ID');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_parts_name');
		$element->setRequired(true);
		$element->setLabel('Název');
    if ($this->nameFromExisting) {
      $element->setValue('kopie ' . $this->nameFromExisting);
    }
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_parts_desc');
		$element->setLabel('Popis');
      if ($this->descriptionFromExisting) {
        $element->setValue('kopie ' . $this->descriptionFromExisting);
      }
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);

        parent::init();
	}
}
