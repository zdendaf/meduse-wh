<?php
/**
 * Formular pro zmenu mnozstvi soucasti ve vyrobe
 */
class Parts_ProductionsForm extends Meduse_FormBootstrap {
    
	public function init()
	{
		$this->setAttribs(array('class' => 'form'));
		$this->setMethod(Zend_Form::METHOD_POST);

		$element = new Meduse_Form_Element_DatePicker(
			'meduse_production_ordering',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->addValidator(new Zend_Validate_Date('mm.DD.yyyy'));
		$element->setLabel('Datum zadání');
		$this->addElement($element);

		$element = new Meduse_Form_Element_DatePicker(
			'meduse_production_delivery',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->addValidator(new Zend_Validate_Date('mm.DD.yyyy'));
		$element->setLabel('Datum dodání');
		$this->addElement($element);


		$element = new Zend_Form_Element_Text('meduse_production_amount');
		$element->setRequired(true);
		$element->setLabel('Množství');
		$this->addElement($element);

		$element = new Zend_Form_Element_Submit('Odeslat');
		$this->addElement($element);

		parent::init();
    }

}
