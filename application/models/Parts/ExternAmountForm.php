<?php
class Parts_ExternAmountForm extends Meduse_FormBootstrap {

    public function init(){
		$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'));

		$element = new Meduse_Form_Element_Text('meduse_part_amount');
		$element->setLabel('Počet');
		$this->addElement($element);

		$element = new Zend_Form_Element_Hidden('meduse_part_id');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);
    
    parent::init();
    }

}
