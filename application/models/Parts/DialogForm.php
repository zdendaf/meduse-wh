<?php

  /**
   * Modal form pro zmenu poctu soucasti skladu.
   *
   * @author Zdenek
   */
  class Parts_DialogForm extends Zend_Form {

    public function init() {
      
      $eOperation = new Zend_Form_Element_Select('operation');
      $eOperation
        ->setLabel('Operace')
        ->setRequired(true)
        ->setMultiOptions(array(
          'reduce' => 'Vyřadit',
          'insert' => 'Vložit',
          'insert-noSub' => 'Vložit bez odečtu podsoučástí',
        ));
      
      $eAmount = new Zend_Form_Element_Text('amount');
      $eAmount
        ->setLabel('Počet [ks]')
        ->setAttrib('id', 'email') // pozustatek, proc?
        ->setRequired(true)
        ->addValidator(new Zend_Validate_Int)
        ->addValidator(new Zend_Validate_GreaterThan(0));
      
      $eWarehouse = new Zend_Form_Element_Select('wh');
      $eWarehouse
        ->setLabel('Sklad')
        ->addMultiOption('base', 'centrální');
      $db = Zend_Registry::get('db');
      $rows = $db->fetchAll($db->select()->from('extern_warehouses'));
      foreach ($rows as $row) {
        $eWarehouse->addMultiOption($row['id'], $row['name']);
      }
      
      $ePart = new Zend_Form_Element_Hidden('part');
      
      $eSubmit = new Zend_Form_Element_Hidden('Odeslat');
      $eSubmit->setValue('Odeslat');
      
      $this
        ->setAction('/parts/operations')
        ->addElement($eWarehouse)
        ->addElement($eOperation)
        ->addElement($eAmount)
        ->addElement($ePart)
        ->addElement($eSubmit);
      
      parent::init();
    }
  }
