<?php
/**
 * Formular pro zmenu mnozstvi soucasti ve vyrobe
 */
class Parts_ProductionsHeaderForm extends Meduse_FormBootstrap {

    public function __construct ($options = null){
        $this->setAttribs(array('class' => 'form'));

		$element = new Meduse_Form_Element_DatePicker(
			'meduse_production_date',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->setRequired(true);
		$element->setLabel('Datum zadání');
		$this->addElement($element);

		$element = new Meduse_Form_Element_DatePicker(
			'meduse_production_deadline',
			array(
				'jQueryParams' => array(
					'dateFormat' => 'dd.mm.yy'
				)
			)
		);
		$element->setRequired(true);
		$element->setLabel('Datum dodání');
		$this->addElement($element);
        
		$element = new Zend_Form_Element_Submit('Odeslat');
		$this->addElement($element);

		parent::init();
    }

}
