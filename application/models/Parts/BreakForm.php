<?php
class Parts_BreakForm extends Meduse_FormBootstrap {
	
    public function init()
    {
		$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			->setMethod(Zend_Form::METHOD_POST);

		$element = new Meduse_Form_Element_Text('meduse_break_amount');
        $element->setRequired(true);
        $element->setLabel('Počet');
        $element->setAttrib('class', 'span1');
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Submit('Odeslat');
        $this->addElement($element);
		
		parent::init();
    }
	
}