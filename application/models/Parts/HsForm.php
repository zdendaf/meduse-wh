<?php

  class Parts_HsForm extends Meduse_FormSkove {

    public function init() {

      /** @var \Zend_Db_Adapter_Pdo_Mysql $db */
      $db = Zend_Registry::get('db');
      $sql = "SELECT * FROM hs_codes WHERE avail_products = 'y' ORDER BY weight, code";
      $hsCodes = $db->query($sql)->fetchAll(Zend_Db::FETCH_ASSOC);

      $element = new Zend_Form_Element_Radio('hs_code');
      if ($hsCodes) {
        foreach ($hsCodes as $data) {
          $value = '<div class="text"><strong>' . $data['code'] . ':</strong> '
            . $data['description'] . '<br><small><strong>' . $data['note'] . '</strong></small></div>';
          $element->addMultiOption($data['code'], $value);
        }
      }
      $this->addElement($element);

      parent::init();
    }
  }

  