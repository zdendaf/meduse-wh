<?php
/**
 * Formular pro typovani externich skladu
 * @author cubanec
 *
 */
class Parts_OperationForm extends Meduse_FormBootstrap {

    public function init()
    {
        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('post');

		$this->setAttrib('enctype', 'multipart/form-data');

		$element = new Meduse_Form_Element_Text('meduse_operation_name');
		$element->setRequired(true);
		$element->setLabel('Název');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_operation_price');
		$element->setLabel('Cena');
		$element->setRequired(false);
		$this->addElement($element);
                
    $element = new Meduse_Form_Element_Text('meduse_operation_time');
		$element->setLabel('Minuty');
    $element->setAttrib('title', 'Vyplňte čas v minutách potřebný pro tuto operaci na 1 součást.');
    $element->setRequired(false);
		$this->addElement($element);

		$element = new Meduse_Form_Element_SelectChosen('meduse_operation_producer');
		$element->setRequired(false);
        $element->addValidator(new Zend_Validate_NotEmpty());
		$element->addMultiOption(NULL, '-- žádný --');
		$element->addMultiOptions(Zend_Registry::get('db')->fetchPairs('SELECT id,name FROM producers ORDER BY name'));
		$element->setLabel('Zpracovatel');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);
        
        parent::init();
    }

}