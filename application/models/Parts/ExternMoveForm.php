<?php
  /**
   * Formular pro presouvani soucasti do externiho skladu
   * @author cubanec
   *
   */
  class Parts_ExternMoveForm extends Meduse_FormBootstrap {
	
    private $_move_back;
    private $_db = null;
    
    public function init() {
      // set db connection
      $this->_db = Zend_Registry::get('db');

      $this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
        ->setMethod(Zend_Form::METHOD_POST);

      $element = new Meduse_Form_Element_Text('meduse_extern_part');
      $element->setRequired(true);
      $element->setLabel('ID');
      $this->addElement($element);

      //kolekce
      $rows = $this->_db->fetchAll($this->_db->select()->from('extern_warehouses'));
      $element = new Meduse_Form_Element_Select('meduse_extern_warehouse');
      $element->setLabel('Externí sklad');
      $element->setRequired(true);
      $element->addMultiOption('null', '- vybrat -');
      foreach($rows as $row){
        $element->addMultiOption($row['id'], $row['name']);
      }
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('meduse_extern_amount');
      $element->setRequired(true);
      $element->setLabel('Počet');
      $this->addElement($element);    	

      $element = new Meduse_Form_Element_Submit('Odeslat');
      $this->addElement($element);

      $this->_move_back = false;

      parent::init();
    }
    
    public function isValid($data){
    	$valid = parent::isValid($data);
    	
    	if($data['meduse_extern_warehouse'] == 'null') {
    		$this->meduse_extern_warehouse->addError('Sklad musí být určen.');
    		$valid = false;
    	}
    	
    	$parts_warehouse = new Table_PartsWarehouse();
    	
      $base_amount = $parts_warehouse->getBaseAmount($data['meduse_extern_part']);
      if(!$this->_move_back && (int) $data['meduse_extern_amount'] > $base_amount){
        $this->meduse_extern_amount->addError("Skladem je $base_amount ks ".$data['meduse_extern_part'].". Více nelze přesunout.");
        $valid = false;
      } elseif ($valid && $this->_move_back) {
        $pwe = new Table_PartsWarehouseExtern();
        $extern_amount = $pwe->getAmount($data['meduse_extern_warehouse'],$data['meduse_extern_part']);
        if((int)$extern_amount < (int)$data['meduse_extern_amount']){
          $this->meduse_extern_amount->addError("V externím skladu je $extern_amount ks ".$data['meduse_extern_part']);
          $valid = false;
        }
      }
      return $valid;
    }
    
    public function setMoveback(){
    	$this->_move_back = true;
    }	
  }