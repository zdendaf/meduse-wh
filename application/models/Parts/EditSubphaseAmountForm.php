<?php
class Parts_EditSubphaseAmountForm extends Meduse_FormBootstrap {

	public function init()
    {
        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('post');

        $element = new Meduse_Form_Element_Text('meduse_subphase_amount');
        $element->setRequired(true);
        $element->setLabel('Počet');
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Submit('Odeslat');
        $this->addElement($element);

        parent::init();
	}
}
