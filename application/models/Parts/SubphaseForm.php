<?php
class Parts_SubphaseForm extends Meduse_FormBootstrap {
	
    public function init()
    {
        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('post');
        
		$this->setAttrib('enctype', 'multipart/form-data');
		
		$element = new Meduse_Form_Element_Text('meduse_subphase_name');
		$element->setRequired(true);
		$element->setLabel('Název');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Textarea('meduse_subphase_desc');
		$element->setLabel('Poznámka');
		$element->setAttrib("cols", 29);
		$element->setAttrib("rows", 6);
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Text('meduse_subphase_amount');
		$element->setLabel('Počet');
		$this->addElement($element);
		
		$element = new Zend_Form_Element_Hidden('id_parts');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);
        
        parent::init();
    }
	
}
