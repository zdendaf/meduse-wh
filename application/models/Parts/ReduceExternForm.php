<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com> 
   */
  class Parts_ReduceExternForm extends Meduse_FormBootstrap {
    
    public function init() {

      $this->setAttrib('class', 'form');
      $element = new Meduse_Form_Element_Text('meduse_reduce_amount');
      $element->setRequired(true);
      $element->setLabel('Počet');
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Submit('Odeslat');
      $this->addElement($element);
      
      parent::init();
    }
    
  }

  