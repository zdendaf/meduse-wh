<?php

class Parts_Exceptions_InActiveOrders extends Exception {
	public array $orders;
    public function __construct(array $orders, $message = '', $code = 0, Throwable $previous = null)
    {
        $this->orders = $orders;
        parent::__construct($message, $code, $previous);
    }
}
