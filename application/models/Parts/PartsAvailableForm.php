<?php
class Parts_PartsAvailableForm extends Meduse_Form {
	
    public function __construct ($options = null){
        parent::__construct('parts_box', 'Dostupnost součásti', $options);
        
		$this->setAttrib('enctype', 'multipart/form-data');

		$element = new Meduse_Form_Element_Text('part_id');
		$element->setRequired(true);
		$element->setLabel('ID');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Submit('Zjistit');
		$this->addElement($element);
    }
	
}