<?php
class Parts_CategoryForm extends Meduse_FormBootstrap {

    private int $wh = 1;

    public function __construct($options = null) {
      if ($options && is_array($options)) {
        $this->wh = (int) ($options['wh'] ?? 1);
        unset($options['wh']);
      }
      parent::__construct($options);
    }

  /**
   * @throws Zend_Form_Exception
   */
  public function init()
    {
        $this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			 ->setMethod(Zend_Form::METHOD_POST);
        
        $element = new Meduse_Form_Element_Text('category_name');
        $element->setLabel('Název');
        $element->setRequired(true);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('category_parent');
        $element->addMultiOption(NULL, ' - žádná -');
        $element->addMultiOptions(Zend_Db_Table::getDefaultAdapter()->fetchPairs("SELECT id, name FROM parts_ctg WHERE parent_ctg IS NULL and id_wh_type = {$this->wh} ORDER BY ctg_order"));
        $element->setLabel('Skupina');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('group');
        $element->setValue(true);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Submit("Odeslat");
        $this->addElement($element);
        
        parent::init();
    }

}
