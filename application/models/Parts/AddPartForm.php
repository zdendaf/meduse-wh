<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Parts_AddPartForm extends Meduse_FormBootstrap {

    protected $partId = NULL;
    protected $amount = NULL;
    protected $superAmount = NULL;

    public function __construct($options = null) {
      $this->partId = $options['data']['id'];
      $this->amount = $options['data']['amount'];
      $this->superAmount = $options['data']['super_amount'];
      unset($options['data']);
      parent::__construct($options);
    }

    public function init() {

      $this->setAttrib('class', 'form');
      $this->setName('parts_box');
      $this->setLegend('Přidávání ' . $this->partId . ' na sklad.');

      $element = new Meduse_Form_Element_Text('meduse_add_amount');
      $element->setRequired(true);
      $element->setLabel('Počet');
      $element->setValue($this->superAmount * $this->amount);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('ok');
      $element->setLabel('Naskladnit');
      $this->addElement($element);

      parent::init();
    }

  }

