<?php
class Parts_FilterForm extends Meduse_FormBootstrap {

    public function init()
    {
        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('get');

        $db = Zend_Registry::get('db');

        //sklad
        $rows = $db->fetchAll($db->select()->from('extern_warehouses'));
        $element = new Meduse_Form_Element_Select('wh');
        $element->setLabel('Sklad');
        $element->addMultiOption('null', '- všechny -');
        $element->addMultiOption('base', 'centrální');
        foreach($rows as $row){
        $element->addMultiOption($row['id'], $row['name']);
        }
        $this->addElement($element);

        //kolekce
        $rows = $db->fetchAll($db->select()->from('collections'));
        $element = new Meduse_Form_Element_MultiCheckbox('coll');
        $element->setLabel('Kolekce');
        foreach($rows as $row){
          $element->addMultiOption($row['id'], " ".$row['abbr']);
        }
        $this->addElement($element);

        // Kategorie.
        $element = new Meduse_Form_Element_Categories(['name' => 'ctg', 'type' => 'pipes', 'all' => true]);
        $this->addElement($element);

        //Dodavatel
        $rows = $db->fetchAll($db->select()->from('producers')->where("EXISTS(SELECT * FROM producers_parts WHERE id_producers = producers.id)")->where('id_wh_type = ?', Parts_Part::WH_TYPE_PIPES)->order('name'));
        $element = new Meduse_Form_Element_Select('producer');
        $element->setLabel('Dodavatel');
        $element->addMultiOption('null', '- všichni -');
        foreach($rows as $row){
          $element->addMultiOption($row['id'], $row['name']);
        }
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('name');
        $element->setLabel('Název');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('id');
        $element->setLabel('ID');
        $this->addElement($element);

        //pouze produkty
        $element = new Meduse_Form_Element_Checkbox('prod');
        $element->setLabel("Pouze produkty");
        $this->addElement($element);

        //pouze nesložené
        $element = new Meduse_Form_Element_Checkbox('no_multiparts');
        $element->setLabel("Pouze nesložené");
        $this->addElement($element);

        $element = new Meduse_Form_Element_Submit('Hledat');
        $element->setValue('Hledat');
        $element->setAttrib('class', 'btn btn-primary');
        $this->addElement($element);

        parent::init();
    }

}