<?php

class Parts_PartsAvailable {

    protected $_subparts_need_arr = null;
    protected $_subparts_wh_arr = null;
    protected $_idProduct = null;
    public    $product_amount_available = 0;


    /**
     * Set part/product id
     */
    public function __construct($id) {
        $this->_idProduct = $id;
    }

    /**
     * Check product amount
     * 
     * @return int $product_amount_available 
     */
    public function checkProductAmount() {
        /**
         * Set subparts
         */
        $this->_setSubpartsArray();
        
        /**
         * Set warehouse parts
         */
        $this->_setWarehousePartsArray();
        
        /**
         * Check amount of available product
         */
        $this->_checkAvailability();
    
        
        #DEBUG var_dump($this->_subparts_need_arr);
        #DEBUG var_dump($this->_subparts_wh_arr);
        #DEBUG var_dump($this->product_amount_available);

        /**
         * Return amount
         */
        return (int)$this->getProductAmountAvailable();
    }
    
    /**
     * Set subparts
     */
    private function _setSubpartsArray()
    {
        $subp = new Table_PartsSubparts(); 
        $subp->getAdapter();
        $subparts = $subp->fetchAll('id_parts = "' . $this->_idProduct . '"');
        $this->_subparts_need_arr = $subparts->toArray();
    }
    
    /**
     * Set warehouse parts
     */
    private function _setWarehousePartsArray() 
    {
        foreach($this->_subparts_need_arr as $partId) {
            $wh = new Table_PartsWarehouse();
            $wh->getAdapter();
            $this->_subparts_wh_arr[$partId['id']] = $wh->getPartAmountNumber($partId['id']);
        }
    }

    /**
     * Check amount of available product
     * recursive fnc
     */
    private function _checkAvailability() 
    {
        $availableProduct = true;
        
        foreach($this->_subparts_need_arr as $partId) {
            $newAmount = $this->_subparts_wh_arr[$partId['id']] - $partId['amount'];
            
            if($newAmount < 0) {
                $availableProduct = false;
            }
            else {
                $this->_subparts_wh_arr[$partId['id']] = $newAmount;
            }
            
            #DEBUG var_dump($newAmount);
        }
        
        // no errors, increase number of available product 
        // and check again until product no available
        if($availableProduct) {
            $this->product_amount_available++;
            $this->_checkAvailability();
        }
    }
    
    /**
     * Get amount of available product
     * 
     * @return int $product_amount_available
     */
    public function getProductAmountAvailable()
    {
        return (int)$this->product_amount_available;
    }
    
    /**
     * Get part summary
     */
    public function getPartsSummary()
    {
        return $this->_subparts_wh_arr;
    }
}
