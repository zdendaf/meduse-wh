<?php
  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Parts_MultiEditAddForm extends Zend_Form {
    
    public function init() {
      
      $this->setAction('/multi-edit/step3');
      $this->setMethod(Zend_Form::METHOD_POST);
      
      $element = new Zend_Form_Element_Hidden('type');
      $element->setValue('add-part');
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Text('part');
      $element
        ->setLabel('ID součásti')
        ->setRequired(true);
      $this->addElement($element);
      
      $element = new Meduse_Form_Element_Text('amount');
      $element
        ->setLabel('Počet')
        ->setRequired(true)
        ->addValidator(new Zend_Validate_Int());
      $this->addElement($element);
      
      parent::init();
      
    }
  }

