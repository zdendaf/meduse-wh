<?php


class Parts_BatchThrowingConfirmForm extends Meduse_FormBootstrap {

  protected $parts = [];

  public function __construct($options = NULL) {
    $this->parts = $options['parts'];
    unset($options['parts']);
    parent::__construct($options);
  }

  public function init() {
    $this->setAttribs(['enctype' => 'multipart/form-data', 'class' => 'form'])
      ->setAction('/parts/batch-throwing')
      ->setMethod(Zend_Form::METHOD_POST);

    $element = new Meduse_Form_Element_Submit('Vyřadit');
    $this->addElement($element);

    $element = new Zend_Form_Element_Hidden('phase');
    $element->setValue('perform');
    $this->addElement($element);

    foreach ($this->parts as $row) {
      if ($row['throw_amount']) {
        $this->addPart($row['id_parts'], $row['throw_amount']);
      }
    }

    parent::init();

    $this->setDecorators([]);
  }


  public function addPart($partId, $amount) {
    $element = new Zend_Form_Element_Hidden('amount_' . $partId);
    $element->setValue($amount);
    $element->setDecorators([]);
    $this->addElement($element);
  }
}