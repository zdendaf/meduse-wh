<?php
/**
 * Mohloby primo rozsirovat tridu zend radku
 */
class Parts_Part {

	const WH_TYPE_PIPES = 1;   // dýmky
	const WH_TYPE_TOBACCO = 2; // tabáky

	const WH_MEASURE_UNIT = 'unit'; // mirou jsou jednotky (kusy)
	const WH_MEASURE_WEIGHT = 'weight'; // mirou je vaha

  const OPERATIONS_RETURN_TYPE_COST = 1;
  const OPERATIONS_RETURN_TYPE_TIME = 2;
  const OPERATIONS_RETURN_TYPE_BOTH = 3;

  /**
   * Maximalni hloubka zanoreni pri hlubokem prepoctu operaci
   */
  const OPERATIONS_DEEP_LEVEL = 20;

	protected $_subparts = NULL;

	protected $_part_row;
  protected $_part_weight_row = NULL;

	/**
	 * ma smysl jen u podsoucasti
	 * @var int
	 */
	protected $_amount;

	public function __construct($id, $amount = 1, $id_wh_type = NULL){
		$tParts = new Table_Parts();
		if(!($row = $tParts->fetchRow($tParts->select()->where("id = ?", $id)))){
			throw new Exception("Pokus vytvořit součást s id=$id se nezdařil. Id nenalezeno v databázi.");
		} else {
			$this->_part_row = $row;
		}

    if ($id_wh_type && $id_wh_type != $this->_part_row->id_wh_type) {
      throw new Parts_Exceptions_BadWH('Součást ' . $id . ' nepatří do tohoto skladu.');
    }

    $tPartsWeight = new Table_PartsWeight();
    $this->_part_weight_row = $tPartsWeight->fetchRow(
      $tPartsWeight->select()->where('id_parts = ?', $id)
    );

		$this->_amount = $amount;
		if($this->_part_row->multipart == 'n'){
			$this->_subparts = array();
		}
	}

	public function getRow(){
		return $this->_part_row;
	}

	public function isProduct(){
		return $this->_part_row->product == 'y' ? true : false;
	}

	public function isSemifinished(){
		return $this->_part_row->semifinished == 'y' ? true : false;
	}

	public function getName(){
		return $this->_part_row->name;
	}

	public function getID(){
		return $this->_part_row->id;
	}

	public function getDesc(){
		return $this->_part_row->description;
	}

	public function getDescription() {
	  return $this->getDesc();
  }

	public function getVersion(){
		return $this->_part_row->version;
	}

	public function getPrice($withOperations = TRUE){
	  return $withOperations ? $this->_part_row->price : $this->_part_row->price - $this->_part_row->operations_cost;
	}

  /**
   * Vraci vyrobni naklady
   *
   * @param bool $cover vcetne kryciho nakladu
   *
   * @return float
   */
  public function getCost(bool $cover = TRUE) {
    $cover_cost = $this->getCoverCost();
    return $cover ? $this->_part_row->price * (1 + $cover_cost / 100) : $this->_part_row->price;
  }


  public function getCoverCost($percent = FALSE, $global = TRUE) {
    if (is_null($this->_part_row->cover_cost) && $global) {
      $table = new Table_ApplicationCoverCost();
      $cover_cost = $table->getCoverCost();
    } else {
      $cover_cost = $this->_part_row->cover_cost;
    }
    if (!$global && is_null($cover_cost)) {
      return '';
    } else {
      return $percent ? round($cover_cost / 100, 2) : round($cover_cost, 2);
    }
  }

  public function getPurchasePrice() {
    return $this->_part_row->price - $this->getOperationSum();
  }

  public function getWeight() {
    return isset($this->_part_weight_row) ?
      $this->_part_weight_row->weight : 0;
  }

  public function getHS() {
    return $this->_part_row->hs;
  }

  public function getSKU() {
    return $this->_part_row->sku;
  }

  public function setWeight($weight, $ekokom_ctg = null) {
    $tPartsWeight = new Table_PartsWeight();
    $tPartsWeight->insertUpdate(array(
      'id_parts' => $this->getID(),
      'weight' => $weight,
      'id_ekokom_ctg' => $ekokom_ctg,
    ));
  }

  public function getTotalWeight() {
    $weight = $this->getWeight();
    $subparts = $this->getSubparts();
    if ($subparts) {
      foreach ($subparts as $part) {
        $weight += $part->getWeight() * $part->getAmount();
      }
    }
    return $weight;
  }

  public function getEkokomCategory() {
    return is_null($this->_part_weight_row) ?
      null : $this->_part_weight_row->id_ekokom_ctg;
  }

  public function getEkokomCategoryName() {
    if ($this->_part_weight_row) {
    $db = Zend_Registry::get('db');
    return $db->select()
      ->from('ekokom_ctg', array('name'))
      ->where('id = ?', $this->_part_weight_row->id_ekokom_ctg)
      ->query()->fetchColumn();
    } else {
      return 'nezařazeno';
    }
  }

  public function getEkokomData(&$data) {
    $subparts = $this->getSubparts();
    if ($subparts) {
      foreach ($subparts as $part) {
        $part->getEkokomData($data);
      }
    }
    $ctg = $this->getEkokomCategory();
    if ($ctg) {
      $weight = (int) $this->getWeight();
      $name = $this->getEkokomCategoryName();
      $data[$ctg] = isset($data[$ctg]) ? $data[$ctg]['weight'] + $weight
        : array('weight' => $weight, 'name' => $name);
    }
  }


	public function getEndPriceAuto($idPricelist = null){
                // is product
                if($this->_part_row->product == 'y') {
                    //return $this->_part_row->end_price_eur;
                    return $this->getEndPrice($idPricelist);
                }
                // is part
		return $this->getPrice();
	}

  /**
   * @throws \Parts_Exceptions_NoPricelist
   */
  public function getEndPrice($idPricelist = NULL, $addVat = FALSE) {
        if (is_null($idPricelist)) {
            throw new Parts_Exceptions_NoPricelist('Musí být zadáno ID ceníku.');
        }
        $tPrices = new Table_Prices();
        $price = $tPrices->getProductPrice($this->getID(), $idPricelist);
        $vat = $addVat ? 1 + Utils::getActualVat() : 1;
        return $price ? $price * $vat : NULL;
	}

  /**
   * Vraci ceny a marze.
   * @param boolean $cover zahrnout do nakladu kryci naklady
   * @return array
   */
  public function getProductPrices($cover = TRUE) {
      // kdyz neni produkt, vratime null
      if (!$this->isProduct()) {
          return false;
      }
      // jinak budeme vrcet pole cen
      $tPrices = new Table_Prices();
      $prices = $tPrices->getProductPrices($this->getID());
      $tRate = new Table_ApplicationRate();
      $rate = $tRate->getRate();
      $cost = $this->getCost($cover) / $rate;
      foreach ($prices as $key => $price) {
        $margin = $price['price'] - $cost;
        $prices[$key]['margin_abs'] = round($margin, 2);
        $prices[$key]['margin'] = $cost == 0 ? 0 : round($margin / $cost * 100, 2);
      }
      return $prices;
  }

  public function getEndPriceEuro($idPricelist = null) {
      return $this->getEndPrice($idPricelist);
  }

	/**
	 * Aktualizuje cenu vsem soucastem, ve kterych figuruje dana soucast
	 */
	public function refreshOperations($deep = FALSE, $level = Parts_Part::OPERATIONS_DEEP_LEVEL, &$visited = array()){

    if (in_array($this->getID(), $visited)) {
      return;
    }

		//aktualizace ceny soucasti
		$this->computeOperations($deep, $level, $visited);

    //aktualizace ceny soucasti, vsem soucastem, ve kterych je dana soucast obsazena
    $select = Zend_Registry::get('db')->select();
    $select->from('parts_subparts', array('id_multipart'));
    $select->where('id_parts = ?', $this->getID());
    $select->group('id_multipart');

    $result = $select->query(Zend_Db::FETCH_ASSOC);
    while ($row = $result->fetch()) {
      $superPart = new Parts_Part($row['id_multipart']);
      $superPart->refreshOperations();
      unset($superPart);
    }
	}

	/**
	 * Aktualizuje cenu vsem soucastem, ve kterych figuruje dana soucast
	 */
	public function refreshPrice(){
		$db = Zend_Registry::get('db');

		//aktualizace ceny soucasti
		$this->computePrice();

		//aktualizace ceny soucasti, vsem soucastem, ve kterych je dana soucast obsazena
		$rows = $db->fetchAll("SELECT id_multipart FROM parts_subparts WHERE id_parts = ? GROUP BY id_multipart", $this->_part_row->id);
		if(count($rows) > 0){
			foreach ($rows as $row) {
				$superPart = new Parts_Part($row['id_multipart']);
				$superPart->refreshPrice();
				unset($superPart);
			}
		}
	}

	/**
	 * Přepočítá cenu součásti a všech jejich podsoučástí (ale pouze v prvním levelu).
   * Cenu součásti vrátí a to včetně nákladů na operace.
	 * @return float
	 */
	public function computePrice() {
		$price = $this->getProducerPrice();
		if ($this->isMultipart()) {
			$rows = Zend_Registry::get('db')->fetchAll('SELECT * FROM parts_subparts WHERE id_multipart = ?', $this->getID());
			foreach($rows as $row){
				$subpart = new Parts_Part($row['id_parts']);
				$price += $subpart->getPrice() * $row['amount']; //scita pouze 1lvl podsoucasti
			}
		}
    $operationsCost = $this->getOperationSum();
    $operationsTime = $this->getOwnOperationSum(self::OPERATIONS_RETURN_TYPE_TIME);

		$price += $operationsCost;

		$this->_part_row->price = $price;
    $this->_part_row->operations_cost = $operationsCost;
    $this->_part_row->operations_time = $operationsTime;
		$this->_part_row->save();

    return $price;
	}

  /**
   * Prepocita sumu operaci vcetne operaci podsoucasti.
   *
   * @param bool $deep
   *    (volitelne) Operace se pocitaji v celem podstromu rekurzi.
   *    Vychozi hodnota je FALSE.
   * @param int $level
   *    (volitelne) Maximalni pocet zanoreni/podsoucasti.
   *    Vychozi hodnota je 10.
   */
  public function computeOperations($deep = FALSE, $level = Parts_Part::OPERATIONS_DEEP_LEVEL, &$visited = array()) {
    if (in_array($this->getId(), $visited)) {
      return array(
        'cost' => $this->_part_row->operations_cost,
        'time' => $this->_part_row->operations_time,
      );
    }
    $operations = $this->getOwnOperationSum(Parts_Part::OPERATIONS_RETURN_TYPE_BOTH);
    if ($this->isMultipart()) {
      $select = Zend_Registry::get('db')->select();
      $select->from('parts_subparts', array('id_parts', 'amount'));
      $select->where('id_multipart = ?', $this->getID());
      $result = $select->query(Zend_Db::FETCH_ASSOC);
      while($row = $result->fetch()) {
        $subpart = new Parts_Part($row['id_parts']);
        if ($deep && $level > 1) {
          $subOperations = $subpart->computeOperations($deep, $level--, $visited);
        }
        else {
          $subOperations = $subpart->getTotalOperationSum(Parts_Part::OPERATIONS_RETURN_TYPE_BOTH);
        }
        $operations['cost'] = $operations['cost'] + $subOperations['cost'] * $row['amount'];
        $operations['time'] = $operations['time'] + $subOperations['time'] * $row['amount'];
        unset($subpart);
      }
    }
    $this->_part_row->operations_cost = $operations['cost'];
    $this->_part_row->operations_time = $operations['time'];
    $this->_part_row->save();
    $visited = array_merge($visited, array($this->getId()));
    return $operations;
  }


  /**
   * Vraci celkove navyseni ceny operacemi se soucastí (zpracovani, povrchove
   * upravy atd.). Hodnota je brána z tabule parts_operations.
   *
   * @return float
   * @throws \Zend_Exception
   */
	public function getOperationSum(): float {
		return Zend_Registry::get('db')
      ->fetchOne('SELECT IFNULL(SUM(price), 0) FROM parts_operations WHERE id_parts = ?', $this->_part_row->id);
	}


  /**
   * Vraci cenu a/nebo cas vlastnich Meduse operaci.
   */
  public function getOwnOperationSum($returnType = self::OPERATIONS_RETURN_TYPE_COST) {
    $select = Zend_Registry::get('db')->select();
    $select->from('parts_operations', array(
      'cost' => new Zend_Db_Expr('IFNULL(SUM(price), 0)'),
      'time' => new Zend_Db_Expr('IFNULL(SUM(time), 0)'),
    ));
    $select->where('time <> ?', 0);
    $select->where('id_parts = ?', $this->getID());

    $result = $select->query(Zend_Db::FETCH_ASSOC)->fetch();
    if ($result) {
      $operations = $result;
    }
    else {
      $operations = array('cost' => 0, 'time' => 0);
    }
    switch ($returnType) {
      case self::OPERATIONS_RETURN_TYPE_COST:
        return $operations['cost'];
      case self::OPERATIONS_RETURN_TYPE_TIME:
        return $operations['time'];
      case self::OPERATIONS_RETURN_TYPE_BOTH:
        return $operations;
    }
  }

  public function getTotalOperationSum($returnType = self::OPERATIONS_RETURN_TYPE_COST) {
    switch ($returnType) {
      case self::OPERATIONS_RETURN_TYPE_COST:
        return $this->_part_row->operations_cost;
      case self::OPERATIONS_RETURN_TYPE_TIME:
        return $this->_part_row->operations_time;
      case self::OPERATIONS_RETURN_TYPE_BOTH:
        return array(
          'cost' => $this->_part_row->operations_cost,
          'time' => $this->_part_row->operations_time,
        );
    }
  }

  /**
   * Vraci pracnost soucasti, tj. pomer mezi vlastnimi a celkovymi naklady.
   *
   * @param type $percent
   *    (volitelne) vysledna hodnota jako procento, default je TRUE.
   * @return float
   */
  public function getLaborIntensive($percent = TRUE) {
    if ($this->_part_row->price == 0) {
      return 0;
    }
    else {
      $p = $this->_part_row->operations_cost / $this->_part_row->price;
      return $percent ? $p * 100 : $p;
    }
  }

	public function setPrice($price){
		$this->_part_row->price = $price;
		$this->_part_row->save();
	}

  public function setProducerPrice($price) {
    $table = new Table_ProducersParts();
    $table->update(array('price' => $price),
      'id_parts = ' . $table->getAdapter()->quote($this->getID()));
  }

  /**
   * Vrátí cenu od dodavatele tak, jak je uložená v DB v tabulce producers_parts.
   */
  public function getProducerPrice() {
    $table = new Table_ProducersParts();
    $select = $table->select()->from($table, 'price')
      ->where('id_parts = ?', $this->getID());
    $price = $select->query()->fetchColumn();
    return $price? $price : 0;
  }

	/**
	 * DEBUGOVACI funkce
	 * zavolano jen 22.8.2011
	 *
	 */
	public static function computeAllFirst(){
		//vypocet operaci poprve
		ini_set("max_execution_time", 0);

		//vypocet cen
		$rows = Zend_Registry::get('db')->fetchAll("SELECT id FROM parts WHERE old_price IS NULL ORDER BY multipart DESC");
		foreach($rows as $row){
			$part = new Parts_Part($row['id']);
			$part->refreshPrice();
		}
	}

	/**
	 * DEBUGOVACI funkce
	 * prvnotni pocitani cen soucasti + plneni tabulky operaci
	 * zavolano jen jednou 22.8.2011
	 */
	public function computeOperationsFirst(){
		if(count($this->getSubparts()) == 1 && preg_match("/[A-Za-z0-9]*x$/", $this->_subparts[0]->getId())){
			//soucast je slozena jen z jedne podsoucasti
			$subPrice = ( $this->_subparts[0]->getPrice() );
			$price = $this->_part_row->price;

			$operationPrice = $price - $subPrice;

			if($operationPrice > 0) {
				echo $this->_part_row->id." = $price, ".$this->_subparts[0]->getID()." = $subPrice  ===>  $operationPrice<hr />";
				Zend_Registry::get('db')->insert("parts_operations", array(
					"id_parts" => $this->_part_row->id,
					"name" => "Zpracování",
					"price" => $operationPrice,
					"inserted" => new Zend_Db_Expr("NOW()")
				));

			}

		}
	}

  /**
   * vraci id prvni kolekce
   * @return int nebo null
   */
	public function getCollection(){
    $collections = $this->getCollections();
    return $collections ? (int) $collections[0]['id'] : null;
	}


  /**
   * vraci pole kolekci, do kterych soucast patri
   * @return array(id, name, abbr)
   */
  public function getCollections() {
    $db = Zend_Registry::get('db');
    $select = new Zend_Db_Select($db);
    $select
      ->from(array('pc' => 'parts_collections'),
        array('id' => 'id_collections'))
      ->joinLeft(array('co' => 'collections'),
        'co.id = pc.id_collections', array('name', 'abbr'))
      ->where('pc.id_parts = ?', $this->getID());
    return $select->query()->fetchAll();
  }

  /**
   * vraci nazev prvni kolekce
   * @return string
   */
	public function getCollectionName() {
    $collections = $this->getCollections();
    return $collections ? $collections[0]['name'] : '';
	}

  /**
   * vraci zkratku prvni kolekce
   * @return string
   */
  public function getCollectionAbbr(){
    $collections = $this->getCollections();
    return $collections ? $collections[0]['abbr'] : '';
	}


	/**
	 * Vraci pole Parts_Part
	 * @return array(Parts_Part)
	 */
  public function getSubparts($orderById = FALSE) {
    $this->_subparts = NULL;
    $tPartsSubparts = new Table_PartsSubparts();
    $subParts = $tPartsSubparts->select();
    $subParts->setIntegrityCheck(FALSE);
    $subParts->from(['s' => 'parts_subparts'])
      ->joinLeft(['p' => 'parts'], "s.id_parts = p.id", [])
      ->joinLeft(['c' => 'parts_ctg'], "p.id_parts_ctg = c.id", [])
      ->joinLeft(['parentC' => 'parts_ctg'], "c.parent_ctg = parentC.id", [])
      ->where("id_multipart = ?", $this->_part_row->id);

    if ($orderById == TRUE) {
      $subParts->order("s.id_parts ASC");
    }
    else {
      $subParts->order("c.name ASC");
    }

    if ($this->_part_row->multipart == 'y') {
      $this->_subparts = [];
      $rows = $tPartsSubparts->fetchAll($subParts);
      foreach ($rows as $tmp) {
        if ($this instanceof Pipes_Parts_Part) {
          $this->_subparts[] = new Pipes_Parts_Part($tmp->id_parts, $this->_amount * $tmp->amount);
        }
        elseif ($this instanceof Tobacco_Parts_Part) {
          $this->_subparts[] = new Tobacco_Parts_Part($tmp->id_parts, $this->_amount * $tmp->amount);
        }
        else {
          $this->_subparts[] = new Parts_Part($tmp->id_parts, $this->_amount * $tmp->amount);
        }
      }
    }

    return $this->_subparts;
  }

  public function hasSubParts() {
    return (!empty($this->getSubparts()));
  }


	/**
	 * Used in MultiEdit
	 */
	public function getPartAmount($idSubPart) {
		$tPartsSubparts = new Table_PartsSubparts();
		$select = $tPartsSubparts->select();
		$select->from(array('s' => 'parts_subparts'))
      ->where("id_multipart = ?", $this->_part_row->id)
      ->where("id_parts = ?", $idSubPart);
		$row = $select->query(Zend_Db::FETCH_ASSOC)->fetch();
		if ($row) {
			return (int) $row['amount'];
		}
    else {
			return 0;
		}
	}

	public function getSubphases(){

		$subphases = array();

		$table_parts_subphases = new Table_PartsSubphases();
		$rows = $table_parts_subphases->fetchAll("id_parts LIKE '".$this->_part_row->id."'");
		foreach($rows as $tmp){
			$subphases[$tmp->id] = array(
				'id' => $tmp->id,
				'name' => $tmp->name,
				'description' => $tmp->description,
				'amount' => $tmp->amount,
			);
		}

		if(count($subphases) < 1) return false;

		return $subphases;

	}

  /**
   * Vrací množství součásti na skladě.
   * Pokud je zadáno datum, vrací množství na skladě k tomuto datu.
   *
   * @param \Meduse_Date|NULL $date
   * @throws Zend_Date_Exception
   *
   * @return int
   */
  public function getWHAmount(Meduse_Date $date = NULL) {
    $tPartsWarehouse = new Table_PartsWarehouse();
    $current = $tPartsWarehouse->getAmount($this->getID());
    if (is_null($date)) {
      return $current;
    }
    else {
      $now = new Meduse_Date();
      $changes = $this->getWHChanges($date, $now, TRUE);
      return $current - $changes['amount_add'] + $changes['amount_remove'];
    }
  }

	public function getAmount(){
		return $this->_amount;
	}

	public function getCtg(){
		return $this->_part_row->id_parts_ctg;
	}

  public function getSuperCtg() {
    $table = new Table_PartsCtg();
    return $table->getParentCategory($this->_part_row->id_parts_ctg);
  }

	public function getCtgName(){
		if($this->_part_row->id_parts_ctg === null){
			return '';
		}
		$parts_ctg = new Table_PartsCtg();
		if(!($row = $parts_ctg->find($this->_part_row->id_parts_ctg)->current())){
			throw new Exception("Neexistuje typ soucasti id=".$this->_part_row->id_parts_ctg." ");
		}

		if(!is_null($row['parent_ctg'])){
			$parent = $parts_ctg->find($row['parent_ctg'])->current();
			return $parent['name'].' - '.$row['name'];
		} else {
			return $row['name'];
		}


	}

	public function getDetail(){
		$detail = array();
		$detail[$this->_part_row->id] = array(
			'name' => $this->_part_row->name,
		);

		if($this->isMultipart()){
			foreach($this->_subparts as $subpart){
				$detail['subparts'][] = $subpart->getDetail();
			}
		}

		return $detail;
	}

	public function isMultipart(){
		return $this->_part_row->multipart == 'y' ? true : false;
	}

  public function getProducer() {
    $select = new Zend_Db_Select(Zend_Registry::get('db'));
    $select->from('producers_parts', 'id_producers')
      ->where('id_parts LIKE ?', $this->getID());
    return $select->query()->fetchColumn();
  }

    public function isSetPart(){
        return $this->_part_row->set == 'y' ? true : false;
    }
    // TOFIX
    public function toggleCollection($idCollections){
		$db = Zend_Registry::get('db');
		if($db->fetchRow("SELECT * FROM parts_collections WHERE id_collections = ? AND id_parts = '".$this->_part_row->id."'",$idCollections)){
			$db->delete("parts_collections", "id_collections = ".$db->quote($idCollections)." AND id_parts = '".$this->_part_row->id."'");
		} else {
			$db->insert("parts_collections", array("id_parts" => $this->_part_row->id, "id_collections" => $idCollections));
		}
    }

	public function toggleSet(){
        $parts = new Table_Parts();
        $parts->update(array("set" => $this->isSetPart() ? 'n' : 'y'), "id LIKE '".$this->_part_row->id."'");
    }

	public function toggleSemifinished(){
        $parts = new Table_Parts();
        $parts->update(array("semifinished" => $this->isSemifinished() ? 'n' : 'y'), "id LIKE '".$this->_part_row->id."'");
    }

    /**
     * Toggles the product flag for a part.
     *
     * @throws Parts_Exceptions_InActiveOrders|Orders_Exceptions_ForbiddenAccess if the part is a product and there are active orders containing this part
     */
    public function toggleProduct(): void
    {
        $tableParts = new Table_Parts();
        $isProduct = $this->isProduct();

        if ($isProduct) {
            $orders = $this->retrieveActiveOrders();
            if (!empty($orders)) {
                throw new Parts_Exceptions_InActiveOrders($orders, 'Cannot unset product flag, some active orders contain this part.');
            }
        }

        $tableParts->update([
            'product' => $isProduct ? 'n' : 'y',
            'designated' => $isProduct ? null : Table_Customers::TYPE_B2C,
        ], 'id LIKE ' . $tableParts->getAdapter()->quote($this->getID()));
    }
	
	public function getTree($withOperations = TRUE) {
    $this->_subparts = NULL;
    $subparts = array();
    if ($this->isMultipart()){
      foreach($this->getSubparts(TRUE) as $subpart){
        $subparts[$subpart->getID()] = $subpart->getTree($withOperations);
      }
    }
    return array(
      'id' => $this->getID(),
      'name' => $this->getName(),
      'amount' => $this->_amount,
      'price' => $this->getPrice($withOperations),
      'subparts' => $subparts,
    );
  }
	
	

	public function checkCircle($id){
		if($this->_part_row->id == $id) throw new Parts_Exceptions_Circle("Součást nemůže obahovat sebe sama jako podsoučást.");
		if($this->isMultipart()){
			foreach($this->getSubparts() as $subpart){
				$subpart->checkCircle($id);
			}
		}
	}

	public function checkMultipart(){
		$parts_subparts = new Table_PartsSubparts();
		if(count($parts_subparts->fetchAll("id_multipart LIKE '".$this->_part_row->id."'")) == 0){
      echo('multipart no more');
			$parts = new Table_Parts();
			$parts->update(array("multipart" => 'n'), "id LIKE '".$this->_part_row->id."'");
		} else {
      echo('still multipart');
    }
	}

	//vlozi vsechny podsoucasti do parts_all_subparts (vcetne samotne soucasti)
	public function insertSubParts($id, $amount){
		try{
			$products_all_parts = new Table_ProductsAllParts();

			$products_all_parts->insert(array(
				'id_products' => $id,
				'id_parts' => $this->_part_row->id,
				'amount' => ($amount*$this->_amount),
			));
		}catch(Exception $e){
			$products_all_parts->getAdapter()->query("
				UPDATE products_all_parts
				SET amount = amount + ".($amount*$this->_amount)."
				WHERE id_products LIKE '".$id."' AND id_parts LIKE '".$this->_part_row->id."'");
			//query(array('amount' => ''), "id_products LIKE '".$id."' AND id_parts LIKE '".$this->_part_row->id."'");
		}
		if($this->_multipart){
			foreach($this->_subparts as $subpart){
				$subpart->insertSubParts($id, $amount);
			}
		}
	}

	public function deleteSubPart($id){
		$parts_subparts = new Table_PartsSubparts();
		$parts_subparts->delete("id_multipart LIKE '".$this->_part_row->id."' AND id_parts LIKE '".$id."'");
    $this->refreshOperations();
    $this->refreshPrice();
	}

  protected function removePartFromActiveOrders($db): array {
    $affectedOrders = [];
    $productsInOrders = $db->select()
      ->from('orders_products', ['id'])
      ->join('orders',
        'orders.id = orders_products.id_orders and orders.status not in (\'closed\', \'trash\', \'deleted\')',
        ['order_id' => 'id', 'order_no' => 'no', 'order_title' => 'title'])
      ->where('orders_products.id_products = ?', $this->_part_row->id)
      ->query(Zend_Db::FETCH_ASSOC)->fetchAll();

    if ($productsInOrders) {
      foreach ($productsInOrders as $row) {
        if ($db->delete('orders_products', 'id = ' . $db->quote($row['id']))) {
          $affectedOrders[$row['order_id']] = [
            'id' => $row['order_id'],
            'no' => $row['order_no'],
            'title' => $row['order_title']
          ];
        }
      }
    }
    return $affectedOrders;
  }

  /**
   * Provede výmaz součásti (přesun do koše),
   * přičemž odstraní případný produkt ze všech nedokončených objednávek.
   *
   * @return array Asociativní pole s dotčenými objednávkami
   * @throws \Parts_Exceptions_InReservations
   * @throws \Parts_Exceptions_OnStock
   * @throws \Zend_Exception
   * @throws \Exception
   */
	public function delete(): array {

		$parts_warehouse = new Table_PartsWarehouse();

		if ($parts_warehouse->getAmount($this->_part_row->id) > 0) {
      throw new Parts_Exceptions_OnStock('Součást ID = '
        . $this->getID() . ' nelze smazat, protože je naskladněná.');
    }

    if ($this->checkReservations()) {
      throw new Parts_Exceptions_InReservations('Součást ID = '
        . $this->getID() . ' nelze smazat, protože je rezervovaná.');
    }

    $this->_checkMultipartsAbove();

		$parts_subparts = new Table_PartsSubparts();
		$parts = new Table_Parts();
    /** @var \Zend_Db_Adapter_Abstract $db */
		$db = Zend_Registry::get('db');

		try {
			$db->beginTransaction();

      $affectedOrders = $this->removePartFromActiveOrders($db);

			$parts_warehouse->delete('id_parts LIKE ' . $db->quote($this->_part_row->id));
      $parts_subparts->delete('id_parts LIKE ' . $db->quote($this->_part_row->id));
      $parts->update(['deleted' => 'y'], 'id = '  . $db->quote($this->_part_row->id));
      $db->commit();
		}
		catch (Exception $e) {
			$db->rollback();
			throw new Exception("Součást {$this->_part_row->id} se nepodařilo odstranit. " . $e->getMessage());
		}

    return $affectedOrders;
	}

  protected function _checkMultipartsAbove() {
    $parts_subparts = new Table_PartsSubparts();

    $where = $parts_subparts->getAdapter()->quoteInto('id_parts = ?', $this->_part_row->id);

    $select = $parts_subparts->select();
    $select->where($where);
    $data = $select->getAdapter()->fetchAll($select);

    foreach ($data as $row) {
      $parts = new Parts_Part($row['id_multipart']);
      $parts->deleteSubPart($this->_part_row->id);
      $parts->checkMultipart();
    }
  }

  /**
   * @throws Parts_Exceptions_IdAlreadyExists
   * @throws Zend_Db_Table_Exception
   * @throws Zend_Db_Adapter_Exception
   * @throws Parts_Exceptions_IdNotExists
   */
  public function saveAs($id, $newId, $newName, $newDescription){

    $tParts = new Table_Parts();
    $row = $tParts->find($newId)->current();
    if ($row !== null) {
      throw new Parts_Exceptions_IdAlreadyExists("Součást s ID = $newId již existuje.");
    }

    $oldRow = $tParts->find($id)->current();
    if ($oldRow === null) {
      throw new Parts_Exceptions_IdNotExists();
    }

    $oldData = $oldRow->toArray();

    $db = Utils::getDb();
    $db->beginTransaction();

    try {
      //kopirovani soucasti
      $newData = array();
      foreach($oldData as $key => $value) {
        $newData[$key] = $value;
      }
      $newData['id'] = $newId;
      $newData['name'] = $newName;
      $newData['description'] = empty($newDescription) ? new Zend_Db_Expr('NULL') : $newDescription;
      $tParts->insert($newData);

      //vkladani do skladu
      $tWarehouse = new Table_PartsWarehouse();
      $tWarehouse->insert(array(
        'id_parts' => $newId,
        'id_users' => 1,
        'amount' => 0
      ));

      //kopirovani podsoucasti
      $tSubParts = new Table_PartsSubparts();
      $rows = $tSubParts->fetchAll($tSubParts->select()->where("id_multipart = ?", $oldData['id']));
      foreach ($rows as $subpart) {
        $tSubParts->insert(array(
          'id_multipart' => $newData['id'],
          'id_parts' => $subpart['id_parts'],
          'amount' => $subpart['amount']
        ));
      }

      //kopirovani kolekci
      $select = $tParts->select();
      $select->setIntegrityCheck(false);
      $collections = $tParts->fetchAll($select->from("parts_collections")->where("id_parts = ?", $oldData['id']));
      foreach ($collections as $collection) {
        $tParts->getAdapter()->insert("parts_collections", array(
          'id_parts' => $newData['id'],
          'id_collections' => $collection['id_collections']
        ));
      }

      //kopirovani cenovych operaci
      $select = $tParts->select();
      $select->setIntegrityCheck(false);
      $collections = $tParts->fetchAll($select->from("parts_operations")->where("id_parts = ?", $oldData['id']));
      foreach ($collections as $operation) {
        $data = $operation->toArray();
        unset($data['changed']);
        unset($data['inserted']);
        $data['id_parts'] = $newData['id'];
        unset($data['id']);
        $tParts->getAdapter()->insert("parts_operations", $data);
      }

      //kopirovani vyrobcu
      $select = $tParts->select();
      $select->setIntegrityCheck(false);
      $producers = $tParts->fetchAll($select->from("producers_parts")->where("id_parts = ?", $oldData['id']));
      $inserted = date('Y-m-d H:i:s');
      foreach ($producers as $producer) {
        $data = $producer->toArray();
        $data['inserted'] = $inserted;
        unset($data['id']);
        $data['id_parts'] = $newData['id'];
        $tParts->getAdapter()->insert("producers_parts", $data);
      }

      $db->commit();
    }
    catch (Exception $e) {
      $db->rollBack();
      throw $e;
    }


	}

	public function getWarehouseTypeId() {
		return $this->_part_row->id_wh_type;
	}

	public function isTobacco() {
		return $this->_part_row->id_wh_type == self::WH_TYPE_TOBACCO;
	}

  /**
   * Přiřazování ceny k součásti pro kontrolu ceny v Table_Parts
   *
   * @param array $checkedParts Již zkontrolované součásti
   * @param int $itx Limit rekurze
   *
   * @return void
   * @throws \Zend_Exception
   */
  public function checkCosts(array &$checkedParts = array(), int $itx = 50) {
    $currentId = $this->getID();
    if (isset($checkedParts[$currentId])) {
      return;
    }
    $inputs = 0.0;
    if ($subparts = $this->getSubparts()) {
      foreach ($subparts as $subpart) {
        /** @var $subpart \Parts_Part */
        if ($subpart->isMultipart() && $itx > 0) {
          $subpart->checkCosts($checkedParts, $itx--);
        }
        $subpartCost = (float) $subpart->getPrice();
        $amount = $this->getPartAmount($subpart->getID());
        $inputs += $subpartCost * $amount;
      }
    }
    $checkedParts[$currentId] = [
      'id' => $currentId,
      'name' => $this->getName(),
      'producerId' => $this->getProducer(),
      'costs' => round($this->getPrice(), 2),
      'price' => round($this->getProducerPrice(), 2),
      'inputs' => round($inputs, 2),
      'operations' => round($this->getOperationSum(), 2),
    ];
  }

  /**
   * Prepocita operace a ceny soucasti.
   */
  public function recalculate(&$visited = array()) {
    $id = $this->getID();
    if (in_array($id, $visited)) {
      return;
    }
    $this->refreshOperations(TRUE, Parts_Part::OPERATIONS_DEEP_LEVEL, $visited);
    $price = $this->getProducerPrice() + $this->getOperationSum();
    $this->setPrice($price);
    $this->refreshPrice();
  }

  /**
   * Otestuje, jestli je soucast rezervovana.
   *
   * @return bool TRUE v pripade, ze je rezervovana, FALSE v pripade, ze neni rezervovana.
   */
  public function checkReservations() {
    $table = new Table_OrdersProductsParts();
    return $table->getTotalAmount($this->getID()) != 0;
  }

  /**
   * Vrati asociativni pole s mnozstvim rezervovane soucasti (hodnota) v objednavce (klic).
   *
   * @return array
   */
  public function getReservationsAmount() {
    $table = new Table_OrdersProductsParts();
    return $table->getReservationsAmount($this->getID());
  }

  public function getMeasure() {
    return $this->_part_row->measure;
  }

  public function getWHChanges(Meduse_Date $from = NULL, Meduse_Date $to = NULL, $sum = FALSE) {
    if (!$from) {
      $from = new Meduse_Date(date('Y-01-01'), 'Y-m-d');
    }
    if (!$to) {
      $to = clone $from;
      $to->addYear(1)->addSecond(-1);
    }
    $tHistory = new Table_PartsWarehouseHistory();
    return $tHistory->getChanges($this->getID(), $from, $to, $sum);

    /** @var Zend_Db_Adapter_Abstract $db */
    /*
    $db = Zend_Registry::get('db');
    $sub = $db->select()
      ->from(array('h' => 'parts_warehouse_history'), array(
        'date' => new Zend_Db_Expr('DATE_FORMAT(h.last_change, \'%Y-%m-%d\')'),
        'amount_add' => new Zend_Db_Expr('IF (amount > 0, amount, 0)'),
        'amount_remove' => new Zend_Db_Expr('IF (amount < 0, ABS(amount), 0)'),
        ))
      ->join(array('p' => 'parts'), 'p.id = h.id_parts AND p.id = \'' . $this->getID() . '\'', array())
      ->where('h.last_change >= ?', $from->get('Y-m-d 00:00:00'))
      ->where('h.last_change <= ?', $to->get('Y-m-d 23:59:59'));
    $select = $db->select()
      ->from(array('sub' => $sub), array(
        'date',
        'amount_add' => new Zend_Db_Expr('SUM(amount_add)'),
        'amount_remove' => new Zend_Db_Expr('SUM(amount_remove)'),
        ));
    if (!$sum) {
      $select
        ->group('date')
        ->order('date');
    }
    $query = $select->query(Zend_Db::FETCH_ASSOC);
    return $sum ? $query->fetch() : $query->fetchAll();
    */
  }

  public function getSales(Meduse_Date $from = NULL, Meduse_Date $to = NULL, $sum = FALSE) {
    if (!$from) {
      $from = new Meduse_Date(date('Y-01-01'), 'Y-m-d');
    }
    if (!$to) {
      $to = clone $from;
      $to->addYear(1);
    }
    $table = new Table_OrdersProducts();
    $data = $table->getProductSales($this->getID(), $from->get('Y-m-d'), $to->get('Y-m-d'), $sum);
    return $data;
  }

  public function setCriticalAmount($critical_amount) {
    $amount = $this->getWHAmount();
    $t = new Table_PartsWarehouse();
    $t->update(array(
      'amount' => $amount,
      'critical_amount' => $critical_amount
    ), 'id_parts = "' . $this->getID() . '"');
  }

  public function getCriticalAmount() {
    $t = new Table_PartsWarehouse();
    $row = $t->fetchRow('id_parts = "' . $this->getID() . '"')->toArray();
    return $row['critical_amount'];
  }

  public function isDeleted() {
    return ($this->_part_row->deleted == 'y');
  }

  public function getPricelistsCtg($fillDefaults = TRUE) {
    return Pricelists::getCtgParts($this->getID(), $fillDefaults);
  }

  /**
   * Nastaví produktu typ zákazníka, pro který je produkt určen.
   *
   * @throws \Parts_Exception_UnknownCustomersType
   * @throws \Parts_Exception_PartIsNotProduct
   */
  public function setDesignatedFor($customersType, $save = TRUE) {
    if (!is_null($customersType) && !in_array($customersType, Customers::getCustomersTypes())) {
      throw new Parts_Exception_UnknownCustomersType('Unknown customers type.');
    }
    if (!$this->isProduct()) {
      throw new Parts_Exception_artIsNotProduct('Part is not product.');
    }
    $this->_part_row->designated = $customersType === Table_Customers::TYPE_ALL ? NULL : $customersType;
    if ($save) {
      $this->_part_row->save();
    }
  }


  /**
   * Vrátí typ zákazníka, pro který je produkt určen.
   * V případě, že je určen pro všechny, vrátí NULL.
   *
   * @return string|null
   *
   * @throws \Parts_Exceptions_PartIsNotProduct
   */
  public function getDesignatedFor() {
    if (!$this->isProduct()) {
      throw new Parts_Exceptions_PartIsNotProduct('Part is not product.');
    }
    return strtoupper($this->_part_row->designated);
  }

  /**
   * Testuje, jestli je produkt určený pro B2B, B2C nebo všechny zákazníky.
   *
   * @throws \Parts_Exceptions_UnknownCustomersType
   * @throws \Parts_Exceptions_PartIsNotProduct
   */
  public function isDesignatedFor($customersType): bool {
    if (!in_array($customersType, Customers::getCustomersTypes())) {
      throw new Parts_Exceptions_UnknownCustomersType('Unknown customers type.');
    }
    $designatedFor = $this->getDesignatedFor();
    return ($customersType === Table_Customers::TYPE_ALL) ?
      is_null($designatedFor) : $designatedFor === $customersType;
  }

  /**
   * @throws \Parts_Exceptions_PartIsNotProduct
   */
  public function isDesignatedForB2b(): bool {
    try {
      return $this->isDesignatedFor(Table_Customers::TYPE_B2B);
    }
    catch (Parts_Exceptions_UnknownCustomersType $e) {
      Utils::logger()->warn('Something wrong. ' . $e->getMessage());
      return FALSE;
    }
  }

  /**
   * @throws \Parts_Exceptions_PartIsNotProduct
   */
  public function isDesignatedForB2c(): bool {
    try {
      return $this->isDesignatedFor(Table_Customers::TYPE_B2C);
    }
    catch (Parts_Exceptions_UnknownCustomersType $e) {
      Utils::logger()->warn('Something wrong. ' . $e->getMessage());
      return FALSE;
    }
  }

  /**
   * @throws \Parts_Exceptions_PartIsNotProduct
   */
  public function isDesignatedForAll(): bool {
    try {
      return $this->isDesignatedFor(Table_Customers::TYPE_ALL);
    }
    catch (Parts_Exceptions_UnknownCustomersType $e) {
      Utils::logger()->warn('Something wrong. ' . $e->getMessage());
      return FALSE;
    }
  }

    /**
     * Retrieve active orders that contain this part.
     *
     * @param bool $withObjects (optional) Set to true to include order objects in the result.
     * @return array An array of active orders.
     * @throws Orders_Exceptions_ForbiddenAccess
     */
    public function retrieveActiveOrders(bool $withObjects = false): array
    {
    $table = new Table_OrdersProducts();
    $orders = $table->retrieveActiveOrdersContainsPart($this->getID());
    if ($withObjects && !empty($orders)) {
        foreach ($orders as $row) {
            $order = null;
            try {
                $order = new Orders_Order($row['id']);
            } finally {
                $row['order'] = $order;
            }
        }
    }
    return $orders;
  }
}

