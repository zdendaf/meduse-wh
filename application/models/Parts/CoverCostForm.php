<?php

  class Parts_CoverCostForm extends Meduse_FormBootstrap {
    
    public function init() {
      
      $table = new Table_ApplicationCoverCost();
      
      $this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'));
			$this->setMethod(Zend_Form::METHOD_POST);
      $this->setLegend('Nastavení výchozí výše krycích nákladů');
      
      $element = new Meduse_Form_Element_Float('cover_cost');
      $element->setRequired(TRUE);
      $element->setValue($table->getCoverCost());
      $element->setLabel('Výchozí výše [%]');      
      $this->addElement($element);

      $element = new Meduse_Form_Element_Checkbox('set_default');
      $element->setLabel('Nastavit pro všechny produkty');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('Uložit');
      $this->addElement($element);
      
      parent::init();
    }
  }

  