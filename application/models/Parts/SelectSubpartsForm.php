<?php
class Parts_SelectSubpartsForm extends Meduse_FormBootstrap {
	
    private $_db = null;
    
    public function init()
    {
        // set db connection
        $this->_db = Zend_Registry::get('db');
        
		$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form', 'id' => 'subparts_filter'))
			->setMethod(Zend_Form::METHOD_POST);

		$categories = new Meduse_Form_Element_Select('coll');
        $categories->setLabel('Kolekce');
        $categories->addMultiOption(null, '- všechny -');
        $categories->addMultiOptions($this->_db->fetchPairs("SELECT id, name FROM collections"));
        $this->addElement($categories);
        
        $filterForm = new Parts_FilterForm();
        
        $this->addElement($filterForm->ctg);
        $this->addElement($filterForm->name);
        $this->name->setLabel("Název / ID");
		
		parent::init();
    }
	
}