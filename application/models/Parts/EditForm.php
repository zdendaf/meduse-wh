<?php
class Parts_EditForm extends Meduse_FormBootstrap {

  /**
   * @throws Zend_Exception
   * @throws Zend_Form_Exception
   */
  public function init(){

		$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			->setMethod(Zend_Form::METHOD_POST);

    $order = 0;

		// WH TYP (pipes nebo tabacco)
		$element = new Zend_Form_Element_Select('meduse_parts_wh_type');
		$db = Zend_Registry::get('db');
		$select = $db->select()->from('warehouse_types', array('id', 'name'));
		$element->addMultiOptions($db->fetchPairs($select));
		$element->setLabel('Typ skladové položky');
    $element->setOrder(++$order);
		$this->addElement($element);

    $element = new Meduse_Form_Element_Categories(['name' => 'meduse_parts_category', 'type' => 'pipes']);
    $element->addMultiOption(NULL, '-- žádná --');
    $element->setOrder(++$order);
    $this->addElement($element);

		$element = new Meduse_Form_Element_Select('meduse_parts_collection');
		$element->addMultiOption(NULL, '-- žádná --');
		$element->addMultiOptions(Zend_Registry::get('db')->fetchPairs('SELECT id,name FROM collections'));
		$element->setLabel('Kolekce');
    $element->setOrder(++$order);
    $this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_parts_name');
		$element->setRequired(true);
		$element->setLabel('Název');
    $element->setOrder(++$order);
    $this->addElement($element);

		$element = new Meduse_Form_Element_Textarea('meduse_parts_desc');
		$element->setLabel('Poznámka');
		$element->setAttrib("cols", 25);
		$element->setAttrib("rows", 6);
    $element->setOrder(++$order);
    $this->addElement($element);

		// typ měrné jednotka
		$element = new Zend_Form_Element_Select('meduse_parts_wh_measure');
		$element->addMultiOptions(array(
			Parts_Part::WH_MEASURE_UNIT => 'kusy',
			Parts_Part::WH_MEASURE_WEIGHT => 'hmotnost'
		));
		$element->setLabel('Typ měrné jednotky');
    $element->setOrder(++$order);
    $this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_parts_version');
		$element->setLabel('Verze');
    $element->setOrder(++$order);
    $this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_parts_hs');
		$element->setLabel('HS code');
    $element->setAttrib('readonly', 'readonly');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Button('hs_change');
    $element->setLabel('Změnit HS code');
    $element->setAttrib('class', 'btn btn-success');
    $element->setOrder(++$order);
    $this->addElement($element);

		$element = new Meduse_Form_Element_Text('meduse_parts_ean');
		$element->setLabel('EAN kód [5 cifer]');
		$element->addValidator(new Zend_Validate_StringLength(5, 5));
		$element->addValidator(new Zend_Validate_Digits());
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Currency('meduse_parts_price');
    $element->setLabel('Pořizovací cena [CZK]');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_parts_cover_cost');
    $element->setLabel('Krycí náklad [%]');
    $element->setOrder(++$order);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('meduse_parts_cover_cost_default');
    $element->setLabel('Vložit výchozí');
    $element->setOrder(++$order);
    $this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
    $element->setOrder(++$order);
    $this->addElement($element);

		parent::init();
    }

}