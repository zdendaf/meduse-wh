<?php
/**
 * Formular pro typovani externich skladu
 * @author cubanec
 *
 */
class Parts_ExternForm extends Meduse_FormBootstrap {
	
    public function init()
    {
        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('post');
        
		$element = new Meduse_Form_Element_Text('meduse_extern_name');
		$element->setRequired(true);
		$element->setLabel('Název');
		$this->addElement($element);
		
		$element = new Meduse_Form_Element_Text('meduse_extern_desc');
		$element->setLabel('Popis');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$this->addElement($element);
        
        parent::init();
    }
	
}