<?php
class Parts_Filter extends Meduse_FormBootstrap
{

    public function init()
    {
        /**
         * Databse
         */
        $db = Zend_Registry::get('db');

        /**
         * Set form legend
         */
        $this->setLegend('Výběr produktů objednávky');

        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('get');

        //sklad
        $rows = $db->fetchAll($db->select()->from('extern_warehouses'));
        $element = new Meduse_Form_Element_Select('wh');
        $element->setLabel('Sklad');
        $element->addMultiOption('null', '- všechny -');
        $element->addMultiOption('base', 'centrální');
        foreach ($rows as $row)
        {
            $element->addMultiOption($row['id'], $row['name']);
        }
        $this->addElement($element);

        //kolekce
        $rows = $db->fetchAll($db->select()->from('collections'));
        $element = new Meduse_Form_Element_MultiCheckbox('coll');
        $element->setLabel('Kolekce');
        foreach ($rows as $row)
        {
            $element->addMultiOption($row['id'], " " . $row['name']);
        }
        $this->addElement($element);

        $this->addElement(new Meduse_Form_Element_Categories('ctg'));

        $element = new Meduse_Form_Element_Submit('Hledat');
        $element->setValue('Hledat');
        $element->setAttrib('class', 'btn btn-primary');
        $this->addElement($element);

        parent::init();
    }

}