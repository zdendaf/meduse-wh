<?php

class Parts_AddCollectionsForm extends Meduse_FormBootstrap {

  public function init() {

    $this->setAttribs(['enctype' => 'multipart/form-data', 'class' => 'form'])
      ->setMethod(Zend_Form::METHOD_POST);

    $element = new Meduse_Form_Element_Text('collection_name');
    $element->setLabel('Název');
    $element->setRequired();
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('collection_code');
    $element->setLabel('Kód');
    $element->setAttrib('class', 'span1');
    $element->addValidator(new Zend_Validate_StringLength(2, 2));
    $element->setRequired();
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('collection_abbr');
    $element->setLabel('Zkratka');
    $element->setAttrib('class', 'span3');
    $element->addValidator(new Zend_Validate_StringLength(0, 20));
    $element->setRequired();
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('collection_is_pipes');
    $element->setLabel('Kolekce dymek');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('Odeslat');
    $this->addElement($element);

    parent::init();
  }

}