<?php


class Parts_BatchThrowingForm extends Meduse_FormBootstrap {

  public function init() {
    $this->setAttribs(['enctype' => 'multipart/form-data', 'class' => 'form'])
      ->setAction('/parts/batch-throwing')
      ->setMethod(Zend_Form::METHOD_POST)
      ->setLegend('Hromadné vyřazení součástí');

    $element = new Zend_Form_Element_Hidden('phase');
    $element->setValue('confirm');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Textarea('parts_ids');
    $element->setAttrib('rows', 20);
    $element->setAttrib('cols', 80);
    $element->setRequired(TRUE);
    $element->setLabel('ID součástí');
    $this->addElement($element);

    $element = new Zend_Form_Element_Note('note');
    $element->setDescription('<p>Zadejte ID součástí k vyřazení. ' .
      'Jednotlivá ID se oddělují novým řádkem, mezerou nebo čárkou. ' .
      'Součásti se vyřadí převedením z centrálního skladu do skladu vyřazených součástí.</p>');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('Vyřadit');
    $this->addElement($element);

    parent::init();
  }

}