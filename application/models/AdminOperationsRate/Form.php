<?php

  /**
   * Formular pro nastaveni ceny za hodinu prace.
   *
   * @author Zdenek
   */
  class AdminOperationsRate_Form extends Meduse_FormBootstrap {
    
    public function init() {
      
      $tRate = new Table_ApplicationOperationsRate();
      
      $eRate = new Meduse_Form_Element_Text('rate');
      $eRate
        ->setLabel('Hodinová sazba [Kč]')
        ->setAttribs(array('maxlength' => '4'))
        ->setValue($tRate->getRate())
        ->addValidator(new Zend_Validate_Int)
        ->addValidator(new Zend_Validate_GreaterThan(0))
        ->setRequired(true);
      
      $eSubmit = new Meduse_Form_Element_Submit('Uložit');
      
      $this
        ->setAttrib('class', 'form')
        ->setLegend('Nastavení hodinové sazby')
        ->addElement($eRate)
        ->addElement($eSubmit);
      
      parent::init();
    }
  }


