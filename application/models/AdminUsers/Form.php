<?php
class AdminUsers_Form extends Meduse_FormBootstrap {

    public function init() {
        $this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			 ->setMethod(Zend_Form::METHOD_POST);

        $element = new Meduse_Form_Element_Text('username');
        $element->setLabel('Login');
        $element->setAttrib('required', 'required');
        $element->setRequired();
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('check_ip');
        $element->setLabel('Kontrola IP');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('is_active');
        $element->setLabel('Aktivní');
        $element->setChecked(true);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('name');
        $element->setLabel('Jméno');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('name_full');
        $element->setLabel('Celé jméno');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Password('password');
        $element->setLabel('Heslo');
        $element->setAttrib('required', 'required');
        $element->setRequired();
        $this->addElement($element);

        $element = new Meduse_Form_Element_Password('re_password');
        $element->setLabel('Heslo znovu');
        $element->setAttrib('required', 'required');
        $element->setRequired();
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('email');
        $element->setLabel('E-mail');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Phone('phone');
        $element->setLabel('Telefon');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('description');
        $element->setLabel('Popis');
        $this->addElement($element);

        $element = new Meduse_Form_Element_MultiCheckbox('role');
        $db = Zend_Registry::get('db');
        $rows2 = $db->fetchPairs($db->select()
          ->from('users_roles', array('id', 'role_translation'))
          ->where('is_active')->where('id <> 1')
          ->order('role_translation ASC'));
        $element->addMultiOptions($rows2);
        $element->setLabel('Role');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('expiration');
        $element->setLabel('Doba odhlášení [min]');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('id');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Submit('Odeslat');
        $this->addElement($element);

        parent::init();
    }
    
    public function populate(array $values) {
      parent::populate($values);
      $idRoles = explode(',', $values['role_ids']);
      $this->getElement('role')->setValue($idRoles);
    }
}
