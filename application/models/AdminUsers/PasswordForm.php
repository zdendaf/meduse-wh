<?php
class AdminUsers_PasswordForm extends Meduse_FormBootstrap {

    public function init(){
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('post');

        $element = new Zend_Form_Element_Password('password');
		$element->setLabel('Zadejte svoje heslo');
		$element->setRequired();
		$this->addElement($element);

        $element = new Zend_Form_Element_Password('new_password');
		$element->setLabel('Zadejte nové heslo');
		$element->setRequired();
		$this->addElement($element);

		$element = new Zend_Form_Element_Password('re_password');
		$element->setLabel('Zopakujte nové heslo');
		$element->setRequired();
		$this->addElement($element);

		$element = new Zend_Form_Element_Submit('Odeslat');
		$this->addElement($element);

        parent::init();
	}

}
