<?php

class Documents {

  public const DATA_UPLOADS_DIR =  '/../data/documents/';

    public static function findAll(int $wh = Table_PartsWarehouse::WH_TYPE): array
    {
        $db = Utils::getDb();
        $subSelect = $db->select()->from('documents_files', [
            'id_documents',
            'cnt' => new Zend_Db_Expr('COUNT(*)'),
            'inserted' => new Zend_Db_Expr('MAX(inserted)'),
            'id' => new Zend_Db_Expr('MAX(id)'),
        ])->group('id_documents');
        $select = $db->select()->from(['d' => 'documents'])
            ->joinLeft(['f' => $subSelect], 'f.id_documents = d.id', [
                'versions' => new Zend_Db_Expr('IFNULL(f.cnt, 0)'),
                'last_date' => 'inserted',
                'last_fid' => 'id',
            ])
            ->join(['u' => 'users'], 'u.id = d.id_users', ['user' => 'name', 'user_name' => 'name_full'])
            ->where('d.deleted = 0')
            ->where('d.wh = ?', $wh)
            ->order('title');
        return $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    }
  
  public static function getForm(?string $legend = NULL, int $wh = Table_PartsWarehouse::WH_TYPE): Meduse_FormBootstrap {
    $form = ($wh === Table_TobaccoWarehouse::WH_TYPE) ? new Documents_TobaccoForm : new Documents_Form();
    if ($legend) {
      $form->setLegend($legend);
    }
    return $form;
  }

    public static function create(array $data = []): Documents_Document
    {
        $document = new Documents_Document();
        if (isset($data['wh'])) {
            $document->setWh($data['wh'], false);
        }
        if (isset($data['title'])) {
            $document->setTitle($data['title'], false);
        }
        if (isset($data['description'])) {
            $document->setDescription($data['description'], false);
        }
        $document->save();

        return $document;
    }

  public static function filesUpload(int $id = NULL, int $wh = NULL, string $title = NULL, string $description = NULL) {
    if (!$id) {
      $document = self::create(['wh' => $wh, 'title' => $title, 'description' => $description]);
    }
    else {
      $document = new Documents_Document($id);
      $document->setTitle($title ?? $document->getTitle());
      if ($description = $description ?? $document->getDescription()) {
        $document->setDescription($description);
      }
    }
    return $document->uploadFile();
  }

  public static function getFileInfo($fid): ?array
  {
    $table = new Table_DocumentsFiles();
    try {
      if ($row = $table->find($fid)->current()) {
        $info = $row->toArray();
        $info['filepath'] = Documents::generatePath($info['id_documents'], $info['id']) . '/' . $info['filename'];
        return $info;
      }
    } catch (Zend_Db_Table_Exception $e) {
      Utils::logger()->err($e->getMessage());
    }
    return NULL;
  }

  public static function generatePath($did, $fid = NULL, $createDir = FALSE): string
  {
    if (is_null($fid)) {
      $table = new Table_DocumentsFiles();
      $fid = (int) $table->select()->from($table, ['last_id' => new Zend_Db_Expr('MAX(id)')])->query()->fetchColumn();
      $fid++;
    }
    $docSubDir = str_pad($did, 8, '0', STR_PAD_LEFT);
    $verSubDir = str_pad($fid, 8, '0', STR_PAD_LEFT);
    $dir = APPLICATION_PATH . Documents::DATA_UPLOADS_DIR . $docSubDir;
    if ($createDir && !file_exists($dir)) {
      mkdir($dir);
    }
    $dir = $dir . '/' . $verSubDir;
    if ($createDir && !file_exists($dir)) {
      mkdir($dir);
    }
    return $dir;
  }

}