<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Customers_Customer {

    protected ?Table_Customers $_table = NULL;
    protected $_row;
    protected array $_addresses = array();

    /** @var int vychozi doba splatnosti */
    public const DUE_DEFAULT = 3;
    /** @var int vychozi hodnota doba splatnosti u plateb na dobirku */
    public const DUE_DEFAULT_COD = 14;
    public const VIES_WSDL_URL = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';

    /**
     * Vytvoreni objektu zakaznika
     *
     * @param mixed $data
     *    pokud je to pole dat, vytvorime noveho zakaznika
     *    pokud je to int, nacteme zakaznika
     * @throws Exception
     *    pokud ID neexistuje nebo nebyla zadana data
     */
    public function __construct($data = NULL) {
      $this->_table = new Table_Customers();
      if (is_array($data)) {
        // Pokud je zadano pole budeme tvorit noveho zakaznika.
        $this->_row = $this->_table->saveForm($data);
        $tCustAddresses = new Table_CustomerAddresses();
        $tCustAddresses->insert(array(
          'id_address' => $this->getBillingAddressId(),
          'id_customers' => $this->getId(),
        ));
      }
      elseif ($data !== NULL) {
        // pokud je zadano id, kontrola existence CID
        $tCustomers = new Table_Customers();
        $this->_row = $tCustomers->find($data)->current();
        if (!$this->_row) {
          throw new Exception('Customer #' . $data . ' non exists.');
        }
        // nacteni adres
        $tAddresses = new Table_CustomerAddresses();
        $this->_addresses = $tAddresses->getAddress($this, TRUE);
      }
      else {
        // pokud nejsou dana data ani id, pak vyjimka
        throw new Exception('Customer ID or customer data must be given.');
      }
    }

    public function getId() {
      return $this->_row->id;
    }

    public function getFirstName() {
      return $this->_row->first_name;
    }

    public function getLastName() {
      return $this->_row->last_name;
    }

    public function getType($long = FALSE): string
    {

      $type = $this->_row->type;
      if ($long) {
        if (is_null($type)) {
          return 'Nezadán';
        }

        return $type == 'B2B' ? 'Bussines' : 'Koncový';
      }

      return is_null($type) ? 'N/A' : $type;
    }

    public function getWhType() {
      return $this->_row->id_wh_type;
    }

    public function isB2B(): bool
    {
      return $this->_row->type === 'B2B';
    }

    public function getPhone() {
      return $this->_row->phone;
    }

    /** @deprecated Use getPhone() instead. */
    public function getMobile() {
      return $this->_row->phone;
    }

    public function getEmail() {
      return $this->_row->email;
    }

    public function getCompany() {
      return $this->_row->company;
    }

    public function getCustomerNo() {
      return $this->_row->customer_no;
    }

    public function getIdentNo() {
      return $this->_row->ident_no;
    }

    public function getVatNo() {
      return $this->_row->vat_no;
    }

    public function getVatInvoice() {
      return $this->_row->vat_invoice;
    }

    public function getSeedId() {
      return $this->_row->seed_id;
    }

    public function getTransferPerc() {
      return $this->_row->transfer_perc;
    }

    public function getAddresses() {
      return $this->_addresses;
    }

    public function getBillingAddress() {
      return (empty($this->_addresses) || is_null($this->_row->id_addresses)) ?
        NULL : $this->_addresses[$this->_row->id_addresses];
    }

    public function getBillingAddressId() {
      return $this->_row->id_addresses;
    }

    public function setBillingAddress($id) {
      $this->_row->id_addresses = $id;
      $this->_row->save();
    }

    public function getShippingAddress() {
      return (empty($this->_addresses) || is_null($this->_row->id_addresses2)) ?
      NULL : $this->_addresses[$this->_row->id_addresses2];
    }

    public function getShippingAddressId() {
      return $this->_row->id_addresses2;
    }

    public function setShippingAddress($id) {
      $this->_row->id_addresses2 = $id;
      $this->_row->save();
    }

    public function update($data) {
      $this->_table->saveForm($data);
    }

    public function getForm($wh = Pipes_Parts_Part::WH_TYPE) {
      $form = $wh === Pipes_Parts_Part::WH_TYPE ? new Customers_Form() : new Tobacco_Customers_Form();
      $form->setLegend("Upravit zákazníka");
      $data = $this->_row->toArray();
			$form->populate($data);
      return $form;
    }

    public function getOrders($params = array()): array
    {
      $tOrders = new Table_Orders();
      $select = $tOrders->select()->setIntegrityCheck(FALSE)
        ->from(array('o' => 'orders'))
        ->joinLeft(array('i' => 'invoice'), 'i.id_orders = o.id AND i.type = "regular"', [
          'invoice' => 'no',
          'id_invoice' => 'id',
          'invoice_issue_date' => 'issue_date'
        ])
        ->where('id_customers = ?', $this->getId())
        ->order('o.status')
        ->order('o.date_exp DESC');
      if (isset($params['status']) && $params['status']) {
        $select->where('status IN (?)', $params['status']);
      }
      if (!$params['view_private']) {
        $select->where('o.private = "n" OR o.owner = ?', $params['user']);
      }
      return $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
    }

    public function getCollectionAmountSums() {
      $tOrders = new Table_Orders();
      return $tOrders->getCollectionAmountSums($this->getId(), Table_PartsCtg::EXTRA_ACCESSORIES);
    }

    public function getDue(): int
    {
      return (int) (is_null($this->_row->due) ? self::DUE_DEFAULT : $this->_row->due);
    }

    public function isAllowedInvoicing() {
      return $this->_row->allow_invoicing == 'y';
    }

    /**
     * @throws Customers_Exceptions_NoVatNo
     * @see http://www.webmastersdiary.com/blog/php-vies-vat-number-validation-european-vat-id/
     */
    public function verifyVatNo() {

      if (!$vatNo = $this->getVatNo()) {
        throw new Customers_Exceptions_NoVatNo('CUSTOMER_HAS_NO_VAT');
      }

      $vatNo = str_replace(array(' ', '.', '-', ',', ', '), '', trim($vatNo));
      $cc = substr($vatNo, 0, 2);
      $vn = substr($vatNo, 2);

      if ($client = new SoapClient(self::VIES_WSDL_URL)){
        $params = array('countryCode' => $cc, 'vatNumber' => $vn);
        try {
          $r = $client->checkVat($params);
          if ($r->valid == TRUE){
            $this->_row->vat_checked = 'y';
          }
          else {
            $this->_row->vat_checked = 'n';
          }
          $this->_row->save();
        }
        catch(SoapFault $e) {
          throw $e;
        }
      }
      else {
        throw new Exception('NO_CONNESTION');
      }
      return $this->isVerifiedVatNo();
    }

    public function isVerifiedVatNo() {
      if ($this->_row->vat_checked == 'y') {
        return TRUE;
      }
      elseif ($this->_row->vat_checked == 'n') {
        return FALSE;
      }
      return NULL;
    }

    public function getBudgets($obj = TRUE) {
      $table = new Table_CustomersBudgets();
      $rows = $table->getCustomerBudgets($this->getId());
      if ($obj) {
        $budgets = array();
        foreach ($rows as $row) {
          $budgets[] = new Customers_Budget($row->id);
        }
        return $budgets;
      }
      else {
        return $rows;
      }
    }

    public function getSpendingProducts($year = NULL) {
      $spending = array();
      $tInvoices = new Table_Invoices();
      $select = $tInvoices->selectSpending($this->getId(), $year, Table_Orders::TYPE_PRODUTCS, FALSE);
      $result = $select->query(Zend_Db::FETCH_OBJ);
      while($row = $result->fetch()) {
        $spending[$row->year] = $row->amount;
      }
      return $spending;
    }
    public function getSpendingServices($year = NULL) {
      $spending = array();
      $tInvoices = new Table_Invoices();
      $select = $tInvoices->selectSpending($this->getId(), $year, Table_Orders::TYPE_SERVICE, FALSE);
      $result = $select->query(Zend_Db::FETCH_OBJ);
      while($row = $result->fetch()) {
        $spending[$row->year] = $row->amount;
      }
      return $spending;
    }

    public function recalculateBudgets() {
      if ($budgets = $this->getBudgets()) {
        /** @var Customers_Budget $budget */
        foreach ($budgets as $budget) {
          $budget->recalculateSpendings();
        }
      }
    }

    public function setDefaultPricelist($pricelistId = NULL) {
      $default = array();
      if ($pricelistId) {
        $default[] = $pricelistId;
      }
      else {
        $table_Pricelists = new Table_Pricelists();
        if (!$pricelists = $table_Pricelists->getAllPricelists($this->getWhType(), $this->getType())) {
          throw new Exception('Nejsou definovány žádné platné ceníky');
        }
        foreach ($pricelists as $pricelist) {
          if ($pricelist['default'] == 'y') {
            $default[] = $pricelist['id'];
          }
        }
        if (!$default) {
          $pricelist = array_shift($pricelists);
          $default[] = $pricelist['id'];
        }
      }
      $table_CustomersPricelists = new Table_CustomersPricelists();
      $table_CustomersPricelists->assignCustomer($this->getId(), $default);
    }

    public function getName() {
      return $this->_row->company ? $this->_row->company : implode(' ', array($this->_row->first_name, $this->_row->last_name));
    }

    public function isRequiredCoo(): bool {
      return $this->_row->coo_requirement == 'y';
    }

    public function isRequiredInvoiceVerification(): bool {
      return $this->_row->invoice_verify_requirement == 'y';
    }

    public function getPriceListCtg(): ?int
    {
      return $this->_row->id_pricelists_ctg !== null ? (int) $this->_row->id_pricelists_ctg : NULL;
    }

    /**
     * @throws Exception
     */
    public function setPricelistsCtg(int $ctgId) {
      if (-1 > $ctgId || Pricelists::CTG_NUM < $ctgId) {
        throw new Exception('Cenikova kategorie mimo rozsah');
      }
      $this->_row->id_pricelist_ctg = $ctgId;
      $this->_row->save();
    }

    /**
     * @return array|mixed|null
     * @throws Zend_Db_Statement_Exception
     */
    public function getPriceList() {
      if (!$this->isB2B()) {
        return Pricelists::getMasterPricelist(false);
      }
      $priceList = NULL;
      $table = new Table_Pricelists();
      $ctgId = $this->getPriceListCtg();
      if ($ctgId !== NULL) {
        $priceList = $table->getPriceListByCtg($ctgId);
      }
      elseif($plId = $table->getCustomersValidPricelist($this->getId())) {
        try {
          $priceList = $table->find($plId)->current()->toArray();
        }
        catch (Exception|Zend_Db_Table_Exception $e) {
          Utils::logger()->err($e->getMessage());
        }
      }
      return $priceList;
    }

    /**
     * @throws Zend_Db_Statement_Exception
     */
    public function getPriceListInfo(): ?Pricelists_Info
    {
      if ($data = $this->getPriceList()) {
        return (new Table_Pricelists())->getPricelists([$data['id']]);
      }
      return NULL;
    }

    /**
     * Přenastaví adresu všech otevřených objednávek.
     * @throws Orders_Exceptions_ForbiddenAccess
     */
    public function resetOrdersAddress(Addresses_Address $address): array
    {
      $addressId = $address->getId();
      $changedOrders = [];
      if ($orders = $this->getOrders(['view_private' => true])) {
        foreach ($orders as $item) {
          $order = new Orders_Order($item->id);
          if (! in_array($order->getStatus(), [Table_Orders::STATUS_CLOSED, Table_Orders::STATUS_TRASH]) && $order->getAddressId() !== $addressId) {
            $order->setAddress($address);
            $changedOrders[] = $order;
          }
        }
      }
      return $changedOrders;
    }

  }

