<?php

class Customers_defaultAddressForm extends Meduse_FormBootstrap  {

  public function init($addresses = null) {
    $this->setLegend('Adresy');
    $this->setAttribs(array('class' => 'form'))
      ->setMethod(Zend_Form::METHOD_POST);
    parent::init();
  }


  public function getAddresses($customerId, $defaultAddress){

   $tCustAddr = new Table_CustomerAddresses();
   $addresses = $tCustAddr->getAddress($customerId);

    $element = new Meduse_Form_Element_Radio('address');
    $element->setLabel('Adresy:');

    $data = array();

        foreach ($addresses as $address) {
          if($defaultAddress != $address['id_address']){
              $data[$address['id_address']] = $address['company'].' '
                      .$address['person'].' '
                      .$address['street'].' '
                      .$address['city'];
          }

       }

   $element->addMultiOptions($data);
   $this->addElement($element);

  $element = new Meduse_Form_Element_Submit('submit');
  $element->setLabel('Zvolit jako výchozí');
  $element->setOptions(array('class' => 'btn'));
  $this->addElement($element);

  }

}
