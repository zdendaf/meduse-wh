<?php
class Customers_ProductDiscount extends Meduse_FormBootstrap {

    public function init()
    {
    	$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			 ->setMethod(Zend_Form::METHOD_POST);

		$element = new Meduse_Form_Element_Text('search_product');
		$element->setLabel('Produkt');
        $element->setAttribs(array('required' => 'required', 'placeholder' => 'Název / ID'));
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('product_discount');
		$element->setLabel('Sleva [%]');
        $element->setAttrib('required', 'required');
        $element->setAttrib('class', 'span1');
		$this->addElement($element);

		$element = new Zend_Form_Element_Hidden('product_id_selected');
		$this->addElement($element);

		$element = new Zend_Form_Element_Hidden('product_edit');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('add_part_discount');
        $element->setLabel('Přidat');
		$this->addElement($element);

        $this->_isModalForm = true;
        parent::init();
	}
}
