<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Customers_WarehouseItemForm extends Meduse_FormBootstrap {

    public function init() {

      $this->_isModalForm = TRUE;
      $this->setAttribs(array('class' => 'form'));

      $element = new Zend_Form_Element_Hidden('id');
      $this->addElement($element);

      $element = new Zend_Form_Element_Text('inventory_number');
      $element->setLabel('Inventární číslo');
      $element->addValidator(new Zend_Validate_StringLength(0, 25));
      $this->addElement($element);

      $element = new Meduse_Form_Element_DatePicker('aquisition_date');
      $element->setLabel('Datum pořízení');
      $element->setRequired(TRUE);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Currency('aquisition_price');
      $element->setLabel('Cena pořízení');
      $element->setRequired(TRUE);
      $this->addElement($element);

    }


  }
