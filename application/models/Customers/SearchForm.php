<?php
class Customers_SearchForm extends Meduse_FormBootstrap 
{
	public function init() 
    {
        $this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			 ->setMethod(Zend_Form::METHOD_GET);

		$element = new Meduse_Form_Element_Text('name');
    $element->setAttrib('style', 'width: 98%');
    $element->setDescription('prohledávané údaje: firma, jméno, příjmení, firma a osoba na faktuře, email');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('Hledat');
		$this->addElement($element);
        
        parent::init();
	}
}
