<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Customers_Warehouse {

    protected $_row = NULL;
    protected $_products = array();

    public function __construct($data = NULL) {
      $tWarehouses = new Table_CustomersWarehouses();
      $tProducts = new Table_CustomersWarehousesProducts();
      if (is_array($data)) {
        $this->_row = $tWarehouses->createRow();
        $this->update($data);
      }
      elseif ($data) {
        $this->_row = $tWarehouses->find((int) $data)->current();
        $this->_products = $tProducts->getProducts($this->_row->id);
      }
      else {
        throw new Customers_Exceptions_IncompleteConstructor('Musi byt zadano ID nebo pole dat pro zalozeni.');
      }
    }

    public function update($data) {
      $this->_row->id_customers = $data['id_customers'];
      $this->_row->name = $data['name'];
      $this->_row->save();
    }

    public function getId() {
      return $this->_row->id;
    }

    public function getName() {
      return $this->_row->name;
    }

    public function getCustomer() {
      return new Customers_Customer($this->_row->id_customers);
    }

    public function getCustomerId() {
      return $this->_row->id_customers;
    }

    public function getProducts() {
      return $this->_products;
    }

    public function getSummary() {
      $tProducts = new Table_CustomersWarehouses();
      return $tProducts->getSummary($this->_row->id);
    }

    public function addProducts($object, $pricelistId = NULL, $amount = 1, $free_amount = 0) {
      if (is_a($object, 'Orders_Order')) { // pridavame z objednavky naraz vice produktu
        $object = new Orders_Order($object->getId());
        $pricelistId = ($pricelistId) ? (int) $pricelistId : (int) $object->getPricelist();
        $orderId = $object->getID();
        $products = $object->getProducts();
      }
      else { // pridavame po jednom produktu
        $orderId = NULL;
        $product = is_string($object) ? new Parts_Part($object) : $object;
        $products = array(
          'id_products' => $product->getID(),
          'amount' => $amount,
          'free_amount' => $free_amount,
        );
      }
      $tProducts = new Table_CustomersWarehousesProducts();
      $tPrices = new Table_Prices();
      foreach ($products as $product) { // po jednom vkladame do tabulky produktu v zakaznickych skladech
        if ($product['free_amount'] > 0) { // vlozeni produktu zdarma
          $tProducts->addProduct($this->_row->id, $product['id_products'], 0, $orderId, $product['free_amount']);
        }
        if ($product['amount'] > 0) { // vlozeni ostatnich produktu dle ceniku
          $price = $pricelistId ? (float) $tPrices->getProductPrice($product['id_products'], $pricelistId) : 0;
          $tProducts->addProduct($this->_row->id, $product['id_products'], $price, $orderId, $product['amount']);
        }
      }
      $this->_products = $tProducts->getProducts($this->_row->id); // obnoveni produktu v objektu
    }
  }
