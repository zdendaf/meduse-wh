<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 16.7.2018
 * Time: 11:59
 */

class Customers_BudgetForm extends Meduse_FormSkove {

  protected $customerId = NULL;
  protected $budget = NULL;

  public function __construct($options = null) {
    if (!isset($options['id_customer'])) {
      throw new Customers_Exceptions_NoCustomer('Nebylo zadáno ID zakazníka.');
    }
    else {
      $this->customerId = $options['id_customer'];
      unset($options['id_customer']);
    }
    if (isset($options['id_budget'])) {
      $this->budget = new Customers_Budget($options['id_budget']);
      unset($options['id_budget']);
    }
    parent::__construct($options);
  }

  public function getModalPart($id = NULL) {
    if ($this->budget) {
      $length = $this->budget->getContractLength();
      $date = $this->budget->getContractDate();
      $i = 0;
      while ($i < $length) {
        $delta = $date->toString('Y');
        $this->removeElement('products_amount_' . $delta);
        $this->removeElement('services_amount_' . $delta);
        $this->removeElement('products_spent_' . $delta);
        $this->removeElement('services_spent_' . $delta);
        $this->removeElement('products_remain_' . $delta);
        $this->removeElement('services_remain_' . $delta);
        $date->addYear(1);
        $i++;
      }
    }
    if ($id) {
      $this->setName($id);
    }
    $this->getElement('contract_date')->setRequired(TRUE);
    $this->getElement('contract_length')->setRequired(TRUE);
    $this->_isModalForm = TRUE;
    return $this;
  }

  public function getTablePart() {
    $this->removeElement('customer_id');
    $this->removeElement('contract_date');
    $this->removeElement('contract_length');
    return $this;
  }

  public function init() {
    $this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
      ->setMethod(Zend_Form::METHOD_POST)
      ->setAction('/customers/budget-save');

    $eCustomerId = new Zend_Form_Element_Hidden('id_customer');
    $eCustomerId->setValue($this->customerId);
    $this->addElement($eCustomerId);

    $eBudgetId = new Zend_Form_Element_Hidden('id_budget');

    $eBudgetDate = new Meduse_Form_Element_DatePicker('contract_date');
    $eBudgetDate->setLabel('Datum smlouvy');

    $eBudgetLength = new Meduse_Form_Element_Text('contract_length');
    $eBudgetLength->addValidator(new Zend_Validate_GreaterThan(0));
    $eBudgetLength->addValidator(new Zend_Validate_Int());
    $eBudgetLength->setLabel('Délka [roky]');
    $eBudgetLength->setAttrib('class', 'span2');
    $eBudgetLength->setAttrib('maxlength', 2);

    $this->addElement($eBudgetId);
    $this->addElement($eBudgetDate);
    $this->addElement($eBudgetLength);
    if ($this->budget) {
      $this->setValues();
    }

    $eSubmit = new Zend_Form_Element_Submit('save');
    $eSubmit->setLabel('Uložit');
    $this->addElement($eSubmit);

    parent::init();
  }

  protected function setValues() {

    $date = $this->budget->getContractDate();
    $length = $this->budget->getContractLength();

    $this->getElement('id_budget')->setValue($this->budget->getId());
    $this->getElement('contract_date')->setValue($date->toString('d.m.Y'));
    $this->getElement('contract_length')->setValue($length);

    $budgets = $this->budget->getYearsBudgets();
    foreach ($budgets as $delta => $budget) {
      $eItemProductAmount = new Meduse_Form_Element_Currency('products_amount_' . $delta);
      $eItemProductAmount->setValue($budget->products_amount);
      $eItemProductAmount->setAttrib('class', 'span12 text-right');
      $this->addElement($eItemProductAmount);

      $eItemServiceAmount = new Meduse_Form_Element_Currency('services_amount_' . $delta);
      $eItemServiceAmount->setValue($budget->services_amount);
      $eItemServiceAmount->setAttrib('class', 'span12 text-right');
      $this->addElement($eItemServiceAmount);

      $eItemProductSpent = new Meduse_Form_Element_Currency('products_spent_' . $delta);
      $eItemProductSpent->setAttrib('readonly', 'readonly');
      $eItemProductSpent->setValue($budget->products_spent);
      $eItemProductSpent->setAttrib('class', 'span12 text-right');
      $this->addElement($eItemProductSpent);

      $eItemServiceSpent = new Meduse_Form_Element_Currency('services_spent_' . $delta);
      $eItemServiceSpent->setAttrib('readonly', 'readonly');
      $eItemServiceSpent->setValue($budget->services_spent);
      $eItemServiceSpent->setAttrib('class', 'span12 text-right');
      $this->addElement($eItemServiceSpent);

      $eItemProductRemain = new Meduse_Form_Element_Currency('products_remain_' . $delta);
      $eItemProductRemain->setAttrib('readonly', 'readonly');
      $eItemProductRemain->setValue($budget->products_amount - $budget->products_spent);
      $eItemProductRemain->setAttrib('class', 'span12 text-right');
      $this->addElement($eItemProductRemain);

      $eItemServiceRemain = new Meduse_Form_Element_Currency('services_remain_' . $delta);
      $eItemServiceRemain->setAttrib('readonly', 'readonly');
      $eItemServiceRemain->setAttrib('class', 'span12 text-right');
      $eItemServiceRemain->setValue($budget->services_amount - $budget->services_spent);
      $this->addElement($eItemServiceRemain);
    }
  }

  public function render(Zend_View_Interface $view = NULL) {
    if ($this->_isModalForm) {
      return parent::render($view);
    }
    try {
      if (is_null($view)) {
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/views/scripts');
      }
      $view->contract_date = $this->budget->getContractDate()->toString('d.m.Y');
      $view->contract_length = $this->budget->getContractLength();
      $view->year_start = $this->budget->getContractDate()->toString('Y');
      switch($view->contract_length) {
        case 1: $view->years = 'rok'; break;
        case 2: case 3: case 4: $view->years = 'roky'; break;
        default:  $view->years = 'let'; break;
      }
      $view->form = $this;
      $html = $view->render('customers/budget-table-form.phtml');
    }
    catch (Zend_View_Exception $e) {
      $html = $e->getMessage();
    }
    return $html;
  }

  public function getId() {

    return $this->budget ? $this->budget->getId() : 0;
  }
}