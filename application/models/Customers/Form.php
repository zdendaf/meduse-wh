<?php
class Customers_Form extends Meduse_FormBootstrap {

  /**
   * @throws Zend_Exception
   * @throws Zend_Form_Exception
   */
  public function init() {

    $this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
     ->setMethod(Zend_Form::METHOD_POST);

    $this->setLegend("Přidat zákazníka");

		$element = new Meduse_Form_Element_Text('customer_company');
		$element->setLabel('Název firmy');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('customer_first_name');
		$element->setLabel('Jméno');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('customer_last_name');
		$element->setLabel('Příjmení');
		$this->addElement($element);



		// Pole pouze pro noveho zakaznika -----------------------------------------
    $element = new Meduse_Form_Element_Text('address_street');
    $element->setLabel('Ulice');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('address_pop_number');
    $element->setLabel('Číslo popisné');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('address_orient_number');
    $element->setLabel('Číslo orientační');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('address_city');
    $element->setLabel('Město');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('address_zip');
    $element->setLabel('PSČ');
    $this->addElement($element);

    $element = new Meduse_Form_Element_SelectChosen('address_country');
    $db = Utils::getDb();
    $rows = $db->fetchPairs('SELECT code, CONCAT(name, " (", code, ")" ) FROM c_country ORDER BY name');
    $element->addMultiOptions($rows);
    $element->setLabel('Země');
    $element->setValue('CZ');
    $this->addElement($element);
    // -------------------------------------------------------------------------



		$element = new Meduse_Form_Element_Phone('customer_phone');
		$element->setLabel('Telefon');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('customer_email');
		$element->setLabel('E-mail');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('customer_ident_no');
		$element->setLabel('ID no');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('customer_vat_no');
		$element->setLabel('Vat no');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('customer_vat_checked');
    $element->setLabel('Ověření VAT');
    $element->addMultiOptions(array(
      '' => 'neověřeno',
      'n' => 'nevalidní',
      'y' => 'validní',
    ));
		$this->addElement($element);

		$element = new Meduse_Form_Element_Select('customer_vat_invoice');
    $element->setLabel('Fakturovat s daní');
    $element->addMultiOptions(array(
      Table_Customers::VAT_INVOICED_AUTO => 'automaticky',
      Table_Customers::VAT_INVOICED_NEVER => 'vždy bez daně',
      Table_Customers::VAT_INVOICED_ALWAYS => 'vždy s daní ',
    ));
		$this->addElement($element);


		$element = new Meduse_Form_Element_Select('customer_type');
		$element->setLabel('Typ zákazníka');
		$element->addMultiOption(NULL, '-- zvol --');
		$element->addMultiOption('B2B', 'B2B');
		$element->addMultiOption('B2C', 'B2C');
		$element->setRequired(TRUE);
		$this->addElement($element);

    $options = [];
    if ($categories = Pricelists::getAllCategoriesIdx(null, true, true)) {
      foreach ($categories as $idx) {
        if ($idx === -1) {
          $options[$idx] = 'Nákladový';
          continue;
        }
        if ($idx === 0) {
          $options[$idx] = 'Retail';
          continue;
        }
        $options[$idx] = 'Wholesale ' . $idx;
      }
    }
    $element = new Meduse_Form_Element_Select('id_pricelists_ctg');
    $element->setLabel('Ceník');
    $element->addMultiOptions($options);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('coo_requirement');
    $element->setLabel('COO');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('invoice_verify_requirement');
    $element->setLabel('Ověření faktury');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Checkbox('seller');
    $element->setLabel('Prodávající');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('Odeslat');
    $this->addElement($element);

    parent::init();
	}

    public function populate(array $values)
    {
        $this->customer_company->setValue($values['company']);
        $this->customer_first_name->setValue($values['first_name']);
        $this->customer_last_name->setValue($values['last_name']);
        $this->customer_phone->setValue($values['phone']);
        $this->customer_email->setValue($values['email']);
        $this->customer_ident_no->setValue($values['ident_no']);
        $this->customer_vat_no->setValue($values['vat_no']);
        $this->customer_vat_checked->setValue($values['vat_checked']);
        $this->customer_vat_invoice->setValue($values['vat_invoice']);
        $this->customer_type->setValue($values['type']);
        $this->coo_requirement->setValue($values['coo_requirement']);
        $this->invoice_verify_requirement->setValue($values['invoice_verify_requirement']);
        $this->id_pricelists_ctg->setValue($values['id_pricelists_ctg']);
        $this->seller->setValue($values['seller'] ? 'y' : 'n');
    }
}
