<?php
class Customers_CategoryDiscount extends Meduse_FormBootstrap {

    public function init()
    {
    	$this->setAttribs(array('enctype' => 'multipart/form-data', 'class' => 'form'))
			 ->setMethod(Zend_Form::METHOD_POST);

		$element = new Meduse_Form_Element_Select('search_category');
		$element->setLabel('Kategorie');
        $element->setAttribs(array('required' => 'required'));
		$this->addElement($element);

		$element = new Meduse_Form_Element_Text('category_discount');
		$element->setLabel('Sleva [%]');
        $element->setAttrib('required', 'required');
        $element->setAttrib('class', 'span1');
		$this->addElement($element);

		$element = new Zend_Form_Element_Hidden('category_edit');
		$this->addElement($element);

		$element = new Meduse_Form_Element_Submit('add_category_discount');
        $element->setLabel('Přidat');
		$this->addElement($element);

        
        $this->_isModalForm = true;
        parent::init();
	}
}
