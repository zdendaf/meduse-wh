<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 29.6.2018
 * Time: 15:51
 */

class Customers_Budget {

  protected $_row = NULL;
  protected $_items = array();

  public static function create(Customers_Customer $customer, Meduse_Date $date, int $length) {
    if ($length < 1) {
      throw new Customers_Exceptions_WrongBudgetContractLength('Doba smlouvy musi byt alespon jeden rok');
    }
    $tBudgets = new Table_CustomersBudgets();
    $row = $tBudgets->createRow(array(
      'id_customers' => $customer->getId(),
      'contract_date' => $date->toString('Y-m-d'),
      'contract_length' => $length,
    ));
    $id = $row->save();
    $spendingProducts = $customer->getSpendingProducts();
    $spendingServices = $customer->getSpendingServices();
    $tItems = new Table_CustomersBudgetsItems();
    for ($i = 0; $i < $length; $i++) {
      $delta = $date->toString('Y') + $i;
      $row = $tItems->createRow(array(
        'id_customers_budgets' => $id,
        'delta' => $delta,
        'products_spent' => isset($spendingProducts[$delta]) ? $spendingProducts[$delta] : 0,
        'services_spent' => isset($spendingServices[$delta]) ? $spendingServices[$delta] : 0,
      ));
      $row->save();
    }
    return new Customers_Budget($id);
  }

  public static function delete($id) {
    $tItems = new Table_CustomersBudgetsItems();
    $tItems->delete('id_customers_budgets = ' . $id);
    $tBudgets = new Table_CustomersBudgets();
    $tBudgets->delete('id = ' . $id);
  }

  public function __construct($id) {
    $tBudgets = new Table_CustomersBudgets();
    $this->_row = $tBudgets->find($id)->current();
    $tItems = new Table_CustomersBudgetsItems();
    $result = $tItems->select()
      ->where('id_customers_budgets = ?', $id)
      ->order('delta ASC')
      ->query();
    while($row = $result->fetch()) {
      $this->_items[$row['delta']] = $tItems->createRow($row);
    }
  }

  public function save() {
    $this->redistribute();
    $this->_row->save();
    foreach($this->_items as $item) {
      $item->save();
    }
  }

  public function getContractDate($obj = TRUE) {
    if ($obj) {
     return new Meduse_Date($this->_row->contract_date, 'Y-m-d');
    }
    else {
      return $this->_row->contract_date;
    }
  }

  public function setContractDate(Meduse_Date $date, $save = TRUE) {
    $this->_row->contract_date = $date->toString('Y-m-d');
    if ($save) {
      $this->_row->save();
    }
  }

  public function getContractLength() {
    return $this->_row->contract_length;
  }

  public function setContractLength(int $length, $save = TRUE) {
    if ($length < 1) {
      throw new Customers_Exceptions_WrongBudgetContractLength('Doba smlouvy musi byt alespon jeden rok');
    }
    $this->_row->contract_length = $length;
    if ($save) {
      $this->_row->save();
    }
  }

  protected function redistribute() {
    $items = array();
    $date = new Meduse_Date($this->_row->contract_date, 'Y-m-d');
    $length = $this->getContractLength();
    $tItems = new Table_CustomersBudgetsItems();
    $i = 0;
    while ($i < $length) {
      $delta = $date->toString('Y');
      if (isset($this->_items[$delta])) {
        $items[$delta] = $this->_items[$delta];
      }
      else {
        $items[$delta] = $tItems->createRow(array(
          'id_customers_budgets' => $this->getId(),
          'delta' => $delta,
        ));
      }
      $date->addYear(1);
      $i++;
    }
    $tItems->delete('id_customers_budgets = ' . $this->getId());
    $this->_items = $items;
  }

  public function getId() {
    return $this->_row->id;
  }

  public function getCustomerId() {
    return $this->_row->id_customers;
  }

  public function getYearsBudgets(Meduse_Date $date = NULL) {
    if ($date) {
      return $this->_items[$date->toString('Y')];
    }
    else {
      return $this->_items;
    }
  }

  public function setYearBudgetAmount($delta, $product, $service, $save = TRUE) {
    $tItems = new Table_CustomersBudgetsItems();
    if (!isset($this->_items[$delta])) {
      $this->_items[$delta] = $tItems->createRow(array(
        'id_customers_budgets' => $this->getId(),
        'delta' => $delta,
      ));
    }
    $this->_items[$delta]->products_amount = $product;
    $this->_items[$delta]->services_amount = $service;
    if ($save) {
      $this->save();
    }
  }

  public function setYearBudgetProductsSpent($delta, $amount, $save = TRUE) {
    if (isset($this->_items[$delta])) {
      $this->_items[$delta]->products_spent = $amount;
    }
    if ($save) {
      $this->save();
    }
  }

  public function setYearBudgetServicesSpent($delta, $amount, $save = TRUE) {
    if (isset($this->_items[$delta])) {
      $this->_items[$delta]->services_spent = $amount;
    }
    if ($save) {
      $this->save();
    }
  }


  public function getForm() {
    return new Customers_BudgetForm(array(
      'id_customer' => $this->getCustomerId(),
      'id_budget' => $this->getId(),
    ));
  }

  public function recalculateSpendings() {
    $customer = new Customers_Customer($this->getCustomerId());
    $spendingProducts = $customer->getSpendingProducts();
    $spendingServices = $customer->getSpendingServices();
    $length = $this->getContractLength();
    $date = $this->getContractDate();
    for ($i = 0; $i < $length; $i++) {
      $delta = $date->toString('Y') + $i;
      $amount = isset($spendingProducts[$delta]) ? $spendingProducts[$delta] : 0;
      $this->setYearBudgetProductsSpent($delta, $amount, FALSE);
      $amount = isset($spendingServices[$delta]) ? $spendingProducts[$delta] : 0;
      $this->setYearBudgetServicesSpent($delta, $amount, FALSE);
    }
    $this->save();
  }
}