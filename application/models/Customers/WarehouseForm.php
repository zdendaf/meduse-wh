<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Customers_WarehouseForm extends Meduse_FormBootstrap {

    protected $customers = array();
    protected $warehouse_id = NULL;

    public function __construct($options = NULL) {
      if (isset($options['warehouse_id'])) {
        $this->warehouse_id = $options['warehouse_id'];
        unset($options['warehouse_id']);
      }
      if (isset($options['id_wh_type'])) {
        $id_wh_type = $options['id_wh_type'];
        unset($options['id_wh_type']);
      }
      else {
        $id_wh_type = 1;
      }
      $tCustomers = new Table_Customers();
      $this->customers = $tCustomers->getOptionsArray(array(
        'id_wh_type = ?' => $id_wh_type,
      ));
      parent::__construct($options);
    }

    public function init() {

      $this->setAttrib('class', 'form');
      $this->setLegend($this->warehouse_id ? 'Editace skladu' : 'Založení skladu');

      $element = new Zend_Form_Element_Hidden('id');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Select('id_customers');
      $element->setLabel('Zákazník');
      $element->setRequired(TRUE);
      $element->addMultiOptions($this->customers);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Text('name');
      $element->setLabel('Název');
      $element->setRequired(TRUE);
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('submit');
      $element->setLabel('Uložit');
      $this->addElement($element);

      parent::init();
    }


  }
