<?php
class Addresses_Form extends Meduse_FormBootstrap {

    protected $_customer = NULL;

    public function __construct($options = null) {
      if (isset($options['customer'])
        && is_a($options['customer'], 'Customers_Customer')) {
        $this->_customer = $options['customer'];
        unset($options['customer']);
      }
      parent::__construct($options);
    }

    public function init() {
        /**
         * Set form legend
         */
        $this->setLegend('Adresa');

        /**
         * Create Elements
         */
        $this->setAttribs(array('class' => 'form'))
             ->setMethod('post');

            $element = new Zend_Form_Element_Hidden('id_customers');
            if ($this->_customer) {
              $element->setValue($this->_customer->getId());
            }
            $this->addElement($element);

            $element = new Meduse_Form_Element_Text('company');
            $element->setLabel('Název firmy');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Text('person');
            $element->setLabel('Kontaktní osoba');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Phone('phone');
            $element->setLabel('Telefon');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Text('street');
            $element->setLabel('Ulice');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Text('pop_number');
            $element->setLabel('Číslo popisné');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Text('orient_number');
            $element->setLabel('Číslo orientační');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Text('city');
            $element->setLabel('Město');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Text('zip');
            $element->setLabel('PSČ');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Select('country');
            $db = Zend_Registry::get('db');
            $rows = $db->fetchPairs("SELECT code, CONCAT(name,' (',code,')') FROM c_country ORDER BY name");
            $element->addMultiOptions($rows);
            $element->setLabel('Země');
            $element->setValue('CZ');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Checkbox('default_bill');
            $element->setLabel('Fakturační adresa');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Checkbox('default_ship');
            $element->setLabel('Výchozí dodací adresa');
            $this->addElement($element);

            $element = new Meduse_Form_Element_Submit('save');
            $element->setLabel('Uložit');
            $this->addElement($element);

            parent::init();
	}

  public function populate(array $values) {
    if (!is_null($this->_customer)) {
      if (isset($values['id']) && $this->_customer->getBillingAddressId() == $values['id']) {
        $this->getElement('default_bill')->setValue(TRUE);
      }
      if (isset($values['id']) && $this->_customer->getShippingAddressId() == $values['id']) {
        $this->getElement('default_ship')->setValue(TRUE);
      }
    }
    parent::populate($values);
  }
}
