<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Addresses_Address {

    const TYPE_BILLING = 'bill';
    const TYPE_SHIPPING = 'ship';
    const TYPE_OTHER = 'other';

    protected $_table = NULL;
    protected $_row = NULL;

    public function __construct($data = NULL) {

      $this->_table = new Table_Addresses();
      if (is_array($data)) {

        if ($data['default_bill'] == 'y' && empty($data['phone'])) {
          throw new Exception('Fakturacni adresa musi obsahovat telefon.');
        }

        $this->_row = $this->_table->saveForm($data);
        $this->setCustomer($data['id_customers']);
        $this->update($data);
      }
      elseif(is_object($data)) {
        $this->_row = $data;
      }
      elseif ($data) {
        $address = $this->_table->find($data)->current();
        if ($address) {
          $this->_row = $address;
        }
        else {
          throw new Exception('Address #' . $data . 'doesnt exists');
        }
      }
      else {
        throw new Exception('Address ID or address data must be given');
      }
    }

    public function getId() {
      return $this->_row->id;
    }

    public function getCustomer() {
      $tCustAddress = new Table_CustomerAddresses();
      $select = $tCustAddress->select()
        ->where('id_address = ?', $this->getId())
        ->where('id_customers <> ?', 0);
      $row = $select->query(Zend_Db::FETCH_OBJ)->fetch();
      return new Customers_Customer($row->id_customers);
    }

    protected function setCustomer($id) {
      $tCustAddress = new Table_CustomerAddresses();
      $tCustAddress->insert(array(
        'id_customers' => $id,
        'id_address' => $this->getId(),
      ));
    }

    public function render($separator = '<br/>') {

      $address = array();
      if (!empty($this->_row->company)) {
        $address[] = $this->_row->company;
      }
      if (!empty($this->_row->person)) {
        $address[] = $this->_row->person;
      }
      if (!empty($this->_row->phone)) {
        $address[] = $this->_row->phone;
      }
      if (!empty($this->_row->street)) {
        $street = $this->_row->street;
        $number = array();
        if (!empty($this->_row->pop_number)) {
          $number[] = $this->_row->pop_number;
        }
        if (!empty($this->_row->orient_number)) {
          $number[] = $this->_row->orient_number;
        }
        if (!empty($number)) {
          $street .= ' ' . implode('/', $number);
        }
        $address[] = $street;
      }
      $city = array();
      if (!empty($this->_row->zip)) {
        $city[] = $this->_row->zip;
      }
      if (!empty($this->_row->city)) {
        $city[] = $this->_row->city;
      }
      if (!empty($city)) {
        $address[] = implode(' ', $city);
      }
      if (!empty($this->_row->country)) {
        $address[] = $this->_row->country;
      }
      return implode($separator, $address);
    }

    public function __toString() {
      return $this->render();
    }

    public function setBilling() {
      $this->_bill = TRUE;
    }

    public function setShipping() {
      $this->_ship = TRUE;
    }

    public function isBilling() {
      $id = $this->getCustomer()->getBillingAddressId();
      return ($this->getId() == $id);
    }

    public function isShipping() {
      return ($this->getId() == $this->getCustomer()->getShippingAddressId());
    }

    public function getForm() {
      $customerObj = $this->getCustomer();
      $form = new Addresses_Form(array('customer' => $customerObj));
      $form->populate($this->_row->toArray());
      return $form;
    }

    public function update(array $data) {

      if ($data['default_bill'] == 'y' && empty($data['phone'])) {
        throw new Exception('Fakturacni adresa musi obsahovat telefon.');
      }

      $this->_row->company = $data['company'];
      $this->_row->person = $data['person'];
      $this->_row->phone = $data['phone'];
      $this->_row->street = $data['street'];
      $this->_row->pop_number = $data['pop_number'];
      $this->_row->orient_number = $data['orient_number'];
      $this->_row->city = $data['city'];
      $this->_row->zip = $data['zip'];
      $this->_row->country = $data['country'];
      $this->_row->save();

      $customer = $this->getCustomer();
      if ($data['default_bill'] == 'y') {
        $customer->setBillingAddress($this->getId());
      }
      if ($data['default_ship'] == 'y') {
        $customer->setShippingAddress($this->getId());
      }
    }

    public function hasPhone() {
      return !empty($this->_row->phone);
    }

    public function getRegion() {
      $tAddress = new Table_Addresses();
      $address = $tAddress->getAddress($this->getId());
      return $address['region'];
    }

  }

