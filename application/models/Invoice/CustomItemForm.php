<?php

  class Invoice_CustomItemForm extends Zend_Form {

    public function __construct($options = null) {

      parent::__construct($options);

      $elemId = new Zend_Form_Element_Hidden('id_item');
      $elemId->setOrder(100);

      $elemInvoice = new Zend_Form_Element_Hidden('id_invoice');
      $elemInvoice->setOrder(101);

      $elemType = new Meduse_Form_Element_Select('type');
      $elemType
        ->setLabel('Typ')
        ->setRequired()
        ->setMultiOptions(array(
          Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_ADDITIONAL
          => Table_InvoiceCustomItems::$customItemTypeToString[Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_ADDITIONAL],
          Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING
          => Table_InvoiceCustomItems::$customItemTypeToString[Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING],
          Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_DISCOUNT
          => Table_InvoiceCustomItems::$customItemTypeToString[Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_DISCOUNT],
      ));
      
      $elemDescription = new Meduse_Form_Element_Text('description');
      $elemDescription
        ->setLabel('Popis')
        ->setRequired()
        ->addValidator(new Zend_Validate_NotEmpty());

      $elemPrice = new Meduse_Form_Element_Currency('price');
      $elemPrice
        ->setLabel('Cena bez DPH [EUR]')
        ->setRequired()
        ->setAttrib('title', 'Zadejte cenu bez daně v eurech.')
        ->addValidator(new Zend_Validate_Float());

      $elemVatIncluded = new Meduse_Form_Element_Checkbox('vat_included');
      $elemVatIncluded->setLabel('včetně DPH');

      $this->setAttrib('id', 'custom_item_form')
        ->setAction('/invoice/add-custom-item')
        ->addElement($elemId)
        ->addElement($elemInvoice)
        ->addElement($elemType)
        ->addElement($elemDescription)
        ->addElement($elemPrice)
        ->addElement($elemVatIncluded);
    }
    
    public function getValues($suppressArrayNotation = false) {
      $values = parent::getValues($suppressArrayNotation);
      $values['price'] = abs($values['price']);
      if ($values['type'] == Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_DISCOUNT) {
        $values['price'] = -$values['price'];
      }
      return $values;
    }
  }

  