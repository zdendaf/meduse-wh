<?php

/**
 * Fomular faktury
 *
 * @author niecj
 */
class Invoice_Form extends Meduse_FormBootstrap {

    public function init():void {

        $this->setAttrib('class', 'form');

        $element = new Zend_Form_Element_Hidden('id');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('order');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('invoice_type');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('proform_no');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('invoice_no');
        $element->setLabel('Číslo faktury');
        $element->setOptions(['maxlength' => '12']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('currency');
        $element->addMultiOptions([
                Table_Invoices::CURRENCY_CZK => Table_Invoices::$currencyToString[Table_Invoices::CURRENCY_CZK],
                Table_Invoices::CURRENCY_EUR => Table_Invoices::$currencyToString[Table_Invoices::CURRENCY_EUR],
            ]);
        $element->setValue(Table_Invoices::CURRENCY_EUR);
        $element->setLabel('Měna');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('language');
        $element->addMultiOptions([
                Table_Invoices::LANGUAGE_EN => Table_Invoices::$languageToString[Table_Invoices::LANGUAGE_EN],
                Table_Invoices::LANGUAGE_CS => Table_Invoices::$languageToString[Table_Invoices::LANGUAGE_CS],
            ]);
        $element->setLabel('Jazyk');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('allow_delivery_addr');
        $element->setLabel('Zobrazit dodací adresu');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('delivery_text');
        $element->setLabel('Text doručení');
        $element->setOptions(['maxlength' => '256']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('packaging');
        $element->setLabel('Balení');
        $element->setRequired(FALSE);
        $element->setOptions(['maxlength' => '256']);
        $element->setAttrib('readonly', 'readonly');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('packaging2');
        $element->setLabel('Balení (2.ř.)');
        $element->setOptions(['maxlength' => '256']);
        $element->setAttrib('readonly', 'readonly');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Float('weight');
        $element->setLabel('Hmotnost brutto [kg]');
        $element->setOptions(['maxlength' => '5']);
        $element->setAttrib('readonly', 'readonly');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Float('invoice_price');
        $element->setLabel('Záloha na faktuře [%]');
        $this->addElement($element);

        // redukce
        $element = new Meduse_Form_Element_Text('reduction');
        $element->setLabel('Redukce faktury [0-99%]');
        $element->addValidators([
                new Zend_Validate_Int(),
                new Zend_Validate_GreaterThan(-1),
                new Zend_Validate_LessThan(100),
            ]);

        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('mask_zero_prices');
        $element->setLabel('Maskovat nulové ceny');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('rounding');
        $element->setLabel('Zaokrouhlit');
        $this->addElement($element);


        $element = new Meduse_Form_Element_Checkbox('show_vat_summary');
        $element->setLabel('Vyčíslit DPH (jen pro EU)');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('show_logo');
        $element->setLabel('Zobrazit logo');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Float('additional_discount');
        $element->setLabel('Dodatečna sleva [%]');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('additional_discount_include');
        $element->setLabel('Rozpočítat slevu');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Textarea('text1');
        $element->setAttrib('cols', 18);
        $element->setAttrib('rows', 2);
        $element->setLabel('Text');
        $element->setOptions(['maxlength' => '500']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('add_address1_head');
        $element->setAttrib('maxlength', 15);
        $element->setLabel('Dodatečný adres. řádek — záhlaví');
        $element->setOptions(['maxlength' => '15']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('add_address1_data');
        $element->setAttrib('maxlength', 50);
        $element->setLabel('Dodatečný adres. řádek — data');
        $element->setOptions(['maxlength' => '50']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Text('add_address2_data');
        $element->setAttrib('maxlength', 50);
        $element->setLabel('Dodatečný adr. řádek — Consignee');
        $element->setOptions(['maxlength' => '50']);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('id_accounts');
        $element->setRequired(TRUE);
        $element->setLabel('Bankovní účet');
        $tAccount = new Table_Accounts();
        $accounts = $tAccount->fetchAll('id_wh_type = 1')->toArray();
        $default = NULL;
        foreach ($accounts as $account) {
            $element->addMultiOption($account['id'], $account['designation']);
            if ($account['is_default'] === 'y') {
                $default = $account['id'];
            }
        }
        $element->setValue([$default]);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Float('rate');
        $element->setRequired(TRUE);
        $element->setLabel('Kurz EUR/CZK');
        $element->setOptions(['maxlength' => '6']);
        $element->setAttrib('apended', '<button name="get_cnb_rate" id="get_cnb_rate" type="button" class="btn btn-primary">kurz ČNB</button>');
        $element->setAttrib('size', 2);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Checkbox('use_actual_rate');
        $element->setLabel('Používat aktuální kurz');
        $this->addElement($element);

        $element = new Meduse_Form_Element_DatePicker('issue_date', ['jQueryParams' => ['dateFormat' => 'dd.mm.yy']]);
        $element->setRequired(TRUE);
        $element->setValue(Zend_Date::now()->toString('dd.MM.yyyy'));
        $element->setLabel('Datum vystavení');
        $this->addElement($element);

        $element = new Meduse_Form_Element_DatePicker('duzp', ['jQueryParams' => ['dateFormat' => 'dd.mm.yy']]);
        $element->setRequired(FALSE);
        $element->setValue(Zend_Date::now()->toString('dd.MM.yyyy'));
        $element->setLabel('DUZP');
        $this->addElement($element);

        $element = new Meduse_Form_Element_DatePicker('due_date', ['jQueryParams' => ['dateFormat' => 'dd.mm.yy']]);
        $element->setLabel('Datum splatnosti');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Submit('Uložit');
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('seller');
        $element->addMultiOptions(Customers::getSellers());
        $element->setLabel('Prodávající');
        $this->addElement($element);

        parent::init();
    }

    public function mapFromRow($row) {
        if (isset($row['id'])) {
            $this->id->setValue($row['id']);
        }
        if (isset($row['id_orders'])) {
            $this->order->setValue($row['id_orders']);
        }
        if (isset($row['no'])) {
            $this->invoice_no->setValue($row['no']);
            $this->proform_no->setValue($row['no']);
        }
        if (isset($row['id_seller'])) {
            $this->seller->setValue($row['id_seller']);
        }
        if (isset($row['type'])) {
            $this->invoice_type->setValue($row['type']);
            switch ($row['type']) {
                case Table_Invoices::TYPE_OFFER:
                    $this->setLegend('Nastavení objednávky');
                    $this->invoice_no->setLabel('Číslo nabídky');
                    $this->removeElement('proform_no');
                    break;
                case Table_Invoices::TYPE_PROFORM:
                    $this->setLegend('Nastavení proformy č. ' . $row['no']);
                    $this->invoice_no->setAttrib('disabled', 'disabled');
                    $this->invoice_no->setLabel('Číslo proformy');
                    $this->removeElement('invoice_no');
                    break;
                case Table_Invoices::TYPE_REGULAR:
                    $this->setLegend('Nastavení faktury');
                    $this->removeElement('proform_no');
                    break;
            }
        }
        if (isset($row['currency'])) {
            $this->currency->setValue($row['currency']);
        }
        if (isset($row['language'])) {
            $this->language->setValue($row['language']);
        }
        if (isset($row['allow_delivery_addr'])) {
            $this->allow_delivery_addr->setValue($row['allow_delivery_addr']);
        }
        if (isset($row['delivery_text'])) {
            $this->delivery_text->setValue($row['delivery_text']);
        }
        if (isset($row['mask_zero_prices'])) {
            $this->mask_zero_prices->setValue($row['mask_zero_prices']);
        }
        if (isset($row['rounding'])) {
            $this->rounding->setValue($row['rounding']);
        }

        if (isset($row['show_vat_summary'])) {
            $this->show_vat_summary->setValue($row['show_vat_summary']);
        }

        if (isset($row['show_logo'])) {
          $this->show_logo->setValue($row['show_logo']);
        }

        if (isset($row['invoice_price'])) {
            $this->invoice_price->setValue($row['invoice_price']);
        }

        if (isset($row['reduction'])) {
            $this->reduction->setValue($row['reduction']);
        }

        if (isset($row['weight'])) {
            $this->weight->setValue($row['weight']);
        }
        if (isset($row['packaging'])) {
            $this->packaging->setValue($row['packaging']);
        }
        if (isset($row['packaging2'])) {
            $this->packaging2->setValue($row['packaging2']);
        }
        if (isset($row['additional_discount'])) {
            $this->additional_discount->setValue($row['additional_discount']);
        }
        if (isset($row['additional_discount_include'])) {
            $this->additional_discount_include->setValue($row['additional_discount_include']);
        }
        if (isset($row['issue_date'])) {
            $date = new Zend_Date($row['issue_date'], 'yyyy-MM-dd');
            $this->issue_date->setValue($date->toString('dd.MM.yyyy'));
        }
        if (isset($row['duzp'])) {
            $date = new Zend_Date($row['duzp'], 'yyyy-MM-dd');
            $this->duzp->setValue($date->toString('dd.MM.yyyy'));
        }
        if (isset($row['due_date']) && !is_null($row['due_date'])) {
            $date = new Zend_Date($row['due_date'], 'yyyy-MM-dd');
            $this->due_date->setValue($date->toString('dd.MM.yyyy'));
        }
        if (isset($row['text1'])) {
            $this->text1->setValue($row['text1']);
        }

        // Dodatecne radky adresy na fakture.
        if (isset($row['add_address1_head'])) {
            $this->add_address1_head->setValue($row['add_address1_head']);
        }
        if (isset($row['add_address1_data'])) {
            $this->add_address1_data->setValue($row['add_address1_data']);
        }
        if (isset($row['add_address2_data'])) {
            $this->add_address2_data->setValue($row['add_address2_data']);
        }

        if (!is_null($row['id_accounts'])) {
            $this->id_accounts->setValue($row['id_accounts']);
        }
        if (isset($row['use_actual_rate'])) {
            $this->use_actual_rate->setValue($row['use_actual_rate']);
            if ($row['use_actual_rate'] == 'y') {
                $this->rate->setAttrib('readonly', 'readonly');
            }
        }
        if (isset($row['rate']) || (isset($row['use_actual_rate']) && $row['use_actual_rate'] == 'y')) {
            $rate = isset($row['use_actual_rate']) && $row['use_actual_rate'] == 'y' ? Utils::getCnbRates('EUR') : $row['rate'];
            $this->rate->setValue($rate);
        }
        else {
            $tRate = new Table_ApplicationRate();
            $this->rate->setValue($tRate->getRate());
        }
    }

}
