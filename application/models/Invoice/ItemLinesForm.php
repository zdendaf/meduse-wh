<?php

  class Invoice_ItemLinesForm extends Meduse_FormSkove {

    protected $_lines = 0;
    protected $_invoiceId;
    protected $_idx = array();

    public function init() {
      $this->setAttribs(array('class' => 'form'));
      $this->setMethod(Zend_Form::METHOD_POST);
      $this->setAction('/invoice/save-item-texts');

      $element = new Zend_Form_Element_Hidden('invoice_id');
      $this->addElement($element);

      $element = new Meduse_Form_Element_Submit('Uložit');
      $this->addElement($element);

      parent::init();
    }

    public function countLines() {
      return $this->_lines;
    }

    public function setInvoiceId($id) {
      $this->_invoiceId = $id;
      $this->getElement('invoice_id')->setValue($this->_invoiceId);
    }

    public function getItem($idx) {
        return $this->{"item_${idx}"};
    }


    public function fill($data) {



      $this->_lines = count($data);
      for ($idx = 0; $idx < $this->_lines; $idx++) {

        $name = 'item_' . $idx;

        $element = new Meduse_Form_Element_Text($name);
        $element->setDecorators(array('ViewHelper'));
        if (isset($data[$idx]['type'])) {
          if ($data[$idx]['type'] == 1 || $data[$idx]['type'] == 2) {
            $element->setAttrib('readonly', 'readonly');
          }
        }
        if (isset($data[$idx]['id'])) {
          $this->_idx[$idx] = $data[$idx]['id'];

          // ID polozky faktury.
          $element2 = new Zend_Form_Element_Hidden('item_idx_' . $idx);
          $element2->setValue($data[$idx]['id']);
          $this->addElement($element2);

          // Cena polozky faktury
          $element3 = new Meduse_Form_Element_Currency('price_' . $idx);
          $element3->setAttrib('class', 'text-right');
          if (isset($data[$idx]['vat_included']) && $data[$idx]['vat_included'] && isset($data[$idx]['price_vat'])) {
            $value = $data[$idx]['price_vat'];
          }
          elseif (isset($data[$idx]['end_price'])) {
            $value = $data[$idx]['end_price'];
          }
          else {
            $value = 0;
          }
          $element3->setValue($value);
          $element3->setDecorators(array('ViewHelper'));
          if (isset($data[$idx]['type'])) {
            if ($data[$idx]['type'] == 2) {
              $element3->setAttrib('readonly', 'readonly');
            }
          }
          $this->addElement($element3);

          // Cena je vcetne dane?
          $element4 = new Meduse_Form_Element_Checkbox('vat_' . $idx);
          if (isset($data[$idx]['vat_included'])) {
            $element4->setValue($data[$idx]['vat_included'] ? 'y' : 'n');
          }
          $element4->setDecorators(array('ViewHelper'));
          if (isset($data[$idx]['type'])) {
            if ($data[$idx]['type'] == 2) {
              $element4->setAttrib('readonly', 'readonly');
            }
          }
          $this->addElement($element4);

          $element5 = new Zend_Form_Element_Hidden('type_' . $idx);
          $element5->setValue($data[$idx]['type']);
          $element5->setDecorators(array('ViewHelper'));
          $this->addElement($element5);
        }


        $text = $data[$idx]['text'];

        if ($data[$idx]['type'] == 0) {
          $look = $idx;
          while ($look + 1 < $this->_lines) {
            $look++;
            if (!isset($data[$look]['id']) && !isset($data[$look]['type'])) {
              $text .= ' ' . $data[$look]['text'];
              $idx++;
            } else {
              break;
            }
          }
        }
        $element->setValue($text);
        $element->setAttrib('size', '256');
        $element->setAttrib('style', 'width: 95%; font-family: monospace;');
        $this->addElement($element);
      }
    }

    public function getLabel($name) {
      return $this->getElement($name)->getLabel();
    }

  }

