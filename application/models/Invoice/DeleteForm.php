<?php

class Invoice_DeleteForm extends  Meduse_FormBootstrap {

  public function init() {

    $this->setAttrib('class', 'form');
    $this->setMethod(Zend_Form::METHOD_POST);

    $element = new Zend_Form_Element_Hidden('id');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('submit');
    $element->setLabel('Odstranit');
    $this->addElement($element);

    parent::init();
  }

}