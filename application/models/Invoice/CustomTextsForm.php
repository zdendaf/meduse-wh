<?php

class Invoice_CustomTextsForm extends Meduse_FormBootstrap {

    public function init($options = null) {
        
        $this->setAttrib('class', 'form');
        $this->setLegend('Vložení / editace uživatelského textu');
        $this->setMethod(Zend_Form::METHOD_POST);
        
        $element = new Zend_Form_Element_Hidden('id');
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Text('description');
        $element->setLabel('Popis');
        $element->setOptions(array('maxlength' => 50));
        $element->setRequired(true);
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('invoice_type');
        $element->setLabel('Doklad');
        $element->addMultiOptions(Table_InvoiceCustomTexts::getInvoiceTypes());
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('type');
        $element->setLabel('Zákazník');
        $element->addMultiOptions(array(
            Table_InvoiceCustomTexts::TYPE_B2B => 'byznys partner',
            Table_InvoiceCustomTexts::TYPE_B2C => 'koncový uživatel'
        ));
        $this->addElement($element);
        
        $element = new Meduse_Form_Element_Select('region');
        $element->setLabel('Region');
        $element->addMultiOptions(array(
            Table_InvoiceCustomTexts::REGION_EU => 'Evropská unie',
            Table_InvoiceCustomTexts::REGION_NON_EU => 'třetí země'
        ));
        $this->addElement($element);

        $element = new Meduse_Form_Element_Select('language');
        $element->setLabel('Jazyk');
        $element->addMultiOptions(array(
            Table_InvoiceCustomTexts::LANGUAGE_CZECH => 'česky',
            Table_InvoiceCustomTexts::LANGUAGE_ENGLISH => 'anglicky'
        ));
        $this->addElement($element);
        
        
        $element = new Meduse_Form_Element_Textarea('text');
        $element->setLabel('Text');
        $element->setOptions(array('rows' => '5'));
        $element->addValidator(new Zend_Validate_StringLength(0, 500));
        $this->addElement($element);

        $element = new Meduse_Form_Element_Submit('Uložit');
        $this->addElement($element);
        
        parent::init($options);
    }
}
