<?php

class Invoice_DeductForm extends Zend_Form {

  protected $candidates;
  protected $deductions;

  public function getCandidates(): array
  {
    $candidates = [];
    foreach ($this->candidates as $paymentId => $c) {
      if (isset($this->deductions[$paymentId])) {
        $candidates[$paymentId] = $this->deductions[$paymentId];
      } elseif ((float)$c['remain'] !== 0.0) {
        $candidates[$paymentId] = $c;
      }
    }
    return $candidates;
  }

  /**
   * @throws Invoice_Exceptions_InvoiceIdNoSet
   * @throws Invoice_Exceptions_NoPayments
   * @throws Invoice_Exceptions_BadType
   * @throws Zend_Form_Exception|Zend_Db_Statement_Exception
   * @throws Exception
   */
  public function __construct($options = NULL) {
    if (!isset($options['id_invoice']) || !$options['id_invoice']) {
      throw new Invoice_Exceptions_InvoiceIdNoSet('ID faktury musí být zadáno.');
    }
    $invoiceId = $options['id_invoice'];
    $invoice = new Invoice_Invoice($invoiceId);

    $this->candidates = $invoice->getPaymentsForDeduction(true);
    $this->deductions = $invoice->getPaymentsForDeduction();

    if ($this->candidates === FALSE) {
      throw new Invoice_Exceptions_BadType('Faktura musí být typu Regular');
    }

    if (empty($this->candidates)) {
      throw new Invoice_Exceptions_NoPayments('Neexistují žádné úhrady zálohových faktur.');
    }

    unset($options['id_invoice']);
    parent::__construct($options);

    $eInvoiceId = new Zend_Form_Element_Hidden('id_invoice');
    $this->addElement($eInvoiceId);

    foreach ($this->getCandidates() as $paymentId => $c) {

      if ((float)$c['remain'] === 0.0 && !isset($this->deductions[$paymentId])) {
        continue;
      }

      $label = 'Odečíst úhradu ' . $c['amount'] . ' ' . $c['currency'] . ' ze dne ' . Meduse_Date::dbToForm($c['date']);
      $checked = isset($this->deductions[$paymentId]);
      $ePayment = new Zend_Form_Element_Checkbox('payment_' . $c['id']);
      $ePayment->setLabel($label);
      $ePayment->setValue($checked);
      $ePayment->setAttribs([
        'data-payment-id' => $c['id'],
        'data-deduction-id' => $this->deductions[$paymentId]['id_payments_deductions'] ?? '',
      ]);
      $this->addElement($ePayment);

      $ePayment = new Meduse_Form_Element_Currency('payment_amount_' . $c['id']);
      $ePayment->setValue($this->deductions[$paymentId]['deducted'] ?? $c['remain']);
      $ePayment->setAttribs([
        'data-payment-id' => $c['id'],
        'data-deduction-id' =>$this->deductions[$paymentId]['id_payments_deductions'] ?? '',
        'data-max' => $c['remain'] + ($this->deductions[$paymentId]['deducted'] ?? 0)
      ]);
      $ePayment->setDecorators(array(
        'ViewHelper',
        'Description',
        'Errors',
      ));
      $this->addElement($ePayment);
    }

    $eSubmit = new Meduse_Form_Element_Submit('submit');
    $eSubmit->setAttrib('class', 'btn btn-success');
    $eSubmit->setLabel('Uložit odpočet');
    $this->addElement($eSubmit);

  }

  public function init(): void
  {
    parent::init();
    $this->setLegend('Odpočet zálohových faktur');
    $this->setAttrib('class', 'form');
  }

  public function __toString() {
    return $this->render();
  }

  public function render(Zend_View_Interface $view = NULL): string
  {
    if (is_null($view)) {
      $view = new Zend_View();
      $view->setScriptPath(APPLICATION_PATH . '/views/scripts');
    }
    $view->form ??= $this;
    $view->candidates ??= $this->getCandidates();
    return $view->render('invoice/deduct-form.phtml');
  }
}