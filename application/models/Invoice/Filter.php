<?php

class Invoice_Filter extends Meduse_FormBootstrap {

  public function init() {

    $this->setMethod('post');
    $this->setAttrib('class', 'form');

    $element = new Meduse_Form_Element_Text('title');
    $element->setLabel('Název');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Select('customer_type');
    $element->setLabel('Typ zákazníka');
    $element->addMultiOptions([
      '' => 'vše',
      Table_Invoices::CUSTOMER_TYPE_B2B => Table_Invoices::CUSTOMER_TYPE_B2B,
      Table_Invoices::CUSTOMER_TYPE_B2C => Table_Invoices::CUSTOMER_TYPE_B2C,
    ]);

    $this->addElement($element);

    $element = new Meduse_Form_Element_Text('no');
    $element->setLabel('Číslo faktury');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Select('type');
    $element->setLabel('Typ faktury');
    $element->addMultiOptions([
      '' => 'vše',
      Table_Invoices::TYPE_PROFORM => Table_Invoices::$typeToString[Table_Invoices::TYPE_PROFORM],
      Table_Invoices::TYPE_REGULAR => Table_Invoices::$typeToString[Table_Invoices::TYPE_REGULAR],
      Table_Invoices::TYPE_CREDIT => Table_Invoices::$typeToString[Table_Invoices::TYPE_CREDIT],
    ]);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Select('region');
    $element->setLabel('Region');
    $element->addMultiOptions([
      '' => 'vše',
      Table_Addresses::REGION_EU => Table_Addresses::REGION_EU,
      Table_Addresses::REGION_NON_EU => Table_Addresses::REGION_NON_EU,
    ]);
    $this->addElement($element);

    $element = new Meduse_Form_Element_Select('language');
    $element->setLabel('Jazyk');
    $element->addMultiOptions([
      '' => 'vše',
      Table_Invoices::LANGUAGE_CS => Table_Invoices::$languageToString[Table_Invoices::LANGUAGE_CS],
      Table_Invoices::LANGUAGE_EN => Table_Invoices::$languageToString[Table_Invoices::LANGUAGE_EN],
    ]);
    $this->addElement($element);

    $element = new Meduse_Form_Element_DatePicker('inserted_from');
    $element->setLabel('Vloženo od');
    $this->addElement($element);

    $element = new Meduse_Form_Element_DatePicker('inserted_to');
    $element->setLabel('Vloženo do');
    $this->addElement($element);

    $element = new Meduse_Form_Element_Submit('Hledat');
    $this->addElement($element);

    parent::init();
  }
}
