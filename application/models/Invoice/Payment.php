<?php

/**
 * @author Zdeněk Filipec <zdendaf@gmail.com>
 */
class Invoice_Payment {

  protected $_table = NULL;
  protected $_row = NULL;

  public static function getPayments($wh = NULL) {
    $table = new Table_Payments();
    return $table->getAllPayments($wh);
  }

  public static function removePayment($id) {
    $table = new Table_Payments();
    return $table->delete('id = ' . $id) > 0;
  }

  public static function getPaymentsGrid($wh = NULL, $params = array()) {

    $view = new Zend_View();

    $ctrlInvoice = $wh == Table_TobaccoWarehouse::WH_TYPE ? 'tobacco-invoice' : 'invoice';
    $ctrlOrder = $wh == Table_TobaccoWarehouse::WH_TYPE ? 'tobacco-orders' : 'orders';

		$gridArray = array(
			'css_class' => 'table table-hover',
      'curr_items_per_page' => 25,
      'curr_sort' => 'date',
      'curr_sort_order' => ZGrid::ORDER_DESC,
			'columns' => array(
				'invoice_no'   => array(
					'header' => _('Č. faktury'),
					'sorting' => TRUE,
					'renderer' => 'url',
					'url' => $view->url(array(
            'controller' => $ctrlInvoice, 'action' => 'download', 'id' => '{invoice_id}'
          )),
				),
				'invoice_type'   => array(
					'header' => _('Typ'),
					'sorting' => TRUE,
				),
				'order_no'   => array(
					'header' => _('Obj. č.'),
					'sorting' => TRUE,
					'renderer' => 'url',
					'url' => $view->url(array(
            'controller' => $ctrlOrder, 'action' => 'detail', 'id' => '{order_id}'
          )) . '#invoices',
				),
				'order_title'   => array(
					'header' => _('Název obj.'),
					'sorting' => FALSE,
					'renderer' => 'url',
					'url' => $view->url(array(
            'controller' => $ctrlOrder, 'action' => 'detail', 'id' => '{order_id}'
          )) . '#invoices',
				),
				'amount' => array(
					'header' => _('Výše úhrady'),
          'class' => 'text-right',
          'css_style' => 'text-align: right',
					'sorting' => TRUE,
          'renderer' => 'currency',
          'currency_options' => array(
            'currency_value' => 'amount',
            'currency_symbol' => 'currency'
          ),
				),
				'rate' => array(
					'header' => _('Kurz'),
          'css_style' => 'text-align: right',
					'sorting' => FALSE,
				),
				'date' => array(
					'header' => _('Datum úhrady'),
          'class' => 'text-center',
          'css_style' => 'text-align: center',
					'sorting' => TRUE,
          'renderer' => 'date',
				),
        'no' => array(
          'header' => _('Doklad'),
          'class' => 'text-center',
          'css_style' => 'text-align: center',
          'sorting' => TRUE,
          'renderer' => 'condition',
          'condition_value' => NULL,
          'condition_false' => array(
            'renderer' => 'url',
            'url' => $view->url(array(
              'controller' => $ctrlInvoice,
              'action' => 'payment-get-recipe',
              'id' => '{id}',
              'sort' => NULL,
              'sorder' => NULL,
            )),
          ),
        ),
        'edit' => array(
          'header' => _(''),
          'class' => 'text-center',
          'css_style' => 'text-align: center',
          'sorting' => FALSE,
          'renderer' => 'Bootstrap_Button',
          'icon' => 'icon-edit',
          'url' => $view->url(array(
            'controller' => $ctrlInvoice,
            'action' => 'payment-edit',
            'id' => '{id}',
            'sort' => NULL,
            'sorder' => NULL,
          )),
        ),
        'remove' => array(
          'header' => _(''),
          'class' => 'remove text-center',
          'css_style' => 'text-align: center',
          'sorting' => FALSE,
          'renderer' => 'Bootstrap_Button',
          'icon' => 'icon-remove',
          'url' => $view->url(array(
            'controller' => $ctrlInvoice,
            'action' => 'payment-remove',
            'id' => '{id}',
            'sort' => NULL,
            'sorder' => NULL,
          )),
        ),
      )
    );

    $table = new Table_Payments();
    $select = $table->getAllPayments($wh, $params, TRUE);
    $grid = new ZGrid($gridArray);
    $grid->setSelect($select);
    $grid->setRequest($params);

    return $grid;
  }


  public function __construct($data = NULL) {

    $this->_table = new Table_Payments();
    $this->_row = $this->_table->createRow();

    if (!is_null($data)) {
      if (is_array($data)) {
        $this->create($data);
      }
      else {
        $this->load((int) $data);
      }
    }
  }

  public function create(array $data) {
    $this->_row = $this->_table->createRow($data);
  }

  public function save($change_order = TRUE) {
    $no = $this->getNo();
    if ($no && $no !== $this->_row->no && !Table_Payments::checkNo($this->getNo(), $this->getWH())) {
      throw new Exception('Číslo dokladu ' . $this->getNo() . ' již existuje.');
    }
    $this->_row->save();
    if ($change_order) {
      $this->getInvoice()->getOrder()->checkPayments();
    }
  }

  public function load($id) {
    $this->_row = $this->_table->find($id)->current();
  }

  public function getRow() {
    return $this->_row;
  }

  public function getInvoice() {
    if ($this->getWH() == Table_PartsWarehouse::WH_TYPE) {
      return new Invoice_Invoice($this->_row->id_invoice);
    }
    else {
      return new Tobacco_Invoice_Invoice($this->_row->id_invoice);
    }
  }

  public function getOrder() {
    return $this->getInvoice()->getOrder();
  }

  public function getId(): int {
    return (int) $this->_row->id;
  }

  public function getDate($formated = FALSE) {
    return $formated ? Meduse_Date::dbToForm($this->_row->date) : $this->_row->date;
  }

  public function getAmount(): float {
    return (float) $this->_row->amount;
  }

  public function getPlacedAmount(): float {
    return (new Table_PaymentsDeductions())->getPlacedAmount($this->getId());
  }

  public function getRemainAmount(): float {
    return $this->getAmount() - $this->getPlacedAmount();
  }

  public function getRate() {
    return $this->_row->rate;
  }

  public function getCurrency() {
    return $this->_row->currency;
  }

  public function getNo() {
    return $this->_row->no;
  }

  public function hasNo(): bool
  {
    return !is_null($this->_row->no);
  }

  public function getMethod($readable = FALSE): string
  {
    if ($readable) {
      return $this->_row->method ? Table_Orders::$paymentToString[$this->_row->method] : Table_Orders::$paymentToString['unknown'];
    }
    return $this->_row->method;
  }

  public function getNote(): ?string
  {
    return $this->_row->note;
  }

  public function getWH() {
    return $this->_row->id_wh_type;
  }


  public function setData($data) {
    $this->_row->id_invoice = $data['id_invoice'];
    $this->_row->amount = $data['amount'];
    $this->_row->currency = $data['currency'];
    $this->_row->rate = $data['rate'];
    $this->_row->date = $data['date'];
    $this->_row->no = empty($data['no']) ? NULL : $data['no'];
    $this->_row->method = $data['method'];
    $this->_row->note = empty($data['note']) ? NULL : $data['note'];
    $this->_row->id_wh_type = $data['id_wh_type'];
  }

  /**
   * Vrati PDF s dokladem o platbe.
   */
  public function getRecipe() {

    // zdrojove objekty - proforma faktura a objednavka
    $invoice = $this->getInvoice();
    $order = $invoice->getOrder();

    // preklady, prevzate z faktur
    $translations = $invoice->getTranslations();
    $t = new Zend_Translate('array', array_merge(Invoice_Payment::$translationEng, $translations['eng']), 'en');
    $t->addTranslation(array_merge(Invoice_Payment::$translationCze, $translations['cze']), 'cs');
    $t->setLocale($invoice->getLanguage());

    // nacteni PDF sablony
    if ($invoice->getWHType() == Table_TobaccoWarehouse::WH_TYPE) {
      $source = Pdf_InvoicePayment::TEMPLATE_PAYMENT_TOBACCO;
    }
    else {
      $source = $invoice->showLogo() ? Pdf_InvoicePayment::TEMPLATE_PAYMENT_PIPES : Pdf_InvoicePayment::TEMPLATE_PAYMENT_PIPES_NOLOGO;
    }
    $pdf = Pdf_InvoicePayment::load($source);

    // nastaveni faktury
    $pdf->setOutput($invoice->getRegion(), $invoice->getCountry(), $invoice->getCustomerType(), $invoice->showVatSummary());
    $pdf->setLanguage($invoice->getLanguage());
    $pdf->setCurrency($this->getCurrency(), $t->translate('currency_name_' . $invoice->getCurrency()));
    $pdf->setRate($this->getRate());
    $pdf->setVat($this->getInvoice()->getVat());

    // titulek faktury
    $pdf->fillTitle($t->translate('title_payment_recipe'), array(447, 800), Meduse_Pdf::TEXT_ALIGN_CENTER);

    // dodavatel
    $supplier = $invoice->getSupplierAddress();
    $supplier[] = $invoice->getSupplierVatNumber();
    $pdf->fillTitle($t->translate('title_supplier'), array(35, 800), Meduse_Pdf::TEXT_ALIGN_LEFT);
    $pdf->fillTextBold(array(
      $t->translate('company_name'),
      $t->translate('address'),
      $t->translate('city_zip'),
      $t->translate('country'),
      $t->translate('vat_no')), array(45, 780), Meduse_Pdf::TEXT_ALIGN_LEFT, 2);
    $pdf->fillAddress($supplier, array(156, 780));

    // odberatel
    $customer = $invoice::prepareAddress($order->getCustomer(TRUE)->getBillingAddressId());
    $customer['phone'] = $invoice->getCustomerPhone();
    if ($invoice->getRegion() != Table_Addresses::REGION_NON_EU
      && $invoice->getCustomerType() != Table_Customers::TYPE_B2C) {
      $customer['vat'] = $invoice->getCustomerVatNumber();
      $customer['reg_no'] = $invoice->getCustomerIDNumber();
    }
    else {
      unset($customer['vat']);
      unset($customer['reg_no']);
    }

    $pdf->setAddress($customer, NULL);
    $pdf->fillTitle($t->translate('title_consignee'), array(35, 672), Meduse_Pdf::TEXT_ALIGN_LEFT);
    $pdf->fillCustomerAddress(array(
      'company' => ($invoice->getCustomerType() == 'B2B' ? $t->translate('company_name') : $t->translate('name')),
      'address' => $t->translate('address'),
      'city_zip' => $t->translate('city_zip'),
      'country' => $t->translate('country'),
      'phone' => $t->translate('phone'),
      'reg_no' => $t->translate('reg_no'),
      'vat_no' => $t->translate('vat_no'),
    ),
      array(45, 655), array(156, 655), NULL
    );

    // cislo faktury
    $pdf->fillTitle($t->translate('invoice_no_vs') . ': ' . $invoice->getNo(), array(344, 562), Meduse_Pdf::TEXT_ALIGN_LEFT);
    $pdf->fillTitle($t->translate('recipe_no') . ': ' . $this->getNo(), array(344, 535), Meduse_Pdf::TEXT_ALIGN_LEFT);

    // datum vystaveni a UZP
    $date = $this->getDate(TRUE);
    $pdf->setIssueDate($date);
    $pdf->setDUZP($date);
    $pdf->fillTextBold(array(
      is_null($pdf->getDueDate()) ? $t->translate('date_issue') : $t->translate('date_issue_due'),
      $t->translate('payment_method')), array(45, 545), Meduse_Pdf::TEXT_ALIGN_LEFT, 2.3);
    $pdf->fillDate(array(156, 545));

    // zpusob platby
    $payment = $invoice->getLanguage() == 'cs' ?
      Table_Orders::$paymentToString[$order->getPaymentMethod()] :
      Table_Orders::$paymentToStringEn[$order->getPaymentMethod()];
    $pdf->fillText($payment, array(156, 526), Meduse_Pdf::TEXT_ALIGN_LEFT, 2.3);

    $amount = $this->getAmount();
    $show_vat = $invoice->showVatSummary();
    $vat = $invoice->getVat();
    if ($show_vat) {
      $amount = $amount / (1 + $vat);
    }
    // jedina polozka faktury
    $items = array(array(
      'description' => $t->translate($this->getInvoice()->isProform() ? 'item_text_proforma' : 'item_text') . ' ' . $invoice->getNo(),
      'qty' => 1,
      'price' => $amount ,
    ));
    $pdf->prepare($items);
    $pdf->setProductTotal($amount);
    $pdf->printHeader($t);
    $pdf->printItems();

    // sumarizace
    $pdf->fillTotalPrice(array(
      'total'     => ($invoice->showVatSummary()) ? $t->translate('total_base') : $t->translate('total_amount'),
      'vat_tax'   => sprintf($t->translate('vat_tax'), $pdf->getVat() * 100),
      'rounding'  => $t->translate('rounding'),
      'total_vat' => ($invoice->getRegion() == 'non-eu') ? $t->translate('total_amount') : $t->translate('total_vat'),
      'deposit'   => sprintf($t->translate('deposit'), NULL),
      'currency'  => $t->translate('currency')
      ), array(339, 176), array(500, 176), array(561, 176));
    if ($this->_row->currency == Table_Invoices::CURRENCY_EUR) {
      $pdf->fillText(
        sprintf($t->translate('rate_by_cnb'), $this->getRate()), array(561, 112), Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }

    // info v paticce
    $pdf->fillText($t->translate('signature'), array(35, 100), Meduse_Pdf::TEXT_ALIGN_LEFT);
    $account = $invoice->getAccountInfo();
    if ($account) {
      $pdf->fillFooter(
        $t->translate('bank') . ': ' . $account['name'] . ', ' . $account['address']
        . ' / IBAN: ' . $account['iban'] . ' / BIC: ' . $account['bic']
        . ' / ' . $t->translate('account_no') . ': ' . $account['account_prefix']
        . '-' . $account['account_number'] . '/' . $account['bank_number'], array(300, 52));
    }

    return $pdf;
  }

  static protected $translationEng = array(
    'title_payment_recipe' => 'TAX RECIPE of received payment',
    'invoice_no_vs' => 'Invoice No. (VS)',
    'item_text_proform' => 'Prepay invoice payment advance payment no.',
    'item_text' => 'Payment of invoice no.',
    'recipe_no' => 'Tax Recipe No.',
  );
  static protected $translationCze = array(
    'title_payment_recipe' => 'DAŇOVÝ DOKLAD o přijaté platbě',
    'invoice_no_vs' => 'Číslo faktury (VS)',
    'item_text_proform' => 'Zálohová platba proforma faktury č.',
    'item_text' => 'Úhrada faktury č.',
    'recipe_no' => 'Číslo daňového dokladu',
  );

  /**
   * Vraci ID nebo objekt faktury, ze ktere se castka uhrady odecte.
   *
   * @param bool $returnObject
   * @return Invoice_Invoice|null|string|Tobacco_Invoice_Invoice
   * @throws Exception
   */
  public function getDeductedInvoice($returnObject = FALSE) {
    if (!$invoiceId = $this->_row->deduct_from) {
      return NULL;
    }
    if ($returnObject) {
      return $this->getWH() == Parts_Part::WH_TYPE_PIPES ? new Invoice_Invoice($invoiceId) : new Tobacco_Invoice_Invoice($invoiceId);
    }
    else {
      return $invoiceId;
    }
  }

  public function setDeductedInvoice($invoice, $deductionId, $amount) {
    if ($invoice instanceof Invoice_Invoice) {
      $deduct_from = $invoice->getId();
    }
    else {
      $deduct_from = $invoice ? (int) $invoice : NULL;
    }
    $dTable = new Table_PaymentsDeductions();
    if ($amount > 0) {
      // Změna odpočtu
      if ($deductionId) {
        $dTable->update([
          'deduct_from' => $deduct_from,
          'amount' => $amount,
          'id_payments' => $this->getId(),
        ], 'id = ' . $deductionId);
        return $deductionId;
      }
      else {
        // Novy odpocet.
        return $dTable->insert([
          'deduct_from' => $deduct_from,
          'amount' => $amount,
          'id_payments' => $this->getId(),
        ]);
      }
    }
    else {
      // Vymaz odpoctu.
      if ($deductionId) {
        $dTable->delete('id = ' . $deductionId);
      }
      return NULL;
    }
  }

  /**
   * Přesune úhradu k ostré faktuře v rámci příslušné objednávky.
   *
   * @throws Exception
   */
  public function moveTo($invoiceId): bool
  {
    $order = $this->getOrder();
    if (!$invoice = $order->getRegularInvoice()) {
      throw new Exception('Příslušná objednávka nemá založenou ostrou fakturu.');
    }
    if ($invoiceId !== $invoice->getId()) {
      throw new Exception('Ostrá faktura má jiné než požadované ID.');
    }
    $this->_row->id_invoice = $invoiceId;
    $this->_row->save();

    return TRUE;
  }
}
