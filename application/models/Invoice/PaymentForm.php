<?php

class Invoice_PaymentForm extends Meduse_FormBootstrap {

  protected $_invoice = NULL;
  protected ?Invoice_Payment $_payment = NULL;

  /**
   * @throws Exception
   */
  public function __construct($options = null) {
    if (isset($options['id_payment'])) {
      $this->_payment = new Invoice_Payment($options['id_payment']);
      $this->_invoice = $this->_payment->getInvoice();
    }
    if (isset($options['id_invoice'])) {
      $this->_invoice = new Invoice_Invoice($options['id_invoice']);
    }

    parent::__construct($options);
  }

  /**
   * @throws Zend_Form_Exception
   */
  public function init() {

    $this->setAttrib('class', 'form');

    $payment = new Zend_Form_Element_Hidden('id_payment');
    if ($this->_payment) {
      $payment->setValue($this->_payment->getId());
    }
    $this->addElement($payment);

    $invoice = new Zend_Form_Element_Hidden('id_invoice');
    if ($this->_invoice) {
      $invoice->setValue($this->_invoice->getId());
    }
    $this->addElement($invoice);

    $wh = new Zend_Form_Element_Hidden('wh');
    if ($this->_invoice) {
      $wh->setValue($this->_invoice->getWHType());
    }
    $this->addElement($wh);

    $amount = new Meduse_Form_Element_Currency('amount');
    $amount->setLabel('Částka');
    $amount->setRequired(TRUE);
    if ($this->_invoice) {
      $amount->setValue($this->_invoice->getUnpaidAmount());
    }
    $this->addElement($amount);

    $method = new Meduse_Form_Element_Select('method');
    $method->setLabel('Způsob platby');
    $method->addMultiOptions(Table_Orders::$paymentToString);
    $this->addElement($method);

    $currency = new Meduse_Form_Element_Select('currency');
    $currency->setLabel('Měna');
    $currency->setRequired(TRUE);
    $currency->setMultiOptions(array('CZK' => 'CZK', 'EUR' => 'EUR'));
    if ($this->_invoice) {
      $currency->setValue($this->_invoice->getCurrency());
      $currency->setAttrib('readonly', 'readonly');
    }
    $this->addElement($currency);

    $rate = new Meduse_Form_Element_Float('rate');
    $rate->setLabel('Kurz');
    $rate->setRequired(TRUE);
    if ($this->_invoice) {
      $rate->setValue($this->_invoice->getCurrency() == 'CZK' ? 1 : $this->_invoice->getRate());
    }
    $this->addElement($rate);

    $date = new Meduse_Form_Element_DatePicker('date');
    $date->setLabel('Datum úhrady');
    $date->setRequired(TRUE);
    $date->setValue(Meduse_Date::now()->get('d.M.Y'));
    $this->addElement($date);

    $fillNo = NULL;
    $no = NULL;
    if ($this->_invoice->getCountry() == Table_Addresses::COUNTRY_CZ) {
      $fillNo = new Meduse_Form_Element_Checkbox('fill_no');
      $fillNo->setLabel('Vystavit doklad');
      $fillNo->setChecked(FALSE);
      $this->addElement($fillNo);

      $no = new Meduse_Form_Element_Text('no');
      $no->setLabel('Číslo dokladu');
      $this->addElement($no);
    }

    $note = new Meduse_Form_Element_Text('note');
    $note->setLabel('Poznámka');
    $note->setAttrib('maxlength', 50);
    $this->addElement($note);

    $submit = new Meduse_Form_Element_Submit('submit');
    $submit->setLabel('Uložit úhradu');
    $this->addElement($submit);

    parent::init();

    if ($this->_payment) {
      $data = $this->_payment->getRow()->toArray();
      if (isset($data['date']) && !empty($data['date'])) {
        $data['date'] = Meduse_Date::dbToForm($data['date']);
      }
      $this->populate($data);
      if ($no && $fillNo) {
        if (empty($data['no'])) {
          $no->setAttrib('disable', 'disable');
          $no->setAttrib('data-no', Table_Payments::getNextNo($this->_invoice->getWHType()));
        } else {
          $fillNo->setValue(TRUE);
          $no->setAttrib('data-no', $data['no']);
        }
      }
    }
    else {
      if ($no && $fillNo) {
        $next = Table_Payments::getNextNo($this->_invoice->getWHType());
        $no->setAttrib('data-no', $next);
        $region = $this->_invoice->getRegion();
        $country = $this->_invoice->getCountry();
        $vatNo = $this->_invoice->getCustomerVatNumber();
        if ((!(($region == Table_Addresses::REGION_NON_EU)
          || ($region == Table_Addresses::REGION_EU && $country != Table_Addresses::COUNTRY_CZ && !empty($vatNo))))
          && !$this->_invoice->isRegular()
        ) {
          $no->setValue($next);
          $fillNo->setValue(TRUE);
        }
        else {
          $no->setAttrib('disable', 'disable');
          $fillNo->setValue(FALSE);
        }
      }
      $method->setValue($this->_invoice->getOrder()->getPaymentMethod());
    }
  }

  public function setLegend($value) {
    if ($this->_invoice) {
      $value .= ($this->_invoice->isRegular() ? ' k faktuře č. ' : ' k proformě č. '). $this->_invoice->getNo();
    }
    parent::setLegend($value);
  }
}
