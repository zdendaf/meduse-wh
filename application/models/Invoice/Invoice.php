<?php

class Invoice_Invoice {

    protected $_row = null;
    protected Orders_Order $_order;
    protected $_customer = null;
    protected $_seller = null;

    protected $priceTotal = 0;
    protected $productRows = array();
    protected $customRows = array();

    protected $versions = array();

    const PRODUCT_LINES_TYPE_NORMAL = 0;
    const PRODUCT_LINES_TYPE_HEADER = 1;
    const PRODUCT_LINES_TYPE_BONUS  = 2;
    const PRODUCT_LINES_TYPE_CUSTOM = 3;

    const DUE_DATE_DAYS = 14;

    const OSS_APPLICABLE_DATE = '2021-08-01';

  /**
   * @throws Orders_Exceptions_ForbiddenAccess
   * @throws Zend_Db_Table_Exception
   */
  public function __construct($invoiceId = null){
      if (is_null($invoiceId)) {
              return;
      }
      $invoices = new Table_Invoices();
      if (!($row = $invoices->find($invoiceId)->current())) {
        throw new Exception("Neexistuje faktura s ID = '$invoiceId'.");
      }
      $this->_row = $row;
      $this->_order = new Orders_Order($this->_row->id_orders);
      $this->_customer = $this->_order->getCustomer();
      $this->_seller = new Customers_Customer($this->_row->id_seller);

      // kontrola prirazeni ceniku u faktury produktu
      if (is_null($this->_row->id_pricelists)) {
        $pricelist_id = $this->_order->getPricelist();
        if (!$this->_order->isService() && is_null($pricelist_id)) {
          throw new Exception('Není přiřazen platný ceník');
        }
        $this->_row->id_pricelists = $pricelist_id;
        $invoices->update(array('id_pricelists' => $pricelist_id), 'id = ' . $invoiceId);
      }

      $this->generateProductRows();
      $this->generateCustomRows();

      $this->setVersions();
    }

    public function getId() {
        return $this->_row->id;
    }

    public function getRow() {
      return $this->_row;
    }

    public function getLanguage() {
        return $this->_row->language;
    }

    public function getRate(): float {
        return (float) $this->_row->rate;
    }

    public function  getIssueDate() {
      return $this->_row['issue_date'];
    }

    public function  getDUZP() {
      return $this->_row['duzp'];
    }

    public function getDueDate() {
      return $this->_row['due_date'];
    }

    public function getCurrency() {
      return $this->_row->currency;
    }

    // celkova sleva v procentech
    public function getAdditionalDiscount() {
      return $this->_row->additional_discount;
    }

    public function getCustomDiscount() {
      return (new Table_Invoices)->getCustomDiscount($this->getId());
    }

    public function getCustomDiscountText() {
      return (new Table_Invoices)->getCustomDiscountText($this->getId());
    }


    public function includeDiscount() {
      return $this->_row->additional_discount_include == 'y';
    }

    public function getType() {
        return $this->_row->type;
    }

    public function getTypeTranslated() {
      return Table_Invoices::$typeToString[$this->_row->type];
    }

    public function getOrder(): ?Orders_Order
    {
      return $this->_order;
    }

    public function getOrderType() {
      return $this->_order->getType();
    }

    public function orderIsService() {
      return $this->getOrderType() == Table_Orders::TYPE_SERVICE;
    }

    public function getUserText() {
        return $this->_row->text1;
    }

    public function setVersions() {
      $tVersions = new Table_InvoiceVersions();
      $this->versions = $tVersions->getVersions($this->getId());
    }

    public function getVersions() {
      return $this->versions;
    }

    public function getVersion($versionId) {
      foreach ($this->versions as $version) {
        if ($version['id'] == $versionId) {
          return $version;
        }
      }
      return null;
    }

    public function getLastVersion($force = false) {
      if ($force || $this->isValid()) {
        return empty($this->versions) ? NULL : end($this->versions);
      }

      return NULL;
    }

    public function setValid(): void
    {
      $this->_row->valid = 'y';
      $this->_row->save();
    }

    public function setInvalid(): void
    {
      $this->_row->valid = 'n';
      $this->_row->save();
    }

    public function isValid(): bool
    {
      return $this->_row->valid === 'y';
    }

    /**
     * Nastavi data faktury na zaklade objednavky daneho ID
     * @param int $orderId ID objednavky
     * @param string|null $type požadovaný typ faktury
     * @return int ID nové faktury
     * @throws Exception v pripade, ze se pokousime vygenerovat fakturu, ktera je v neodpovidajicim stavu
     */
    public function setOrder(int $orderId, string $type = NULL): int
    {
      $invoices = new Table_Invoices();
      $order = new Orders_Order($orderId);
      $row = $invoices->createRow(array(
        'id_orders' => $order->getID(),
        'id_wh_type' => $order->getWhType(),
        'show_excise' => 'y',
        'registered' => 'n',
        'use_actual_rate' => $order->isExpectedPaymentRateDefault() ? 'y' : 'n',
        'rate' => $order->getExpectedCurrency() === Table_Orders::CURRENCY_EUR ? $order->getExpectedPaymentRate() : 1,
        'issue_date' => date('Y-m-d'),
        'duzp' => date('Y-m-d'),
        'export' => $order->getCountry() === Table_Addresses::COUNTRY_CZ ? 'n' : 'y',
      ));
      $this->_row = $row;
      $this->_order = $order;
      $this->_customer = $this->_order->getCustomer();

      if (is_null($this->_customer)) {
        throw new Exception('Nelze generovat fakturu, k objednávce není přiřazen zákazník.');
      }

      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $this->_row->id_users = (int)$authNamespace->user_id;
      $this->_row->inserted = new Zend_Db_Expr('now()');
      $this->_row->last_change = new Zend_Db_Expr('now()');

      if (!is_null($type)) {
        if ($type === Table_Invoices::TYPE_REGULAR) {
          $this->_row->type = $this->_order->checkRegularCondition() ? Table_Invoices::TYPE_REGULAR : Table_Invoices::TYPE_PROFORM;
        }
        else {
          $this->_row->type = $type;
        }
      }
      else {
        $this->_row->type = $this->_order->getDefaultInvoiceType();
      }

      // podle typu zvolíme číslo
      switch ($this->_row->type) {
        case Table_Invoices::TYPE_PROFORM:
          $this->_order->generateProformaNo();
          $this->_row->no = $this->_order->getProformaNo();
          break;
        case Table_Invoices::TYPE_OFFER:
          $this->_row->no = $invoices->getNextNo($order, [
            'prefix_extra' => 'Q',
            'type' => Table_Invoices::TYPE_OFFER,
          ]);
          break;
        case Table_Invoices::TYPE_REGULAR:
          $this->_row->no = $invoices->getNextNo($order);
          break;
      }

      $previous = $this->getPrevious();
      if ($previous instanceof self) {
        $this->copySettingsFrom($previous);
      }
      else {
        $this->_row->id_pricelists = $this->_order->getPricelist();

        // automaticke prednastaveni zobrazeni DPH dle země nebo regionu
        if ($this->getCountry() === Table_Addresses::COUNTRY_CZ) {
          $this->_row->show_vat_summary = 'y';
        }
        elseif ($this->getRegion() === Table_Addresses::REGION_NON_EU) {
          $this->_row->show_vat_summary = 'n';
        }
        else {
          $this->_row->show_vat_summary = $order->getCustomer(TRUE)->isVerifiedVatNo() ? 'n' : 'y';
        }

        $tAddresses = new Table_Addresses();
        $address = $tAddresses->getAddress($this->_customer['id_addresses']);
        $country = $address['country'];

        $this->_row->language = $country === 'CZ' ? Table_Invoices::LANGUAGE_CS : Table_Invoices::LANGUAGE_EN;

        $expected = $this->_order->getExpectedPayment();
        $this->_row->invoiced_total = is_null($expected) ? 0 : $expected;

        $this->_row->currency = $this->_order->getExpectedCurrency();

        $this->_row->id_accounts = $this->_order->getDefaultAccount();

        $this->calculateVat();
      }

      // Vlozeni uzivatelskeho textu z sablony
      $this->setTextFromTemplate();

      if ($this->_row->type === Table_Invoices::TYPE_REGULAR) {
        if (!$this->_row->due_date) {
          $date = new Meduse_Date();
          $date->addDay(self::DUE_DATE_DAYS);
          $this->_row->due_date = $date->get('Y-m-d');
        }
      }

      // uložení nastavení
      $this->_row->save();

      if ($previous instanceof self) {
        $this->copyCustomItemsFrom($previous);
        $this->copyProductsItemsFrom($previous);
      }
      else if ($this->_order->getCarrier()) {
        $type = Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING;
        $description = $this->_order->getCarrierMethodFullName();
        $price = (float)$this->_order->getCarrierPrice() / $this->_row->rate;
        $data = [
          'type' => $type,
          'description' => $description,
          'price' => $price,
        ];
        $this->setCustomItem($data);
      }

      if ($previous && $previous->isProform()) {
        $this->setDeductionsFromProforma($previous);
      }

      return $this->getId();
    }

  /**
   * Najde pro fakturu jejiho pripadneho predka: pro proformu hleda jen v nabidkach,
   * pro ostrou fakturu hleda proformu a pak pripadne nabidku.
   *
   * @return Invoice_Invoice|null
   */
    protected function getPrevious(): ?Invoice_Invoice
    {
      switch ($this->_row->type) {
        case Table_Invoices::TYPE_REGULAR:
          $previousTypes = [Table_Invoices::TYPE_PROFORM, Table_Invoices::TYPE_OFFER];
          break;
          case Table_Invoices::TYPE_PROFORM;
            $previousTypes = [Table_Invoices::TYPE_OFFER];
            break;
        default:
          return NULL;
      }

      $previous = NULL;
      $invoices = $this->_order->getInvoices();

      if (in_array(Table_Invoices::TYPE_PROFORM, $previousTypes, true)) {
        $previous = $this->findProforma($invoices);
      }

      if (!$previous && in_array(Table_Invoices::TYPE_OFFER, $previousTypes, true)) {
        $previous = $this->findOffer($invoices);
      }

      return $previous;
    }

    private function findProforma(array $invoices): ?Invoice_Invoice {
      /** @var Invoice_Invoice $invoice */
      foreach ($invoices as $invoice) {
        if ($invoice->isProform()) {
          return $invoice;
        }
      }
      return null;
    }

  private function findOffer(array $invoices): ?Invoice_Invoice {
    /** @var Invoice_Invoice $invoice */
    foreach ($invoices as $invoice) {
      if ($invoice->isOffer()) {
        return $invoice;
      }
    }
    return null;
  }

    protected function copySettingsFrom(Invoice_Invoice $invoice): void
    {
      $this->_row->id_pricelists = $invoice->getPricelist();
      $this->_row->vat = $invoice->getVat();
      $this->_row->show_vat_summary = $invoice->showVatSummary() ? 'y' : 'n';
      $this->_row->language = $invoice->getLanguage();
      $this->_row->currency = $invoice->getCurrency();
      $this->_row->rate = $invoice->getRate();
      $this->_row->invoiced_total = $invoice->getInvoicedPrice(FALSE);
      $this->_row->additional_discount = $invoice->getAdditionalDiscount();
      $this->_row->additional_discount_include = $invoice->isAdditionalDiscountIncluded() ? 'y' : 'n';
      $this->_row->packaging = $invoice->getPackagingText();
      $this->_row->weight = $invoice->getWeightText();
      $this->_row->allow_delivery_addr = $invoice->allowDeliveryAddress() ? 'y' : 'n';
      $this->_row->delivery_text = $invoice->getDeliveryText();
      $this->_row->mask_zero_prices = $invoice->maskZeroPrices() ? 'y' : 'n';
      $this->_row->id_accounts = $invoice->getAccountId();
    }

    protected function maskZeroPrices() {
      return $this->_row->mask_zero_prices == 'y';
    }

    protected function copyCustomItemsFrom(Invoice_Invoice $proform) {
      $tCustom = new Table_InvoiceCustomItems();
      $cItems = $tCustom->select()
        ->from('invoice_custom_items', array(
          'id_invoice' => new Zend_Db_Expr($this->getId()),
          'type', 'description', 'price'))
        ->where('id_invoice = ?', $proform->getId())
        ->query(Zend_Db::FETCH_ASSOC)->fetchAll();
      foreach ($cItems as $item) {
        $tCustom->insert($item);
      }
    }

    protected function copyProductsItemsFrom(Invoice_Invoice $proform) {
      $tItems = new Table_InvoiceItems();
      $pItems = $tItems->select()
        ->from('invoice_items', array(
          'id_invoice' => new Zend_Db_Expr($this->getId()),
          'id_orders', 'id_parts', 'text', 'text_free', 'price', 'price_free'))
        ->where('id_invoice = ?', $proform->getId())
        ->query(Zend_Db::FETCH_ASSOC)->fetchAll();
      foreach ($pItems as $item) {
        $tItems->insert($item);
      }
    }

    public function getDeliveryText() {
      return $this->_row->delivery_text;
    }

    public function getPackagingText() {
      return $this->_row->packaging;
    }

    public function getWeightText() {
      return $this->_row->weight;
    }


    public function isAdditionalDiscountIncluded() {
      return $this->_row->additional_discount_include == 'y';
    }

    public function getText() {
      return $this->_row->text1;
    }

    public function setText($text, $save = TRUE) {
      $this->_row->text1 = $text;
      if ($save) {
        $this->_row->save();
      }
    }

    public function getOrderId() {
        return $this->_order->getID();
    }

    public function setCustomItem(array $data) {
        if (!isset($data['description']) || !isset($data['price'])) {
          return FALSE;
        }
        if (isset($data['type']) && !in_array($data['type'], array(
                Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_ADDITIONAL,
                Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_DELIVERY,
                Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING,
                Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_PACKAGING))) {
             $data['type'] = NULL;
        }
        $vatIncluded = isset($data['vat_included']) && ($data['vat_included'] === 'y' || $data['vat_included'] === TRUE);
        $rowData = array(
          'id_invoice' => $data['id_invoice'] ?? $this->getId(),
          'type' => $data['type'] ?? Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_ADDITIONAL,
          'description' => $data['description'],
          'price' =>  $vatIncluded ? $data['price'] / (1 + $this->getVat()) : $data['price'],
          'vat_included' => $vatIncluded ? 'y' : 'n',
        );
        $tItems = new Table_InvoiceCustomItems();
        if (isset($data['id_item']) && is_numeric($data['id_item'])) {
          $id = (int) $data['id_item'];
          $tItems->update($rowData, 'id = ' . $id);
        }
        else {
          $id = $tItems->insert($rowData);
        }
        return $id;
    }


    public function getParts() {
      if (is_null($this->_order)) {
        return null;
      }
      $tOrders = new Table_Orders();
      return $tOrders->getSortedOrderParts($this->_row->id_orders);
      }

    public function getCustomItems($type = Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_ADDITIONAL) {
      $items = [];
      $tItems = new Table_InvoiceCustomItems();
      $data = $tItems->getInvoiceItems($this->getId(), $type);
      if ($data) {
        foreach ($data as $row) {
          $items[] = [
            'text' => $row['description'],
            'cost' => (float) $row['price'],
            'vat_included' => $row['vat_included'] == 'y',
            'ctg_name' => $this->getLanguage() == 'cs' ? 'Dodatečné služby' : 'Additional services',
            'type' => $row['type'],
            'id' => $row['id'],
          ];
        }
      }

      if ($fees = $this->_order->getFees()) {
        foreach ($fees as $fee) {
          $items[] = [
            'text' => $this->getLanguage() == 'cs' ? $fee['text_cs'] : $fee['text_en'],
            'cost' => (float) $fee['price'],
            'vat_included' => FALSE,
            'ctg_name' => $this->getLanguage() == 'cs' ? 'Dodatečné služby' : 'Additional services',
            'type' => Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_ADDITIONAL,
            'id' => NULL,
          ];
        }
      }

      return $items;
    }

    public function removeShipping() {
      $tItems = new Table_InvoiceCustomItems();
      return $tItems->removeAllInvoiceItems($this->getId(), Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING);
    }


  public function getAccountId() {
      return $this->_row->id_accounts;
    }

    public function getAccountInfo() {
      $account = array();
      $tAccounts = new Table_Accounts();
      $accountRow = $tAccounts->find($this->_row->id_accounts)->current();
      if ($accountRow) {
        $account = $accountRow->toArray();
      }
      return $account;
    }

    public function getSupplierAddress() {
      $addrObj = $this->_seller->getBillingAddress();
      $address = self::prepareAddress($addrObj->getId());
      unset($address['person']);
      unset($address['country']);
      unset($address['phone']);
      return $address;
    }

    public function getRegion() {
      $tAddress = new Table_Addresses();
      $address = $tAddress->getAddress($this->_order->_address);
      return $address['region'];
    }

    public function getCountry() {
      $tAddress = new Table_Addresses();
      $address = $tAddress->getAddress($this->_order->_address);
      return $address['country'];
    }

    public function getSupplierVatNumber() {
      return $this->_seller->getVatNo();
    }

    public function getSupplierIDNumber() {
        return $this->_seller->getIdentNo();
    }

    public function getAddAddressRowHead() {
      return $this->_row->add_address1_head;
    }

    public function getAddAddressRow() {
      return $this->_row->add_address1_data;
    }

    public function getAddAddressRow2nd() {
      return $this->_row->add_address2_data;
    }

    public function getCustomerBillingAddress() {
      $address = self::prepareAddress($this->_customer['id_addresses'], $this->_customer['type']);
      unset($address['country']);
      unset($address['phone']);
      if ($value = $this->getAddAddressRow()) {
        $address['additional_row'] = $value;
      }
      return $address;
    }



  public function getCustomerDeliveryAddress() {
      if (isset($this->_order->_address)) {
        $address = self::prepareAddress($this->_order->_address, $this->_customer['type']);
        unset($address['country']);
        if ($value = $this->getAddAddressRow2nd()) {
          $address['additional_row'] = $value;
        }
        return $address;
      }
      return [];
    }

    public function getIssuerName() {
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      return $authNamespace->user_name_full;
    }

    public function allowDeliveryAddress() {
      return $this->_row->allow_delivery_addr == 'y';
    }

    public function getCustomerPhone() {
      return $this->_customer->phone;
    }

    public function getCustomerVatNumber() {
      return $this->_customer->vat_no;
    }

    public function getCustomerIDNumber() {
      return $this->_customer->ident_no;
    }

    public function getCustomerType() {
      return $this->_customer->type;
    }

    public function getCustomerId() {
      return $this->_customer->id;
    }

    public function getCustomer() {
      return new Customers_Customer($this->_customer->id);
    }

    public function getNo() {

      if ($this->isProform()) {
        $order = $this->getOrder();
        return $order ? $order->getProformaNo() : null;
      }

      return $this->_row->no;

    }

    /**
     * @throws Exception
     */
    public function setNo(?string $no, bool $save = true): self
    {
        if (!$this->isRegular() && !$this->isCredit()) {
            throw new RuntimeException('Only regular or credit invoice can be numbered.');
        }
        $this->_row->no = $no;
        if ($save) {
            $this->_row->save();
        }
        return $this;
    }


    /**
     * Vrací boolovskou hodnotu, zda-li se má vypočítávat DPH.
     *
     * Logika zaávisí na adrese zákazníka:
     * - DPH se vždy nezobrazuje, pokud je zákazník mimo EU (třetí země).
     * - DPH se vždy zobrazuje, pokud je zákazník z ČR
     * - DPH se zobrazuje pro ostatní země EU, pokud není ověřené VAT number.
     *
     * @return bool
     */
    public function showVatSummary(): bool
    {
      switch ($this->getCustomer()->getVatInvoice()) {
        case Table_Customers::VAT_INVOICED_AUTO:
          if ($this->getRegion() === Table_Addresses::REGION_NON_EU) {
            return FALSE;
          }

          if ($this->getCountry() === Table_Addresses::COUNTRY_CZ) {
            return TRUE;
          }

          return $this->_row->show_vat_summary === 'y' && !$this->isVatVerified();

        case Table_Customers::VAT_INVOICED_ALWAYS:
          return TRUE;

        case Table_Customers::VAT_INVOICED_NEVER:
        default:
          return FALSE;
      }
    }

    /**
     * Zobrazova logo na fakture?
     * @return bool
     */
    function showLogo() {
      return $this->_row->show_logo == 'y';
    }

    /**
     * Nastaví hodnotu podle pravidel.
     * @see showVatSummary
     */
    public function setShowVatSummary($bool) {
      if ($this->getRegion() == Table_Addresses::REGION_NON_EU) {
        $bool = FALSE;
      }
      elseif ($this->getCountry() == Table_Addresses::COUNTRY_CZ) {
        $bool = TRUE;
      }
      else {
        $bool = !$this->isVatVerified();
      }
      $this->_row->show_vat_summary = $bool ? 'y' : 'n';
      $this->_row->save();
    }

    public function getShipping() {
      $cost = 0;
      $text = array();
      $tItems = new Table_InvoiceCustomItems();
      $data = $tItems->getInvoiceItems($this->getId(), Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING);
      if ($data) foreach ($data as $row) {
        $price = (float) $row['price'];
        $cost += $price;
        $text[] = $row['description'];
      }
      return array(
        'show' => !empty($text),
        'cost' => $cost,
        'text' => implode(", ", $text)
      );
    }

    public function getPackaging(): array
    {
      $t = new Zend_Translate('array', self::$translationEng, 'en');
      $t->addTranslation(self::$translationCze, 'cs');
      $t->setLocale($this->getLanguage());

      $cost = 0;
      $text = array();
      $tItems = new Table_InvoiceCustomItems();
      $data = $tItems->getInvoiceItems($this->getId(), Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_PACKAGING);
      if ($data)
        foreach ($data as $row) {
          $cost += (float) $row['price'];
          $text[] = $row['description'];
        }
      $ret = array();
      if (count($text) > 2) {
        $ret[0] = array_shift($text);
        $ret[1] = implode(', ', $text);
      } else {
        $ret = $text;
      }

      $orderPackaging = $this->_order->getPackaging(true);
      $packages = [];
      if ($orderPackaging['items']) {
        $types = [];
        foreach ($orderPackaging['items'] as $item) {
          $type = $item['type'];
          if (!array_key_exists($type, $types)) {
            $types[$type] = 0;
          }
          $types[$type]++;
        }
        foreach ($types as $type => $count) {
          $packages[] = $count  . ' x ' . $t->translate($type);
        }
      }

      return [
        'cost' => $cost,
        'text' => $ret,
        'weight' => $orderPackaging['weight'],
        'packages' => implode(' + ', $packages),
      ];
    }

    public function getDelivery() {
      $cost = 0;
      $text = array();
      $tItems = new Table_InvoiceCustomItems();
      $data = $tItems->getInvoiceItems($this->getId(), Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_DELIVERY);
      if ($data)
        foreach ($data as $row) {
          $cost += (float) $row['price'];
          $text[] = $row['description'];
        }
      return array(
        'cost' => $cost,
        'text' => implode(", ", $text)
      );
    }

    protected function _prepareParts() {
      $t = new Zend_Translate('array', Invoice_Invoice::$translationEng, 'en');
      $t->addTranslation(Invoice_Invoice::$translationCze, 'cs');
      $t->setLocale($this->getLanguage());
      $parts = $this->getParts()->toArray();
      // slouceni a preklad kategorii pro potreby fakturace
      foreach ($parts as $key => $part) {
        switch ($part['ctg_id']) {
          case Table_PartsCtg::TYPE_SETS:
            $parts[$key]['ctg_name'] = $t->translate('ctg_pipe_sets');
            break;
          case Table_PartsCtg::PRESENTATION_TOOLS:
            $parts[$key]['ctg_name'] = $t->translate('ctg_presentation_tools');
            break;
          default:
            $parts[$key]['ctg_name'] = $t->translate('ctg_extra_accessories');
            $parts[$key]['ctg_id'] = 9999; //dummy id
            break;
        }
      }
      return $parts;
    }

    public function prepareParts() {

      $ossApplicable = $this->isOssApplicable();
      $ossCoefficient = $this->getOssCoefficient();
      $addVat = $this->isAddedVatInPrice();
      $priceList = $this->getPricelist();

      $parts = $this->_prepareParts();
      $savedTexts = $this->getItemTexts();
      $savedPrices = $this->getItemPrices();
      $text = array();
      $price = array();
      $tItems = new Table_InvoiceItems();

      foreach ($parts as $part) {
        $id_parts = $part['id_products'];
        $insert = array();

        // Texty polozek faktury
        if (isset($savedTexts[$part['id_products']])) {
          // Ulozene.
          $text[] = array(
            $savedTexts[$part['id_products']][0],
            $savedTexts[$part['id_products']][1] ?? $savedTexts[$part['id_products']][0]
          );
        }
        else {
          // Neulozene, budeme ukladat.
          $text[] = array($part['name'], $part['name']);
          $insert['text'] = $part['name'];
          $insert['text_free'] = $part['name'];
        }

        // Ceny polozek faktury.
        if (isset($savedPrices[$part['id_products']])) {
          // Ulozene.
          $price[] = array(
            $savedPrices[$part['id_products']][0],
            $savedPrices[$part['id_products']][1] ?? $savedPrices[$part['id_products']][0],
            $savedPrices[$part['id_products']][2] ?? 'n',
          );
        }
        else {
          // Neulozene, budeme ukladat.
          $part = new Parts_Part($part['id_products']);
          $list_price = $part->getEndPrice($priceList, $addVat) * $ossCoefficient;
          $price[] = array($list_price, $list_price, $ossApplicable ? 'y' : 'n');
          $insert['price'] = $list_price;
          $insert['price_free'] = $list_price;
          $insert['vat_included'] = $ossApplicable ? 'y' : 'n';
        }

        if ($insert) {
          $tItems->insertUpdate(array(
            'id_invoice' => $this->getId(),
            'id_parts' => $id_parts,
          ) + $insert, true);
        }
      }

      foreach ($text as $count => $item) {
        $parts[$count]['text'] = $item;
      }
      foreach ($price as $count => $item) {
        $parts[$count]['price'] = $item;
      }

      return $parts;
    }

    public function getItemLines() {
      return $this->productRows;
    }

    /**
     * Ma faktura produktove radky?
     * @return boolean
     */
    public function hasItemLines() {
      return empty($this->productRows);
    }

    /**
     * Testuje, zda-li je na fakture produktova polozka dane kategorie.
     * @param int|array $ctg
     *      ID kategorie pro set
     * @see Table_PartsCtg
     *
     * @return boolean
     */
    public function hasProductOfCategory($ctg) {
      if (!is_array($ctg)) {
        $ctg = array($ctg);
      }
      $has = FALSE;
      if (!empty($this->productRows)) {
        foreach ($this->productRows as $item) {

          if (isset($item['id'])) {
            $product = new Pipes_Parts_Part($item['id']);
            if (in_array($product->getSuperCtg(), $ctg)) {
              $has = TRUE;
              break;
            }
          }
        }
      }
      return $has;
    }

    public function saveItemTexts($data) {
      $tItems = new Table_InvoiceItems();
      if ($data) {
        foreach ($data as $id_parts => $row) {
          $tItems->insertUpdate(array(
            'id_parts' => $id_parts,
            'id_invoice' => $this->getId(),
            'text' => $row[0],
            'text_free' => $row[1] ?? $row[0],
          ));
        }
      }
    }

    public function saveItemPrices($data) {
      $tItems = new Table_InvoiceItems();
      if ($data) {
        $v = 1 + $this->getVat();
        foreach ($data as $id_parts => $row) {
          $vatIncluded = isset($row['vat_included']) && $row['vat_included'] ? 'y' : 'n';
          $price = $vatIncluded === 'y' ? $row['price'] / $v : $row['price'];
          $price_free = isset($row['price_free']) ? ($vatIncluded ? $row['price_free'] / $v : $row['price_free']) : $price;
          $tItems->insertUpdate(array(
            'id_parts' => $id_parts,
            'id_invoice' => $this->getId(),
            'price' => $price,
            'price_free' => $price_free,
            'vat_included' => $vatIncluded,
          ));
        }
      }
      $this->getInvoicedPrice();
    }

    public function getItemTexts() {
      $texts = array();
      $tItems = new Table_InvoiceItems();
      $data = $tItems->fetchAll('id_invoice = ' . $this->getId())->toArray();
      if ($data) {
        foreach ($data as $row) {
          $texts[$row['id_parts']] = array(
            0 => $row['text'],
            1 => $row['text_free'],
          );
        }
      }
      return $texts;
    }

    public function getItemPrices() {
      $prices = array();
      $orderId = $this->getOrder()->getID();
      /** @var \Zend_Db_Select $select */
      $select = Zend_Registry::get('db')->select();
      $select->from(array('ii' => 'invoice_items'), array('id_parts', 'price', 'price_free', 'vat_included'))
        ->join(array('op' => 'orders_products'),
          'op.id_products = ii.id_parts AND op.id_orders = ' . $orderId, array())
        ->where('ii.id_orders = ?', $orderId)
        ->where('ii.id_invoice = ?', $this->getId());
      if ($data = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll()) {
        foreach ($data as $row) {
          $prices[$row['id_parts']] = array(
            0 => $row['price'],
            1 => $row['price_free'],
            2 => $row['vat_included'],
          );
        }
      }
      return $prices;
    }

    public function generatePdf($reduction = 0) {

      // Test nenulovych cen pro objednavky mimo EU
      if ($this->getRegion() == Table_Addresses::REGION_NON_EU) {
        $this->testZeroProductPrices(TRUE);
      }

      $t = new Zend_Translate('array', Invoice_Invoice::$translationEng, 'en');
      $t->addTranslation(Invoice_Invoice::$translationCze, 'cs');
      $t->setLocale($this->getLanguage());

      $data = array();

      $data['parts'] = $this->prepareParts();
      $data['custom'] = $this->getCustomItems();

      $data['supplier'] =   $this->getSupplierAddress();
      $data['supplier'][] = $this->getSupplierVatNumber();
      $data['supplier'][] = $this->getSupplierIDNumber();

      $data['billing'] = $this->getCustomerBillingAddress();
      $data['delivery'] = $this->getCustomerDeliveryAddress();
      if (is_null($data['delivery'])) {
        $data['delivery'] = $data['billing'];
      }
      $data['billing']['phone'] = $this->getCustomerPhone();
      if ($this->getRegion() != Table_Addresses::REGION_NON_EU
        && $this->getCustomerType() != Table_Customers::TYPE_B2C) {
        $data['billing']['vat'] = $this->getCustomerVatNumber();
        $data['billing']['reg_no'] = $this->getCustomerIDNumber();
      } else {
        unset($data['billing']['vat']);
        unset($data['billing']['reg_no']);
      }
      $data['payment'] = $this->getLanguage() == 'cs' ?
        Table_Orders::$paymentToString[$this->_order->getRow()->payment_method] :
        Table_Orders::$paymentToStringEn[$this->_order->getRow()->payment_method];
      $data['carrier'] = $this->_order->getCarrier();
      $data['invoice'] = $this->_row->toArray();

      $data['deductions'] = $this->getPaymentsForDeduction();

      $cType = $this->getCustomerType();
      $region = $this->getRegion();
      $country = $this->getCountry();

      $pdf = $this->getPdf($region);

      if ($this->showVatSummary()) {
        $pdf->setVat($this->getVat());
      }

      // nastaveni cisla faktury
      if ($this->isProform()) {
        $no = $t->translate('pagination_number') . ' '
          . $this->_order->getProformaNo() . ' – '
          . $t->translate('order_number') . ' ' . $this->_order->getNO();
      }
      elseif ($this->isOffer()) {
        $no = $t->translate('pagination_number_quotation') . ' '
          . $this->getNo() . ' – '
          . $t->translate('order_number') . ' ' . $this->_order->getNO();
      }
      else {
          $no = $t->translate('pagination_number') . ' '
            . $this->getNo() . ' – '
            . $t->translate('order_number') . ' ' . $this->_order->getNO();
      }
      $pdf->setNo($no);

      // nastaveni vyspupu
      $pdf->setOutput($region, $country, $cType, $this->showVatSummary());
      $pdf->setLanguage($this->getLanguage());
      $pdf->setCurrency($this->_row->currency, $t->translate('currency_name_' . $this->_row->currency));
      $pdf->setRate($this->getRate());


      $pdf->setReduction($reduction);

      $pdf->setItemDiscount($this->getCustomDiscount(), $this->getCustomDiscountText());
      $pdf->setDiscount($this->getAdditionalDiscount(), $this->includeDiscount());

      $pdf->setIssueDate($this->_row['issue_date']);
      if (!$this->isOffer() && !$this->isProform()) {
        $pdf->setDUZP($this->_row['duzp']);
      }
      $pdf->setDueDate($this->_row['due_date']);

      if ($this->orderIsService()) {
        $pdf->prepare($this->_order->getServices(), $this->customRows);
      }
      else {
        $pdf->prepare($this->productRows, $this->customRows);
      }
      $pdf->setProductTotal($this->getPriceTotal());
      $pdf->setDeposit($this->_row->invoice_price);

      $pdf->setDeductions($data['deductions']);

      $shipping = $this->getShipping();
      $pdf->setShipping($shipping);

      $packaging = $this->getPackaging();
      $pdf->setPackaging(array(
        'text' => $packaging['packages'],
        'weight' => $packaging['weight'],
        'weight_unit' => 'kg',
        'cost' => $packaging['cost']
      ));
      $pdf->setAddress($data['billing'], $data['delivery']);

      $pdf->setVat($this->getVat());

      $pdf->setShowHs($this->getRegion() == Table_Addresses::REGION_NON_EU);

      // titulek faktury
      switch ($this->getType()) {
        case Table_Invoices::TYPE_PROFORM:
          $text = $t->translate('title_proform');
          break;
        case Table_Invoices::TYPE_OFFER:
          $text = $t->translate('title_offer');
          break;
        case Table_Invoices::TYPE_CREDIT:
          $text = $t->translate('title_credit');
          break;
        case Table_Invoices::TYPE_REGULAR:
        default:
          $text = $t->translate('title_invoice');
          break;
      }
      $pdf->fillTitle($text, array(447, 800), Meduse_Pdf::TEXT_ALIGN_CENTER);

      // box dodavatel
      $pdf->fillTitle($t->translate('title_supplier'), array(35, 800), Meduse_Pdf::TEXT_ALIGN_LEFT);
      $pdf->fillTextBold(array(
        $t->translate('company_name'),
        $t->translate('address'),
        $t->translate('city_zip'),
        $t->translate('country'),
        $t->translate('vat_no'),
        $t->translate('reg_no'),
        ), array(45, 780), Meduse_Pdf::TEXT_ALIGN_LEFT, 2);
      $pdf->fillAddress($data['supplier'], array(120, 780));

      $deliveryCoords = null;
      if (!$this->orderIsService()) {
        // box odberatel - dodaci adresa
        $pdf->fillTitle($t->translate('delivery_address'), array(344, 672), Meduse_Pdf::TEXT_ALIGN_LEFT);
        if ($data['delivery'] && $this->allowDeliveryAddress()) {
          $deliveryCoords = array(350, 655);
        } else {
          $deliveryCoords = null;
          $text = $this->_row['delivery_text'] ?
            $this->_row['delivery_text'] : $t->translate('same_as_consignee');
          $pdf->fillText($text, array(350, 650), Meduse_Pdf::TEXT_ALIGN_LEFT, 2);
        }
      }

      // box odberatel - fakturace
      $pdf->fillTitle($t->translate('title_consignee'), array(35, 672), Meduse_Pdf::TEXT_ALIGN_LEFT);

      $transArray = array(
        'company' => $this->getCustomerType() == 'B2B' ? $t->translate('company_name') : $t->translate('name'),
        'person' => $t->translate('contact_person'),
        'address' => $t->translate('address'),
        'city_zip' => $t->translate('city_zip'),
        'country' => $t->translate('country'),
        'phone' => $t->translate('phone'),
        'reg_no' => $t->translate('reg_no'),
        'vat_no' => $t->translate('vat_no'),
      );
      if (isset($data['billing']['additional_row']) || isset($data['delivery']['additional_row'])) {
        $head = $this->getAddAddressRowHead();
        $transArray['additional_row'] = $head ? $head : '';
      }
      $pdf->fillCustomerAddress($transArray, array(45, 655), array(120, 655), $deliveryCoords);

      // box datum vystaveni, DUZP, splatnost a box zpusobu platby
      $pdf->fillTextBold(array(
        $this->getDateLabel($t),
        $t->translate('payment_method')), array(45, 545), Meduse_Pdf::TEXT_ALIGN_LEFT, 2.3);
      $pdf->fillDate(array(156, 545));
      $pdf->fillText($data['payment'], array(156, 526), Meduse_Pdf::TEXT_ALIGN_LEFT, 2.3);

      // box cislo faktury
      switch ($this->getType()) {
        case Table_Invoices::TYPE_OFFER:
          $pdf->fillTitle($t->translate('quotation_no') . ': ' . $this->getNo(), array(344, 562));
          break;
        case Table_Invoices::TYPE_PROFORM:
          $pdf->fillTitle($t->translate('proform_no') . ': ' . $this->_order->getProformaNo(), array(344, 562));
          break;
        case Table_Invoices::TYPE_REGULAR:
          $pdf->fillTitle($t->translate('invoice_no') . ': ' . $this->getNo(), array(344, 562));
          break;
        case Table_Invoices::TYPE_CREDIT:
          $tInvoice = new Table_Invoices;
          $source = $tInvoice->getInvoiceByOrder($this->getOrderId());
          if ($source && !is_null($source['no'])) {
            $pdf->fillTitle($t->translate('invoice_no') . ': ' . $source['no'], array(344, 580));
          }
          $pdf->fillTitle($t->translate('credit_no') . ': ' . $this->getNo(), array(344, 562));
          break;
      }

      if (!$this->orderIsService()) {
        // box baleni
        $pdf->fillPackaging(array(
          $t->translate('packaging'), '',
          $t->translate('total_weight')), array(344, 545), array(550, 545));
      }

      // hlavicka polozek
      $pdf->printHeader($t);

      // vypis polozek
      $pdf->printItems();

      // vypis polozkove slevy, nakladu na prepravu/balneho a dodatecne slevy
      $pdf->fillNonItemsPrice(array(
        'item_discount' => $pdf->getItemDiscountText(),
        'shipping' => $t->translate('shipping_costs_1'),
        'additional_discount' => sprintf($t->translate('additional_discount'), $pdf->getDiscount()),
      ), array(35, 227), array(505, 227), array(561, 227));


      $pdf->fillTotalPrice(array(
        'total'       => ($this->showVatSummary()) ? $t->translate('total_base') : $t->translate('total_amount'),
        'vat_tax'     => sprintf($t->translate('vat_tax'), $pdf->getVat() * 100),
        'deduction'   => $t->translate('deduction'),
        'deduction_vat' => $t->translate('deduction_vat'),
        // 'rounding'    => $t->translate('rounding'),
        'total_vat'   => ($this->getRegion() == 'non-eu') ? $t->translate('total_amount') : $t->translate('total_vat'),
        'deposit'     => sprintf($t->translate('deposit'), $pdf->getDepositPercentage()),
        'currency'    => $t->translate('currency'),
        'remains' => $t->translate('remains'),
        ), array(339, 188), array(500, 188), array(561, 188));

      if ($this->_row->currency == Table_Invoices::CURRENCY_EUR) {
        $pdf->fillText(
          sprintf($t->translate('rate_by_cnb'), $this->getRate()), array(561, 112), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }

      $pdf->fillNotice($this->getUserText(), array(35, 188));
      $pdf->fillText($t->translate('signature'), array(35, 100), Meduse_Pdf::TEXT_ALIGN_LEFT);

      // paticka
      $account = $this->getAccountInfo();
      if ($account) {
        $pdf->fillFooter(
          $t->translate('bank') . ': ' . $account['name'] . ', ' . $account['address']
          . ' / IBAN: ' . $account['iban'] . ' / BIC: ' . $account['bic']
          . ' / ' . $t->translate('account_no') . ': ' . $account['account_prefix']
          . '-' . $account['account_number'] . '/' . $account['bank_number'], array(300, 52));
      }

      // Verze ukladame jen pro neredukovane PDF.
      if (!$reduction) {
        $filePath = $this->_saveFile($pdf);
        $this->_addVersion($filePath);
        $this->setValid();
      }
      return $pdf;
    }


    protected function _addVersion($filePath) {
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $tVersions = new Table_InvoiceVersions();
      $tVersions->insert(array(
        'id_invoice' => $this->getId(),
        'filename' => $filePath,
        'id_users' => $authNamespace->user_id,
      ));
      $this->setVersions();
    }

  /**
   * Rutina ukladajici PDF soubor s fakturou do filesystemu
   * @param Pdf_Invoice|Pdf_InvoiceServices $pdf
   * @return string
   * @throws Zend_Pdf_Exception|Zend_Exception
   */
    protected function _saveFile($pdf): string
    {
      $filename = $this->_generateFilename();
      $date = new Meduse_Date();
      $path = implode('/', array(
        Zend_Registry::get('config')->data->path,
        'invoices',
        $date->toString('Y-m'),
      ));
      if (!mkdir($path, 041777, true) && !is_dir($path)) {
        throw new RuntimeException(sprintf('Directory "%s" was not created', $path));
      }
      $filePath =  $path . '/' . $filename . '.pdf';
      $pdf->save($filePath);
      return $filePath;
    }

  /**
   * Routine for generating the invoice's filename.
   *
   * @return string
   * @throws Zend_Date_Exception
   * @throws Zend_Db_Table_Exception
   */
    protected function _generateFilename(): string
    {
      // Invoice number.
      if ($this->isOffer()) {
        $filename[] = $this->getLanguage() === 'en' ? 'quotation' : 'nabidka';
      }
      elseif ($no = $this->getNo()) {
        $filename[] = $no;
      }

      // Date.
      $date = new Meduse_Date($this->getIssueDate(), 'Y-m-d');
      $filename[] = $date->toString('d_m_Y');

      // Company.
      $tCustomers = new Table_Customers();
      $rCustomer = $tCustomers->find($this->getCustomerId())->current();
      if ($company = $rCustomer->company ?? null) {
        setlocale(LC_CTYPE, 'cs_CZ.utf-8');
        $filename[] = strtr(iconv('UTF-8', 'ASCII//TRANSLIT', $company), array(' ' => '-', "'" => '', ',' => '', '"' => ''));
      }

      // Country.
      $tAddresses = new Table_Addresses();
      if ($rCustomer) {
        $rAddress = $tAddresses->find($rCustomer->id_addresses)->current();
        if ($country = $rAddress->country ?? null) {
          $filename[] = $country;
        }
      }

      // Order number.
      $tOrders = new Table_Orders();
      $rOrder = $tOrders->find($this->getOrderId())->current();
      if ($orderNo = $rOrder->no ?? null) {
        $filename[] = $orderNo;
      }

      // Version.
      $filename[] = count($this->versions) + 1;
      $filename = implode('_', $filename);

      return str_replace('/', '-', $filename);
    }

    public function getCountryName() {
      $tAddresses = new Table_Addresses();
      $addArr = $tAddresses->getAddress($this->_customer['id_addresses']);
      return $addArr['country_name'];
    }

    public function getPricelist() {
      $id_pricelist = $this->_order->getPricelist();
      if (is_null($id_pricelist) && !is_null($this->_row->id_pricelists)) {
        $tOrder = new Table_Orders();
        $tOrder->update(array('id_pricelists' => $this->_row->id_pricelists), 'id = ' . $this->_order->getID());
        $id_pricelist = $this->_row->id_pricelists;
      }
      return $id_pricelist;
    }

    /**
     * Generovni radku polozek faktury
     */
    public function generateProductRows() {

      $addVat = $this->getRegion() === Table_Addresses::REGION_NON_EU && !$this->getCustomer()->isB2B();
      $vat = 1 + $this->getVat();

      $items = $this->prepareParts();

      $last_cid = null;
      $acc = false;

      // koeficient dodatecne slevy
      //$addDisc = $this->includeDiscount() ? (100 - $this->getAdditionalDiscount()) / 100 : 1;
      $addDisc = 1;
      foreach ($items as $item) {

        // TODO Maskovat nulove ceny se uz asi nepouziva? Overit u Jiriho.
        // $mask = $this->_row->mask_zero_prices == 'y' || ($this->getRegion() == Table_Addresses::REGION_NON_EU && $this->getPriceTotal() == 0);
        $mask = FALSE;

        $part = new Parts_Part($item['id_products']);

        $item['end_price_eur'] = $part->getEndPrice($this->getPricelist(), $addVat);

        $vatIncluded = isset($item['price'][2]) ? $item['price'][2] == 'y' : FALSE;

        /* nastaveni slev - uplatnuje se postupne globalni sleva na zakaznika
         * pak pripadna sleva na kategorii a nakonec specialni sleva na produkt
         * TODO: tento system slev se nebude pouzivat, protoze se zacaly
         * pouzivat ceniky. Zbude jen $addCisc - sleva na fakture
         */
        $discount = $item['discount_category'] > 0 ?
          $item['discount_category'] : $item['discount_global'];
        $discount = $item['discount_product'] > 0 ?
          $item['discount_product'] : $discount;

        $unitPrice = (float) $item['price'][0] * (1 - $discount) * $addDisc;
        $unitPriceVat = $unitPrice * $vat;

        $amount = (int) $item['amount'];
        if ($mask) {
          $amount -= (int) $item['free_amount'];
        }
        $price = $amount * $unitPrice;
        if ($amount) {
          if ($item['ctg_id'] != $last_cid && !$acc) {
            $text = $item['ctg_name'];
            $this->productRows[] = array(
              'text' => $text,
              'type' => self::PRODUCT_LINES_TYPE_HEADER
            );
            $last_cid = $item['ctg_id'];
          }

        $this->productRows[] = array(
          'id' => $item['id_products'],
          'text' => $item['text'][0],
          'hs' => $item['hs'],
          'hs_code' => $item['hs_code'],
          'amount' => $amount,
          'end_price' => $unitPrice,
          'price_vat' => $unitPriceVat,
          'price' => $price,
          'vat' => $this->getVat(),
          'type' => self::PRODUCT_LINES_TYPE_NORMAL,
          'vat_included' => $vatIncluded,
        );
        $this->priceTotal += $price;
        }
      }
      $last_cid = null;


      // Slevove radky pro FREE kusy.
      foreach ($items as $item) {
        if ($item['free_amount'] == 0) {
          continue;
        }
        if ($last_cid != $item['ctg_id']) {
          $last_cid = $item['ctg_id'];
          $this->productRows[] = array(
            'text' => 'BONUS - ' . $item['ctg_name'],
            'type' => self::PRODUCT_LINES_TYPE_HEADER
          );
        }

        $part = new Parts_Part($item['id_products']);
        $item['end_price_eur'] = -$part->getEndPrice($this->getPricelist(), $addVat);

        $vatIncluded = isset($item['price'][2]) ? $item['price'][2] == 'y' : FALSE;

        $endPrice = -$item['price'][1];
        $endPriceVat = $endPrice * $vat;

        if ($mask) {
          $price = 1 * $addDisc;
          $this->priceTotal += $addDisc;
        } else {
          $price = $item['free_amount'] * -$item['price'][1];
          $this->priceTotal += $price;
        }

        $this->productRows[] = array(
          'id' => $item['id_products'],
          'text' => ($this->getLanguage() === 'cs' ? 'sleva - ' : 'discount - ') . $item['text'][0],
          'hs' => $item['hs'],
          'hs_code' => $item['hs_code'],
          'amount' => $item['free_amount'],
          'end_price' => $endPrice,
          'price_vat' => $endPriceVat,
          'price' => $price,
          'vat' => $this->getVat(),
          'type' => self::PRODUCT_LINES_TYPE_BONUS,
        );
      }
    }

    // vypis dodatecnych custom polozek
    public function generateCustomRows() {
      $this->customRows = array();
      //$addDisc = $this->includeDiscount() ? (100 - $this->getAdditionalDiscount()) / 100 : 1;
      $addDisc = 1;
      $customItems = $this->getCustomItems();
      if (!empty($customItems)) {
        $this->customRows[] = array(
          'text' => $customItems[0]['ctg_name'],
          'type' => self::PRODUCT_LINES_TYPE_HEADER
        );
        $vat = $this->getVat();
        foreach ($customItems as $item) {
          $iPrice = $item['cost'] * $addDisc;
          $this->customRows[] = array(
            'text' => $item['text'],
            'amount' => 1,
            'end_price' => $iPrice,
            'price' => $iPrice,
            'vat' => $vat,
            'type' => self::PRODUCT_LINES_TYPE_CUSTOM,
          );
          $this->priceTotal += $iPrice;
        }
      }
    }

    public function resetItemsTexts() {
      $tItems = new Table_InvoiceItems();
      $lines = $tItems->fetchAll('id_invoice = ' . $this->getId());
      foreach($lines as $line) {
        $part = new Parts_Part($line['id_parts']);
        $name = $part->getName();
        $tItems->update(array(
          'text' => $name,
          'text_free' => $name), 'id = ' . $line['id']);
      }
    }

    public function resetItemsPrices() {

      $ossApplicable = $this->isOssApplicable();
      $ossCoefficient = $this->getOssCoefficient();
      $tItems = new Table_InvoiceItems();

      $addVat = $this->getRegion() === Table_Addresses::REGION_NON_EU && !$this->getCustomer()->isB2B();;

      $lines = $tItems->fetchAll('id_invoice = ' . $this->getId());
      foreach($lines as $line) {
        $part = new Parts_Part($line['id_parts']);
        $price = $part->getEndPrice($this->getPricelist(), $addVat);
        $data = array(
          'price' => $price * $ossCoefficient,
          'price_free' => $price * $ossCoefficient,
          'vat_included' => $ossApplicable ? 'y' : 'n',
        );
        $tItems->update($data, 'id = ' . $line['id']);
      }

      $this->getInvoicedPrice();
    }

    public static function prepareAddress($addressId, $type = 'B2B') {
      $address = NULL;
      if ($addressId) {
        $address = [];
        $tAddresses = new Table_Addresses();
        $addArr = $tAddresses->getAddress($addressId);
        $address['company'] = $type == 'B2B' ? $addArr['company'] : $addArr['person'];
        $address['person'] =  $type != 'B2B' ? NULL : $addArr['person'];
        $street = $addArr['street'] . ' ' . (($addArr['pop_number']) ? $addArr['pop_number'] : '') . (($addArr['orient_number']) ? '/' . $addArr['orient_number'] : '');
        $address['street'] = explode("\n", wordwrap($street, Meduse_Pdf_Invoice::ADDRESS_LINES_SIZE, "\n", FALSE));
        $address['zip'] = explode("\n", wordwrap($addArr['city'] . ' ' . $addArr['zip'], Meduse_Pdf_Invoice::ADDRESS_LINES_SIZE, "\n", FALSE));
        $address['country'] = $addArr['country'];
        $address['country_name'] = $addArr['country_name'];
        if ($addArr['phone']) {
          $address['phone'] = $addArr['phone'];
        }
      }
      return $address;
    }

    /**
     * Generuje z běžné faktury dobropis
     *
     * @return \Invoice_Invoice Objekt: dobropis
     * @throws Exception
     */
    public function generateCreditNote() {
      // Kontrola typu faktury
      if ($this->getType() == Table_Invoices::TYPE_REGULAR) {
        $tInvoice = new Table_Invoices();
        $tInvoiceItems = new Table_InvoiceItems();
        $tInvoiceCustomItems = new Table_InvoiceCustomItems();

        // == Vytvoření dobropisu == //
        $order = new Orders_Order($this->getOrderId());
        $no = $tInvoice->getNextNo($order, array(
          'prefix_extra' => 'C',
          'type' => Table_Invoices::TYPE_CREDIT,
        ));

        // Získání informací z původní faktury
        $invoice_regular = $tInvoice->find($this->getId())->toArray();

        // Úprava informací pro potřeby dobropisu
        $invoice_rewrite = array(
            'type' => Table_Invoices::TYPE_CREDIT,
            'no' => $no,
            'inserted' => date('Y-m-d H:i:s'),
            'last_change' => date('Y-m-d H:i:s'),
        );

        // Založení dobropisu
        $invoice_credit = array_merge($invoice_regular[0], $invoice_rewrite);
        unset($invoice_credit['id']);
        $creditID = $tInvoice->insert($invoice_credit);


        // ==Převod položek== //

        //Položky z objednávky
        $items = $tInvoiceItems->select()->where('id_invoice = ?', $this->getId())->query()->fetchAll();
        foreach ($items as $item) {
          unset($item['id']);
          $item['id_invoice'] = $creditID;
          $item['price'] = is_null($item['price']) ? NULL : ($item['price']*(-1));
          $item['price_free'] = is_null($item['price_free']) ? NULL : ($item['price_free']*(-1));

          $tInvoiceItems->insert($item);
        }

        //Zvláštní položky
        $items_custom = $tInvoiceCustomItems->select()->where('id_invoice = ?', $this->getId())->query()->fetchAll();
        foreach ($items_custom as $item_custom) {
          unset($item_custom['id']);
          $item_custom['id_invoice'] = $creditID;
          $item_custom['price'] = ($item_custom['price']*(-1));

          $tInvoiceCustomItems->insert($item_custom);
        }

        return new Invoice_Invoice($creditID);
      } else {
        throw new Exception('Faktura typu "' . Table_Invoices::TYPE_CREDIT . '" lze generovat jen z faktury typu "' . Table_Invoices::TYPE_REGULAR . '".');
      }
    }

    /**
     * Testuje, zda-li faktura obsahuje nejake prepravni polozky.
     */
    public function isShippingCustomItem() {
      $tItems = new Table_InvoiceCustomItems();
      $items = $tItems->getInvoiceItems($this->getId(), array(Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING));
      return !empty($items);
    }

    static protected $translationEng = array(
        'title_proform' => "P R O - F O R M A   I N V O I C E",
        'title_offer' => "Q U O T A T I O N",
        'title_credit' => "C R E D I T   N O T E",
        'title_invoice' => "I N V O I C E",
        'title_supplier' => "SELLER",
        'company_name' => "Company name",
        'name' => "Name",
        'address' => "Address",
        'city_zip' => "City/Postal Code",
        'country' => "Country",
        'reg_no' => "Reg.No.",
        'reg_no_vat' => 'Reg.No. / VAT No.',
        'vat_no' => 'VAT No.',
        'title_consignee' => "BUYER",
        'delivery_address' => "Consignee",
        'date_issue' => "Issued",
        'date_ts' => "TS",
        'date_due' => "Due",
        'date_prefix' => "",
        'date_suffix' => " date",
        'payment_method' => "Payment method",
        'quotation_no' => "Quotation No.",
        'invoice_no' => "Invoice No.",
        'proform_no' => "Pro-forma invoice No.",
        'credit_no'  => "Credit Note No.",
        'packaging' => "Packaging",
        'total_weight' => "Total weight brutto",
        'header_id_no' => "ID No.",
        'header_full_description' => "Full description of goods / Subject of invoicing",
        'header_serial_no' => "Serial No.",
        'header_packages' => "Packages\n(Pcs)",
        'header_qty' => "Quantity",
        'header_unit' => "Unit Price",
        'header_subtotal' => "Sub Total\nValue",
        'net_vat' => "net of VAT",
        'shipping_costs_1' => "Shipping costs by %c",
        'shipping_costs_2' => "Shipping costs (incl. %d) by %c",
        'total_amount' => "Total amount",
        'total_base' => 'Tax base',
        'vat_tax' => 'VAT tax %1$s %%',
        'total_vat' => "Total incl. VAT tax",
        'issued' => "Invoice issued by",
        'signature' => "Signature",
        'bank' => "Bank",
        'czech_republic' => "Czech Republic",
        'account_no' => "Account No",
        'phone' => 'Phone',
        'header_hs_code' => 'HS code',
        'notice_b2c' => 'Purchaser (consignee) declares that the goods on this invoice is for private use only and is familiar with the fact that its placement into business sector for commercial use is strictly prohibited.',
        'currency' => 'Currency',
        'currency_name_CZK' => 'Czech Crown',
        'currency_name_EUR' => 'Euro',
        'notice_b2b_world' => 'We certify that the goods exported to the %1$s are of pure national origin of Czech Republic. They contain Czech materials and they are being exported from Czech Republic. The goods were manufactured by MEDUSE DESIGN',
        'notice_b2b_eu' => 'Delivery of goods to EU Member States is exempted from VAT according to § 64 of Act 235/2004 Coll. The person liable to declare and pay VAT (including entitlement to the deduction) is the purchaser of the goods.',
        'same_as_consignee' => 'Same as buyer',
        'additional_discount' => 'Additional discount %1$s %%',
        'ctg_pipe_sets' => 'Pipe sets',
        'ctg_presentation_tools' => 'Presentation tools',
        'ctg_extra_accessories' => 'Extra accessories',
        'rate_by_cnb' => 'Price converted from EUR using the exchange rate of the ČNB: 1 EUR = %s CZK',
        'rounding' => 'Rounding',
        'deposit' => '%s %% deposit',
        'header_subject' => 'Subject of invoicing',
        'header_price' => 'Price',
        'pagination_number' => 'inv.no.',
        'pagination_number_quotation' => 'quot.no.',
        'with_vat' => 'with VAT',
        'item_text_proforma' => 'Payment of the proforma invoice no.',
        'deduction' => 'Deduction of deposit',
        'deduction_vat' => 'Deduction VAT of deposit',
        'remains' => 'Remains to pay',
        'order_number' => 'ord.no.',
        'page_number' => 'pg.',
        'contact_person' => 'Contact person',
        'subtotal' => 'Subtotal',
        'box' => 'box',
        'pallete' => 'palette',
    );

    static protected $translationCze = array(
        'title_proform' => "P R O - F O R M A   F A K T U R A",
        'title_offer' => "N A B Í D K A",
        'title_credit' => "O P R A V N Ý   D A Ň O V Ý   D O K L A D",
        'title_invoice' => "F A K T U R A",
        'title_supplier' => "DODAVATEL",
        'company_name' => "Firma",
        'name' => "Jméno",
        'address' => "Adresa",
        'city_zip' => "Město/PSČ",
        'country' => "Země",
        'reg_no' => "IČ",
        'reg_no_vat' => 'IČ / DIČ',
        'vat_no' => 'DIČ',
        'title_consignee' => "ODBĚRATEL",
        'delivery_address' => "Dodací adresa",
        'date_issue' => "Vystaveno",
        'date_ts' => "DUZP",
        'date_due' => "Splatnost",
        'date_prefix' => "",
        'date_suffix' => "",
        'payment_method' => "Způsob platby",
        'quotation_no' => "Číslo nabídky",
        'invoice_no' => "Číslo faktury",
        'proform_no' => "Číslo pro-forma faktury",
        'credit_no'  => "Číslo opravného daň. dokladu",
        'packaging' => "Balení",
        'total_weight' => "Celková hmotnost brutto",
        'header_id_no' => "ID číslo",
        'header_full_description' => "Celkový popis zboží / Předmět fakturace",
        'header_serial_no' => "Seriové číslo",
      	'header_packages' => "Balení\n(ks)",
        'header_qty' => "Počet",
        'header_unit' => "Jedn. cena",
        'header_subtotal' => "Mezisoučet",
        'net_vat' => "bez DPH",
        'shipping_costs_1' => "Přepravní náklady pomocí %c",
        'shipping_costs_2' => "Přepravní náklady (vč. %d) pomocí %c",
        'total_amount' => "Celková částka",
        'total_base' => 'Základ',
        'vat_tax' => 'DPH %1$s %%',
      	'total_vat' => "Celkem vč. DPH",
        'issued' => "Vystavil",
        'signature' => "Podpis",
        'bank' => "Banka",
        'czech_republic' => "Česká republika",
        'account_no' => "Číslo účtu",
        'phone' => 'Telefon',
        'header_hs_code' => 'HS kód',
        'notice_b2c' => 'Kupující (příjemce), prohlašuje, že zboží uvedené na této faktuře je pouze pro soukromé použití a je obeznámen s tím, že jeho umístění do podnikatelského sektoru pro komerční účely je přísně zakázáno.',
        'currency' => 'Měna',
        'currency_name_CZK' => 'Česká koruna',
        'currency_name_EUR' => 'Euro',
        'same_as_consignee' => 'Stejná jako odběratel',
        'additional_discount' => 'Dodatečná sleva %1$s %%',
        'ctg_pipe_sets' => 'Sety dýmek',
        'ctg_presentation_tools' => 'Prezentační nástroje',
        'ctg_extra_accessories' => 'Extra příslušenství',
        'rate_by_cnb' => 'Ceny přepočítány z EUR dle kurzu vyhlášeného ČNB: 1 EUR = %s CZK',
        'rounding' => 'Zaokrouhlení',
        'deposit' => '%s %% záloha',
        'header_subject' => 'Předmět fakturace',
        'header_price' => 'Cena',
        'pagination_number' => 'fakt.č.',
        'pagination_number_quotation' => 'nab.č.',
        'with_vat' => 's DPH',
        'item_text_proforma' => 'Úhrada proforma faktury č.',
        'deduction' => 'Odpočet zálohy',
        'deduction_vat' => 'Odpočet DPH ze zálohy',
        'remains' => 'Zbývá uhradit',
        'order_number' => 'obj.č.',
        'page_number' => 'str.',
        'contact_person' => 'Kontaktní osoba',
        'subtotal' => 'Mezisoučet',
        'box' => 'krabice',
        'pallete' => 'paleta',
    );

    public function getTranslations() {
      return array(
        'cze' => self::$translationCze,
        'eng' => self::$translationEng,
      );
    }

    public function register() {
      $type = $this->getType();
      if ($type == Table_Invoices::TYPE_REGULAR || $type == Table_Invoices::TYPE_CREDIT) {
        $table = new Table_Invoices();
        $table->update(array('registered' => 'y'), 'id = ' . $this->getId());
      }
    }

    public function deregister() {
      $table = new Table_Invoices();
      $table->update(array('registered' => 'n'), 'id = ' . $this->getId());
    }

    public function isRegistered() {
      return $this->_row->registered == 'y';
    }

    public function isLocked() {
      $table = new Table_Invoices();
      $result = $table->select()->from($table, array(
        'locked' => new Zend_Db_Expr("IF(registered = 'n' OR DATE_FORMAT(NOW(), '%Y-%m-%d') < DATE_FORMAT(DATE_ADD(IFNULL(duzp, NOW()), INTERVAL 1 MONTH), '%Y-%m-15'), 'n', 'y')"),
      ))->where('id = ?', $this->getId())->query()->fetchColumn();
      return $result == 'y';
    }

    public function getWHType() {
      return $this->_row->id_wh_type;
    }

    public function getPriceTotal(): float {
      if ($this->orderIsService()) {
       $services = $this->_order->getServicesTotalPrice();
       $total_price = $this->getType() === Table_Invoices::TYPE_CREDIT ?
         $this->getCustomItemsTotalPrice(array(Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_ADDITIONAL)) - $services
         : $this->getCustomItemsTotalPrice(array(Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_ADDITIONAL)) + $services;
       return (float) $total_price;
      }

        return (float) $this->priceTotal;
    }

    public function getCustomItemsTotalPrice($types = []): float {
      $table = new Table_InvoiceCustomItems();
      return (float) $table->getInvoiceItemsSum($this->getId(), $types);
    }

    public function getVat() {
      return (float) $this->_row->vat;
    }

    public function getDeposit() {
      if ($this->_row->invoice_price > 0) {
        return $this->_row->invoice_price / 100;
      }
      else {
        return 1;
      }
    }


  /**
   * Vrati cenu na fakture.
   *
   * @param bool $calculate
   * Prepocitat cenu nebo vzit ulozenou?
   *
   * @param bool $brute
   * Pokud je cena hruba, ve vysledne cene nejsou zapocitany zalohy a uhrady.
   * Pouziti pro ziskani celkove cestky napr. pro objednavky.
   *
   * @return float
   */
    public function getInvoicedPrice(bool $calculate = TRUE, bool $brute = FALSE): float
    {

      if (!$brute && !$calculate) {
        return (float) $this->_row->invoiced_total;
      }

      $price = $this->getPriceTotal();
      $discount = $this->getAdditionalDiscount();
      if ($discount) {
        $price *= (100 - $discount) / 100;
      }

      if (!$this->orderIsService()) {
        $customItemsPrice = $this->getCustomItemsTotalPrice([
          Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_DELIVERY,
          Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_DISCOUNT,
          Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_PACKAGING,
          Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING,
        ]);
        $price += $customItemsPrice !== false ? $customItemsPrice : 0;
      }

      if ($this->showVatSummary()) {
        $price *= (1 + $this->getVat());
      }

      $currency = $this->getCurrency();
      if ($currency === 'CZK') {
        $price *= $this->getRate();
      }

      if ($brute) {
        return (float) $price;
      }

      $price *= $this->getDeposit();

      $precision = $this->getCurrency() === Table_Invoices::CURRENCY_CZK ? 0 : 2;
      $price = round($price, $precision);

      if ($deductArr = $this->getDeductionTotal()) {
        $deduction = $deductArr['total_vat'];
        $price += round($deduction, $precision);
      }

      // Pokud vystavime ostrou fakturu s nulovou fakturovanou cenou
      $order = $this->getOrder();
      if ($order && $price === 0.0 && $this->getType() === Table_Invoices::TYPE_REGULAR) {
        $date = $order->getDatePayDeposit();
        // datum akceptace v pripade dobirky
        if ($order->getPaymentMethod() === Table_Orders::PM_CASH_ON_DELIVERY) {
          $order->setDatePaymentAccept($date);
        }
        else {
          // datum celkove platby
          $order->setDatePayAll($date);
        }
      }

      $this->_row->invoiced_total = $price;
      $this->_row->save();

      return $price;

    }

    public function isPaid(): bool
    {
      $tPayments = new Table_Payments();
      $invoiced = $this->getInvoicedPrice(FALSE);
      $payments = $tPayments->getPaymentsSumForInvoice($this->getId());
      return $invoiced <= $payments;
    }

    public function getUnpaidAmount() {
      $tPayments = new Table_Payments();
      $invoiced = $this->getInvoicedPrice(FALSE);
      $payments = $tPayments->getPaymentsSumForInvoice($this->getId());
      return $invoiced - $payments;
    }

  public function isOffer(): bool
  {
    return $this->getType() === Table_Invoices::TYPE_OFFER;
  }

    public function isProform(): bool
    {
      return $this->getType() === Table_Invoices::TYPE_PROFORM;
    }

    public function isCredit(): bool
    {
      return $this->getType() === Table_Invoices::TYPE_CREDIT;
    }

    public function isRegular(): bool
    {
      return $this->getType() === Table_Invoices::TYPE_REGULAR;
    }

    public function getPayments() {
      $payments = array();
      $tPayments = new Table_Payments();
      $rows = $tPayments->getPaymentsForInvoice($this->getId());

      if (!empty($rows)) {
        foreach ($rows as $row) {
          $payments[] = new Invoice_Payment($row->id);
        }
      }
      return $payments;
    }

    public function hasPayments() {
      $tPayments = new Table_Payments();
      return !empty($tPayments->getPaymentsForInvoice($this->getId()));
    }

  /**
   * @deprecated Use getInvoicedPrice();
   */
    public function recalculateInvoicedPrice() {
      return $this->getInvoicedPrice();
    }

    public static function getUnpaidInvoices($options = array()) {

      $unpaid = array();

      $options['overdue'] = TRUE;
      $options['unpaid'] = TRUE;

      $table = new Table_Invoices();
      $stats = $table->getPaymentsStats($options);

      if ($stats) {
        foreach ($stats as $row) {
          $unpaid[] = new Invoice_Invoice($row->id);
        }
      }

      return $unpaid;
    }

  /**
   * Testuje, jestli je mozne vymazat fakturu.
   *
   * 1) Faktura je zalohova
   *    - nesmi byt v ucetnictvi
   *    - nesmi k ni existovat uhrada
   * 2) Faktura je ostra
   *    - nesmi byt v ucetnictvi
   *    - objednavka nesmi byt expedovana
   *    - neexistuje uhrada
   *    - faktura je v rade posledni nebo nema cislo
   *    - neexistuje odpocet zaloh
   * @return bool
   * @throws Zend_Db_Statement_Exception
   */
    public function isDeletable(): bool
    {
      $deletable = false;
      if ($this->isOffer() || $this->isCredit()) {
        $deletable = TRUE;
      }
      elseif ($this->isProform()) {
        $registered = $this->isRegistered();
        $payments = $this->hasPayments();

        $deletable = !$registered && !$payments;
      }

      elseif ($this->isRegular()) {
        $registered = $this->isRegistered();
        $payments = $this->hasPayments();
        $last = $this->isLast();
        $number = $this->getNo();
        $deductions = $this->getPaymentsForDeduction();

        $order = $this->getOrder();
        $closed = $order && $order->isClosed();

        $deletable = !$registered && !$closed && !$payments && ($last || !$number) && !$deductions;
      }

      return $deletable;

    }

    public function isLast() {
      $table = new Table_Invoices();
      $lastNo = $table->getLastNo($this->getOrder());
      return ($lastNo == $this->getNo());
    }

    public function isVatVerified(): bool
    {
      return $this->_customer->vat_checked === 'y';
    }

    public function isExport() {
      if (is_null($this->_row->export)) {
        // musime zjistit z adresy objednavky
        return Table_Addresses::COUNTRY_CZ != $this->getOrder()->getCountry();
      }
      else {
        return $this->_row->export == 'y';
      }
    }

    public function delete() {

      if (!$this->isDeletable()) {
        throw new Exception('Fakturu nelze odstranit.');
      }

      /** @var $db Zend_Db_Adapter_Abstract */
      $db = Zend_Registry::get('db');
      $db->beginTransaction();

      $id = $this->getId();

      $order = $this->getOrder();
      if ($this->isRegular()) {
        $order->setDatePayAll(NULL);
      }

      // odstraneni PDF souboru a jejich verzi
      if ($files = $this->getVersions()) {
        foreach ($files as $file) {
          unlink($file);
        }
      }
      $db->delete('invoice_versions', 'id_invoice = ' . $id);

      // odstraneni radkovych polozek
      $db->delete('invoice_items', 'id_invoice = ' . $id);

      // odstraneni custom polozek
      $db->delete('invoice_custom_items', 'id_invoice = ' . $id);

      // odstraneni faktury z databaze
      $db->delete('invoice', 'id = ' . $id);

      $db->commit();
    }

  /**
   * Vraci seznam uhrad, ktere se z celkove regular faktury odecitaji nebo mohou odecitat.
   * Navratovou hodnotou je asociativni pole.
   * @throws Zend_Db_Statement_Exception
   */
    public function getPaymentsForDeduction($allCandidates = FALSE) {
      if (!$this->isRegular()) {
        return FALSE;
      }

      if ($allCandidates) {
        return (new Table_Payments())->getPaymentsForDeduction($this->getCustomerId());
      }

      return (new Table_Invoices())->getPaymentsForDeduction($this->getId());
    }

    public function testZeroProductPrices($throwException = FALSE) {
      if ($prices = $this->getItemPrices()) {
        $credit = $this->getType() === Table_Invoices::TYPE_CREDIT;
        $zeroPrices = FALSE;
        $productIds = [];
        foreach ($prices as $productId => [$price, $free]) {
          if ($credit) {
            $price *= -1;
          }
          if (!$price || ((float) $price <= 0)) {
            $zeroPrices = TRUE;
            $productIds[] = $productId;
          }
        }
        if ($throwException && $zeroPrices) {
          $msg = 'Nelze generovat fakturu mimo EU s nulovými cenami protuktů: '
            . implode(', ', $productIds);
          throw new Invoice_Exceptions_ZeroProductPrice($msg);
        }
        return $zeroPrices;
      }
      return NULL;
    }

    public function isTransferredTaxLiability($wh = Parts_Part::WH_TYPE_PIPES) {
      $conditions = [];
      $conditions[] = $this->isRegular();
      $conditions[] = $this->getCustomer()->isB2B();
      $conditions[] = $this->getRegion() == Table_Addresses::REGION_EU;
      $conditions[] = $this->getCountry() != Table_Addresses::COUNTRY_CZ;
      $conditions[] = !$this->showVatSummary();
      $conditions[] = $this->getCustomer()->isVerifiedVatNo() ? $this->getCustomer()->isVerifiedVatNo() : FALSE;
      if ($wh == Parts_Part::WH_TYPE_PIPES) {
        $conditions[] = $this->getOrder()->getCarrierMethodId() == Table_OrdersCarriers::EX_WORKS;
      }
      elseif ($wh == Parts_Part::WH_TYPE_TOBACCO) {
        $conditions[] = $this->getOrder()->getCarrierMethodId() == Table_OrdersCarriers::EX_WORKS_TOBACCO;
      }
      $isLiability = TRUE;
      foreach ($conditions as $condition) {
        $isLiability = $isLiability && $condition;
      }
      return $isLiability;
    }

    public function setCurrency($currency, $save = TRUE) {
      $this->_row->currency = $currency;
      if ($save) {
        $this->_row->save();
      }
    }

    public function setRate($rate, $save = TRUE) {
      if (is_null($rate)) {
        $this->_row->use_actual_rate = 'y';
        // $this->_row->rate = $rate;
      }
      else {
        $this->_row->use_actual_rate = 'n';
        $this->_row->rate = $rate;
      }
      if ($save) {
        $this->_row->save();
      }
    }

    public function loadShipmentFromOrder() {
      $order = $this->getOrder();
      $shipmentName = $order->getCarrierMethodFullName();
      $shipmentPrice = (float) $order->getCarrierPrice(TRUE);
      if ($shipmentName) {
        $this->removeShipping();
        $ossApplicable = $this->isOssApplicable();
        $ossCoefficient = $this->getOssCoefficient();
        $price = $shipmentPrice * $ossCoefficient;
        if ($ossApplicable) {
          $price = $price * (1 + $this->getVat());
        }
        $data = [
          'type' => Table_InvoiceCustomItems::CUSTOM_ITEM_TYPE_SHIPPING,
          'description' => $shipmentName,
          'price' => $price,
          'vat_included' => $ossApplicable,
        ];
        $this->setCustomItem($data);
      }
      else {
        throw new Exception('Nepřidáno. Není definovaná přeprava.');
      }
    }

    public function getDeductionTotal() {
      $deductions = $this->getPaymentsForDeduction();
      if (!$deductions) {
        return NULL;
      }
      $total = 0;
      foreach ($deductions as $d) {
        $total += $d['deducted'];
      }
      $vat = $this->getVat();
      return array(
        'total' => -($total / (1 + $vat)),
        'vat'   => -($total / (1 + $vat)) * $vat,
        'total_vat' => -$total,
      );
    }

    /**
     * Vraci miru redukovane faktury (0-99 procent).
     *
     * @return int
     */
    public function getReduction() {
        return (int) $this->_row->reduction;
    }

    /**
     * @return bool
     */
    public function isOssApplicable() {
        if ($this->getDUZP() < self::OSS_APPLICABLE_DATE) {
          return false;
        }
        return $this->showVatSummary() && $this->getRegion() === 'eu' && $this->getCountry() !== 'CZ';
    }

    /**
     * @return float
     */
    protected function getOssCoefficient() {
        if ($this->isOssApplicable()) {
            $tDPH = new Table_ApplicationDph();
            $dphCz = $tDPH->getActualDph($this->_row['issue_date']) / 100;
            $dph = $this->getVat();
            return (float) (1 + $dphCz) / (1 + $dph);
        }
        else {
            return 1.00;
        }
    }

    public function update($data) {
        $table = new Table_Invoices();
        $table->update($data, 'id  = ' . $this->getId());
        $this->_row->refresh();
        $this->calculateVat(true);
        $this->recalculateInvoicedPrice();
    }

    public static function create($data) {
        $table = new Table_Invoices();
        $invoiceId = $table->insert($data);
        $invoice = new Invoice_Invoice($invoiceId);
        $invoice->calculateVat(true);
        $invoice->recalculateInvoicedPrice();
        return $invoice;
    }

    public function calculateVat($save = false) {
        $tDph = new Table_ApplicationDph();
        if ($this->isOssApplicable()) {
            $code = $this->getCountry();
            $this->_row->vat = ($tDph->getActualDph($this->_row['issue_date'], $code) / 100);
        }
        else {
            $this->_row->vat = ($tDph->getActualDph($this->_row['issue_date']) / 100);
        }
        if ($save) {
            $this->_row->save();
        }
    }

  /**
   * @param $region
   *
   * @return Pdf_Invoice|Pdf_InvoiceServices
   */
  protected function getPdf($region) {

    if ($this->showLogo()) {
      if ($this->orderIsService()) {
        $pdf = $this->showVatSummary() ? Pdf_InvoiceServices::load(Pdf_InvoiceServices::TEMPLATE_SERVICES_PIPES_VAT) : Pdf_InvoiceServices::load(Pdf_InvoiceServices::TEMPLATE_SERVICES_PIPES);
        $pdf->type = $this->getType();
      }
      else {
        if ($region == Table_Addresses::REGION_EU) {
          $pdf = $this->showVatSummary() ? Pdf_Invoice::load(Meduse_Pdf_Invoice::TEMPLATE_EU_VAT) : Pdf_Invoice::load(Meduse_Pdf_Invoice::TEMPLATE_EU);
        }
        else {
          $pdf = Pdf_Invoice::load(Pdf_Invoice::TEMPLATE_NON_EU);
        }
      }
    }
    else {
      if ($this->orderIsService()) {
        $pdf = $this->showVatSummary() ? Pdf_InvoiceServices::load(Pdf_InvoiceServices::TEMPLATE_SERVICES_PIPES_VAT_NOLOGO) : Pdf_InvoiceServices::load(Pdf_InvoiceServices::TEMPLATE_SERVICES_PIPES_NOLOGO);
        $pdf->type = $this->getType();
      }
      else {
        if ($region == Table_Addresses::REGION_EU) {
          $pdf = $this->showVatSummary() ? Pdf_Invoice::load(Meduse_Pdf_Invoice::TEMPLATE_EU_VAT_NOLOGO) : Pdf_Invoice::load(Meduse_Pdf_Invoice::TEMPLATE_EU_NOLOGO);
        }
        else {
          $pdf = Pdf_Invoice::load(Pdf_Invoice::TEMPLATE_NON_EU_NOLOGO);
        }
      }
    }

    return $pdf;
  }

  public function setTextFromTemplate() {

    // Paramentry.
    $customerType = $this->getCustomerType();
    $region = $this->getRegion();
    $language = $this->getLanguage();
    $type = $this->getType();
    $wh = $this->getWHType();

    // Dotaz.
    $table = new Table_InvoiceCustomTexts();
    $select = $table->select();
    $select->from($table, ['text']);
    $select->where('type = ?', $customerType);
    $select->where('region = ?', $region);
    $select->where('language = ?', $language);
    $select->where('invoice_type = ?', $type);
    $select->where('id_wh_type = ?', $wh);

    // Nastaveni textu.
    if ($text = $select->query()->fetchColumn()) {
      $this->setText($text, FALSE);
      return true;
    }
    else {
      return false;
    }
  }

  protected function getDateLabel($t): string {
    $due_label = $t->translate('date_prefix') . $t->translate('date_issue');
    if (!$this->isOffer() && !$this->isProform()) {
      $due_label .= ' / ' . $t->translate('date_ts');
    }
    if (!is_null($this->getDueDate())) {
      $due_label .= ' / ' . $t->translate('date_due');
    }
    $due_label .= $t->translate('date_suffix');
    return $due_label;
  }

  /**
   * V některých případech se do ceny záměrně přidává DPH.
   *
   * @return bool
   */
  public function isAddedVatInPrice(): bool
  {
    $customer = $this->getCustomer();
    $region = $this->getRegion();
    $isB2B = $customer->isB2B();

    // (a) zázkazník je B2C a je mimo EU.
    if ($region === Table_Addresses::REGION_NON_EU && !$isB2B) {
      return true;
    }

    return false;
  }

  public function setDeductionsFromProforma($proforma) {

    if (! $proforma instanceof self) {
      if (is_string($proforma) || is_int($proforma)) {
        try {
          $proforma = new Invoice_Invoice((int) $proforma);
        }
        catch (Exception $e) {
          Utils::logger()->warn($e->getMessage());
          return false;
        }
      }
    }
    if (!$proforma->isProform()) {
      Utils::logger()->warn('Invoice must be proforma type.');
      return false;
    }

    $cnt = 0;
    if ($payments = $proforma->getPayments()) {

      $invoiceId = $this->getId();

      $tDeductions = new Table_PaymentsDeductions();
      $adapter = $tDeductions->getAdapter();

      /** @var Invoice_Payment $payment */
      foreach ($payments as $payment) {
        if (!$payment->hasNo()) {
          continue;
        }

        $paymentId = $payment->getId();

        $data = [
          'id_payments' => $paymentId,
          'deduct_from' => $invoiceId,
          'amount' => $payment->getRemainAmount(),
        ];
        try {
          $tDeductions->insert($data);
        }
        catch (Exception $e) {
          $tDeductions->update($data, [
            'id_payments = ' . $adapter->quote($paymentId),
            'deduct_from = ' . $adapter->quote($invoiceId),
          ]);
        }
        $cnt++;
      }
    }

    return $cnt;
  }
}
