<?php
class Zend_View_Helper_DateFromDb{
    function dateFromDb($date){
    	if ($date !== NULL) {
			  $dateObj = new Meduse_Date($date, 'Y-m-d');
			  return $dateObj->toString("d.m.Y");
    	}
    	else return '';
	}
}
