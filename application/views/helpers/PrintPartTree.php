<?php
/**
 * Created by PhpStorm.
 * User: Zdenek
 * Date: 28.12.2017
 * Time: 12:12
 */

class Zend_View_Helper_PrintPartTree {

  public function printPartTree($tree, $level = 0) {
    $string = '<tr><td class="text-left">' . str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $level) . $tree['id'] . ' - ' .
      $tree['name'] . '</td><td class="text-right">' . $tree['amount'] . 'x</td>' .
      (Zend_Registry::get('acl')->isAllowed('parts/detail/price') ? '<td class="text-right">'
        . number_format($tree['price'],2) . ' Kč</td>' : '') .'</tr>';
    if (!empty($tree['subparts'])) {
      foreach($tree['subparts'] as $subpart) {
        $string .= $this->printPartTree($subpart, $level + 1);
      }
    }
    return $string;
  }

}