<?php

class Zend_View_Helper_OrdersFilterForm{
    public function ordersFilterForm($form){
?>

<script type="text/javascript">
$(document).ready(function() {
	jQuery("#select_all_checkboxex").mousedown(function(){

        jQuery("[type=checkbox]:unchecked").parent().parent().attr('style', "background-color: yellow;");
        jQuery("[type=checkbox]:unchecked").attr("checked", true);
	});
	jQuery("#deselect_all_checkboxex").mousedown(function(){
        jQuery("[type=checkbox]:checked").parent().parent().removeAttr("style");
        jQuery("[type=checkbox]:checked").removeAttr("checked");

	});	
});
</script>


<div class="box_header orders_box">Zobrazit objednávky</div>
<div class="box orders_box" style="text-align: left">
	<form method="post">
		<table style="margin: auto;" class="datalist">
<tr>
	<th>Název</th><th>Popis</th><th>Deadline</th><th>Země</th><th>Stav</th><th>&nbsp;</th>
</tr>

<?php 
$i = 0;
$lastStatus = null;
foreach($form->getElements() as $element){
	$class = ($i++ % 2) ? " class='odd'" : '';
	if($element->getType() == 'Meduse_Form_Element_Checkbox'){
		$fields = explode('|', $element->getLabel());
		if(isset($fields[2]) && !empty($fields[2])){
			$zend_date = new Zend_Date($fields[2], "yyyy-MM-dd");
			$datum = $zend_date->toString("d.M.yyyy");
		} else {
			$datum = '';
		}
        if($lastStatus != $fields[4]){

            if($lastStatus !== null){
				if(preg_match("/^Strat/", $fields[4])){
?>
            <tr><th colspan="6">Strategické objednávky</th></tr>
<?php
				} else {
?>
            <tr><th colspan="6">Otevřené objednávky</th></tr>
<?php
				}
            }
            $lastStatus = $fields[4];
        }

?>
			<tr <?= $class ?> name="<?= $element->getId() ?>_line">
				<td><?php echo $fields[0]?></td> <!-- nazev -->
				<td><?php echo $fields[1]?></td> <!-- popis -->
				<td style="text-align: right;"><?php echo $datum ?></td> <!-- deadline -->
				<td><?php echo $fields[3]?></td> <!-- zeme -->
				<td><?php echo $fields[4]?></td> <!-- stav -->
				<td style="text-align: center;">
<?php
	//POKUD JE OBJEDNAVKA REZERVOVANA, PAK NEZOBRAZIT CHECKBOX
	$tmp_order = new Orders_Order($element->getId());
	if($tmp_order->isReady()){
		echo "rezervace";
	} else {
?>
					<input id="<?php echo $element->getId()?>" name="<?php echo $element->getFullyQualifiedName() ?>" type="checkbox" <?= ($element->getValue() == 'on' ? "checked='checked'": '')?>/>
<?php
	} 
?>
				</td>
			</tr>
<?php

	} 
}
?>
			<tr><th colspan="6"> </th></tr>
			<tr><td colspan="6" style="text-align: center;"><a id="select_all_checkboxex" href="javascript:;">označit všechny</a> | <a id="deselect_all_checkboxex" href="javascript:;">zrušit všechny označené</a></td></tr>
			<tr><th colspan="6">Výběr součástí</th></tr>
			<tr><td colspan="6"><table style="margin: auto;">
<?php
foreach($form->getElements() as $element){
	if($element->getType() == 'Meduse_Form_Element_Select'){
		echo $element;
	}
} 
?>
			</table></td></tr>
			<tr><th colspan="6">&nbsp;</th></tr>
			<tr><td colspan="6"><table style="margin: auto;">
				<?php echo $form->Odeslat ?>
			</table></td></tr>
		</table>
	</form>
</div>

<script type="text/javascript">
<!--
$(document).ready(function() {

	jQuery("[type=checkbox]").click( function() {
        if($(this).attr('checked') == false){
			jQuery("[name='"+$(this).attr('name')+"_line']").removeAttr('style');
		} else {
			jQuery("[name='"+$(this).attr('name')+"_line']").attr('style', "background-color: yellow;");
		}
	});


});

//-->
</script>

<?php
	}
	
	public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }	
}
