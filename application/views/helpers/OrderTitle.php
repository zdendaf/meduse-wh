<?php

  class Zend_View_Helper_OrderTitle {

    public function orderTitle($order) {
      if (!is_a($order, 'Orders_Order')) {
        $order = new Orders_Order($order);
      }

      switch ($order->getStatus()) {
        case Table_Orders::STATUS_OPEN:
          $css = 'label-success';
          break;
        case Table_Orders::STATUS_CLOSED:
          $css = 'label-important';
          break;
        default:
          $css = '';
          break;
      }

      return '<h3><div class="pull-left m-right-x2 no-print">'
        . '<span class="label ' . $css . ' p">'
        . Table_Orders::$statusToString[$order->getStatus()] . '</span> '
        . ($order->isPrivate() ? '<span class="label label-warning p">Soukromá</span>' : '')
        . '</div> Objednávka č. ' . $order->getNO() . ' - '
        . $order->getTitle() . '</h3>';
    }
  }
