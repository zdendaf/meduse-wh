<?php

class Zend_View_Helper_NavigationMultiEdit {
    
  private $_action = 'index';
  private $_menuItems = array();

  public function navigationMultiEdit($action) {
    $this->_action = $action;
    $this->_menuItems = array(
        'index' => 'Výběr produktu',
        'step2' => 'Příklad úpravy',
        'step3' => 'Výběr produktů',
        'step4' => 'Potvrzení změny',
    );        
    echo $this->_generateBreadcrumbs();
  }

  private function _generateBreadcrumbs() {
    $it = 1;
    $html = '<div class="well-small"><ul class="breadcrumb text-center">';
    foreach ($this->_menuItems as $key => $item) {
      $divider = $it < count($this->_menuItems)? '<span class="divider">&raquo;</span>' : '';
      $class = $key == $this->_action? 'class="active" ' : '';
      $html .= '<li ' . $class .'>' . $item . $divider . '</li>';
      $it++;
    }
    $html .= '</ul></div>';
    return $html;
  }
}
