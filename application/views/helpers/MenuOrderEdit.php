<?php

class Zend_View_Helper_MenuOrderEdit {

    private
      $_action,
      $_canMoveForward,
      $_showProducts,
      $_showTransport,
      $_menuItems,
      $_last;

    public function menuOrderEdit($action, $order = FALSE, $showProducts = TRUE, $showTransport = TRUE) {

        if ($order) {
          $orderObj = is_a($order, 'Orders_Order') ? $order : new Orders_Order($order);
        }
        else {
          $orderObj = NULL;
        }

        $this->_action = $action;
        $this->_canMoveForward = $order !== FALSE;
        $this->_showProducts = $showProducts;
        $this->_showTransport = $showTransport;

        $this->_menuItems = array(
            'index' => 'Objednávka',
            'customer' => 'Zákazník',
        );

        if (!$this->_showProducts) {
            $this->_menuItems['fees'] = 'Fixní poplatky';
            $this->_menuItems['services'] = 'Služby';
            $this->_menuItems['payment'] = 'Platba';
            $this->_last = 'payment';
        }
        else {
          $this->_menuItems['fees'] = 'Fixní poplatky';
          $this->_menuItems['payment'] = 'Platba';
        }

        if ($this->_showTransport) {
            $this->_menuItems['transport'] = 'Doprava';
            $this->_last = 'transport';
        }

        if ($this->_showProducts && !is_null($orderObj) && !$orderObj->hasReservedProducts()) {
            $this->_menuItems['items'] = 'Produkty';
            $this->_last = 'items';
        }

        echo $this->_generateBreadcrumbs();
    }

    private function _generateBreadcrumbs()
    {
        // breadcrumb start tag
        $html = '<ul class="breadcrumb">';

        // generate items
        foreach ($this->_menuItems as $actionName => $title)
        {
            // item start tag
            $html .= '<li>';

            if ($this->_canMoveForward && $actionName != 'items') {
                $title = '<a href="' . Zend_Layout::getMvcInstance()->getView()->url(array('action' => $actionName)) . '">' . $title . '</a>';
            } elseif ($this->_canMoveForward && $actionName == 'items') {
                $title = '<a href="' . Zend_Layout::getMvcInstance()->getView()->url(array('controller' => 'orders', 'action' => 'select-products')) . '">' . $title . '</a>';
            }

            if ($actionName == $this->_action) {
                $item = '<b>' . $title . '</b>';
            } else {
                $item = $title;
            }
            if ($actionName != $this->_last) {
                $item .= ' <span class="divider">»</span>';
            }
            $html .= $item;

            // item end tag
            $html .= '</li>';
        }

        // breadcrumb end tag
        $html .= '</ul>';

        return $html;
    }

}
