<?php
class Zend_View_Helper_DateTimeFromDb{
    function dateTimeFromDb($date){
    	if($date !== NULL){
			$zend_date = new Zend_Date($date, 'yyyy-MM-dd HH:mm:ss');
			return $zend_date->toString("dd.MM.yyyy HH:mm:ss");
    	} else return '';
	}
}
