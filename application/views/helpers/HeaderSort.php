<?php
class Zend_View_Helper_HeaderSort {
  protected $view;
  public function setView($view) {
    $this->view = $view;
  }
  public function headerSort($title, $key, $tab = NULL) {
    $title = str_replace(' ', '&nbsp;', $title);
    $urlParams = array(
      'sort' => $key,
      'tab' => $tab,
      'sorder' => $this->view->sort == $key && $this->view->sorder == 'asc' ? 'desc' : 'asc'
    );
    $url = $this->view->url($urlParams);
    $sorting = $this->view->sort == $key ? ($this->view->sorder == 'desc' ? '&nbsp;⇑' : '&nbsp;⇓') : '';
    echo '<a href="' . $url . '" class="ord-sort">' . $title . $sorting . '</a>';
	}
}
