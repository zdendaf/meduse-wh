<?php

class Zend_View_Helper_OrdersFilterForm{
    public function ordersFilterForm($form){
?>

<style>
    .headline { background-color: #EBEBEB }
    .active { background-color: #ffffbf; }
</style>

<script type="text/javascript">
$(document).ready(function() {
	jQuery("#select_all_checkboxex").mousedown(function(){

        jQuery(".order_checkbox:not(:checked)").parent().parent().attr('class', "active");
        jQuery(".order_checkbox:not(:checked)").attr("checked", true);
	});
	jQuery("#deselect_all_checkboxex").mousedown(function(){
        jQuery(".order_checkbox:checked").parent().parent().removeAttr("class");
        jQuery(".order_checkbox:checked").removeAttr("checked");

	});

	jQuery('.order_checkbox:checked').parent().parent().attr('class', "active");

});
</script>


<div class="" style="text-align: left">
	<form method="post">
		<table class="table table-condensed">
            <tr class="headline">
                <th>Název</th>
                <th>Popis</th>
                <th>Deadline</th>
                <th>Země</th>
                <th>Stav</th>
                <th>&nbsp;</th>
            </tr>

<?php 
$i = 0;
$lastStatus = null;
foreach($form->getElements() as $element){
	$class = ($i++ % 2) ? " class='odd'" : '';
	if($element->getType() == 'Meduse_Form_Element_Checkbox' && $element->getName() != 'all_parts'){
		$fields = explode('|', $element->getLabel());
		if(isset($fields[2]) && !empty($fields[2])){
			$date = new Meduse_Date($fields[2], "Y-m-d");
			$datum = $date->get("d.m.Y");
		} else {
			$datum = '';
		}
        if($lastStatus != $fields[4]){

            if($lastStatus !== null){
				if(preg_match("/^Potvrz/", $fields[4])){
?>
            <tr><th colspan="6" class="headline">Potvrzené objednávky</th></tr>
<?php
				} elseif(preg_match("/^P/", $fields[4])){
?>
            <tr><th colspan="6" class="headline">Předběžné objednávky</th></tr>
<?php
				} else {
?>
            <tr><th colspan="6" class="headline">Otevřené objednávky</th></tr>
<?php
				}
            } elseif(preg_match("/^Str/", $fields[4])) {
?>
            <tr><th colspan="6" class="headline">Strategické</th></tr>
<?php
			}
            $lastStatus = $fields[4];
        }

?>
			<tr <?= $class ?> name="<?= $element->getId() ?>_line">
				<td><?php echo $fields[0]?></td> <!-- nazev -->
				<td><?php echo $fields[1]?></td> <!-- popis -->
				<td style="text-align: right;"><?php echo $datum ?></td> <!-- deadline -->
				<td><?php echo $fields[3]?></td> <!-- zeme -->
				<td><?php echo $fields[4]?></td> <!-- stav -->
				<td style="text-align: center;">
<?php
	//POKUD JE OBJEDNAVKA REZERVOVANA, PAK NEZOBRAZIT CHECKBOX
	$tmp_order = new Orders_Order($element->getId());
	if($tmp_order->isReady()){
		echo "rezervace";
	} else {
?>
					<input id="<?php echo $element->getId()?>" name="<?php echo $element->getFullyQualifiedName() ?>" class="order_checkbox" type="checkbox" <?= ($element->getValue() == 'on' ? "checked='checked'": '')?>/>
<?php
	} 
?>
				</td>
			</tr>
<?php

	} 
}
?>
			<tr><th colspan="6"> </th></tr>
			<tr><td colspan="6" style="text-align: center;"><a id="select_all_checkboxex" href="javascript:;">označit všechny</a> | <a id="deselect_all_checkboxex" href="javascript:;">zrušit všechny označené</a></td></tr>
			<tr>
                <td colspan="6">
                    <div class="form-well">
                        <div class="well">
                            <fieldset class="form">
                                <legend>Výběr součástí</legend>
                                    <div class="dashed-outline">
                                        <table class="table" style="background: none;">
                                        <?php
                                            foreach($form->getElements() as $element){
                                                if(
                                                    $element->getType() == 'Meduse_Form_Element_Collections' ||
                                                    $element->getType() == 'Meduse_Form_Element_Categories' ||
                                                    $element->getType() == 'Meduse_Form_Element_Select' ||
                                                    $element->getName() == 'all_parts'
                                                ){
                                                    echo $element;
                                                }
                                            } 
                                        ?>
                                            
                                        
                                            <?php echo $form->Odeslat ?>
                                        
                                                    
                                        </table>
                                    </div>
                            </fieldset>
                        </div>
                    </div>
                </td>
            </tr>
			
		</table>
	</form>
</div>


<script type="text/javascript">
<!--
$(document).ready(function() {

	jQuery('.order_checkbox').click( function() {
		if($(this).is(':checked')){
            jQuery("[name='"+$(this).attr('name')+"_line']").attr('class', "active");
		} else {
			jQuery("[name='"+$(this).attr('name')+"_line']").removeAttr('class');
		}
	});


});

//-->
</script>

<?php
	}
	
	public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }	
}
