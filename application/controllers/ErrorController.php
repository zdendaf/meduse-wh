<?php
class ErrorController extends Meduse_Controller_Action{
	
	public function errorAction(){
		$db = Zend_Registry::get('db');
		$errors = $this->_getParam('error_handler');
		$this->_helper->layout->disableLayout();
		throw $errors->exception;
		$string = '';
		foreach($errors->request->getParams() as $key => $param){
			$string .= "$key => $param,\n";
		}
		$db->insert('errors', array(
			'params' => $string,
			'exception' => $errors->exception
		));
	}
	
}