<?php
class LoginController extends Meduse_Controller_Action{

    const WORKER_NON_EXISTS = 1;


    public function getForm(){
        return new Login_Form(array(
            'action' => $this->view->baseUrl().'/login/process',
            'method' => 'post',
        ));
    }

    public function getAuthAdapter(array $params){
        // Leaving this to the developer...
        // Makes the assumption that the constructor takes an array of
        // parameters which it then uses as credentials to verify identity.
        // Our form, of course, will just pass the parameters 'username'
        // and 'password'.
       $authAdapter = new Zend_Auth_Adapter_DbTable(
		    Zend_Registry::get('db'),
		    'users',
		    'username',
		    'password'
		);
		
		$tUser = new Table_Users();
		$hash = $tUser->getSalt($params['username']);
		$authAdapter
		    ->setIdentity($params['username'])
		    ->setCredential(sha1($hash . $params['password']));
		return $authAdapter;
    }
    

	public function indexAction(){
        $this->_helper->layout->setLayout('bootstrap-simple');
		if($this->_request->getParam('allow')) {
			if(Meduse_ipChecker::allowIP($this->_request->getParam('allow'))) {
				$this->_flashMessenger->addMessage("Nová lokace byla povolena.");
				$this->redirect('/login');
			}
		}

		$auth = Zend_Auth::getInstance();
		if ($auth->hasIdentity()) {
		  $dest = $this->view->baseUrl();
			$this->redirect($dest);
		}
		$this->view->title = "Login";
		//$this->_helper->layout->setLayout('simple');	
		$form = $this->getForm();
		$this->view->form = $form;
	}
	
    
public function processAction()  {
    $this->_helper->layout->setLayout('bootstrap-simple');
    $request = $this->getRequest();

    // Check if we have a POST request
    if (!$request->isPost()) {
      return $this->_helper->redirector('index');
    }

    $db = Zend_Registry::get('db');


    // Get our form and parse it
    $form = $this->getForm();



    if (!$form->isValid($request->getPost())) {
      // Invalid entries
      $this->view->form = $form;
      $this->_flashMessenger->addMessage("Neplatné uživatelské jméno nebo heslo.");

      $db->insert('users_login_log', array(
        'username' => $request->getParam('username'),
        'password' => ($request->getParam('password') == '' ? '' : new Zend_Db_Expr("SHA('" . $request->getParam('password') . "')")),
        'ip' => $_SERVER['REMOTE_ADDR'],
        'browser' => $_SERVER['HTTP_USER_AGENT']
      ));

      $this->redirect("login");
      return; // re-render the login form
    }

    $db->insert('users_login_log', array(
      'username' => $request->getParam('username'),
      'password' => ($request->getParam('password') == '' ? '' : new Zend_Db_Expr("SHA('" . $request->getParam('password') . "')")),
      'ip' => $_SERVER['REMOTE_ADDR'],
      'browser' => $_SERVER['HTTP_USER_AGENT']
    ));

    // Get our authentication adapter and check credentials
    $adapter = $this->getAuthAdapter($form->getValues());

    $auth = Zend_Auth::getInstance();
    $result = $auth->authenticate($adapter);
    if (!$result->isValid()) {
      $this->view->form = $form;
      $this->_flashMessenger->addMessage("Neplatné uživatelské jméno nebo heslo.");
      return $this->redirect("login"); // re-render the login form
    }

    $select = $db->select()
      ->from('users', array('id', 'email', 'name_full', 'password_expiration', 'password_hash', 'username', 'expiration'))
      ->joinLeft('users_permissions', 'users_permissions.id_users = users.id', array())
      ->joinLeft('users_roles', 'users_roles.id = users_permissions.id_users_roles', array('role' => new Zend_Db_Expr("GROUP_CONCAT(role_name)")))
      ->where('users.username LIKE ?', $auth->getIdentity())
      ->where('users.is_active = 1')
      ->group('users.id');

    $row = $db->fetchRow($select);
    // user is blocked
    if (!$row) {
      Zend_Auth::getInstance()->clearIdentity();
      $this->view->form = $form;
      $this->_flashMessenger->addMessage("Přístup odepřen. Kontaktujte manažera.");
      return $this->redirect("login"); // re-render the login form
    }


    // We're authenticated! Redirect to the home page
    $authNamespace = new Zend_Session_Namespace('Zend_Auth');
    $authNamespace->user = $auth->getIdentity();
    $authNamespace->user_id = $row['id'];
    $authNamespace->username = $row['username'];
    $authNamespace->user_role = explode(',', $row['role']);
    $authNamespace->user_email = $row['email'];
    $authNamespace->user_name_full = $row['name_full'];
    $authNamespace->salt = $row['password_hash'];
    $authNamespace->passwordExpired = false;
    $authNamespace->expiration = (int) $row['expiration'];
    $authNamespace->setExpirationSeconds($row['expiration'] * 60);

    // password is expired
    $expDate = new DateTime($row['password_expiration']);
    $nowDate = new DateTime('now');
    if ($expDate <= $nowDate) {
      $authNamespace->passwordExpired = true;
      $this->_flashMessenger->addMessage("Vaše heslo expirovalo. Prosím, změňte si ho.");
      $this->redirect($this->view->url(array(
          'controller' => 'admin',
          'action' => 'user-pwd'
      )));
    }
    $this->redirect("index");
  }
    
public function clearAction(){
  switch ($this->getParam('reason')) {
    case self::WORKER_NON_EXISTS:
      $this->_flashMessenger->addMessage('Neexistuje účet pracovníka. Kontaktujte administrátora nebo manažera.');
      break;
    default:
      $this->_flashMessenger->addMessage('Byli jste odhlášeni.');
      break;
  }
  Zend_Auth::getInstance()->clearIdentity();
  $this->redirect('/login');
}

}
