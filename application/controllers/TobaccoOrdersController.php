<?php
  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class TobaccoOrdersController extends Meduse_Controller_Action {

    protected $db = NULL;

    public function init() {
      $this->db = Zend_Registry::get('db');
      $this->_helper->layout->setLayout('bootstrap-tobacco');
    }
  
    public function indexAction() {
    
      $reg = Zend_Registry::get('tobacco_orders');
    
      $tOrders = new Table_TobaccoOrders();
      $this->view->title = "Objednávky";
    
      $emailTemplates = new Table_EmailTemplates();
      $this->view->templates = $emailTemplates->getList();
      $this->view->sendmailForm = new Tobacco_EmailTemplates_SendmailForm();
    
    
      $this->view->tab = $this->_request->getParam('tab');;
      $filterTabs = Utils::isAllowed('tobacco-orders/list-all-orders') ? [] : ['closed'];
      $this->view->tabs = $this->getDataTabs($this->view->tab, $filterTabs);
    
      if ($this->view->tab) {
      
        // pokud zname zalozku, vygenerujeme obsah a posleme ajaxem
        $this->_helper->layout->setLayout('ajax');
        $reg->last_orders_tab = $this->view->tab;
        $params = ['tab' => $this->view->tab];
      
        // strankovani
        $params['page'] = $this->_request->getParam('page', 1);
        
        $params['user'] = Utils::getCurrentUserId();
        $params['view_private'] = Utils::isAllowed('orders/view-private');
      
        // deafultni razeni - ruzne pro konkretni zalozky
        switch ($this->view->tab) {
          case 'new':
            $sort = 'date_request';
            break;
          case 'open':
            $sort = 'date_open';
            break;
          case 'closed':
            $sort = 'date_exp';
            break;
          case 'trash':
            $sort = 'date_delete';
            break;
          default:
            $sort = 'no';
            break;
        }
        $params['sort'] = $this->_request->getParam('sort', $sort);
        $sorder = (in_array($params['sort'], [
          'no',
          'date_exp',
          'date_delete',
          'date_request',
          'date_open',
        ])) ? 'desc' : 'asc';
        $params['sorder'] = $this->_request->getParam('sorder', $sorder);
      
        // expedovane objdenavky - zalozky s roky
        if ($this->view->tab === 'closed') {
          if (!$reg->last_orders_year) {
            $reg->last_orders_year = date('Y');
          }
          $params['year'] = $this->_request->getParam('year', $reg->last_orders_year);
          $reg->last_orders_year = $params['year'];
          $this->view->year = $params['year'];
          $this->view->years = $tOrders->getYears();
        }
      
        // vygenerovany datovy grid
        $this->view->grid = $tOrders->getDataGrid($params);
      }
    
      // pokud nezname zalozku, vygenerujeme contejner pro ajax call
      else {
        if (!$reg->last_orders_tab) {
          $reg->last_orders_tab = $filterTabs ? reset($filterTabs) : 'open';
        }
        $this->view->lastTab = $reg->last_orders_tab;
        $this->_helper->layout->setLayout('bootstrap-tobacco');
      }
    }
  
    public function detailAction() {

      if(!$this->_request->getParam('id')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky.');
        $this->redirect('/tobacco-orders');
      }

      $acl = Zend_Registry::get('acl');
      $this->view->acl = $acl;

      $orders =  new Table_TobaccoOrders();
      try {
        $order = new Tobacco_Orders_Order($this->_request->getParam('id'));
      }
      catch (Orders_Exceptions_ForbiddenAccess $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
        $this->redirect($this->view->url(array('action' => 'index', 'order' => NULL, 'id' => NULL)));
      }
      if ($order->isService()) {
        $this->view->data = $order->getServices();
      }
      else {
        $this->view->data = $orders->getSortedOrderParts($this->_request->getParam('id'));
        $this->view->pipeSummary = $orders->getPipeSummary($this->_request->getParam('id'));
      }

      $tobaccoAmount = $order->getTobbacoAmount();
      $this->view->tobaccoAmount = $tobaccoAmount;

      $this->view->packages = '';
      if ($packagesSum = $order->getPackagesSum()) {
        $pkgs = array();
        foreach ($packagesSum as $package => $amount) {
          $pkgs[] = $amount . ' ks ' . $package . 'g';
        }
        $this->view->packages = '(' . implode(' + ', $pkgs) . ')';
      }

      $customer = NULL;
      $customerId = $order->getRow()->id_customers;
      // get addresses
      if ($customerId) {
        $customer = new Customers_Customer($order->getRow()->id_customers);
        $this->view->customer = $customer;
        $this->view->customerName = $customer->getName();
        $order->address = $order->getAddress();
        $order->addressInvoice = $customer->getBillingAddress();
      }
      $this->view->title = 'Objednávka ' . $order->getNO();
      $this->view->order = $order;
      $this->view->invoices = $order->getInvoices(!$acl->isAllowed('invoice/show-non-numbered'));
      $this->view->dbRowOrder = $orders->find($this->_request->getParam('id'))->current();

      $this->view->carrier = $order->getCarrier();

      $tPricelists = new Table_Pricelists();
      $pricelistId = $order->getPricelist();
      if ($pricelistId) {
        $this->view->priceList = $tPricelists->find($pricelistId)->current();
      } else {
        $this->view->priceList = null;
      }

      $tCoefs = new Table_PricesCoefs();
      $recPricelist = $tCoefs->findPoint($tobaccoAmount);

      if ($recPricelist && $customer) {
        $id = $customer->getType() == 'B2B' ? $recPricelist['id_b2b'] : $recPricelist['id_b2c'];
        if ($id != $pricelistId) {
          $this->view->recommendedPricelist = array(
            'id' => $id,
            'name' => $customer->getType() == 'B2B' ? $recPricelist['name_b2b'] : $recPricelist['name_b2c'],
          );
        } else {
          $this->view->recommendedPricelist = null;
        }
      }

      $allow_edit = $order->getStatus() != Table_Orders::STATUS_CLOSED && $order->getStatus() != Table_Orders::STATUS_DELETED && $acl->isAllowed('tobacco-order-edit/index');
      if (isset($_SESSION['Zend_Auth']['user_role'])) {
        foreach($_SESSION['Zend_Auth']['user_role'] as $role) {
          if (in_array($role, array('manager', 'admin'))) {
            $allow_edit = TRUE;
            break;
          }
        }
      }
      $this->view->allow_edit = $allow_edit;

      $allow_edit_address = is_null($customer) && $order->getStatus() != Table_Orders::STATUS_CLOSED;
      if (isset($_SESSION['Zend_Auth']['user_role'])) {
        foreach($_SESSION['Zend_Auth']['user_role'] as $role) {
          if (in_array($role, array('manager', 'admin', 'accountant_tobacco'))) {
            $allow_edit_address = TRUE;
            break;
          }
        }
      }

      $this->view->allow_edit_address = $allow_edit_address;
      $this->view->allow_invoices = $acl->isAllowed('tobacco-invoice/index');

      // zobrazit moznost vystavit ostrou fakturu
      $this->view->allow_invoicing = $order->checkRegularCondition();

      $this->view->uploadForm = new Orders_FileUploadForm();
      $this->view->uploadForm->setOrderId($order->getID());
      $this->view->uploadForm->setAction('/tobacco-orders/file-upload');
      $this->view->files = $order->getFilesList();

      $regularInvoice = $order->getRegularInvoice();
      $this->view->regular_invoice_id = $regularInvoice ? $regularInvoice->getId() : NULL;
    }


    public function fileUploadAction() {
      $this->_helper->viewRenderer->setNoRender();
      if ($this->_request->isPost()) {
        $received = [];
        $idOrder = $this->getParam('id_orders');
        $upload = new Zend_File_Transfer();
        $files = $upload->getFileInfo();
        foreach ($files as $file => $info) {
          if (!$upload->isUploaded($file)) {
            $this->_flashMessenger->addMessage('Nebyl nahrán žádný soubor.');
            continue;
          }
          if (!$upload->isValid($file)) {
            $this->_flashMessenger->addMessage('Soubor <em>' . $file . '</em> není přípustný.');
            continue;
          }
          $dir = APPLICATION_PATH . Tobacco_Orders_Order::DATA_UPLOADS_DIR . Tobacco_Orders_Order::DATA_UPLOADS_SUBDIR_PREFIX . $idOrder;
          if (!file_exists($dir)) {
            mkdir($dir);
          }
          $filename =  $dir . '/' . $info['name'];
          $upload->addFilter('Rename', ['target' => $filename, 'overwrite' => TRUE], $file);
          $received[] = $info['name'];
        }
        if ($upload->receive()) {
          $auth = new Zend_Session_Namespace('Zend_Auth');
          $table = new Table_OrdersFiles();
          $data = [];
          foreach ($received as $item) {
            $data[] = [
              'filename' => $item,
              'title' => $this->_request->getParam('title'),
              'id_users' => $auth->user_id,
            ];
          }
          $table->addFiles($idOrder, $data);
          $this->_flashMessenger->addMessage('Soubor <em>' . implode(', ', $received) . '</em> byl přidán k objednávce.');
        }
        else {
          $this->_flashMessenger->addMessage('Soubor se nepodařilo k objednávce přidat.');
        }

        $this->redirect('/tobacco-orders/detail/id/' . $idOrder . '#infos');
      }
    }

    public function fileRemoveAction() {
      $this->_helper->viewRenderer->setNoRender();
      $idOrder = $this->_request->getParam('id');
      $idFile = $this->_request->getParam('fid');
      $order = new Tobacco_Orders_Order($idOrder);
      if ($order->fileDelete($idFile)) {
        $this->_flashMessenger->addMessage('Soubor byl smazán.');
      }
      else {
        $this->_flashMessenger->addMessage('Soubor se nepodařilo smazat.');
      }
      $this->redirect('/tobacco-orders/detail/id/' . $idOrder . '#infos');
    }

    public function fileDownloadAction() {
      $this->_helper->viewRenderer->setNoRender();
      $idOrder = $this->_request->getParam('id');
      $idFile = $this->_request->getParam('fid');
      $order = new Tobacco_Orders_Order($idOrder);
      if ($file = $order->getFile($idFile)) {
        $dir = APPLICATION_PATH . Tobacco_Orders_Order::DATA_UPLOADS_DIR . Tobacco_Orders_Order::DATA_UPLOADS_SUBDIR_PREFIX . $idOrder;
        $filepath = $dir . '/' . $file['filename'];
        if (file_exists($filepath)) {
          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
          header('Expires: 0');
          header('Cache-Control: must-revalidate');
          header('Pragma: public');
          header('Content-Length: ' . filesize($filepath));
          flush(); // Flush system output buffer
          readfile($filepath);
          die();
        }
        else {
          http_response_code(404);
          die();
        }
      }
      else {
        die("Invalid file name!");
      }
    }

    public function setPricelistAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      try {
        $order = new Tobacco_Orders_Order($this->_request->getParam('id'));
        $order->setPricelist($this->_request->getParam('pricelist'));
        $this->_flashMessenger->addMessage("Ceník byl přiřazen.");
      } catch (Exception $e) {
        $this->_flashMessenger->addMessage("Ceník nebyl přiřazen.");
      }
      $this->_redirect('/tobacco-orders/detail/id/' . $order->getID());
    }

    public function openAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $order = new Orders_Order($this->_request->getParam('id'));
      if ($order->getStatus() != Table_Orders::STATUS_OPEN) {

        // pokud jiz objenavka byla expedovana a vratila se zpet
        if ($order->getStatus() == Table_Orders::STATUS_CLOSED) {
          $tPartsWarehouse = new Table_PartsWarehouse();
          $tOrdersProducts = new Table_OrdersProducts();
          $tOrdersProductsBatch = new Table_OrdersProductsBatch();
          $tPartsBatch     = new Table_PartsBatch();

          $products = $tOrdersProducts->select()->where('id_orders = ?', $this->_request->getParam('id'))->query()->fetchAll();
          if ($products) {
            foreach ($products as $product) {
              $tPartsWarehouse->insertPart($product['id_products'], $product['amount']);

              $batches = $tOrdersProductsBatch->select()->where('id_orders_products = ?', $product['id'])->query()->fetchAll();
              if ($batches) {
                foreach ($batches as $batch) {
                  $tPartsBatch->addBatchAmount($product['id_products'], $batch['batch'], $batch['amount']);
                  $tOrdersProductsBatch->delete('id = "' . $batch['id'] . '"');
                }
              }
            }
          }
        }

        $orders = new Table_TobaccoOrders();
        $orders->openOrder($this->_request->getParam('id'));

        // odeslani emailu
        $tEmails = new Table_Emails();
        $tEmails->sendEmail(Table_Emails::EMAIL_TOBACCO_ORDER_OPEN_NOTICE, array(
          'orderId' => $order->getID(),
        ));

        // zapsani do aktualit
        $act = new Table_Actualities();
        $act->add("Otevřel objednávku č. " . $order->getNO(), Tobacco_Parts_Part::WH_TYPE);
        $this->_flashMessenger->addMessage("Objednávka č. ".$order->getNO()." byla označena jako otevřená.");
      }
      $this->_redirect('/tobacco-orders/detail/id/' . $order->getID());
    }

    public function payAction() {

      if (!$this->_request->getParam('id')){
        $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky.');
        $this->redirect("/tobacco-orders");
      }
      switch ($this->_request->getParam('from', null)) {
        case 'detail':
          $redirect = '/tobacco-orders/detail/id/' . $this->_request->getParam('id');
          break;
        case 'index':
        default:
          $redirect = '/tobacco-orders';
          break;
      }
      $form = new Orders_PaymentAcceptDateForm();
      $tOrders = new Table_TobaccoOrders();
      $idOrder = (int) $this->_request->getParam('id');
      $order = new Orders_Order($idOrder);
      $rOrder = $order->getRow();
      if($this->_request->isPost() && $form->isValid($this->_request->getParams())){
        $tOrders->update(array(
          'date_payment_accept' => $order->fromHRtoISO($this->_request->getParam('meduse_date_payment_accept')),
          'date_pay_all' => $order->fromHRtoISO($this->_request->getParam('meduse_date_payment_accept')),
          ), "id = " . $tOrders->getAdapter()->quote($idOrder));
        $this->_flashMessenger->addMessage("Datum potvrzení bylo uloženo.");
        $this->redirect($redirect);
      } elseif($rOrder !== null && $rOrder->date_payment_accept) {
        $form->meduse_date_payment_accept->setValue($order->fromISOtoHR($rOrder->date_payment_accept) );
      }
      $this->view->form = $form;
      $this->view->title = 'Objednávka ' . $order->getNO() . ' - potvrzení platby';
    }

    public function expeditionAction(){

      if (!$this->_request->getParam('id')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky.');
        $this->redirect('/tobacco-orders');
      }
      switch ($this->_request->getParam('from', null)) {
        case 'detail':
          $redirect = '/tobacco-orders/detail/id/' . $this->_request->getParam('id');
          break;
        case 'index':
        default:
          $redirect = '/tobacco-orders';
          break;
      }

      $order = new Tobacco_Orders_Order($this->_request->getParam('id'));

      if (is_null($order->getCustomer())) {
        $this->_flashMessenger->addMessage('Objednávku nelze expedovat bez zadání uživatele.');
        $this->redirect('/tobacco-orders/detail/id/' . $this->_request->getParam('id'));
      }

      $tableOrders = new Table_TobaccoOrders();
      $rowOrder = $tableOrders->find($order->getID())->current();

      $form = new Tobacco_Orders_ExpeditionForm();
      if ($this->_request->isPost()) {
        $params = $this->_request->getParams();
        if ($form->isValid($params)) {
          try {

            $date = new Zend_Date($params['expedition_date'], 'dd.MM.yyyy');
            $data['date_exp'] = $date->toString('yyyy-MM-dd');
            if ($params['tracking_number']) {
              $data['tracking_no'] = $params['tracking_number'];
            }
            $order->processExpedition($data);

            // odeslani emailu
            $tEmails = new Table_Emails();
            $tEmails->sendEmail(Table_Emails::EMAIL_TOBACCO_ORDER_EXPEDITION_NOTICE, array(
              'orderId' => $order->getID(),
            ));

            $act = new Table_Actualities();
            $act->add("Expedoval objednávku č. " . $order->getNO(), Table_TobaccoWarehouse::WH_TYPE);
            $this->_flashMessenger->addMessage("Uloženo");
          } catch (Exception $e) {
            $this->_flashMessenger->addMessage("Objednávku se nepodařilo expedovat. <br>" . $e->getMessage());
            switch ($e->getCode()) {
              case 2: $this->_redirect('/tobacco-orders/prepare/id/' . $order->getID()); break;
              default: $this->_redirect($redirect); break;
            }
          }
          $this->_redirect($redirect);
        }
      } else {
        $date = Zend_Date::now();
        $params = array(
          'order_id' => $rowOrder->id,
          'expedition_date' => $rowOrder->date_exp ? $rowOrder->date_exp : $date->toString('dd.MM.yyyy'),
          'tracking_number' => $order->getTrackingNo(),
        );
      }
      $form->populate($params);
      $this->view->form = $form;
      $this->view->title = 'Objednávka ' . $order->getNO() . ' - expedice';
    }

    public function selectProductsAction() {
      try {
        $order = new Orders_Order($this->_request->getParam('order'));
      }
      catch (Orders_Exceptions_ForbiddenAccess $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
        $this->redirect($this->view->url(array('action' => 'index', 'order' => NULL, 'id' => NULL)));
        exit;
      }
      $params = array(
        'name' => $this->_request->getParam('name', null),
        'id' => $this->_request->getParam('id', null),
        'fs_only' => $order->getPaymentMethod() == Table_Orders::PM_CASH,
      );
      $tParts = new Table_TobaccoParts();
      $this->view->data = $tParts->getProducts($params);
      $this->view->orderid = $order->getID();

      $tOrders = new Table_Orders();
      $this->view->order_items = array();
      $this->view->order_free_items = array();
      foreach($tOrders->getSortedOrderParts($order->getID()) as $part) {
        $this->view->order_items[$part['id_products']] = $part['amount'];
        if($part['free_amount'] > 0) {
          $this->view->order_free_items[$part['id_products']] = $part['free_amount'];
        }
      }
      $tReservations = new Table_OrdersProductsParts();
      $this->view->rezerv = $tReservations->getOrdersReservations(array($order->getID()));
    }

    public function deleteItemAction(){
      if (!$this->_request->getParam('order')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky');
        $this->redirect('/tobacco-orders');
      }

      if(!$this->_request->getParam('item')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID položky');
        $this->redirect('/tobacco-orders/detail/id' . $this->_request->getParam('order'));
      }

      $order = new Orders_Order($this->_request->getParam('order'));
      $order->cancelAllPrepared($this->_request->getParam('item'));
      $orders_products = new Table_OrdersProducts();
      $select = $orders_products->select()->where("id_orders =".$this->_request->getParam('order')." AND id_products LIKE '".$this->_request->getParam('item')."'");
      $row = $orders_products->fetchRow($select);
      $orders_products->getAdapter()->delete("orders_products" ,"id = ".$row['id']);
      $order->recalculateExpectedPayment();
      $act = new Table_Actualities();
      $act->add("Smazal produkt ".$this->_request->getParam('item')." z objednávky č. ".$order->getNO());
      $this->_flashMessenger->addMessage("Položka objednávky ".$this->_request->getParam('item')." vymazána.");
      $this->redirect('tobacco-orders/detail/id/'.$this->_request->getParam('order') . '#items');
      $this->_helper->viewRenderer->setNoRender();
    }

    public function prepareAction(){
      if (!$this->_request->getParam('id')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky');
        $this->_redirect('/tobacco-orders');
      }
      $orders = new Table_Orders();
      $order = new Orders_Order($this->_request->getParam('id'));

      if ($order->getStatus() == Table_Orders::STATUS_CLOSED) {
        $this->_flashMessenger->addMessage('Objednávka již byla expedována.');
        $this->_redirect('/tobacco-orders');
      }
      $this->view->title = 'Objednávka ' . $order->getNO() . ' - příprava';
      $this->view->data = $orders->getOrderParts($this->_request->getParam('id'));
      $this->view->order = $order;
    }

    public function prepareAllAction() {

      if (!$this->_request->getParam('order')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky');
        $this->_redirect('/tobacco-orders');
      }

      if(!$this->_request->getParam('item')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID položky');
        $this->_redirect('/tobacco-orders/prepare/id' . $this->_request->getParam('order'));
      }

      $item = $this->_request->getParam('item');
      $order = new Tobacco_Orders_Order($this->_request->getParam('order'));
      $part = new Tobacco_Parts_Part($item);
      $batchArray = array();
      // pokud se jedna o tabak, zobrazime formular pro vyber sarze
      if (in_array($part->getCtg(), array(
        Table_TobaccoWarehouse::CTG_TOBACCO_NFS,
        Table_TobaccoWarehouse::CTG_TOBACCO_FS))) {

        $batches = $part->getBatches();
        $amount = $order->getProductAmount($part->getID());
        $form = new Tobacco_Parts_ReservationBatchForm();
        $form->setOrderId($order->getID());
        $form->setPartId($part->getID());
        $form->setBatches($batches, $amount);
        $form->setTotalAmount($amount);
        $form->setLegend('Vyběr šarží pro rezervaci produktu ' . $part->getID());

        if ($this->_request->isPost()
          && $form->isValid($this->_request->getParams())) {
            foreach($form->getValues() as $name => $value) {
              @list($b, $n, $i) = explode('_', $name, 3);
              if ($n != 'number') continue;
              $batchArray[$value] = $form->getValue('batch_amount_' . $i);
            }
        } else {
          $this->view->title = 'Objednávka ' . $order->getNO() . ' - rezervace';
          $this->view->form = $form;
          return;
       }
            }
       $this->_helper->viewRenderer->setNoRender();

        try{
          $order->reserveAll($part->getID(), $batchArray);
          $this->_flashMessenger->addMessage("Všechny součásti produktu byly rezervovány a vyřazeny ze skladu.");
          if ($order->isReady()) {
            // odesleme mail o pripravenosti vsech soucasti
            $tEmails = new Table_Emails();
            $tEmails->sendEmail(Table_Emails::EMAIL_TOBACCO_ORDER_IS_READY_NOTICE, array(
              'orderId' => $order->getID(),
            ));
          }
        } catch (Parts_Exceptions_LowAmount $e) {
          $this->_flashMessenger->addMessage("Na skladu neni dostatek součásti ".$e->getMessage());
        } catch (Exception $e) {
          $this->_flashMessenger->addMessage("Nepodařilo se rezervovat všechny součásti produktu. ".$e->getMessage());
        }
        $this->_request->setParams(array());

        //kontrola limitního množství
        $tParts = new Table_PartsWarehouse();
        $crit = $tParts->checkCritical($item);
        print_r($crit);
        if ($crit) {$this->_flashMessenger->addMessage('Množství balení tabáku ' . $crit . ' na skladě dosáhlo kritické úrovně.');}

        $this->_redirect('tobacco-orders/prepare/id/'.$this->_request->getParam('order'));

    }

    public function cancelAllPreparedAction(){
      $this->_helper->viewRenderer->setNoRender();
      if (!$this->_request->getParam('order')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky');
        $this->_redirect('/tobacco-orders');
      }
      if(!$this->_request->getParam('item')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID položky');
        $this->_redirect('/tobacco-orders/prepare/id' . $this->_request->getParam('order'));
      }
      $order   = new Tobacco_Orders_Order($this->_request->getParam('order'));
      $product = new Tobacco_Parts_Part($this->_request->getParam('item'));
      try {
        $order->cancelAllPrepared($product->getID());
        $this->_flashMessenger->addMessage("Rezervace zrušena. Součásti jsou zpátky na skladě.");
      } catch (Exception $e) {
        $this->_flashMessenger->addMessage("Rezervaci se nepodarilo zrusit.".$e->getMessage());
      }
      $this->_redirect('tobacco-orders/prepare/id/'.$this->_request->getParam('order'));
    }

    public function returnAction() {
      if (!$this->_request->getParam('id')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky.');
        $this->redirect('tobacco-orders');
      }
      $order = new Tobacco_Orders_Order($this->_request->getParam('id'));
      try{
        $order->returnFromClosed();
        $act = new Table_Actualities();
        $act->add("Vrátil na sklad objednávku č. " . $order->getNO());
        $this->_flashMessenger->addMessage("Objednávka byla vrácena na sklad.");
      } catch (Exception $e) {
        $this->_flashMessenger->addMessage("Objednávku se nepodařilo vrátit. <br>".$e->getMessage());
      }
      $this->redirect('tobacco-orders/detail/id/' . $this->_request->getParam('id'));
    }

      /**
       * @throws Orders_Exceptions_ForbiddenAccess
       * @throws Exception
       */
      public function deleteAction(): void
      {
          if (!$this->_request->getParam('id')) {
              $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky.');
              $this->redirect('tobacco-orders');
          }
          $order = new Tobacco_Orders_Order($this->_request->getParam('id'));

          if ($order->hasReservedProducts()) {
              $this->_flashMessenger->addMessage('Objednávku nelze odstranit, má rezervované produkty.');
              $this->redirect($this->view->url(['action' => 'detail']));
          }

          /** @var array<Invoice_Invoice> $numberedInvoices */
          $numberedInvoices = $order->getInvoices(true);
          $invoiceNumbers = [];
          if ($numberedInvoices) {
              foreach ($numberedInvoices as $invoice) {
                  if (!$invoice->isRegular() && !$invoice->isCredit()) {
                      continue;
                  }
                  $invoiceNumbers[$invoice->getId()] = $invoice->getNo();
                  $invoice->setNo(null);
              }
          }

          $order->delete();

          $act = new Table_Actualities();
          $act->add("Vyhodil do koše objednávku č. " . $order->getNO());

          $this->_flashMessenger->addMessage("Objednávka č. " . $order->getNO() . " " . $order->getTitle() . " byla vyhozena do koše.");

          if ($invoiceNumbers) {
              array_walk($invoiceNumbers, static function (&$item, $key) {
                  $item = '<a href="/tobacco-invoice/prepare/id/' . $key . '" target="_blank">' . $item . '</a>';
              });
              $this->_flashMessenger->addMessage('Těmto fakturám byla odebrána čísla: ' . implode(', ', $invoiceNumbers));
          }

          $this->redirect('tobacco-orders/detail/id/' . $this->_request->getParam('id'));
      }

      public function getCalculatorDataAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $order = new Orders_Order($this->_request->getParam('order'));
      $data = $order->getOrderItemsPriceCount();
      $rate = $order->getExpectedPaymentRate();
      $currency = $order->getExpectedPaymentCurrency();
      $price = $currency == 'CZK' ? $data['CntEur'] * $rate : $data['CntEur'];
      $this->_helper->json(array('result' => array(
        'price' => Meduse_Currency::format($price),
        'currency' => $currency,
      )));
    }

    public function notifyAction() {
      $id_order = $this->_request->getParam('order_id');
			$id_customer = $this->_request->getParam('customer_id');
			$email_subject = $this->_request->getParam('email_subject');
			$email_body = $this->_request->getParam('email_body');
			$email_to = $this->_request->getParam('email_to');

			if ($id_order && $id_customer) {
				$tCustomers = new Table_Customers();
				$tCustomers->update(array('email' => $email_to), 'id = ' . $tCustomers->getAdapter()->quote($id_customer) . ' AND email IS NULL');

        //kontrola stavu obj.
        $tOrders = new Table_Orders();
        $order_info = $tOrders->find($id_order)->current()->toArray();

        switch ($order_info['status']) {
          case Table_Orders::STATUS_NEW:
            $email_type = Table_Emails::EMAIL_ORDER_NEW_CUSTOMER_NOTICE;
          break;

          case Table_Orders::STATUS_OPEN:
            $email_type = Table_Emails::EMAIL_ORDER_OPEN_CUSTOMER_NOTICE;
          break;

          case Table_Orders::STATUS_CLOSED:
            $email_type = Table_Emails::EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE;
          break;
        }

        //Odesílání e-mailu
				$email = new Table_Emails();
				$email->sendEmail($email_type, array(
					'orderId' => $id_order,
					'subject' => $email_subject,
					'body' => $email_body,
					'to' => $email_to
				));
			}
			$this->_redirect($this->view->url(array('action' => null)));
    }

    public function prepareListAction() {
      if (!$this->_request->getParam('id')) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky');
        $this->_redirect('/tobacco-orders');
      }
      $orders = new Table_Orders();
      $order = new Orders_Order($this->_request->getParam('id'));

      if ($order->getStatus() == Table_Orders::STATUS_CLOSED) {
        $this->_flashMessenger->addMessage('Objednávka již byla expedována.');
        $this->_redirect('/tobacco-orders');
      }
      $this->view->title = 'Objednávka ' . $order->getNO() . ' - příprava';
      $this->view->data = $orders->getOrderPartsDetailed($this->_request->getParam('id'));
      $this->view->order = $order;
    }



  public function stampAllAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $id = $this->_request->getParam('id');

    try {$tobaccoPartsWarehouse = new Tobacco_Orders_Order($id);}
    catch (Exception $e) {
      $this->_flashMessenger->addMessage($e->getMessage());
      $this->_redirect('/tobacco-orders');
    }

    //ověření, zdali je obj. otevřená
    try {
      $tobaccoPartsWarehouse->validateOrderStatus($id, Table_Orders::STATUS_OPEN);
    }
    catch (Exception $e){
      $this->_flashMessenger->addMessage($e->getMessage());
      $this->_redirect('/tobacco-orders');
    }

    $orders = new Table_Orders();
    $order = new Orders_Order($id);

    $data = $orders->getOrderPartsCtg(Table_TobaccoWarehouse::CTG_TOBACCO_NFS, $id);
    $multiparts = array(); // pro kontrolu limitního množství
    $tSubparts = new Table_PartsSubparts(); //pro kontrolu podčástí a lim. množství

    foreach ($data as $d) {
      //získání šarží tabáku
      $tPartsBatch = new Table_PartsBatch();
      $batches = $tPartsBatch->findByPart($d['id']);

      //určení šarží pro okolkování
      $tPartsBatch  = new Table_PartsBatch();
      $batches_list = $tPartsBatch->getAmountByPart($d['id'], $d['amount']);

      //získání ID odpovídajícího prodejného tabáku
      $part    = new Tobacco_Parts_Part($d['id']);
      $supPart = $part->getSupParts();
      $supPart = $supPart[0];

      if (is_null($supPart)){
        $message = 'K nekolkovanému tabáku ' . $d['id'] . ': "' . $d['name'] . '" neexistuje žádný příslušící kolkovaný.';
      } else {
        //kontrola množství součástí (tabáku, kolků atp.);
        $subparts_missing = array();
        $sp_test          = TRUE;

        foreach ($tSubparts->selectSubpartsAmount($supPart['id_multipart'])->query()->fetchAll() as $subpart) {
          ///určení množství potřebného v obj.
          $subpart_amount = $d['amount'] * $subpart['amount_per_multipart'];
          echo $subpart['part'] . ': ' . $subpart_amount . ' (skladem: ' . $subpart['amount_warehouse'] . ') <br />';
          ///kontrola dostupnosti
          if ($subpart_amount > $subpart['amount_warehouse']) {
            $subparts_missing[] = $subpart['part'] . ' (' . ($subpart_amount - $subpart['amount_warehouse']) . ')';
            $sp_test = FALSE;
          }
        }

        //refaktorizace šarží do formátu pro kolkovací fci
        if ($sp_test) {
          //práce s šaržemi
          $toStamp = array();
          foreach ($batches_list as $value) {
            $toStamp[] = array(
                'part' => $supPart['id_parts'],
                'multipart' => $supPart['id_multipart'],
                'batch' => $value['batch'],
                'amount' => $value['amount'],
            );
          }


          //okolkování tabáků
          if ($toStamp) {
            $totalAmount = 0;
            $tWarehouse = new Table_TobaccoWarehouse();
            $this->db->beginTransaction();

            foreach ($toStamp as $row) {
              $tWarehouse->stamp(
                      $row['part'], $row['multipart'], $row['batch'], $row['amount']);
              $totalAmount += $row['amount'];
            }
            $this->db->commit();

            //nahrazení neokolkovaných tabáků v obj. okolkovanými
            $order = new Tobacco_Orders_Order($id);
            $order->cancelAllPrepared($supPart['id_parts']);
            $order->cancelAllPrepared($supPart['id_multipart']);
            $order->removeProduct($supPart['id_parts']);
            $order->addProduct($supPart['id_multipart'], $totalAmount);

            //zadání pro kontrolu limitního množství
            $multiparts[] = $supPart['id_multipart'];
          }

          //výstupní zpráva
          $batches_message = array();
          foreach ($batches_list as $bl_val) {
            $batches_message[] = $bl_val['batch'] . ' (' . $bl_val['amount'] . ')';
          }
          $message = $d['name'] . ': ' . implode(', ', $batches_message);
        } else {
          $message = $d['name'] . ' – chybí násl. součásti: ' . implode(', ', $subparts_missing);
        }
      }

      //VÝSTUP:
      $this->_flashMessenger->addMessage($message);
    }

    //LIMITNÍ MNOŽSTVÍ

    //kontrola limitního množství
    $parts_critical = array();
    foreach (array_unique($multiparts) as $multipart) {
      $parts_amount = $tSubparts->selectSubpartsAmount($multipart)->query()->fetchAll();
      foreach ($parts_amount as $part_amount) {
        if ($part_amount['amount_warehouse']<=$part_amount['amount_critical']) {
          $parts_critical[$part_amount['part']] = $part_amount['amount_warehouse'];
        }
      }
    }
    ///report limitního množství
    $message_critical = array();
    foreach ($parts_critical as $crit_multipart => $crit_amount) {
      $message_critical[] = $crit_multipart . ' (' . $crit_amount . ')';
    }
    if ($message_critical) {
      $this->_flashMessenger->addMessage('Následující součásti dosáhly limitního množství: ' . implode(', ', $message_critical));
    }

    $this->_redirect('/tobacco-orders/prepare/id/' . $id);
  }


  public function cancelAllPreparedTotalAction(){
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    //info o obj.
    $id = $this->_request->getParam('id');

    if (!$id) {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky');
      $this->_redirect('/tobacco-orders');
    }

    $orders = new Table_Orders();
    $order = new Orders_Order($id);

    if ($order->getStatus() == Table_Orders::STATUS_CLOSED) {
      $this->_flashMessenger->addMessage('Objednávka již byla expedována.');
      $this->_redirect('/tobacco-orders');
    }

    //získání produktů z obj.
    $data = $orders->getOrderParts($id);

    //odrezervování všeho
    foreach ($data as $category) {
      foreach ($category as $d) {
        $order   = new Tobacco_Orders_Order($this->_request->getParam('id'));
        $product = new Tobacco_Parts_Part($d['id']);
        $order->cancelAllPrepared($product->getID());
      }
    }

    //přesměrování zpět na přípravu obj.
    $this->_redirect('tobacco-orders/prepare/id/'.$this->_request->getParam('id'));
  }

  public function prepareAllTotalAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);
    if (!$id = $this->_request->getParam('id')) {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID objednávky');
      $this->_redirect('/tobacco-orders');
    }
    $order = new Tobacco_Orders_Order($id);
    if ($order->getStatus() == Table_Orders::STATUS_CLOSED) {
      $this->_flashMessenger->addMessage('Objednávka již byla expedována.');
      $this->_redirect('/tobacco-orders');
    }
    $messages = $order->prepareAllForSale();
    if ($messages) {
      foreach ($messages as $msg) {
        $this->_flashMessenger->addMessage($msg);
      }
    }
    if ($order->isReady()) {
      // odesleme mail o pripravenosti vsech soucasti
      $tEmails = new Table_Emails();
      $tEmails->sendEmail(Table_Emails::EMAIL_TOBACCO_ORDER_IS_READY_NOTICE, array(
        'orderId' => $order->getID(),
      ));
    }
    //přesměrování zpět na přípravu obj.
    $this->_request->setParams(array());
    $this->_redirect('tobacco-orders/prepare-list/id/' . $this->_request->getParam('id'));
  }

  public function calculateExpectedPaymentAction() {

    $id = $this->_request->getParam('id');
    $order = new Tobacco_Orders_Order($id);
    $order->recalculateExpectedPayment();

    $destination = $this->_request->getParam('destination', NULL);
    if ($destination) {
      $this->_redirect($destination);
    }
    else {
      $this->_redirect('/tobacco-orders/detail/id/' . $id);
    }
  }

  public function ajaxGetOrderQueueAction() {
    $orderId = $this->_request->getParam('order');
    $order = new Tobacco_Orders_Order($orderId);
    $queue = EmailFactory_Queue::createQueue();
    $queue->setSubject('Info k přípravě objednávky č. ' . $order->getNO());
    $queue->save();
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $this->_helper->json(array('queue' => $queue->getId()));
  }

  public function ajaxSendOrderQueueAction() {
    $queueId = $this->_request->getParam('queue');
    $queue = EmailFactory_Queue::getQueue($queueId);
    $queue->send(TRUE);
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $this->_helper->json(array('status' => 'sended, deleted', 'queue' => $queueId));
  }

  public function ajaxPrepareForSaleAction() {

    $orderId = $this->_request->getParam('id');
    $partId = $this->_request->getParam('part');
    $amount = $this->_request->getParam('amount');
    $queueId = $this->_request->getParam('queue');

    if ($orderId && $partId && $amount && $queueId) {
      $queue = EmailFactory_Queue::getQueue($queueId);
      $order = new Tobacco_Orders_Order($orderId);
      $part = new Tobacco_Parts_Part($partId);

      $success = $order->prepareForSale($part, $amount, $queue);
      $messages = $queue->getNewMessages(TRUE, '<br>');
      $queue->save();
    }
    else {
      $success = FALSE;
      $messages = array('Chyba: nedostatečné vstupní údaje');
    }

    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $this->_helper->json(array(
      'success' => $success,
      'messages' => $messages,
    ));
  }

  public function newFromClipboardAction() {
      $result = array();
      $form = new Tobacco_Orders_ClipboardForm();
      if ($this->_request->isPost()) {
        $json = NULL;
        $body = $this->getRequest()->getRawBody();
        $chunks = explode('&', $body);
        foreach ($chunks as $chunk) {
          list($key, $value) = explode('=', $chunk);
          if ($key == 'json') {
            $value = urldecode(rawurldecode($value));
            $json = strtr($value, "'", "\"");
            break;
          }
        }
        if ($json) {
          $result = Orders::createFromJson($json, Table_TobaccoWarehouse::WH_TYPE);
          if ($result['order']) {
            $this->_flashMessenger->addMessage('Objednávka č. ' . $result['order']->getNo() . ' byla vytvořena.');
            $this->_flashMessenger->addMessage('<br>Zkontrolujte fakturační adresu z objednávky:<br>' . $result['billing']);
            if ($result['shipping']) {
              $this->_flashMessenger->addMessage('Zkontrolujte dodací adresu z objednávky:<br>' . $result['shipping']);
            }
            $this->redirect('/tobacco-orders/detail/id/' . $result['order']->getId());
          }
          $this->view->result = $result;
        }
      }
      $data = $this->_request->getParam('data');
      if ($data) {
        $json = base64_decode($data);
        $form->setJsonData($json);
      }
      elseif(isset($result['json']) && $result['json']) {
        $form->setJsonData($result['json']);
      }
      $this->view->title = $form->getLegend();
      $this->view->form = $form;
  }
  
    private function getDataTabs($active = NULL, $filterTabs = []): array {
    
      $tabs = [];
    
      if (!$active) {
        $active = empty($filterTabs) ? 'open' : reset($filterTabs);
      }
    
      $availableTabs = [
        'new' => [
          'href' => 'new',
          'title' => 'Vložené',
          'active' => $active == 'new',
        ],
        'open' => [
          'href' => 'open',
          'title' => 'Otevřené',
          'active' => $active == 'open',
        ],
        'closed' => [
          'href' => 'closed',
          'title' => 'Expedované',
          'active' => $active == 'closed',
        ],
        'trash' => [
          'href' => 'trash',
          'title' => 'Koš',
          'active' => $active == 'trash',
        ],
      ];
    
      if (empty($filterTabs)) {
        return $availableTabs;
      }
    
      foreach ($availableTabs as $key => $tab) {
        if (in_array($key, $filterTabs)) {
          $tabs[$key] = $tab;
        }
      }
    
      return $tabs;
    }
  
  
}
