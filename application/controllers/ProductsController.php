<?php
/**
 * Historicky, nepouzivany kontroler
 * Urceny ke smazani
 */
//class ProductsController extends Meduse_Controller_Action{
//
//	public function indexAction(){
//		$this->_helper->layout->setLayout('layout2');
//	}
//
//	public function addAction(){
//		$this->_helper->layout->setLayout('layout2');
//		$this->view->title = " - Přidat produkt";
//
//		$form = new Products_Form();
//
//		if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
//			$products_table = new Table_Products();
//			$products_table->addProduct(
//				$form->getValue('meduse_products_id'),
//				$form->getValue('meduse_products_name'),
//				$form->getValue('meduse_products_type'),
//				$form->getValue('meduse_products_collection'),
//				$form->getValue('meduse_products_desc')
//			);
//			$this->_redirect('products/select-parts/product/'.$form->getValue('meduse_products_id'));
//		}
//
//		$this->view->form = $form;
//	}
//
//	public function editAction(){
//		$this->_helper->layout->setLayout('layout2');
//		if(!$this->_request->getParam('id')) $this->_redirect('products/overview');
//		$this->view->id = $this->_request->getParam('id');
//		$product = new Products_Product($this->_request->getParam('id'));
//		$form = new Products_EditForm();
//
//		if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
//			$data = array(
//				'id_products_types' => $form->meduse_products_type->getValue(),
//				'id_collections' => ($form->meduse_products_collection->getValue() == NULL ? new Zend_Db_Expr('NULL') : $form->meduse_products_collection->getValue()),
//				'name' => $form->meduse_products_name->getValue(),
//				'description' => strip_tags($form->meduse_products_desc->getValue())
//			);
//			$products = new Table_Products();
//			$parts = new Table_Parts();
//			try{
//				$products->update($data, "id LIKE '".$this->_request->getParam('id')."'");
//				$select = $parts->select()->where("id LIKE ?", $this->_request->getParam('id'));
//				if($parts->fetchRow($select)){
//					unset($data['id_products_types']);
//					$parts->update($data, "id LIKE '".$this->_request->getParam('id')."'");
//				}
//				$this->_flashMessenger->addMessage("Produkt upraven.");
//			} catch (Exception $e) {
//				$this->_flashMessenger->addMessage("Produkt se nepodařilo upravit.");
//			}
//			$this->_redirect('products/detail/id/'.$this->_request->getParam('id'));
//		}
//
//		$form->meduse_products_name->setValue($product->getName());
//		$form->meduse_products_collection->setValue($product->getCollection());
//		$form->meduse_products_type->setValue($product->getType());
//		$form->meduse_products_desc->setValue($product->getDesc());
//		$this->view->form = $form;
//
//	}
//
//	/**
//	 * uprava mnozstvi soucasti v produktu
//	 * @return unknown_type
//	 */
//	public function editAmountAction(){
//
//		$this->_helper->layout->setLayout('layout2');
//
//		if(!$this->_request->getParam('id')) $this->_redirect('parts/overview');
//		if(!$this->_request->getParam('part')) $this->_redirect('parts/overview');
//
//		$product = new Products_Product($this->_request->getParam('id'));
//		$part    = new Parts_Part($this->_request->getParam('part'));
//		$this->view->id = $this->_request->getParam('id');
//
//		$form = new Meduse_Form('parts_box', 'Upravit množství součásti '.$this->_request->getParam('part').' v produktu '.$this->_request->getParam('id'));
//		$element = new Meduse_Form_Element_Text('meduse_part_amount');
//		$element->setRequired(true);
//		$element->setLabel('Počet');
//		$element->setValue($product->getPartAmount($part->getID()));
//		$form->addElement($element);
//		$element = new Meduse_Form_Element_Submit('Odeslat');
//		$form->addElement($element);
//
//		if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
//			if($this->_request->getParam('multiedit')){
//				$this->_redirect('products/multi-edit3/id/'.$product->getID()."/type/edit-amount/part/".$part->getID()."/amount/".$form->meduse_part_amount->getValue());
//			} else {
//				$products_parts = new Table_ProductsParts();
//				$products_parts->update(array('amount' =>  $form->meduse_part_amount->getValue()), "id_products LIKE '".$product->getID()."' AND id_parts LIKE '".$part->getID()."'");
//				$this->_flashMessenger->addMessage("Počet součásti ".$this->_request->getParam('part')." upraven.");
//				$this->_redirect('products/detail/id/'.$product->getID());
//			}
//		}
//
//		$this->view->form = $form;
//
//		return $this->render('edit');
//
//	}
//
//	public function editWrapAmountAction(){
//		$this->_helper->layout->setLayout('layout2');
//
//	public function saveAsAction(){
//		$this->_helper->layout->setLayout('layout2');
//		if(!$this->_request->getParam('id')) $this->_redirect('products/overview'); //id produktu co se ma ukladat jako
//		$product = new Products_Product($this->_request->getParam('id'));
//		$form = new Products_SaveAsForm();
//		$this->view->form = $form;
//		if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
//
//			$product->saveAs(
//				$product->getID(),
//				$form->getValue('meduse_products_id'),
//				$form->getValue('meduse_products_name'),
//				$form->getValue('meduse_products_type'),
//				$form->getValue('meduse_products_collection'),
//				$form->getValue('meduse_products_desc')
//			);
//			$this->_flashMessenger->addMessage("Produkt ".$this->_request->getParam('id')." byl uložen jako produkt ".$form->getValue('meduse_products_id').".");
//
//			$this->_redirect('products/detail/id/'.$form->getValue('meduse_products_id'));
//		} elseif(!$this->_request->getParam('Odeslat')) {
//			$form->getElement("meduse_products_name")->setValue($product->getName());
//		}
//		return $this->render('add');
//	}
//
//	public function selectPartsAction(){
//		$parts = new Table_Parts();
//		$products = new Table_Products();
//		if($this->_request->getParam('product')){
//			$this->view->id = $this->_request->getParam('product');
//			$db = Zend_Registry::get('db');
//			$coll = $db->fetchOne('SELECT id_collections FROM products WHERE id = "'.$this->_request->getParam('product').'"');
//		} else {
//			$this->_redirect('products/overview');
//		}
//		if($this->_request->getParam('wrap')){
//			//pokud balim soucasti zobrazim jen soucasti daneho produktu
//			$this->view->id_wrap = $this->_request->getParam('wrap');
//			$this->view->data = $products->getParts($this->_request->getParam('product'));
//		} else {
//			$this->view->data = $parts->getSubparts($coll);
//		}
//
//		//Zend_Debug::dump($this->view->data); die();
//
//		if($this->_request->getParam('Odeslat')){
//			if($this->_request->getParam('id_wrap')){
//				//prirazuji se soucasti obalum produktu
//				$table = new Table_ProductsWrapsParts();
//				$id_row_name = "id_products_wraps";
//				$id_row_value = $this->_request->getParam('id_wrap');
//			} else {
//				//prirazuji se soucasti produktu
//				$table = new Table_ProductsParts();
//				$id_row_name = "id_products";
//				$id_row_value = $this->_request->getParam("id_product");
//			}
//
//			try{
//				foreach($this->_request->getParams() as $key => $value){
//					if($value == 'on'){
//						/* stara vec
//						if(substr($key,0,3) == 'set'){ //baleni setu soucasti
//							$db = Zend_Registry::get('db');
//							$db->insert('products_wraps_sets', array(
//								"id_products_wraps" => $id_row_value,
//								"id_products" => substr($key, 4),
//								"amount" => $this->_request->getParam($key."_amount"),
//							));
//						} else {
//						 *
//						 */
//						if($this->_request->getParam('wrap')){
//							//vkladaji se soucasti do obalu
//							$table->insert(array(
//								$id_row_name => $id_row_value,
//								"id_parts" => $key,
//								"amount" => $this->_request->getParam($key."_amount"),
//							));
//						} else {
//							//pridavaji se soucasti do produktu
//							//kotrola, jestli je uz polozka vlozena v databazi
//							if($db->fetchRow(
//									"SELECT * FROM products_parts WHERE id_parts = ".$db->quote($key).
//									" AND id_products = ".$db->quote($id_row_value)
//							)){
//								//kontrola, jestli uz je obsazena soucast v produktu v danem mnozstvi
//								if($db->fetchRow(
//										"SELECT * FROM products_parts WHERE id_parts = ".$db->quote($key).
//										" AND id_products = ".$db->quote($id_row_value).
//										" AND amount = ".$db->quote($this->_request->getParam($key."_amount"))
//								)){
//									continue; //nevkladat - zadna zmena
//								} else {
//									//update
//									if($this->_request->getParam($key."_amount") == 0){
//										$this->_flashMessenger->addMessage("Součást $key smazejte v detailu produktu ne tak, že jí nastavíte 0ks.");
//										continue;
//									}
//
//									$table->update(array(
//										"amount" => $this->_request->getParam($key."_amount"),
//									), "$id_row_name = ".$db->quote($id_row_value)." AND id_parts = ".$db->quote( $key ));
//								}
//							} else {
//								//vkladani
//								$table->insert(array(
//									$id_row_name => $id_row_value,
//									"id_parts" => $key,
//									"amount" => $this->_request->getParam($key."_amount"),
//								));
//							}
//						}
//
//					}
//				}
//				$this->_flashMessenger->addMessage("Součásti byly přiřazeny.");
//			} catch (Exception $e) {
//				$this->_flashMessenger->addMessage("Součásti se nepodařilo přiřadit.".$e->getMessage());
//			}
//
//			$products_all_parts = new Table_ProductsAllParts();
//			$products_all_parts->addProduct($this->_request->getParam("id_product"));
//
//			$this->_redirect('products/detail/id/'.$this->view->id);
//		}
//	}
//	/*
//	public function selectSetAction(){
//		$product = new Products_Product($this->_request->getParam("id"));
//		$products = new Table_Products();
//		$select = $products->select()
//		->where("id_products_types = ?", 2)
//		->where("id_collections = ?", $product->getCollection())
//		->where("id NOT IN (SELECT id_parts_set FROM products_parts_set WHERE id_products LIKE '".$product->getID()."')")
//		->order("id"); //vybere jen sety soucasti
//		$this->view->data = $products->fetchAll($select)->toArray();
//		$this->view->id = $this->_request->getParam("id");
//		if($this->_request->getParam('Odeslat')){
//			if(!$this->_request->getParam("id_product")) $this->_redirect('products/overview');
//			$db = Zend_Registry::get('db');
//			foreach($this->_request->getParams() as $key => $value){
//				if($value == 'on'){
//					$db->insert('products_parts_set', array(
//						"id_products" => $this->_request->getParam("id_product"),
//						"id_parts_set" => $key,
//						"amount" => $this->_request->getParam($key."_amount"),
//					));
//				}
//			}
//			$this->_redirect('products/detail/id/'.$this->_request->getParam("id_product"));
//			Zend_Debug::dump($this->_request->getParams());
//		}
//	}
//	*/
//	public function deleteProductAction(){
//		if(!$this->_request->getParam('id')) $this->_redirect('products/overview');
//		$product = new Products_Product($this->_request->getParam('id'));
//		$db = Zend_Registry::get('db');
//		try{
//			$db->beginTransaction();
//			$product->delete();
//			$db->commit();
//			$this->_flashMessenger->addMessage("Produkt ".$this->_request->getParam('id')." ostraněn.");
//		} catch ( Exception $e ) {
//			$db->rollback();
//			$this->_flashMessenger->addMessage("Produkt ".$this->_request->getParam('id')." se nepodařilo odstranit. <br />Zkontrolujte, zda-li není obsažen v nějaké z objednávek.");
//		}
//		$this->_redirect('products/overview/type/'.$this->_request->getParam('type'));
//		$this->_helper->viewRenderer->setNoRender();
//	}
//
//	public function deletePartAction(){
//		if(!$this->_request->getParam('id')) $this->_redirect('products/overview');
//		if(!$this->_request->getParam('part')) $this->_redirect('products/overview');
//		$product = new Products_Product($this->_request->getParam('id'));
//		try{
//			$product->deletePart($this->_request->getParam('part'));
//			$this->_flashMessenger->addMessage("Součást ".$this->_request->getParam('part')." ostraněna.");
//		} catch ( Exception $e ) {
//			$this->_flashMessenger->addMessage("Součást ".$this->_request->getParam('part')." se nepodařilo odstranit.<br />");
//		}
//		$this->_redirect('products/detail/id/'.$this->_request->getParam('id'));
//		$this->_helper->viewRenderer->setNoRender();
//	}
//
//	public function deleteSetAction(){
//		if(!$this->_request->getParam('id')) $this->_redirect('products/overview');
//		if(!$this->_request->getParam('set')) $this->_redirect('products/overview');
//		$product = new Products_Product($this->_request->getParam('id'));
//		try{
//			$product->deleteSubSet($this->_request->getParam('set'));
//			$this->_flashMessenger->addMessage("Set ".$this->_request->getParam('set')." ostraněn.");
//		} catch ( Exception $e ) {
//			$this->_flashMessenger->addMessage("Set ".$this->_request->getParam('set')." se nepodařilo odstranit.<br />");
//		}
//		$this->_redirect('products/detail/id/'.$this->_request->getParam('id'));
//		$this->_helper->viewRenderer->setNoRender();
//	}
//
//	public function deleteWrapAction(){
//		if(!$this->_request->getParam('id')) $this->_redirect('products/overview');
//		if(!$this->_request->getParam('wrap')) $this->_redirect('products/overview');
//		$product = new Products_Product($this->_request->getParam('id'));
//		try{
//			$product->deleteWrap($this->_request->getParam('wrap'));
//			$this->_flashMessenger->addMessage("Obal ostraněn.");
//		} catch ( Exception $e ) {
//			$this->_flashMessenger->addMessage("Obal se nepodařilo odstranit.<br />".$e->getMessage());
//		}
//		$this->_redirect('products/detail/id/'.$this->_request->getParam('id'));
//		$this->_helper->viewRenderer->setNoRender();
//	}
//
//	public function detailAction(){
//		if(!$this->_request->getParam('id')) $this->_redirect('products/overview');
//		$product = new Products_Product($this->_request->getParam('id'));
//		$categories = new Table_PartsCtg();
//		$this->view->categories = $categories->getCategories();
//		//$parts_warehouse = new Table_PartsWarehouse();
//		$this->view->product = $product;
//		//$this->view->wraps_parts = $wraps_parts->getWraps($this->_request->getParam('id'));
//	}
//
//	public function overviewAction(){
//		$products = new Table_Products();
//		$types = new Table_ProductsTypes();
//		$this->view->types = $types->getTypes();
//
//		//filtr
//		if($this->_request->getParam('type')){
//			if($this->_request->getParam('type') == "craft-sets"){
//				$this->view->data = $products->getDatalist(1);
//				$this->view->col = "Craft collection";
//				unset($this->view->data[2]);
//				unset($this->view->data[3]);
//				unset($this->view->data[4]);
//				unset($this->view->data['others']);
//			}
//			if($this->_request->getParam('type') == 'craft-acc'){
//				$this->view->data = $products->getDatalist(1);
//				$this->view->col = "Craft collection";
//				unset($this->view->data[1]);
//				unset($this->view->data[4]);//tiskoviny pryc - na zadost jury
//			}
//			if($this->_request->getParam('type') == "sepia-sets"){
//				$this->view->data = $products->getDatalist(2);
//				$this->view->col = "Sepia collection";
//				unset($this->view->data[2]);
//				unset($this->view->data[3]);
//				unset($this->view->data[4]);
//				unset($this->view->data['others']);
//			}
//			if($this->_request->getParam('type') == 'sepia-acc'){
//				$this->view->data = $products->getDatalist(2);
//				$this->view->col = "Sepia collection";
//				unset($this->view->data[1]);
//				unset($this->view->data[4]);//tiskoviny pryc - na zadost jury
//			}
//		}
//		$this->view->type = $this->_request->getParam('type');
//	}
//
//	public function packAction(){
//		if(!$this->_request->getParam('id')) $this->_redirect('products/overview');
//		$product = new Products_Product($this->_request->getParam('id'));
//		$categories = new Table_PartsCtg();
//		$this->view->categories = $categories->getCategories();
//
//		if($this->_request->getParam('Odeslat')){
//
//			$products_wraps = new Table_ProductsWraps();
//			$message = '';
//			foreach($this->_request->getParams() as $param_name => $param_value){
//				if(substr($param_name, -7) == '_amount' && $param_value > 0){
//
//					$param_name = explode('_', $param_name);
//					$param_name = $param_name[0];
//
//					$products_wraps->insert(array(
//						'id_wraps' => $param_name,
//						'id_products' => $this->_request->getParam('id'),
//						'wrap_amount' => $param_value
//					));
//
//					$message .= "Obal ".$param_name." přidán {$param_value}x.<br />";
//				}
//			}
//
//
//			$this->_flashMessenger->addMessage($message);
//			$this->_redirect('products/detail/id/'.$this->_request->getParam('id'));
//		} else {
//			//vyber obalu
//			$this->view->wraps = true;
//			$categories = new Table_WrapsCtg();
//
//			$wraps = new Table_Wraps();
//
//			$this->view->data = $wraps->getDatalist();
//			$this->view->categories = $categories->getCategories();
//
//		}
//		$this->view->product = $product;
//
//	}
//
//	public function multiEditAction(){
//		$products = new Table_Products();
//		$types = new Table_ProductsTypes();
//		$this->view->types = $types->getTypes();
//
//		//filtr
//		if($this->_request->getParam('type')){
//			if($this->_request->getParam('type') == "craft-sets"){
//				$this->view->data = $products->getDatalist(1);
//				$this->view->col = "Craft collection";
//				unset($this->view->data[2]);
//				unset($this->view->data[3]);
//			}
//			if($this->_request->getParam('type') == 'craft-acc'){
//				$this->view->data = $products->getDatalist(1);
//				$this->view->col = "Craft collection";
//				unset($this->view->data[1]);
//			}
//			if($this->_request->getParam('type') == "sepia-sets"){
//				$this->view->data = $products->getDatalist(2);
//				$this->view->col = "Sepia collection";
//				unset($this->view->data[2]);
//				unset($this->view->data[3]);
//			}
//			if($this->_request->getParam('type') == 'sepia-acc'){
//				$this->view->data = $products->getDatalist(2);
//				$this->view->col = "Sepia collection";
//				unset($this->view->data[1]);
//			}
//		}
//		$this->view->type = $this->_request->getParam('type');
//
//		$multi_edit = new Products_MultiEdit();
//		$multi_edit->setStep(1);
//		$multi_edit->saveToSession();
//	}
//
//	public function multiEdit2Action(){
//		$categories = new Table_PartsCtg();
//		$this->view->categories = $categories->getCategories();
//		$this->view->product = new Products_Product($this->_request->getParam('id'));
//
//		//multiedit
//		$multi_edit = new Products_MultiEdit();
//		$multi_edit->setStep(2);
//		$multi_edit->setProduct($this->view->product->getID());
//		$multi_edit->saveToSession();
//	}
//
//	public function multiEdit3Action(){
//		$db = Zend_Registry::get('db');
//
//		$multi_edit = new Products_MultiEdit();
//		$multi_edit->setStep(3);
//
//
//
//		switch($this->_request->getParam('type')){
//			case "delete-part":
//				$product = new Products_Product($multi_edit->getProduct());
//				$multi_edit->setAction(Products_MultiEdit::DELETE);
//				$part = new Parts_Part($this->_request->getParam('part'));
//				$multi_edit->setPartAmount($product->getPartAmount($part->getID()));
//				$multi_edit->setPart($part->getID());
//				//$db->fetchAll("SELECT id, name, IFNULL(description, '') AS description FROM products WHERE EXISTS(SELECT * FROM products_parts WHERE id_products = products.id AND id_parts = ".$db->quote($this->_request->getParam('part')).")");
//				break;
//			case "edit-amount":
//				$product = new Products_Product($multi_edit->getProduct());
//				$new_amount = (int) $this->_request->getParam('amount');
//
//				if( //validace formulare
//					!$this->_request->getParam('part') ||
//					!$this->_request->getParam('amount') ||
//					(int) $this->_request->getParam('amount') < 0
//				){
//					$this->_flashMessenger->addMessage("Špatně vyplněný formulář.");
//					$multi_edit->setStep(2);
//					$multi_edit->saveToSession();
//					$this->_redirect('products/multi-edit2/id/'.$this->_request->getParam('id'));
//				}
//
//				$part = new Parts_Part($this->_request->getParam('part'));
//
//				$multi_edit->setAction(Products_MultiEdit::EDIT);
//				$multi_edit->setPartAmount($product->getPartAmount($part->getID()));
//				$multi_edit->setNewPartAmount($new_amount);
//				$multi_edit->setPart($part->getID());
//				break;
//			case "add-part":
//
//				if( //validace formulare
//					!$this->_request->getParam('part') ||
//					!$this->_request->getParam('amount') ||
//					(int) $this->_request->getParam('amount') < 0
//				){
//					$this->_flashMessenger->addMessage("Špatně vyplněný formulář.");
//					$multi_edit->setStep(2);
//					$multi_edit->saveToSession();
//					$this->_redirect('products/multi-edit-add');
//					return;
//				}
//
//				$part = new Parts_Part($this->_request->getParam('part'));
//				$multi_edit->setAction(Products_MultiEdit::ADD);
//				$new_amount = (int) $this->_request->getParam('amount');
//				$multi_edit->setNewPartAmount($new_amount);
//				$multi_edit->setPart($part->getID());
//				break;
//
//			case "add-wrap":
//
//				if( //validace formulare
//					!$this->_request->getParam('wrap') ||
//					!$this->_request->getParam('amount') ||
//					(int) $this->_request->getParam('amount') < 0
//				){
//					$this->_flashMessenger->addMessage("Špatně vyplněný formulář.");
//					$multi_edit->setStep(2);
//					$multi_edit->saveToSession();
//					$this->_redirect('products/multi-edit-add-wrap');
//					return;
//				}
//				//$part = new Parts_Part($this->_request->getParam('part'));
//				$multi_edit->setAction(Products_MultiEdit::ADD_WRAP);
//				$new_amount = (int) $this->_request->getParam('amount');
//				$multi_edit->setNewPartAmount($new_amount);
//				$multi_edit->setWrap($this->_request->getParam('wrap'));
//				break;
//			case "delete-wrap":
//				$multi_edit->setAction(Products_MultiEdit::DELETE_WRAP);
//				$multi_edit->setWrap($this->_request->getParam('wrap'));
//				break;
//			default:
//				break;
//		}
//
//		$multi_edit->saveToSession();
//		$this->view->products = $multi_edit->getPossibleChangedProducts();
//	}
//
//	public function multiEdit4Action(){
//		$multi_edit = new Products_MultiEdit();
//		$multi_edit->setStep(4);
//		$db = Zend_Registry::get('db');
//
//		if($this->_request->getParams() && $this->_request->isPost() && !$this->_request->getParam('confirm')){
//			$products = array();
//			foreach($this->_request->getParams() as $name => $value){
//				if(substr($name, 0, 7) == 'product'){
//					$products[]=$value;
//				}
//			}
//			$multi_edit->setProducts($products);
//			$multi_edit->saveToSession();
//		} elseif ($this->_request->isPost() && $this->_request->getParam('confirm')) {
//			//uklada se multizmena
//			switch($multi_edit->getAction()){
//				case Products_MultiEdit::ADD:
//					try{
//						$db->beginTransaction();
//						foreach($multi_edit->getSelectedProducts() as $product_id){
//							$db->insert("products_parts", array(
//								'id_products'	=> $product_id,
//								'id_parts'		=> $multi_edit->getPart(),
//								'amount'		=> $multi_edit->getNewPartAmount()
//							));
//						}
//						$db->commit();
//					} catch ( Exception $e ) {
//						$db->rollback();
//						$this->_flashMessenger->addMessage("Uprava se nezdařila.");
//						$this->_redirect('products/multi-edit');
//						return;
//					}
//					break;
//				case Products_MultiEdit::ADD_WRAP:
//					try{
//						$db->beginTransaction();
//						foreach($multi_edit->getSelectedProducts() as $product_id){
//							$db->insert("products_wraps", array(
//								'id_products'	=> $product_id,
//								'id_wraps'		=> $multi_edit->getWrap(),
//								'wrap_amount'		=> $multi_edit->getNewPartAmount()
//							));
//						}
//						$db->commit();
//					} catch ( Exception $e ) {
//						$db->rollback();
//						$this->_flashMessenger->addMessage("Uprava se nezdařila.");
//						throw $e; die();
//						$this->_redirect('products/multi-edit');
//						return;
//					}
//					break;
//				case Products_MultiEdit::EDIT:
//					try{
//						$db->beginTransaction();
//						foreach($multi_edit->getSelectedProducts() as $product_id){
//							$db->update("products_parts", array(
//								"amount" => $multi_edit->getNewPartAmount()
//								), "id_products = '".$product_id."' AND id_parts = '".$multi_edit->getPart()."' AND amount = ".$multi_edit->getPartAmount()
//							);
//						}
//						$db->commit();
//					} catch ( Exception $e ) {
//						$db->rollback();
//						$this->_flashMessenger->addMessage("Uprava se nezdařila.");
//						$this->_redirect('products/multi-edit');
//						return;
//					}
//					break;
//				case Products_MultiEdit::DELETE:
//					try{
//						$db->beginTransaction();
//						foreach($multi_edit->getSelectedProducts() as $product_id){
//							$db->delete(
//								"products_parts",
//								"id_products = '".$product_id."' AND id_parts = '".$multi_edit->getPart()."'" // AND amount = ".$multi_edit->getPartAmount()
//							);
//						}
//						$db->commit();
//					} catch ( Exception $e ) {
//						$db->rollback();
//						$this->_flashMessenger->addMessage("Uprava se nezdařila.");
//						$this->_redirect('products/multi-edit');
//						return;
//					}
//					break;
//				case Products_MultiEdit::DELETE_WRAP:
//					try{
//						$db->beginTransaction();
//						foreach($multi_edit->getSelectedProducts() as $product_id){
//							$db->delete(
//								"products_wraps",
//								"id_products = '".$product_id."' AND id_wraps = '".$multi_edit->getWrap()."'" // AND amount = ".$multi_edit->getPartAmount()
//							);
//						}
//						$db->commit();
//					} catch ( Exception $e ) {
//						throw $e;
//						$db->rollback();
//						$this->_flashMessenger->addMessage("Uprava se nezdařila.");
//						$this->_redirect('products/multi-edit');
//						return;
//					}
//					break;
//			}
//			$this->_flashMessenger->addMessage("Uprava ".count($multi_edit->getSelectedProducts())." produktu provedena.");
//			$this->_redirect('products/multi-edit');
//
//		}
//
//		$this->view->selected_products = $multi_edit->getSelectedProducts();
//
//	}
//
//	public function multiEditAddAction(){
//
//	}
//
//	public function multiEditAddWrapAction(){
//
//	}
//
//}
