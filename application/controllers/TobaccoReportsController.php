<?php

/**
 * Description of TobaccoReportsController
 *
 * @author Vojtěch Mrkývka
 */
class TobaccoReportsController extends Meduse_Controller_Action {

  const DOWNLOAD_PATH = '/export/';
  const TEMP_PATH = '/export/';

  public function init() {
    $this->db = Zend_Registry::get('db');
    $this->_helper->layout->setLayout('bootstrap-tobacco');
  }

  public function amountAction() {
    $tWarehouse = new Table_TobaccoWarehouse();
    $this->view->title = 'Reporty: Množství';

    $this->view->materials = $tWarehouse
            ->getFiltredTobaccoWarehouseStatus($tWarehouse::CTG_MATERIALS)
            ->query()->fetchAll();
    $this->view->materials_sum = $tWarehouse
            ->getFiltredWarehouseStatusAmountSum($tWarehouse::CTG_MATERIALS)
            ->query()->fetchAll();

    $tMacaretions = new Table_Macerations();
    $this->view->totalWeight = $tMacaretions->getTotalWeight();
    $this->view->barrelsCount = $tMacaretions->getBarrelsCount();
    if ($recipes = $tMacaretions->getBarrelsByRecipes()) {
      $pattern_batch = '/(L\d{6}\w\d{2})/i';
      $replacement_batch = '<a href="' . $this->view->url(['controller' => 'tobacco', 'action' => 'macerations-detail']) .  '/batch/$1#changes">$1</a>';
      foreach ($recipes as &$recipe) {
        $recipe['batches'] =  preg_replace($pattern_batch, $replacement_batch, $recipe['batches']);
      }
    }
    $this->view->recipes = $recipes;
    $this->view->barrels = $tMacaretions->getBarrels();

    $tWarehouse = new Table_TobaccoWarehouse();
    $this->view->overviewNFS = $tWarehouse->getProductsOverviewNFS();
    $this->view->overviewFS = $tWarehouse->getProductsOverviewFS();
    $this->view->recipesNFS = $tWarehouse->getProductsByMixNFS();
    $this->view->recipesFS = $tWarehouse->getProductsByMixFS();
    $this->view->packsNFS = $tWarehouse->getProductsByPackNFS();
    $this->view->packsFS = $tWarehouse->getProductsByPackFS();
    $this->view->productsNFS = $tWarehouse->getProductsReportNFS();
    $this->view->productsFS = $tWarehouse->getProductsReportFS();

    $this->view->print = $this->_request->getParam('print');
  }

  public function wasteAction() {
    #$tMacerations = new Table_Macerations();
    $tOrdersProducts = new Table_OrdersProducts();
    #$tTobaccoWarehouse = new Table_TobaccoWarehouse();
    $tRecipes = new Table_Recipes();
    $tParts = new Table_Parts();

    //Reporty přes šarže
    #$this->view->barrels = $tMacerations->getBarrels();
    #$this->view->productsNFS = $tTobaccoWarehouse->getProductReportAmountNFS();
    #$this->view->productsFS = $tTobaccoWarehouse->getProductReportAmountFS();
    #$this->view->orders = $tOrdersProducts->getAmountReportBatchesNonExpd();
    #$this->view->invoices = $tOrdersProducts->getAmountReportBatchesExpd();

    //Reporty přes příchuti
    $this->view->flavour_barrels = $tRecipes->getMacerationsWasteReport()->query()->fetchAll();
    $this->view->flavour_productsNFS = $tParts->getProductsWasteReport(Table_TobaccoWarehouse::CTG_TOBACCO_NFS);
    $this->view->flavour_productsFS = $tParts->getProductsWasteReport(Table_TobaccoWarehouse::CTG_TOBACCO_FS);
    $this->view->flavour_ordersOPEN = $tOrdersProducts->getAmountReportFlavours(Table_Orders::STATUS_OPEN);
    $this->view->flavour_ordersCLOSED = $tOrdersProducts->getAmountReportFlavours(Table_Orders::STATUS_CLOSED);
  }

  public function salesAction() {
    $this->view->post = false;
    $this->view->title = 'Reporty: Prodeje';
    $form = new Tobacco_Reports_SalesForm();
    $form->populate($this->_request->getParams());
    $this->view->form = $form;
    if ($this->_request->isPost()) {
      $top = new Table_OrdersProducts();
      $tos = new Table_OrdersServices();

      $options = array(
        'wh' => Table_TobaccoWarehouse::WH_TYPE,
        'from' => $this->_request->getParam('from'),
        'to' => $this->_request->getParam('to'),
        'country' => $this->_request->getParam('country'),
        'customer' => $this->_request->getParam('customer'),
        'flavor' => $this->_request->getParam('flavor'),
        'pkg' => $this->_request->getParam('pkg'),
        'type' => $this->_request->getParam('type'),
        'order' => $this->_request->getParam('order'),
        'onlypaid' => $this->_request->getParam('onlypaid'),
      );
      $this->view->tobacco = $top->getTotalTobaccoSales($options);
      $this->view->coal = $top->getTotalCoalSales($options);
      $this->view->services = $tos->getTotalSales($options);
      $this->view->showPrices = Zend_Registry::get('acl')->isAllowed('tobacco-reports/show-prices');
      $this->view->lastName = '';
      $this->view->post = true;
    }
  }

  public function ekokomAction() {
    $this->view->post = false;
    $this->view->title = 'Reporty: Ekokom';
    $form = new Tobacco_Reports_EkokomForm();
    $form->populate($this->_request->getParams());
    $this->view->form = $form;
    if ($this->_request->isPost()) {
      $top = new Table_OrdersProducts();
      $options = array(
        'from' => $this->_request->getParam('from'),
        'to' => $this->_request->getParam('to'),
      );
      $products = $top->getEkokomSales($options);
      $data = array();
      if ($products) {
        foreach ($products as $product) {
          $part = new Parts_Part($product['id']);
          $partData = array();
          $part->getEkokomData($partData);
          if ($partData) {
            foreach($partData as $ctg => $pd) {
              $data[$ctg] = isset($data[$ctg]) ?
                array('weight' => $data[$ctg]['weight'] + $pd['weight'] * $product['amount'], 'name' => $pd['name']) :
                array('weight' => $pd['weight'] * $product['amount'], 'name' => $pd['name']);
            }
          }
        }
      }
      $this->view->data = $data;
      $this->view->post = true;
    }
  }

  public function historyAction() {
    $this->view->title = 'Reporty: Historie';
    $grid = new Tobacco_Reports_HistoryGrid();
    $this->view->grid = $grid->generateGrid($this->getRequest()->getParams());
  }

  public function dutyAction() {

    $this->view->title = 'Reporting pro Celní správu';
    $this->view->form = new Tobacco_Reports_DutyForm();

    $to = $this->_request->getParam('to');
    $toDate = empty($to)? new Meduse_Date() : new Meduse_Date($to, 'd.m.Y');

    $from = $this->_request->getParam('from');
    if (empty($from)) {
      $fromDate = clone $toDate;
      $fromDate->subMonth(3);
    }
    else {
      $fromDate = new Meduse_Date($from, 'd.m.Y');
    }

    $type = $this->_request->getParam('type', Tobacco_Reports_Duty::TYPE_STAMPS);
    $export = $this->_request->getParam('export', 'n');
    $output = $this->_request->getParam('output', Tobacco_Reports_Duty::OUTPUT_SCREEN);

    $this->view->form->setFrom($fromDate);
    $this->view->form->setTo($toDate);
    $this->view->form->setType($type);
    $this->view->form->setExport($export == 'y');
    $this->view->form->setOutput($output);

    if ($this->_request->isPost()) {

      if ($type == Tobacco_Reports_Duty::TYPE_PRODUCTS) {

        $this->view->ajax = TRUE;
        $this->view->type = Tobacco_Reports_Duty::TYPE_PRODUCTS;
        $products = Tobacco_Reports_Duty::getProductsList();
        $list = array();
        foreach ($products as $row) {
          $list[] = $row->id;
        };
        $this->view->products = '["' . implode('", "', $list) . '"]';
        $this->view->from = $fromDate;
        $this->view->to = $toDate;
        $this->view->hash = base64_encode('products_' . $fromDate->get('Y-m-d') . '_' . $toDate->get('Y-m-d') . '_' . date('Ymd_His'));
        return;
      }

      $report = new Tobacco_Reports_Duty();
      $report->setFrom($fromDate);
      $report->setTo($toDate);
      $report->setType($type);
      $report->setExport($export == 'y');
      $report->setOutput($output);
      $report->generate();

      if ($output == $report::OUTPUT_SCREEN) {
        $this->view->table = $report->render();
      }
      elseif ($output == $report::OUTPUT_FILE) {

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        $objPHPExcel = $report->render();
        $name = 'duty-' . $type . '.xlsx';
        $path = $type == Tobacco_Reports_Duty::TYPE_PRODUCTS ?
          Zend_Registry::get('config')->aclCache->cacheDir . self::TEMP_PATH
          : Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH;
        if (!file_exists($path)) {
          mkdir($path);
        }
        $filename = $path . $name;
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(TRUE);
        $objWriter->save($filename);
        $fp = fopen($filename, 'rb');
        $this->_response->clearAllHeaders();
        $this->_response->setHeader('Content-Description', 'File Transfer');
        $this->_response->setHeader('Expires','0',true);
        $this->_response->setHeader('Pragma','public',true);
        $this->_response->setHeader('Cache-Control','must-revalidate, post-check=0,pre-check=0', true);
        $tokens = explode('.', $filename);
        switch (array_pop($tokens)) {
          case 'xls':  $type = 'application/vnd.ms-excel'; break;
          case 'xlsx': $type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'; break;
          default: $type = '';
        }
        if ($type) {
          $this->_response->setHeader('Content-Type', $type);
        }
        $this->_response->setHeader('Content-Length',  filesize($filename));
        $this->_response->setHeader('Content-Disposition', 'attachment; filename="' . $name, true);
        fpassthru($fp);
        fclose($fp);
        unlink($filename);
      }
    }
  }

  public function dutyDownloadAction() {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout->disableLayout();

    $hash = $this->_request->getParam('hash');
    $filePath = Tobacco_Reports_Duty::getZipFile($hash);
    $parts = explode('/', $filePath);
    $fileName = end($parts);

    $fp = fopen($filePath, 'rb');
    $this->_response->clearAllHeaders();
    $this->_response->setHeader('Content-Description', 'File Transfer');
    $this->_response->setHeader('Expires','0',TRUE);
    $this->_response->setHeader('Pragma','public',TRUE);
    $this->_response->setHeader('Cache-Control','must-revalidate, post-check=0,pre-check=0', TRUE);
    $this->_response->setHeader('Content-Type', 'application/octet-stream');
    $this->_response->setHeader('Content-Length',  filesize($filePath));
    $this->_response->setHeader('Content-Disposition', 'attachment; filename="' . $fileName, TRUE);

    fpassthru($fp);
    fclose($fp);
    unlink($filePath);
  }

  public function dutyAjaxProductsAction() {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout->disableLayout();
    $id = $this->_request->getParam('id');
    $hash = $this->_request->getParam('hash');
    $from = new Meduse_Date($this->_request->getParam('from'));
    $to = new Meduse_Date($this->_request->getParam('to'));

    $report = new Tobacco_Reports_Duty();
    $report->setFrom($from);
    $report->setTo($to);
    $report->setType(Tobacco_Reports_Duty::TYPE_PRODUCTS);
    $report->setOutput(Tobacco_Reports_Duty::OUTPUT_FILE);
    $report->setHash($hash);
    $report->setProducts(array($id));
    $report->generate();
    $filename = $report->render();

    echo json_encode(array($filename));
  }
}
