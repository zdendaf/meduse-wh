<?php
class ProducersController extends Meduse_Controller_Action{

  public function init() {
    $this->_helper->layout->setLayout('bootstrap-basic');
  }

	//prehled
	public function indexAction(){
		$table = new Table_Producers();

		$this->view->filter = new Producers_SearchForm;
		$this->view->filter->isValid($this->getRequest()->getParams());

		$this->_helper->layout->setLayout('bootstrap-basic');
		$this->view->title = "Dodavatelé";

		if($this->getRequest()->getParam('Hledat')) {
			$select = $table->getDataList(true);
      $select->where('id_wh_type = ?', Parts_Part::WH_TYPE_PIPES);
			if($this->getRequest()->getParam('name',false)){
				$select->where("p.name LIKE ?","%".$this->getRequest()->getParam('name')."%");
			}
			if($this->getRequest()->getParam('category',false)){
				$select->where("p.id_producers_ctg = ?",$this->getRequest()->getParam('category'));
			}
			$this->view->data = $table->fetchAll($select);
		}



	}

	public function addAction(){

		$this->_helper->layout->setLayout('bootstrap-basic');
		$this->view->title = "Přidat dodavatele";

		$form = new Producers_Form();
    $form->setLegend($this->view->title);
		$person = new Producers_PersonForm();

    $form->removeElement('meduse_producer_is_active');
		$form->addElement($person->getElement('meduse_producer_deputy'));
		$form->addElement($person->getElement('meduse_producer_phone'));
		$form->addElement($person->getElement('meduse_producer_phone2'));
		$form->addElement($person->getElement('meduse_producer_email'));
		$form->addElement($person->getElement('meduse_producer_position_select'));
		$form->getElement('Odeslat')->setOrder(99);

		if ($this->_request->getParam('Odeslat') && $this->_request->isPost() && $form->isValid($this->_request->getParams())) {

      $producer = new Producers_Producer();
      $extWh = $this->_request->getParam('meduse_producer_warehouse', FALSE);
      $producerData = array(
				'id_extern_warehouses' => empty($extWh) ? NULL : $extWh,
				'name' => $this->_request->getParam('meduse_producer_name'),
				'address' => $this->_request->getParam('meduse_producer_address'),
				'www' => $this->_request->getParam('meduse_producer_www'),
				'activity' => $this->_request->getParam('meduse_producer_activity'),
				'description' => $this->_request->getParam('meduse_producer_desc'),
				'id_producers_ctg' => $this->_request->getParam('meduse_producer_category'),
      );
      $producer->create($producerData);

			//vlozeni kontaktni osoby
      $personData = array(
				'name' => $this->_request->getParam('meduse_producer_deputy'),
				'phone'	=> $this->_request->getParam('meduse_producer_phone'),
				'phone2'=> $this->_request->getParam('meduse_producer_phone2'),
				'email'	=> $this->_request->getParam('meduse_producer_email'),
				'position' => $this->_request->getParam('meduse_producer_position'),
			);
      $producer->addPerson($personData);

      $act = new Table_Actualities();
			$act->add("Vložil nového dodavatele '" . $this->_request->getParam("meduse_producer_name") . "'.");

			$this->_flashMessenger->addMessage('Dodavatel uložen.');
			$this->_redirect('producers');
		}

		$this->view->form = $form;
	}

  public function addPersonAction() {

    $this->_helper->layout->setLayout('bootstrap-basic');

    if (!$this->_request->getParam('id')) {
      return $this->redirect('producers');
    }

    $id_producers = (int) $this->_request->getParam('id');

    $table = new Table_Producers();
    $isCarrier = $table->isCarrier($id_producers);
    $this->view->title = ($isCarrier) ? "Přidat kontakt - dopravce" : "Přidat kontakt - výrobce";

    $form = new Producers_PersonForm();
    $form->setLegend($this->view->title);

    // is carrier remove fields
    if ($isCarrier) {
      $form->removeElement('meduse_producer_email');
      $form->removeElement('meduse_producer_position');
      $form->removeElement('meduse_producer_position_select');
    }

    //vlozeni kontaktni osoby
    if ($this->_request->getParam('Odeslat') && $this->_request->isPost()) {
      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        $data = $form->getValidValues($params);
        $table2 = new Table_ProducersPersons();
        $table2->add([
          'id_producers' => $id_producers,
          'name' => $data['meduse_producer_deputy'],
          'phone' => $data['meduse_producer_phone'],
          'phone2' => $data['meduse_producer_phone2'],
          'email' => $data['meduse_producer_email'],
          'position_old' => $data['meduse_producer_position'],
          'position' => $data['meduse_producer_position_select'],
        ]);
        $act = new Table_Actualities();
        if ($isCarrier) {
          $act->add("Přidal dopravci nový kontakt '" . $this->_request->getParam("meduse_producer_deputy") . "'.");
          $this->_flashMessenger->addMessage("Dopravce uložen.");
          return $this->redirect('producers/detail-carrier/id/' . $id_producers);
        }
        else {
          $act->add("Přidal dodavateli nový kontakt '" . $this->_request->getParam("meduse_producer_deputy") . "'.");
          $this->_flashMessenger->addMessage("Dodavatel uložen.");
          return $this->redirect('producers/detail/id/' . $id_producers);
        }
      }
    }
    $this->view->form = $form;
    $this->render('add');
  }

  public function removePersonAction() {

    $this->_helper->layout->disableLayout();

    $producer_id = $this->_request->getParam('id');
    $person_id = $this->_request->getParam('person');

    if ($person_id) {
      $table = new Table_ProducersPersons();
      if ($table->delete('id = ' . $table->getAdapter()->quote($person_id))) {
        $this->_flashMessenger->addMessage('Kontakt byl odstraněn.');
      } else {
        $this->_flashMessenger->addMessage('Kontakt nebyl odstraněn.');
      }
    } else {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID kontaktu.');
    }

    if ($producer_id) {
      $this->_redirect('producers/detail/id/' . $producer_id);
    } else {
      $this->_redirect('producers');
    }

  }


  public function addMultipleProductionsAction() {

    $producer = $this->getRequest()->getParam('producer');
    $producer = empty($producer) ? new Zend_Db_Expr('NULL') : $producer;

    $form = new Parts_ProductionsForm();
    $form->removeElement('meduse_production_amount');
    $form->getElement('meduse_production_ordering')->setRequired();
    $form->getElement('meduse_production_delivery')->setRequired();
    if (!$this->_request->getParam('production_merge', FALSE) && !$form->isValid($this->_request->getParams())) {
      $this->_flashMessenger->addMessage('Nebyly zadány požadované datumy nebo mají špatný formát. Povolený formát je DD.MM.RRRR');
      $this->redirect($_SERVER['HTTP_REFERER']);
      die;
    };

    $this->_db->beginTransaction();
    try {
      $productions = new Table_Productions;

      // Slouceni vyroby.
      if ($this->_request->getParam('production_merge')) {
        $rProduction = $productions->find($this->_request->getParam('production_id'))
          ->current();
        if (!$rProduction) {
          $this->_flashMessenger->addMessage("Při slučování výroby musí být označena alespoň jedna výroba.");
          $this->redirect($this->view->url([
            'controller' => 'parts',
            'action' => 'productions',
          ], NULL, TRUE));
        }
        $idProduction = $rProduction->id;
      }

      // Nova vyroba
      else {
        $authNamespace = new Zend_Session_Namespace('Zend_Auth');
        $userId = $authNamespace->user_id;
        $date = new Zend_Date($this->_request->getParam('meduse_production_ordering'), "d.MM.yyyy");
        if ($this->_request->getParam('meduse_production_delivery', FALSE)) {
          $date_delivery = new Zend_Date($this->_request->getParam('meduse_production_delivery'), "d.MM.yyyy");
          $date_delivery = $date_delivery->toString("yyyy-MM-dd");
        }
        else {
          $date_delivery = new Zend_Db_Expr('NULL');
        }
        $idProduction = $productions->insert([
          'id_producers' => $producer,
          'id_users' => $userId,
          'date_ordering' => $date->toString("yyyy-MM-dd"),
          'date_delivery' => $date_delivery,
          'status' => 'open',
          'inserted' => new Zend_Db_Expr("NOW()"),
        ]);
      }

      foreach ($this->_request->getParams() as $name => $value) {
        if (preg_match("/^part_/", $name)) { //soucast musi zacinat "part_"
          $idPart = substr($name, 5, strlen($name)); //odstraneni "part_"
          $amount = (int) trim($value);

          //TODO: prevadet podsoucasti do externiho skladu

          $select = $this->_db->select()
            ->from('productions_parts', [
              'id',
              'amount',
              'amount_remain',
            ])
            ->where('id_productions = ?', $idProduction)
            ->where('id_parts = ?', $idPart);
          $row = $select->query(Zend_Db::FETCH_ASSOC)->fetch();
          if ($row['id']) {
            $this->_db->update('productions_parts', [
              'amount' => $amount + $row['amount'],
              'amount_remain' => $amount + $row['amount_remain'],
              'inserted' => new Zend_Db_Expr("NOW()"),
            ], 'id = ' . $row['id']);
          }
          else {
            $this->_db->insert('productions_parts', [
              'id_productions' => $idProduction,
              'id_parts' => $idPart,
              'amount' => $amount,
              'amount_remain' => $amount,
              'inserted' => new Zend_Db_Expr("NOW()"),
            ]);
          }
        }
        if (preg_match("/^operation_/", $name)) { //operace musi zacinat "operation_"
          $idOperation = substr($name, 10, strlen($name)); //odstraneni "part_"
          $amount = (int) trim($value);

          $operationRow = $this->_db->fetchRow("SELECT * FROM parts_operations WHERE id = :id", ['id' => $idOperation]);

          if ($operationRow) {

            //prevadeni soucasti do externiho skladu, pokud se nejedna o meduse
            //v pripade meduse soucasti rezervovat?


            $pwe = new Table_PartsWarehouseExtern();

            $extWHid = $pwe->getWarehouseByProducer($operationRow['id_producers']);
            if (empty($extWHid)) {
              throw new Exception('Table_PartsWarehouseExtern->getWarehouseByProducer vraci prazdny vysledek');
            }

            $tWarehouse = new Table_PartsWarehouse();
            if ($amount <= $tWarehouse->getBaseAmount($operationRow['id_parts'])) {
              $pwe->insertPart($extWHid, //sklad
                $operationRow['id_parts'], //soucast
                $amount //pocet
              );
            }
            else {
              $missing = $amount - $tWarehouse->getBaseAmount($operationRow['id_parts']);
              $this->_db->rollBack();
              $this->_flashMessenger->addMessage("Není dostatek součásti " . $operationRow['id_parts'] . " na centrálním skladu. Chybí $missing ks.");
              $this->redirect($_SERVER['HTTP_REFERER']);
              return;
            }

            $this->_flashMessenger->addMessage("$amount ks " . $operationRow['id_parts'] . " bylo přeneseno do externího skladu '" . $pwe->getWhName($extWHid) . "'.");

            $exists = $this->_db->select()
              ->from('w_productions_current')
              ->where('id_productions = ?', $idProduction)
              ->where('id_parts = ?', $operationRow['id_parts'])
              ->query()
              ->fetch();

            if ($this->_request->getParam('production_merge', FALSE) && $exists) {
              $this->_db->update('productions_parts', [
                'amount' => new Zend_Db_Expr('amount + ' . $this->_db->quote($amount)),
                'amount_remain' => new Zend_Db_Expr('amount_remain + ' . $this->_db->quote($amount)),
                'changed' => new Zend_Db_Expr("NOW()"),
              ], 'id_productions = ' . $this->_db->quote($idProduction) . 'AND id_parts = ' . $this->_db->quote($operationRow['id_parts']));
            }
            else {
              $this->_db->insert('productions_parts', [
                'id_productions' => $idProduction,
                'id_parts' => $operationRow['id_parts'],
                'id_parts_operations' => $idOperation,
                'amount' => $amount,
                'amount_remain' => $amount,
                'inserted' => new Zend_Db_Expr("NOW()"),
              ]);
            }
          }

        }
      }

      $tEmails = new Table_Emails();
      try {
        $tEmails->sendEmail(Table_Emails::EMAIL_PRODUCTION_CREATION, ['productionId' => $idProduction]);
      }
      catch (Zend_Mail_Transport_Exception $e) {
        $this->_flashMessenger->addMessage('Pozor: Nepodařilo se odeslat mail.');
      }

      $this->_db->commit();
    }
    catch (Exception $e) {
      $this->_db->rollBack();
      throw $e;
    }

    $this->_flashMessenger->addMessage("Výroba uložena.");
    $this->redirect($this->view->url([
      'controller' => 'producers',
      'action' => 'show-production',
      'id' => $idProduction,
    ], NULL, TRUE));

  }

  public function showProductionAction() {
    // set layout
    $this->_helper->layout->setLayout('bootstrap-basic');

    /**
     * Set title
     */
    $this->view->title = "Výroba ";

    // table producers
    $TProducers = new Table_Producers();
    $this->view->TProducers = $TProducers;

    // table productions
    $productions = new Table_Productions;
		$this->view->details = $productions->getDetails($this->_request->getParam('id'));
		$this->view->title .= $this->view->details['name'];
		$this->view->parts = $productions->getParts($this->_request->getParam('id'));

		//dodaci listy
    $deliveries = $productions->getDeliveries($this->_request->getParam('id'));
		$this->view->deliveries = $deliveries;
		$this->view->deliveries_count = count($deliveries);

		try {
      $this->view->special_price_allowed = Zend_Registry::get('acl')->isAllowed('producers/special/price');
    }
    catch (Zend_Exception $e) {
      $this->view->special_price_allowed = FALSE;
    }


		if($this->getRequest()->getParam('export')) {
			$TProducers = new Table_Producers();
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$text_delimiter = '"';
			$field_delimiter = ';';
			$line_delimiter = "\n";
			$export_string = $text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.'EXPORT VÝROBY '.$this->view->details['name'].' K DATU '.date('d.m.Y').$text_delimiter.$field_delimiter.$line_delimiter.$line_delimiter;

			$export_string .=
				 $text_delimiter."ID".$text_delimiter.$field_delimiter.
				 $text_delimiter."Název".$text_delimiter.$field_delimiter.
				 $text_delimiter."ID dodavatele".$text_delimiter.$field_delimiter.
				 $text_delimiter."Název dodavatele".$text_delimiter.$field_delimiter.
				 $text_delimiter."Počet".$text_delimiter.$field_delimiter.
				 $line_delimiter;

			foreach($this->view->parts as $part){
				$export_string .=
					 $text_delimiter.$part['id_parts'].$text_delimiter.$field_delimiter.
					 $text_delimiter.$part['name'].$text_delimiter.$field_delimiter.
					 $text_delimiter.$TProducers->getPartId($this->view->details['id_producers'], $part['id_parts']).$text_delimiter.$field_delimiter.
					 $text_delimiter.$TProducers->getPartName($this->view->details['id_producers'], $part['id_parts']).$text_delimiter.$field_delimiter.
					 $text_delimiter.$part['amount_remain'].$text_delimiter.$field_delimiter.
					 $line_delimiter;
			}

			$this->_response->clearAllHeaders();
			$this->_response->setHeader('Content-Description', 'File Transfer');
			$this->_response->setHeader('Expires','0',true);
			$this->_response->setHeader('Pragma','public',true);
			$this->_response->setHeader('Cache-Control','must-revalidate, post-check=0,pre-check=0',true);
			$this->_response->setHeader('Content-Type', 'text/csv; charset=windows-1250');
			$this->_response->setHeader('Content-Disposition', 'attachment; filename="vyroba.csv"',true);
			$this->_response->setBody($export_string);

		}

	}

	public function editProductionsHeaderAction(){
        $this->_helper->layout->setLayout('bootstrap-basic');
		$this->view->title = "Úprava hlavičky výroby";
		if(!$this->_request->getParam('production')) $this->_redirect('parts/overview');
		$form = new Parts_ProductionsHeaderForm();
        $form->setLegend($this->view->title);

		if($this->_request->isPost() && $form->isValid($this->_request->getParams())){
			$rProduction = $this->_db->fetchRow("SELECT * FROM productions WHERE id = :id", array('id' => $this->_request->getParam('production')));

			$data = array();
			if($form->getElement('meduse_production_date')->getValue()){
				$tmp = new Zend_Date($form->getElement('meduse_production_date')->getValue(), 'dd.MM.yyyy');
				$data['date_ordering'] = $tmp->toString('yyyy-MM-dd');
			}
			if($form->getElement('meduse_production_date')->getValue()){
				$tmp = new Zend_Date($form->getElement('meduse_production_deadline')->getValue(), 'dd.MM.yyyy');
				$data['date_delivery'] = $tmp->toString('yyyy-MM-dd');
			}

			$this->_db->update('productions',$data, "id = ".$this->_db->quote((int)$this->_request->getParam('production')));
			$this->_flashMessenger->addMessage("Výroba uložena.");
			$this->_redirect($this->view->url(array('action' => 'show-production', 'id'=>$this->_request->getParam('production'), 'production'=>null)));
		} else if (!$this->_request->isPost()) {
			$rProduction = $this->_db->fetchRow("SELECT * FROM productions WHERE id = :id", array('id' => (int) $this->_request->getParam('production')));
			if($rProduction){
				$form->meduse_production_date->setValue($this->view->DateFromDb($rProduction['date_ordering']));
				$form->meduse_production_deadline->setValue($this->view->DateFromDb($rProduction['date_delivery'])); //TODO: Smazat
			} else {
				if(!$this->_request->getParam('production')) $this->_redirect('parts/overview');
			}
		}
		$this->view->form = $form;
		return $this->render('add');
	}

  public function addDeliveryAction() {
    // set layout
    $this->_helper->layout->setLayout('bootstrap-basic');

    /**
     * Set title
     */
    $this->view->title = "Nový dodací list ";

    /**
     * Productions table
     */
    $productions = new Table_Productions;

    /**
     * Load form
     */
    $form = new Producers_DeliveryProductionAddForm();

    // remove decorators and buttons because form is a part of multiform
    $form->removeElement('submit');
    $form->removeDecorator('Form');
    $form->removeDecorator('Fieldset');
    $form->removeDecorator('HtmlTag');
    $form->removeDecorator('Dashed');
    $form->removeDecorator('Obal');

    /**
     * Send
     */
    if ($this->getRequest()
        ->isPost() && $form->isValid($this->_request->getParams())) {

      /**
       * get params send
       */
      $params = $this->getRequest()->getParams();

      /**
       * check correct parts amount
       */
      $parts_arr = $params['parts'];
      $parts_max_arr = $params['parts_max'];

      /* Uzivatelovo ID */
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $userId = $authNamespace->user_id;

      $this->_db->beginTransaction();
      try {
        $date = new Zend_Date($form->date_delivery->getValue(), "dd.MM.yyyy");
        $deliveries = new Table_ProductionsDeliveries;
        $deliveryID = $deliveries->insert([
          'id_productions' => $this->_request->getParam('production'),
          'id_users' => $userId,
          'delivery_date' => $date->toString("yyyy-MM-dd"),
          'description' => $form->getValue('delivery_description'),
          'inserted' => new Zend_Db_Expr('NOW()'),
        ]);

        $warehouse = new Table_PartsWarehouse();
        foreach ($parts_arr as $name => $value) {
          //$deliveryID

          if ($value < 1) {
            continue;
          }

          if (preg_match("/^part_/", $name)) { //soucast musi zacinat "part_"
            $idPart = substr($name, 5, strlen($name)); //odstraneni "part_"
            $amount = abs((int) trim($value));

            $productionsPart = $this->_db->fetchRow("SELECT * FROM productions_parts WHERE id_productions = :prod AND id_parts = :part", [
              'prod' => $this->_request->getParam('production'),
              'part' => $idPart,
            ]);

            if (empty($productionsPart['id_parts_operations'])) {
              // nejedna se o operaci, provede se odecet podsoucasti
              // odecita se z libovolneho externiho skladu
              $warehouse->addPart($idPart, $amount);
            }
            else {
              // dodaci list pro operaci
              $pwe = new Table_PartsWarehouseExtern();

              $operationRow = $this->_db->fetchRow("SELECT * FROM parts_operations WHERE id = :id", ['id' => $productionsPart['id_parts_operations']]);
              if (!$operationRow) {
                throw new Exception('Nepodarilo se najit operaci!');
              }

              $extWHid = $pwe->getWarehouseByProducer($operationRow['id_producers']);
              if (empty($extWHid)) {
                throw new Exception('Table_PartsWarehouseExtern->getWarehouseByProducer vraci prazdny vysledek');
              }


              if ($amount > $pwe->getAmount($extWHid, $operationRow['id_parts'])) {
                $missing = $amount - $pwe->getAmount($extWHid, $operationRow['id_parts']);
                $this->_db->rollBack();
                $this->_flashMessenger->addMessage("Není dostatek součásti " . $operationRow['id_parts'] . ". Chybí $missing ks.");
                $this->redirect($_SERVER['HTTP_REFERER']);
                return;
              }
              else {
                $pwe->removePart($extWHid, //sklad
                  $operationRow['id_parts'], //soucast
                  $amount //pocet
                );
              }
            }

            $this->_db->insert('productions_deliveries_parts', [
              'id_productions_deliveries' => $deliveryID,
              'id_productions_parts' => $productionsPart['id'],
              'delivery_amount' => $amount,
              'inserted' => new Zend_Db_Expr("NOW()"),
            ]);

            $this->_db->update('productions_parts', ['amount_remain' => new Zend_Db_Expr("(amount_remain) - $amount")], "id = " . $productionsPart['id']);
            $this->_db->update('productions_parts', ['amount_remain' => '0'], "amount_remain < 0");
          }
        }

        if ($form->status->getValue() == 'closed') {
          $this->_db->update('productions', ['status' => 'closed'], "id = " . $this->_db->quote($this->_request->getParam('production')));
        }

        // commit
        $this->_db->commit();

        // add actuality
        $act = new Table_Actualities();
        $act->add("Přidal dodací list.");

        // message and redirect
        $this->_flashMessenger->addMessage("Dodací list uložen, položky skladu naskladněny.");
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
        $this->_db->rollBack();
      }
      return $this->redirect('producers/show-production/id/' . $this->_request->getParam('production'));
    }

    /**
     * To view
     */
    $this->view->title .= $this->view->details['name'];
    $this->view->details = $productions->getDetails($this->_request->getParam('production'));
    $this->view->parts = $productions->getParts($this->_request->getParam('production'));
    $this->view->form = $form;
  }

  public function closeProductionAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if($this->getRequest()->getParam('id')){
			$this->_db->update('productions', array(
				'status' => 'closed'
			), "id = ".$this->_db->quote($this->getRequest()->getParam('id')));
			$this->_redirect('/producers/show-production/id/'.$this->getRequest()->getParam('id'));
		} else {
			$this->_redirect('/');
		}
	}

	public function detailAction(){
		$this->_helper->layout->setLayout('bootstrap-basic');

		$producers = new Table_Producers();
		$producersPersons = new Table_ProducersPersons();
		$producersParts = new Table_ProducersParts();
		$tOperations = new Table_PartsOperations();

		if( !($id = $this->_request->getParam('id')) || count($producers->find($id)) < 1 ) return $this->_redirect('producers');

		$this->view->data_producer = $producers->find($id)->current();
		$this->view->persons = $producersPersons->fetchAll("id_producers = ".$producers->getAdapter()->quote($id));
		$this->view->parts = $producersParts->getParts($id);
		$this->view->operations = $tOperations->getOperationsByProducer($id);
	}

	public function detailCarrierAction(){
    $carrier = new Producers_Carrier($this->_request->getParam('id'));
		$this->view->carrier = $carrier;
    $this->view->title = 'Dopravce ' . $carrier->getName();
	}

  public function addCarrierMethodAction() {
    $form = new Orders_CarrierMethodForm();
    $form->removeElement('is_active');
    if ($this->_request->isPost()) {
      $data = $this->_request->getParams();
      if ($form->isValid($data)) {
        $carrier = new Producers_Carrier($form->getValue('id_producers'));
        $data = array(
          'name' => $form->getValue('name'),
          'default_price' => $form->getValue('default_price') ? (float) $form->getValue('default_price') : NULL,
          'is_pos' => $form->getValue('is_pos'),
        );
        $carrier->addDelivery($data);
        $this->_flashMessenger->addMessage('Metoda byla přidána');
        $this->_redirect($this->view->url(array('action' => 'detail-carrier')));
      }
      else {
        $form->populate($data);
      }
    }
    $form->setLegend('Přidání doručovací metody');
    $form->setProducer($this->_request->getParam('id'));
    $this->view->form = $form;
    $this->renderScript('producers/edit-carrier-method.phtml');
  }

  public function editCarrierMethodAction() {

    $form = new Orders_CarrierMethodForm();
    if ($this->_request->isPost()) {
      $data = $this->_request->getParams();
      if ($form->isValid($data)) {
        $carrier = new Producers_Carrier($form->getValue('id_producers'));
        $formData = array(
          'id' => $form->getValue('id'),
          'id_producers' => $form->getValue('id_producers'),
          'name' => $form->getValue('name'),
          'default_price' => $form->getValue('default_price') ? (float) $form->getValue('default_price') : NULL,
          'is_active' => $form->getValue('is_active'),
          'is_pos' => $form->getValue('is_pos'),
        );
        $carrier->updateDelivery($formData);
        $this->_flashMessenger->addMessage('Metoda byla upravena.');
        $this->redirect($this->view->url(array('action' => 'detail-carrier', 'id' => $formData['id_producers'], 'pid' => NULL)));
      }
      else {
        $form->populate($data);
      }
    }
    else {
      $methodId = $this->_request->getParam('id');
      $tOrdersCarriers = new Table_OrdersCarriers();
      $data = $tOrdersCarriers->find($methodId)->current()->toArray();
      $form->populate($data);
    }
    $form->setLegend('Editace doručovací metody');
    $this->view->form = $form;
  }

	public function editPartAction(){
		if(!$this->_request->getParam('id')) {
            return $this->_redirect('producers');
        }
		$this->_helper->layout->setLayout('bootstrap-basic');
		$form = new Producers_PartForm;
		$form->id->setValue($this->_request->getParam('id'));

		$producersParts = new Table_ProducersParts();
		$partRow = $producersParts->find($this->_request->getParam('id'))->current();
		$form->p_part_id->setValue($partRow->producers_part_id);
		$form->p_part_name->setValue($partRow->producers_part_name);
		$form->p_part_min_amount->setValue($partRow->min_amount);
    $form->p_part_price->setValue($partRow->price);
		$form->setDescription($form->getDescription().' '.$partRow->id_parts);

		if($this->_request->isPost() && $form->isValid($this->_request->getParams())) {

			$partRow->producers_part_id = trim($form->p_part_id->getValue());
			$partRow->producers_part_name = trim($form->p_part_name->getValue());
			$partRow->min_amount = trim($form->p_part_min_amount->getValue());
      $partRow->price = trim($form->p_part_price->getValue());
			$partRow->save();

      $part = new Parts_Part($partRow->id_parts);
      $part->refreshPrice();

			$act = new Table_Actualities();
			$act->add("Upravil součást ".$partRow->id_parts." dodavatele '".$partRow->id_producers."'.");
			$this->_flashMessenger->addMessage("Součást upravena.");
			$this->_redirect($this->view->url(array('controller'=>'producers','action'=>'detail','id'=>$partRow->id_producers),null,true));
		}

		$this->view->form = $form;
		$this->render('add');
	}

    public function deletePartAction() {
		if(!$this->_request->getParam('id')) {
            return $this->_redirect('producers');
        }
        $producersParts = new Table_ProducersParts();
        $partRow = $producersParts->find($this->_request->getParam('id'))->current();
        if($partRow) {
            $partRow->is_deleted = 1;
            $partRow->id_parts = 'xxx' . $partRow->id_parts;
            $partRow->save();
            $this->_flashMessenger->addMessage("Součást odstraněna.");
            $this->_redirect($this->view->url(array('controller'=>'producers','action'=>'detail','id'=>$partRow->id_producers),null,true));
        }
        return $this->_redirect('producers');
    }

	public function editAction(){
		$this->view->title = 'Editace dodavatele';
		$this->_helper->layout->setLayout('bootstrap-basic');
		$table = new Table_Producers();

		if( !($id = $this->_request->getParam('id')) || count($table->find($id)) < 1 ) return $this->_redirect('producers');

		$form = new Producers_Form();
                $form->setLegend($this->view->title);

		if($this->_request->getParam('Odeslat') && $this->_request->isPost()){

			if($form->isValid($this->_request->getParams())){

				$wheId = $this->_request->getParam("meduse_producer_warehouse");

				$data = array(
					"name"			=> $this->_request->getParam("meduse_producer_name"),
					"address"		=> $this->_request->getParam("meduse_producer_address"),
					"www"			=> $this->_request->getParam("meduse_producer_www"),
					"activity"		=> $this->_request->getParam("meduse_producer_activity"),
					"id_extern_warehouses" => empty($wheId) ? new Zend_Db_Expr('NULL') : $wheId,
					"description"	=> $this->_request->getParam("meduse_producer_desc"),
					"id_producers_ctg" => $this->_request->getParam("meduse_producer_category"),
					"is_active" => $this->_request->getParam("meduse_producer_is_active")
				);

				if($this->_request->getParam("meduse_producer_warehouse")){
					$data['id_extern_warehouses'] = $this->_request->getParam("meduse_producer_warehouse");
				}
				$table->edit($data, $id);

				$act = new Table_Actualities();
				$act->add("Upravil dodavatele '".$this->_request->getParam("meduse_producer_name")."'.");

				$this->_flashMessenger->addMessage("Dodavatel upraven.");
				return $this->_redirect('producers/detail/id/'.$id);
			}

		} else {

			$row = $table->find($id)->current();
			if(!empty($row->id_extern_warehouses)){
				//$form->removeElement('meduse_producer_warehouse');
				$whe = new Table_PartsWarehouseExtern();
				$form->meduse_producer_warehouse->addMultiOption($row->id_extern_warehouses, $whe->getWhName($row->id_extern_warehouses));
				$form->meduse_producer_warehouse->setValue($row->id_extern_warehouses);

			}

			//plneni z databaze
			$row = $table->find($id)->current();
			$data = array(
				'meduse_producer_name'	    => $row->name,
				'meduse_producer_address'   => $row->address,
				'meduse_producer_www'	    => $row->www,
				'meduse_producer_desc'	    => $row->description,
				'meduse_producer_activity'  => $row->activity,
				"meduse_producer_category"  => $row->id_producers_ctg,
                "meduse_producer_is_active" => (($row->is_active == 'y') ? true : false),
				'meduse_producer_carriers' => $row->id_orders_carriers
			);
			$form->populate($data);
		}

		$this->view->form = $form;
		//return $this->render('add');
	}

		public function editCarrierAction() {

			$this->view->title = 'Editace dopravce';
			$this->_helper->layout->setLayout('bootstrap-basic');
			$table = new Table_Producers();


			if( !($id = $this->_request->getParam('id')) || count($table->find($id)) < 1 ) return $this->_redirect('producers');

			$form = new Producers_Form();
			$form->setLegend($this->view->title);

			$this->view->title = 'Úprava dopravce';
			$form->setDescription("Úprava dopravce");
			$form->removeElement('meduse_producer_warehouse');
			$form->removeElement('meduse_producer_activity');
			$form->removeElement('meduse_producer_category');
      $elem = new Meduse_Form_Element_Text('tracking_pattern');
      $elem->setLabel('Tracking pattern:');
      $form->addElement($elem);
			$form->addElement(new Zend_Form_Element_Hidden('meduse_producer_carriers'));
			//$data['meduse_producer_category'] = '21';

			if($this->_request->getParam('Odeslat') && $this->_request->isPost()){

				if($form->isValid($this->_request->getParams())){

					$wheId = $this->_request->getParam("meduse_producer_warehouse");

					$data = array(
						"name"			=> $this->_request->getParam("meduse_producer_name"),
						"address"		=> $this->_request->getParam("meduse_producer_address"),
						"www"			=> $this->_request->getParam("meduse_producer_www"),
						"activity"		=> $this->_request->getParam("meduse_producer_activity"),
						"id_extern_warehouses" => empty($wheId) ? new Zend_Db_Expr('NULL') : $wheId,
						"description"	=> $this->_request->getParam("meduse_producer_desc"),
						"id_producers_ctg" => $this->_request->getParam("meduse_producer_category"),
						"is_active" => $this->_request->getParam("meduse_producer_is_active"),
            'tracking_pattern' => $this->_request->getParam('tracking_pattern'),
					);

					if($this->_request->getParam("meduse_producer_warehouse")){
						$data['id_extern_warehouses'] = $this->_request->getParam("meduse_producer_warehouse");
					}

					$table->edit($data, $id);

					$act = new Table_Actualities();
					$act->add("Upravil dopravce '".$this->_request->getParam("meduse_producer_name")."'.");

					$this->_flashMessenger->addMessage("Dopravce upraven.");
					return $this->_redirect('producers/detail-carrier/id/'.$id);
				}

			} else {

				$row = $table->find($id)->current();
				if(!empty($row->id_extern_warehouses)){
					$whe = new Table_PartsWarehouseExtern();
					$form->meduse_producer_warehouse->addMultiOption($row->id_extern_warehouses, $whe->getWhName($row->id_extern_warehouses));
					$form->meduse_producer_warehouse->setValue($row->id_extern_warehouses);

				}

				//plneni z databaze
				$row = $table->find($id)->current();
				$data = array(
					'meduse_producer_name'	    => $row->name,
					'meduse_producer_address'   => $row->address,
					'meduse_producer_www'	      => $row->www,
					'meduse_producer_desc'	    => $row->description,
					'meduse_producer_activity'  => $row->activity,
					"meduse_producer_category"  => $row->id_producers_ctg,
					"meduse_producer_is_active" => (($row->is_active == 'y') ? true : false),
					'meduse_producer_carriers'  => $row->id_orders_carriers,
          'tracking_pattern'          => $row->tracking_pattern,
				);

					$form->removeElement('meduse_producer_warehouse');
					$form->removeElement('meduse_producer_activity');
					$form->addElement(new Zend_Form_Element_Hidden('meduse_producer_carriers'));
					$data['meduse_producer_category'] = Table_Producers::CTG_CARRIERS;
          $form->populate($data);
			}

			$this->view->form = $form;
			$this->render('edit');
		}


  public function editPersonAction() {

    $this->_helper->layout->setLayout('bootstrap-basic');

    $isCarrier = $this->_getParam('carrier');

    $form = new Producers_PersonForm();
    $form->setLegend("Upravit kontakt");

    // is carrier remove fields
    if (isset($isCarrier)) {
      $form->removeElement('meduse_producer_email');
      $form->removeElement('meduse_producer_position');
      $form->removeElement('meduse_producer_position_select');
    }

    $table = new Table_ProducersPersons();

    if (!($id = $this->_request->getParam('id')) || count($table->find($id)) < 1) {
      return $this->redirect('producers');
    }

    if ($this->_request->getParam('Odeslat') && $this->_request->isPost()) {

      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        $data = $form->getValidValues($params);
        $table->edit([
          'name' => $data['meduse_producer_deputy'],
          'phone' => $data['meduse_producer_phone'],
          'phone2' => $data['meduse_producer_phone2'],
          'email' => $data['meduse_producer_email'],
          'position_old' => $data['meduse_producer_position'],
          'position' => $data['meduse_producer_position_select'],
        ], $id);

        $this->_flashMessenger->addMessage("Kontakt byl upraven");
        $row = $table->find($id)->current();

        $act = new Table_Actualities();
        $table = new Table_Producers();

        if ($table->isCarrier($row->id_producers)) {
          $act->add("Upravil kontakt dopravce '" . $this->_request->getParam("meduse_producer_name") . "'.");
          return $this->redirect('producers/detail-carrier/id/' . $row->id_producers);
        }
        else {
          $act->add("Upravil kontakt dodavatele '" . $this->_request->getParam("meduse_producer_name") . "'.");
          return $this->redirect('producers/detail/id/' . $row->id_producers);
        }

      }


    }
    else {

      //plneni z databaze
      $row = $table->find($id)->current();
      $data = [
        'meduse_producer_deputy' => $row->name,
        'meduse_producer_phone' => $row->phone,
        'meduse_producer_phone2' => $row->phone2,
        'meduse_producer_email' => $row->email,
        'meduse_producer_position' => $row->position_old,
        'meduse_producer_position_select' => $row->position,
      ];
      $form->populate($data);
    }

    $this->view->form = $form;

    return $this->render('add');

  }

  public function deleteAction()
  {

    if ($this->_request->getParam('id')) {
      $this->_helper->viewRenderer->setNoRender();

      $table = new Table_Producers();

      $table2 = new Table_ProducersPersons();
      $table2->delete("id_producers = " . $table->getAdapter()->quote((int)$this->_request->getParam('id')));

      try {
        if ($table->delete("id = " . $table->getAdapter()->quote((int)$this->_request->getParam('id')))) {
          $this->_flashMessenger->addMessage("Dodavatel odstraněn spolu se všemi kontakty.");
        }
      } catch (Exception $e) {
        $this->_flashMessenger->addMessage("Dodavatele se nepodařilo odstranit. V databázi existují záznamy výroby, které jsou s ním spojené.");
      }

    }
    $this->redirect('producers');

  }

  public function activateAction() {
    $this->_helper->viewRenderer->setNoRender();
    if (! $producerId = $this->_request->getParam('id')) {
      $message = 'ID výrobce nebylo zadáno.';
      $this->_flashMessenger->addMessage($message);
      $this->redirect($this->view->url([
        'controller' => 'producers',
        'action' => 'index',
        'status' => null,
      ]));
    }

    $status = (bool) $this->_request->getParam('status', 1);
    $producer = new Producers_Producer($producerId);
    $producer->setActive($status);
    $message = $producer->getName() . ($status ? ' byl aktivován.' : ' byl deaktivován.');
    $this->_flashMessenger->addMessage($message);

    if ($back = $this->_request->getParam('back')) {
      $backUrl = base64_decode($back);
    }
    else {
      $backUrl = $this->view->url([
        'controller' => 'producers',
        'action' => 'detail',
        'id' => $producerId,
        'status' => null
      ]);
    }

    $this->redirect($backUrl);
  }


	//priradi vyrobce soucasti a presmeruje na detail soucasti
	public function assignAction(){
		if(!$this->_request->getParam('part')) $this->_redirect('parts/warehouse');
		if(!$this->_request->getParam('id')) $this->_redirect('parts/warehouse');
		$part = new Parts_Part($this->_request->getParam('part'));
		$authNamespace = new Zend_Session_Namespace('Zend_Auth');
		$table = new Table_ProducersParts();
		$table->insert(array(
			"id_producers"	=> (int) $this->_request->getParam('id'),
			"id_parts"		=> $this->_request->getParam('part'),
			"id_users"		=> $authNamespace->user_id,
			"inserted"		=> date('Y-m-d H:i:s')
		));

		$this->_flashMessenger->addMessage("Dodavatel přiřazen.");

		return $part->isTobacco()?
				$this->_redirect('tobacco/detail/id/' . $this->_request->getParam('part'))
				: $this->_redirect('parts/detail/id/' . $this->_request->getParam('part'));
	}

	public function removePartAction(){
		if(!$this->_request->getParam('part')) $this->_redirect('parts/warehouse');
		$part = new Parts_Part($this->_request->getParam('part'));
		$table = new Table_ProducersParts();
		$table->delete("id_parts = ".$table->getAdapter()->quote($this->_request->getParam('part')));
		$this->_flashMessenger->addMessage("Dodavatel odebrán.");
		return $part->isTobacco()?
			$this->_redirect('tobacco/detail/id/'.$this->_request->getParam('part'))
			: $this->_redirect('parts/detail/id/' . $this->_request->getParam('part') . '/cnt/1');
	}

  /**
   * Report soucasti k objednani po vyrobcich
   * @throws Zend_Form_Exception
   */
	public function partsAction(){
		$this->_helper->layout->setLayout('bootstrap-basic');

		$this->view->title = "Objednávka součástí - podle dodavatelů";

		$tOrders = new Table_Orders();

		$costs = [];
		$options = [];
		$this->view->strategicTime = 0;
		$this->view->strategicPrice = 0;
    $this->view->strategicOperation = 0;
    if ($this->view->showStrategicCost = Utils::isAllowed('orders/show-strategic-costs')) {
      $tOrdersProducts = new Table_OrdersProducts();
      $costs = $tOrdersProducts->getCosts(NULL, [Table_Orders::STATUS_STRATEGIC]);
      $options['costs'] = $costs;
    }
    if ($costs) {
      foreach ($costs as $item) {
        $this->view->strategicTime += $item['operation_time_total'];
        $this->view->strategicOperation += $item['operation_cost_total'];
        $this->view->strategicPrice += $item['price_total'];
      }
    }

    $form = new Orders_FilterForm($options);

		//producer select
		$rows = $tOrders->getAdapter()->fetchAll($tOrders->getAdapter()->select()->from('producers')->where("EXISTS(SELECT * FROM producers_parts WHERE id_producers = producers.id)")->order('name'));
    $element = new Meduse_Form_Element_Select('producer');
    $element->setLabel('Dodavatel');
    $element->addMultiOption('null', '- všichni -');
    foreach ($rows as $row) {
      $element->addMultiOption($row['id'], $row['name']);
    }
    $form->addElement($element);
    //producer select

		//vsechny soucasti
		$element = new Meduse_Form_Element_Checkbox('all_parts');
		$element->setLabel('Všechny součásti');
		$form->addElement($element);
		//vsechny soucasti

		$this->view->title = "Objednávka součástí";

		if($this->_request->isPost() && $form->isValid($this->_request->getParams())){

			$report = new Orders_PartsReport();
			$report->setFilter($form->getValues());
			$report->setOrder(Orders_PartsReport::ORDER_PRODUCERS);
			if ($form->all_parts->getValue() === 'y') {
				$report->setShowRows(Orders_PartsReport::SHOW_ALL);
			}
      else {
				$report->setShowRows(Orders_PartsReport::SHOW_MISSING_ONLY);
			}

			$fValues = $form->getValues();
			foreach ($fValues as $oid => $val) {
				if (is_int($oid) && $val === 'on') {
          $orders[] = $oid;
        }
			}

      $data = $report->getData();

			$this->view->orders = $orders;
			$this->view->show_report = TRUE;
			$this->view->data = $data;
      $this->view->show_sums = Utils::isAllowed('producers/parts-show-sums');
		}
		else {
      $this->view->show_report = FALSE;
    }
		$this->view->form = $form;
	}

	/**
	 * Zobrazeni prehledu vyroby - pro potvrzeni
	 * parametry: producer=idProducer,a libovolne mnozstvi soucasti
   * "p_"+idSoucasti=pocetKS
   * nebo "o_"+idOperace=pocetKS pro vyrobu operaci
	 */
	public function showOrderAction(){

		$this->_helper->layout->setLayout('bootstrap-basic');

		$parts           = array();
		$operations      = array();
		$producers       = array();
		$TProducersParts = new Table_ProducersParts();

		if($this->_request->getParam('prdcr')){
			$selectedProducer = (int) $this->_request->getParam('prdcr');
		} else {
			$selectedProducer = null;
		}

    $params = $this->_request->getParams();
		foreach($params as $name => $value){

			//soucasti
			if(preg_match("/^p_/",$name)){ //soucast musi zacinat "p_"
				$id = substr($name, 2, strlen($name)); //odstraneni "p_"
				$idproducer = $TProducersParts->getProducer($id);
				if(!is_null($selectedProducer) && $selectedProducer != $idproducer){
					continue;
				}
				if((int) $value < 1) continue;
				$parts[$id] = (int) $value;
				if(!in_array($idproducer, $producers)){ //!is_null($idproducer) &&
					array_push($producers, $idproducer);
				}
			}

			//operace
			if(preg_match("/^o_/",$name)){
				$id = (int) substr($name, 2, strlen($name)); //odstraneni "o_"
				$idproducer = $TProducersParts->getOperationProducer($id);
				if(!is_null($selectedProducer) && $selectedProducer != $idproducer){
					continue;
				}
				if((int) $value < 1) continue;
				$operations[$id] = (int) $value;
				if(!is_null($idproducer) && !in_array($idproducer, $producers)){ //
					array_push($producers, $idproducer);
				}
			}
		}
		if(count($parts) < 1 && count($operations) < 1 ){
			$this->_flashMessenger->addMessage("Musí být zadaná alespoň jedna nenulová hodnota výroby.");
			 $this->_redirect('producers/parts');
		}
		$this->view->producers = $producers;
		if(count($producers) > 1){
			$this->view->more_producers = true;
			return;
		} elseif(count($producers) == 1) {

			$this->view->form = new Parts_ProductionsForm();

			$TProducers = new Table_Producers();
			$TProductions = new Table_Productions();

			$idproducer = array_pop($producers);
			$this->view->productions = $TProductions->getProductionsByProducer($idproducer, count($operations) > 0);
			$this->view->producer_data = $TProducers->find($idproducer)->current();
			$finalParts = array();
			foreach($parts as $id => $amount){
				$tmp = new Parts_Part($id);
				array_push($finalParts, array(
					'id' => $id,
					'name' => $tmp->getName(),
					'amount' => $amount,
					'p_id' => $TProducers->getPartId($idproducer, $id),
					'p_name' => $TProducers->getPartName($idproducer, $id)
				));
			}
			foreach($operations as $id => $amount){
				$opr = $this->_db->fetchRow("SELECT * FROM parts_operations WHERE id = :id",array('id' => $id));
				if($opr){
					$tmp = new Parts_Part($opr['id_parts']);
					array_push($finalParts, array(
						'id' => $opr['id_parts'],
						'name' => $tmp->getName(),
						'operation' => $opr['name'],
						'operation_id' => $opr['id'],
						'amount' => $amount
					));
				}

			}

			$this->view->parts = $finalParts;
		}



	}

}
