<?php

class TobaccoDocumentsController extends Meduse_Controller_Action
{

    public function init(): void
    {
        $this->_helper->layout->setLayout('bootstrap-tobacco');
        $this->view->assign('title', 'Procesy (dokumentace)');
    }

    public function indexAction(): void
    {
        $this->view->assign('docs', Documents::findAll(Table_TobaccoWarehouse::WH_TYPE));
    }

    public function addAction(): void
    {
        $title = 'Vložení nového dokumentu';
        $form = Documents::getForm($title, Table_TobaccoWarehouse::WH_TYPE);
        if ($this->_request->isPost() && !$this->saveDocument()) {
            $form->populate($this->_request->getParams());
        }

        $this->view->assign('title', $title);
        $this->view->assign('form', $form);
        $this->renderScript('documents/form.phtml');
    }

    private function checkId(): int
    {
        if (!$id = $this->_request->getParam('id')) {
            $this->_flashMessenger->addMessage('Nebylo zadáno ID dokumentu.');
            $this->redirect($this->view->url(['action' => 'index', 'id' => null]));
        }
        return (int) $id;
    }

    public function downloadAction(): void
    {
        $this->_helper->viewRenderer->setNoRender();
        if (!$fid = $this->_request->getParam('fid')) {
            $this->_flashMessenger->addMessage('Nebylo zadano FID souboru.');
            $this->redirect($this->view->url(['action' => 'index', 'fid' => null]));
        }
        if (!$fileInfo = Documents::getFileInfo($fid)) {
            $this->_flashMessenger->addMessage("Data k souboru FID = $fid nebyla nenalazena.");
            $this->redirect($this->view->url(['action' => 'index', 'fid' => null]));
        }
        if (!file_exists($fileInfo['filepath'])) {
            $this->_flashMessenger->addMessage("Soubor '{$fileInfo['filepath']}' nebyl nalezen.");
            $this->redirect($this->view->url(['action' => 'index', 'fid' => null]));
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($fileInfo['filename']) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($fileInfo['filepath']));
        flush(); // Flush system output buffer
        readfile($fileInfo['filepath']);

        exit(200);
    }

    public function editAction(): void
    {
        $id = $this->checkId();
        $title = 'Editace infa o dokumentu';

        $doc = new Documents_Document($id);
        $form = $doc->getEditForm($title);
        if ($this->_request->isPost()) {
            $params = $this->_request->getParams();
            if ($form->isValid($params)) {
                $data = $form->getValidValues($params);
                $doc->setTitle($data['title'], false)
                    ->setDescription($data['description'], false)
                    ->save();
                $this->_flashMessenger->addMessage('Změny byly uloženy.');
                $this->redirect($this->view->url(['action' => 'detail']));
            }
        }

        $this->view->assign('title', $title);
        $this->view->assign('form', $form);
        $this->renderScript('documents/form.phtml');
    }

    /**
     * @throws Zend_Form_Exception
     */
    public function addVersionAction(): void
    {
        $id = $this->checkId();
        $title = 'Vložení nové verze';

        $doc = new Documents_Document($id);
        $form = $doc->getAddVersionForm($title, Table_TobaccoWarehouse::WH_TYPE);
        if ($this->_request->isPost()) {
            $params = $this->_request->getParams();
            if ($form->isValid($params)) {
                $this->saveFile($id);
                $this->redirect($this->view->url(['action' => 'detail']));
            }
        }

        $this->view->assign('title', $title);
        $this->view->assign('form', $form);
        $this->renderScript('documents/form.phtml');
    }

    protected function saveDocument(): bool
    {
        $form = Documents::getForm(null, Table_TobaccoWarehouse::WH_TYPE);
        try {
            $params = $this->_request->getParams();

            if ($form->isValid($params)) {
                $data = $form->getValidValues($params);
                $id = $data['id'] ? (int) $data['id'] : null;

                $title = $data['title'];
                $description = $data['description'] ?: null;

                $this->saveFile($id, $title, $description);
                $this->redirect($this->view->url(['action' => 'index']));

                return true;
            }

            $this->_flashMessenger->addMessage('Zadaná data nejsou validní.');
            return false;
        } catch (Zend_Form_Exception $e) {
            Utils::logger()->err($e->getMessage());
            $this->_flashMessenger->addMessage('Něco se nepovedlo. Viz systémový log.');
            return false;
        }
    }

    protected function saveFile($id = null, $title = null, $description = null): void
    {
        $result = Documents::filesUpload($id, Table_TobaccoWarehouse::WH_TYPE, $title, $description);
        if (is_array($result)) {
            foreach ($result as $message) {
                $this->_flashMessenger->addMessage($message);
            }
        }
        if ($result === true) {
            $this->_flashMessenger->addMessage('Dokument byl uložen.');
        }
    }

    public function detailAction(): void
    {
        if (!$id = $this->_request->getParam('id')) {
            $this->_flashMessenger->addMessage('Nebylo zadáno ID dokumentu.');
            $this->redirect($this->view->url(['action' => 'index', 'id' => null]));
        }

        $doc = new Documents_Document($id);
        $files = $doc->getFiles();

        $this->view->assign('title', $doc->getTitle());
        $this->view->assign('description', $doc->getDescription());
        $this->view->assign('user_name', $doc->getUserName());
        $this->view->assign('last_version', $doc->getLastVersion());
        $this->view->assign('files', $files);
        $this->view->assign('files_count', count($files));
    }

    public function deleteAction(): void
    {
        $id = $this->checkId();
        $doc = new Documents_Document($id);
        $undo = (bool) $this->getParam('undo', 0);
        if ($undo) {
            $message = "Dokument '{$doc->getTitle()}' byl obnoven.";
            $doc->setDeleted(false);
        } else {
            $doc->setDeleted(true);
            $message = "Dokument '{$doc->getTitle()}' byl smazán. <a href='{$this->view->url(['undo' => 1])}'>Vrátit</a>";
        }
        $this->_flashMessenger->addMessage($message);
        $this->redirect($this->view->url(['action' => 'index', 'id' => null, 'undo' => null]));
    }
}