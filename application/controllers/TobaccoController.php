<?php

class TobaccoController extends Meduse_Controller_Action {

  const DOWNLOAD_PATH = '/export-medite/';

  protected $db = NULL;

  public function init() {
    $this->db = Zend_Registry::get('db');
    $this->_helper->layout->setLayout('bootstrap-tobacco');
  }

	public function indexAction() {
		$this->_helper->layout()->disableLayout();
		$this->redirect('/tobacco-orders');
	}

	public function warehouseAction() {
    $this->view->show = true;
    if ($this->_request->getParam('new')) {
      Zend_Registry::get('tobacco_warehouse')->filter_id = null;
      Zend_Registry::get('tobacco_warehouse')->filter_name = null;
      Zend_Registry::get('tobacco_warehouse')->filter_producer = null;
      $this->redirect('tobacco/warehouse');
    }
    $reg = Zend_Registry::get('tobacco_warehouse');
    if (!isset($reg->last_tab)) {
      $reg->last_tab = 'materials';
    }
    if ($this->_request->isPost()) {
      $this->view->show = false;
      $reg->filter_id = $this->_request->getParam('filter_id');
      $reg->filter_name = $this->_request->getParam('filter_name');
      $reg->filter_producer = $this->_request->getParam('filter_producer');
      Zend_Registry::set('tobacco_warehouse', $reg);
    }
    if ($this->_request->getParam('sort')) {
      $this->view->show = false;
    }
    $this->view->filter = new Tobacco_Warehouse_FilterForm();
    $this->view->tab = $this->_request->getParam('tab', $reg->last_tab);
    $warehouse = new Table_TobaccoWarehouse();
    $params = $this->getRequest()->getParams();
    switch ($this->view->tab) {

      case 'packaging':
        $this->warehouseActionPackagng(Table_TobaccoWarehouse::CTG_PACKAGING, $params, $warehouse);
        break;
      case 'packaging_labels':
        $this->warehouseActionPackagng(Table_TobaccoWarehouse::CTG_PACKAGING_LABELS, $params, $warehouse);
        break;
      case 'packaging_labels_box_010':
        $this->warehouseActionPackagng(Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_BOX_010, $params, $warehouse);
        break;
      case 'packaging_labels_box_050':
        $this->warehouseActionPackagng(Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_BOX_050, $params, $warehouse);
        break;
      case 'packaging_labels_sachet_010':
        $this->warehouseActionPackagng(Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_SACHET_010, $params, $warehouse);
        break;
      case 'packaging_sachets':
        $this->warehouseActionPackagng(Table_TobaccoWarehouse::CTG_PACKAGING_SACHETS, $params, $warehouse);
        break;
      case 'packaging_boxes':
        $this->warehouseActionPackagng(Table_TobaccoWarehouse::CTG_PACKAGING_BOXES, $params, $warehouse);
        break;
      case 'packaging_other':
        $this->warehouseActionPackagng(Table_TobaccoWarehouse::CTG_PACKAGING_OTHER, $params, $warehouse);
        break;

      case 'tobacco_nfs':
        $this->view->filter->removeProducers();
        $this->view->title = Table_TobaccoWarehouse::$ctgToString[Table_TobaccoWarehouse::CTG_TOBACCO_NFS];
        $this->view->grid = $warehouse->getDataGridTobaccoNFS($params);
        break;

      case 'duty_stamps':
        $this->view->filter->removeProducers();
        $this->view->title = Table_TobaccoWarehouse::$ctgToString[Table_TobaccoWarehouse::CTG_DUTY_STAMPS];
        $this->view->grid = $warehouse->getDataGridDutyStamps($params);
        $form = new Tobacco_Warehouse_AddItemForm();
        if (isset($params['sort'])) {
          $form->setAction($form->getAction() . '/sort/' . $params['sort']);
        }
        $this->view->addItemForm = $form;
        break;
      case 'tobacco_fs':
        $this->view->filter->removeProducers();
        $this->view->title = Table_TobaccoWarehouse::$ctgToString[Table_TobaccoWarehouse::CTG_TOBACCO_FS];
        $this->view->grid = $warehouse->getDataGridTobaccoFS($params);
        break;
      case 'accessories':
        $this->view->title = Table_TobaccoWarehouse::$ctgToString[Table_TobaccoWarehouse::CTG_ACCESSORIES];
        $this->view->grid = $warehouse->getDataGridAccessories($params);
        $form = new Tobacco_Warehouse_AddItemForm();
        if (isset($params['sort'])) {
          $form->setAction($form->getAction() . '/sort/' . $params['sort']);
        }
        $this->view->addItemForm = $form;
        break;
      case 'materials':
      default:
        $this->view->title = Table_TobaccoWarehouse::$ctgToString[Table_TobaccoWarehouse::CTG_MATERIALS];;
        $this->view->grid = $warehouse->getDataGridMaterials($params);
        $form = new Tobacco_Warehouse_AddItemForm();
        if (isset($params['sort'])) {
          $form->setAction($form->getAction() . '/sort/' . $params['sort']);
        }
        $this->view->addItemForm = $form;
        break;
    }


  }

  /**
   * @param int $subcategory
   * @param array $params
   * @param \Table_TobaccoWarehouse $warehouse
   */
  private function warehouseActionPackagng($subcategory, $params, $warehouse) {
    $this->view->title = Table_TobaccoWarehouse::$ctgToString[$subcategory];
    $this->view->grid = $warehouse->getDataGridPackaging($params, $subcategory);
    $form = new Tobacco_Warehouse_AddItemForm();
    if (isset($params['sort'])) {
      $form->setAction($form->getAction() . '/sort/' . $params['sort']);
    }
    $this->view->addItemForm = $form;
  }


	public function partsDetailAction() {

		// polozka
		$id = $this->_request->getParam('id');

    try {
      $part = new Tobacco_Parts_Part($id);
    }
    catch (Exception $e) {
      $this->_flashMessenger->addMessage($e->getMessage());
      $this->redirect('tobacco/warehouse');
    }

    $this->view->part = $part;

    // Mnozstvi na sklade - pocet kusu nebo kilogramu.
    $amount = $part->getWHAmount(NULL, Tobacco_Parts_Part::UNIT_KILOGRAMS);
    $this->view->amount = $amount;

		// Jenotka.
    $measure = $part->getMeasure();
    $this->view->measure = $measure;
    $this->view->measure_unit = $measure === Parts_Part::WH_MEASURE_UNIT ? 'ks' : 'kg';

    // Cena za jednotku.
    $unit_price = $part->getUnitPrice(Tobacco_Parts_Part::UNIT_KILOGRAMS);
    $this->view->price = $unit_price;

    // Celkova cena soucasti na sklade.
    $this->view->price_total = $unit_price * $amount;

    // Je mozno prepociat vyrobni cenu?
    $this->view->recalculating_enabled = $part->reCalculatingAllowed();

		// vyrobce
		$tProducerParts = new Table_ProducersParts();
		$producer = $tProducerParts->getProducer($part->getID());
		$tProducers = new Table_Producers();
		$producer = $tProducers->find($producer);
		if ($producer) {
			$this->view->producer = $producer->current();
		}

		// produkt (EAN a DPH)
		if ($part->isProduct()) {
			$tEans = new Table_Eans();
			$this->view->ean = $tEans->getCode($part->getID());
			$tDPH = new Table_ApplicationDph();
			$this->view->dph = $tDPH->getActualDph();
		}

    // Pohyby na skladu
    $year = $this->getParam('year', date('Y'));
    $from = new Meduse_Date($year . '-01-01', 'Y-m-d');

    /* @internal
     * POHYBY PRODUKTU
     * Hola, hola,
     * produkt se hybá.
     * Ode zdi ke zdi,
     * skladník zatíná pěsti.
     */
    $totalAdd = 0;
    $totalRemove = 0;
    if ($changes = $part->getWHChanges($from)) {
      foreach ($changes as $item) {
        $totalAdd += $item['amount_add'];
        $totalRemove += $item['amount_remove'];
      }
    }
    $this->view->changes = $changes;
    $this->view->changesTotalAdd = $totalAdd;
    $this->view->changesTotalRemove = $totalRemove;
    $this->view->changesTotalDiff = $totalAdd - $totalRemove;


    // Prodeje produktu
    if ($part->isProduct()) {
      $totalAmount = 0;
      if ($sales = $this->view->sales = $part->getSales($from)) {
        foreach ($sales as $item) {
          $totalAmount += $item['amount'];
        }
      }
      $this->view->sales = $sales;
      $this->view->salesTotalAmount = $totalAmount;
    }
    else {
      $this->view->sales = NULL;
    }

    // Stav skladu na zacatku roku
    $this->view->historyAmountStart = $part->getWHAmount($from);

    if ($part->isInspected()) {
      // Inventarni stavy na zacatku roku
      $wh = new Tobacco_Warehouse();
      if ($snapshots = $wh->getSnapshots($part, $from)) {
        $this->view->snapshot = array_shift($snapshots);
      }
      $this->view->allow_fix = Zend_Registry::get('acl')->isAllowed('tobacco/warehouse-snapshot-fix');
      $snapshots = $wh->getSnapshots($part);
      $this->view->snapshot_last = array_shift($snapshots);
    }
    else {
      $this->view->snapshot = NULL;
    }



    // Stav skladu ke konci roku
    $from->addYear(1);
    $this->view->historyAmountEnd = $part->getWHAmount($from);

    $this->view->year = $year;


		// nastaveni view
		$this->_helper->layout->setLayout('bootstrap-tobacco');
    $this->view->title = 'Detail položky ' . $part->getID() . ' "' . $part->getName() . '"';

    if ($part->isTobaccoNFS()) {
      $mix = NULL;
      try {
        $mix = $part->getMix();
        $mixObj = $part->getMix(TRUE);
        $this->view->mix = $mixObj;
        $this->view->mixId = $mixObj->getId();
        $this->view->mixUnitPrice = $mixObj->getUnitPrice();
        $this->view->mixName = $mixObj->getName();
        $this->view->mixFlavour = '#' . str_pad($mixObj->getFlavorId(), 3, '0', STR_PAD_LEFT) . ': ' . $mixObj->getFlavorName();
        $this->view->mixWeight = $mix['amount'];
      }
      catch(Tobacco_Parts_Exceptions_NoContentException $e) {
        $this->view->message = 'Pozor, produkt nemá definovaný obsah.';
      }
      $mixForm = new Tobacco_Parts_MixForm();
      $mixForm->setPartId($part->getID());
      if ($mix) {
        $mixForm->populate(array(
          'mix' => $mix['id_mixes'],
          'amount' => $mix['amount'],
        ));
      }
      $this->view->mixForm = $mixForm;
    }

    $this->view->showSubparts = in_array($part->getCtg(), array(
      Table_TobaccoWarehouse::CTG_TOBACCO_NFS,
      Table_TobaccoWarehouse::CTG_TOBACCO_FS,
      Table_TobaccoWarehouse::CTG_ACCESSORIES
    ));

    $this->view->deleted = $part->isDeleted();

	}

  public function partsAddAction() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
    if ($this->getRequest()->isPost()) {
      $data = $this->getRequest()->getParams();
      $subparts = array();
      if (in_array($data['category'], array(
        Table_TobaccoWarehouse::CTG_TOBACCO_NFS,
        Table_TobaccoWarehouse::CTG_TOBACCO_FS,
        Table_TobaccoWarehouse::CTG_ACCESSORIES))) {
        $count = isset($data['subparts_count']) ? (int) $data['subparts_count'] : 0;
        for($it = 0; $it < $count; $it++) {
          if ($data['subpart_id_' . $it]) {
            if (isset($subparts[$data['subpart_id_' . $it]])) {
            $subparts[$data['subpart_id_' . $it]] += (int) $data['subpart_amount_' . $it];
            } else {
              $subparts[$data['subpart_id_' . $it]] = (int) $data['subpart_amount_' . $it];
            }
          }
        }
      }
      try {
        $part = new Tobacco_Parts_Part($data);
        if ($subparts) {
          $part->addSubParts($subparts);
        }

        if ($data['category'] == Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS) {
          $tFlavors = new Table_PartsFlavors();
          $tFlavors->merge(array(
            'id_parts' => $data['id'], 'id_batch_flavors' => $data['flavor'],
          ), 'id LIKE "'. $data['id'] .'"');
        }

        if (isset($data['weight'])) {
          $part->setWeight($data['weight'], $data['ekokom_ctg']);
        }

        $this->_flashMessenger->addMessage('Položka byla přidána.');
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage('Položka nebyla přidána: ' . $e->getMessage());
      }
      $this->redirect('/tobacco/warehouse');
      exit;
    }

    $category = $this->getRequest()->getParam('ctg');

    $form = new Tobacco_Parts_Form(array(
      'name' => 'xxx',
      'category' => $category,
      'categories' => Table_TobaccoWarehouse::$ctgToString,
      'description' => 'Nová položka skladu tabáků',
    ));
    $form->setLegend('Nová položka skladu tabáků');
    $this->view->form = $form;

    $this->render('parts-edit');
  }

  public function   partsEditAction() {
    if ($this->getRequest()->isPost()) {
      $params = $this->getRequest()->getParams();

      try {
        $data = array(
          'name' => $params['name'],
          'description' => $params['description'],
          'price' => $params['price'],
          'id_parts_ctg' => $params['category'],
          'product' => isset($params['product'])? $params['product'] : 'n',
          'sku' => $params['sku'],
          'hs' => $params['hs'],
        );

        $part = new Tobacco_Parts_Part($params['id']);

        $tParts = new Table_Parts();
        $tParts->update($data, 'id LIKE "'. $params['id'] .'"');
        if ($params['producers']) {
          $tProducers = new Table_ProducersParts();
          $tProducers->update(array(
            'id_producers' => $params['producers']
            ), 'id_parts LIKE "' . $params['id'] . '"');
        }

        if ($params['category'] == Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS) {
          $tFlavors = new Table_PartsFlavors();
          $tFlavors->merge(array(
            'id_parts' => $params['id'], 'id_batch_flavors' => $params['flavor'],
          ), 'id_parts LIKE "'. $params['id'] .'"');
        }

        $tTobaccoWarehouse = new Table_TobaccoWarehouse();
        $tTobaccoWarehouse->update(array(
          'amount' => $part->getWHAmount(),
          'critical_amount' => $params['critical_amount']
        ), 'id_parts = "' . $params['id'] . '"');

        $this->_flashMessenger->addMessage('Položka "' . $params['id']
          . '" byla aktualizována.');
      } catch (Exception $e) {
        $this->_flashMessenger->addMessage('Aktualizace položka "' . $params['id']
          . '" se nezdařila: ' . $e->getMessage());
      }

      if (isset($params['weight'])) {
        $part->setWeight($params['weight'], $params['ekokom_ctg']);
      }

      /*
      // vaha soucasti
      if (isset($params['weight'])) {
        try {
        $weightData = array(
          'id_parts' => $params['id'],
          'weight' => $params['weight'],
          'id_ekokom_ctg' => $params['ekokom_ctg'],
        );
        $tPartsWeight = new Table_PartsWeight();
        $tPartsWeight->insertUpdate($weightData);
        $this->_flashMessenger->addMessage('Hmotnost položky "' . $params['id']
          . '" byla aktualizována.');
        } catch (Exception $e) {
          $this->_flashMessenger->addMessage('Aktualizace hmotnosti položky "' . $params['id']
            . '" se nezdařila: ' . $e->getMessage());
        }
      }
      */
      $this->redirect('/tobacco/parts-detail/id/' . $params['id']);

    }
    else {
      $this->_helper->layout->setLayout('bootstrap-tobacco');
      $part = new Tobacco_Parts_Part($this->getRequest()->getParam('id'));

      $form = new Tobacco_Parts_Form(array(
        'category' => $part->getCtg(),
        'flavor' => $part->getFlavor(),
        'categories' => Table_TobaccoWarehouse::$ctgToString,
        'description' => 'Editace položky skladu tabáků',
      ));

      if ($part->reCalculatingAllowed()) {
        $form->getElement('price')->setAttrib('readonly', 'readonly');
      }

      $tProducers = new Table_ProducersParts();
      $tTobaccoWarehouse = new Table_TobaccoWarehouse();
      $critical_amount = $tTobaccoWarehouse->getCriticalAmount($this->getRequest()->getParam('id'));

      $data = array(
        'name' => $part->getName(),
        'id' => $part->getID(),
        'description' => $part->getDesc(),
        'producers' => $tProducers->getProducer($part->getID()),
        'price' => $part->getPrice(),
        'product' => $part->isProduct()? 'y' : 'n',
        'critical_amount' => $critical_amount,
        'weight' => $part->getWeight(),
        'ekokom_ctg' => $part->getEkokomCategory(),
        'sku' => $part->getSKU(),
        'hs' => $part->getHS(),
      );
      $form->setLegend('Editace položky skladu tabáků');
      $form->getElement('id')->setAttrib('readonly', 'readonly');
      $form->populate($data);
      $this->view->form = $form;
    }

  }

  public function addItemAction() {
    if ($this->getRequest()->isPost()) {
      $part_id = $this->_request->getParam('id');
      if (in_array($part_id, Tobacco_Parts_Part::INSPECTED_PARTS_ID)) {
        $this->_flashMessenger->addMessage('Položka lze naskladnit pouze přes objednávku materiálu.');
        $this->redirect('/tobacco/productions-add');
      }
      $form = new Tobacco_Warehouse_AddItemForm();
      $data = array(
        'id' => $part_id,
        'amount' => $this->getRequest()->getParam('amount'),
      );
      if ($form->isValid($data)) {
        $part = new Tobacco_Parts_Part($data['id']);
        $ctg = $part->getParentCtg();
        $x = (float) str_replace(',', '.', $data['amount']);
        $amount = ($ctg == Table_TobaccoWarehouse::CTG_MATERIALS) ?
          round($x * 1000) : round($x);
        try {
          $tTW = new Table_TobaccoWarehouse();
          $tTW->addPart($data['id'], $amount, FALSE, 2);
          $this->_flashMessenger->addMessage('Položka ID = "' . $data['id']
            . '" byla naskladněna.');
        } catch (Exception $e) {
          $this->_flashMessenger->addMessage('Položka ID = "' . $data['id']
            . '" nebyla naskladněna. ' . $e->getMessage());
        }
      }
      else {
        $this->_flashMessenger->addMessage('Nebyla zadána správna data.');
      }
    }
    $sort = $this->_request->getParam('sort', null);
    if (is_null($sort)) {
      $this->redirect('/tobacco/warehouse/#' . $data['id']);
    }
    else {
      $this->redirect('/tobacco/warehouse/sort/' . $sort . '#' . $data['id']);
    }
  }

// ----------------------------------------------------------------------------
// MACERACE
// @todo Refaktorizovat do samostatneho controlleru
// ----------------------------------------------------------------------------

  /**
   * Prehled maceraci
   */
  public function macerationsAction() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
    $tMaceration = new Table_Macerations();
    $params = $this->getRequest()->getParams();
    $grid = $tMaceration->getDataGrid($params);
    $grid_zero = $tMaceration->getDataGrid($params, 'zero');
    $grid_discarded = $tMaceration->getDataGrid($params, 'discarded');
    $this->view->grid = $grid;
    $this->view->grid_zero = $grid_zero;
    $this->view->grid_discarded = $grid_discarded;
    $this->view->form = new Tobacco_Macerations_CheckForm();
    $this->view->storno = new Tobacco_Macerations_CancelForm();
  }

  public function macerationsAddAction() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
    $form = new Tobacco_Macerations_Form();

    //nepoužívá se, data jsou odesílána na macerationsEdit
    /*
    if ($this->getRequest()->isPost()) {
      $data = $this->getRequest()->getParams();
      if ($form->isValid($data)) {
        try {
          $maceration = new Tobacco_Macerations_Maceration();
          $maceration->setBatchNumber('L' . date('ymd') . $data['batch']);
          $maceration->setRecipe($data['recipe']);
          $maceration->setWeight($data['weight']);
          $maceration->setCritical($data['amount_critical']);

          $mapper = new Table_Recipes();

          $recipe_parts = new Tobacco_Recipes_Parts($mapper->getRecipeParts($maceration->getRecipeId()));
          $parts = $recipe_parts->getParts();

          foreach ($parts as $part) {
            $maceration->addIngredient($part['id_parts'], $part['ratio']);
          }
          $maceration->create();
        } catch (Exception $e) {
          print_r($e) ;
          $this->view->message = $e->getMessage();
        }
      }
    }
    */
    $this->view->form = $form;
  }

  public function macerationsEditAction() {

    $this->_helper->layout->setLayout('bootstrap-tobacco');
    $form = new Tobacco_Macerations_EditForm();
    if ($this->getRequest()->isPost()) {

      // kontrola jednoho sudu v jednom dni
      $created = new Meduse_Date($this->_request->getParam('created'), 'd.m.Y');
      $recipe = new Tobacco_Recipes_Recipe($this->_request->getParam('recipe'));
      $testBatch = Tobacco_Macerations_Maceration::generateBatch($created, $recipe->getFlavorId());
      if (!Tobacco_Macerations_Maceration::isFreeBatch($testBatch)) {
        $this->_flashMessenger->addMessage('Lze založit jen jeden barel dané receptury v jednom dni.');
        $this->redirect('tobacco/macerations');
      }

      // nová data, načtena z předchozího formuláře

      $recipeId = $this->getRequest()->getParam('recipe');
      $weight = (float) strtr($this->getRequest()->getParam('weight'), ',', '.');
      $created = $this->getRequest()->getParam('created');
      $critical = (float) strtr($this->getRequest()->getParam('amount_critical'), ',', '.');
      $tRecipes = new Table_Recipes();
      $items = $tRecipes->getRecipeItems($recipeId, $weight);
      $form->setRecipeId($recipeId);
      $form->setWeight($weight);
      $form->setCreated($created);
      $form->setCritical($critical);
    }
    elseif ($this->getRequest()->getParam('id')) {
      // nacteni existujici macerace
      $maceration = new Tobacco_Macerations_Maceration($this->getRequest()->getParam('id'));
      $formData = array(
        'id' => $maceration->getId(),
        'recipe' => $maceration->getRecipeId(),
        'name' => $maceration->getName(),
        'description' => $maceration->getDescription(),
        'total' => $maceration->getAmountIn(),
        'created' => $maceration->getStart(),
        'amount_critical' => $maceration->getCritical(),
      );
      $form->populate($formData);
      $items = $maceration->getIngredients();
    }

    $form->setItems($items);
    $this->view->form = $form;
  }

  public function macerationsSaveAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $ok = true;
    if ($this->getRequest()->isPost()) {
      $params = $this->getRequest()->getParams();
      $data = array(
        'id' => $params['id'],
        'amount_in' => (float) strtr($params['total'], ',', '.'),
        'name' => $params['name'],
        'description' => $params['description'],
        'recipe' => $params['recipe'],
        'start' => $params['created'],
        'amount_critical' => (float) strtr($params['amount_critical'], ',', '.'),
      );
      $recipe_id = $data['recipe'];
      $items = array();
      $count = 0;
      while(isset($params['item_id_' . $count])) {
        $items[$count] = array(
          'id' => $params['item_id_' . $count],
          'name' => $params['item_name_' . $count],
          'weight' => (float) strtr($params['item_weight_' . $count], ',', '.'),
        );
        $count++;
      }
      $form = new Tobacco_Macerations_EditForm();
      $recipes_table = new Table_Recipes();
      if ($form->isValid($data)) {
        try {
          if ($id = $this->getRequest()->getParam('id')) {
            $maceration = new Tobacco_Macerations_Maceration($id);
          }
          else {
            $maceration = new Tobacco_Macerations_Maceration();
          }
          $maceration
            ->setDescription($data['description'])
            ->setName($data['name'])
            ->setCritical($data['amount_critical']);
          if ($id) {
            $maceration->setAmountIn((float)$data['amount_in']);
            if ($status = $maceration->update($items)) {
              $this->_flashMessenger->addMessage('Macerace byla úspěšně upravena.');
            }
            else {
              $this->_flashMessenger->addMessage('Došlo k potížím, macerace nebyla upravena.');
            }
            $this->redirect('/tobacco/macerations-detail/id/' . $id);
          }
          else {
            /* zpracování šarže:
             * L + datum ve formátu YYMMDD + triciselne ozn. příchuti
             *
             * např.: L150618001
             */

            // získání receptu
            $recipe = new Tobacco_Recipes_Recipe($recipe_id);
            // zpracování data vytvoření
            $date = new Meduse_Date($params['created'], 'd.m.Y');
            $batch = Tobacco_Macerations_Maceration::generateBatch($date, $recipe->getFlavorId());
            if (!Tobacco_Macerations_Maceration::isFreeBatch($batch)) {
              $this->_flashMessenger->addMessage('Lze založit jen jeden barel dané receptury v jednom dni.');
              $this->redirect('tobacco/maserations');
              throw new Tobacco_Exceptions_OnlyOneBarrelPerDayException();
            }

            $maceration->setStart($date->toString('Y-m-d'));
            $maceration->setBatchNumber($batch);
            $maceration->setRecipe($recipe_id);

            /* Zpracování ingrediencí:
             *
             * každý tabák se skládá z určitých ingrediencí podle receptu (získán výše)
             */
            $recipe_parts = new Tobacco_Recipes_Parts($recipes_table->getRecipeParts($maceration->getRecipeId()));
            $parts = $recipe_parts->getParts();

            foreach ($parts as $part) {
              $maceration->addIngredient($part['id_parts'], $part['share'] * 0.01 * $params['total']);
            }

            $critical = $maceration->create();

            if(!empty($critical)) {
              $this->_flashMessenger->addMessage('Následující položky dosáhly kritické hodnoty: ' . implode(', ', $critical));
            }
          }
        } catch (Exception $e) {
          $this->view->message = $e->getMessage();
          $ok = false;
        }
      }
      if (!$ok) {
        $this->_helper->layout->setLayout('bootstrap-tobacco');

        $items = $recipes_table->getRecipeItems($recipe_id, $data['total']);

        $form->setItems($items);
        $form->setRecipeId($data['recipe']);

        $this->view->form = $form;
        $this->render('macerations-edit');
        return;
      }
    }
    $this->redirect('/tobacco/macerations');
  }

  public function macerationsStornoAction() {
    if ($this->_request->isPost()) {
      $params = $this->_request->getParams();
      try {
        $maceration = new Tobacco_Macerations_Maceration($params['batch_storno']);
        $maceration->storno($params['reason'], $params['amount_storno'], Meduse_Date::formToDb($params['date']));
        $this->_flashMessenger->addMessage('Storno macerátu proběhlo.');
      } catch (Exception $e){
        $this->_flashMessenger->addMessage('Storno se nezdařilo. ' . $e->getMessage());
      }
    }
    $this->_redirect('/tobacco/macerations');
  }

  /**
   * Vmichani maceratu do jineho sudu.
   */
  public function macerationsMergeAction() {
    $batch = $this->_request->getParam('batch');
    $form = new Tobacco_Macerations_MergeForm(array('batch' => $batch));
    if ($this->_request->isPost()) {
      if ($form->isValid($this->_request->getParams())) {
        $from = $this->_request->getParam('merge_from');
        $to = $this->_request->getParam('merge_to');
        $amount = (float) strtr($this->_request->getParam('merge_amount'), ',', '.');
        if (is_null($from) || is_null($to)) {
          throw new Tobacco_Macerations_Exceptions_NoBatchForMergeException();
        }
        $barrelFrom = new Tobacco_Macerations_Maceration($from);
        $barrelTo = new Tobacco_Macerations_Maceration($to);
        $barrelFrom->mergeTo($barrelTo, $amount);
        $this->redirect('tobacco/macerations');
      }
    }
    $this->view->form = $form;
  }

  public function macerationsCheckAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    if ($this->getRequest()->isPost()) {
      $batch = $this->getRequest()->getParam('batch');
      $amount = $this->getRequest()->getParam('amount');
      $amount = empty($amount) ?
        null : (float) str_replace(',', '.', $amount);;
      try {
        $maceration = new Tobacco_Macerations_Maceration($batch);
        $limit = $maceration->check($amount);
        if ($limit) {
          $this->_flashMessenger->addMessage('Množství je pod limitem');
        }
        $this->_flashMessenger->addMessage('Kontrola byla uložena');
      } catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
      }
    }
    $this->redirect('/tobacco/macerations/');
  }

  public function macerationsDetailAction() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
    if (!$id = $this->_request->getParam('id')) {
      if (!$batch = $this->_request->getParam('batch')) {
        $this->redirect('/tobacco/macerations');
      }
      else {
        $maceration = new Tobacco_Macerations_Maceration($batch);
        $this->redirect('/tobacco/macerations-detail/id/' . $maceration->getId());
      }
    }
    $maceration = new Tobacco_Macerations_Maceration($id);
    $ingredients = $maceration->getIngredients();
    $this->view->maceration = $maceration;
    $this->view->ingredients = $ingredients;

    $year = $this->getParam('year', date('Y'));
    $from = new Meduse_Date($year . '-01-01', 'Y-m-d');






    if ($snapshots = $maceration->getSnapshots($from)) {
      $this->view->snapshot = array_shift($snapshots);
    }
    else {
      $this->view->snapshot = NULL;
    }

    if ($snapshots = $maceration->getSnapshots()) {
      /** @see #176 */
      while ($snap = array_shift($snapshots)) {
        if (strpos($snap['note'], 'xxx') === FALSE) {
          $this->view->snapshot_last = $snap;
          break;
        }
      }
    }
    else {
      $this->view->snapshot_last = NULL;
    }
    $this->view->allow_fix = Zend_Registry::get('acl')->isAllowed('tobacco/macerations-snapshot-fix');

    // $this->view->historyAmountStart = $maceration->getWeight($from);

    /*
    if ($related = $maceration->getRelatedParts(FALSE)) {
      foreach ($related as $id => &$item) {
        $part = new Tobacco_Parts_Part($id);
        if ($amount = $part->getSales($from, NULL, TRUE)) {
          $item['sales_amount'] = $amount;
        }
        else {
          unset($related[$id]);
        }
      }
    }
    $this->view->related_parts = $related;
    */

    if ($year != date('Y')) {
      // Stav skladu ke konci roku
      $to = clone ($from);
      $to->addYear(1)->subDay(1);
    }
    else {
      // Stav nyní
      $to = new Meduse_Date(date('Y-m-d 23:59:59'), 'Y-m-d H:i:s');
    }
    $this->view->historyAmount = $maceration->getWeight($to);
    $this->view->year = $year;

    // Karta macerace nebude jen o danem maceratu, ale o vsech aktivnich sudech
    // v danem obdobi. Aktivni sud je ten, u ktereho doslo k nejakym zmenam.
    $totalWeightStart = 0;
    $totalWeightEnd = 0;
    $totalChanges = 0;
    $totalReal = NULL;

    $recipe = $maceration->getRecipe();
    $barrelIds = [];
    if ($barrels = Tobacco_Macerations::getMacerationsByRecipes($recipe->getId())) {
      foreach ($barrels as $idx => &$item) {
        $barrel = new Tobacco_Macerations_Maceration($item['id']);
        try {
          $ws = $barrel->getWeight($from);
          $wt = $barrel->getWeight($to);
        }
        catch (Zend_Date_Exception $e) {
        }
        $totalWeightStart += $ws;
        $totalWeightEnd += $wt;
        $changes = $barrel->getChanges($from, $to, TRUE);
        if ($changes['amount'] || $wt || $wt) {

          $item['changes'] = (float) $changes['amount'];
          $totalChanges += (float) $changes['amount'];
          $barrelIds[] = $item['id'];
          if ($lastSnap = $barrel->getSnapshots($to)) {
            $totalReal += $lastSnap[0]['amount_real'];
            $totalRealDate = $lastSnap[0]['date'];
          }
        }
        else {
          unset($barrels[$idx]);
        }
      }

      if ($changes = Tobacco_Macerations::getChanges($barrelIds, $from, $to)) {

        $pattern_part = '/(TA\d+|TK\d+)/i';
        $replacement_part = '<a href="' . $this->view->url(['action' => 'parts-detail', 'id' => NULL, 'batch' => NULL]) .  '/id/$1#changes">$1</a>';

        $pattern_batch = '/(L\d{6}\w\d{2})/i';
        $replacement_batch = '<a href="' . $this->view->url(['action' => 'macerations-detail', 'id' => NULL, 'batch' => NULL]) .  '/batch/$1#changes">$1</a>';

        foreach ($changes as &$item) {
          /*
          if ($item['type'] != Table_MacerationsHistory::TYPE_CREATE) {
            $totalChanges += (float) $item['amount'];
          }
          */
          $item['description'] = preg_replace($pattern_part, $replacement_part, $item['description']);
          $item['description'] = preg_replace($pattern_batch, $replacement_batch, $item['description']);
        }
      }
      $this->view->changes = $changes;
    }

    $totalWeightEnd = $totalWeightStart + $totalChanges < 0 ? 0 : $totalWeightStart + $totalChanges;

    $this->view->barrels = $barrels;
    $this->view->totalChanges = $totalChanges;
    $this->view->totalWeightStart = $totalWeightStart;
    $this->view->totalWeightEnd = $totalWeightEnd;
    $this->view->totalWeightReal = $totalReal;
    if (!is_null($totalReal)) {
      $this->view->totalWeightRealDate = new Meduse_Date($totalRealDate, 'Y-m-d H:i:s');
      $this->view->totalDiff = $totalWeightEnd - $totalReal;
      $this->view->totalDiffPerc = ($totalWeightEnd - $totalReal) / $totalReal * 100;
    }
    $this->view->recipeName = $recipe->getName();
    $this->view->title = $this->view->recipeName . ' – maceráty';
  }


  public function macerationsPackAction() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
    $batch = $this->getRequest()->getParam('batch');
    if (!$batch) {
      $this->_redirect('/tobacco/macerations');
    }
    try {
      $form = new Tobacco_Macerations_PackForm();
      $form->setMaceration($batch);
      $this->view->form = $form;
    } catch (Exception $e) {
      $this->_flashMessenger->addMessage($e->getMessage());
      $this->_redirect('/tobacco/macerations');
    }
  }

  public function macerationsPackPostAction() {
    if ($this->getRequest()->isPost()) {
      $params = $this->getRequest()->getParams();
      $form = new Tobacco_Macerations_PackForm();
      $items = $form->getParts($params);
      if ($items) {
        foreach ($items as $item) {
          if ($item['amount'] > 0) {
            $maceration = new Tobacco_Macerations_Maceration($item['batch']);
            $part = new Tobacco_Parts_Part($item['id_parts']);
            $this->db->beginTransaction();
            try {
              $maceration->pack($part, $item['amount'], $item['weight']);
              $this->_flashMessenger->addMessage('Položka ID = "' . $item['id_parts']
                . '" byla naskladněna.');
              $this->db->commit();
            } catch (Exception $e) {
              $this->_flashMessenger->addMessage('Položka ID = "' . $item['id_parts']
                . '" nebyla naskladněna. ' . $e->getMessage());
              $this->db->rollBack();
            }
          }
        }
      }
    }
    $this->_redirect('/tobacco/macerations');
  }

  public function ajaxSubpartsCountChangeAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $partId = $this->getRequest()->getParam('part');
    $subpartId = $this->getRequest()->getParam('subpart');
    $amount = (int) $this->getRequest()->getParam('amount');

    $tSubparts = new Table_PartsSubparts();
    $result = $tSubparts->update(array('amount' => $amount),
      array(
        'id_multipart LIKE "'. $partId . '"',
        'id_parts LIKE "' . $subpartId . '"',
      ));
    $this->_helper->json(array('result' => $result));
  }

  public function ajaxPartsMixSaveAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $partId = $this->getRequest()->getParam('id_parts');
    $mixId = $this->getRequest()->getParam('id_mixes');
    $amount = $this->getRequest()->getParam('amount');

    $table = new Table_PartsMixes();
    $mix = $table->getMix($partId);

    $result = null;
    if ($mix) {
      $result = $table->update(array(
        'id_mixes' => $mixId,
        'amount' => $amount,
      ), 'id_parts = "' . $partId . '"');
    } else {
      $result = $table->insert(array(
        'id_parts' => $partId,
        'id_mixes' => $mixId,
        'amount' => $amount,
      ));
    }
    $this->_helper->json(array('result' => $result));
  }

  public function ajaxMacerationsGetAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);
    $batch = $this->getRequest()->getParam('batch');
    $maceration = new Tobacco_Macerations_Maceration($batch);
    $data = array(
      'batch' => $maceration->__get('batch'),
      'amount' => $maceration->__get('weight'),
    );
    $this->_helper->json(array('result' => $data));
  }

  public function ajaxGetPartAmountAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $data = ['status' => NULL, 'amount' => NULL, 'error' => NULL];
    try {
      $date = new Meduse_Date($this->_request->getParam('date'), 'Ymd');
      $part = new Tobacco_Parts_Part($this->_request->getParam('id'));
      $amount = $part->getWHAmount($date);
      $data['amount'] = $part->getMeasure() == 'weight' ? $amount / 1000 : $amount;
      $data['status'] = TRUE;
    }
    catch (Exception $e) {
      $data['status'] = FALSE;
      $data['error'] = $e->getMessage();
    }
    $this->_helper->json($data);
  }

  public function ajaxGetMacerationWeightAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $data = ['status' => NULL, 'weight' => NULL, 'error' => NULL];
    try {
      $date = new Meduse_Date($this->_request->getParam('date'), 'Ymd');
      $maceration = new Tobacco_Macerations_Maceration($this->_request->getParam('id'));
      $data['weight'] = round($maceration->getWeight($date), 3);
      $data['status'] = TRUE;
    }
    catch (Exception $e) {
      $data['status'] = FALSE;
      $data['error'] = $e->getMessage();
    }
    $this->_helper->json($data);
  }

  public function partsSelectSubpartsAction() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
    $part = new Tobacco_Parts_Part($this->getRequest()->getParam('part'));
    $this->view->part = $part;

    $tParts = new Table_Parts();
    $subParts = $tParts->getSubpartsById($part->getID());
    $subPartsArray = array();
      foreach($subParts as $tmpPart){
        $subPartsArray[$tmpPart['id_parts']] = $tmpPart['amount'];
      }
    $this->view->subparts = $subPartsArray;


    $this->view->subpart = $part->getSubparts();
    $this->view->data = $part->getPossibleSubparts();

  }

  public function partsStampAction() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
    $partId = $this->_request->getParam('id');
    $orderId = $this->_request->getParam('order');
    $maxAmount = (int) $this->_request->getParam('amount', PHP_INT_MAX);
    if ($partId) {

      $part = new Tobacco_Parts_Part($partId);
      $supParts = $part->getSupParts();
      $batches = $part->getBatches();

      $form = new Tobacco_Parts_StampForm();
      $form->setPartId($partId);
      $form->setSupParts($supParts);
      $form->setBatches($batches, $maxAmount);
      $form->setRequestedAmount($maxAmount);

      if ($orderId) {
        $form->setOrderId($orderId);
      }

      $this->view->form = $form;

    } else {
      if ($orderId) {
        $this->_redirect('/tobacco-orders/prepare/id/' . $orderId);
      } else {
        $this->_redirect('/tobacco/warehouse');
      }
    }
  }

  public function partsStampPostAction() {
    if ($this->getRequest()->isPost()) {

      //získávání informací
      $params = $this->getRequest()->getParams();
      $partId = $params['id_multipart'];
      $subPartId = $params['id_part'];

      //získání ID objednávky (pokud není kolkováno přes sklad)
      $orderId = $this->_request->getParam('id_order');

      //určení následného přesměrování (v závislosti na kolkování v objednávce nebo ve skladu)
      $redirectUrl = $orderId ?
          'tobacco-orders/prepare/id/' . $orderId : 'tobacco/warehouse';

      $part = new Tobacco_Parts_Part($partId);
      $toStamp = array();
      $totalAmount = 0;

      foreach ($params as $key => $value) {
        if (substr($key, 0, 12) == 'batch_number') {
          $idx = substr($key, 13);
          $batch = $params['batch_number_' . $idx];
          $amount = (int) $params['batch_amount_' . $idx];
          $toStamp[] = array(
            'part' => $subPartId,
            'multipart' => $partId,
            'batch' => $batch,
            'amount' => $amount,
            );
        }
      }
      if ($toStamp) {
        $tWarehouse = new Table_TobaccoWarehouse();
        $this->db->beginTransaction();
        try {
          foreach ($toStamp as $row) {
            $tWarehouse->stamp(
              $row['part'], $row['multipart'], $row['batch'], $row['amount']);
              $totalAmount += $row['amount'];
          }
          $this->_flashMessenger->addMessage('Tabák ' . $part->getName()
            . ' byl okolkován. ');
          $this->db->commit();
        } catch (Exception $e) {
          $this->_flashMessenger->addMessage('Tabák nebyl okolkován. '
            . $e->getMessage());
          $this->db->rollBack();
          $this->_redirect($redirectUrl);
        }
      }
    }

    // pokud kolkujeme primo v objednavce musime nekolkovane produkty
    // odstranit a kolkovane do objednavky pridat
    if ($orderId) {

      $order = new Tobacco_Orders_Order($orderId);
      $order->cancelAllPrepared($subPartId);
      #$order->cancelAllPrepared($partId);
      $order->removeProduct($subPartId);

      $order->addProduct($partId, $totalAmount);
      if ($totalAmount<(int)$params['req_amount']) {
        $this->_flashMessenger->addMessage('Nedostatek neokolkovaného zboží na skladě. Okolkování neúplné.');
        $order->addProduct($subPartId, (int)$params['req_amount']-$totalAmount);
      }
    }

    // kontrola limitního množství součástí
    $tSubparts = new Table_PartsSubparts();
    $parts_amount = $tSubparts->selectSubpartsAmount($partId)->query()->fetchAll();
    $parts_message = array();
    foreach ($parts_amount as $part_amount) {
      if ($part_amount['amount_warehouse']<=$part_amount['amount_critical']) {
        $parts_message[] = $part_amount['part'] . ' (' . $part_amount['amount_warehouse'] . ')';
      }
    }

    if ($parts_message) {
      $this->_flashMessenger->addMessage('Následující součásti dosáhly limitního množství: ' . implode(', ', $parts_message));
    }

    $this->_redirect($redirectUrl);
  }



  public function deleteSubpartAction() {
    $params = $this->getRequest()->getParams();
    $part = new Tobacco_Parts_Part($params['part']);
    try {
      $part->deleteSubPart($params['subpart']);
      $this->_flashMessenger->addMessage('Podsoučást "' . $params['subpart']
        . '" byla odebrána.');
    } catch (Exception $e) {
      $this->_flashMessenger->addMessage('Odebrání podsoučásti "'
        . $params['subpart'] . '" se nezdařilo.');
    }
    $this->_redirect('/tobacco/parts-detail/id/' . $params['part'] . '#subparts');
  }

  public function recipesAction() {
	  $this->_helper->layout->setLayout('bootstrap-tobacco');
	  $this->view->title = "Receptury";

	  $tRecipes = new Table_Recipes();
	  $params = $this->getRequest()->getParams();
	  $this->view->grid_active = $tRecipes->getRecipesGrid($params + array('status' => Table_Recipes::STATE_ACTIVE));
    $this->view->grid_inactive = $tRecipes->getRecipesGrid($params + array('status' => Table_Recipes::STATE_INACTIVE));
  }

  public function recipeDetailAction() {
	  $this->_helper->layout->setLayout('bootstrap-tobacco');
	  $this->view->title = "Detail receptury";

	  $id = $this->getRequest()->getParam('id');
	  $mapper = new Table_Recipes();

    $recipe = new Tobacco_Recipes_Recipe($id);
    $this->view->recipe = $recipe;

    $recipe_parts = new Tobacco_Recipes_Parts($mapper->getRecipeParts($id));
    $this->view->recipe_parts = $recipe_parts;
  }

  public function recipeAddAction() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
    $this->recipeEditAction();
  }

  public function recipeEditAction() {
	  $this->_helper->layout->setLayout('bootstrap-tobacco');
	  $form = new Tobacco_Recipes_EditForm();
	  $id = $this->_request->getParam('id');
    $redirect = is_null($id);

	  if ($this->_request->isPost()) {

			if ($form->isValid($this->_request->getPost())) {
        $data = $form->getValidValues($this->_request->getParams());
        try {
          $recipe = new Tobacco_Recipes_Recipe($id);
          $id = $recipe->set($data)->getId();
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
          $this->redirect($this->view->url());
        }
				$this->view->success = true;
        if ($redirect) {
          $this->redirect($this->view->url(array('id' => $id, 'action' => 'recipe-edit')));
        }
			}
	  }

	  if ($id) {
      $this->view->title = "Editovat recepturu";
      $recipe = new Tobacco_Recipes_Recipe($id);

      $arr = $recipe->toArray();
      $form->populate($arr);
      $this->view->recipe = $recipe;
    }
    else {
      $this->view->title = "Vložit novou recepturu";
    }
	  $this->view->form = $form;

  }

  public function recipeSaveAsAction() {

    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $recipeId = $this->_request->getParam('id');

    if (! $recipeId) {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID zdrojové receptury.');
      $this->redirect('/tobacco/recipes');
    }

    $recipe = new Tobacco_Recipes_Recipe($recipeId);
    $newRecipe = $recipe->saveAs();

    $this->_flashMessenger->addMessage('Byla vytvořena ' . $newRecipe->getName());
    $this->redirect('/tobacco/recipe-detail/id/' . $newRecipe->getId());

  }

  public function recipeDeleteAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

	  $tRecipes = new Table_Recipes();

	  $id = $this->getRequest()->getParam('id');
	  $where = $tRecipes->getAdapter()->quoteInto('id = ?', $id);
	  $tRecipes->update(array('state' => 'hidden'), $where);

	  $this->_helper->redirector('recipes', 'tobacco');
  }

  public function ajaxRecipePartsAction() {

	  $this->_helper->layout->disableLayout();
	  $recipe_id = (int) $this->_request->getParam('id');
	  if (!$recipe_id) {
	    $this->view->new = TRUE;
    }
    else {
      $message = NULL;
      $recipe = new Tobacco_Recipes_Recipe($recipe_id);
      $form = new Tobacco_Recipes_PartForm(array(
        'excluded' => $recipe->getParts(),
      ));
      try {
        // add part
        $validValues = $form->getValidValues($this->_request->getParams());
        $part_id = $validValues['part_id'] ?? NULL;
        $percent = $validValues['percent'] ?? NULL;
        if ($part_id && $percent > 0) {
          $recipe->addPart($part_id, $percent);
        }
        // remove part
        $delete_id = (int) $this->_request->getParam('delete');
        if ($delete_id) {
          $recipe->removePart($delete_id);
        }
      }
      catch (Exception $e) {
        $message = $e->getMessage();
      }

      if ($recipe->checkFlavor() === FALSE) {
        $message = 'Pozor! Příchuť jednosložkového receptu neodpovídá složce příchuti.';
      }

      $this->view->message = $message;
      $this->view->recipe_parts = $recipe->getParts();
      $this->view->form = $form;
    }
  }

  public function reportsMacerationsAction() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
	  $this->view->title = "Report – macerace";
    $tMacaretions = new Table_Macerations();
    $this->view->totalWeight = $tMacaretions->getTotalWeight();
    $this->view->barrelsCount = $tMacaretions->getBarrelsCount();
    $this->view->recipes = $tMacaretions->getBarrelsByRecipes();
    $this->view->barrels = $tMacaretions->getBarrels();
  }

  public function reportsProductsAction() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
	  $this->view->title = "Report - produkty";
    $tWH = new Table_TobaccoWarehouse();
    $this->view->overviewNFS = $tWH->getProductsOverviewNFS();
    $this->view->overviewFS = $tWH->getProductsOverviewFS();
    $this->view->recipesNFS = $tWH->getProductsByMixNFS();
    $this->view->recipesFS = $tWH->getProductsByMixFS();
    $this->view->productsNFS = $tWH->getProductsReportNFS();
    $this->view->productsFS = $tWH->getProductsReportFS();
  }


    public function partsDeleteAction(){
      if (!$this->_request->getParam('id')) {
        $this->redirect('tobacco/warehouse');
      }
      $affectedOrders = [];
      $part = new Tobacco_Parts_Part($this->_request->getParam('id'));
      try {
        $affectedOrders = $part->delete();
      }
      catch (Parts_Exceptions_InReservations $e) {
        $amounts = $part->getReservationsAmount();
        $ordersIds = array_keys($amounts);
        $orders = array();
        foreach ($ordersIds as $id) {
          $order = new Orders_Order($id);
          $orders[] = '<a href="/tobacco-orders/prepare/id/' . $order->getID() . '">'
            . 'obj. č.' . $order->getNO() . ' – ' . $order->getTitle() . '</a>';
        }
        $msg = "Položku skladu " . $part->getID()
          . " se nepodařilo odstranit, protože je rezervovaná v těchto objednávkách:<br>"
          . implode('<br>', $orders);
        $this->_flashMessenger->addMessage($msg);
        $this->redirect('tobacco/parts-detail/id/' . $part->getID());
      }
      catch (Parts_Exceptions_OnStock $e) {
        $msg = "Položku skladu ID = " . $part->getID()
          . " se nepodařilo odstranit, protože je stále na skladě.";
        $this->_flashMessenger->addMessage($msg);
        $this->redirect('tobacco/parts-detail/id/' . $part->getID());
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage("Položku skladu ID = " . $part->getID()
          . " se nepodarilo odstranit. " . $e->getMessage());
        $this->redirect('tobacco/parts-detail/id/' . $part->getID());
      }
      $this->_flashMessenger->addMessage("Položka skladu ID = " . $part->getID()
        . " byla ostraněna.");
      if ($affectedOrders) {
        $message = 'Zároveň byl tento produkt odebrán z následujících objednávek: <ul>';
        foreach ($affectedOrders as $item) {
          $url = $this->view->url([
            'controller' => 'orders',
            'action' => 'detail',
            'id' => $item['id']
          ]);
          $message .= "<li><a href='{$url}'>#{$item['no']}</a>: {$item['title']}</li>";
        }
        $message .= '</ul>';
        $this->_flashMessenger->addMessage($message);
      }
      $this->redirect('tobacco/warehouse');
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
    }
    public function exportAction() {

      $this->view->title = "Export";

      $path = Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH;

      if ($this->_request->getParam('download')) {
        $this->view->download = $path . '/' . $this->_request->getParam('download');
      }

      $dir = dir($path);
      $entries = array();
      while (FALSE !== ($entry = $dir->read())) {
        if ($entry != '.' && $entry != '..') {
          $filepath = $path . $entry;
          $time = filemtime($filepath);
          if (key_exists($time, $entries)) {
            $time++;
          }
          $entries[$time] = array(
            'name' => $entry,
            'size' => $this->human_filesize(filesize($filepath)),
            'time' => $time,
          );
        }
      }
      ksort($entries);
      $this->view->entries = array_reverse($entries);
    }

  /**
   * @throws Parts_Exceptions_BadWH
   * @throws PHPExcel_Exception
   * @throws PHPExcel_Writer_Exception
   * @throws Zend_Exception
   */
  public function exportGenerateAction() {
      $this->_helper->viewRenderer->setNoRender();
      $this->_helper->layout->disableLayout();
      $objPHPExcel = Reports::generateCompleteTobaccoExport();
      $filename = "export_medite_".date('d-m-Y') . ".xlsx";
      $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
      $objWriter->setOffice2003Compatibility(TRUE);
      $objWriter->save(Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH . $filename);
      $this->redirect($this->view->url(array('action' => 'export-download', 'filename' => $filename)));
    }

  public function exportDownloadAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    $ext = $this->_request->getParam('ext', '');
    $filename = $this->_request->getParam('filename') . ($ext ? '.' . $ext : '');
    if (!empty($filename)) {
      $filepath = Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH . $filename;
      $fp = fopen($filepath, 'rb');
      $this->_response->clearAllHeaders();
      $this->_response->setHeader('Content-Description', 'File Transfer');
      $this->_response->setHeader('Expires','0',true);
      $this->_response->setHeader('Pragma','public',true);
      $this->_response->setHeader('Cache-Control','must-revalidate, post-check=0,pre-check=0', true);
      $tokens = explode('.', $filename);
      switch (array_pop($tokens)) {
        case 'xls':  $type = 'application/vnd.ms-excel'; break;
        case 'xlsx': $type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'; break;
        case 'pdf': $type = 'application/pdf'; break;
        default: $type = '';
      }
      if ($type) {
        $this->_response->setHeader('Content-Type', $type);
      }
      $this->_response->setHeader('Content-Length',  filesize($filepath));
      $this->_response->setHeader('Content-Disposition', 'attachment; filename="' . $filename, true);
      fpassthru($fp);
      fclose($fp);

      if($this->_request->getParam('unlink') == 1) {
        unlink($filepath);
      }
    }
  }

    public function mixesAction() {
      $this->_helper->layout->setLayout('bootstrap-tobacco');
      $this->view->title = "Popisy mixů";

      $tMixes = new Table_Mixes();
      $params = $this->getRequest()->getParams();

      $this->view->grid_active = $tMixes->getDataGrid($params + array('status' => Table_Mixes::STATE_ACTIVE));
      $this->view->grid_inactive = $tMixes->getDataGrid($params + array('status' => Table_Mixes::STATE_INACTIVE));
    }

    public function mixesAddAction() {
      $this->_helper->layout->setLayout('bootstrap-tobacco');
      $this->view->title = "Přidat mix";

      $request = $this->getRequest();
      $form = new Tobacco_Mixes_AddForm();

      if ($request->isPost()) {
        if ($form->isValid($request->getPost())) {
          $data = array(
            'name' => $form->getValue('name'),
            'id_flavor' => $form->getValue('flavor_id'),
            'create' => new Zend_Db_Expr('NOW()'),
          );
          $tMixes = new Table_Mixes();
          $id = $tMixes->insert($data);
          $params = array('id' => $id);
          $this->_helper->redirector('mixes-edit', 'tobacco', null, $params);
        }
      }

      $this->view->form = $form;
    }

    public function mixesDetailAction() {
      $year = $this->_request->getParam('year', date('Y'));
      $from = new Meduse_Date($year . '-01-01');
      $to = clone $from;
      $to->addYear(1);
      $this->_helper->layout->setLayout('bootstrap-tobacco');
      $id = $this->getRequest()->getParam('id');
      $mix = new Tobacco_Mixes_Mix($id);

      $relatedIds = [];
      $relatedTotalAmount = 0;
      $relatedStockAmountStart = 0;
      $relatedStockAmountEnd = 0;
      $relatedTotalWeight = 0;
      $relatedStockWeightStart = 0;
      $relatedStockWeightEnd = 0;
      if ($related = $mix->getRelatedParts(FALSE)) {
        foreach ($related as $id => &$item) {
          $part = new Tobacco_Parts_Part($id);
          $amount = (int) $part->getSales($from, NULL, TRUE);
          $weight = $part->getTobbacoAmount();
          $tobaccoAmount = (float) $amount * $weight;
          $onStockStart = (int) $part->getWHAmount($from);
          $onStockStartWeight = (float) $onStockStart * $weight;
          $onStockEnd = (int) $part->getWHAmount($to);
          $onStockEndWeight = (float) $onStockEnd * $weight;
          $item['sales_amount'] = $amount;
          $item['tobacco_amount'] = (float) $tobaccoAmount;
          $item['stock_amount_start'] = $onStockStart;
          $item['stock_weight_start'] = $onStockStartWeight;
          $item['stock_amount_end'] = $onStockEnd;
          $item['stock_weight_end'] = $onStockEndWeight;
          $relatedStockAmountStart += $onStockStart;
          $relatedStockWeightStart += $onStockStartWeight;
          $relatedTotalAmount += $amount;
          $relatedStockAmountEnd += $onStockEnd;
          $relatedTotalWeight += $tobaccoAmount;
          $relatedStockWeightEnd += $onStockEndWeight;
          $relatedIds[] = $id;
        }
      }

      $totalChanges = 0;
      $totalWeightStart = 0;
      $totalWeightEnd = 0;
      $macerations = [];
      if ($relatedIds && $macerations = $mix->getMacerations()) {
        foreach ($macerations as $idx => &$item) {
          $maceration = new Tobacco_Macerations_Maceration($item['id']);
          $ws = $maceration->getWeight($from);
          $wt = $maceration->getWeight($to);
          $totalWeightStart += $ws;
          $totalWeightEnd += $wt;
          $changes = $maceration->getChanges($from, $to, TRUE, $relatedIds);
          if ($changes['amount'] || $wt || $wt) {
            $item['changes'] = (float) $changes['amount'];
            $totalChanges += (float) $changes['amount'];
          }
          else {
            unset($macerations[$idx]);
          }
        }
      }

      $diff_weight = $totalChanges + $relatedTotalWeight + $relatedStockWeightEnd - $relatedStockWeightStart;
      $diff_percent = $relatedTotalWeight + $relatedStockWeightEnd == 0 ? 0 : (1 + ($totalChanges / ($relatedTotalWeight + $relatedStockWeightEnd - $relatedStockWeightStart))) * 100;

      // roky pro select
      $years = [];
      $created = new Meduse_Date($mix->getCreated());
      $min = (int) $created->toString('Y');
      $now = (int) date('Y');
      while($now >= $min) {
        $years[] = $now--;
      }

      // data pro pohled
      $this->view->mix = $mix;
      $this->view->year = $year;
      $this->view->years = $years;
      $this->view->macerations = $macerations;
      $this->view->totalChanges = $totalChanges;
      $this->view->totalWeightStart = $totalWeightStart;
      $this->view->totalWeightEnd = $totalWeightEnd;
      $this->view->totalWeightDiff = $totalWeightStart - $totalWeightEnd + $totalChanges;
      $this->view->totalWeightDiffPercent = $totalChanges == 0 ? 0 : (1 + (($totalWeightStart - $totalWeightEnd) / $totalChanges)) * 100;
      $this->view->relatedParts = $related;
      $this->view->relatedTotalAmount = $relatedTotalAmount;
      $this->view->relatedTotalWeight = $relatedTotalWeight;
      $this->view->relatedStockAmountStart = $relatedStockAmountStart;
      $this->view->relatedStockWeightStart = $relatedStockWeightStart;
      $this->view->relatedStockAmountEnd = $relatedStockAmountEnd;
      $this->view->relatedStockWeightEnd = $relatedStockWeightEnd;
      $this->view->diff_weight = $diff_weight;
      $this->view->diff_percent = $diff_percent;

      $this->view->unit_price = $mix->getUnitPrice();

      $this->view->title = $mix->getName() . ' – mix';
    }

    public function mixesEditAction() {
      $this->_helper->layout->setLayout('bootstrap-tobacco');
      $this->view->title = "Editovat mix";

      $tMixes = new Table_Mixes();
      $request = $this->getRequest();
      $form = new Tobacco_Mixes_EditForm();

      $id = $request->getParam('id');

      if ($request->isPost()) {
        if ($form->isValid($request->getPost())) {
          $data = array(
            'name' => $form->getValue('name'),
            'id_flavor' => $form->getValue('id_flavor'),
            'description' => $form->getValue('description'),
            'state' => $form->getValue('state')
          );

          $where = $tMixes->getAdapter()->quoteInto('id = ?', $id);
          $tMixes->update($data, $where);
          $this->view->success = true;
        }
      }

      $mix = $tMixes->find($id)->current()->toArray();
      $form->populate($mix);

      $this->view->mix = $mix;
      $this->view->form = $form;
    }

    public function mixesDeleteAction() {
      $table = new Table_Mixes();
      $id = $this->getRequest()->getParam('id');
      $where = $table->getAdapter()->quoteInto('id = ?', $id);
      $table->update(array('state' => 'hidden'), $where);

      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $this->_helper->redirector('mixes', 'tobacco');
    }

    public function ajaxMixesRecipesAction() {
      $this->_helper->layout->disableLayout();

      $form = new Tobacco_Mixes_RecipesForm();
      $request = $this->getRequest();
      $mapper = new Table_Mixes();

      // edit, delete
      $mix_id = (int)$request->getParam('id');
      $recipe_id = $request->getParam('recipe_id');
      $ratio = (int) $request->getParam('ratio');

      if ($ratio > 0) {
        $mapper->insertRecipe($mix_id, $recipe_id, $ratio);
      }

      $delete_id = (int) $request->getParam('delete');

      if (isset($delete_id)) {
        $mapper->deletePart($delete_id);
      }

      $mix_recipes = $mapper->getMixRecipes($mix_id);
      $this->view->mix_recipes = $mix_recipes;
      $this->view->form = $form;
    }

    public function partsPackAction() {
      $this->_helper->layout->setLayout('bootstrap-tobacco');
      $id = $this->getRequest()->getParam('id');
      $options['part_id'] = $id;
      $part = new Tobacco_Parts_Part($id);
      try {
        $mix = $part->getMix();
        $mixObj = new Tobacco_Mixes_Mix($mix['id_mixes']);
        $ingredients = $mixObj->getIngredients(true);
        $tMacerations = new Table_Macerations();
        foreach ($ingredients as $key => $row) {
          $ingredients[$key]['barrels'] = $tMacerations->getBarrels($row['id_recipes']);
        }
        $options['ingredients'] = $ingredients;
        $options['weight'] = $mix['amount'];
        $form = new Tobacco_Parts_PackForm();
        $form->init($options);
        $this->view->form = $form;
      }
      catch (Tobacco_Parts_Exceptions_NoContentException $e) {
        $this->_flashMessenger->addMessage('Produkt nelze nabalit, nemá definovaný obsah.');
        $this->_redirect($this->view->url(array('action' => 'parts-detail')));
      }
    }

    public function partsPackProcessAction() {
      $this->_helper->layout->disableLayout();
      $request = $this->getRequest();
      if ($request->isPost()) {
        $partId = $this->getRequest()->getParam('part_id');
        $amount = (int) $this->getRequest()->getParam('amount');
        $barrels = array();
        $count = (int) $this->getRequest()->getParam('count');
        for ($it = 0; $it < $count; $it++) {
          $barrels[] = $this->getRequest()->getParam('barrels_' . $it);
        }

        $this->db->beginTransaction();
        try {
          $part = new Tobacco_Parts_Part($partId);
          $critical = $part->pack($amount, $barrels);
          $this->db->commit();

          //limitní množství
          if (!empty($critical['batches'])) {
            $this->_flashMessenger->addMessage('Některé maceráty dosáhly limitního množství: ' . implode(', ', $critical['batches']));
          }
          if (!empty($critical['subparts'])) {
            $this->_flashMessenger->addMessage('Některé součásti dosáhly limitního množství: ' . implode(', ', $critical['subparts']));
          }
        } catch (Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
          $this->db->rollBack();
        }
      }
      $this->redirect('/tobacco/warehouse');
    }

    public function partsPackCheckAjaxAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

      $partId = $this->getRequest()->getParam('part');
      $amount = (int) $this->getRequest()->getParam('amount');
      $barrels = $this->getRequest()->getParam('barrels');
      $max = $amount;

      $part = new Tobacco_Parts_Part($partId);
      $status = $part->checkMix($max, $barrels);

      $data = array(
        'part' => $partId,
        'amount' => $amount,
        'barrels' => $barrels,
        'max' => $max,
        'status' => $status,
      );
      $this->_helper->json(array('result' => $data));
    }

    public function partsBreakAction() {

      if (!$partId = $this->_request->getParam('part')) {
        $this->_flashMessenger->addMessage('Chybí ID součásti k rozkladu.');
        $this->redirect('tobacco/warehouse');
      }

      try {
        // Sestaveni formulare.
        $form = new Tobacco_Parts_BreakForm();
        $form->appendBatches($partId);
        $form->setLegend('Rozložení součásti ' . $partId . ' zpátky na sklad');
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
        $this->redirect('tobacco/parts-detail/id/' . $partId);
      }

      if ($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())) {
        // Kolekce parametru pro rozklad.
        $params = $this->_request->getParams();
        $batches = [];
        $barrels = [];
        $totalAmount = 0;
        foreach ($params as $key => $value) {
          if (substr($key, 0, 12) == 'batch_number') {
            $idx = substr($key, 13);
            $batch = $params['batch_number_' . $idx];
            $amount = (int) $params['batch_amount_' . $idx];
            $batches[] = array('batch' => $batch, 'amount' => $amount);
            $totalAmount += $amount;
          }
          if (substr($key, 0, 6) == 'barrel') {
            $idx = substr($key, 7);
            $barrel = $params['barrel_' . $idx];
            $ratio = (int) $params['ratio_' . $idx];
            $barrels[] = array('id' => $barrel, 'ratio' => $ratio);
          }
        }
        $nfstowh = $this->_request->getParam('nfstowh', 'n') == 'y';
        try {
          // Pokus o rozklad.
          $part = new Tobacco_Parts_Part($partId);
          $list = $part->disassemble($batches, $barrels, $nfstowh);
          // Info o rozkladu.
          $act = new Table_Actualities();
          $act->add("Rozložil " . $totalAmount . " ks součásti " . $partId);
          $msg = 'Rozloženo ' . $totalAmount . ' ks ' . $partId . '.';
          if ($list) {
            $msg .= '<br>Vloženo: <ul>';
            foreach ($list as $idx => $amount) {
              $msg .= '<li>' . $idx . ' ' . $amount . '</li>';
            }
            $msg .= '</ul>';
          }
          $this->_flashMessenger->addMessage($msg);
        }
        catch (Exception $e) {
          // Neuspech.
          $this->_flashMessenger->addMessage('Rozložení se nezdařilo: ' . $e->getMessage());
        }
        $this->redirect('tobacco/parts-detail/id/' . $partId);
    }
    $this->view->form = $form;
    $this->render('parts-add');
  }

  public function partsTrashAction() {
    $this->view->title = "Koš součástí";
    $data = Parts::getTrashData(Parts_Part::WH_TYPE_TOBACCO);
    $this->view->data = $data;
  }

  public function productionsAction() {
    $this->view->title = "Přehled skladových objednávek";
    $data = [
      'stamps' => [
        'name' => 'Kolky',
        'production' => Productions::getProductionsRows(Table_TobaccoWarehouse::WH_TYPE, NULL, [
          Table_TobaccoWarehouse::CTG_DUTY_STAMPS,
        ]),
      ],
      'materials' => [
        'name' => 'Suroviny',
        'production' => Productions::getProductionsRows(Table_TobaccoWarehouse::WH_TYPE, NULL, [
          Table_TobaccoWarehouse::CTG_MATERIALS_TOBACCO,
          Table_TobaccoWarehouse::CTG_MATERIALS_FLAVORS,
          Table_TobaccoWarehouse::CTG_MATERIALS_OTHER,
        ]),
      ],
      'packaging' => [
        'name' => 'Obalový materiál',
        'production' => Productions::getProductionsRows(Table_TobaccoWarehouse::WH_TYPE, NULL, [
          Table_TobaccoWarehouse::CTG_PACKAGING,
        ]),
      ],
      'accessories' => [
        'name' => 'Doplňkové zboží',
        'production' => Productions::getProductionsRows(Table_TobaccoWarehouse::WH_TYPE, NULL, [
          Table_TobaccoWarehouse::CTG_ACCESSORIES,
        ]),
      ],
    ];
    $this->view->data = $data;
  }


  public function productionsAddAction() {
    if ($id = $this->_request->getParam('id')) {
      $this->view->title = 'Úprava objednávka materiálu';
      $production = new Tobacco_Productions_Production($id);
      $id_producers = $production->producer->getId();
      $params = array(
        'id' => $id,
        'id_producers' => $production->producer->getId(),
        'date_ordering' => $production->date_ordering->get('d.m.Y'),
      );
      foreach ($production->parts as $partId => $item) {
        $params['part_' . $partId] = $item['amount'];
      }
    }
    else {
      $this->view->title = 'Nová objednávka materiálu';
      $id_producers = $this->_request->getParam('id_producers');
      $params = $this->_request->getParams();
    }
    $form = new Tobacco_Productions_ProductionForm();
    if ($id_producers) {
      $form->addParts($id_producers);
    }
    $form->setLegend($this->view->title);
    $form->populate($params);
    $this->view->form = $form;
  }

  public function productionsSaveAction() {
    if (!$this->_request->isPost()) {
      $this->redirect('/tobacco/productions-add');
    }
    $id_producers = $this->_request->getParam('id_producers');
    $form = new Tobacco_Productions_ProductionForm();
    if ($id_producers) {
      $form->addParts($id_producers);
    }
    $params = $this->_request->getParams();
    if (!$form->isValid($params)) {
      $this->productionsAddAction();
      $this->render('productions-add');
      return;
    }
    $values = $form->getValidValues($params);
    try {
      if ($id = $this->_request->getParam('id')) {
       $production = new Tobacco_Productions_Production($id);
       $production->update($values);
      }
      else {
        $production = Productions::create(Table_TobaccoWarehouse::WH_TYPE, $values);
      }
      $name = $production->producer->getName();
      $this->_flashMessenger->addMessage('Objednávka materiálu od ' . $name .  ' byla uložena.');
      $this->redirect('/tobacco/productions-detail/id/' . $production->id);
    }
    catch (Exception $e) {
      $this->_flashMessenger->addMessage('Objednávka materiálu nebyla uložena: ' . $e->getMessage());
      $this->redirect('/tobacco/productions');
    }

  }

  public function productionsDetailAction() {
    $id = $this->_request->getParam('id');
    $production = new Tobacco_Productions_Production($id);
    $name = $production->producer->getName();
    $date = $production->date_ordering->get('d.m.Y');
    $this->view->title = "Objednávka materiálu $name – $date";
    $this->view->production = $production;
    $this->view->status = $production->status == Table_Productions::STATUS_CLOSED ? 'přijatá' : 'otevřená';
    $this->view->status_label = $production->status == Table_Productions::STATUS_CLOSED ? 'label label-success' : 'label label-important';
    $this->view->invoices = $production->getInvoices();
    $this->view->notes = $production->getNotes();
    $form = new Tobacco_Productions_ProductionDeliveryForm();
    $form->setProductionId($id);
    $this->view->form = $form;
  }

  public function productionsDeliveryAction() {
    if (!$this->_request->isPost()) {
      $this->redirect('/tobacco/productions');
    }
    $params = $this->_request->getParams();
    $form = new Tobacco_Productions_ProductionDeliveryForm();
    if ($form->isValid($params)) {
      $values = $form->getValidValues($params);
      $production = new Tobacco_Productions_Production($values['id']);
      try {
        $message = $production->confirmDeliveryAll($values['date_delivery'], $values['description'], $values['invoice_id']);
        $this->_flashMessenger->addMessage('Přijetí objednávky bylo potvrzeno.');
        if ($message) {
          $this->_flashMessenger->addMessage($message);
        }
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage('Přijetí objednávky se nezdařilo: ' . $e->getMessage());
      }
      $this->redirect('/tobacco/productions-detail/id/' . $values['id']);
    }
    else {
      $this->productionsDetailAction();
      $this->render('productions-detail');
      return;
    }
  }

  public function warehouseSnapshotAddAction() {
    $params = $this->_request->getParams();
    $part = new Tobacco_Parts_Part($params['id']);
    $date = new Meduse_Date($this->_request->getParam('date'), 'Y-m-d');
    $form = new Tobacco_Parts_SnapshotForm();
    $form->setPart($part);
    $form->setDate($date);
    if (in_array($this->_request->getParam('type'), ['invent', 'ajax'])) {
      $form->setNote('Inventura ' . $date->get('d/m/Y'));
      $form->removeFix();
    }
    $type = $this->_request->getParam('type', 'default');
    if ($this->_request->isPost()) {
      $wh = new Tobacco_Warehouse();
      $date = new Meduse_Date($params['date'], 'd.m.Y');
      $id_snapshot = $this->_request->getParam('snapshot_id', NULL);
      if ($id = $wh->setSnapshot($part, $params['amount_real'], $date, $params['amount_wh'], $params['note'], $id_snapshot)) {
        if ($params['fix'] == 'y') {
          if ($wh->fixSnapshot($id)) {
            $this->_flashMessenger->addMessage('Stav skladu byl nastaven dle skutečnosti.');
          }
        }
        if ($this->_request->getParam('type') == 'invent') {
          $this->_flashMessenger->addMessage('Stav byl zaznamenán.');
          $this->redirect('/tobacco/close-year/type/parts/year/' . $date->get('Y') . '#' . $params['id']);
        }
        elseif ($type == 'ajax') {
          $this->_helper->layout()->disableLayout();
          $this->_helper->viewRenderer->setNoRender(TRUE);
          $data = [
            'part_id' => $params['id'],
            'status' => TRUE,
            'snapshot_id' => $id,
            'amount_real' => $params['amount_real'],
            'amount_wh' => $params['amount_wh'],
            'date' => $params['date'],
          ];
          $this->_helper->json($data);
        }
        else {
          $this->_flashMessenger->addMessage('Stav byl zaznamenán.');
          $this->redirect('tobacco/warehouse-snapshot#' . $date->get('Y'));
        }
      }
      else {
        if ($this->_request->getParam('type') == 'ajax') {
          $this->_helper->layout()->disableLayout();
          $this->_helper->viewRenderer->setNoRender(TRUE);
          $this->_helper->json(['status' => FALSE]);
        }
        else {
          $this->_flashMessenger->addMessage('Nastala chyba, stav nebyl zaznamenán.');
          $this->redirect($this->view->url());
        }
      }
    }

    $this->view->form = $form;
    $this->view->part = $part;
  }

  public function warehouseSnapshotAction() {
    $wh = new Tobacco_Warehouse();
    $this->view->title = 'Fyzický stav skladu';
    $this->view->allow_fix = Zend_Registry::get('acl')->isAllowed('tobacco/warehouse-snapshot-fix');
    $snapshots = [];
    if ($years = $wh->getSnapshotsYears()) {
      foreach ($years as $year) {
        $from = new Meduse_Date($year . '-01-01 00:00:00', 'Y-m-d H:i:s');
        $to = new Meduse_Date($year . '-12-31 23:59:59', 'Y-m-d H:i:s');
        $snapshots[$year] = $wh->getSnapshots(NULL, $from, $to, 'ASC');
      }
    }
    $this->view->years = $years;
    $this->view->snapshots = $snapshots;
  }

  public function macerationsSnapshotAction() {
    $this->view->title = 'Fyzický stav macerace';
    $this->view->allow_fix = Zend_Registry::get('acl')->isAllowed('tobacco/macerations-snapshot-fix');
    $snapshots = [];
    if ($years = Tobacco_Macerations::getSnapshotsYears()) {
      foreach ($years as $year) {
        $from = new Meduse_Date($year . '-01-01 00:00:00', 'Y-m-d H:i:s');
        $to = new Meduse_Date($year . '-12-31 23:59:59', 'Y-m-d H:i:s');
        $snapshots[$year] = Tobacco_Macerations::getSnapshots(NULL, $from, $to, 'ASC');
      }
    }
    $this->view->years = $years;
    $this->view->snapshots = $snapshots;
  }

  public function warehouseSnapshotFixAction() {
    $this->_helper->layout->disableLayout();
    $params = $this->_request->getParams();
    if ($id = $params['id']) {
      $wh = new Tobacco_Warehouse();
      if ($wh->fixSnapshot($id)) {
        $this->_flashMessenger->addMessage('Účetní stav skladu položky byl upraven podle fyzického stavu.');
      }
      else {
        $this->_flashMessenger->addMessage('Srovnání účetního a fyzického skladového stavu položky se nepovedlo.');
      }
      $snapshot = $wh->getSnapshot($id);
      $partId = $snapshot['id_parts'];
      $year = substr($snapshot['date'], 0, 4);
      $this->redirect('tobacco/parts-detail/id/' . $partId .  '/year/' . $year . '#changes');
    }
    else {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID snapshotu.');
      $this->redirect('tobacco/warehouse-snapshot');
    }
  }

  public function macerationsSnapshotAddAction() {
    $params = $this->_request->getParams();
    $maceration = new Tobacco_Macerations_Maceration($this->_request->getParam('id'));
    $date = new Meduse_Date($this->_request->getParam('date'), 'Y-m-d');
    $form = new Tobacco_Macerations_SnapshotForm();
    $form->setMaceration($maceration);
    $form->setDate($date);
    if ($this->_request->getParam('type') == 'invent') {
      $form->setNote('Inventura ' . $date->get('d/m/Y'));
      $form->removeFix();
    }
    $type = $this->_request->getParam('type', 'default');
    if ($this->_request->isPost()) {
      $data = $form->getValidValues($this->_request->getParams());
      $real = $data['amount_real'] ?? 0.0;
      $wh = $data['amount_wh'] ?? 0.0;
      $date = new Meduse_Date($data['date'], 'd.m.Y');
      $id_snapshot = $this->_request->getParam('snapshot_id', NULL);
      if ($id = $maceration->setSnapshot($real, $date, $wh, $data['note'], NULL, $id_snapshot)) {
        $this->_flashMessenger->addMessage('Stav byl zaznamenán.');
        if ($data['fix'] == 'y') {
          Tobacco_Macerations::fixSnapshot($id);
          $this->_flashMessenger->addMessage('Stav skladu byl nastaven dle skutečnosti.');
        }
        if ($back = $this->_request->getParam('back')) {
          $this->redirect($back);
        }
        elseif ($this->_request->getParam('type') == 'invent') {
          $this->redirect('/tobacco/close-year/type/macerations/year/' . $date->get('Y') . '#' . $params['id']);
        }
        elseif ($type == 'ajax') {
          $this->_helper->layout()->disableLayout();
          $this->_helper->viewRenderer->setNoRender(TRUE);
          $data = [
            'maceration_id' => $params['id'],
            'status' => TRUE,
            'snapshot_id' => $id,
            'amount_real' => $params['amount_real'],
            'amount_wh' => $params['amount_wh'],
            'date' => $params['date'],
          ];
          $this->_helper->json($data);
        }
        else {
          $this->redirect('tobacco/macerations-snapshot');
        }
      }
      else {
        if ($this->_request->getParam('type') == 'ajax') {
          $this->_helper->layout()->disableLayout();
          $this->_helper->viewRenderer->setNoRender(TRUE);
          $this->_helper->json(['status' => FALSE]);
        }
        else {
          $this->_flashMessenger->addMessage('Nastala chyba, stav nebyl zaznamenán.');
          $this->redirect($this->view->url());
        }
      }
    }

    $this->view->form = $form;
    $this->view->maceration = $maceration;
  }


  public function macerationsSnapshotFixAction() {
    $this->_helper->layout->disableLayout();
    $params = $this->_request->getParams();
    if ($id = $params['id']) {
      Tobacco_Macerations::fixSnapshot($id);
      $this->_flashMessenger->addMessage('Účetní stav macerace byl upraven podle fyzického stavu.');
      $snapshot = Tobacco_Macerations::getSnapshot($id);
      $macerationId = $snapshot['id_macerations'];
      $year = substr($snapshot['date'], 0, 4);
      $this->redirect('tobacco/macerations-detail/id/' . $macerationId .  '/year/' . $year . '#changes');
    }
    else {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID snapshotu.');
      $this->redirect('tobacco/macerations-snapshot');
    }
  }

  function warehouseAjaxSetCriticalAmountAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);
    if ($this->_request->isPost()) {
      $idPart = $this->_request->getParam('partId');
      $amount = $this->_request->getParam('amount');
      try {
        $part = new Tobacco_Parts_Part($idPart);
        $part->setCriticalAmount($amount);
        $amount =$part->getCriticalAmount();
        $this->_helper->json([
          'status' => TRUE,
          'partId' => $part->getID(),
          'amount' => $part->getMeasure() == 'weight' ? sprintf('%01.3f', $amount) : $amount,
        ]);
      }
      catch(Exception $e) {
        $this->_helper->json([
          'status' => FALSE,
          'error' => $e->getMessage(),
        ]);
      }
    }
  }

  function macerationsRepairAction() {

    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);


    $batches = [  'L131024A01' => 60,  'L151121B01' => 8,  'L160614C01' => 3,  'L160809D01' => 5,  'L160923E01' => 15,  'L170330A01' => 20,  'L170607A01' => 20,  'L170731A01' => 30,  'L180806001' => 20,  'L190204001' => 20,  'L190301001' => 10,  'L191002001' => 30,  'L131024A02' => 66,  'L140904B02' => 35,  'L160429C02' => 1,  'L160614D02' => 3,  'L160701E02' => 15,  'L160923F02' => 15,  'L170125G02' => 7,  'L170223H02' => 7,  'L170330A02' => 30,  'L170607A02' => 30,  'L170815A02' => 30,  'L180605A02' => 20,  'L180702002' => 20,  'L181211002' => 6,  'L190204002' => 20,  'L190524002' => 30,  'L131024A03' => 63,  'L170623A03' => 10,  'L170718A03' => 20,  'L180404A03' => 20,  'L181002003' => 12,  'L190301003' => 20,  'L190524003' => 30,  'L131024A04' => 60,  'L151013B04' => 6,  'L151121C04' => 12,  'L151223D04' => 8,  'L160202E04' => 10,  'L161004F04' => 7.5,  'L161107G04' => 15,  'L170330A04' => 20,  'L170516A04' => 20,  'L170619A04' => 15,  'L170815A04' => 30,  'L180605A04' => 20,  'L181211004' => 6,  'L190301004' => 30,  'L190807004' => 30,  'L131024A05' => 60,  'L170412A05' => 7,  'L131024A06' => 60,  'L141909B06' => 10,  'L160909C06' => 15,  'L170223D06' => 5,  'L170428A06' => 20,  'L170717A06' => 30,  'L180503A06' => 23.5,  'L181002006' => 12,  'L190423006' => 30,  'L131024A07' => 60,  'L140904B07' => 35,  'L160202C07' => 7.5,  'L160614D07' => 3,  'L160701E07' => 11,  'L161024F07' => 15,  'L170125G07' => 7,  'L170223H07' => 7,  'L170330A07' => 15,  'L170428A07' => 10,  'L170516A07' => 30,  'L170630A07' => 15,  'L170825A07' => 30,  'L180503A07' => 20,  'L180702007' => 20,  'L181002007' => 26,  'L190204007' => 20,  'L190423007' => 20,  'L190617007' => 30,  'L131024A08' => 60,  'L160614B08' => 4,  'L160818C08' => 10,  'L161202D08' => 15,  'L170412A08' => 13,  'L170607A08' => 20,  'L170717A08' => 30,  'L180404A08' => 30,  'L181105008' => 20,  'L190423008' => 30,  'L191014008' => 30,  'L131024A09' => 60,  'L151013D09' => 10,  'L151121C09' => 8,  'L151223D09' => 8,  'L160809E09' => 5,  'L160923F09' => 14,  'L170223G09' => 5,  'L170330A09' => 20,  'L170516A09' => 20,  'L170628A09' => 15,  'L170828A09' => 30,  'L180302A09' => 30,  'L180702009' => 20,  'L181211009' => 6,  'L190204009' => 20,  'L190524009' => 30,  'L190919009' => 30,  'L131024A10' => 60,  'L141909B10' => 10,  'L151121C10' => 12,  'L160202D10' => 5,  'L160614E10' => 4,  'L160719F10' => 15,  'L161202G10' => 15,  'L170428A10' => 10,  'L170607A10' => 20,  'L170830A10' => 30,  'L180605A10' => 20,  'L181211010' => 6,  'L190301010' => 20,  'L191002010' => 30,  'L131024A11' => 60,  'L141105C11' => 20,  'L150629C11' => 9,  'L151013D11' => 10,  'L151121E11' => 8,  'L160429F11' => 12,  'L160719G11' => 15,  'L160923H11' => 15,  'L161024I11' => 15,  'L161223J11' => 45,  'L170125K11' => 15,  'L170314L11' => 10,  'L170412A11' => 20,  'L170607A11' => 30,  'L170731A11' => 60,  'L180302A11' => 30,  'L180702011' => 20,  'L181011011' => 30,  'L190423011' => 20,  'L190604011' => 30,  'L131024A12' => 60,  'L151121B12' => 12,  'L160202C12' => 5,  'L160429D12' => 8,  'L160809E12' => 5,  'L160923F12' => 7,  'L161107G12' => 15,  'L170314H12' => 10,  'L170428A12' => 20,  'L170623A12' => 15,  'L170828A12' => 30,  'L180302A12' => 30,  'L180702012' => 20,  'L181211012' => 6,  'L190204012' => 20,  'L190524012' => 30,  'L190919012' => 30,  'L131024A13' => 60,  'L151121B13' => 8,  'L160429C13' => 12,  'L160809D13' => 5,  'L160923E13' => 15,  'L170125F13' => 5,  'L170223G13' => 5,  'L170330A13' => 30,  'L170623A13' => 10,  'L170717A13' => 30,  'L171222A13' => 11,  'L180702013' => 52,  'L181105013' => 30,  'L190423013' => 30,  'L190723013' => 30,  'L131024A14' => 63,  'L160818B14' => 10,  'L161223C14' => 15,  'L170428A14' => 20,  'L170623A14' => 10,  'L170817A14' => 30,  'L180503A14' => 20,  'L181002014' => 30,  'L190524014' => 30,  'L131024A15' => 60,  'L161202B15' => 5,  'L161004C15' => 15,  'L170428A15' => 10,  'L170623A15' => 10,  'L170829A15' => 30,  'L181002015' => 20,  'L140904A28' => 6,  'L150108A28' => 6,  'L150629B28' => 5,  'L151013C28' => 3,  'L151121D28' => 12,  'L160809E28' => 5,  'L161202F28' => 5,  'L170125G28' => 5,  'L170314H28' => 10,  'L170428A28' => 10,  'L170607A28' => 20,  'L170828A28' => 30,  'L180503A28' => 20,  'L181211028' => 6,  'L190204028' => 20,  'L190617028' => 30,  'L140904A29' => 9,  'L150629B29' => 5,  'L151013C29' => 6,  'L151121D29' => 12,  'L160614E29' => 3,  'L160809F29' => 5,  'L160916G29' => 15,  'L170314H29' => 10,  'L170428A29' => 10,  'L170607A29' => 30,  'L170829A29' => 30,  'L180605A29' => 20,  'L181211029' => 6,  'L190301029' => 30,  'L190712029' => 30,  'L141219A30' => 6,  'L150119B30' => 10,  'L150601C30' => 6,  'L150629D30' => 9,  'L151013E30' => 10,  'L151121F30' => 8,  'L151223G30' => 8,  'L160429H30' => 12,  'L160809I30' => 5,  'L160909J30' => 20,  'L161014K30' => 15,  'L161027J30' => 15,  'L161223K30' => 45,  'L170223L30' => 5,  'L170330A30' => 30,  'L170607A30' => 30,  'L170817A30' => 30,  'L171222A30' => 38,  'L180404A30' => 30,  'L181002030' => 12,  'L181105030' => 30,  'L190301030' => 30,  'L190426030' => 150,  'L190712030' => 30,  'L150601A34' => 3,  'L151013B34' => 3,  'L151121C34' => 8,  'L160429D34' => 12,  'L160909E34' => 15,  'L170125F34' => 5,  'L170314G34' => 10,  'L170412A34' => 20,  'L170607A34' => 20,  'L170720A34' => 45,  'L180605A31' => 20,  'L181105031' => 20,  'L190423031' => 30,  'L150601A37' => 3,  'L151013B37' => 3,  'L161202C37' => 5,  'L160429D37' => 12,  'L160916E37' => 20,  'L170330A37' => 20,  'L170607A37' => 20,  'L170829A37' => 30,  'L180302A32' => 34,  'L181211032' => 6,  'L190204032' => 20,  'L190524032' => 5,  'L180302A33' => 10,  'L180404A33' => 32,  'L180702033' => 20,  'L181105033' => 30,  'L190423033' => 30,  'L150601A32' => 1,  'L151013B32' => 6,  'L151121C32' => 8,  'L160429D32' => 10,  'L161202E32' => 15,  'L170428A32' => 10,  'L170607A32' => 20,  'L170830A32' => 30,  'L191002034' => 10,  'L150601A33' => 1,  'L151013B33' => 3,  'L161202C33' => 5,  'L160429D33' => 12,  'L160909E33' => 15,  'L170125F33' => 5,  'L170314G33' => 10,  'L170428A33' => 10,  'L170623A33' => 15,  'L170824A33' => 30,  'L180404A35' => 30,  'L190204035' => 20,  'L190524035' => 30,  'L151013A31' => 3,  'L151223B31' => 8,  'L160614C31' => 4,  'L160809D31' => 5,  'L160916E31' => 14,  'L170314F31' => 10,  'L170428A31' => 10,  'L170607A31' => 20,  'L170830A31' => 30,  'L180605A36' => 12,  'L190204036' => 15,  'L190423036' => 10,  'L190905036' => 10,  'L160429A39' => 10,  'L161014B39' => 6,  'L170125C39' => 5,  'L170330A39' => 20,  'L170816A39' => 30,  'L171222A37' => 12,  'L180503A37' => 12,  'L181011037' => 10,  'L190301037' => 20,  'L151013A35' => 3,  'L160809B35' => 5,  'L170223C35' => 5,  'L170516A35' => 11.8,  'L170830A35' => 15,  'L181002038' => 10,  'L160722A40' => 6,  'L161024B40' => 6,  'L170125C40' => 5,  'L170223D40' => 5,  'L170412A40' => 20,  'L170718A40' => 30,  'L180404A42' => 30,  'L161014A41' => 6,  'L170125B41' => 6,  'L170223C41' => 6,  'L170412A41' => 20,  'L170623A41' => 15,  'L170815A41' => 30,  'L171222A54' => 30,  'L190204054' => 20,  'L190524054' => 5,  'L190712054' => 30,  'L170516A50' => 30,  'L170802A50' => 30,  'L180302A55' => 30,  'L190301055' => 30,  'L171222A59' => 17,  'L180404A59' => 12.5,  'L190301059' => 20,  'L180605A60' => 10,  'L181011060' => 30,  'L180503A62' => 5,  'L180605A62' => 10,  'L180806062' => 20,  'L190204062' => 20,  'L190627062' => 20,  'L180503A63' => 20,  'L181002063' => 20,  'L190204063' => 20,  'L190423063' => 10,  'L190301064' => 20,  'L190524064' => 30,  'L190426065' => 150,  'L190905065' => 30,  'L161014A43' => 15,  'L161223B43' => 45,  'L161024A48' => 15,  'L161223B48' => 45,  'L161024A45' => 15,  'L161223B45' => 45,  'L161024A47' => 15,  'L161223B47' => 45,  'L190426046' => 150,  'L161024A46' => 15,  'L161223B46' => 45,  'L161014A42' => 15,  'L161223B42' => 45,  'L180605A48' => 10,  'L180806048' => 20,  'L190204048' => 15,  'L161014A44' => 15,  'L161223B44' => 45,  'L161024A49' => 15,  'L161223B49' => 45,  'L140926A16' => 10,  'L140926A17' => 10,  'L181113017' => 30,  'L190301017' => 30,  'L190426017' => 150,  'L190617017' => 30,  'L140926A18' => 6,  'L140926A19' => 6,  'L140926A20' => 6,  'L140926A21' => 6,  'L140926A22' => 6,  'L140926A23' => 6,  'L140926A24' => 6,  'L140926A25' => 6,  'L190301090' => 30,  'L191014090' => 10,  'L190426093' => 150,  'L190301098' => 30,  'L190627201' => 25,  'L190627202' => 25,  'L190627203' => 25,  'L190723204' => 25,  'L190627205' => 25,  'L190627206' => 25,  'L190627207' => 9,  'L190905209' => 25,  'L190726210' => 25  ];

    $recipeId = $this->_request->getParam('recipe');
    $fix = $this->_request->getParam('fix', 0);

    $startDate = new Meduse_Date($this->_request->getParam('startdate', '2018-31-12'), 'Y-m-d');
    $endDate = new Meduse_Date($this->_request->getParam('enddate', '2019-12-03'), 'Y-m-d');

    $recipe = new Tobacco_Recipes_Recipe($recipeId);

    $startAmount = $this->_request->getParam('startamount', 0); // 20.401;
    $endAmount = $this->_request->getParam('endamount', 0); // 10.400;

    $fixer = new Tobacco_Macerations_Fixer($recipe, $startDate, $startAmount, $endDate, $endAmount);
    $fixer->setBatchList($batches);

    if ($fix) {
      $fixer->fix();
    }
    else {
      $fixer->check();
    }

    die('<pre>' . implode('<br>', $fixer->getMessages()) . '</pre>');
  }

  public function  ajaxMacerationInfoAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $idParts = $this->_request->getParam('id_parts');
    $date = new Meduse_Date($this->_request->getParam('date'), 'Y-m-d');
    $info = Tobacco_Macerations::getPartsHistoryInfo($idParts, $date);
    $data = ['status' => TRUE, 'info' => $info];
    $this->_helper->json($data);
  }

  public function warehouseDailyPackingAction() {
    $params = [];
    if ($this->_request->isPost()) {
      $packing = [];
      $params = $this->_request->getParams();
      foreach ($params as $key => $value) {
        $value = (int) $value;
        if ((strpos($key, 'amount_', 0) === 0) && $value > 0) {
          [$null, $mixId, $partId] = explode('_', $key);
          $macerationIds = [];
          $fit = 0;
          while (isset($params['batch_' . $mixId . '_' . $fit])) {
            $macerationIds[] = $params['batch_' . $mixId . '_' . $fit];
            $fit++;
          }
          $packing[] = [
            'id_mixes' => $mixId,
            'id_parts' => $partId,
            'amount' => $value,
            'barrels' => $macerationIds,
          ];
        }
      }
      $this->db->beginTransaction();
      try {
        foreach ($packing as $item) {
          $part = new Tobacco_Parts_Part($item['id_parts']);
          $critical = $part->pack($item['amount'], $item['barrels']);
          if (!empty($critical['batches'])) {
            $this->_flashMessenger->addMessage('Některé maceráty dosáhly limitního množství: '
              . implode(', ', $critical['batches']));
          }
          if (!empty($critical['subparts'])) {
            $this->_flashMessenger->addMessage('Některé součásti dosáhly limitního množství: '
              . implode(', ', $critical['subparts']));
          }
        }
        $this->db->commit();
        $msg = $this->generatePackingMessage($packing);
        $email = new Table_Emails();
        $email->sendEmail(Table_Emails::EMAIL_TOBACCO_PACKING_REPORT, ['body' => $msg]);
        $this->_flashMessenger->addMessage($msg);
        if (Zend_Registry::get('acl')->isAllowed('tobacco/warehouse')) {
          $this->redirect('/tobacco/warehouse/tab/tobacco_nfs');
        }
        else {
          $this->redirect('/tobacco/warehouse-daily-packing');
        }
      }
      catch (Exception $e) {
        $this->db->rollBack();
        $this->view->message = 'Neprovedeno. ' . $e->getMessage();
      }
    }
    $this->view->title = 'Hromadné zadání nabalení produktů';
    $form = new Tobacco_Mixes_DailyPackingForm();
    if ($params) {
      $form->populate($params);
    }
    $this->view->form = $form;
  }

  public function closeYearAction() {

    $type = $this->_request->getParam('type', 'parts');

    if ($type == 'parts') {
      $wh = new Tobacco_Warehouse();
      $years = $wh->getSnapshotsYears();
      $year = array_shift($years);
      $report = $wh->getClosingReport($year);
      $title = 'Inventura materiálu a produktů v roce ' . $year;
    }
    elseif ($type == 'macerations') {
      $years = Tobacco_Macerations::getSnapshotsYears();
      $year = array_shift($years);
      $report = Tobacco_Macerations::getClosingReport($year);
      $title = 'Inventura macerace v roce ' . $year;
    }
    else {
      throw new Exception("Unknown type of report '$type'");
    }

    $this->view->type = $type;
    $this->view->report = $report;
    $this->view->title = $title;
    $this->view->year = $year;
    $this->view->close_date = $year < date('Y') ? $year . '-12-31' : date('Y-m-d');
    $this->view->show_abs_diff = $this->_request->getParam('diff') === 'abs';
  }

  public function closeYearProcessAction() {
    $type = $this->_request->getParam('type', 'parts');
    if ($type == 'parts') {
      $wh = new Tobacco_Warehouse();
      $years = $wh->getSnapshotsYears();
      $year = array_shift($years);
      try {
        $wh->closeYear($year);
        $msg = "Uzavření skladu materiálu a produktů roku $year proběhlo úspěšně.";
        $url = 'tobacco/warehouse-snapshot';
      }
      catch (Exception $e) {
        $msg = "Uzavření skladu materiálu a produktů roku $year se nezdařilo: "
        . $e->getMessage();
        $url = 'tobacco/close-year/type/parts';
      }
    }
    elseif ($type == 'macerations') {
      $years = Tobacco_Macerations::getSnapshotsYears();
      $year = array_shift($years);
      try {
        Tobacco_Macerations::closeYear($year);
        $msg = "Uzavření skladu materiálu a produktů roku $year proběhlo úspěšně.";
        $url = 'tobacco/macerations-snapshot';
      }
      catch (Exception $e) {
        $msg = "Uzavření skladu materiálu a produktů roku $year se nezdařilo: "
          . $e->getMessage();
        $url = 'tobacco/close-year/type/macerations';
      }
    }
    else {
      throw new Exception("Unknown type of report '$type'");
    }
    $this->_flashMessenger->addMessage($msg);
    $this->redirect($url);
  }

  /**
   * @param array $data ['id_mixes', 'id_parts', 'amount', 'barrels']
   * @return string
   */
  protected function generatePackingMessage(array $data) {

    $base_url = 'https://warehouse.medusepipes.com';
    $message  = '<strong>Report hromadného nabalení produktů — ' . date('d.m.Y H:i:s') . '</strong>';
    $message .= '<ul>';

    foreach ($data as $item) {
      $id_parts = $item['id_parts'];
      $part = new Tobacco_Parts_Part($id_parts);
      $batches = [];
      foreach ($item['barrels'] as $id_macerations) {
        $barrel = new Tobacco_Macerations_Maceration($id_macerations);
        $batches[$id_macerations] = $barrel->getBatchNumber();
      }
      $batches_str = [];
      foreach ($batches as $id => $batch) {
        $batches_str[] = "<a href='$base_url/tobacco/macerations-detail/id/$id'>$batch</a>";
      }
      $batches_str = 'sudy: ' . implode(', ', $batches_str);
      $name = $part->getName();
      $amount_packed = $item['amount'];
      $amount_str = 'aktuální stav: ' . $part->getWHAmount(). ' ks';
      $message .= "<li>$amount_packed ks <a href='$base_url/tobacco/parts-detail/id/$id_parts'>$id_parts</a> $name";
      $message .= " ($batches_str — $amount_str)</li>";
    }
    $message .= '</ul>';
    return $message;
  }

  private function human_filesize($bytes, $decimals = 2) {
    $sz = 'BKMGTP';
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f ", $bytes / pow(1024, $factor)) . substr($sz, $factor, 1);
  }


  public function partsRecalculateAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    $id = $this->_request->getParam('id');
    if ($id) {
      try {
        $part = new Tobacco_Parts_Part($id);
        $part->refreshPrice();
        $this->_flashMessenger->addMessage('Cena byla přepočítána.');
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
      }
    }
    $this->redirect($this->view->url(['action' => 'parts-detail']));
  }

  public function partsRestoreAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $partId = $this->_request->getParam('id');
    $part = new Tobacco_Parts_Part($partId);
    if ($part->restore()) {
      $this->_flashMessenger->addMessage('Položka byla obnovena z koše.');
    }
    else {
      $this->_flashMessenger->addMessage('Položku se nebpodařilo obnovit.');
    }
    $this->redirect($this->view->url(['action' => 'parts-detail']));
  }


  /**
   * @throws Zend_Db_Table_Exception
   * @throws Zend_Form_Exception
   * @throws Zend_Db_Adapter_Exception
   * @throws Parts_Exceptions_IdNotExists
   */
  public function partsSaveAsAction()
  {
    $existingPartId = $this->_request->getParam('id');

    if (!$existingPartId) {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID existující součásti.');
      $this->redirect($this->view->url(['action' => 'warehouse']));
      exit;
    }

    try {
      $part = new Tobacco_Parts_Part($existingPartId);
    } catch (Exception $e) {
      $this->_flashMessenger->addMessage($e->getMessage());
      $this->redirect($this->view->url(['action' => 'warehouse']));
      exit;
    }

    $title = "Uložit {$existingPartId} jako...";
    $form = new Parts_SaveAsForm(['from' => $part]);
    $form->setLegend($title);

    if ($this->_request->isPost()) {
      $params = $this->_request->getParams();

      if ($form->isValid($params)) {
        $newPartId = $form->getValue('meduse_parts_id');
        $newPartName = $form->getValue('meduse_parts_name');
        $newPartDescription = $form->getValue('meduse_parts_desc');

        try {
          $part->saveAs($existingPartId, $newPartId, $newPartName, $newPartDescription);
          $saved = true;
        } catch (Parts_Exceptions_IdAlreadyExists $e) {
          $errorMessage = "ID = {$newPartId} nelze použít, již existuje.";
          $saved = false;
        } catch (Exception $e) {
          $errorMessage = $e->getMessage();
          $saved = false;
        }
        if ($saved) {
          $this->_flashMessenger->addMessage("Součást {$existingPartId} byla uložena jako {$newPartId}.");
          $this->redirect($this->view->url(['action' => 'parts-detail', 'id' => $newPartId]));
        }
      } else {
        $form->populate($params);
      }
    }

    $this->view->title = $title;
    $this->view->message = $errorMessage ?? null;
    $this->view->form = $form;
  }

  public function macerationsDiscardAction() {
    $this->macerationDiscard(TRUE);
  }
  public function macerationsRestoreAction() {
    $this->macerationDiscard(FALSE);
  }

  private function macerationDiscard(bool $discard) {
    $process = TRUE;
    if (!$idMaceration = $this->_request->getParam('id')) {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID macerátu.');
    }

    if (!$maceration = new Tobacco_Macerations_Maceration($idMaceration)) {
      $this->_flashMessenger->addMessage("Macerát s ID = $idMaceration nebyl nalezen.");
      $process = FALSE;
    }

    if ($process) {
      $maceration->discard($discard);
      $message = $discard ? 'Macerát byl vyřazen.' : 'Macerát byl obnoven.';
      $this->_flashMessenger->addMessage($message);
    }

    $back = $this->_request->getParam('back');
    $this->redirect($back ? base64_decode($back) : $this->view->url(['action' => 'macerations', 'id' => NULL]));
  }

    /**
     * @throws Zend_Form_Exception
     */
    public function macerationCalculatorAction(): void
    {
        $title = 'Kalkulátor macerace';
        $weight = (float) $this->_request->getParam('weight', 10.0);
        $recipe = null;

        if ($recipeId = (int) $this->_request->getParam('recipe', 0)) {
            $recipe = new Tobacco_Recipes_Recipe($recipeId);
        }

        $this->view->assign('title', $title);
        $this->view->assign('weight', $weight);
        $this->view->assign('setting', Tobacco_Recipes_Recipe::getCalculatorSettingForm($recipeId, $weight, $title));
        if ($recipe) {
            $this->view->assign('form', Tobacco_Recipes_Recipe::getCalculatorForm($recipe, $weight));
        }

    }

    public function batchConsolidateAction(): void {
        $result = null;
        if ($this->_request->isPost()) {
            $dryRun = $this->_request->getParam('dry-run', false) !== false;
            $table = new Table_PartsWarehouse();
            $result = $table->batchConsolidate($dryRun);
        }
        $this->view->assign('title', 'Konsolidace čísel šarží');
        $this->view->assign('dryRun', $dryRun ?? null);
        $this->view->assign('result', $result);
    }
}
