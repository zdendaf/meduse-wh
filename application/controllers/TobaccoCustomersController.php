<?php

  class TobaccoCustomersController extends Meduse_Controller_Action {

    protected $db = NULL;

    public function init() {
      $this->db = Zend_Registry::get('db');
      $this->_helper->layout->setLayout('bootstrap-tobacco');
    }

    public function indexAction() {
  		$form = new Customers_SearchForm();
    	$form->isValid($this->getRequest()->getParams());
      $form->setLegend('Hledat zákazníka');
      $this->view->filter = $form;
      $table = new Table_Customers();
      $select = $table->getDataList(true, 2);

      if ($this->_request->getParam('Hledat')) {
        $name = $this->_request->getParam('name', false);
        if (($name !== false) && $chunks = explode(' ', $name)) {
          foreach ($chunks as $chunk) {
            $chunk = addslashes(strip_tags(trim($chunk)));
            if (empty($chunk)) {
              continue;
            }
            $select->where("c.first_name LIKE '%$chunk%' OR c.last_name LIKE '%$chunk%' OR c.company LIKE '%$chunk%' OR c.email LIKE '%$chunk%'" .
              "OR a.company LIKE '%$chunk%' OR a.person LIKE '%$chunk%'");
          }
        }
      }

      $table->enrichDataList_InvoiceLastIssueDate($select);
      $grid = $this->_getGrid();
      $grid->setSelect($select);
      $grid->setRequest($this->getRequest()->getParams());
      $this->view->grid = $grid;
    }

  /**
   * Vytvoreni a editace zakaznika.
   */
	public function editAction() {
    $id = $this->_request->getParam('id');
    $search = $this->_request->getParam('search');
    $customerObj = NULL;
    if (is_null($id)) {
      // pokud neni ID, vytvorime noveho zakaznika
      $form = new Tobacco_Customers_Form();
      $form->setLegend('Nový zákazník');
    }
    else {
      // pokud je ID editujeme zakaznika
      $customerObj = new Customers_Customer((int) $id);
      $form = $customerObj->getForm(Tobacco_Parts_Part::WH_TYPE);
      $form->setLegend('Upravit zákazníka');
      $form->removeElement('address_street');
      $form->removeElement('address_pop_number');
      $form->removeElement('address_orient_number');
      $form->removeElement('address_city');
      $form->removeElement('address_zip');
      $form->removeElement('address_country');
    }
		if (!$search && $this->_request->isPost()) {
      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        if (is_object($customerObj)) {
          $customerObj->update($params);
        }
        else {
          $customerObj = new Customers_Customer($params);
        }
        if ($orderId = $this->_request->getParam('order')) {
          $order = new Tobacco_Orders_Order($orderId);
          $order->setCustomer($customerObj);
          $this->redirect($this->view->url(array('controller' => 'tobacco-order-edit', 'action' => 'customer', 'order' => $orderId, 'id' => NULL)));
        }
        elseif ($params['destination']) {
          $this->redirect($params['destination']);
        }
        else {
          $this->redirect($this->view->url(array(
            'action' => 'detail',
            'id' => $customerObj->getId(),
          )));
        }
      }
		}
		if ($search) {
      $form->getElement('customer_company')->setValue($search);
    }
		$this->view->form = $form;
	}

    public function detailAction() {

      $id = (int) $this->_request->getParam('id');
      $customerObj = new Customers_Customer($id);
      $this->view->customer = $customerObj;
      $this->view->title = 'Detail zákazníka – ' . $customerObj->getName();
      $params = array();
      $auth = new Zend_Session_Namespace('Zend_Auth');
      $params['user'] = $auth->user_id;
      $params['view_private'] = Zend_Registry::get('acl')->isAllowed('orders/view-private');
      $this->view->orders = $customerObj->getOrders($params);
      $this->view->addresses = $customerObj->getAddresses();
      $this->view->addressBill = $customerObj->getBillingAddress();
      $this->view->addressShip = $customerObj->getShippingAddress();
      $this->view->identNo = $customerObj->getIdentNo();
      $this->view->vatNo = $customerObj->getVatNo();
      $validVatNo = $customerObj->isVerifiedVatNo();
      $this->view->isValidVatNo = $validVatNo;
      $this->view->isValidVatNoClass = is_null($validVatNo)? '' : ($validVatNo ? 'btn-success' : 'btn-danger');
    }

    public function deleteAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $tCustomers = new Table_Customers();
      if($this->_request->getParam('id')) {
        $rCustomers = $tCustomers->find($this->_request->getParam('id'))->current();
        $rCustomers->deleted = 1;
        $rCustomers->save();
        $this->_flashMessenger->addMessage("Zákazník byl odstraněn.");
      }
      $this->_redirect('/tobacco-customers');
    }

    public function addressAction() {
    $address = NULL;
    $id = $this->_request->getParam('id');
    if ($id) {
      $address = new Addresses_Address($id);
      $form = $address->getForm();
    }
    else {
      $customer = $this->_request->getParam('customer');
      $customerObj = new Customers_Customer($customer);
      $form = new Addresses_Form(array('customer' => $customerObj));
    }

    if ($this->_request->isPost()) {
      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        try {
          if (is_object($address)) {
            $address->update($params);
            $this->_flashMessenger->addMessage('Adresa byla aktualizována');
          }
          else {
            $address = new Addresses_Address($params);
            $this->_flashMessenger->addMessage('Nová adresa byla uložena');
          }

          // Změna dodací adresy u otevřených objednávek.
          if ($address->isShipping()) {
            $changedOrders = array_map(function($o){
              return $o->getNO();
            }, $address->getCustomer()->resetOrdersAddress($address));
            if ($changedOrders) {
              $message = 'Těmto objednávkám byla změněná dodací adresa: ' . implode(', ', $changedOrders);
              $this->_flashMessenger->addMessage($message);
            }
          }

          $this->redirect($params['destination']);
        }
        catch (Exception $e) {
          $this->view->message = $e->getMessage();
          $form->populate($params);
        }
      }
    }
		$this->view->form = $form;
	}

    protected function _getGrid() {


      $columns = array(
        'type' => array(
          'header' => _('Typ') ,
          'css_style' => 'text-align: left',
          'sorting' => true
        ),
        'pricelists' => array(
            'header' => _('Ceníky') ,
            'css_style' => 'text-align: left' ,
            'sorting' => true,
            'renderer' => 'url',
            'url' => $this->view->url(array(
                'module' => 'default' , 'controller' => 'tobacco-pricelists' ,
                'action' => 'assign' , 'customer' => '{id}'
            ), null, true)
        ),
        'company' => array(
          'header' => _('Firma') ,
          'css_style' => 'text-align: left' ,
          'sorting' => true,
          'renderer' => 'url',
          'url' => $this->view->url(array(
            'module' => 'default' , 'controller' => 'tobacco-customers' ,
            'action' => 'detail' , 'id' => '{id}'
          ), null, true)
        ),
        'first_name' => array(
          'header' => _('Jméno') ,
          'css_style' => 'text-align: left',
          'sorting' => true,
          'renderer' => 'url' ,
          'url' => $this->view->url(array(
            'module' => 'default' , 'controller' => 'tobacco-customers' ,
            'action' => 'detail' , 'id' => '{id}'
          ), null, true)
        ),
        'last_name' => array(
          'header' => _('Příjmení') , 'sorting' => true ,
          'css_style' => 'text-align: left' ,
          'renderer' => 'url' ,
          'url' => $this->view->url(array(
            'module' => 'default' , 'controller' => 'tobacco-customers' ,
            'action' => 'detail' , 'id' => '{id}'
          ), null, true)
        ),
        'email' => [
          'header' => _('Email'),
          'sorting' => TRUE,
          'css_style' => 'text-align: left',
        ],
        'country' => array(
          'header' => _('Země') , 'sorting' => true ,
          'css_style' => 'text-align: center' ,
        ),
        'billing_company' => [
          'header' => _('Firma na faktuře'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
        ],
        'person' => [
          'header' => _('Osoba na faktuře'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
        ],
        'invoice_last_issue_date' => [
          'header' => _('Posl.fakt.'),
          'sorting' => true,
          'renderer' => 'date',
        ],
        'delete' => array(
          'header' => '' , 'css_style' => 'text-align: center',
          'renderer' => 'bootstrap_action' , 'type' => 'delete' ,
          'url' => $this->view->url(array(
            'module' => 'default' , 'controller' => 'tobacco-customers',
            'action' => 'delete' , 'id' => '{id}'
          ), null, true)
        ),
        'edit' => array(
          'header' => '' , 'css_style' => 'text-align: center' ,
          'renderer' => 'bootstrap_action' , 'type' => 'edit' ,
          'url' => $this->view->url(array(
            'module' => 'default' , 'controller' => 'tobacco-customers' ,
            'action' => 'edit' , 'id' => '{id}'
          ), null, true)
        )
      );

      $acl = Zend_Registry::get('acl');
      if (!$acl->isAllowed('tobacco-pricelists/assign')) {
        unset($columns['pricelists']);
      }
      if (!$acl->isAllowed('tobacco-customers/delete')) {
        unset($columns['delete']);
      }

      $grid = new ZGrid(array(
        'css_class' => 'table table-condensed table-hover sticky',
        'title' => _("Zákazníci") ,
        'add_link_url' => $this->view->url(array(
          'module' => 'default' , 'controller' => 'tobacco-customers' ,
          'action' => 'edit'
        ), null, true) ,
        'allow_add_link' => true,
        'add_link_title' => _('Nový zákazník') ,
        'columns' => $columns,
      ));
      return $grid;
    }

    public function ajaxSearchAction() {
       $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $params['wh'] = Table_TobaccoWarehouse::WH_TYPE;
        $grid = Customers::getDataGrid($params);
        $html = $grid->render();
        $this->_helper->json(array('html' => $html));
    }

    public function ajaxVerifyVatAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(TRUE);
      $data = array(
        'status' => FALSE,
        'result' => NULL,
        'message' => NULL,
      );
      $cId = $this->_request->getParam('customer');
      try {
        $customer = new Customers_Customer($cId);
        $verified = $customer->verifyVatNo();
        $data = array(
          'status' => TRUE,
          'result' => $verified,
          'message' => $verified ? 'VERIFY_DONE_VALID' : 'VERIFY_DONE_INVALID',
        );
      }
      catch (Exception $e) {
        $data['message'] = $e->getMessage();
      }
      $this->_helper->json($data);
    }

    public function addressDeleteAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $addressId = $this->_request->getParam('id');
      $backUrl = $this->_request->getParam('destination', '/tobacco-customers');

      if (!$addressId) {
        $message = 'Nebylo zadáno ID adresy';
      }
      else {
        $tAddresses = new Table_Addresses();
        $updated = $tAddresses->update(['deleted' => 1], 'id = ' . $addressId);

        if ($updated === 0) {
          $message = 'Adresu se nepodařilo odstranit';
        }
        else {
          $message = 'Adresa byla odstraněna. <a href="'
            . $this->view->url(['action' => 'address-undelete'])
            . '?destination=' . $backUrl . '">Vrátit</a>';
        }
      }
      $this->_flashMessenger->addMessage($message);
      $this->redirect($backUrl);
    }

    public function addressUndeleteAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $addressId = $this->_request->getParam('id');
      $backUrl = $this->_request->getParam('destination', '/tobacco-customers');

      if (!$addressId) {
        $message = 'Nebylo zadáno ID adresy';
      }
      else {
        $tAddresses = new Table_Addresses();
        $updated = $tAddresses->update(['deleted' => 0], 'id = ' . $addressId);

        if ($updated === 0) {
          $message = 'Adresu se nepodařilo obnovit';
        }
        else {
          $message = 'Adresa byla obnovena.';
        }
      }
      $this->_flashMessenger->addMessage($message);
      $this->redirect($backUrl);
    }


  }
