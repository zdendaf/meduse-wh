<?php
class ActualitiesController extends Meduse_Controller_Action{
	
	public function indexAction() {
		$this->_helper->layout->setLayout('bootstrap-basic');
		$act = new Table_Actualities();
		$this->view->data = $act->getActualities(40);
	}
	
}
