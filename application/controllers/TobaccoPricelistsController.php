<?php
class TobaccoPricelistsController extends Meduse_Controller_Action {

    const IMPORT_DIR = '/../data/uploads/';

    function init() {
      $this->_helper->layout->setLayout('bootstrap-tobacco');
      parent::init();
    }

    private function _getPricelists() {
        $tLists = new Table_Pricelists();
        $this->view->lists = $tLists->fetchAll('deleted = "n" AND id_wh_type = ' . Table_TobaccoWarehouse::WH_TYPE)->toArray();
        $this->view->pricelistForm = new Tobacco_Pricelists_Form(NULL, NULL);
        $this->view->pricelistForm->setAction('/tobacco-pricelists/edit');
        $this->view->importForm = new Pricelists_ImportForm();
        $this->view->importForm->setAction('/tobacco-pricelists/import');
        $this->view->importForm->removeElement('import');
        $this->view->openModal = FALSE;
    }

    public function indexAction() {
      $this->_getPricelists();
    }

    public function editAction() {
      if ($this->getRequest()->isPost()) {
        $form = new Tobacco_Pricelists_Form(NULL, NULL);
        $req = $this->getRequest();
        if ($form->isValid($req->getParams())) {
            $this->_save($req);
            $this->redirect('/tobacco-pricelists');
        }
        else {
          $this->_getPricelists();
          $this->view->pricelistForm->populate($req->getParams());
          $this->view->openModal = TRUE;
          $this->renderScript('tobacco-pricelists/index.phtml');
        }
      }
      else {
        $this->indexAction();
      }
    }

    private function _save($req) {
        $tLists = new Table_Pricelists();

        $date = new Zend_Date($req->getParam('valid'), false, new Zend_Locale('cs'));

        $default = 'n';

        // Nastavení výchozího ceníku
        if ($req->getParam('default')=='y') {
          if (strtotime($req->getParam('valid'))>time()) {
            $this->_flashMessenger->addMessage('Ceník platný až v budoucnu nelze nastavit jako výchozí.');
          }
          else {
            $tLists->update(array('default' => 'n'), 'type ="' . $req->getParam('type') . '"');
            $this->_flashMessenger->addMessage('Výchozí ceník nastaven.');
            $default = 'y';
          }
        }

        $data = array(
            'id_wh_type' => Table_TobaccoWarehouse::WH_TYPE,
            'name' => $req->getParam('name'),
            'abbr' => $req->getParam('abbr'),
            'type' => $req->getParam('type'),
            'valid' => $date->get('YYYY-MM-dd'),
            'default' => $default,
            'export' => $req->getParam('export'),
            'assign' => $req->getParam('assign')
        );

        $id = (int) $req->getParam('id');
        if ($id) {
            $tLists->update($data, 'id = ' . $id);
            $this->_flashMessenger->addMessage('Ceník byl <em>' . $data['name'] . '</em> byl upraven.');
        } else {
            $tLists->insert($data);
            $this->_flashMessenger->addMessage('Ceník byl <em>' . $data['name'] . '</em> byl uložen.');
        }
    }


    public function deleteAction() {
        $ids = explode(' ', trim($this->getRequest()->getParam('id')));
        if ($ids) {
            $tLists = new Table_Pricelists();
            $defaultTest = $tLists->select()
                    ->where('id IN ( ' . implode(', ', $ids) . ')')
                    ->where('`default` = ?', 'y')
                    ->query()->fetch();
            if ($defaultTest) {
              $this->_flashMessenger->addMessage('Ceníky nebyly odstraněny, protože se ve výběru vyskytuje výchozí ceník.');
            } else {
              $tLists->update(array('deleted' => 'y'), 'id IN ( ' . implode(', ', $ids) . ')');
              $this->_flashMessenger->addMessage('Zvolené ceníky byly odstraněny.');
            }

        }
        $this->_redirect('/tobacco-pricelists');
    }

    public function assignAction() {

        if ($this->getRequest()->isPost()) {
            $tCP = new Table_CustomersPricelists();
            $pricelists = $this->getRequest()->getParam('pricelists', array());
            $tCP->assignCustomer($this->getRequest()->getParam('customer'), $pricelists);
            $orderId = $this->getRequest()->getParam('order');
            $message = empty($pricelists) ?
              'Ceníky byly odebrány.' : 'Ceníky byly přiřazeny';
            $this->_flashMessenger->addMessage($message);
            if ($orderId) {
              $this->_redirect('/tobacco-orders/detail/id/' . $orderId);
            } else {
              $this->_redirect('/tobacco-customers');
            }
        }
        $this->view->orderId = $this->getRequest()->getParam('order');
        $tPricelists = new Table_Pricelists();
        $this->view->lists = $tPricelists->getCustomersPricelists($this->getRequest()->getParam('customer'));
        $tCustomers = new Table_Customers();
        $this->view->customer = $tCustomers->find($this->getRequest()->getParam('customer'))->current()->toArray();
    }

    public function getPricelistsAjaxAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $tPricelsists = new Table_Pricelists();
        $lists = $tPricelsists->getCustomersPricelists($params['customer']);
        $this->_helper->json($lists);
    }


    public function getPricelistAjaxAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $data = array();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $tLists = new Table_Pricelists();
            $list = $tLists->find($id)->current();
            if ($list) {
                $data = $list->toArray();
                $date = new Zend_Date($data['valid'], null);
                $data['valid'] = $date->get('d.M.Y');
            }
        }
        $this->_helper->json($data);
    }


  public function importAction() {
    $adapter = new Zend_File_Transfer_Adapter_Http();
    $adapter->setDestination(APPLICATION_PATH . self::IMPORT_DIR);
    $files = $adapter->getFileInfo();
    $adapter->receive();
    if ($adapter->isUploaded($files['datafile']['name']) && $adapter->isValid($files['datafile']['name'])) {
      $fPath = $files['datafile']['destination'] . "/" . $files['datafile']['name'];
      $file = fopen($fPath, 'r');
      if ($file === FALSE) {
        $this->_flashMessenger->addMessage('Chyba: soubor se podařilo otevřít.');
        $this->redirect('/tobacco-pricelists');
      }
      $ids = explode(' ', trim($this->getRequest()->getParam('id_pricelists')));

      if (!$this->_request->getParam('update_only', FALSE)) {
        $tPrices = new Table_Prices();
        $tPrices->delete('id_pricelists IN (' . implode(', ', $ids) . ')');
      }

      $cols = count($ids) + 2;
      $row = 0;
      while ($data = fgetcsv($file, 1024, ';', '"')) {
        $row++;
        $num = count($data);
        if ($num != $cols) {
          $this->_flashMessenger->addMessage('Pozor: na řádku ' . $row . ' není správný počet sloupců.');
        }
        $this->_importPrice($data, $ids);
      }
    }
    $this->redirect('/tobacco-pricelists');
  }

  private function _importPrice($data, $idPriceLists) {
    $tPrices = new Table_Prices();
    $idPart = trim($data[0]);
    $namePart = trim($data[1]);
    if (!empty($idPart)) {
      foreach ($idPriceLists as $i => $idPriceList) {
        try {
          $tPrices->savePrice($idPart, $namePart, $idPriceList, Meduse_Currency::parse($data[$i + 2]));
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage('Cenu se nepodařilo naimportovat. ' . $e->getMessage());
        }
      }
    }
  }

    public function ajaxGetInfoAction() {
      $this->_helper->layout()->disableLayout();

      $ids = explode(' ', $this->getRequest()->getParam('ids'));
      $mapper = new Table_Pricelists();
      $this->view->pricelists = $mapper->getPricelists($ids);
    }

    public function exportAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNeverRender();

      $this->getResponse()
        ->setHeader('Content-Disposition', 'attachment; filename="export.csv"')
        ->setHeader('Content-type', 'application/octet-stream');

      $ids = explode(' ', $this->getRequest()->getParam('ids'));
      $mapper = new Table_Pricelists();
      $pricelists = $mapper->getPricelists($ids);

      foreach ($pricelists->getPrices() as $part_id => $prices) {
        echo $part_id . ';';
        $product = new Tobacco_Parts_Part($part_id);
        echo $product->getName() . ';';
        $price_array = array();
        foreach ($prices as $price) {
          if (isset($price)) {
            $price_array[] = $price;
          } else {
            $price_array[] = '';
          }
        }
        echo implode(';', $price_array);
        echo "\n";
      }
    }
}
