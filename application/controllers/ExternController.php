<?php
/**
 * Tento kontroler umoznuje lidem z externich skladu pristupovat do skladu meduse
 * a kontrolovat stav soucasti, ktere maji mit skladem.
 *
 * @author Jakub Niec
 */
class ExternController extends Meduse_Controller_Action {

    private $_extern_wh = 6; //6 = externi sklad BYSTRC

    
    public function init() {
      $this->_helper->layout->setLayout('bootstrap-basic');
    }
    /**
     * Prehled soucasti v externim skladu
     */
    public function indexAction() {

        $tPartsWarehouse = new Table_PartsWarehouse();

        $this->view->wh_data = $tPartsWarehouse->getFilteredWarehouseStatus(
            $this->_extern_wh,
            $this->_request->getParam('coll','null'),
            $this->_request->getParam('ctg','null'),
            $this->_request->getParam('name','null'),
            $this->_request->getParam('id','null'),
            $this->_request->getParam('prod','null'),
            $this->_request->getParam('producer','null'),
            $this->_request->getParam('no_multiparts','null')
        );
		/*
		$opts = array(
			'warehouse' => $this->_extern_wh,
            'collection_id' => $this->_request->getParam('coll','null'),
            'category_id' => $this->_request->getParam('ctg','null'),
            'name' => $this->_request->getParam('name','null'),
            'id' => $this->_request->getParam('id','null'),
            'is_product' => $this->_request->getParam('prod','null'),
            'producer' => $this->_request->getParam('producer','null'),
            'is_multipart' => $this->_request->getParam('no_multiparts','null'),
			'type' => Parts_Part::WH_TYPE_PIPES
		);
		$this->view->wh_data = $tPartsWarehouse->getFiltredWarehouseStatus($opts);*/
    }

    /**
     * Editace poctu soucasti na externim skladu
     */
    public function editAmountAction() {

        if($this->_request->getParam('id', false)) {
            $id = $this->_request->getParam('id', false);
        } else {
            $this->_redirect("/extern");
        }

        $form = new Parts_ExternAmountForm();
        $form->setLegend('Editace počtu součástí ' . $id);

        $whExtern = new Table_PartsWarehouseExtern();
        $whCentral = new Table_PartsWarehouse();
        if($this->_request->isPost() && $form->isValid($this->_request->getParams())) {
            $oldAmount = $whExtern->getAmount($this->_extern_wh, $form->meduse_part_id->getValue());
            $newAmount = (int) $form->meduse_part_amount->getValue();
            $centralAmount = $whCentral->getAmount($id);
            $authNamespace = new Zend_Session_Namespace('Zend_Auth');

            $diff = 0;
            if($oldAmount > $newAmount) {
                $diff = $oldAmount-$newAmount;
                $whExtern->removePart($this->_extern_wh, $form->meduse_part_id->getValue(), $diff);
                $diff *= -1;
            } else if($oldAmount < $newAmount) {
                $diff = $newAmount-$oldAmount;
                $whExtern->insertPart($this->_extern_wh, $form->meduse_part_id->getValue(), $diff);
            } else {
                //pokud $oldAmount = $newAmount, nedelej nic
            }



            if($diff != 0) {
                //Zaroven upravime pocet soucasti na centralnim skladu
                $newCentralAmount = $centralAmount + $diff;
                if($newCentralAmount < 0) {
                    $newCentralAmount = 0;
                }
                $whCentral->update(array(
                    'id_users' => $authNamespace->user_id,
                    'amount' => $newCentralAmount,
                ), "id_parts = ".$whExtern->getAdapter()->quote($form->meduse_part_id->getValue()));
            }

            $this->_flashMessenger->addMessage("Položka skladu upravena.");
            $this->_redirect('/extern');
        } else {
            $form->meduse_part_id->setValue($id);
            $form->meduse_part_amount->setValue($whExtern->getAmount($this->_extern_wh, $id));
        }

        $this->view->form = $form;
        $this->view->id = $id;
    }

}
