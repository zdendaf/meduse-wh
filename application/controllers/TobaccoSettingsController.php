<?php

/**
 * Nastavení v rámci skladu tabáku
 *
 * @author Vojtěch Mrkývka
 */
class TobaccoSettingsController extends Meduse_Controller_Action {
  public function init() {
    // nastavení designu
    $this->_helper->layout->setLayout('bootstrap-tobacco');
  }

  /*
   * Nastavení kurzu EUR/CZK
   */
  public function rateAction() {
    $this->view->form = new Tobacco_Settings_Rate_Form();
  }

  public function rateUpdateAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    if ($this->getRequest()->isPost()) {
      $form = new Tobacco_Settings_Rate_Form();
      if ($form->isValid($this->getRequest()->getPost())) {
        $tRate = new Table_ApplicationRate();
        $tRate->insert(array('rate' => $form->getValue('rate'), 'id_wh_type'=> '2'));
      }
    }
    $this->_redirect('/tobacco-settings/rate');
  }

  /*
   * Nastavení spotřební daně
   */
  public function exciseAction() {
    $tExcises = new Table_ApplicationExcise();
    $this->view->excises = $tExcises->fetchAll(null, array('active_from_date DESC', 'id DESC'))->toArray();
  }

  public function exciseDeleteAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $request = $this->getRequest();
    if ($request->isPost() && !is_null($id = $request->getParam('excise_id', null))) {
      $tExcises = new Table_ApplicationExcise();
      $tExcises->delete('id = ' . $id);
      $this->_flashMessenger->addMessage('Hodnota byla odstraněna.');
    }
    $this->_redirect('/tobacco-settings/excise');
  }

  public function exciseAddAction() {
    $form = new Tobacco_Settings_Excise_Form();
    $tExcises = new Table_ApplicationExcise();
    $request = $this->getRequest();

    if ($request->isPost()) {
      if ($form->isValid($data = $request->getPost())) {
        $zend_date = new Zend_Date($data['active_from_date']);
        $zend_date = $zend_date->toString('yyyy-MM-dd');

        // Update pro existující data
        if (is_null($tExcises->fetchAll("active_from_date = '$zend_date'")->current())) {
          $tExcises->insert(array('value' => $data['value'], 'active_from_date' => $zend_date));
          $this->_flashMessenger->addMessage('Hodnota byla přidána.');
        } else {
          $tExcises->update(array('value' => $data['value'], 'inserted' => date('Y-m-d')), "active_from_date = '$zend_date'");
          $this->_flashMessenger->addMessage('Hodnota byla upravena.');
        }

        $this->_redirect('/tobacco-settings/excise');
      }
      else {
        $this->_flashMessenger->addMessage('Všechna pole musí být správně vyplněna.');
        if (empty($data['active_from_date'])) {$data['active_from_date'] = date('d.m.Y');}
        $form->populate($data);
      }
    }
    else {
      $form->populate(array('active_from_date' => date('d.m.Y')));
    }

    $this->view->form = $form;
  }

  public function flavorKeysAction() {
    $tBatchFlavors = new Table_BatchFlavors();
    $this->view->flavors = $tBatchFlavors->fetchAll('active = "y"');
  }

  public function flavorKeysEditAction() {
    $tBatchFlavors = new Table_BatchFlavors();
    $form = new Tobacco_Settings_FlavorKeys_Edit_Form();
    $id = $this->getRequest()->getParam('id', null);

    //odeslaný formulář
    if ($this->getRequest()->isPost()) {
      $post = $this->getRequest()->getPost();
      if ($form->isValid($post)) {
        //testování zabraného ID
        if ($post['id']!=$post['id_new'] AND !is_null($tBatchFlavors->fetchRow('id = ' . $post['id_new']))) {
          $form->getElement('id_new')->addError('ID je již používáno.');
        } else {
          if ($post['id']) {
            $tBatchFlavors->update(array(
                'id' => $post['id_new'],
                'name' => $post['name'],
                'is_mix' => $post['is_mix'],
                    ), 'id = ' . $post['id']);
            $this->_flashMessenger->addMessage('Příchuť byla úspěšně upravena.');
          } else {
            $tBatchFlavors->insert(array(
                'id' => $post['id_new'],
                'name' => $post['name'],
                'is_mix' => ($post['is_mix'] == 'y') ? 'y' : 'n',
            ));
            $this->_flashMessenger->addMessage('Příchuť byla úspěšně přidána.');
          }
          $this->redirect('/tobacco-settings/flavor-keys/');
        }

      } else {
        $form->populate($post);
      }
    } else {
    //neodeslaný formulář
      if ($id) {
        $data = $tBatchFlavors->find($id)->current();
        if ($data) {
          $input = $data->toArray();
          $form->populate($input);
          $form->populate(array('id_new' => $input['id']));
        }
        else {
          $this->_flashMessenger->addMessage('Příchuť nenalezena.');
          $this->redirect('/tobacco-settings/flavor-keys');
        }
      }
    }
    if ($id) {
        $form->setLegend('Úprava popisu příchutě');
    }
    else {
      $form->setLegend('Vložení nové příchutě');
    }

    $this->view->form = $form;
  }

  public function flavorKeysDeleteAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    $request = $this->getRequest();
    if ($request->isPost() && !is_null($id = $request->getParam('id', null))) {
      $tSets = new Table_BatchFlavors();
      $tSets->update(array('active' => 'n'), 'id = ' . $id);
      $this->_flashMessenger->addMessage('Příchuť byla odebrána.');
    }
    $this->_redirect('/tobacco-settings/flavor-keys/');
  }

  public function recipesProportionAction() {
    if ($this->_request->isPost()) {
      $settings = Zend_Registry::get('settings');

      $settings->set('recipe_percentage_total_tobacco',
        $this->_request->getParam('recipe_percentage_total_tobacco'), $settings::TYPE_FLOAT);
      $settings->set('recipe_percentage_total_preservative',
        $this->_request->getParam('recipe_percentage_total_preservative'), $settings::TYPE_FLOAT);
      $settings->set('recipe_percentage_sugar_glycerin',
        $this->_request->getParam('recipe_percentage_sugar_glycerin'), $settings::TYPE_FLOAT);
      $settings->set('recipe_tobacco_id',
        $this->_request->getParam('recipe_tobacco_id'), $settings::TYPE_STRING);
      $settings->set('recipe_preservative_id',
        $this->_request->getParam('recipe_preservative_id'), $settings::TYPE_STRING);
      $settings->set('recipe_sugar_id',
        $this->_request->getParam('recipe_sugar_id'), $settings::TYPE_STRING);
      $settings->set('recipe_glycerin_id',
        $this->_request->getParam('recipe_glycerin_id'), $settings::TYPE_STRING);

      $this->_flashMessenger->addMessage('Nastavení bylo uloženo.');

      // Prepocitani aktivnich receptur podle novych hodnot.
      if ($this->_request->getParam('recipe_recalculate', 'n') === 'y') {
        $tRecipes = new Table_Recipes();
        $recipes = $tRecipes->getRecipesPairs();
        $cnt = 0;
        foreach ($recipes as $id => $name) {
          $recipe = new Tobacco_Recipes_Recipe($id);
          try {
            $recipe->save();
            $cnt++;
          }
          catch (Tobacco_Recipes_Exceptions_BadRatioException $e) {
            $this->_flashMessenger->addMessage('Nastal problém při přepočtu receptury ' . $name . ': ' . $e->getMessage());
          }
        }
        $this->_flashMessenger->addMessage('Bylo přepočítáno ' . $cnt . ' receptur.');
      }
      $this->redirect($this->view->url());
    }
    $form = new Tobacco_Recipes_ProporcionForm();
    $this->view->form = $form;
  }



  public function emailTemplatesAction() {
    $this->view->title = 'Správa e-mailových šablon';
		$tEmailTemplates = new Table_EmailTemplates();
		$this->view->templates = $tEmailTemplates
            ->fetchAll('id_wh_type = ' . Table_TobaccoWarehouse::WH_TYPE)
            ->toArray();
  }

  public function emailTemplatesAddAction() {
		$this->view->title = 'Vložení nové e-mailové šablony';
		$form = new Tobacco_EmailTemplates_Form();
		$form->setAction($this->view->url(array('action' => 'email-templates-save')));
		$this->view->form = $form;
	}

	public function emailTemplatesEditAction() {
		$tid = (int) $this->_request->getParam('id');
		$this->view->title = 'Úprava e-mailové šablony';
		$emailTemplates = new Table_EmailTemplates();
		$template = $emailTemplates->find($tid)->current()->toArray();
		$form = new Tobacco_EmailTemplates_Form();
		$form->setAction($this->view->url(array('action' => 'email-templates-save')));
		$form->populate(array(
			'email_template_id' => $template['id'],
			'email_template_name' => $template['name'],
			'email_template_subject' => $template['subject'],
			'email_template_body' => $template['body'],
      'default_for' => $template['default_for'],
		));
		$this->view->form = $form;
	}


	public function emailTemplatesSaveAction() {
		$params = $this->_request->getParams();

    $emailTemplates = new Table_EmailTemplates();
    //reset výchozí šablony, je-li nastavena
    if ($params['default_for']) {
      $emailTemplates->update(array('default_for' => 0), 'default_for = \'' . $params['default_for'] . '\' AND id_wh_type = \'' . Table_TobaccoWarehouse::WH_TYPE . '\'');
    }


		if ($params['email_template_id']) {
			// update existujici sablony
			$emailTemplates->update(array(
				'name' => $params['email_template_name'],
				'subject' => $params['email_template_subject'],
				'body' => $params['email_template_body'],
        'default_for' => $params['default_for'],
			), 'id = ' . $emailTemplates->getAdapter()->quote($params['email_template_id']));
		} else {
			// vytvoreni nove sablony
			$emailTemplates->insert(array(
				'name' => $params['email_template_name'],
				'subject' => $params['email_template_subject'],
				'body' => $params['email_template_body'],
        'id_wh_type' => Table_TobaccoWarehouse::WH_TYPE,
        'default_for' => $params['default_for'],
			));
		}
		$this->_redirect($this->view->url(array('action' => 'email-templates')));
	}

	public function emailTemplatesDeleteAction() {
		$tid = $this->_request->getParam('id');
		$emailTeplates = new Table_EmailTemplates();
		$result = $emailTeplates->delete('id = ' . $emailTeplates->getAdapter()->quote($tid));
		$this->_flashMessenger = $result ? 'Šablona byla vymazána' : 'Šablonu se nepodařilo vymazat';
		$this->_redirect($this->view->url(array('action' => 'email-templates')));
	}

  public function accountsListAction() {
    $tAccounts = new Table_Accounts();
    $this->view->accounts = $tAccounts->select()
      ->where('id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE)
      ->query()->fetchAll();
  }

  public function accountsEditAction() {
    $form = new AdminAccounts_Form(['id_wh_type' => Table_TobaccoWarehouse::WH_TYPE]);
    $tAccounts = new Table_Accounts();
    $id = $this->getRequest()->getParam('id');
    if ($id) {
      $data = $tAccounts->find($id)->current()->toArray();
      if ($data) {
        $form->populate($data);
        $form->setLegend('Editace bankovního účtu');
        $this->view->id = $id;
      }
      else {
        $this->_flashMessenger->addMessage('Účet nenalezen.');
        $this->redirect('/tobacco-settings/accounts-list');
      }
    }
    else {
      $form->setLegend('Vložení bankovního účtu');
    }
    $this->view->form = $form;
  }

  public function accountsSaveAction()
  {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    if ($this->getRequest()->isPost()) {
      $data = $this->getRequest()->getPost();
      $form = new AdminAccounts_Form(['id_wh_type' => Table_TobaccoWarehouse::WH_TYPE]);
      if ($form->isValid($data)) {
        $tAccounts = new Table_Accounts();
        //nastavování je-li účet výchozí
        if ($data['is_default'] == 'y') {
          $tAccounts->update(array('is_default' => 'n'), 'id_wh_type = ' . $data['id_wh_type']);
        }

        if ($data['id']) {
          //kontrola odebrání označení výchozího účtu (pro upozornění)
          $defEdit = $tAccounts->find($data['id'])->current()->toArray();

          $tAccounts->update(array(
            'id_wh_type' => $data['id_wh_type'],
            'designation' => $data['designation'],
            'name' => $data['name'],
            'address' => $data['address'],
            'iban' => $data['iban'],
            'bic' => $data['bic'],
            'account_prefix' => $data['account_prefix'],
            'account_number' => $data['account_number'],
            'bank_number' => $data['bank_number'],
            'currency' => $data['currency'],
            'is_default' => $data['is_default'],
            'payment_method' => $data['payment_method'] ? $data['payment_method'] : null,
          ), 'id = ' . $data['id']);

          $this->_flashMessenger->addMessage('Údaje byly aktualizovány.');

          //označení odebrání výchozího účtu
          if ($defEdit['is_default'] == 'y' && $data['is_default'] == 'n') {
            $this->_flashMessenger->addMessage('Bylo odebráno označení výchozího účtu.');
          }
        } else {
          $tAccounts->insert(array(
            'id_wh_type' => $data['id_wh_type'],
            'designation' => $data['designation'],
            'name' => $data['name'],
            'address' => $data['address'],
            'iban' => $data['iban'],
            'bic' => $data['bic'],
            'account_prefix' => $data['account_prefix'],
            'account_number' => $data['account_number'],
            'bank_number' => $data['bank_number'],
            'currency' => $data['currency'],
            'is_default' => $data['is_default'],
            'payment_method' => $data['payment_method'] ? $data['payment_method'] : null,
          ));
          $this->_flashMessenger->addMessage('Údaje byly vloženy.');
        }
      } else {
        $this->_flashMessenger->addMessage('Invalidní data');
      }
    }
    $this->redirect('/tobacco-settings/accounts-list');
  }

  public function accountsDeleteAction() {
    $request = $this->getRequest();
    if ($request->isPost() && !is_null($id = $request->getParam('account_id'))) {
      $tAccounts = new Table_Accounts();

      //ověření, zdali se nejedná o výchozí účet
      $defEdit = $tAccounts->find($id)->current()->toArray();

      $tAccounts->delete('id = ' . $id);
      $this->_flashMessenger->addMessage('Účet byl odstraněn.');

      if ($defEdit['is_default'] == 'y') {
        $this->_flashMessenger->addMessage('Bylo odebráno označení výchozího účtu.');
      }
    }
    $this->redirect('/tobacco-settings/accounts-list');
  }

}