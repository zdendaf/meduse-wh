<?php
/**
 * Multi-editace soucasti
 *
 * @author niecj
 */
class MultiEditController extends Meduse_Controller_Action{

  public function init() {
    $this->_helper->layout->setLayout('bootstrap-basic');
    $this->view->title = 'Multieditace produktů';
    parent::init();
  }
  
	public function indexAction() {
    $this->view->action = 'index';
		$tParts = new Table_Parts();

		if($this->_request->getParam('type')){
			$this->view->type = $this->_request->getParam('type');
			$select = $tParts->getDatalist(true);

			$select->where(
				"p.id_parts_ctg = 23" //pouze sety dymek
			);

			if($this->_request->getParam('type') == "craft-sets"){
				$select->join(
					array('c' => 'parts_collections'),
					"c.id_parts = p.id AND c.id_collections = 1",
					array()
				);
				$this->view->col = "Craft collection";
			} else if($this->_request->getParam('type') == "sepia-sets"){
				$select->join(
					array('c' => 'parts_collections'),
					"c.id_parts = p.id AND c.id_collections = 2",
					array()
				);
				$this->view->col = "Sepia collection";
			} else { 
				$select->join(
					array('c' => 'parts_collections'),
					"c.id_parts = p.id AND c.id_collections = 3",
					array()
				);
				$this->view->col = "Nautila collection";
			}
			$this->view->data = $tParts->fetchAll($select);      
		}
    
		$multi_edit = new Products_MultiEdit();
		$multi_edit->setStep(1);
		$multi_edit->saveToSession();

	}

	public function step2Action() {
    $this->view->action = 'step2';
		$categories = new Table_PartsCtg();
		$this->view->categories = $categories->getCategories();
		$this->view->part = new Parts_Part($this->_request->getParam('id'));

    $this->view->addItemForm = new Parts_MultiEditAddForm();
    
		//multiedit
		$multi_edit = new Products_MultiEdit();
		$multi_edit->setStep(2);
		$multi_edit->setProduct($this->view->part->getID());
		$multi_edit->saveToSession();
	}

	public function step3Action() {

		$this->view->action = 'step3';
    $multi_edit = new Products_MultiEdit();
		$multi_edit->setStep(3);

		switch($this->_request->getParam('type')){
			case "delete-part":
				$multi_edit->setAction(Products_MultiEdit::DELETE);
				$part = new Parts_Part($this->_request->getParam('part'));
				$multi_edit->setPartAmount(0);
				$multi_edit->setPart($part->getID());
				break;

			case "add-part":
				if( //validace formulare
					!$this->_request->getParam('part') ||
					!$this->_request->getParam('amount') ||
					(int) $this->_request->getParam('amount') < 0
				){
					$this->_flashMessenger->addMessage("Špatně vyplněný formulář.");
					$multi_edit->setStep(2);
					$multi_edit->saveToSession();
					$this->_redirect('multi-edit/add');
					return;
				}
        try {
          $part = new Parts_Part($this->_request->getParam('part'));
        } catch (Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
          $this->_redirect('multi-edit/step2/id/' . $multi_edit->getProduct());
        }
				$multi_edit->setAction(Products_MultiEdit::ADD);
				$new_amount = (int) $this->_request->getParam('amount');
				$multi_edit->setNewPartAmount($new_amount);
				$multi_edit->setPart($part->getID());
				break;
			
      default:
				break;
		}

		$multi_edit->saveToSession();
		$this->view->products = $multi_edit->getPossibleChangedProducts();
	}

	public function step4Action(){
    
    $this->view->action = 'step4';
		$multi_edit = new Products_MultiEdit();
		$multi_edit->setStep(4);
		$db = $this->_db;

		if ($this->_request->getParams() && $this->_request->isPost() 
        && !$this->_request->getParam('confirm')){
			$products = array();
			foreach($this->_request->getParams() as $name => $value){
				if(substr($name, 0, 7) == 'product'){
					$products[]=$value;
				}
			}
			$multi_edit->setProducts($products);
			$multi_edit->saveToSession();
		} elseif ($this->_request->isPost() 
        && $this->_request->getParam('confirm')) {
			//uklada se multizmena
			switch($multi_edit->getAction()){
				case Products_MultiEdit::ADD:
					try{
						$db->beginTransaction();
						foreach($multi_edit->getSelectedProducts() as $product_id){
							$db->insert("parts_subparts", array(
								'id_multipart'	=> $product_id,
								'id_parts'		=> $multi_edit->getPart(),
								'amount'		=> $multi_edit->getNewPartAmount()
							));
						}
						$db->commit();
					} catch ( Exception $e ) {
						$db->rollback();
						$this->_flashMessenger->addMessage("Uprava se nezdařila.");
						$this->_redirect('multi-edit');
						return;
					}
					break;
				case Products_MultiEdit::DELETE:
					try{
						$db->beginTransaction();
						foreach($multi_edit->getSelectedProducts() as $product_id){
							$db->delete(
								"parts_subparts",
								"id_multipart = '" . $product_id . "' AND id_parts = '" 
                  . $multi_edit->getPart()."'" 
							);
						}
						$db->commit();
					} catch ( Exception $e ) {
						$db->rollback();
						$this->_flashMessenger->addMessage("Uprava se nezdařila.");
						$this->_redirect('multi-edit');
						return;
					}
					break;
			}
			$this->_flashMessenger->addMessage("Úprava " 
        . count($multi_edit->getSelectedProducts()) . " produktu provedena.");
			$this->_redirect('multi-edit');
		}
    
    $question = 'Opravdu chcete ';
    switch ($multi_edit->getAction()) {
      case Products_MultiEdit::ADD:
        $question .= "přidat " . $multi_edit->getNewPartAmount() 
          . " ks součásti " . $multi_edit->getPart(). " k těmto produktům?";
        break;
      case Products_MultiEdit::EDIT:
        $question .= "upravit počet součásti " . $multi_edit->getPart() 
          . " z " . $multi_edit->getPartAmount() . " ks na " 
          . $multi_edit->getNewPartAmount() . " ks u těchto produktů?";
        break;
      case Products_MultiEdit::DELETE:
        $question .= "odstranit součást " . $multi_edit->getPart() 
          . " z těchto produktů?";
        break;
    }
    $this->view->question = $question;
    $this->view->selected_products = array();
    foreach ($multi_edit->getSelectedProducts() as $id) {
      $product = new Parts_Part($id);
      $this->view->selected_products[$id] = $product->getName();
    }
	}

}
