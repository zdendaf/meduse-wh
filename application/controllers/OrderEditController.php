<?php
class OrderEditController extends Meduse_Controller_Action {

  const
    ACTION_INDEX = 'index',
    ACTION_CUSTOMER = 'customer',
    ACTION_PAYMENT = 'payment',
    ACTION_TRANSPORT = 'transport',
    ACTION_FEES = 'fees',
    ACTION_SERVICES = 'services',
    ACTION_PRODUCTS = 'products';

  const AUTOMATIC_ORDER_TITLE = 'N/A - dle zákazníka';

  /** @var Orders_Order $order */
  protected ?Orders_Order $order = NULL;
  protected Orders_Form $form;

	public function init() {
		$this->_helper->layout->setLayout('bootstrap-basic');
		$this->view->action = $this->_request->getActionName();
		$orderId = (int) $this->_request->getParam('order');
		$this->view->order = $orderId;

		$this->form = new Orders_Form([
      'from_action' => $this->view->action,
      'action' => $this->view->url(['action' => 'process']),
    ]);

		if ($this->view->order) {
      try {
        $order = new Orders_Order($this->view->order);
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
        $this->redirect('/orders');
        return;
      }

      $this->order = $order;
			$this->view->title = $order->getNO().'. '.$order->getTitle();
			$this->view->orderObj = $order;
      if ($order->hasReservedProducts()) {
        $this->view->allow_edit = FALSE;
        $this->view->allow_edit_note = 'Objednávku nelze plně editovat, protože má rezervované produkty. Pro umožnění plné editace kontaktujte vedoucího skladu.';
      }
      else {
        $this->view->allow_edit = TRUE;
      }

      $this->form->setupOrder($order);

      $status = $order->getStatus();
      $this->view->showProducts = (
              $status != Table_Orders::STATUS_OPEN
              || Utils::isAllowed('orders/edit-opened-products'))
              && $status != Table_Orders::STATUS_CLOSED;
		}
		else {
			$newOrder = new Zend_Session_Namespace('new_order');
			if($newOrder->new_order) {
				$this->form->populate($newOrder->new_order);
				$newOrder->new_order = null;
			}
      $this->form->setNextNo(1);
			$this->view->title = 'Nová objednávka';
		}



	}

	public function indexAction() {

		$this->form->showIndexElements();

    // editace jiz zalozene obejdnavky
    if ($this->view->order) {

      // test, jestli je objednavka produktova nebo sluzby
      $order = new Orders_Order($this->view->order);

      // Objednavka produktů.
      if (!$order->isService()) {
        // Odebrani selektu pro zmenu stavu
        if ($order->hasReservedProducts() || $order->isClosed()) {
          $this->form->removeElement('meduse_orders_status');
        }
      }
    }

    if (Utils::isDisAllowed('orders/special/status')){
        $this->form->removeElement('meduse_orders_status');
    }
    $this->view->showProductsTransport = isset($this->order) && !$this->order->isService();
		$this->view->form = $this->form;
	}

	public function paymentAction() {
    if (!isset($this->order)) {
      $this->redirect('/order-edit/index');
    }
		$this->form->showPaymentElements();
    if (!$this->order->isService()) {
      $this->view->showProductsTransport = isset($this->order);
    }
		$this->view->form = $this->form;
		$this->render('index');
	}

	public function transportAction() {
		$this->form->showTransportElements();
    $this->view->showProductsTransport = isset($this->order) && !$this->order->isService();
		$this->view->form = $this->form;
		$this->render('index');
	}

	public function customerAction() {
    if (!isset($this->order)) {
      $this->redirect('/order-edit/index');
    }
    $customer_id = $this->order->getRow()->id_customers;
    if(!is_null($customer_id) && $customer_id != 0) {
      $customer = new Customers_Customer($customer_id);
			$this->view->customer = $customer;
      $this->view->addresses = $customer->getAddresses();
      $this->view->address = $this->order->getAddress();
      $this->view->order = $this->order;
		}
    else {
      $params = $this->getRequest()->getParams();
      $params['order_id'] = $this->order->getRow()->id;
      $params['wh'] = Table_PartsWarehouse::WH_TYPE;
      $this->view->grid = Customers::getDataGrid($params);
    }
    $this->view->showProductsTransport = isset($this->order) && !$this->order->isService();
	}

  public function feesAction() {
    if (!isset($this->order)) {
      $this->redirect('/order-edit/index');
    }
    $this->form->showFeesElements();
    if (!$this->order->isService()) {
      $this->view->showProductsTransport = isset($this->order);
    }
    $this->view->form = $this->form;
    $this->render('index');
  }

	public function processAction() {

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$action = $this->_request->getParam('from_action',false);
		$ord = $this->_request->getParam('order');
		//zpracovani vsech formularu
		if ($this->_request->isPost() && $this->_request->getParam('from_action',false)) {

			switch($action){
        case self::ACTION_INDEX:
					$this->form->showIndexElements();
					break;
        case self::ACTION_PAYMENT:
					$this->form->showPaymentElements();
					break;
        case self::ACTION_TRANSPORT:
					$this->form->showTransportElements();
					break;
        case self::ACTION_FEES:
          $this->form->showFeesElements();
          break;
				default:
					$this->redirect('/orders');
					break;
			}

      $valid_no = TRUE;
      // Pokud jsme na prvni straně formuláře, musíme zkontrolovat unikátnost čísla objednávky.
      if ($this->_request->getParam('from_action') == 'index') {
        $no = $this->_request->getParam('meduse_orders_no');
        $id = $this->_request->getParam('order');
        $valid_no = Orders_Order::isValidNo($no, $id, Table_PartsWarehouse::WH_TYPE);
      }

      $params = $this->_request->getParams();
      $isValid = $this->form->isValid($params);
			if ($isValid && $valid_no) {

        $formData = $this->form->getValues();
			  $orderRow = $this->mapOrderFromForm($formData, $this->view->order);

        // Kontrola zmeny deadlinu objednavky.
        $deadlineEmailData = [];
        if ($ord && $action === self::ACTION_INDEX) {
          try {
            $orderObj = new Orders_Order($ord);
            $deadlineOld = (string) Meduse_Date::dbToForm($orderObj->getDeadline());
            $deadlineNew = (string) $formData['meduse_orders_deadline'];
            if ($deadlineOld !== $formData['meduse_orders_deadline'] . '') {
              $deadlineEmailData = [
                'id' => $orderRow->id,
                'title' => $orderRow->title,
                'no' => $orderRow->no,
                'deadlineOld' => $deadlineOld,
                'deadlineNew' => $deadlineNew
              ];
            }
          }
          catch (Exception $e) {
            Utils::logger()->err($e->getMessage());
          }
        }

				$status = $this->_request->getParam('meduse_orders_status', false);
        $orderObj = null;
        try {
          $orderObj = new Orders_Order($orderRow->id);
          $statusOld = $orderObj->getStatus();
        } catch (Exception $e) {
          $statusOld = NULL;
        }
        //status menime pouze, kdyz se ruzni od predesleho
        if ($orderRow->status != $statusOld) {

            // pokud menime stav otevrene objednavky,
            // do jineho stavu, nez expedovana
            // musime vyskladnit rezervovane soucastky
            if ($statusOld == Table_Orders::STATUS_OPEN && $status != Table_Orders::STATUS_CLOSED) {
              $orders =  new Table_Orders();
              $orders_parts = $orders->getOrderParts($orderRow->id);
              foreach($orders_parts as $category) {
                foreach($category as $row) {
                  try {
                    $orderObj->cancelAllPrepared($row['id']);
                  } catch (Parts_Exceptions_LowAmount $e) {}
                }
              }
            }
            switch($status) {

                case Table_Orders::STATUS_OFFER:
                    $orderRow->status = Table_Orders::STATUS_OFFER;
                    break;

                case Table_Orders::STATUS_STRATEGIC:
                    $orderRow->status = Table_Orders::STATUS_STRATEGIC;
                    $orderRow->strategic_start = date('Y-m-d');
                    $orderRow->strategic_priority = 5;
                    $orderRow->strategic_months = 1;
                    break;

                case Table_Orders::STATUS_CONFIRMED:
                    if ($orderRow->type == Table_Orders::TYPE_PRODUTCS) {
                      if (is_null($orderObj->getCustomer())) {
                        $this->_flashMessenger->addMessage('Zakázku nelze otevřít, není přiřazený zákazník!');
                        $this->redirect($this->view->url(array('action' => $action, 'order' => $ord)));
                      }
                    }
                    $orderRow->status = Table_Orders::STATUS_CONFIRMED;
                    $orderRow->date_confirm = date('Y-m-d');
                    break;

                case Table_Orders::STATUS_OPEN:
                    $orderRow->status = Table_Orders::STATUS_OPEN;
                    $orderRow->date_open = date('Y-m-d');
                    break;

                case Table_Orders::STATUS_CLOSED:
                    $orders   =  new Table_Orders();
                    $products = $orders->getOrderParts($orderObj->getID());
                    if (empty($products)) {
                      $this->_flashMessenger->addMessage('Objednávku nelze expedovat, nemá přiřazeny žádné produkty.');
                      $this->redirect($this->view->url(array('action' => $action, 'order' => $ord)));
                    }
                    if (!$orderObj->isReady()) {
                      $this->_flashMessenger->addMessage('Objednávku nelze expedovat, nejsou rezervovány všechny položky.');
                      $this->redirect($this->view->url(array('action' => $action, 'order' => $ord)));
                    }
                    $orderRow->status = Table_Orders::STATUS_CLOSED;
                    $orderRow->date_exp = date('Y-m-d');
                    break;

                case Table_Orders::STATUS_TRASH:
                    $orderRow->status = Table_Orders::STATUS_TRASH;
                    $orderRow->date_delete = date('Y-m-d');
                    break;

                default:
                    break;
            }
        }

        $this->addMessage($action);
				$orderRow->save();

        if ($orderObj && $action === self::ACTION_FEES) {
          $feesToAdd = [];
          $feesToRemove = [];
          $tFees = new Table_ApplicationFees();
          if ($list = $tFees->list(true)) {
            foreach ($list as $item) {
              if ($formData['fee_' . $item['id']] === 'y') {
                $feesToAdd[] = $item['id'];
              }
              else {
                $feesToRemove[] = $item['id'];
              }
            }
          }
          if ($feesToAdd) {
            $orderObj->setFees($feesToAdd);
          }
          if ($feesToRemove) {
            $orderObj->unsetFees($feesToRemove);
          }
        }

        /**
         * @see #94
         * Zmena meny objednavky musi spustit prepocet ocekavane celkove castky.
         */
        if ($orderObj && in_array($action, array(self::ACTION_PAYMENT, self::ACTION_TRANSPORT))){
          try {
            $orderObj = new Orders_Order($orderRow->id);
            $orderObj->recalculateExpectedPayment();
          }
          catch (Exception $e) {}
        }
        elseif ($orderObj) {
          $orderObj->invalidInvoices();
        }

        // Pokud je vse ulozeno a doslo ke zmene deadlinu, odesleme email.
        if ($deadlineEmailData) {
          $tEmails = new Table_Emails();
          $tEmails->sendEmail(Table_Emails::EMAIL_ORDER_DEADLINE_NOTICE, $deadlineEmailData);
        }

				$this->_flashMessenger->addMessage("Uloženo");
				$this->redirect($this->view->url(array('action' => $action, 'order' => $orderRow->id)));
			}
      else {
        if (!$valid_no) {
          $this->_flashMessenger->addMessage('Objednávka s číslem ' . $this->_request->getParam('meduse_orders_no') . ' již existuje.');
        }
				$this->_flashMessenger->addMessage("Formulář nebyl uložen. Vyplňte správně požadované položky:<br />");

				foreach ($this->form->getErrors() as $field => $error) {
					foreach ($error as $what) {
						if ($field == 'meduse_orders_no' && $what == 'isEmpty') {
							$this->_flashMessenger->addMessage('Chybí číslo zakázky.');
						}
						if ($field == 'meduse_orders_type' && $what == 'isEmpty') {
							$this->_flashMessenger->addMessage('Chybí typ zakázky.');
						}
						if ($field == 'meduse_orders_name' && $what == 'isEmpty') {
							$this->_flashMessenger->addMessage('Chybí název zakázky.');
						}
						if ($field == 'meduse_orders_deadline' && $what == 'minimum') {
							$this->_flashMessenger->addMessage('Deadline nesmí být v minulosti.');
						}
          }
				}

				if(!$this->view->order) {
					$ord = null;
				}
				$newOrder = new Zend_Session_Namespace('new_order');
				$newOrder->new_order = $this->form->getValues();
				$this->redirect($this->view->url(array('action' => $action, 'order' => $ord)));
			}
		}
	}

	public function generateProformaNo(){
		return Orders_Order::getNextProformaNo();
	}

	private function mapOrderFromForm($formData, $orderId) {
		$tOrders = new Table_Orders();
		if (empty($orderId)) {
			$rOrder = $tOrders->createRow();
			$rOrder->inserted = new Zend_Db_Expr("NOW()");
			$rOrder->date_request = new Zend_Db_Expr("NOW()");
			$authNamespace = new Zend_Session_Namespace('Zend_Auth');
			$rOrder->owner = $authNamespace->user_id;
		}
		else {
			$rOrder = $tOrders->find($orderId)->current();
		}

		$formFields = array(
      // 'private' => "meduse_orders_private",
			'no' => "meduse_orders_no",
      'type' => "meduse_orders_type",
			'title' => "meduse_orders_name",
			'date_deadline' => "meduse_orders_deadline",
      'date_deadline_internal' => "meduse_orders_deadline_internal",
			'description' => "meduse_orders_desc",
      'coo_requirement' => "meduse_coo_requirement",
      'invoice_verify_requirement' => "meduse_invoice_verify_requirement",
      'di' => 'meduse_di',
      'status' => "meduse_orders_status",
      'id_pricelists' => "meduse_orders_pricelist",
			'payment_method' => "meduse_orders_payment_method",
			'expected_payment' => "meduse_orders_expected_payment",
      'expected_payment_rate' => "meduse_orders_expected_payment_rate",
			'expected_payment_deposit' => "meduse_orders_expected_payment_deposit",
			'expected_payment_currency' => "meduse_orders_expected_payment_currency",
			'id_orders_carriers' => "meduse_orders_carrier",
			'carrier_price' => "meduse_orders_carrier_price",
			'discount' => "meduse_orders_discount",
			'discount_type' => "meduse_orders_discount_type",
			'date_pay_all' => "meduse_orders_pay_all",
			'date_pay_deposit' => "meduse_orders_pay_deposit"
		);

    foreach ($formFields as $dbName => $fieldName) {
      if (isset($formData[$fieldName])) {
        if (in_array($fieldName, array(
          'meduse_orders_deadline',
          'meduse_orders_deadline_internal',
          'meduse_orders_pay_all',
          'meduse_orders_pay_deposit'
        ))){
          if (!empty($formData[$fieldName])){
            $date = new Zend_Date($formData[$fieldName], 'dd.MM.yyyy');
            $rOrder->{$dbName} = $date->toString('yyyy-MM-dd');
          }
          else {
            $rOrder->{$dbName} = new Zend_Db_Expr('NULL');
          }
        }
        elseif ($fieldName == 'meduse_orders_carrier_price') {
          if (trim($formData[$fieldName]) == '') {
            $rOrder->{$dbName} = new Zend_Db_Expr('NULL');
          }
          else {
            $rOrder->{$dbName} = (float) $formData[$fieldName];
          }
        }
        elseif ($fieldName == 'meduse_orders_pricelist') {
          $rOrder->{$dbName} = $formData[$fieldName] == 'null' ? new Zend_Db_Expr('NULL') : (int) $formData[$fieldName];
        }
        elseif ($fieldName == 'meduse_orders_payment_method') {
          $rOrder->{$dbName} = $formData[$fieldName] == 'null' ? new Zend_Db_Expr('NULL') : $formData[$fieldName];
        }
        elseif ($fieldName == 'meduse_orders_expected_payment_rate') {
          $rOrder->{$dbName} = $formData['meduse_orders_expected_payment_rate_use_default'] == 'y' ? new Zend_Db_Expr('NULL') : (float) $formData[$fieldName];
        }
        else {
          $rOrder->{$dbName} = $formData[$fieldName];
        }
      }
    }
		return $rOrder;

	}

	public function ajaxCustomerAction() {
    $table = new Table_Customers();
    $results = $table->getPossibleNames($this->_request->getParam('term'), 1);
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->json($results);
	}

  public function assignCustomerAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $customerId = $this->_request->getParam('customer', false);
    $order = $this->_request->getParam('order', false);
    if ($order !== false && $customerId !== false) {

      $customer = new Customers_Customer($customerId);
      $this->order->getRow()->id_customers = $customerId;
      $this->_flashMessenger->addMessage('Objednávce byl přiřazen zákazník.');


      $billingAddressId = $customer->getBillingAddressId();
      $tAddress = new Table_Addresses();
      $addressRow = $tAddress->getAddress($billingAddressId);
      if ($addressRow['country'] === Table_Addresses::COUNTRY_CZ) {
        $this->order->setExpectedCurrency(Table_Orders::CURRENCY_CZK);
      }
      else {
        $this->order->setExpectedCurrency(Table_Orders::CURRENCY_EUR);
      }

      $this->order->getRow()->purchaser = $customer->isB2B() ? 'business' : 'personal';

      $addressId = $customer->getShippingAddressId();
      $this->order->getRow()->id_addresses = $addressId ?? $billingAddress;

      $this->order->getRow()->coo_requirement = $customer->isRequiredCoo() ? 'y' : 'n';
      $this->order->getRow()->invoice_verify_requirement = $customer->isRequiredInvoiceVerification() ? 'y' : 'n';

      // Automaticke nastaveni nazvu objednavky.
      if ($this->order->getTitle() == self::AUTOMATIC_ORDER_TITLE) {
        $this->order->getRow()->title = $this->getAutomaticTitle();
      }

      if (!$priceList = $customer->getPriceList()) {
        $this->_flashMessenger->addMessage('Pozor, zákazník ' . $customer->getName() . ' nemá přiřazen platný ceník. Zvolte ceník v objednávce.');
      }
      $this->order->assignPriceList($priceList['id'], FALSE);

      $this->order->getRow()->save();
      $this->addMessage(self::ACTION_CUSTOMER, 'přiřazen zákazník ' . $customer->getName());
      $this->redirect($this->view->url([
        'action' => 'customer',
        'customer' => null,
      ]));
    }
    $this->_flashMessenger->addMessage('Nebylo zadáno ID zákazníka nebo objednávky.');
    $this->redirect('/orders');
  }

	public function removeCustomerAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    if (false !== $this->_request->getParam('order', false) && false !== $this->_request->getParam('customer', false)) {

      // Automaticke nastaveni objednavky.
      if ($this->order->getTitle() ==  $this->getAutomaticTitle()) {
        $this->order->getRow()->title = self::AUTOMATIC_ORDER_TITLE;
      }

      $this->order->getRow()->id_customers = new Zend_Db_Expr('NULL');
      $this->order->getRow()->id_addresses = new Zend_Db_Expr('NULL');
      $this->order->getRow()->purchaser = new Zend_Db_Expr('NULL');
      $this->order->getRow()->id_pricelists = new Zend_Db_Expr('NULL');
      $this->order->getRow()->save();
      $this->addMessage(self::ACTION_CUSTOMER, 'odebrán zákazník');
      $this->_flashMessenger->addMessage("Přiřazení zákazníka bylo zrušeno.");
      $this->redirect($this->view->url(array('action' => 'customer', 'customer' => null)));
    }
    $this->redirect("/orders");
	}

  private function addPricelists($pricelists) {
    foreach ($pricelists as $list) {
      $this->form->meduse_orders_pricelist->addMultiOption($list['id'], $list['name']);
    }
  }


  public function servicesAction() {
    if (!isset($this->order)) {
      $this->redirect('/order-edit/index');
    }
    if ($this->_request->isPost()) {
      $data = $this->getAllParams();
      $i = 0;
      $services = array();
      while($i < $this->getParam('rowsnum')) {
        $services[] = array(
          'id' => empty($this->getParam('id_' . $i)) ? NULL : $this->getParam('id_' . $i),
          'description' => $this->getParam('description_' . $i),
          'price' => $this->getParam('price_' . $i),
          'qty' => $this->getParam('qty_' . $i),
          'delete' => $this->getParam('delete_' . $i) == 'y',
        );
        $i++;
      }
      $order = new Orders_Order($data['id_orders']);
      if ($order->setServices($services)) {
        $this->addMessage(self::ACTION_SERVICES);
        $this->_flashMessenger->addMessage('Služby byly uloženy');
        $total = $order->getServicesTotalPrice();
        $order->setExpectedPayment($total);
        $order->invalidInvoices();
      }
      else {
        $this->_flashMessenger->addMessage('Při ukládání došlo k chybě.');
      }
    }
    else {
      $order = new Orders_Order($this->getParam('order'));
    }
    $services = $order->getServices();
    $form = new Orders_ServicesForm(array('rows' => count($services)));
    $data = array('id_orders' => $this->getParam('order'), 'rows' => $services);
    $form->populate($data);
    $this->view->form = $form;
  }

  public function setAddressAction() {
    $orderId = $this->getParam('order');
    $addressId = $this->getParam('address');
    if ($orderId && $addressId) {
      $order = new Orders_Order($orderId);
      $address = new Addresses_Address($addressId);
      $order->setAddress($address);
    }
    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
    $this->redirect('/order-edit/customer/order/' . $orderId);
  }

  protected function addMessage($action, $text = NULL) {
	  if (!$this->order) {
	    return;
    }
    $status = $this->order->getStatus();
    if (!in_array($status, Orders_Order::NOTIFIED_STATES)) {
      return;
    }
    /** @var EmailFactory_Queue $queue */
    if (!$this->order->isPrivate()) {
      $queue = $this->order->getQueue();
      switch ($action) {
        case self::ACTION_INDEX:
          $queue->add('Základní údaje');
          break;
        case self::ACTION_PAYMENT:
          $queue->add('Platební údaje');
          break;
        case self::ACTION_TRANSPORT:
          $queue->add('Přepravní údaje');
          break;
        case self::ACTION_CUSTOMER:
          $queue->add('Údaje o zákazníkovi: ' . $text);
          break;
        case self::ACTION_SERVICES:
          $queue->add('Položky služeb');
          break;
        case self::ACTION_PRODUCTS:
          $queue->add('Produkt: ' . $text);
          break;
      }
    }
  }

  protected function getAutomaticTitle() {
    $tCustomer = new Table_Customers();
    $rCustomer = $tCustomer->find($this->order->getRow()->id_customers)->current();
    return $rCustomer->company ? $rCustomer->company : implode(' ', array($rCustomer->first_name, $rCustomer->last_name));
  }
}
