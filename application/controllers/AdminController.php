<?php
class AdminController extends Meduse_Controller_Action {


  public function init(){
      $this->_helper->layout->setLayout('bootstrap-basic');
  }


  public function indexAction(){
	}

	public function computePricesAction(){
		ini_set('max_execution_time', '0');

		$this->_db->query("
			CREATE TABLE IF NOT EXISTS tmp_parts (
				`parts_id` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
				`price` decimal(12,2) NOT NULL,
				`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (`parts_id`)
			);
		");

		$this->view->total = $this->_db->fetchOne("SELECT COUNT(*) FROM parts");
		$this->view->ready = $this->_db->fetchOne("SELECT COUNT(*) FROM tmp_parts");

		$dataToProcess = $this->_db->fetchAll("
			SELECT p.id, t.* FROM
			parts AS p
			LEFT JOIN tmp_parts AS t ON p.id = t.parts_id
			WHERE t.parts_id IS NULL
			ORDER BY p.multipart
		");

		if($this->_request->getParam('confirm')){
			foreach($dataToProcess as $row){
				$part = new Parts_Part($row['id']);
				$part->refreshPrice();
				$this->_db->insert("tmp_parts", array(
					'parts_id' => $row['id'],
					'price' => $part->getPrice()
				));
			}
		}
    
		$this->view->data = $this->_db->fetchAll("SELECT * FROM tmp_parts");
	}
  
	public function emailTestAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		try{
			$tEmails = new Table_Emails();
			//$tEmails->sendEmail(Table_Emails::EMAIL_ORDER_CONFIRM, array('orderId' => 123));
			$tEmails->sendEmail(Table_Emails::EMAIL_PRODUCTION_CREATION, array('productionId' => 152));
			$this->_flashMessenger->addMessage("Email odeslan");
		} catch (Exception $e) {
			$this->_flashMessenger->addMessage("CHYBA: ".$e->getMessage());
		}
    
		$this->_redirect("/admin");
	}


    /**
     * Settings DPH
     */
    public function settingsDphAction() {
        // set layout
        $this->_helper->layout->setLayout('bootstrap-basic');

        /**
         * Get params
         */
        $act = $this->_getParam('act');
        $gid = $this->_getParam('id');
        if (isset($act) && isset($gid)) {
            // delete
            try {
                $table = new Table_ApplicationDph();
                $table->delDphValue($gid);

                // message
                $this->_flashMessenger->addMessage("DPH bylo odstraněno");

                // redirect out
                $this->redirect('admin/settings-dph');
            }
            catch (Zend_Exception $e) {
                echo $e->getMessage();
            }
        }


        /**
         * Form
         */
        $form = new AdminDph_Form();
        $form->setLegend('Nastavení DPH');

        /**
         * Get data
         */
        $table = new Table_ApplicationDph();
        $select = $table->getData();

        /**
         * Inicialize datagrid
         *
         * columns:
         *  value, active_from_date, delete
         */
        $grid = new ZGrid([
            'title' => _("Seznam DPH"),
            'add_link_url' => $this->view->url([
                'module' => 'default',
                'controller' => 'admin',
                'action' => 'settings-dph',
                'act' => 'add',
            ], null, true),
            'allow_add_link' => true,
            'add_link_title' => _('Přidat nový'),
            'columns' => [
                'code_c_country' => [
                    'header' => _('Země'),
                    'css_style' => 'text-align: left',
                    'sorting' => true,

                ],
                'value' => [
                    'header' => _('Hodnota [%]'),
                    'css_style' => 'text-align: left',
                    'sorting' => true,

                ],
                'active_from_date' => [
                    'header' => _('Aktivní od'),
                    'renderer' => 'date',
                    'css_style' => 'text-align: left',
                    'sorting' => true,

                ],
                'delete' => [
                    'header' => '',
                    'css_style' => 'text-align: center',
                    'renderer' => 'action',
                    'type' => 'delete',
                    'url' => $this->view->url([
                        'module' => 'default',
                        'controller' => 'admin',
                        'action' => 'settings-dph',
                        'act' => 'del',
                        'id' => '{id}',
                    ], null, true),
                ],
            ],

        ]);

        $grid->setSelect($select);
        $grid->setRequest($this->getRequest()->getParams());


        /**
         * Send form
         */
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()
                ->getPost())) {
            // set param
            try {
                $date = new Zend_Date($form->getValue('active_from_date'));
                $table->addDphValue($form->getValue('dph'), $date->toString('yyyy-MM-dd'), $form->getValue('code_c_country'));

                // message
                $this->_flashMessenger->addMessage("DPH bylo přidáno");

                // redirect out
                $this->redirect('admin/settings-dph');
            }
            catch (Zend_Exception $e) {
                echo $e->getMessage();
            }
        }


        /**
         * To view
         */
        $this->view->form = (($act == 'add') ? $form : '');
        $this->view->grid = $grid;
    }


    public function usersListAction()
        {
            // set layout
            $this->_helper->layout->setLayout('bootstrap-basic');

            /**
             * Users list data
             */
            $tUsers = new Table_Users();
            $users_select = $tUsers->getList(true);

            $authNamespace = new Zend_Session_Namespace('Zend_Auth');

            /**
             * Inicialize datagrid
             */            
            $grid = new ZGrid(array(
                'title' => _("Uživatelé") ,
                'add_link_url' => $this->view->url(array(
                    'module' => 'default' , 'controller' => 'admin' ,
                    'action' => 'user-add'
                ), null, true) ,
                'allow_add_link' => Zend_Registry::get('acl')->isAllowed('admin/user-add'),
                'add_link_title' => _('Přidat uživatele'),
                'columns' => array(
                        'id' => array(
                            'header' => _('ID') ,
                            'css_style' => 'text-align: left' ,
                            'sorting' => true
                        ),
                        'check_ip' => array(
                            'header' => _('Kontrola IP') ,
                            'sorting' => false,
                            'renderer' => 'Flag',
                            'img_on' => '/public/images/checked.png',
                            'img_off' => '/public/images/unchecked.png',
                        ),
                        'is_active' => array(
                            'header' => _('Aktivní') ,
                            'sorting' => false,
                            'renderer' => 'Flag',
                            'img_on' => '/public/images/checked.png',
                            'img_off' => '/public/images/unchecked.png',
                        ),
                        'username' => array(
                            'header' => _('Login') ,
                            'css_style' => 'text-align: left' ,
                            'sorting' => true
                        ),
                        'name' => array(
                            'header' => _('Jméno') ,
                            'css_style' => 'text-align: left' ,
                            'sorting' => true

                        ),
                        'name_full' => array(
                            'header' => _('Celé jméno') ,
                            'css_style' => 'text-align: left' ,
                            'sorting' => true

                        ),
                        'email' => array(
                            'header' => _('E-mail') ,
                            'css_style' => 'text-align: left' ,
                            'sorting' => true
                        ),
                        'phone' => array(
                            'header' => _('Telefon') ,
                            'css_style' => 'text-align: left' ,
                            'sorting' => false
                        ),
                        'role_translation' => array(
                            'header' => _('Role') ,
                            'css_style' => 'text-align: left' ,
                            'sorting' => false
                        ),
                    'edit' => array(
                        'header' => '' ,
                        'renderer' => 'action' , 'type' => 'edit' ,
                        'url' => $this->view->url(array(
                            'module' => 'default' , 'controller' => 'admin' ,
                            'action' => 'user-edit', 'u' => '{id}'
                        ), null, true)
                    ),
                    'delete' => array(
                        'header' => '' ,
                        'renderer' => 'action' , 'type' => 'delete' ,
                        'url' => $this->view->url(array(
                            'module' => 'default' , 'controller' => 'admin' ,
                            'action' => 'user-delete', 'u' => '{id}'
                        ), null, true)
                    )
                )

            ));

            $grid->setSelect($users_select);
            
            $grid->setRequest($this->getRequest()->getParams());


            /**
             * To view
             */
            $this->view->datagrid = $grid->render();
        }


        public function userEditAction() {
            // set layout
            $this->_helper->layout->setLayout('bootstrap-basic');

            /**
             * Get user id
             */
            $uid = $this->_getParam('u', '0');


            /**
             * Get user data
             */
            $tUser = new Table_Users();
            $data = $tUser->getUser((int)$uid);

            if(!$data) {
                $this->_redirect('admin/users-list');
            }


            /**
             * Get user role
             */
            $authNamespace = new Zend_Session_Namespace('Zend_Auth');


            /**
             * Load form
             */
            $form = new AdminUsers_Form();
            $form->removeElement('password');
            $form->removeElement('re_password');
            if(!in_array('admin', $authNamespace->user_role) 
              && !in_array('manager', $authNamespace->user_role)) {
                $form->removeElement('role');
            }


            /**
             * Send form
             */
            if ($this->getRequest()->isPost()
                && $form->isValid($this->getRequest()->getPost())) {
                    // set param
                    $uid = $form->getValue('id');
                    $active = $form->getValue('is_active') == 'y' ? 1 : 0;
                    $check_ip = $form->getValue('check_ip') == 'y' ? 1 : 0;
                    $username = $form->getValue('username');
                    $name = $form->getValue('name');
                    $name_full = $form->getValue('name_full');
                    $email = $form->getValue('email');
                    $phone = $form->getValue('phone');
                    $expiration = $form->getValue('expiration');
                    $description = $form->getValue('description');
                    // common user can not change the role yourself
                    if(!in_array('admin', $authNamespace->user_role) 
                      && !in_array('manager', $authNamespace->user_role)) {
                        $role = null;
                    }
                    else {
                        $role = $form->getValue('role');
                    }
                    
                    // save it
                    try {
                        $tUser = new Table_Users();
                        $tUser->editUser($uid, $check_ip, $active, $username, $name, $name_full, $email, $phone, $role, $description, $expiration);

                        // message
                        $this->_flashMessenger->addMessage("Uživatel <strong>" . $username . "</strong> byl upraven");

                        // redirect out
                        $this->_redirect('admin/users-list');
                    }
                    catch (Zend_Exception $e) {
                        echo $e->getMessage();
                    }
            }
            else {
                $form->populate($data);
                $form->getElement('is_active')->setChecked($data['is_active']);
                if(isset($data['check_ip'])) {
                    $form->getElement('check_ip')->setChecked($data['check_ip']);
                }
            }


            /**
             * To view
             */
            $this->view->form = $form;
            $this->view->title = 'Upravit uživatele: &nbsp;<strong>' . $data['username'] . ' &nbsp;(' . $data['name_full'] . ')</strong>';
            $this->view->uid = $uid;
        }


        public function userAddAction()
        {
            // set layout
            $this->_helper->layout->setLayout('bootstrap-basic');

            /**
             * Load form
             */
            $form = new AdminUsers_Form();

            /**
             * Send form
             */
            if ($this->getRequest()->isPost()
                    && $form->isValid($this->getRequest()->getPost())) {
                        // set param
                        $username = $form->getValue('username');
                        $name = $form->getValue('name');
                        $name_full = $form->getValue('name_full');
                        $email = $form->getValue('email');
                        $phone = $form->getValue('phone');
                        $role = $form->getValue('role');
                        $description = $form->getValue('description');
                        $password = $form->getValue('password');
                        $re_password = $form->getValue('re_password');
                        $is_active = $form->getValue('is_active');
                        $expiration = $form->getValue('expiration');
                        if($password == $re_password) {
                            try {
                                $tUser = new Table_Users();
                                $tUser->addUser($username, $name, $name_full, $email, $phone, $role, $description, $password, $is_active, $expiration);

                                // message
                                $this->_flashMessenger->addMessage("Uživatel <strong>" . $username . "</strong> byl přidán");

                                // redirect out
                                $this->_redirect('admin/users-list');
                            }
                            catch (Zend_Exception $e) {
                                echo $e->getMessage();
                            }
                        }
            }


            /**
             * To view
             */
            $this->view->form = $form;
            $this->view->title = 'Přidat uživatele';
        }


  public function userPwdAction() {
    // set layout
    $this->_helper->layout->setLayout('bootstrap-basic');

    /**
     * Get user id
     */
    $authNamespace = new Zend_Session_Namespace('Zend_Auth');
    $uid = $this->_getParam('u', $authNamespace->user_id);
    $isAdmin = Zend_Registry::get('acl')->isAllowed('admin/users-list');

    /**
     * Get user data
     */
    $tUser = new Table_Users();
    $data = $tUser->getUser((int)$uid);
    if (!$data) {
      $this->redirect(isAdmin ? 'admin/users-list' : '/');
    }

    /**
     * Load form
     */
    $form = new AdminUsers_PasswordForm();


    /**
     * Send form
     */
    if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {

      $password = $form->getValue('password');
      $new_password = $form->getValue('new_password');
      $re_password = $form->getValue('re_password');

      $passwordHash = Zend_Registry::get('db')->select()
        ->from('users', array('password'))
        ->where('id = ?', $authNamespace->user_id)
        ->query()
        ->fetchColumn();

      if (sha1($_SESSION["Zend_Auth"]["salt"] . $password) != $passwordHash) {
        $this->_flashMessenger->addMessage("Zadáno nesprávné heslo.");
        $this->redirect($isAdmin ? "admin/user-pwd/u/". $uid : 'admin/user-pwd');
      }

      if ($new_password == $re_password) {

        if ($new_password == $password) {
          $this->_flashMessenger->addMessage("Nové heslo nesmí být stejné jako původní heslo.");
          $this->redirect($isAdmin ? "admin/user-pwd/u/". $uid : 'admin/user-pwd');
        }
        $psValidator = new Meduse_Validate_PasswordStrength();
        if (!$psValidator->isValid($new_password)) {

          $this->_flashMessenger->addMessage('Nové heslo musí být dostatečně silné:');
          foreach ($psValidator->getMessages() as $msg) {
            $this->_flashMessenger->addMessage($msg);
          }
          $this->redirect($isAdmin ? "admin/user-pwd/u/". $uid : 'admin/user-pwd');
        }
        try {
          $tUser = new Table_Users();
          $tUser->changePwd($uid, $new_password);

          // message
          $this->_flashMessenger->addMessage("Heslo bylo změněno");
          $authNamespace = new Zend_Session_Namespace('Zend_Auth');
          $authNamespace->passwordExpired = false;

          $this->redirect($isAdmin ? "admin/user-pwd/u/". $uid : '/');
        }
        catch (Zend_Exception $e) {
          echo $e->getMessage();
        }
      }
      else {
        // throw message
        $this->_flashMessenger->addMessage("Zadaná hesla se neshodují.");
        $this->redirect($isAdmin ? "admin/user-pwd/u/". $uid : 'admin/user-pwd');
      }
    }

    /**
     * To view
     */
    $this->view->form = $form;
    $this->view->data = $data;
    $this->view->title = 'Změna hesla – uživatel: ' . $data['username'];
  }

        public function userDeleteAction()
        {
            /**
             * Disable layout
             */
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            /**
             * Get user id
             */
            $uid = $this->_getParam('u', '0');


            /**
             * Get user data
             */
            $tUser = new Table_Users();
            $data = $tUser->getUser((int)$uid);

            if(!$data) {
                $this->_redirect('admin/users-list');
            }

            /**
             * Set as delete
             */
            try {
                $tUser = new Table_Users();
                $tUser->setDelete($data['id']);

                // message
                $this->_flashMessenger->addMessage("Uživatel byl smazán");

                // redirect out
                $this->_redirect('admin/users-list');
            } catch (Zend_Exception $e) {
                throw new Exception($e->getMessage());
            }
        }

        public function accountsListAction() {
            $this->_helper->layout->setLayout('bootstrap-basic');
            $tAccounts = new Table_Accounts();
            $this->view->accounts = $tAccounts->select()
              ->where('id_wh_type = ?', Table_PartsWarehouse::WH_TYPE)
              ->query()->fetchAll();
        }

        public function accountsEditAction() {
            $this->_helper->layout->setLayout('bootstrap-basic');
            $form = new AdminAccounts_Form(['id_wh_type' => Table_PartsWarehouse::WH_TYPE]);
            $tAccounts = new Table_Accounts();
            $id = $this->getRequest()->getParam('id', null);
            if ($id) {
                $data = $tAccounts->find($id)->current()->toArray();
                if ($data) {
                    $form->populate($data);
                    $form->setLegend('Editace bankovního účtu');
                    $this->view->id = $id;
                } else {
                    $this->_flashMessenger->addMessage('Účet nenalezen.');
                    $this->redirect('/admin/accounts-list');
                }
            } else {
                $form->setLegend('Vložení bankovního účtu');
            }
            $this->view->form = $form;
        }

        public function accountsSaveAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            if ($this->getRequest()->isPost()) {
                $data = $this->getRequest()->getPost();
                $form = new AdminAccounts_Form(['id_wh_type' => Table_PartsWarehouse::WH_TYPE]);
                if ($form->isValid($data)) {
                    $tAccounts = new Table_Accounts();
                    //nastavování je-li účet výchozí
                    if ($data['is_default']=='y') {
                        $tAccounts->update(array('is_default' => 'n'), 'id_wh_type = ' . $data['id_wh_type']);
                    }
                    
                    if ($data['id']) {
                        //kontrola odebrání označení výchozího účtu (pro upozornění)
                        $defEdit = $tAccounts->find($data['id'])->current()->toArray();
                        
                        $tAccounts->update(array(
                            'id_wh_type' => $data['id_wh_type'],
                            'designation' => $data['designation'],
                            'name' => $data['name'],
                            'address' => $data['address'],
                            'iban' => $data['iban'],
                            'bic' => $data['bic'],
                            'account_prefix' => $data['account_prefix'],
                            'account_number' => $data['account_number'],
                            'bank_number' => $data['bank_number'],
                            'currency' => $data['currency'],
                            'is_default' => $data['is_default'],
                            'payment_method' => $data['payment_method'] ? $data['payment_method'] : null,
                        ), 'id = ' . $data['id']);
                        
                        $this->_flashMessenger->addMessage('Údaje byly aktualizovány.');
                        
                        //označení odebrání výchozího účtu
                        if ($defEdit['is_default']=="y" && $data['is_default']=="n") {
                            $this->_flashMessenger->addMessage('Bylo odebráno označení výchozího účtu.');
                        }
                    } else {
                        $tAccounts->insert(array(
                            'id_wh_type' => $data['id_wh_type'],
                            'designation' => $data['designation'],
                            'name' => $data['name'],
                            'address' => $data['address'],
                            'iban' => $data['iban'],
                            'bic' => $data['bic'],
                            'account_prefix' => $data['account_prefix'],
                            'account_number' => $data['account_number'],
                            'bank_number' => $data['bank_number'],
                            'currency' => $data['currency'],
                            'is_default' => $data['is_default'],
                            'payment_method' => $data['payment_method'] ? $data['payment_method'] : null,
                        ));
                        $this->_flashMessenger->addMessage('Údaje byly vloženy.');
                    }
                } else {
                    $this->_flashMessenger->addMessage('Invalidní data');
                }
            }
            $this->redirect('/admin/accounts-list');
        }

        public function accountsDeleteAction() {
            $request = $this->getRequest();
            if ($request->isPost() && !is_null($id = $request->getParam('account_id', null))) {
                $tAccounts = new Table_Accounts();
                
                //ověření, zdali se nejedná o výchozí účet
                $defEdit = $tAccounts->find($id)->current()->toArray();
                
                $tAccounts->delete('id = ' . $id);
                $this->_flashMessenger->addMessage('Účet byl odstraněn.');
                
                if ($defEdit['is_default']=="y") {
                    $this->_flashMessenger->addMessage('Bylo odebráno označení výchozího účtu.');
                }
            }
            $this->redirect('/admin/accounts-list');
        }

        public function jsonGetCnbEurRateAction() {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $rates = Utils::getCnbRates();
            echo $rates ? $rates['EUR'] : 0;
        }

        public function rateSettingsAction() {
            $this->_helper->layout->setLayout('bootstrap-basic');
            $this->view->form = new AdminRate_Form();
        }

        public function rateUpdateAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            if ($this->getRequest()->isPost()) {
                $form = new AdminRate_Form();
                if ($form->isValid($this->getRequest()->getPost())) {
                    $tRate = new Table_ApplicationRate();
                    $tRate->insert(array('rate' => $form->getValue('rate')));
                }
            }
            $this->_redirect('/admin/rate-settings');
        }
        
        
  public function operationsRateSettingsAction() {
    $form = new AdminOperationsRate_Form();
    if ($this->getRequest()->isPost()) {
      $data['rate'] = $this->getRequest()->getParam('rate');
      if ($form->isValid($data)) {
        $tRate = new Table_ApplicationOperationsRate();
        $tRate->insert($data);
        
        $tPO = new Table_PartsOperations();
        $tPO->recalculate();
        
        $this->_flashMessenger->addMessage('Sazba byla uložena.');
        $this->_helper->redirector('operations-rate-settings', 'admin');
      } else {
        $form->getElement('rate')->setValue($data['rate']);
      }
    }
    $this->_helper->layout->setLayout('bootstrap-basic');
    $this->view->form = $form;
  }
  
  public function flushAclCacheAction() {
    $acl = Zend_Registry::get('acl');
    $acl->flushCache();
    $this->_flashMessenger->addMessage('ACL cache byla vyprázdněna.');
    $this->_redirect($_SERVER['HTTP_REFERER']);
  }
  
  public function coverCostSettingsAction() {
    $form = new Parts_CoverCostForm();
    if ($this->getRequest()->isPost()) {
      $data['cover_cost'] = Meduse_Currency::parse($this->getRequest()->getParam('cover_cost'));
      if ($form->isValid($data)) {
        $table = new Table_ApplicationCoverCost();
        $table->insert($data);
        $this->_flashMessenger->addMessage('Výchozí výše krytí byla uložena.');

        if ($this->_request->getParam('set_default') === 'y') {
          $num = Parts::setDefaultCoverCost();
          $this->_flashMessenger->addMessage('Bylo upraveno ' . $num . ' produktů.');
        }

        $this->_helper->redirector('cover-cost-settings', 'admin');
      }
      else {
        $form->getElement('cover_cost')->setValue($data['cover_cost']);
      }
    }
    $this->view->form = $form;
  }
}
