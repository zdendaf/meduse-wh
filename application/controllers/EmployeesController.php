<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class EmployeesController extends Meduse_Controller_Action{

    /** @var int počet měsíců zobrazených na přehledu pracovníka */
    const REWARD_DASHBOARD_MONTHS_LIMIT = 13;

    public function init() {
      $this->_helper->layout->setLayout('bootstrap-employees');
      parent::init();
    }

    public function indexAction() {
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      try {
        $employee = new Employees_Employee();
      }
      catch (Employees_Exceptions_NonExists $e) {
        // pokud neexistuje pracovnik prirazeny k prihlasenemu uzivateli
        // jedna se o manazera a proto redirektujeme na prehled pracovniku.
        if (Zend_Registry::get('acl')->isAllowed('employees/admin')) {
          $this->redirect('employees/admin');
        }
        else {
          $this->redirect($this->view->url(array(
            'controller' => 'login',
            'action' => 'clear',
            'reason' => 1)));
        }

      }
      $date = new Meduse_Date();
      $this->view->employee = $employee;
      $this->view->rewards = $employee->getRewards(NULL, NULL, 12);

      // datum dalsiho nezalozeneho mesice
      if ($this->view->rewards) {
        $this->view->nextRewardDate = new Meduse_Date($this->view->rewards[0]->getDate());
        $this->view->nextRewardDate->addMonth(1);
      }
      else {
        $this->view->nextRewardDate = new Meduse_Date();
      }
      if (Zend_Registry::get('config')->alveno->enabled) {
        try {
          $dsi = new Alveno_Dsi(null);
          $this->view->lastRecordDate = $dsi->getLastRecord();
        }
        catch (Exception $e) {
          $this->view->message = $e->getMessage();
        }
      }
      $this->view->vacations = $employee->getVacations($date->get('Y'));
      $this->view->absence = $employee->getAbsence($date);
      $this->view->status = $employee->getStatus();
      $this->view->states = $employee->getStatesToChoose();
      $this->view->work = $employee->getWork($date);
      $this->view->title = "Dashboard";
      $this->view->editEmployee = Zend_Registry::get('acl')->isAllowed('employees/edit');
      $this->view->closeMonth = Zend_Registry::get('acl')->isAllowed('employees/close-month');
      $this->view->reopenMonth = Zend_Registry::get('acl')->isAllowed('employees/reopen-month');
      $this->view->absenceAdd = Zend_Registry::get('acl')->isAllowed('employees/absence-add');
      $this->view->showWage = Zend_Registry::get('acl')->isAllowed('employees/special/show-wage') || $employee->getUserId() == $authNamespace->user_id;
      $this->renderScript('employees/dashboard.phtml');
    }

    public function adminAction() {
      $this->view->title = "Správa pracovníků";

      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $tEmployees = new Table_Employees();

      if (in_array('admin', $authNamespace->user_role) || in_array('manager', $authNamespace->user_role)) {
        $options = $this->_getGridOptions();
        $params = $this->_request->getParams();
        $pipesEmployees = $tEmployees->getAdminGrid($options, $params, Parts_Part::WH_TYPE_PIPES);
        $tobaccoEmployees = $tEmployees->getAdminGrid($options, $params, Parts_Part::WH_TYPE_TOBACCO);
        $this->view->gridPipes = $pipesEmployees->render();
        $this->view->gridTobacco = $tobaccoEmployees->render();
        $this->view->deactivated = Employees_Employee::getDeactivated(NULL, TRUE);
      }
      if (in_array('dept_leader', $authNamespace->user_role)) {
        $tDepts = new Table_Departments();
        $depts = $tDepts->getManagedDepartments($authNamespace->user_id);
        $deptsIds = array();
        if ($depts) {
          foreach ($depts as $dept) {
            $deptsIds[] = $dept->id;
          }
          $deptEmployees = $tEmployees->getDeptsEmployees($deptsIds);
          $empArr = array();
          foreach ($deptEmployees as $emp) {
            $empArr[] = new Employees_Employee($emp->id);
          }
          $this->view->deptEmployees = $empArr;
        }
      }
    }

    public function addAction() {


      $type = $this->_request->getParam('type', Table_Employees::TYPE_EMPLOYEE);

      $form = new Employees_EditForm();

      if ($type === Table_Employees::TYPE_SUPPLIER) {
        $this->view->title = "Nový živnostník";
        $form->setType(Table_Employees::TYPE_SUPPLIER);
      }
      else {
        $this->view->title = "Nový zaměstnanec";
        $form->setType(Table_Employees::TYPE_EMPLOYEE);
      }

      $form->setLegend($this->view->title);

      if ($this->_request->isPost()) {
        $data = $this->_request->getParams();
        if ($form->isValid($data)) {
          $this->_prepareData($data);
          $employee = new Employees_Employee($data);
          $this->_flashMessenger->addMessage('Uživatel ' . $employee->getFullName() . ' byl přidán');
          $this->redirect($this->view->url(array('action' => 'admin', 'type' => NULL)));
        }
        else {
          $form->populate($data);
        }
      }
      $this->view->form = $form;
      $this->renderScript('employees/edit.phtml');
    }

    public function editAction() {
      $id = $this->_request->getParam('id');
      if (is_null($id)) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID uživatele');
        $this->redirect($this->view->url(array('action' => 'admin')));
        exit;
      }
      $employee = new Employees_Employee($id);
      $form = new Employees_EditForm();
      $form->setType($employee->getType());

      if ($employee->getType() === Table_Employees::TYPE_EMPLOYEE) {
        $this->view->title = "Editace zaměstnance";
      }
      else {
        $this->view->title = "Editace živnostníka";
      }
      $form->setLegend($this->view->title);
      if ($this->_request->isPost()) {
        $data = $this->_request->getParams();
        if ($form->isValid($data)) {
          $this->_prepareData($data);
          $employee->update($data);
          $this->_flashMessenger->addMessage('Zaměstnanec ' . $employee->getFullName() . ' byl aktualizován');
          $destination = $this->_request->getParam('destination', $this->view->url(array('action' => 'admin', 'id' => NULL)));
          $this->redirect($destination);
          exit;
        }
      }
      else {
        $data = $employee->getFormData();
      }
      $data['id_employees'] = $this->_request->getParam('id');
      $form->populate($data);
      $this->view->form = $form;
    }

    private function _prepareData(&$data) {
      unset($data['controller']);
      unset($data['action']);
      unset($data['module']);
      unset($data['Uložit']);
      unset($data['destination']);
      $data['start'] = Meduse_Date::formToDb($data['start']);
      if (isset($data['not_end']) && $data['not_end'] === 'y') {
        $data['end'] = NULL;
      }
      else {
        $data['end'] = Meduse_Date::formToDb($data['end']);
      }
      unset($data['not_end']);
      if ($data['type'] == Table_Employees::TYPE_EMPLOYEE) {
        $data['overtime_rate'] = Meduse_Currency::parse($data['overtime_rate']);
        $data['overtime_extra_rate'] = Meduse_Currency::parse($data['overtime_extra_rate']);
      }
      $data['birthday'] = Meduse_Date::formToDb($data['birthday']);
      $data['bonus'] = trim($data['bonus']) === "" ? NULL : intval($data['bonus']);
      $data['alveno'] = intval($data['alveno'])? $data['alveno'] : NULL;
    }

    public function ajaxGetUserInfoAction() {
      $results = array();
      $id = $this->_request->getParam('id');
      if ($id) {
        $table = new Table_Users();
        $results = $table->getUser($id, FALSE);
      }
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $this->_helper->json($results);
    }

    public function viewMonthAction() {
      $employee = new Employees_Employee($this->_request->getParam('id'));
      $date = new Meduse_Date($this->_request->getParam('date'), 'Y-m');
      $reward = new Employees_Reward($employee, $date);
      $this->view->rewardsDept = $reward->getDeptRewards();
      if (!$employee->isPartTime()) {
        $this->view->formOvertime = new Employees_OvertimeForm(array('reward' => $reward));
        $this->view->formOvertime->setAction($this->view->url(array('action' => 'overtime-cash')));
      }

      if (!$reward->isClosed() && $reward->getDate()->get('m') === '12') {
        $this->view->formVacations = new Employees_VacationsForm(['reward' => $reward]);
        $this->view->formVacations->setAction($this->view->url(array('action' => 'vacation-claim')));
      }

      $this->view->formReward = new Employees_RewardForm();
      $this->view->formReward->setReward($reward);
      $this->view->formReward->setAction($this->view->url(array('action' => 'reward-add')));
      $this->view->reward = $reward;
      $this->view->date = $date;
      $this->view->employee = $employee;
      $this->view->title = 'Přehled měsíce';
      $this->view->worksheet = new Employees_WorkSheet($employee->getWork($date, true));
    }

    public function showMonthAction() {
      $this->renderScript('employees/view-month.phtml');
      $this->viewMonthAction();
    }

    public function closeMonthAction() {
      $employee = new Employees_Employee($this->_request->getParam('id'));
      $date = new Meduse_Date($this->_request->getParam('date'), 'Y-m');
      $reward = new Employees_Reward($employee, $date);
      try {
        $reward->closeMonth();
        $this->_flashMessenger->addMessage('Měsíc byl uzavřen');
      }
      catch (Employees_Exceptions_RewardAlreadyClosed $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
      }
      $this->redirect($this->view->url(array('action' => 'dashboard', 'date' => NULL)));
    }


    public function reopenMonthAction() {

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $employee = new Employees_Employee($this->_request->getParam('id'));
      $date = new Meduse_Date($this->_request->getParam('date'), 'Y-m');
      $reward = new Employees_Reward($employee, $date);
      try {
        $reward->setStatus(Employees_Reward::REWARD_STATUS_OPEN);
        $this->_flashMessenger->addMessage('Měsíc ' . $date->toString('m/Y')
          . ' byl pro pracovníka ' . $employee->getFullName() . ' otevřen.');
      }
      catch(Employees_Exceptions_RewardUnknownStatus $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
      }
      $this->_redirect(filter_input(INPUT_SERVER, 'HTTP_REFERER'));
    }

    public function openMonthAction() {
      $this->reopenMonthAction();
    }

    /**
     * @throws Zend_Exception
     * @throws Employees_Exceptions_NonExists
     * @throws Employees_Exceptions_IncompleteContructor
     * @throws Zend_Date_Exception
     * @throws Zend_Db_Statement_Exception
     */
    public function dashboardAction() {
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      try {
        $employee = new Employees_Employee($this->_request->getParam('id'));
      }
      catch (Employees_Exceptions_NonExists $e) {
        // pokud neexistuje pracovnik prirazeny k prihlasenemu uzivateli
        // jedna se o manazera a proto redirektujeme na prehled pracovniku.
        $this->redirect('employees/admin');
      }
      $this->view->employee = $employee;
      $this->view->rewards = $employee->getRewards(NULL, NULL, self::REWARD_DASHBOARD_MONTHS_LIMIT);

      if ($lastClosedReward = $employee->getLastClosedReward()) {
        $date = $lastClosedReward->getDate();
        $year = (int) $date->get('Y');
        $year = ($date->get('m') === '12') ? $year + 1 : $year;
      }
      else {
        $year = (int) date('Y');
      }
      $this->view->vacationYear = $year;
      $this->view->vacation = $employee->getVacation($year);
      $this->view->vacationSum = $employee->getVacationSum($year);
      $this->view->vacationRest = $employee->getVacationRest($year);

      // datum dalsiho nezalozeneho mesice
      if ($this->view->rewards) {
        $this->view->nextRewardDate = new Meduse_Date($this->view->rewards[0]->getDate());
        $this->view->nextRewardDate->addMonth(1);
      }
      else {
        $this->view->nextRewardDate = new Meduse_Date();
      }
      if (Zend_Registry::get('config')->alveno->enabled) {
        try {
          $dsi = new Alveno_Dsi('meduse');
          $this->view->lastRecordDate = $dsi->getLastRecord();
        }
        catch (Exception $e) {
          $this->view->message = $e->getMessage();
        }
      }
      $this->view->title = "Dashboard";
      $this->view->editEmployee = Zend_Registry::get('acl')->isAllowed('employees/edit');
      $this->view->closeMonth = Zend_Registry::get('acl')->isAllowed('employees/close-month');
      $this->view->reopenMonth = Zend_Registry::get('acl')->isAllowed('employees/reopen-month');
      $this->view->absenceAdd = Zend_Registry::get('acl')->isAllowed('employees/absence-add');
      $this->view->showWage = Zend_Registry::get('acl')->isAllowed('employees/special/show-wage') || $employee->getUserId() == $authNamespace->user_id;
    }

    public function setStatusAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $employee = new Employees_Employee();
      $employee->setStatus($this->_request->getParam('status'));
      $this->_redirect('employees');
    }

    /**
     * Editace prace prihlaseneho uzivatele
     */
    public function worksheetAction() {
      $employeeId = $this->_request->getParam('id');
      $employee = new Employees_Employee($employeeId);
      $date = new Meduse_Date($this->_request->getParam('date'), 'Y-m');
      $work = new Employees_Work($employee, $date);
      $sheet = $work->getSheet();
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      $this->view->userId = $authNamespace->user_id;
      $this->view->employee = $employee;
      $this->view->sheet = $sheet;
      $this->view->total = $sheet->getTotal();
      $this->view->hours = $sheet->getWorkingHours();
      if ($employee->getType() == Table_Employees::TYPE_EMPLOYEE && !$employee->isPartTime()) {
        $this->view->overtime = $sheet->getOvertime(FALSE);
        $this->view->overtime_debt = $sheet->getOvertime(TRUE);
      }
      else {
        $this->view->overtime = NULL;
        $this->view->overtime_debt = NULL;
      }

      $this->view->title = 'Docházkový list – ' . $employee->getFirstName() . ' ' . $employee->getLastName() . ' – ' . $date->toString('m/Y');
    }

    public function ajaxEditWorkAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      if ($this->_request->isPost()) {
        $tWorks = new Table_EmployeesWorks();
        $id = $this->_request->getParam('id');
        $row = $tWorks->save(array(
          'id' => $id == 'null' ? NULL : $id,
          'id_users' => $this->_request->getParam('id_users'),
          'id_employees' => $this->_request->getParam('id_employees'),
          'date' => $this->_request->getParam('date'),
          'column' => $this->_request->getParam('column'),
          'value' => $this->_request->getParam('value'),
        ));
        if (!is_null($row)) {
          $this->_helper->json(array('status' => TRUE, 'row' => $row));
        }
        else {
          $this->_helper->json(array('status' => TRUE, 'row' => NULL));
        }
      }
    }

    public function settingsAction() {
      $this->view->title = 'Docházka – nastaverní';
      $form = new Employees_SettingsForm();
      if ($this->_request->isPost()) {
        $data = $this->_request->getParams();
        if ($form->isValid($data)) {
          Table_Settings::set('employees_diets_rate5', $data['employees_diets_rate5'], 'float');
          Table_Settings::set('employees_diets_rate12', $data['employees_diets_rate12'], 'float');
          Table_Settings::set('employees_diets_rate18', $data['employees_diets_rate18'], 'float');
          Table_Settings::set('employees_diets_ratef', $data['employees_diets_ratef'], 'float');
          Table_Settings::set('employees_car_rate', $data['employees_car_rate'], 'float');
          $this->_flashMessenger->addMessage('Sazby byly uloženy');
          $this->_redirect($this->view->url(array()));
          exit;
        }
      } else {
        $data = array(
          'employees_diets_rate5'  => Table_Settings::get('employees_diets_rate5'),
          'employees_diets_rate12' => Table_Settings::get('employees_diets_rate12'),
          'employees_diets_rate18' => Table_Settings::get('employees_diets_rate18'),
          'employees_diets_ratef'  => Table_Settings::get('employees_diets_ratef'),
          'employees_car_rate'  => Table_Settings::get('employees_car_rate'),
        );
      }

      $tHolidays = new Table_EmployeesHolidays();
      $this->view->holidays = $tHolidays->getHolidays(date('Y'));
      $this->view->holidaysForm = new Employees_HolidaysForm();
      $this->view->holidaysForm->setAction($this->view->url(array('action' => 'holidays-add')));
      $form->populate($data);
      $this->view->form = $form;
    }

    public function holidaysAddAction() {
      if ($this->_request->isPost()) {
        $form = new Employees_HolidaysForm();
        $data = $this->_request->getParams();
        if ($form->isValid($data)) {
          $values = $form->getValues();
          $tHolidays = new Table_EmployeesHolidays();
          $tHolidays->insert(array(
            'date' => Meduse_Date::formToDb($values['date']),
            'change' => $values['change'] ? Meduse_Date::formToDb($values['change']) : NULL,
          ));
          $this->_flashMessenger->addMessage('Volno ' . $values['date'] . ' bylo přináno.');
        }
      }
      $this->_redirect($this->view->url(array('action' => 'settings', 'date' => NULL)));
    }

    public function holidaysRemoveAction() {
      if ($date = $this->_request->getParam('date')) {
        $tHolidays = new Table_EmployeesHolidays();
        $tHolidays->delete('date = "' . $date . '"');
        $this->_flashMessenger->addMessage('Volno ' . Meduse_Date::dbToForm($date) . ' bylo odstraněno.');
      }
      $this->_redirect($this->view->url(array('action' => 'settings', 'date' => NULL)));
    }

    public function vacationAddAction() {
      $employeeId = $this->_request->getParam('id');
      $employee = new Employees_Employee($employeeId);
      $this->view->title = 'Požadavek na dovolenou';
      $form = new Employees_AbsenceForm();
      $form->removeElement('type');
      $form->removeElement('description');
      $form->setLegend($this->view->title . ' – ' . $employee->getFullName());
      if ($this->_request->isPost()) {
        $params = $this->_request->getParams();
        if ($form->isValid($params)) {
          $startDate = new Meduse_Date($params['start'], 'd.m.Y');
          $endDate = new Meduse_Date($params['end'], 'd.m.Y');
          $table = new Table_EmployeesAbsence();
          $table->insert(array(
            'id_employees' => $params['id_employees'],
            'start' => $startDate->toString('Y-m-d'),
            'end' => $endDate->toString('Y-m-d'),
            'total' => $startDate->getCountWorkingDaysTo($endDate, TRUE),
            'type' => Table_EmployeesAbsence::TYPE_VACATION,
          ));
          $this->_flashMessenger->addMessage('Dovolená byla uložena.');

          $authNamespace = new Zend_Session_Namespace('Zend_Auth');
          // pokud si planuje uzivatel vlastni dovolenou
          // redirect je na jeho dashboard
          if ($authNamespace->user_id == $params['id_employees']) {
            $this->_redirect($this->view->url(array(
              'action' => 'index',
              'id' => NULL
            )));
          }
          // jinak je redirect na admin. dashboard
          else {
            $this->_redirect($this->view->url(array(
              'action' => 'dashboard',
              'id' => $params['id_employees']
            )));
          }
        }
        $form->populate($params);
      } else {
       $form->getElement('id_employees')->setValue($employee->getId());
      }
      $this->view->form = $form;
      $this->renderScript('employees/absence-add.phtml');
    }

    public function absenceAddAction() {
      $employeeId = $this->_request->getParam('id');
      $employee = new Employees_Employee($employeeId);
      $this->view->title = 'Oznámení absence';
      $form = new Employees_AbsenceForm();
      $form->setLegend($this->view->title . ' – ' . $employee->getFullName());
      if ($this->_request->isPost()) {
        $params = $this->_request->getParams();
        if ($form->isValid($params)) {
          $startDate = new Meduse_Date($params['start'], 'd.m.Y');
          $endDate = new Meduse_Date($params['end'], 'd.m.Y');
          $table = new Table_EmployeesAbsence();
          $table->insert(array(
            'id_employees' => $params['id_employees'],
            'start' => $startDate->toString('Y-m-d'),
            'end' => $endDate->toString('Y-m-d'),
            'total' => $startDate->getCountWorkingDaysTo($endDate, TRUE),
            'type' => $params['type'],
            'description' => $params['description'],
            'approved' => 'y',
          ));
          $this->_flashMessenger->addMessage('Absence byla vložena.');
          $this->_redirect($this->view->url(array('action' => 'index', 'id' => NULL)));
        }
        $form->populate($params);
      } else {
       $form->getElement('id_employees')->setValue($employee->getId());
      }
      $this->view->form = $form;
      $this->renderScript('employees/absence-add.phtml');
    }

    public function vacationAction() {
      $this->view->title = 'Přehled dovolených';
      $tVacation = new Table_EmployeesAbsence();
      $gVacation = $tVacation->getGrid(array(
          'css_class' => 'table table-bordered table-hover',
          'columns' => array(
            'last_name' => array(
              'header' => 'příjmení',
              'renderer' => 'bootstrap_url',
              'url' => $this->view->url(array(
                'action' => 'dashboard',
                'id' => '{id_employees}',
                )
              ),
            ),
            'first_name' => array(
              'header' => 'jméno'
            ),
            'department' => array(
              'header' => 'oddělení'
            ),
            'start' => array(
              'header' => 'začátek',
              'renderer' => 'function',
              'class_name' => 'Meduse_Date',
              'function_name' => 'dbToForm',
              'function_attributes' => array('[[start]]'),
            ),
            'end' => array(
              'header' => 'konec',
              'renderer' => 'function',
              'class_name' => 'Meduse_Date',
              'function_name' => 'dbToForm',
              'function_attributes' => array('[[end]]'),
            ),
            'total' => array(
              'header' => 'celkem'
            ),
            'approved' => array(
              'header' => '',
              'renderer' => 'condition',
              'condition_value' => 'n',
              'condition_true' => array(
                'renderer' => 'bootstrap_buttonGroup',
                'group' => array(
                  array(
                    'renderer' => 'bootstrap_button',
                    'url' => $this->view->url(array(
                      'action' => 'vacation-approve',
                      'id' => '{id}',
                      )),
                    'icon' => 'icon icon-check',
                    'text' => 'schválit',
                  ),
                  array(
                    'renderer' => 'bootstrap_button',
                    'url' => $this->view->url(array(
                      'action' => 'vacation-reject',
                      'id' => '{id}',
                      )),
                    'icon' => 'icon icon-remove',
                    'text' => 'zamítnout',
                  ),
                ),
              ),
              'condition_false' => array(
                'renderer' => 'bootstrap_flag',
                'states' => array('n', 'y'),
              ),
            ),
          )
        ), Table_EmployeesAbsence::COND_RUNNING_NEXT);
      $this->view->grid = $gVacation->render();
    }

    public function vacationApproveAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $vid = $this->_request->getParam('id');
      if ($vid) {
        $tVacation = new Table_EmployeesAbsence();
        $tVacation->update(array('approved' => 'y'), 'id = ' . $vid);
        $this->_flashMessenger->addMessage('Dovolená byla schválena.');
      }
      $this->_redirect($this->view->url(array('action' => 'vacation', 'id' => NULL)));
    }

    public function vacationRejectAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $vid = $this->_request->getParam('id');
      if ($vid) {
        $tVacation = new Table_EmployeesAbsence();
        $tVacation->delete('id = ' . $vid);
        $this->_flashMessenger->addMessage('Dovolená byla zamítnuta.');
      }
      $this->_redirect($this->view->url(array('action' => 'vacation', 'id' => NULL)));
    }

    public function fetchDsiRecordsAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      try {
        $terminals = array(
          new Alveno_Dsi('meduse'),
          new Alveno_Psi('medite'),
          new Alveno_Psi('meduse_vyroba'),
        );
        foreach ($terminals as $term) {

            $term->getNewRecords();
            $tWorks = new Table_EmployeesRecords();
            $records = $term->fetchAllRecords();
            if (!$term->distStates()) {
              $tWorks->preprocessRecords($records);
            }
            if ($records) {
              foreach ($records as $record) {
                $tWorks->insert($record);
              }
            }

            $affected = $term->getAffected();
            if ($affected) {
              foreach ($affected as $employeeId => $dates) {
                foreach ($dates as $date) {
                  new Employees_Reward(new Employees_Employee($employeeId), new Meduse_Date($date . '-01'));
                }
              }
            }

          $messages = $term->fetchAllMessages();
          if ($messages) {
            foreach ($messages as $message) {
              $this->_flashMessenger->addMessage($message);
            }
          }
        }
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
      }

      $acl = Zend_Registry::get('acl');
      if ($acl->isAllowed('employees/admin')) {
        $this->_redirect($this->view->url(array('action' => 'admin', 'id' => NULL)));
      }
      else {
        $this->_redirect($this->view->url(array('action' => 'index', 'id' => NULL)));
      }
    }


    public function recalculateAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $reward = new Employees_Reward(
        new Employees_Employee($this->_request->getParam('id')),
        new Meduse_Date($this->_request->getParam('date'), 'Y-m')
      );
      try {
        $reward->recalculate();
        $message = 'Přepočet proběhnul ';
        switch ($reward->getStatus()) {
          case Employees_Reward::REWARD_STATUS_FAIL;
            $message .= 's chybami.';
            break;
          default:
            $message .= 'bez chyb.';
            break;
        }
        $this->_flashMessenger->addMessage($message);
      }
      catch (Employees_Exceptions_RewardAlreadyClosed $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
      }
      if (Zend_Registry::get('acl')->isAllowed('employees/dashboard')) {
        $this->redirect($this->view->url(array('action' => 'dashboard')));
      }
      else {
        $this->redirect($this->view->url(array('action' => 'index', 'id' => NULL, 'date' => NULL)));
      }
    }

    public function bustripAddAction() {

      $this->view->form = new Employees_BustripsForm();

    }

    public function accountReportAction() {
      $dateStr = $this->_request->getParam('date', NULL);
      $date = new Meduse_Date($dateStr, 'Y-m');
      if (is_null($dateStr)) {
        $date->subMonth(1);
      }

      $this->view->data = array();
      $this->view->date = $date;

      $prev = new Meduse_Date($date);
      $prev->subMonth(1);
      $this->view->datePrev = $prev;

      $next = new Meduse_Date($date);
      $next->addMonth(1);
      $this->view->dateNext = $next;

      $this->view->title = 'Přehled nemocenské a dovolených';

      $tEmp = new Table_Employees();
      $employees = $tEmp->getEmployees(TRUE);

      if ($employees) {
        foreach ($employees as $employee) {
          $employeeObj = new Employees_Employee($employee->id);
          $reward = new Employees_Reward($employeeObj, $date, FALSE);
          $this->view->data[] = $reward->getAccountData();
        }
      }
    }

    public function rewardAddAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      if ($this->_request->isPost()) {
        $form = new Employees_RewardForm();
        if ($form->isValid($this->_request->getParams())) {
          $values = $form->getValues();
          $table = new Table_EmployeesRewardsDept();
          $table->insert($values);
          $this->_flashMessenger->addMessage('Položka uložena.');
        }
        else {
          $this->_flashMessenger->addMessage('Nebyla zadána správná data.');
        }
      }
      $this->_redirect($this->view->url(array('action' => 'view-month')));
    }

    public function rewardRemoveAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $table = new Table_EmployeesRewardsDept();
      $table->delete('id = ' . $this->_request->getParam('reward'));
      $this->_redirect($this->view->url(array('action' => 'view-month', 'reward' => NULL)));
    }

    public function overtimeCashAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      if ($this->_request->isPost()) {
        $employee = new Employees_Employee($this->_request->getParam('id'));
        $date = new Meduse_Date($this->_request->getParam('date'), 'Y-m');
        $reward = new Employees_Reward($employee, $date);
        $form = new Employees_OvertimeForm(array('reward' => $reward));
        $max = $reward->getOvertime(TRUE);
        if ($form->isValid($this->_request->getParams())) {
          $values = $form->getValues();
          $cash = (float) $values['overtime_cash'];
          $bank = (float) $values['overtime_bank'];
          if ($max >= $cash + $bank) {
            $reward->setOvertimeCash($cash);
            $reward->setOvertimeBank($bank);
            $this->_flashMessenger->addMessage('Čas zpeněžen.');
          }
          else {
            $this->_flashMessenger->addMessage('Součet hodin nesmí být větší než ' . $max . '.');
          }
        }
        else {
          $this->_flashMessenger->addMessage('Nebyl zadán správný počet hodin. Maximum je ' . $max . '.');
        }
      }
      $this->redirect($this->view->url(array('action' => 'view-month')));
    }

    public function vacationClaimAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      if ($this->_request->isPost()) {

        $idWorker = (int) $this->_request->getParam('worker_id');
        $year = ((int) $this->_request->getParam('year'));

        $date = new Meduse_Date($year . '-12-31');
        $worker = new Employees_Employee($idWorker);
        $reward = new Employees_Reward($worker, $date, false);

        $form = new Employees_VacationsForm(['reward' => $reward]);
        $params = $this->_request->getParams();
        if (!$form->isValid($params)) {
          $this->_flashMessenger->addMessage('Nesprávné hodnoty pro nastavení dovolené.');
        }
        else {
          $claimed = (float) $this->_request->getParam('claimed');
          $transferred = (float) $this->_request->getParam('transferred');
          $tClaims = new Table_EmployeesVacationClaims();
          try {
            $tClaims->insert(['id_employees' => $idWorker, 'year' => $year + 1, 'claimed' => $claimed, 'transferred' => $transferred]);
          }
          catch (Exception $e) {
            $tClaims->update(['claimed' => $claimed, 'transferred' => $transferred], ['id_employees = ' . $idWorker, 'year = ' . ($year + 1)]);
          }
          $tEmp = new Table_Employees();
          $tEmp->update(['vacation' => $claimed], 'id = ' . $idWorker);

          $this->_flashMessenger->addMessage('Nastavení dovolené uloženo.');
        }
      }
      $this->redirect($this->view->url(array('action' => 'view-month')));
    }


    private function _getGridOptions() {
      return array(
        'css_class' => 'table table-bordered table-hover',
        'columns' => array(
          'last_name' => array(
            'header' => 'příjmení',
            'sorting' => TRUE,
          ),
          'first_name' => array(
            'header' => 'jméno',
            'sorting' => TRUE,
          ),
          'typ' => array(
            'header' => 'typ',
            'sorting' => FALSE,
          ),
          'department' => array(
            'header' => 'oddělení',
          ),
          'operations' => array(
            'header' => '',
            'renderer' => 'bootstrap_buttonGroup',
            'css_class' => 'text-right',
            'group' => array(
              array(
                'icon' => 'icon icon-file',
                'text' => 'přehled',
                'url' => $this->view->url(array('action' => 'dashboard', 'id' => '{id}')),
              ),
              array(
                'icon' => 'icon icon-edit',
                'text' => 'editovat',
                'url' => $this->view->url(array('action' => 'edit', 'id' => '{id}')),
              ),
              array(
                'icon' => 'icon icon-check',
                'text' => 'uzavřít měsíc',
                'url' => $this->view->url(array('action' => 'close-month', 'id' => '{id}')),
              ),
            ),
          ),
        )
      );
    }

    public function fetchDsiLastAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $this->redirect($this->view->url(array('action' => 'dashboard')));
    }
  }

