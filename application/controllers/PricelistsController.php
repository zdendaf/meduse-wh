<?php
class PricelistsController extends Meduse_Controller_Action {

    const IMPORT_DIR = '/../data/uploads/';

    public function init() {
      $this->_helper->layout->setLayout('bootstrap-basic');
    }

    public function manageAction() {
      $this->view->lists = Pricelists::getPriceLists();
      $this->view->pricelistForm = Pricelists::getPricelistsForm();
      $this->view->importForm = Pricelists::getImportForm();
      $this->view->openModal = FALSE;
    }

    public function editAction() {
      if ($this->getRequest()->isPost()) {
        $form = new Pricelists_Form(NULL, NULL);
        $req = $this->getRequest();
        if ($form->isValid($req->getParams())) {
            $this->save($req);
            $this->redirect('/pricelists/manage');
        }
        else {
          $this->getPricelists();
          $this->view->pricelistForm->populate($req->getParams());
          $this->view->openModal = TRUE;
          $this->renderScript('pricelists/manage.phtml');
        }
      }
      else {
        $this->manageAction();
      }
    }

    private function save($req) {
        $date = new Zend_Date($req->getParam('valid'), false, new Zend_Locale('cs'));
        $data = array(
            'name' => $req->getParam('name'),
            'abbr' => str_replace(' ', '_', $req->getParam('abbr')),
            'type' => $req->getParam('type'),
            'region' => $req->getParam('region'),
            'valid' => $date->get('YYYY-MM-dd'),
            'assign' => $req->getParam('assign')
        );
        $tLists = new Table_Pricelists();
        $id = (int) $req->getParam('id');
        if ($id) {
            $tLists->update($data, 'id = ' . $id);
            $this->_flashMessenger->addMessage('Ceník byl <em>' . $data['name'] . '</em> byl upraven.');
        } else {
            $tLists->insert($data);
            $this->_flashMessenger->addMessage('Ceník byl <em>' . $data['name'] . '</em> byl uložen.');
        }
    }


    public function deleteAction() {
        $ids = explode(' ', trim($this->getRequest()->getParam('id')));
        if ($ids) {
            $tLists = new Table_Pricelists();
            $tLists->update(array('deleted' => 'y'), 'id IN ( ' . implode(', ', $ids) . ')');
            $this->_flashMessenger->addMessage('Zvolené ceníky byly odstraněny.');
        }
        $this->redirect('/pricelists/manage');
    }

    public function assignAction() {
        $this->_helper->layout->setLayout('bootstrap-basic');
        $edit = $this->getRequest()->getParam('edit', 0);
        $detail = $this->getRequest()->getParam('detail', 0);
        if ($this->getRequest()->isPost()) {
            $tCP = new Table_CustomersPricelists();
            $pricelists = $this->getRequest()->getParam('pricelists', array());
            $customerId = $this->getRequest()->getParam('customer');
            $tCP->assignCustomer($customerId, $pricelists);
            $orderId = $this->getRequest()->getParam('order');
            $message = empty($pricelists) ?
              'Ceníky byly odebrány.' : 'Ceníky byly přiřazeny';
            $this->_flashMessenger->addMessage($message);
            if ($orderId && $edit == 0) {
              $this->redirect('/orders/detail/id/' . $orderId);
            }
            elseif ($orderId && $edit == 1) {
              $this->redirect($this->view->url(array(
                'controller' => 'order-edit',
                'action' => 'customer',
                'order' => $orderId)));
            }
            elseif ($detail) {
              $this->redirect($this->view->url(array(
                'controller' => 'customers',
                'action' => 'detail',
                'id' => $customerId,
                'customer' => NULL)));
            }
            else {
              $this->redirect('/customers');
            }
        }
        $this->view->orderId = $this->getRequest()->getParam('order');
        $tCustomers = new Table_Customers();
        $customer = $tCustomers->find($this->getRequest()->getParam('customer'))->current()->toArray();
        // U B2B zakazniku se neomezujeme pri nabidce ceniku jen na B2B ceniky.
        $ignore = $customer['type'] === Table_Customers::TYPE_B2B;
        $tPricelists = new Table_Pricelists();
        $this->view->lists = $tPricelists->getCustomersPricelists($this->getRequest()->getParam('customer'), $ignore);
        $this->view->customer = $customer;
        $this->view->detail = $detail;
        $this->view->edit = $edit;
    }

    public function getPricelistsAjaxAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $tPricelsists = new Table_Pricelists();
        $lists = $tPricelsists->getCustomersPricelists($params['customer']);
        $this->_helper->json($lists);
    }


    public function getPricelistAjaxAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $data = array();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $tLists = new Table_Pricelists();
            $list = $tLists->find($id)->current();
            if ($list) {
                $data = $list->toArray();
                $date = new Zend_Date($data['valid'], null);
                $data['valid'] = $date->get('d.M.Y');
            }
        }
        $this->_helper->json($data);
    }


  public function importAction() {
    $adapter = new Zend_File_Transfer_Adapter_Http();
    $adapter->setDestination(APPLICATION_PATH . self::IMPORT_DIR);
    $files = $adapter->getFileInfo();
    $adapter->receive();
    if ($adapter->isUploaded($files['datafile']['name']) && $adapter->isValid($files['datafile']['name'])) {
      $fPath = $files['datafile']['destination'] . "/" . $files['datafile']['name'];
      $file = fopen($fPath, 'r');
      if ($file === FALSE) {
        $this->_flashMessenger->addMessage('Chyba: soubor se podařilo otevřít.');
        $this->redirect('/pricelists');
      }
      $ids = explode(' ', trim($this->getRequest()->getParam('id_pricelists')));

      if (!$this->_request->getParam('update_only', FALSE)) {
        $tPrices = new Table_Prices();
        $tPrices->delete('id_pricelists IN (' . implode(', ', $ids) . ')');
      }

      $cols = count($ids) + 2;
      $row = 0;
      while ($data = fgetcsv($file, 1024, ';', '"')) {
        $row++;
        $num = count($data);
        if ($num != $cols) {
          $this->_flashMessenger->addMessage('Pozor: na řádku ' . $row . ' není správný počet sloupců.');
        }
        $this->importPrice($data, $ids);
      }
    }
    $this->redirect('/pricelists/manage');
  }

  protected function importPrice($data, $idPriceLists) {
    $tPrices = new Table_Prices();
    $idPart = trim($data[0]);
    $namePart = trim($data[1]);
    if (!empty($idPart)) {
      foreach ($idPriceLists as $i => $idPriceList) {
        try {
          $tPrices->savePrice($idPart, $namePart, $idPriceList, Meduse_Currency::parse($data[$i + 2]));
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage('Cenu se nepodařilo naimportovat. ' . $e->getMessage());
        }
      }
    }
  }

    public function ajaxGetInfoAction() {
      $this->_helper->layout()->disableLayout();

      $ids = explode(' ', $this->getRequest()->getParam('ids'));
      $mapper = new Table_Pricelists();
      $this->view->pricelists = $mapper->getPricelists($ids);
    }

    /** Export master ceníků */
    public function exportAction() {

      switch ($this->_request->getParam('type')) {
        case 'pdf': return $this->pdfAction();
        case 'xlsx': return $this->xlsxAction();
      }

      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNeverRender();

      $this->getResponse()
        ->setHeader('Content-Disposition', 'attachment; filename="export.csv"')
        ->setHeader('Content-type', 'application/octet-stream');

      $ids = explode(' ', $this->getRequest()->getParam('ids'));
      $mapper = new Table_Pricelists();
      $pricelists = $mapper->getPricelists($ids);

      foreach ($pricelists->getPrices() as $part_id => $prices) {
        echo $part_id . ';';
        $product = new Pipes_Parts_Part($part_id);
        echo $product->getName() . ';';
        $price_array = array();
        foreach ($prices as $price) {
          if (isset($price)) {
            $price_array[] = $price;
          } else {
            $price_array[] = '';
          }
        }
        echo implode(';', $price_array);
        echo "\n";
      }
    }

  public function ctgAction() {
      $this->view->title = 'Přehled slevových kategorií';
      $this->view->ctgDefaults = Pricelists::getCtgDefaults();
      $this->view->ctgLimits = Pricelists::getCtgLimits();
      $this->view->ctgAccessories = Pricelists::getCtgPartsCtg(Table_PartsCtg::TYPE_ACCESSORIES);
      $this->view->ctgCollections = Pricelists::getCtgCollections();
      $this->view->collections = Parts::getCollections();
      $this->view->texts = Pricelists::getCtgTexts();
  }

  /**
   * Editace slevovych kategorii.
   *
   * @return void
   */
    public function editCtgAction() {

      // Může být default, collection, product
      $type = $this->_request->getParam('type', 'default');

      $this->view->title = 'Editace slevových kategorií';
      if ($back = $this->_request->getParam('back')) {
        $this->view->backUrl = base64_decode($back);
      }
      else {
        $this->view->backUrl = $this->view->url(['action' => 'ctg', 'type' => NULL, 'id' => NULL]);
      }

      switch ($type) {
        case 'part':
          $this->editCtgActionPart();
          break;
        case 'collection':
          $this->editCtgActionCollection();
          break;
        case 'accessories':
          $this->editCtgActionAccessories();
          break;
        case 'limits':
          $this->editCtgActionLimits();
          break;
        case 'texts':
          $this->editCtgActionTexts();
          break;
        case 'defaults':
        default:
          $this->editCtgActionDefault();
          break;
      }
    }

    protected function editCtgActionDefault() {
      $form = Pricelists::getCtgDefaultsForm();
      if ($this->_request->isPost()) {
        $params = $this->_request->getParams();
        if ($form->isValid($params)) {
          if ($values = $form->getValidValues($params)) {
            $data = [];
            foreach ($values as $name => $value) {
              if (strpos($name, 'discount_') === 0) {
                list(, $idCtg) = explode('_', $name);
                $data[] = ['id' => (int) $idCtg, 'discount' => (int) $value];
              }
            }
            Pricelists::setCtgDefaults($data);
            $this->_flashMessenger->addMessage('Změny byly uloženy');
            $this->redirect($this->view->url());
          }
        }
      }
      $this->view->form = $form;
    }

    protected function editCtgActionTexts() {
      $form = Pricelists::getCtgTextsForm();
      if ($this->_request->isPost()) {
        $params = $this->_request->getParams();
        if ($form->isValid($params)) {
          $status = TRUE;
          $values = $form->getValidValues($params);
          foreach($values as $idx => $text) {
            $status = $status && Utils::setSetting($idx, $text, Table_Settings::TYPE_STRING);
          }
          if ($status) {
            $this->_flashMessenger->addMessage('Změny byly uloženy.');
          }
          else {
            $this->_flashMessenger->addMessage('Při ukládání došlo k chybám. Viz systemový log.');
          }
          $this->redirect($this->view->url());
        }
      }
      $this->view->form = $form;
    }

    protected function editCtgActionLimits() {
      $form = Pricelists::getCtgLimitsForm();
      if ($this->_request->isPost()) {
        $params = $this->_request->getParams();
        if ($form->isValid($params)) {
          if ($values = $form->getValidValues($params)) {
            $status = TRUE;
            foreach ($values as $name => $value) {
              if (strpos($name, 'limit_') === 0) {
                list(, $idCtg) = explode('_', $name);
                $value = $value === '' ? NULL : (int) $value;
                $status = $status && Utils::setSetting('pricelists_limit_' . $idCtg, $value, Table_Settings::TYPE_INT);
              }
            }
            if ($status) {
              $this->_flashMessenger->addMessage('Změny byly uloženy.');
            }
            else {
              $this->_flashMessenger->addMessage('Při ukládání došlo k chybám. Viz systemový log.');
            }
            $this->redirect($this->view->url());
          }
        }
      }
      $this->view->form = $form;
    }

    protected function editCtgActionCollection() {
      $idColl = (int) $this->_request->getParam('id');
      $collections = Parts::getCollections();
      if (!key_exists($idColl, $collections)) {
        $this->_flashMessenger->addMessage('Kolekce ID = ' . $idColl . ' neexistuje.');
        $this->redirect($this->view->url(['action' => 'ctg', 'type' => NULL, 'id' => NULL]));
      }
      $form = Pricelists::getCtgCollectionForm($idColl);
      if ($this->_request->isPost()) {
        $params = $this->_request->getParams();
        if ($form->isValid($params)) {
          if ($values = $form->getValidValues($params)) {
            $data = [];
            foreach ($values as $name => $value) {
              if (strpos($name, 'discount_') === 0) {
                list(, $idCtg) = explode('_', $name);
                $data[] = ['id_pricelists_ctg' => (int) $idCtg, 'discount' => $value === '' ? NULL : (int) $value];
              }
            }
            Pricelists::setCtgCollection($idColl, $data);
            $this->_flashMessenger->addMessage('Změny byly uloženy');
            $this->redirect($this->view->url());
          }
        }
      }
      $this->view->form = $form;
    }

  protected function editCtgActionAccessories() {
    $form = Pricelists::getCtgAccessoriesForm();
    if ($this->_request->isPost()) {
      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        if ($values = $form->getValidValues($params)) {
          $data = [];
          foreach ($values as $name => $value) {
            if (strpos($name, 'discount_') === 0) {
              list(, $idCtg) = explode('_', $name);
              $data[] = ['id_pricelists_ctg' => (int) $idCtg, 'discount' => $value === '' ? NULL : (int) $value];
            }
          }
          Pricelists::setCtgPartsCtg(Table_PartsCtg::TYPE_ACCESSORIES, $data);
          $this->_flashMessenger->addMessage('Změny byly uloženy');
          $this->redirect($this->view->url());
        }
      }
    }
    $this->view->form = $form;
  }

    protected function editCtgActionPart() {
      $idPart = (string) $this->_request->getParam('id');
      $form = Pricelists::getCtgPartForm($idPart);
      if ($this->_request->isPost()) {
        $params = $this->_request->getParams();
        if ($form->isValid($params)) {
          if ($values = $form->getValidValues($params)) {
            $data = [];
            foreach ($values as $name => $value) {
              if (strpos($name, 'discount_') === 0) {
                list(, $idCtg) = explode('_', $name);
                $data[] = ['id_pricelists_ctg' => (int) $idCtg, 'discount' => $value === '' ? NULL : (int) $value];
              }
            }
            Pricelists::setCtgPart($idPart, $data);
            $this->_flashMessenger->addMessage('Změny byly uloženy');
            $this->redirect($this->view->url());
          }
        }
      }
      $this->view->form = $form;
    }

    protected function indexAction() {
      $useCache = (bool) $this->_request->getParam('use_cache', FALSE);
      $this->view->prices = Pricelists::getPrices(NULL, true, $useCache);
    }

  /**
   * @throws Zend_Pdf_Exception
   * @throws Zend_Exception
   */
  public function pdfAction() {
      $this->_helper->layout()->disableLayout();

      $draft = (bool) $this->_request->getParam('draft', 0);
      $ctgId = $this->_request->getParam('ctg');
      if ($ctgId !== NULL) {
        $ctgIds = explode(' ', $ctgId);
        $ctgIds = array_map(function ($item) {
          return (int) $item;
        }, $ctgIds);
      }
      else {
        $ctgIds = Pricelists::getAllCategoriesIdx();
        array_unshift($ctgIds, 0);
      }

      $type = $this->_request->getParam('type', 'pdf');
      $process = in_array($type, ['pdf', 'xlsx']);

      if (!$process) {
        $this->_flashMessenger->addMessage('Neznámý typ výstupu.');
      }

      if ($process && $this->checkCategoryIds($ctgIds)) {
        $count = count($ctgIds);
        $first = reset($ctgIds);
        if ($count === 1) {
          $fileName = $first == 0 ? 'retail_price_list'  : 'wholesale_' . $first . '_price_list';
        }
        else  {
          $fileName = 'multi_price_list';
        }
        $fileName .= ($draft ? '_draft.' : '.') . $type;

        $collections = $this->_request->getParam('collections');
        $categories = $this->_request->getParam('categories');
        $excluded = $this->_request->getParam('excluded');
        $filter = [
          'collections' => $collections ? explode(' ', $collections) : [],
          'categories' => $categories ? explode(' ', $categories) : [],
          'excluded' => $excluded ? explode(' ', $excluded) : [],
        ];

        $this->view->filename = $fileName;
        switch ($type) {
          case 'pdf':
            $this->view->pdf = Pricelists::generatePdf($ctgIds, $draft, $filter);
            return;

          case 'xlsx':
            $filePath = Zend_Registry::get('config')->dirs->tmp . '/' . $fileName;
            try {
              $objPHPExcel = Pricelists::generateXlsx($ctgIds, $draft, $filter);
              $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
              $objWriter->setOffice2003Compatibility(TRUE);
              $objWriter->save($filePath);
              $this->view->fileSize = filesize($filePath);
              $this->view->xlsxResource = fopen($filePath, 'r');
              fclose($filePath);
              unlink($this->view->xlsx);
            }
            catch (Exception $e) {
              Utils::logger()->err($e->getMessage() . ' Trace: ' . PHP_EOL . $e->getTraceAsString());
            }
            return;
        }
      }

      $this->redirect($this->view->url([
        'action' => NULL,
        'use_cache' => 1,
        'ctg' => NULL,
        'draft' => NULL,
      ]));
    }

    private function checkCategoryIds(array $categories): bool
    {
      $ok = TRUE;
      foreach ($categories as $ctgId) {
        if ($ctgId < 0 || $ctgId > Pricelists::CTG_NUM) {
         $ok = FALSE;
          $this->_flashMessenger->addMessage('Číslo slevové kategorie (' . $ctgId . ') není platné.');
        }
      }
      return $ok;
    }

  /**
   * Vygeneruje interní ceníky.
   * @return void
   */
    public function generateAction() {

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();


      $draft = (bool) $this->_request->getParam('draft', 0);

      $ctgId = $this->getParam('ctg');
      if ($ctgId !== NULL) {
        $ctgIds = explode(' ', $ctgId);
        $ctgIds = array_map(static function ($item) {
          return (int) $item;
        }, $ctgIds);
      }
      else {
        $ctgIds = Pricelists::getAllCategoriesIdx();
        array_unshift($ctgIds, 0);
        array_unshift($ctgIds, -1);
      }

      $problems = [];
      $generated = Pricelists::generate($ctgIds, $draft, $problems);

      $message = (count($generated) === 1 ? 'Byl generován ceník ' : 'Byly generovány ceníky ') . implode(', ', $generated) . '.';
      $this->_flashMessenger->addMessage($message);

      if ($problems) {
        foreach ($problems as $problem) {
          $this->_flashMessenger->addMessage($problem);
        }
      }

      $this->redirect($this->view->url([
        'action' => NULL,
        'ctg' => NULL,
        'use_cache' => 1,
        'draft' => NULL,
      ]));
    }

  /**
   * @throws Zend_Form_Exception
   */
  public function mappingAction():void
    {
      $mappingForm = Pricelists::getMappingForm(Table_PartsWarehouse::WH_TYPE);
      $this->view->form = $mappingForm;

      if ($this->_request->isPost()) {
        $params = $this->_request->getParams();
        if ($mappingForm->isValid($params) && $values = $mappingForm->getValidValues($params)) {
          foreach ($values as $key => $value) {
            Utils::setSetting(Pricelists::MAPPING_PREFIX . $key, $value, Table_Settings::TYPE_INT);
          }
        }
      }
    }
}
