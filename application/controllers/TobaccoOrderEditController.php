<?php

  class TobaccoOrderEditController extends Meduse_Controller_Action {

    protected $db = NULL;
    protected $form;

    public function init() {

      $this->db = Zend_Registry::get('db');
      $this->_helper->layout->setLayout('bootstrap-tobacco');

      $this->view->action = $this->_request->getActionName();

      $orderId = (int) $this->_request->getParam('order', null);
      $this->view->order = $orderId;


      $this->form = new Tobacco_Orders_Form([
        'from_action' => $this->view->action,
        'action' => $this->view->url(['action' => 'process']),
      ]);
      $this->form->setLegend("Objednávka");

      // editace existujici zakazky
      if ($this->view->order) {

        try {
          $order = new Orders_Order($this->view->order);
        }
        catch (Orders_Exceptions_ForbiddenAccess $e) {
          $this->_helper->getHelper('FlashMessenger')->addMessage($e->getMessage());
          $this->_redirect($this->view->url(array('controller' => 'tobacco-orders', 'action' => 'index', 'order' => NULL, 'id' => NULL)));
        }

        $this->order = $order;
        $this->view->title = 'Objednávka ' . $order->getNO() . ' - editace';
        $this->view->orderObj = $order;

        $this->form->meduse_orders_no->setValue($order->getNO());
        $this->form->meduse_orders_type->setValue($order->isService() ? Table_Orders::TYPE_SERVICE : Table_Orders::TYPE_PRODUTCS);
        // $this->form->meduse_orders_private->setValue($order->isPrivate() ? 'y' : 'n');
        $this->form->meduse_orders_name->setValue($order->getTitle());
        $this->form->meduse_orders_desc->setValue($order->getDesc());
        $this->form->meduse_orders_status->setValue($order->getStatus());
        $this->form->meduse_orders_carrier->setValue($order->getCarrierMethodId());
        $this->form->meduse_orders_carrier_price->setValue($order->getRow()->carrier_price);
        $this->form->meduse_orders_expected_payment_currency->setValue($order->getRow()->expected_payment_currency);
        $this->form->meduse_orders_payment_method->setValue($order->getRow()->payment_method);

        $status = $order->getStatus();
        $this->view->showProducts = (
                $status != Table_Orders::STATUS_OPEN
                || Utils::isAllowed('orders/edit-opened-products'))
                && $status != Table_Orders::STATUS_CLOSED;
        $customer = $order->getCustomer();
        if ($customer && !$order->isService()) {
          $tPricelists = new Table_Pricelists();
          $pricelistsValid = $tPricelists->getCustomersPricelists($customer->id);
          $pricelistCurrent = $tPricelists->getCustomersValidPricelist($customer->id);
          if ($pricelistCurrent) {
            $this->form->meduse_orders_pricelist->removeMultiOption('null');
          }
          $this->addPricelists($pricelistsValid);
          $this->form->meduse_orders_pricelist->setValue(
            is_null($order->getRow()->id_pricelists) ?
              $pricelistCurrent : $order->getRow()->id_pricelists
          );
        }

      }

      // zalozeni nove zakazky
      else {
        $newOrder = new Zend_Session_Namespace('new_order');
        if($newOrder->new_order) {
          $this->form->populate($newOrder->new_order);
          $newOrder->new_order = null;
        }
        $this->form->setNextNo(Table_TobaccoWarehouse::WH_TYPE);
        $this->form->setLegend('Nová objednávka');
        $this->view->title = 'Nová objednávka';
      }

      $this->form->setAction($this->view->url(array('action' => 'process')));
      $this->form->addElement("hidden", "from_action", array('value'=>$this->view->action));
    }

    private function removeIndexElements() {
      $this->form->removeElement('meduse_orders_pricelist');
      $this->form->removeElement('meduse_orders_payment_method');
      $this->form->removeElement('meduse_orders_expected_payment_currency');
      $this->form->removeElement('meduse_orders_carrier');
      $this->form->removeElement('meduse_orders_carrier_price');
      $this->form->removeElement('meduse_orders_discount');
      $this->form->removeElement('meduse_orders_discount_type');
      $this->form->removeElement('meduse_orders_pay_all');
      $this->form->removeElement('meduse_orders_pay_deposit');
      $this->form->removeElement('default_price');
    }

    private function removePaymentElements() {
      $this->form->removeElement('meduse_orders_no');
      $this->form->removeElement('meduse_orders_type');
      $this->form->removeElement('meduse_orders_name');
      $this->form->removeElement('meduse_orders_desc');
      $this->form->removeElement('meduse_orders_deadline');
      $this->form->removeElement('meduse_orders_status');
      $this->form->removeElement('meduse_orders_carrier');
      $this->form->removeElement('meduse_orders_carrier_price');
      $this->form->removeElement('default_price');
      if ($this->order->isService()) {
        $this->form->removeElement('meduse_orders_pricelist');
      }
    }

    private function removeTransportElements() {
      $this->form->removeElement('meduse_orders_no');
      $this->form->removeElement('meduse_orders_type');
      $this->form->removeElement('meduse_orders_name');
      $this->form->removeElement('meduse_orders_desc');
      $this->form->removeElement('meduse_orders_deadline');
      $this->form->removeElement('meduse_orders_status');
      $this->form->removeElement('meduse_orders_pricelist');
      $this->form->removeElement('meduse_orders_payment_method');
      $this->form->removeElement('meduse_orders_expected_payment_currency');
    }

    private function removePrivate($hard = TRUE) {
      /*
      if ($hard || Utils::isDisallowed('orders/create-private')) {
        $this->form->removeElement('meduse_orders_private');
      }
      */
    }

    public function indexAction() {
      $this->removeIndexElements();
      $this->removePrivate(FALSE);

      if(!$this->view->order) {
        $this->form->getElement('meduse_orders_status')->setValue(Table_Orders::STATUS_NEW);
      }
      if(!Utils::isAllowed('orders/special/status')){
          $this->form->removeElement('meduse_orders_status');
      }
      $this->view->showProductsTransport = isset($this->order) && !$this->order->isService();
      $this->view->form = $this->form;
    }

    public function paymentAction() {
      $this->form->setLegend("Objednávka - Platba");
      $this->removePaymentElements();
      $this->removePrivate();
      $this->view->form = $this->form;
      $this->view->showProductsTransport = isset($this->order) && !$this->order->isService();
      $this->render('index');
    }

    public function transportAction() {
      $this->form->setLegend("Objednávka - Doprava");
      $this->removeTransportElements();
      $this->removePrivate();
      $this->view->form = $this->form;
      $this->view->showProductsTransport = isset($this->order) && !$this->order->isService();
      $this->render('index');
    }

    public function customerAction() {
      $customer_id = $this->order->getRow()->id_customers;
      if(!is_null($customer_id) && $customer_id != 0) {
        $customer = new Customers_Customer($customer_id);
        $this->view->customer = $customer;
        $this->view->addresses = $customer->getAddresses();
        $this->view->address = $this->order->getAddress();
        $this->view->order = $this->order;
      }
      else {
        $params = $this->getRequest()->getParams();
        $params['order_id'] = $this->order->getRow()->id;
        $params['wh'] = Table_TobaccoWarehouse::WH_TYPE;
        $this->view->grid = Customers::getDataGrid($params);
      }
      $this->view->showProductsTransport = isset($this->order) && !$this->order->isService();
    }

    public function servicesAction() {
      if ($this->_request->isPost()) {
        $data = $this->getAllParams();
        $i = 0;
        $services = array();
        while($i < $this->getParam('rowsnum')) {
          $services[] = array(
            'id' => empty($this->getParam('id_' . $i)) ? NULL : $this->getParam('id_' . $i),
            'description' => $this->getParam('description_' . $i),
            'price' => $this->getParam('price_' . $i),
            'qty' => $this->getParam('qty_' . $i),
            'delete' => $this->getParam('delete_' . $i) == 'y',
          );
          $i++;
        }
        $order = new Orders_Order($data['id_orders']);
        if ($order->setServices($services)) {
          $this->_flashMessenger->addMessage('Služby byly uloženy');
          $total = $order->getServicesTotalPrice();
          $order->setExpectedPayment($total);
          $order->invalidInvoices();
        }
        else {
          $this->_flashMessenger->addMessage('Při ukládání došlo k chybě.');
        }
      }
      else {
        $order = new Orders_Order($this->getParam('order'));
      }
      $services = $order->getServices();
      $form = new Orders_ServicesForm(array('rows' => count($services)));
      $data = array('id_orders' => $this->getParam('order'), 'rows' => $services);
      $form->populate($data);
      $this->view->form = $form;
    }

    /**
     * @throws Orders_Exceptions_ForbiddenAccess
     * @throws Zend_Date_Exception
     * @throws Zend_Exception
     * @throws Zend_Db_Table_Exception
     * @throws Exception
     */
    public function processAction(): void {

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $action = $this->_request->getParam('from_action',false);
      $ord = $this->_request->getParam('order');

      //zpracovani vsech formularu
      if ($this->_request->isPost() && $this->_request->getParam('from_action',false)) {

        $this->removeFormElements($action);

        $valid_no = TRUE;
        // Pokud jsme na prvni straně formuláře, musíme zkontrolovat unikátnost čísla objednávky.
        if ($this->_request->getParam('from_action') === 'index') {
          $no = $this->_request->getParam('meduse_orders_no');
          $id = $this->_request->getParam('order');
          $valid_no = Orders_Order::isValidNo($no, $id, Table_TobaccoWarehouse::WH_TYPE);
        }

        if ($this->form->isValid($this->_request->getParams()) && $valid_no) {
          $values = $this->form->getValues();
          $orderRow = $this->mapOrderFromForm($values, $this->view->order);

          if ($orderRow && isset($values['meduse_orders_expected_payment_currency'])) {
            if ($values['meduse_orders_expected_payment_currency'] === Table_Orders::CURRENCY_CZK) {
              $orderRow->expected_payment_rate = 1;
            }
            else {
              $orderRow->expected_payment_rate = null;
            }
          }
          $id = $orderRow->save();

          // prepocet ceny
          $order = new Tobacco_Orders_Order($id);
          $order->recalculateExpectedPayment();

          $this->_flashMessenger->addMessage("Uloženo");
          $this->redirect($this->view->url(array('action' => $action, 'order' => $orderRow->id)));

        }
        else {
          if (!$valid_no) {
            $this->_flashMessenger->addMessage('Objednávka s číslem ' . $this->_request->getParam('meduse_orders_no') . ' již existuje.');
          }
          $this->_flashMessenger->addMessage("Formulář nebyl uložen. "
            . "Vyplňte správně požadované položky:<br />");

          foreach ($this->form->getErrors() as $field => $error) {
            foreach ($error as $what) {
              if ($field === 'meduse_orders_no' && $what === 'isEmpty') {
                $this->_flashMessenger->addMessage('Chybí čílso zakázky.');
              }
              if ($field ==='meduse_orders_name' && $what === 'isEmpty') {
                $this->_flashMessenger->addMessage('Chybí název zakázky.');
              }
              if ($field === 'meduse_orders_deadline' && $what === 'minimum') {
                $this->_flashMessenger->addMessage('Deadline nesmí být v minulosti.');
              }
            }
          }

          if(!$this->view->order) {
            $ord = null;
          }
          $newOrder = new Zend_Session_Namespace('new_order');
          $newOrder->new_order = $this->form->getValues();
          $this->redirect($this->view->url(array('action' => $action, 'order' => $ord)));
        }
      }
    }

    public function assignCustomerAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $customer = $this->_request->getParam('customer', false);
      $order = $this->_request->getParam('order', false);
      if ($order !== false && $customer !== false ) {
        $tCustomer = new Table_Customers();
        $rCustomer = $tCustomer->find($customer)->current();
        $this->order->getRow()->id_customers = $customer;
        $this->order->getRow()->purchaser    = $rCustomer->type == 'B2B' ? 'business' : 'personal';
        $this->order->getRow()->id_addresses = $rCustomer->id_addresses;
        $tPricelists = new Table_Pricelists();
        $this->order->getRow()->id_pricelists = $tPricelists->getCustomersValidPricelist($customer);
        $this->order->getRow()->title         = $rCustomer->company ? $rCustomer->company : implode(' ' , array ($rCustomer->first_name, $rCustomer->last_name));

        $customerObj = new Customers_Customer($customer);
        $billingAddressId = $customerObj->getBillingAddressId();
        $tAddress = new Table_Addresses();
        $addressRow = $tAddress->getAddress($billingAddressId);
        if ($addressRow['country'] === Table_Addresses::COUNTRY_CZ) {
          $this->order->setExpectedCurrency(Table_Orders::CURRENCY_CZK);
        }
        else {
          $this->order->setExpectedCurrency(Table_Orders::CURRENCY_EUR);
        }


        $this->order->getRow()->save();
        $this->_flashMessenger->addMessage("Objednávce byl přiřazen zákazník.");
        $this->_redirect($this->view->url(array('action' => 'customer', 'customer' => null)));
      }
      $this->_flashMessenger->addMessage('Nebylo zadáno ID zákazníka nebo objednávky.');
      $this->_redirect("/tobacco-orders");
    }

    public function removeCustomerAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      if (false !== $this->_request->getParam('order', false)
          && false !== $this->_request->getParam('customer', false)) {
        $this->order->getRow()->id_customers = new Zend_Db_Expr('NULL');
        $this->order->getRow()->id_addresses = new Zend_Db_Expr('NULL');
        $this->order->getRow()->purchaser    = new Zend_Db_Expr('NULL');
        $this->order->getRow()->id_pricelists = new Zend_Db_Expr('NULL');
        $this->order->getRow()->title = 'N/A - dle zákazníka';
        $this->order->getRow()->save();
        $this->_flashMessenger->addMessage("Přiřazení zákazníka bylo zrušeno.");
        $this->_redirect($this->view->url(array('action' => 'customer', 'customer' => null)));
      }
      $this->_redirect("/tobacco-orders");
    }

    private function removeFormElements($action) {
      switch($action){
        case 'index':
          $this->removeIndexElements();
          $this->removePrivate(FALSE);
          break;
        case 'payment':
          $this->removePaymentElements();
          $this->removePrivate();
          break;
        case 'transport':
          $this->removeTransportElements();
          $this->removePrivate();
          break;
        case 'customer':
          throw new Exception('lalalalal');
          break;
        default:
          $this->_redirect('/orders');
          break;
      }
    }

    /**
     * @throws Zend_Date_Exception
     * @throws Zend_Db_Table_Exception
     */
    private function mapOrderFromForm($formData, $orderId) {

      $tOrders = new Table_Orders();
      if(empty($orderId)) {
        $rOrder = $tOrders->createRow();
        $rOrder->id_wh_type = Table_TobaccoWarehouse::WH_TYPE;
        $rOrder->inserted = new Zend_Db_Expr("NOW()");
        $rOrder->date_request = new Zend_Db_Expr("NOW()");
        $authNamespace = new Zend_Session_Namespace('Zend_Auth');
        $rOrder->owner = $authNamespace->user_id;
      } else {
        $rOrder = $tOrders->find($orderId)->current();
      }

      $formFields = array(
        // 'private' => "meduse_orders_private",
        'no' => "meduse_orders_no",
        'type' => "meduse_orders_type",
        'title' => "meduse_orders_name",
        'date_deadline' => "meduse_orders_deadline",
        'description' => "meduse_orders_desc",
        'status' => "meduse_orders_status",
        'id_pricelists' => "meduse_orders_pricelist",
        'payment_method' => "meduse_orders_payment_method",
        //'expected_payment' => "meduse_orders_expected_payment",
        'expected_payment_rate' => "meduse_orders_expected_payment_rate",
        'expected_payment_deposit' => "meduse_orders_expected_payment_deposit",
        'expected_payment_currency' => "meduse_orders_expected_payment_currency",
        'id_orders_carriers' => "meduse_orders_carrier",
        'carrier_price' => "meduse_orders_carrier_price",
        'discount' => "meduse_orders_discount",
        'discount_type' => "meduse_orders_discount_type",
        'date_pay_all' => "meduse_orders_pay_all",
        'date_pay_deposit' => "meduse_orders_pay_deposit"
      );

      foreach ($formFields as $dbName => $fieldName) {
        if (isset($formData[$fieldName])) {
          if (in_array($fieldName, array('meduse_orders_deadline', 'meduse_orders_pay_all', 'meduse_orders_pay_deposit'))){
            if (!empty($formData[$fieldName])){
              $date = new Zend_Date($formData[$fieldName], 'dd.MM.yyyy');
              $rOrder->{$dbName} = $date->toString('yyyy-MM-dd');
            }
            else {
              $rOrder->{$dbName} = new Zend_Db_Expr('NULL');
            }
          }
          elseif ($fieldName === 'meduse_orders_carrier_price') {
            if (trim($formData[$fieldName]) === '') {
              $rOrder->{$dbName} = new Zend_Db_Expr('NULL');
            }
            else {
              $rOrder->{$dbName} = (float) $formData[$fieldName];
            }
          }
          elseif ($fieldName === 'meduse_orders_pricelist') {
            $rOrder->{$dbName} = $formData[$fieldName] === 'null' ? new Zend_Db_Expr('NULL') : (int) $formData[$fieldName];
          }
          elseif ($fieldName === 'meduse_orders_payment_method') {
            $rOrder->{$dbName} = $formData[$fieldName] === 'null' ? new Zend_Db_Expr('NULL') : $formData[$fieldName];
          }
          elseif ($fieldName === 'meduse_orders_expected_payment_rate') {
            $rOrder->{$dbName} = $formData['meduse_orders_expected_payment_rate_use_default'] == 'y' ? new Zend_Db_Expr('NULL') : (float) $formData[$fieldName];
          }
          else {
            $rOrder->{$dbName} = $formData[$fieldName];
          }
        }
      }
      return $rOrder;
    }

    private function getGrid($destination = NULL) {
      $grid = new ZGrid(array(
        'title' => _("Zákazníci") ,
        'css_class' => 'table table-bordered table-hover table-striped',
        'add_link_url' => $this->view->url(array(
            'module' => 'default' , 'controller' => 'tobacco-customers' ,
            'action' => 'edit'
        ), null, true) . ($destination ? '?destination=' . $destination : ''),
        'allow_add_link' => true ,
        'add_link_title' => _('Přidat zákazníka') ,
        'columns' => array(
            'company' => array(
            'header' => _('Firma') ,
            'css_style' => 'text-align: left' ,
            'sorting' => true,
            'renderer' => 'url' ,
            'url' => $this->view->url(array(
                    'module' => 'default' , 'controller' => 'tobacco-customers' ,
                    'action' => 'detail' , 'id' => '{id}'
                ), null, true)
            ) ,
        'first_name' => array(
            'header' => _('Jméno') ,
            'css_style' => 'text-align: left' ,
            'sorting' => true,
            'renderer' => 'url' ,
            'url' => $this->view->url(array(
                'module' => 'default' , 'controller' => 'tobacco-customers' ,
                'action' => 'detail' , 'id' => '{id}'
            ), null, true)
          ) ,
          'last_name' => array(
                'header' => _('Příjmení') , 'sorting' => true ,
                'css_style' => 'text-align: left' ,
                'renderer' => 'url' ,
                'url' => $this->view->url(array(
                    'module' => 'default' , 'controller' => 'tobacco-customers' ,
                    'action' => 'detail' , 'id' => '{id}'
                ), null, true)
            ) ,
          'phone' => array(
                'header' => _('Telefon') , 'sorting' => false ,
                'css_style' => 'text-align: left'
          ),
          'assign' => array(
                'header' => '' , 'css_style' => 'text-align: center' ,
                'renderer' => 'action' ,
                'type' => 'assign' ,
                'url' => $this->view->url(array(
                    'module' => 'default' , 'controller' => 'tobacco-order-edit' ,
                    'action' => 'assign-customer' , 'customer' => '{id}' , 'order' => $this->order->getRow()->id
                ), null, true)
           )
        )
      ));
      return $grid;
    }

    private function addPricelists($pricelists) {
      foreach ($pricelists as $list) {
        $this->form->meduse_orders_pricelist->addMultiOption($list['id'], $list['name']);
      }
    }

    public function ajaxCustomerAction() {
      $table = new Table_Customers();
      $results = $table->getPossibleNames($this->_request->getParam('term'), Table_TobaccoWarehouse::WH_TYPE);
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $this->_helper->json($results);
    }

  }
