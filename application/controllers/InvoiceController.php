<?php
class InvoiceController extends Meduse_Controller_Action{

  protected Zend_Db_Adapter_Abstract $db;


  public function init(): void {
    $this->_helper->layout->setLayout('bootstrap-basic');
    $this->db = Utils::getDb();
  }

	public function indexAction() {

    $filter = new Invoice_Filter();
    $filter->setAction($this->view->url(array('page' => null)));

		$tInvoices = new Table_Invoices();

    $acl = Zend_Registry::get('acl');

    if ($this->_request->getParam('new')) {
      Zend_Registry::get('invoice_filter')->title = null;
      Zend_Registry::get('invoice_filter')->customer_type = null;
      Zend_Registry::get('invoice_filter')->no = null;
      Zend_Registry::get('invoice_filter')->type = null;
      Zend_Registry::get('invoice_filter')->region = null;
      Zend_Registry::get('invoice_filter')->language = null;
      Zend_Registry::get('invoice_filter')->inserted_from = null;
      Zend_Registry::get('invoice_filter')->inserted_to = null;
      $this->redirect($this->view->url(array('page' => null, 'new' => null)));
    }

    $invoice_filter = Zend_Registry::get('invoice_filter');
    if ($this->_request->isPost()) {
      $invoice_filter->title         = $this->_request->getParam('title');
      $invoice_filter->customer_type = $this->_request->getParam('customer_type');
      $invoice_filter->no            = $this->_request->getParam('no');
      $invoice_filter->type          = $this->_request->getParam('type');
      $invoice_filter->region        = $this->_request->getParam('region');
      $invoice_filter->language      = $this->_request->getParam('language');
      $invoice_filter->inserted_from = $this->_request->getParam('inserted_from');
      $invoice_filter->inserted_to   = $this->_request->getParam('inserted_to');
      Zend_Registry::set('invoice_filter', $invoice_filter);
    }

    //uchování dat ve formuláři
    $filter->isValid(array(
        'title'         => $invoice_filter->title,
        'customer_type' => $invoice_filter->customer_type,
        'no'            => $invoice_filter->no,
        'type'          => $invoice_filter->type,
        'region'        => $invoice_filter->region,
        'language'      => $invoice_filter->language,
        'inserted_from' => $invoice_filter->inserted_from,
        'inserted_to'   => $invoice_filter->inserted_to,
    ));
    $this->view->filter = $filter;

    $select = $tInvoices->getFilteredInvoicesSelect([
      'wh' => Table_PartsWarehouse::WH_TYPE,
      'only_numbered' => ! Utils::isAllowed('invoice/show-non-numbered'),
      'only_closed' => Utils::isDisallowed('invoice/download-not-closed'),
      'filter' => $invoice_filter,
      ]
    );
		$gridArray = array(
			'css_class' => 'table table-hover',
      'curr_items_per_page' => 25,
      'curr_sort' => 'inserted',
      'curr_sort_order' => ZGrid::ORDER_DESC,
			'columns' => array(
				'order_no'   => array(
					'header' => _('Obj. č.'),
					'sorting' => true,
					'renderer' => 'url',
					'url' => $this->view->url(array(
						'module' => 'default' , 'controller' => 'orders' ,
                        'action' => 'detail', 'id' => '{order_id}' , 'sort' => null, 'sorder' => null
					)) . '#invoices',
				),
				'title' => array(
					'header' => _('Název'),
					'sorting' => true,
					'renderer' => 'url',
					'url' => $this->view->url(array(
						'module' => 'default' , 'controller' => 'orders' ,
                        'action' => 'detail', 'id' => '{order_id}' , 'sort' => null, 'sorder' => null
					)) . '#invoices',
				),
				'invoiced_total' => array(
				  'header' => _('Částka'),
          'header_class' => 'text-right',
          'sorting' => FALSE,
          'renderer' => 'currency',
          'currency_options' => array(
            'currency_value' => 'invoiced_total',
            'currency_symbol' => 'currency',
            'rounding_precision' => 2,
          ),
          'css_class' => 'text-right',
        ),
        'customer_type' => array(
					'header' => _('ZK'),
					'sorting' => true,
				),
        'invoice_no' => array(
					'header' => _('Číslo'),
					'sorting' => true,
				),
        'invoice_type' => array(
					'header' => _('Typ faktury'),
					'sorting' => true,
				),
        'region' => array(
					'header' => _('Region'),
					'sorting' => true,
				),
        'language' => array(
					'header' => _('Jazyk'),
					'sorting' => true,
				),
        'inserted' => array(
					'header' => _('Vloženo'),
					'sorting' => true,
          'renderer' => 'date'
				),
				'locked' => array(
					'header' => '',
          'renderer' => 'condition',
          'condition_value' => 'n',
          'condition_true' => array(
            'renderer' => 'Action',
            'type' => 'edit',
            'url' => $this->view->url(array(
              'module' => 'default' , 'controller' => 'invoice' ,
                          'action' => 'prepare', 'id' => '{id}' , 'sort' => null, 'sorder' => null
            ))
          ), null, true
				),
				'PDF' => array(
					'header' => 'Faktura',
          'css_style' => 'text-align: center',
          'renderer' => 'Bootstrap_Button',
          'icon' => 'icon-download-alt',
					'url' => $this->view->url(array(
						'module' => 'default' , 'controller' => 'invoice' ,
                        'action' => 'download', 'id' => '{id}' , 'sort' => null, 'sorder' => null
					)), null, true
				),
				'registered' => array(
					'header' => '',
          'renderer' => 'condition',
          'condition_value' => 'y',
          'condition_true' => array(
            'renderer' => 'Bootstrap_Button',
            'icon' => 'icon-warning-sign',
            'url' => $this->view->url(array(
              'module' => 'default' , 'controller' => 'invoice' ,
                          'action' => 'deregister', 'id' => '{id}' , 'sort' => null, 'sorder' => null
            )),
          ), null, true
				),
				'payment' => array(
					'header' => 'Úhrada',
          'css_style' => 'text-align: center',
          'renderer' => 'condition',
          'condition_value' => 'u',
          'condition_true' => array(
            'renderer' => 'Bootstrap_Button',
            'icon' => 'icon-plus',
            'url' => $this->view->url(array(
              'module' => 'default',
              'controller' => 'invoice',
              'action' => 'payment-add',
              'invoice' => '{id}',
              'id' => NULL,
              'sort' => NULL,
              'sorder' => NULL,
            )),
          ),
          null, true
				),

			)
		);

    if (!$acl->isAllowed('invoice/prepare')) {
      unset($gridArray['columns']['locked']);
    }
    if (!$acl->isAllowed('invoice/deregister')) {
      unset($gridArray['columns']['registered']);
    }
    if (!$acl->isAllowed('orders/detail')) {
      unset($gridArray['columns']['order_no']['renderer']);
      unset($gridArray['columns']['title']['renderer']);
    }
    if (!$acl->isAllowed('customers/detail')) {
      unset($gridArray['columns']['company']['renderer']);
    }
    $grid = new ZGrid($gridArray);
		$grid->setSelect($select);
		$grid->setRequest($this->_request->getParams());
		$this->view->grid = $grid->render();
	}

  /*
   * Přehled neuhrazených faktur po splatnosti
   */
  public function unpaidAction() {
    $this->view->title = 'Přehled neuhrazených faktur po splatnosti';
    $table = new Table_Invoices();
    $this->view->invoices = $table->getPaymentsStats(array(
      'overdue' => TRUE,
      'unpaid' => TRUE,
      'wh' => Table_PartsWarehouse::WH_TYPE,
      ));
  }

  public function prepareAction(): void {
    $messages = [];

    $form = new Invoice_Form();

    // ulozeni nastaveni faktury
    // formular je validni - vlozime nebo updatujeme
    if ($this->_request->isPost() && $form->isValid($this->_request->getPost())) {

      $invoiceId = $this->_request->getParam('id', NULL);

      // kontrola unikatnosti cisla faktury
      $table = new Table_Invoices();
      $no = $this->_request->getParam('invoice_no', FALSE) ? $this->_request->getParam('invoice_no') : $this->_request->getParam('proform_no', NULL);
      $type = $this->_request->getParam('invoice_type');
      $id = $table->isFree($no, $type, Pipes_Parts_Part::WH_TYPE);
      if ($id === TRUE || $id == $this->_request->getParam('id')) {
        $authNamespace = new Zend_Session_Namespace('Zend_Auth');

        $issueDate = $this->_request->getParam('issue_date') ? new Meduse_Date($this->_request->getParam('issue_date'), 'Y-m-d') : new Meduse_Date();
        $duzp = $this->_request->getParam('duzp') ? new Meduse_Date($this->_request->getParam('duzp'), 'Y-m-d') : new Meduse_Date();
        $dueDate = $this->_request->getParam('due_date') ? new Meduse_Date($this->_request->getParam('due_date', 'Y-m-d')) : NULL;


        /**
         * Nastavení data splatnosti u regular faktury.
         *
         * 1) Faktura neexituje:
         *    a) Datum splatnosti je prázdné:
         *        Použít výchozí hodnotu z data vystavení.
         *    b) Datum splatnosti není prázdné.
         *        Zkontrolovat maximum, případně oprávnění:
         *        - Pokud projde, uložit tuto hodnotu.
         *        - Pokud ne, použít výchozí hodnotu z data vystavení.
         *
         * 2) Faktura existuje:
         *    a) Datum splatnosti je prázdné:
         *        Použít původní hodnotu.
         *    b) Datum splatnosti je menší nebo rovno jako před uložením:
         *        Použít novou hodnotu.
         *    c) Datum splatnosti je větší než před uložením:
         *        Zkontrolovat maximum, případně oprávnění:
         *        - Pokud projde, uložit tuto hodnotu.
         *        - Pokud ne, použít původní hodnotu.
         */
        if ($type === Table_Invoices::TYPE_REGULAR) {
          $maxDate = new Meduse_Date($issueDate);
          $maxDate->addDay(Invoice_Invoice::DUE_DATE_DAYS);
          if ($invoiceId) {
            $invoice = new Invoice_Invoice($invoiceId);
            $dueDateSaved = new Meduse_Date($invoice->getDueDate());
            /* 2a */
            if (!$dueDate) {
              $dueDate = $dueDateSaved;
            }
            /* 2c */
            elseif ($dueDate > $dueDateSaved) {
              if ($maxDate < $dueDate && !Zend_Registry::get('acl')
                  ->isAllowed('invoice/overdue')) {
                $dueDate = $dueDateSaved;
              }
            }
          }
          /* 1a, 1b */
          elseif (!$dueDate || ($maxDate < $dueDate && !Zend_Registry::get('acl')
                ->isAllowed('invoice/overdue'))) {
            $dueDate = $maxDate;
          }
        }

        $data = [
          'mask_zero_prices' => $this->_request->getParam('mask_zero_prices', 'n'),
          'invoice_price' => $this->_request->getParam('invoice_price', 0),
          'reduction' => $this->_request->getParam('reduction', 0),
          'show_vat_summary' => $this->_request->getParam('show_vat_summary', 'n'),
          'rounding' => $this->_request->getParam('rounding', 'n'),
          'allow_delivery_addr' => $this->_request->getParam('allow_delivery_addr', 'n'),
          'delivery_text' => $this->_request->getParam('delivery_text', NULL),
          'weight' => $this->_request->getParam('weight'),
          'type' => $type,
          'no' => $no,
          'currency' => $this->_request->getParam('currency'),
          'rate' => $this->_request->getParam('rate'),
          'use_actual_rate' => $this->_request->getParam('use_actual_rate'),
          'language' => $this->_request->getParam('language'),
          'packaging' => $this->_request->getParam('packaging'),
          'packaging2' => $this->_request->getParam('packaging2'),
          'additional_discount' => (int) $this->_request->getParam('additional_discount'),
          'additional_discount_include' => $this->_request->getParam('additional_discount_include', 'n'),
          'issue_date' => $issueDate->toString('Y-m-d'),
          'duzp' => $duzp->toString('Y-m-d'),
          'due_date' => is_null($dueDate) ? NULL : $dueDate->get('Y-m-d'),
          'text1' => $this->_request->getParam('text1'),
          'add_address1_head' => $this->_request->getParam('add_address1_head') ? $this->_request->getParam('add_address1_head') : NULL,
          'add_address1_data' => $this->_request->getParam('add_address1_data') ? $this->_request->getParam('add_address1_data') : NULL,
          'add_address2_data' => $this->_request->getParam('add_address2_data') ? $this->_request->getParam('add_address2_data') : NULL,
          'id_accounts' => $this->_request->getParam('id_accounts'),
          'show_logo' => $this->_request->getParam('show_logo', 'y'),
          'id_seller' => $this->_request->getParam('seller'),
        ];

        //existuje faktura
        if ($invoiceId) {
          $invoice = new Invoice_Invoice($invoiceId);
          $invoice->update($data);
        }
        else {
          $data += [
            'id_orders' => (int) $this->_request->getParam('order'),
            'id_users' => $authNamespace->user_id,
            'inserted' => new Zend_Db_Expr('NOW()'),
          ];
          $invoice = Invoice_Invoice::create($data);
          $invoiceId = $invoice->getId();
        }

        switch ($invoice->getType()) {
          case Table_Invoices::TYPE_OFFER: $this->_flashMessenger->addMessage('Nabídka byla uložena.'); break;
          case Table_Invoices::TYPE_PROFORM: $this->_flashMessenger->addMessage('Proforma byla uložena.'); break;
          case Table_Invoices::TYPE_REGULAR: $this->_flashMessenger->addMessage('Faktura byla uložena.'); break;
          case Table_Invoices::TYPE_CREDIT: $this->_flashMessenger->addMessage('Dobropis byl uložen.'); break;
        }

        $this->redirect($this->view->url([
          'controller' => 'invoice',
          'id' => $invoiceId,
          'order' => NULL,
        ]));
        return;
      }

      $messages[] = 'Číslo faktury ' . $no . ' jiz existuje.';
    }

    // neni POST

    $tInvoice = new Table_Invoices();
    $invoiceId = $this->_request->getParam('id', NULL);

    // zalozeni nové z objednávky
    if (!$invoiceId) {
      $type = $this->_request->getParam('type');
      $orderId = $this->_request->getParam('order', NULL);
      if (!$orderId) {
        $this->redirect('/orders');
      }
      $invoiceId = $tInvoice->getInvoiceIdByOrder($orderId, $type);
      try {
        if ($invoiceId) {
          $invoice = new Invoice_Invoice($invoiceId);
        }
        else {
          $order = new Orders_Order($orderId);
          if (!$order->hasItems()) {
            $this->_flashMessenger->addMessage('Nelze vystavit fakturu, objednávka nemá žádné položky.');
            $this->redirect('/orders/detail/id/' . $orderId);
          }
          $invoice = new Invoice_Invoice();
          // nastavení typu faktury z objednávky
          $invoiceId = $invoice->setOrder($orderId, $type);
          if ($order->isService() && $type == Table_Invoices::TYPE_REGULAR) {
            $order->processExpedition();
            $this->_flashMessenger->addMessage('Objednávka služeb byla vyexpedována.');
          }
          $this->redirect('/invoice/prepare/id/' . $invoiceId);
        }
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
        $this->redirect('/orders/detail/id/' . $orderId);
      }
    }

    // editace existujíci
    else {
      try {
        $invoice = new Invoice_Invoice($invoiceId);
        $orderId = $invoice->getOrderId();
      }
      catch (Parts_Exceptions_NoPricelist $e) {
        $this->_flashMessenger->addMessage($e->getMessage() . ' Ceník zvolte v nastavení objednávky.');
        $this->redirect('/invoice');
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
        $this->redirect('/invoice');
      }
    }

    // upozorneni na vystaveni pokladniho dokladu
    $this->view->noticeCashReceipt = ($invoice->isRegular() && $invoice->getOrder()
        ->getPaymentMethod() == Table_Orders::PM_CASH);
    // upozorneni na vystaveni prepravniho dokladu
    $this->view->noticeTAXLiability = $invoice->isTransferredTaxLiability();

    $order = new Orders_Order($orderId);

    // kontrola uzamčení
    if ($invoice->isLocked()) {
      $this->_flashMessenger->addMessage('Fakturu nelze upravovat, již je v účetní evidenci.');
      $this->redirect('/invoice');
    }

    // vyplneni nastavení faktury --------------------------------------------
    if ($this->_request->isPost()) {
      $form->populate($this->_request->getPost());
    }
    else {
      $form->mapFromRow($invoice->getRow());
      $curr_invoice = $invoice->getCurrency();
      $curr_order = $invoice->getOrder()->getExpectedPaymentCurrency();
      if ($curr_invoice != $curr_order) {
        $messages[] = 'Pozor: Měna v objednávce (' . $curr_order . ') neodpovídá měně na faktuře (' . $curr_invoice . ').';
      }
    }

    // defaultni pole
    if ($invoice->getRegion() === Table_Addresses::REGION_NON_EU) {
      // vycisleni DPH
      $form->removeElement('show_vat_summary');
      $elem = new Zend_Form_Element_Hidden('show_vat_summary');
      $elem->setValue('n');
      $form->addElement($elem);
      // maskovani nulovych cen
      $form->removeElement('mask_zero_prices');
      $elem = new Zend_Form_Element_Hidden('mask_zero_prices');
      $elem->setValue('y');
      $form->addElement($elem);
    }
    if ($invoice->getCountry() === Table_Addresses::COUNTRY_CZ) {
      // vycisleni DPH
      $form->removeElement('show_vat_summary');
      $elem = new Zend_Form_Element_Hidden('show_vat_summary');
      $elem->setValue('y');
      $form->addElement($elem);
    }

    if ($order->isService()) {
      $form->removeElement('packaging');
      $form->removeElement('weight');
      $form->removeElement('allow_delivery_addr');
      $form->removeElement('delivery_text');
    }
    else {
      $packaging = $invoice->getPackaging();
      $form->getElement('packaging')->setValue($packaging['packages']);
      $form->getElement('weight')->setValue($packaging['weight']);
    }

    if ($invoice->isProform() || $invoice->isOffer()) {
      $form->removeElement('duzp');
    }

    $this->view->invoiceId = $invoiceId;
    $this->view->invoiceType = $invoice->getType();
    $this->view->items = $invoice->getCustomItems(NULL);

    $this->view->form = $form;
    $this->view->orderId = $invoice->getOrderId();

    if ($order = $invoice->getOrder()) {
      $this->view->orderCurrency = $order->getExpectedPaymentCurrency();
      $this->view->orderIsService = $order->isService();
      $this->view->paymentMethod = $order->getPaymentMethod();
      if ($customer = $order->getCustomer(TRUE)) {
        $this->view->verifiedVat = $customer->isVerifiedVatNo();
      }
    }

    $this->view->isRegular = $invoice->isRegular();
    $this->view->versions = $invoice->getVersions();
    $this->view->invoiceIsDeletable = $invoice->isDeletable();

    $this->view->reduction = $invoice->getReduction();
    $this->view->vat = $invoice->getVat();

    // dodatecne polozky -----------------------------------------------------
    $customItemForm = new Invoice_CustomItemForm();
    $customItemForm->populate(['id_invoice' => $invoiceId]);
    $this->view->customItemForm = $this->view->partial('invoice/custom-item.phtml', ['form' => $customItemForm]);

    // produktove polozky faktury --------------------------------------------
    if (!$order->isService()) {
      try {
        $itemLines = $invoice->getItemLines();
      }
      catch (Parts_Exceptions_NoPricelist $e) {
        $url = $this->view->url([
          'controller' => 'pricelists',
          'action' => 'assign',
          'customer' => $order->getCustomer()->id,
          'order' => $orderId,
        ]);
        $this->_flashMessenger->addMessage('Zákazník nemá přiřazený platný ceník. ' . '<a class="btn btn-primary btn-small" href="' . $url . '">' . '<i class="icon icon-white icon-list"></i> Přiřadit</a>');
        $this->redirect('orders/detail/id/' . $orderId);
      }
      $itemLinesForm = new Invoice_ItemLinesForm();
      $itemLinesForm->setInvoiceId($invoice->getId());
      $itemLinesForm->fill($itemLines);
      $this->view->itemLinesForm = $itemLinesForm;
      $this->view->itemLines = $itemLines;
    }

    $this->view->acl = Zend_Registry::get('acl');

    // odpocet uhrad zaloh
    try {
      $this->view->deductForm = new Invoice_DeductForm(['id_invoice' => $invoiceId]);
    }
    catch (Invoice_Exceptions_NoPayments $e) {
      $this->view->deductForm = NULL;
    }
    catch (Invoice_Exceptions_BadType $e) {
      $this->view->deductForm = FALSE;
    }

    $this->view->message = implode('<br>', $messages);

    switch ($this->view->invoiceType) {
      case Table_Invoices::TYPE_OFFER: $this->view->title = 'Nastavení nabídky'; break;
      case Table_Invoices::TYPE_PROFORM: $this->view->title = 'Nastavení proforma faktury'; break;
      case Table_Invoices::TYPE_REGULAR: $this->view->title = 'Nastavení faktury'; break;
      case Table_Invoices::TYPE_CREDIT: $this->view->title = 'Nastavení dobropisu'; break;
    }

  }

  /**
   * akce generovani faktury ve formatu PDF
	 */
	public function generateAction():void
  {

		$this->_helper->layout()->disableLayout();
		$id = $this->_request->getParam('id');
		if (is_null($id)) {
			$this->redirect('/invoice');
		}
    try {
      $invoice = new Invoice_Invoice($id);

      // kontrola dopravy
      if ($invoice->isRegular() && !$invoice->orderIsService() && !$invoice->isShippingCustomItem()) {
        $this->_flashMessenger->addMessage('Nelze generovat fakturu, chybí přepravní náklady.');
        $this->redirect($this->view->url(array('action' => 'prepare', 'id' => $invoice->getId())));
      }

      $order = $invoice->getOrder();
      if (! $order instanceof Orders_Order) {
        $message = sprintf('Invoice %d has no order.', $invoice->getId());
        throw new RuntimeException($message);
      }

      $rate = $invoice->getRate();
      $currency = $invoice->getCurrency();
      $price = $invoice->getInvoicedPrice();

      if ($invoice->getType() === Table_Invoices::TYPE_REGULAR && $order->getPaymentMethod() === Table_Orders::PM_CASH) {

        $tEmail = new Table_Emails();
        $tEmail->sendEmail(Table_Emails::EMAIL_INVOICE_IN_CASH_NOTICE, $invoice);

        // Pokud k regular fakture neexistuji uhrady a platba je v hotovosti, vytvorime ihned uhradu.
        if (!$invoice->getPayments()) {
          $issue = $invoice->getIssueDate();
          $data = array(
            'id_invoice' => $invoice->getId(),
            'id_wh_type' => $invoice->getWHType(),
            'amount' => $currency == Table_Invoices::CURRENCY_EUR ? round($price, 2) : round($price, 0),
            'currency' => $currency,
            'rate' => $rate,
            'date' => $issue ? $issue : new Zend_Db_Expr('now()'),
          );
          $payment = new Invoice_Payment($data);
          $payment->save();
          $tEmail->sendEmail(Table_Emails::EMAIL_PAYMENT_NOTICE_ADD, $payment);
        }
      }

      $invoice->getCustomer()->recalculateBudgets();

      $expected = $invoice->getInvoicedPrice(TRUE, TRUE);
      if (in_array($invoice->getType(), [Table_Invoices::TYPE_PROFORM, Table_Invoices::TYPE_REGULAR], true)) {
        $order->setExpectedPayment($expected, $rate, $currency);
        $order->setExpectedPaymentRate($rate);
      }

      if ($invoice->isProform()) {
        $order->setExpectedPaymentDeposit($price, $rate, $currency);
      }

      $reduction = (int) $this->_request->getParam('reduction', 0);

      $this->view->pdf = $invoice->generatePdf($reduction);

      if ($version = $invoice->getLastVersion()) {
        $path_chunks = explode('/', $version['filename']);
        $this->view->filename = array_pop($path_chunks);
      }
      else {
        $this->view->filename = NULL;
      }
    }

		catch (Parts_Exceptions_NoPricelist $e) {
      $this->_flashMessenger->addMessage($e->getMessage() .
        ' Ceník zvolte v nastavení objednávky.');
      Utils::logger()->warn($e->getMessage());
      $this->redirect('/invoice');
		}

    catch (Invoice_Exceptions_ZeroProductPrice $e) {
      $this->_flashMessenger->addMessage($e->getMessage());
      Utils::logger()->warn($e->getMessage());
      $this->redirect($this->view->url(array('action' => 'prepare')));
    }

		catch (Exception|RuntimeException $e) {
			$this->getHelper('flashMessenger')->addMessage($e->getMessage());
			$this->redirect('/invoice');
		}
	}

    public function downloadAction(): void {
      $this->_helper->layout()->disableLayout();
      $id = $this->_request->getParam('id');

      try {
        if (is_null($id)) {
          throw new Exception('Nebylo zadáno ID faktury.');
        }

        $invoice = new Invoice_Invoice($id);
        $order = $invoice->getOrder();
        if ($order && !$order->isClosed() && Utils::isDisallowed('invoice/download-not-closed')) {
          throw new Exception('Lze stáhnout faktury pouze k expedovaným objednávkám.');
        }

        if (is_null($this->_request->getParam('vid'))) {
          if (!$version = $invoice->getLastVersion($order && $order->isClosed())) {
            throw new Invoice_Exceptions_NotValid('Faktura č. ' . $invoice->getNo() . ' není aktuální, je potřeba vygenerovat novou verzi.');
          }
        } else {
          $version = $invoice->getVersion($this->_request->getParam('vid'));
        }

        if (is_null($version)) {
          throw new Exception('Faktura č. ' . $invoice->getNo() . ' není vygenerovaná.');
        }

        if (Utils::hasCurrentUserRole('accountant_pipes')) {
          $invoice->register();
        }

        $pdf = Meduse_Pdf_Invoice::load($version['filename']);
        $exploded = explode('/', $version['filename']);
        $this->view->filename = array_pop($exploded);
        $this->view->pdf = $pdf;
        $this->render('generate');
      }

      catch (Exception $e) {
        if (isset($version['filename']) && $e instanceof Zend_Pdf_Exception) {
          $message = 'Soubor ' . $version['filename'] . ' nelze otevřít pro čtení.';
        }
        elseif($e instanceof Invoice_Exceptions_NotValid) {
          $url = $this->view->url(['controller' => 'invoice', 'action' => 'prepare', 'id' => $id]);
          $message = $e->getMessage() . '<br><a class="btn btn-small" href="' . $url . '">' .
            '<i class="icon icon-edit"></i> ' .
            'Přejít na nastavení a generovat...</a>';
        }
        else {
          $message = $e->getMessage();
        }
        Utils::logger()->warn($message);
        $this->_flashMessenger->addMessage($message);
        $this->redirect($_SERVER['HTTP_REFERER']);
      }
    }

  public function addCustomItemAction() {
    $this->_helper->layout()->disableLayout();
    if ($this->getRequest()->isPost()) {
      $form = new Invoice_CustomItemForm();
      $form->populate($this->getRequest()->getParams());
      $data = $form->getValues(TRUE);
      $invoice = new Invoice_Invoice($data['id_invoice']);
      $id = $invoice->setCustomItem($data);
      if ($id === FALSE) {
        $this->_flashMessenger->addMessage('Volitelnou položku se nepodařilo uložit.');
      }
      elseif (isset($data['id_item'])) {
        $this->_flashMessenger->addMessage('Volitelná položka byla aktualizována.');
      }
      else {
        $this->_flashMessenger->addMessage('Volitelná položka byla přidána.');
      }
      $invoice->getInvoicedPrice();
      $this->redirect('/invoice/prepare/id/' . $data['id_invoice']);
    }
    else {
      $this->redirect('/invoice');
    }
  }

    public function jsonGetCustomItemAction() {
        $item = array();
        $id = $this->getRequest()->getParam('id', null);
        if (!is_null($id)) {
            $tItems = new Table_InvoiceCustomItems();
            $item = $tItems->find($id)->current();
        }
        if ($item) {
          $item = $item->toArray();
          if ($item['vat_included'] === 'y') {
            $invoice = new Invoice_Invoice($item['id_invoice']);
            $item['price'] = round((float) $item['price'] * (1 + $invoice->getVat()), 2);
          }
          else {
            $item['price'] = round((float) $item['price'], 2);
          }
        }
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->json($item);
    }

    public function removeCustomItemAction() {
        $this->_helper->layout()->disableLayout();
        $invoiceId = $this->getRequest()->getParam('invoice');
        $id = $this->getRequest()->getParam('id', null);
        if (!is_null($id)) {
            $tItems = new Table_InvoiceCustomItems();
            $tItems->delete('id = ' . $id);
        }
        $invoice = new Invoice_Invoice($invoiceId);
        $invoice->recalculateInvoicedPrice();
        $this->_redirect('/invoice/prepare/id/' . $invoiceId);
    }


    public function customTextsAction() {
        $this->_helper->layout->setLayout('bootstrap-basic');
        $this->view->texts = Table_InvoiceCustomTexts::getAllText();
        $this->view->invoiceTypes = Table_InvoiceCustomTexts::getInvoiceTypes();
    }

    public function customTextsEditAction() {
        $this->_helper->layout->setLayout('bootstrap-basic');
        $data = array();
        $form = new Invoice_CustomTextsForm();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $data = $form->getValidValues($this->_request->getPost());
                $tTexts = new Table_InvoiceCustomTexts();
                if (isset($data['id']) && is_numeric($data['id'])) {
                    if ($tTexts->update($data, 'id = ' . $data['id'])) {
                        $this->_flashMessenger->addMessage('Text byl upraven.');
                    }
                }
                else {
                    unset($data['id']);
                    if ($tTexts->insert($data)) {
                        $this->_flashMessenger->addMessage('Text byl uložen.');
                    }
                }
                $this->redirect('/invoice/custom-texts');
            }
            else {
                $form->populate($data);
            }
        }
        else {
            $id = $this->getRequest()->getParam('id', null);
            if (is_null($id)) {
                $form->setLegend('Vložení nového uživatelského textu');
            }
            else {
                $form->setLegend('Editace uživatelského textu');
                $tTexts = new Table_InvoiceCustomTexts();
                $data = $tTexts->find($id)->current()->toArray();
                $form->populate($data);
            }
        }
        $this->view->form = $form;
    }

    public function customTextsRemoveAction() {
        if ($this->getRequest()->isPost()) {
            $id = (int) $this->getRequest()->getParam('id');
            if ($id) {
                $tTexts = new Table_InvoiceCustomTexts();
                if ($tTexts->delete('id = ' . $id)) {
                    $this->_flashMessenger->addMessage('Text byl vymazán');
                }
            }
        }
        $this->redirect('/invoice/custom-texts');
    }


  public function customTextsGetJsonAction() {

    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $tTexts = new Table_InvoiceCustomTexts();

    $id = $this->_request->getParam('id');

    if (is_null($id)) {

      //získání parametrů
      $invoiceId = $this->_request->getParam('invoice');
      $language = $this->_request->getParam('language');

      $invoice = new Invoice_Invoice($invoiceId);
      $order = $invoice->getOrder();
      $customer = $invoice->getCustomer();
      switch ($invoice->getType()) {
        case Table_Invoices::TYPE_PROFORM:
          $typeInvoice = Table_InvoiceCustomTexts::INVOICE_TYPE_PROFORMA;
          break;
        case Table_Invoices::TYPE_REGULAR:
          $typeInvoice = Table_InvoiceCustomTexts::INVOICE_TYPE_REGULAR;
          break;
        default:
          $typeInvoice = Table_InvoiceCustomTexts::INVOICE_TYPE_ALL;
      }
      $typeCustomer = $customer->getType();
      $tAddress = new Table_Addresses();
      $address = $tAddress->getAddress($order->_address);
      $region = $address['region'];

      //texty odpovídající typu
      $select = $tTexts->select()->setIntegrityCheck(FALSE);
      $select->where('id_wh_type = ?', 1);
      $select->where('invoice_type IN (?)', [$typeInvoice, Table_InvoiceCustomTexts::INVOICE_TYPE_ALL]);
      $select->where('type = ?', $typeCustomer);
      $select->where('region = ?', $region);
      $select->where('language = ?', $language);
      $items_p = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();

      //texty neodpovídající typu
      $select = $tTexts->select()->setIntegrityCheck(FALSE);
      $select->where('id_wh_type = ?', 1);
      $select->where('type != ?', $typeCustomer);
      $select->where('invoice_type NOT IN (?)', [$typeInvoice, Table_InvoiceCustomTexts::INVOICE_TYPE_ALL]);
      $select->orWhere('region != ?', $region);
      $select->orWhere('language != ?', $language);
      $items_n = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();

      $items = array_merge($items_p, [['id' => '0']], $items_n);
    }
    else {
      $items = $tTexts->find($id)->toArray();
    }
    $this->_helper->json($items);
  }

  public function saveItemTextsAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      if ($this->getRequest()->isPost()) {
        $data = $this->getRequest()->getParams();
        $invoiceId = $data['invoice_id'];
        unset($data['controller']);
        unset($data['action']);
        unset($data['module']);
        unset($data['invoice_id']);
        $invoice = new Invoice_Invoice($invoiceId);

        $texts = array();
        $prices = array();

        foreach($data as $name => $value) {
          list($item, $idx) = explode('_', $name, 2);
          if ($item === 'item') {
            $index = 'type_' . $idx;
            if (isset($data[$index]) && $data[$index] == 2) {
              continue;
            }
            $int = (int) $idx;
            if ($int > 0) {
              if (isset($data['item_idx_' . $idx])) {
                $texts[$data['item_idx_' . $idx]][] = $value;
              }
            }
          }
          elseif ($item == 'price') {
            if (isset($data['type_' . $idx]) && $data['type_' . $idx] == 2) {
              continue;
            }
            $int = (int) $idx;
            if ($int > 0) {
              if (isset($data['item_idx_' . $idx])) {
                if (!isset($prices[$data['item_idx_' . $idx]])) {
                  $prices[$data['item_idx_' . $idx]] = [];
                }
                $prices[$data['item_idx_' . $idx]]['price'] = Meduse_Currency::parse($value);
              }
            }
          }
          elseif ($item == 'vat') {
            if (isset($data['type_' . $idx]) && $data['type_' . $idx] == 2) {
              continue;
            }
            $int = (int) $idx;
            if ($int > 0) {
              if (isset($data['item_idx_' . $idx])) {
                if (!isset($prices[$data['item_idx_' . $idx]])) {
                  $prices[$data['item_idx_' . $idx]] = [];
                }
                $prices[$data['item_idx_' . $idx]]['vat_included'] = $value == 'y';
              }
            }
          }
        }

        $invoice->saveItemTexts($texts);
        $invoice->saveItemPrices($prices);
        $this->_flashMessenger->addMessage('Produktové položky byly uloženy.');
        $this->redirect('/invoice/prepare/id/' . $invoiceId);
      }
      $this->redirect('/invoice');
    }

    public function resetPricesAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $id = $this->_request->getParam('id');
      if ($id) {
        $invoice = new Invoice_Invoice($id);
        $invoice->resetItemsPrices();
        $this->_flashMessenger->addMessage('Ceny byly resetovány dle ceníku');
        $this->_redirect('invoice/prepare/id/' . $id);
      }
      $this->_flashMessenger->addMessage('Nebylo zadáno ID faktury.');
      $this->_redirect('invoice');
    }

    public function resetTextsAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $id = $this->_request->getParam('id');
      if ($id) {
        $invoice = new Invoice_Invoice($id);
        $invoice->resetItemsTexts();
        $this->_flashMessenger->addMessage('Texty položek byly nastaveny na výchozí hodnoty.');
        $this->_redirect('invoice/prepare/id/' . $id);
      }
      $this->_flashMessenger->addMessage('Nebylo zadáno ID faktury.');
      $this->_redirect('invoice');
    }

    public function ajaxCompanyAction() {
      $table = new Table_Customers();
      $wh_type = $this->_request->getParam('wh');
      if (empty($wh_type)) {$wh_type = 1;}
      $results = $table->getPossibleCompanyNamesInvoice($this->_request->getParam('term'), $wh_type);
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $this->_helper->json($results);
    }

    /**
     * Získává informaci o tom, v jaké měně je účet s daným ID (kvůli kontrole ve faktuře).
     *
     * Pokud informace neexistuje, vrací NULL.
     */
    public function jsonGetAccountCurrencyAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $tAccounts = new Table_Accounts();
      $select = $tAccounts->find((int)$this->_request->getParam('id'));

      $currency = NULL;

      if($select->current()) {
        $row = $select->current()->toArray();
        $currency = $row['currency'];
      }

      $this->_helper->json($currency);


    }

    public function createCreditNoteAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      // otestování zadání ID faktury
      if($invoiceID = $this->_request->getParam('id')) {
        // otestování, jestli vstupní faktura existuje
        try {
          $invoice = new Invoice_Invoice($invoiceID);

          // přesměrování na existující dobropis
          $tInvoice = new Table_Invoices();
          $existing_credit = $tInvoice->select()
                                      ->where('id_orders = ?', $invoice->getOrderId())
                                      ->where('type = ?', Table_Invoices::TYPE_CREDIT)
                                      ->query()->fetch();

          if ($existing_credit) {
            $this->_flashMessenger->addMessage('Dobropis již existuje, bylo na ni jen přesměrováno');
            $this->_redirect('/invoice/prepare/id/' . $existing_credit['id']);
          }

          // generováné nového dobropisu
          try {
            $credit_note = $invoice->generateCreditNote();
            $this->_redirect('/invoice/prepare/id/' . $credit_note->getId());
          } catch (Exception $e) {
            $this->_flashMessenger->addMessage($e->getMessage());
            $this->_redirect('/invoice/prepare/id/' . $invoiceID);
          }
        } catch (Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
          $this->_redirect('/invoice');
        }


      } else {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID vstupní faktury');
        $this->_redirect('/invoice');
      }
    }

    public function deregisterAction() {
      $id = $this->getParam('id');
      if ($id) {
        $invoice = new Invoice_Invoice($id);
        $invoice->deregister();
        $this->_flashMessenger->addMessage('Faktura č. ' . $invoice->getNo() . ' byla znovu odemknuta pro úpravy.');
      }
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $this->redirect($this->view->url(array('action' => NULL, 'id' => NULL)));
    }

    public function paymentsAction() {
      $params = array(
        'sort' => $this->getParam('sort'),
        'sorder' => $this->getParam('sorder'),
        'page' => $this->getParam('page'),
      );
      $payments = Invoice_Payment::getPaymentsGrid(Parts_Part::WH_TYPE_PIPES, $params);
      $this->view->payments = $payments->render();
      $this->view->title = 'Přijaté úhrady – přehled';
    }

   public function paymentAddAction() {
      $title = 'Nová úhrada';
      if ($this->_request->isPost()) {
        $back = $this->_request->getParam('back');
        $redirect = is_null($back) ? $this->view->url(array('action' => 'payments', 'invoice' => NULL)) : base64_decode($back);
        $data = array(
          'id_invoice' => $this->getParam('id_invoice'),
          'amount' => $this->getParam('amount'),
          'currency' => $this->getParam('currency'),
          'rate' => $this->getParam('rate'),
          'date' => Meduse_Date::formToDb($this->getParam('date')),
          'no' => $this->getParam('no'),
          'method' => $this->getParam('method'),
          'note' => $this->getParam('note'),
          'id_wh_type' => $this->getParam('wh'),
        );
        $payment = new Invoice_Payment($data);
        try {
          $payment->save();
          $tEmail = new Table_Emails();
          $tEmail->sendEmail(Table_Emails::EMAIL_PAYMENT_NOTICE_ADD, $payment);
        }
        catch(Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
        }
        $this->redirect($redirect);
      }

      $form = new Invoice_PaymentForm(array('id_invoice' => $this->getParam('invoice')));
      $form->setLegend($title);
      $this->view->title = $title;
      $this->view->form = $form;
      $this->render('payment-form');
    }

   public function paymentEditAction() {
      $title = 'Změna úhrady';
      $form = new Invoice_PaymentForm(array('id_payment' => $this->getParam('id')));
      $form->setLegend($title);
      if ($this->_request->isPost()) {
        $back = $this->_request->getParam('back');
        $redirect = is_null($back) ? $this->view->url(array('action' => 'payments', 'invoice' => NULL)) : base64_decode($back);
        $data = array(
          'id' => $this->getParam('id_payment'),
          'id_invoice' => $this->getParam('id_invoice'),
          'amount' => $this->getParam('amount'),
          'currency' => $this->getParam('currency'),
          'rate' => $this->getParam('rate'),
          'date' => Meduse_Date::formToDb($this->getParam('date')),
          'no' => $this->getParam('no'),
          'method' => $this->getParam('method'),
          'note' => $this->getParam('note'),
          'id_wh_type' => $this->getParam('wh'),
        );
        $payment = new Invoice_Payment($data['id']);
        $payment->setData($data);
        try {
          $payment->save();
          $tEmail = new Table_Emails();
          $tEmail->sendEmail(Table_Emails::EMAIL_PAYMENT_NOTICE_CHANGE, $payment);
        }
        catch(Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
        }
        $this->redirect($redirect);
      }
      $this->view->title = $title;
      $this->view->form = $form;
      $this->render('payment-form');
    }

    public function paymentGetRecipeAction() {
      $this->_helper->layout()->disableLayout();
      $payment = new Invoice_Payment($this->getParam('id'));
      $pdf = $payment->getRecipe();
      $this->view->pdf = $pdf;
      $this->view->filename = 'Daňový doklad k přijaté platbě ' . $payment->getNo();
      $this->render('generate');
    }

    public function paymentRemoveAction() {
      $id = $this->getParam('id');
      $back = $this->_request->getParam('back');
      $redirect = is_null($back) ? $this->view->url(array('action' => 'payments', 'id' => NULL)) : base64_decode($back);
      $payment = new Invoice_Payment($id);
      $order = $payment->getOrder();
      $removed = FALSE;
      $message = '';
      try {
        $removed = Invoice_Payment::removePayment($id);
      }
      catch (Zend_Db_Statement_Exception $e) {
        $message = 'Úhrada je použitá jako odpočet ve faktuře.';
      }
      if ($removed) {
        $order->checkPayments();
        $tEmail = new Table_Emails();
        $tEmail->sendEmail(Table_Emails::EMAIL_PAYMENT_NOTICE_REMOVE, $payment);
        $this->_flashMessenger->addMessage('Úhrada byla odstraněna');
      }
      else {
        if ($message) {
          $message = 'Úhradu se nepodařilo odstranit. ' . $message;
        }
        $this->_flashMessenger->addMessage($message);
      }
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $this->redirect($redirect);
    }

    public function deleteAction(): void
    {
      if ($id = $this->_request->getParam('id')) {
        try {

          $invoice = new Invoice_Invoice($id);
          $type = Table_Invoices::$typeToString[$invoice->getType()];
          $no = $invoice->getNo();
          $no = $no ? ' ' . $no : '';

          if ($this->_request->isPost()) {
            $invoice->delete();
            $invoice->getCustomer()->recalculateBudgets();
            $this->_flashMessenger->addMessage(sprintf('Dokument %s%s byl odstraněn ze systému.', $type, $no));
            $this->redirect($this->view->url(array('action' => 'index', 'id' => NULL)));
          }
          else {
            $form = new Invoice_DeleteForm();
            $form->setLegend(sprintf('Opravdu smazat dokument %s%s?', $type, $no));
            $form->id->setValue($id);
            $this->view->form = $form;
            $this->view->id = $id;
            $this->view->title = sprintf('Odstranění dokumentu %s%s', $type, $no);
          }
        }
          catch (Exception $e) {
          $this->_flashMessenger->addMessage(sprintf('Dokument %s%s se nepodařilo odstranit: ' . $e->getMessage(), $type, $no));
          Utils::logger()->warn($e->getMessage());
          $this->redirect($this->view->url(array('action' => 'prepare')));
         }
      }
      else {
        $this->_flashMessenger->addMessage('Nebyl zadaný identifikátor.');
        $this->redirect($this->view->url(array('action' => 'index', 'id' => NULL)));
      }
    }

    public function ajaxSaveDeductAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $invoiceId = $this->_request->getParam('id');
      $paymentId = $this->_request->getParam('id_payment');
      $deductionId = $this->_request->getParam('id_deduction');
      $amount = $this->_request->getParam('amount');
      $payment = new Invoice_Payment($paymentId);
      try {
        $deductionId = $payment->setDeductedInvoice($invoiceId, $deductionId, $amount);
        $data = array('status' => TRUE, 'message' => 'Uloženo.', 'id_deduction' => $deductionId ? $deductionId : '');
      }
      catch(Exception $e) {
        $data = array('status' => FALSE, 'message' => $e->getMessage());
      }
      $this->_helper->json($data);
    }

  public function loadShipmentFromOrderAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    $invoiceId = $this->_request->getParam('id');
    try {
      if (!$invoiceId) {
        throw new Exception('Neni zadano ID faktury.');
      }
      $invoice = new Invoice_Invoice($invoiceId);
      $invoice->loadShipmentFromOrder();
    }
    catch (Exception $e) {
      $this->_flashMessenger->addMessage($e->getMessage());
    }
    $this->redirect('/invoice/prepare/id/' . $invoiceId);
  }

  public function paymentMoveToAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $paymentId = $this->_request->getParam('id');
      $invoiceId = $this->_request->getParam('to');
      $back = $this->_request->getParam('back');

      $process = TRUE;

      if (!$paymentId) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID úhrady.');
        $process = FALSE;
      }

      if (!$invoiceId) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID faktury.');
        $process = FALSE;
      }

      if (!$payment = new Invoice_Payment($paymentId)) {
        $this->_flashMessenger->addMessage("Úhrada s ID = $paymentId nebyla nalezena");
        $process = FALSE;
      }

      if ($process) {
        try {
          if ($payment->moveTo($invoiceId)) {
            $payment->getOrder()->checkPayments();
            $this->_flashMessenger->addMessage('Úhrada byla přesunuta.');
          }
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
        }
      }

      $this->redirect($back ? base64_decode($back) : $this->view->url(['controller' => 'invoice', 'action' => 'index']));
  }
}