<?php

class ReportsController extends Meduse_Controller_Action {

  protected $db = NULL;
  
  public function init() {
    $this->db = Zend_Registry::get('db');
    $this->_helper->layout->setLayout('bootstrap-basic');
  }
  
  public function salesAction() {

    $reports = new Reports();

    $this->view->post = FALSE;

    $this->view->title = 'Reporty: Prodeje';
    $this->view->form = $reports->getSalesForm($this->_request->getParams());

    if ($this->_request->isPost()) {

      $options = array(
        'from' => $this->_request->getParam('from'),
        'to' => $this->_request->getParam('to'),
        'country' => $this->_request->getParam('country'),
        'customer' => $this->_request->getParam('customer'),
        'type' => $this->_request->getParam('type', 'ALL'),
      );

      $this->view->types = [
        Table_Customers::TYPE_B2B,
        Table_Customers::TYPE_B2C,
        Table_Customers::TYPE_ALL
      ];

      $data_sets = $reports->getPipesSales($options);
      $data_access = $reports->getAccessoriesSales($options);
      $data_services = $reports->getServicesSales($options);

      $this->view->ct_pipes = $data_sets;
      $this->view->ct_access = $data_access;
      $this->view->services = $data_services;

      $totals = [
        Table_Customers::TYPE_B2B => $data_sets['sum']['total'][Table_Customers::TYPE_B2B]['price'] + $data_access['sum'][Table_Customers::TYPE_B2B]['price'] + $data_services['sum'][Table_Customers::TYPE_B2B]['price'],
        Table_Customers::TYPE_B2C => $data_sets['sum']['total'][Table_Customers::TYPE_B2C]['price'] + $data_access['sum'][Table_Customers::TYPE_B2C]['price'] + $data_services['sum'][Table_Customers::TYPE_B2C]['price'],
        Table_Customers::TYPE_ALL => $data_sets['sum']['total'][Table_Customers::TYPE_ALL]['price'] + $data_access['sum'][Table_Customers::TYPE_ALL]['price'] + $data_services['sum'][Table_Customers::TYPE_ALL]['price'],
      ];
      $this->view->totals = $totals;

      $this->view->chart1_json = json_encode([
        [
          'price' => round($data_sets['sum']['total'][Table_Customers::TYPE_ALL]['price'] / 1000),
          'label' => 'Sety dýmek'
        ],
        [
          'price' => round($data_access['sum'][Table_Customers::TYPE_ALL]['price'] / 1000),
          'label' => 'Příslušenství'
          ],
        [
          'price' => round($data_services['sum'][Table_Customers::TYPE_ALL]['price'] / 1000),
          'label' => 'Služby'
        ],
      ]);
      $this->view->chart2_json = json_encode([
        [
          Table_Customers::TYPE_B2B => round($data_sets['sum']['total'][Table_Customers::TYPE_B2B]['price'] / 1000),
          Table_Customers::TYPE_B2C => round($data_sets['sum']['total'][Table_Customers::TYPE_B2C]['price'] / 1000),
          'label' => 'Sety dýmek'
        ],
        [
          Table_Customers::TYPE_B2B => round($data_access['sum'][Table_Customers::TYPE_B2B]['price'] / 1000),
          Table_Customers::TYPE_B2C => round($data_access['sum'][Table_Customers::TYPE_B2C]['price'] / 1000),
          'label' => 'Příslušenství'
          ],
        [
          Table_Customers::TYPE_B2B => round($data_services['sum'][Table_Customers::TYPE_B2B]['price'] / 1000),
          Table_Customers::TYPE_B2C => round($data_services['sum'][Table_Customers::TYPE_B2C]['price'] / 1000),
          'label' => 'Služby'
        ],
      ]);
      $this->view->chart3_json = json_encode([
        [
          'price' => round($data_sets['sum']['total'][Table_Customers::TYPE_B2B]['price'] / 1000),
          'label' => Table_Customers::TYPE_B2B
        ],
        [
          'price' => round($data_sets['sum']['total'][Table_Customers::TYPE_B2C]['price'] / 1000),
          'label' => Table_Customers::TYPE_B2C
        ],
      ]);

      $this->view->post = TRUE;
    }
  }
}
