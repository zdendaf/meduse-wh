<?php

class CustomersController extends Meduse_Controller_Action
{

	public function init() {
		$this->_helper->layout->setLayout('bootstrap-basic');
		$this->view->title = "Zákazníci";
	}

	//prehled

  /**
   * @throws Zend_Form_Exception
   * @throws Zend_Exception
   * @throws Exception
   */
  public function indexAction(): void
  {
    // set title
    $this->view->title ??= 'Zákazníci';

    $form = new Customers_SearchForm();
    $form->isValid($this->_request->getParams());
    $form->setLegend('Hledat zákazníka');
    $this->view->filter ??= $form;

    $table = new Table_Customers();

    $select = $table->getDataList();

    $columns = [
      'type' => [
        'header' => _('Typ'),
        'css_style' => 'text-align: left',
        'sorting' => TRUE,
      ],
      'id_pricelists_ctg' => [
        'header' => 'Cen.kat.',
        'css_style' => 'text-align: center',
        'sorting' => TRUE,
      ],
      'company' => [
        'header' => _('Firma'),
        'css_style' => 'text-align: left',
        'sorting' => TRUE,
        'renderer' => 'url',
        'url' => $this->view->url([
          'module' => 'default',
          'controller' => 'customers',
          'action' => 'detail',
          'id' => '{id}',
        ], NULL, TRUE),
      ],
      'first_name' => [
        'header' => _('Jméno'),
        'css_style' => 'text-align: left',
        'sorting' => TRUE,
        'renderer' => 'url',
        'url' => $this->view->url([
          'module' => 'default',
          'controller' => 'customers',
          'action' => 'detail',
          'id' => '{id}',
        ], NULL, TRUE),
      ],
      'last_name' => [
        'header' => _('Příjmení'),
        'sorting' => TRUE,
        'css_style' => 'text-align: left',
        'renderer' => 'url',
        'url' => $this->view->url([
          'module' => 'default',
          'controller' => 'customers',
          'action' => 'detail',
          'id' => '{id}',
        ], NULL, TRUE),
      ],
      'email' => [
        'header' => _('Email'),
        'sorting' => TRUE,
        'css_style' => 'text-align: left',
      ],
      'country' => [
        'header' => _('Země'),
        'sorting' => TRUE,
        'css_style' => 'text-align: center',
      ],
      'billing_company' => [
        'header' => _('Firma na faktuře'),
        'css_style' => 'text-align: left',
        'sorting' => TRUE,
      ],
      'person' => [
        'header' => _('Osoba na faktuře'),
        'css_style' => 'text-align: left',
        'sorting' => TRUE,
      ],
      'delete' => [
        'header' => '',
        'css_style' => 'text-align: center',
        'renderer' => 'action',
        'type' => 'delete',
        'url' => $this->view->url([
          'module' => 'default',
          'controller' => 'customers',
          'action' => 'delete',
          'id' => '{id}',
        ], NULL, TRUE),
      ],
      'edit' => [
        'header' => '',
        'css_style' => 'text-align: center',
        'renderer' => 'action',
        'type' => 'edit',
        'url' => $this->view->url([
          'module' => 'default',
          'controller' => 'customers',
          'action' => 'detail',
          'id' => '{id}',
        ], NULL, TRUE),
      ],
    ];
    $acl = Zend_Registry::get('acl');
    if (!$acl->isAllowed('pricelists/assign')) {
      unset($columns['pricelists']);
    }
    if (!$acl->isAllowed('customers/delete')) {
      unset($columns['delete']);
    }


    $grid = new ZGrid([
      'title' => _("Zákazníci"),
      'add_link_url' => $this->view->url([
        'module' => 'default',
        'controller' => 'wallet',
        'action' => 'add.entry',
      ], NULL, TRUE),
      'allow_add_link' => FALSE,
      'add_link_title' => _('New record'),
      'columns' => $columns,
    ]);


    if ($this->_request->getParam('Hledat')) {
      $name = $this->_request->getParam('name', false);
      if (($name !== false) && $chunks = explode(' ', $name)) {
        foreach ($chunks as $chunk) {
          $chunk = addslashes(strip_tags(trim($chunk)));
          if (empty($chunk)) {
            continue;
          }
          $select->where("c.first_name LIKE '%$chunk%' OR c.last_name LIKE '%$chunk%' OR c.company LIKE '%$chunk%' OR c.email LIKE '%$chunk%'" .
            "OR a.company LIKE '%$chunk%' OR a.person LIKE '%$chunk%'");
        }
      }
    }

    $grid->setSelect($select);
    $grid->setRequest($this->_request->getParams());
    $this->view->grid ??= $grid;

  }


  /**
   * Vytvoreni a editace zakaznika.
   */
  public function editAction(): void
  {
    $id = $this->_request->getParam('id');
    $search = $this->_request->getParam('search');
    $customerObj = NULL;
    if ($id === null) {
      // pokud neni ID, vytvorime noveho zakaznika
      $form = new Customers_Form();
    }
    else {
      // pokud je ID editujeme zakaznika
      $customerObj = new Customers_Customer((int) $id);
      $form = $customerObj->getForm();
      $form->removeElement('address_street');
      $form->removeElement('address_pop_number');
      $form->removeElement('address_orient_number');
      $form->removeElement('address_city');
      $form->removeElement('address_zip');
      $form->removeElement('address_country');
    }
    if (!$search && $this->_request->isPost()) {
      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        $data = $form->getValidValues($params);

        if ($data['customer_type'] !== Table_Customers::TYPE_B2B) {
          $data['coo_requirement'] = 'n';
          $data['invoice_verify_requirement'] = 'n';
        }

        if (is_object($customerObj)) {
          $isNew = FALSE;
          $data['id'] = $customerObj->getId();
          $customerObj->update($data);
        }
        else {
          // novy zakaznik
          $isNew = TRUE;
          $customerObj = new Customers_Customer($data);
        }
        if ($orderId = $this->_request->getParam('order')) {
          $order = new Orders_Order($orderId);
          $order->setCustomer($customerObj);
          $order->setTitle($customerObj->getName());
          $this->redirect($this->view->url([
            'controller' => 'order-edit',
            'action' => 'customer',
            'order' => $orderId,
            'id' => NULL,
          ]));
        }
        else {
          $this->redirect($this->view->url([
            'action' => 'detail',
            'id' => $customerObj->getId(),
          ]));
        }
      }
    }
    if ($search) {
      $form->getElement('customer_company')->setValue($search);
    }
    $this->view->form = $form;
  }

  public function deleteAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$tCustomers = new Table_Customers();
		if($this->_request->getParam('id')) {
			$rCustomers = $tCustomers->find($this->_request->getParam('id'))->current();
			$rCustomers->deleted = 1;
			$rCustomers->save();
			$this->_flashMessenger->addMessage("Zákazník odstraněn.");
		}
		$this->redirect('/customers');
	}

	public function addressAction() {
    $address = NULL;
    $id = $this->_request->getParam('id');
    if ($id) {
      $address = new Addresses_Address($id);
      $form = $address->getForm();
    }
    else {
      $customer = $this->_request->getParam('customer');
      $customerObj = new Customers_Customer($customer);
      $form = new Addresses_Form(array('customer' => $customerObj));
    }

    if ($this->_request->isPost()) {
      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        $data = $form->getValidValues($params);
        try {
          if (is_object($address)) {
            $address->update($data);
            $this->_flashMessenger->addMessage('Adresa byla aktualizována');
          }
          else {
            $address = new Addresses_Address($data);
            $this->_flashMessenger->addMessage('Nová adresa byla uložena');
          }

          // Změna dodací adresy u otevřených objednávek.
          if ($address->isShipping()) {
            $changedOrders = array_map(function($o){
              return $o->getNO();
            }, $address->getCustomer()->resetOrdersAddress($address));
            if ($changedOrders) {
              $message = 'Těmto objednávkám byla změněná dodací adresa: ' . implode(', ', $changedOrders);
              $this->_flashMessenger->addMessage($message);
            }
          }

          $this->redirect($params['destination']);
        }
        catch (Exception $e) {
          $this->view->message = $e->getMessage();
          $form->populate($params);
        }
      }
    }
		$this->view->form = $form;
	}

  /**
   * @throws Zend_Db_Statement_Exception
   * @throws Zend_Exception
   * @throws Zend_Form_Exception
   * @throws Customers_Exceptions_NoCustomer
   */
  public function detailAction() {

    $id = (int) $this->_request->getParam('id');
    $customerObj = new Customers_Customer($id);

    $this->view->customer = $customerObj;
    $this->view->title = 'Detail zákazníka – ' . $customerObj->getName();

    $params = [];
    $auth = new Zend_Session_Namespace('Zend_Auth');
    $params['user'] = $auth->user_id;
    $params['view_private'] = Zend_Registry::get('acl')
      ->isAllowed('orders/view-private');
    $params['status'] = [Table_Orders::STATUS_CLOSED];

    $spent = 0.0;
    if ($orders = $customerObj->getOrders($params)) {
        foreach ($orders as $order) {

            if ($order->id_invoice === null) {
                $order->price = null;
                continue;
            }

            $invoice = new Invoice_Invoice($order->id_invoice);
            $priceInvoiced = $invoice->getInvoicedPrice();
            $priceShipping = $invoice->getCustomItemsTotalPrice(['shipping']);

            if ($priceInvoiced === 0.0) {
                $orderObj = new Orders_Order($order->id);
                if ($invoices = $orderObj->getInvoices(true)) {
                    foreach ($invoices as $invoice) {
                        if (!$invoice->isProform()) {
                            continue;
                        }
                        $priceInvoiced = $invoice->getInvoicedPrice();
                        $priceShipping = $invoice->getCustomItemsTotalPrice(['shipping']);
                        break;
                    }
                }
            }

            $price = $priceInvoiced - $priceShipping;

            if ($invoice->getCurrency() === Table_Invoices::CURRENCY_CZK) {
                $rate = $invoice->getRate();
                if ($rate === 1.0) {
                    $rate = Utils::getRate();
                }
                $price /= $rate;
            }

            $order->price = round($price, 2);
            $spent += $order->price;
        }
    }

    $this->view->orders = $orders;
    $this->view->totalSpent = $spent;

    $this->view->addresses = $customerObj->getAddresses();

    $this->view->addressBill = $customerObj->getBillingAddress();
    $this->view->addressShip = $customerObj->getShippingAddress();

    $this->view->vatNo = $customerObj->getVatNo();
    $validVatNo = $customerObj->isVerifiedVatNo();
    $this->view->isValidVatNo = $validVatNo;
    $this->view->isValidVatNoClass = is_null($validVatNo) ? '' : ($validVatNo ? 'btn-success' : 'btn-danger');

    /** @todo ODSTANIT SLEVY * */ /**
     * Product discount
     */
    // table
    $tCustProdDiscount = new Table_CustomersProductsDiscount();
    $CuPrDi_select = $tCustProdDiscount->getProductDiscountList($this->_request->getParam('id'), TRUE);

    // grid
    $product_discount_grid = new ZGrid([
      'add_link_url' => $this->view->url([
        'module' => 'default',
        'controller' => 'wallet',
        'action' => 'add.entry',
      ], NULL, TRUE),
      'allow_add_link' => FALSE,
      'add_link_title' => _('Přidat'),
      'style' => 'width: 500px;',
      'columns' => [
        'id_products' => [
          'header' => _('ID'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
        ],
        'name' => [
          'header' => _('Produkt'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
        ],
        'discount' => [
          'header' => _('Sleva [%]'),
          'sorting' => TRUE,
          'css_style' => 'text-align: left',
        ],
        'delete' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'action',
          'type' => 'delete',
          'url' => $this->view->url([
            'module' => 'default',
            'controller' => 'customers',
            'action' => 'delete-product-discount',
            'c' => $this->_request->getParam('id'),
            'p' => '{id}',
          ], NULL, TRUE),
        ],
        'edit' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'action',
          'type' => 'edit',
          'url' => $this->view->url([
            'module' => 'default',
            'controller' => 'customers',
            'action' => 'detail',
            'id' => $this->_request->getParam('id'),
            'ep' => '{id}#pdis',
          ], NULL, TRUE),
        ],
      ],
    ]);

    $product_discount_grid->setSelect($CuPrDi_select);
    $product_discount_grid->setRequest($this->getRequest()->getParams());

    $this->view->product_discount_list = $product_discount_grid;


    /**
     * Product discount form
     */
    $product_discount_form = new Customers_ProductDiscount();
    $this->view->product_discount_form = $product_discount_form;


    /**
     * Add product discount
     */
    if ($this->getRequest()
        ->isPost() && $product_discount_form->isValid($this->_request->getParams())) {
      $product_id = $product_discount_form->getValue('product_id_selected');
      $discount = $product_discount_form->getValue('product_discount');
      $product_edit = $product_discount_form->getValue('product_edit');

      if (!empty($product_id) && !empty($discount)) {
        $tCPD = new Table_CustomersProductsDiscount();

        // edit
        if (!empty($product_edit)) {
          $tCPD->editProductDiscount($product_edit, $discount);
          $this->_helper->redirector->gotoSimple('detail', 'customers', NULL, ['id' => $this->_request->getParam('id')]);
        }
        // insert
        else {
          if (!$tCPD->checkExist($this->_request->getParam('id'), $product_id)) {
            $tCPD->addDiscount($this->_request->getParam('id'), $product_id, $discount);
            $this->_helper->redirector->gotoSimple('detail', 'customers', NULL, ['id' => $this->_request->getParam('id')]);
          }
        }
      }
    }


    /**
     * Get edit product data
     */
    if ($this->getRequest()->getParam('ep')) {
      $tCPD = new Table_CustomersProductsDiscount();
      $edit_data = $tCPD->getProductDiscount($this->_getParam('ep'));

      $product_discount_form->getElement('add_part_discount')
        ->setLabel('Upravit');

      $product_discount_form->getElement('product_id_selected')
        ->setValue($edit_data['id_products']);
      $product_discount_form->getElement('search_product')
        ->setValue('(' . $edit_data['id_products'] . ') ' . $edit_data['name']);
      $product_discount_form->getElement('product_discount')
        ->setValue($edit_data['discount']);
      $product_discount_form->getElement('product_edit')
        ->setValue($edit_data['id']);
    }


    /**
     * Category discount
     */
    // table
    $tCustCatDiscount = new Table_CustomersCategoryDiscount();
    $CuCaDi_select = $tCustCatDiscount->getCategoryDiscountList($this->_request->getParam('id'), TRUE);

    // grid
    $category_discount_grid = new ZGrid([
      'add_link_url' => $this->view->url([
        'module' => 'default',
        'controller' => 'wallet',
        'action' => 'add.entry',
      ], NULL, TRUE),
      'allow_add_link' => FALSE,
      'add_link_title' => _('Přidat'),
      'style' => 'width: 500px;',
      'columns' => [
        'id_parts_ctg' => [
          'header' => _('ID'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
        ],
        'name' => [
          'header' => _('Kategorie'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
        ],
        'discount' => [
          'header' => _('Sleva [%]'),
          'sorting' => TRUE,
          'css_style' => 'text-align: left',
        ],
        'delete' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'action',
          'type' => 'delete',
          'url' => $this->view->url([
            'module' => 'default',
            'controller' => 'customers',
            'action' => 'delete-category-discount',
            'c' => $this->_request->getParam('id'),
            'cat' => '{id}',
          ], NULL, TRUE),
        ],
      ],
    ]);

    $category_discount_grid->setSelect($CuCaDi_select);
    $category_discount_grid->setRequest($this->getRequest()->getParams());

    $this->view->category_discount_list = $category_discount_grid;


    /**
     * Category discount form
     */
    $category_discount_form = new Customers_CategoryDiscount();

    $tCat = new Table_PartsCtg();
    $cat_list = $tCat->getCategorySelectList();
    $category_discount_form->getElement('search_category')
      ->setMultiOptions($cat_list);

    $this->view->category_discount_form = $category_discount_form;


    /**
     * Add category discount
     */
    if ($this->getRequest()
        ->isPost() && $category_discount_form->isValid($this->_request->getParams())) {
      $category_id = $category_discount_form->getValue('search_category');
      $discount = $category_discount_form->getValue('category_discount');

      if (!empty($category_id) && !empty($discount)) {
        $tCCD = new Table_CustomersCategoryDiscount();
        if (!$tCCD->checkExist($this->_request->getParam('id'), $category_id)) {
          $tCCD->addDiscount($this->_request->getParam('id'), $category_id, $discount);
          $this->_helper->redirector->gotoSimple('detail', 'customers', NULL, ['id' => $this->_request->getParam('id')]);
        }
      }
    }

    $this->view->budgets = $customerObj->getBudgets();
    $this->view->newBudgetForm = new Customers_BudgetForm(['id_customer' => $customerObj->getId()]);

    $this->view->pricelist = $customerObj->getPriceList();
  }


  /**
   * Get products for autocomplete discount
   */
  public function getProductsAjaxAction() {
    $term = $this->_getParam('term');

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $tParts = new Table_Parts();
      $parts_list = $tParts->getProductByIdOrName($term);

      $this->_helper->json(array('parts' => $parts_list->toArray()));
  }


  /**
   * Get categories for autocomplete discount
   */
  public function getCategoriesAjaxAction() {
    $term = $this->_getParam('term');

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $tCat = new Table_PartsCtg();
    $cat_list = $tCat->getCategoryByIdOrName($term);

    $this->_helper->json(['categories' => $cat_list]);
  }


  public function deleteProductDiscountAction()
  {
      $product_id = $this->_getParam('p');

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $tCPD = new Table_CustomersProductsDiscount();
      $tCPD->delete('id = ' . (int)$product_id);

      $this->_helper->redirector->gotoSimple('detail', 'customers', null, array('id' => $this->_getParam('c')));
  }

  public function deleteCategoryDiscountAction()
  {
      $category_id = $this->_getParam('cat');

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $tCCD = new Table_CustomersCategoryDiscount();
      $tCCD->delete('id = ' . (int)$category_id);

      $this->_helper->redirector->gotoSimple('detail', 'customers', null, array('id' => $this->_getParam('c')));
  }

  /**
   * Prehled zakaznickych skladu
   */
  public function warehousesAction() {
    $params = $this->_request->getParams();
    // data grid
    $tCustomersWarehouses = new Table_CustomersWarehouses();
    $options = array();
    $options['add_link_url'] = $this->view->url(array('action' => 'warehouse-add'));
    $options['columns']['name']['url'] = $this->view->url(array('action' => 'warehouse-detail', 'id' => '{id}'));
    $grid = $tCustomersWarehouses->getGrid($options);
    $grid->setRequest($params);
    $this->view->title = 'Zákaznické sklady';
    $this->view->grid = $grid->render();
  }

  /**
   * Zalozeni noveho zakaznickeho skladu
   */
  public function warehouseAddAction() {

    $form = new Customers_WarehouseForm();

    if ($this->_request->isPost()) {
      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        try {
          $wh = new Customers_Warehouse($params);
          $this->_flashMessenger->addMessage('Sklad "' . $wh->getName() . '" byl vytvořen.');
          $this->_redirect($this->view->url(array(
            'action' => 'warehouse-detail',
            'id' => $wh->getId(),
          )));
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage('Sklad nebyl založen. ' . $e->getMessage());
        }
      }
      $form->populate($params);
    }
    $this->view->title = 'Založení zákaznického skladu';
    $this->view->form = $form;
    $this->renderScript('customers/warehouse-edit.phtml');
  }

  /**
   * Editace zakaznickeho skladu
   */
  public function warehouseEditAction() {




  }

  /**
   * Zruseni zakaznickeho skladu
   *
   * zatim jen pro sklad, ktery je prazdny
   *
   * @todo Co s polozkami skladu, ktery neni prazny a rusi se?
   */
  public function warehouseDeleteAction() {

  }

  /**
   * Detailni prehled konkretniho zakaznickeho skladu
   */
  public function warehouseDetailAction() {
    $id = $this->_request->getParam('id');
    try {
      $this->view->warehouse = new Customers_Warehouse($id);
      $this->view->products = $this->view->warehouse->getProducts();
      $this->view->customer = $this->view->warehouse->getCustomer();
      $this->view->summary = $this->view->warehouse->getSummary();
      $this->view->modalForm = new Customers_WarehouseItemForm();
      $this->view->title = 'Detail zákaznického skladu';
    }
    catch (Exception $e) {
      $this->_flashMessenger->addMessage($e->getMessage());
      $this->_redirect($this->view->url(array('action' => 'warehouses', 'id' => NULL)));
    }
  }

  public function ajaxWarehouseItemUpdateAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);
    $id = $this->_request->getParam('id');
    if ($id) {
      $date = new Meduse_Date($this->_request->getParam('aquisition_date'), 'd.m.Y');
      $tProducts = new Table_CustomersWarehousesProducts();
      $tProducts->update(array(
        'aquisition_date' => $date->toString('Y-m-d h:i:s'),
        'aquisition_price' => $this->_request->getParam('aquisition_price'),
        'inventory_number' => $this->_request->getParam('inventory_number'),
      ), 'id = ' . $id);
      $row = $tProducts->find($id)->current();
      $warehouse = new Customers_Warehouse($row->id_customers_warehouses);
      $summary = $warehouse->getSummary();
      $status = TRUE;
      $date = new Meduse_Date($row->aquisition_date, 'Y-m-d h:i:s');
      $data = array(
        'id' => $row->id,
        'aquisition_date' => $date->toString('d.m.Y'),
        'aquisition_price' => Meduse_Currency::format($row->aquisition_price),
        'inventory_number' => $row->inventory_number,
        'summary_count' => $summary->count,
        'summary_price' => Meduse_Currency::format($summary->price),
      );
    }
    else {
      $status = FALSE;
      $data = array();
    }
   $this->_helper->json(array('status' => $status, 'data' => $data));
  }

  public function ajaxSearchAction() {
     $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $params = $this->getRequest()->getParams();
      $params['wh'] = Table_PartsWarehouse::WH_TYPE;
      $grid = Customers::getDataGrid($params);
      $html = $grid->render();
      $this->_helper->json(array('html' => $html));
  }


  public function ajaxVerifyVatAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $data = array(
      'status' => FALSE,
      'result' => NULL,
      'message' => NULL,
    );
    $cId = $this->_request->getParam('customer');
    try {
      $customer = new Customers_Customer($cId);
      $verified = $customer->verifyVatNo();
      $data = array(
        'status' => TRUE,
        'result' => $verified,
        'message' => 'Proběhlo ověření na serveru VIES',
      );
    }
    catch (Exception $e) {
      $data['message'] = $e->getMessage();
    }
    $this->_helper->json($data);
  }

  public function budgetSaveAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);

    // odeslani modal casti, tzn. nastaveni budgetu
    if ($this->_request->getParam('id_customer')) {
      $date = new Meduse_Date($this->_request->getParam('contract_date'));
      $length = $this->_request->getParam('contract_length');
      if ($budgetId = $this->_request->getParam('id_budget')) {
        if ($length > 0) {
          $budget = new Customers_Budget($budgetId);
          $budget->setContractDate($date, FALSE);
          $budget->setContractLength($length, FALSE);
          $budget->save();
        }
        else {
          Customers_Budget::delete($budgetId);
        }
      }
      else {
        $customer = new Customers_Customer($this->_request->getParam('id_customer'));
        Customers_Budget::create($customer, $date, $length);
      }
      $this->_flashMessenger->addMessage('Smluvní budget byl uložen');
      $this->redirect('customers/detail/id/' . $this->_request->getParam('id_customer'));
    }

    else {
      $budget = new Customers_Budget($this->_request->getParam('id_budget'));
      $length = $budget->getContractLength();
      $start = (int) $budget->getContractDate()->toString('Y');
      for ($i = 0; $i < $length; $i++) {
        $product = $this->_request->getParam('products_amount_' . ($start + $i));
        $service = $this->_request->getParam('services_amount_' . ($start + $i));
        $budget->setYearBudgetAmount($start + $i, $product, $service, FALSE);
      }
      $budget->save();
      $this->_flashMessenger->addMessage('Částky byly uloženy.');
      $this->redirect('customers/detail/id/' . $budget->getCustomerId());
    }

  }

  public function addressDeleteAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $addressId = $this->_request->getParam('id');
    $backUrl = $this->_request->getParam('destination', '/customers');

    if (!$addressId) {
      $message = 'Nebylo zadáno ID adresy';
    }
    else {
      $tAddresses = new Table_Addresses();
      $updated = $tAddresses->update(['deleted' => 1], 'id = ' . $addressId);

      if ($updated === 0) {
        $message = 'Adresu se nepodařilo odstranit';
      }
      else {
        $message = 'Adresa byla odstraněna. <a href="'
          . $this->view->url(['action' => 'address-undelete'])
          . '?destination=' . $backUrl . '">Vrátit</a>';
      }
    }
    $this->_flashMessenger->addMessage($message);
    $this->redirect($backUrl);
  }

  public function addressUndeleteAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $addressId = $this->_request->getParam('id');
    $backUrl = $this->_request->getParam('destination', '/customers');

    if (!$addressId) {
      $message = 'Nebylo zadáno ID adresy';
    }
    else {
      $tAddresses = new Table_Addresses();
      $updated = $tAddresses->update(['deleted' => 0], 'id = ' . $addressId);

      if ($updated === 0) {
        $message = 'Adresu se nepodařilo obnovit';
      }
      else {
        $message = 'Adresa byla obnovena.';
      }
    }
    $this->_flashMessenger->addMessage($message);
    $this->redirect($backUrl);
  }
}