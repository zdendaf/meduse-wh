<?php
class TobaccoPricesController extends Meduse_Controller_Action {

    public function init() {
      $this->_helper->layout->setLayout('bootstrap-tobacco');
      parent::init();
    }

    public function editAction() {

        $partId = $this->getRequest()->getParam('part');
        try {
            $part = new Tobacco_Parts_Part($partId);
        } catch (Exception $e) {
            $this->_flashMessenger->addMessage('Nepodařilo se načíst produkt <em>' . $partId . '</em>.');
            $this->_redirect('/tobacco/warehouse');
        }
        if (!$part->isProduct()) {
            $this->_flashMessenger->addMessage('Součást <em>' . $partId . '</em> není produktem.');
            $this->_redirect('/tobacco/parts-detail/id/' . $partId);
        }

        $tPrice = new Table_Prices();
        $this->view->prices = $tPrice->getProductPricesAllPricelists($partId);
        $this->view->part = $part;
    }

    public function saveAction() {
        if (!$this->getRequest()->isPost()) {
            $this->_redirect('/tobacco-prices');
        }
        $params = $this->getRequest()->getPost();
        $tPrices = new Table_Prices();
        foreach ($params as $name => $value) {

            list($base, $id) = explode('_', $name, 2);
            if ($base != 'price') {
                continue;
            }
            $price = (float) $value;
            if ($price > 0) {
                $tPrices->savePrice($params['part'], '', $id, $price);
            } else {
                $tPrices->delete('id_parts = "' . $params['part'] . '" AND id_pricelists = ' . $id);
            }
        }
        $this->_flashMessenger->addMessage('Ceny byly uloženy');
        $this->_redirect('/tobacco/parts-detail/id/' . $params['part']);
    }
}
