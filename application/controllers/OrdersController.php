<?php
class OrdersController extends Meduse_Controller_Action{

  public function init() {
    $this->_helper->layout->setLayout('bootstrap-basic');
    parent::init();
  }

  /**
   * Prehled Objednavek
   */
  public function indexAction() {

    $reg = Zend_Registry::get('parts_warehouse');
    
    $orders = new Table_Orders();
    $db = $orders->getAdapter();
    $db->setFetchMode(Zend_Db::FETCH_OBJ);
    $this->view->title = "Objednávky";
  
    $this->view->tab = $this->_request->getParam('tab');;
    $filterTabs = Utils::isAllowed('orders/list-all-orders') ? [] : ['closed'];
    $this->view->tabs = $this->getDataTabs($this->view->tab, $filterTabs);
    
    if ($this->view->tab) {
      $reg->last_orders_tab = $this->_request->getParam('tab');
      $this->_helper->layout->setLayout('ajax');

      // Celkovy prehled -------------------------------------------------
      if ($this->view->tab == 'total') {
        if (Utils::isAllowed('orders/special/all-sets-counts')) {

          $this->view->data = $orders->getAllSetsCounts();
          $this->view->data2 = $orders->getAllPipesCounts();

          $reports = new Reports();
          $this->view->requested_products = $reports->getRequestedProducts();
          $this->view->requested_services = $reports->getRequestedServices();

        }
      }

      else {
        $params = [];

        // maji se v prehledu objednavek zobrazovat sluzby?
        $params['service'] = Utils::isAllowed('orders/extra-show-services');

        // Expedovane objednavky ---------------------------------------
        if ($this->view->tab == 'closed') {
          if ($this->_request->getParam('sort') && $this->_request->getParam('sort') == 'no') {
            $params['order'] = ['YEAR(date_exp) DESC', 'no'];
          }
          else {
            if ($this->_request->getParam('sort') && $this->_request->getParam('sort') == 'name') {
              $params['order'] = ['YEAR(date_exp) DESC', 'title'];
            }
            else {
              if ($this->_request->getParam('sort') && $this->_request->getParam('sort') == 'invoice') {
                $params['order'] = ['YEAR(date_exp) DESC', 'invoice_no'];
              }
              else {
                $params['order'] = 'date_exp DESC';
              }
            }
          }
          if ($this->_request->getParam('year', date('Y'))) {
            $params['where'][] = [
              'YEAR(date_exp) = ?',
              $this->_request->getParam('year', date('Y')),
            ];
          }
          $emailTemplates = new Table_EmailTemplates();
          $this->view->templates = $emailTemplates->getList();
          $this->view->sendmailForm = new EmailTemplates_SendmailForm();
        }
        // Strategicke objednavky --------------------------------------
        elseif ($this->view->tab == 'strategic') {
          $params['order'] = 'strategic_priority DESC';
          $this->view->costs_show = Utils::isAllowed('orders/show-strategic-costs');
          $tOrdersProducts = new Table_OrdersProducts();
          $costs = $tOrdersProducts->getCosts(NULL, [Table_Orders::STATUS_STRATEGIC]);
          $this->view->costs = $costs;
          $this->view->costs_sums = [];
          $this->view->costs_sums['time'] = 0;
          $this->view->costs_sums['price'] = 0;
          $this->view->costs_sums['operation'] = 0;
          $this->view->costs_sums['amount_sets'] = 0;
          $this->view->costs_sums['price_sets'] = 0;
          $this->view->costs_sums['price_acc'] = 0;
          foreach ($costs as $item) {
            $this->view->costs_sums['time'] += $item['operation_time_total'];
            $this->view->costs_sums['operation'] += $item['operation_cost_total'];
            $this->view->costs_sums['price'] += $item['price_total'];
            $this->view->costs_sums['amount_sets'] += $item['amount_sets'];
            $this->view->costs_sums['price_sets'] += $item['price_sets'];
            $this->view->costs_sums['price_acc'] += $item['price_acc'];
          }
        }
        // Trashovane objednavky ---------------------------------------
        elseif ($this->view->tab == 'trash') {
          $params['order'] = 'date_delete DESC';
          $this->view->data2 = $orders->getAllSetsTrashCounts();
        }
        // Ostatní
        else {
          if ($this->_request->getParam('sort')) {
            $this->view->sort = $this->_request->getParam('sort');
            $sorder = $this->_request->getParam('sorder', 'asc') == 'desc' ? ' DESC' : ' ASC';
            $this->view->sorder = $this->_request->getParam('sorder', 'asc');
            switch ($this->_request->getParam('sort')) {
              case 'type':
                $params['order'] = ['type' . $sorder];
                break;
              case 'no':
                $params['order'] = ['no' . $sorder];
                break;
              case 'name':
                $params['order'] = ['title' . $sorder];
                break;
              case 'request':
                $params['order'] = ['date_request' . $sorder];
                break;
              case 'deadline':
                $params['order'] = ['date_deadline' . $sorder];
                break;
              case 'internal':
                $params['order'] = ['date_deadline_internal' . $sorder];
                break;
            }
          }
        }
        
        $params['user'] = Utils::getCurrentUserId();
        $params['view_private'] = Utils::isAllowed('orders/view-private');
        $this->view->data = $orders->getOrdersNew($this->view->tab, $params);
      }
    }
    else {
      if (!$reg->last_orders_tab) {
        $reg->last_orders_tab = $filterTabs ? reset($filterTabs) : 'open';
      }
      $this->view->lastTab = $reg->last_orders_tab;
      $this->_helper->layout->setLayout('bootstrap-basic');
    }
  }

  /**
   * @deprecated
   * @return void
   * @throws \Zend_Form_Exception
   */
  public function ordersReportAction() {
    $queryStatus = '';
		$this->_helper->layout->setLayout('bootstrap-basic');
		$form = new Orders_ReportForm();
		if($this->getRequest()->getParam('Odeslat',false) && $form->isValid($this->getRequest()->getParams())) {
			$queryStatus = $this->getRequest()->getParam('status', '');
			$queryColl = $this->getRequest()->getParam('collection', false);
			$dateFrom = $this->getRequest()->getParam('order_close_from', false);
			$dateTo = $this->getRequest()->getParam('order_close_to', false);
            $customer = $this->getRequest()->getParam('customer', false);
            if($customer) {
                $customerId = $this->getRequest()->getParam('customer_id', false);
            } else {
                $customerId = null;
            }
			$tOrders = new Table_Orders();
			$this->view->data = $tOrders->getCollectionsReport($queryStatus, $queryColl, $dateFrom, $dateTo, $customerId);
			$this->view->data2 = $tOrders->getPartsReport($queryStatus, $queryColl, $dateFrom, $dateTo, $customerId);
		}

    switch ($queryStatus) {
      case Table_Orders::STATUS_CLOSED:
        $statusSubTitle = 'na expedovaných objednávkách';
        break;
      case Table_Orders::STATUS_CONFIRMED:
        $statusSubTitle = 'na potvrzených objednávkách';
        break;
      case Table_Orders::STATUS_DELETED:
        $statusSubTitle = 'na smazaných objednávkách';
        break;
      case Table_Orders::STATUS_NEW:
        $statusSubTitle = 'na předběžných objednávkách';
        break;
      case Table_Orders::STATUS_OFFER:
        $statusSubTitle = 'nabídek';
        break;
      case Table_Orders::STATUS_OPEN:
        $statusSubTitle = 'na otevřených objednávkách';
        break;
      case Table_Orders::STATUS_STRATEGIC:
        $statusSubTitle = 'na strategických objednávkách';
        break;
      case Table_Orders::STATUS_TRASH:
        $statusSubTitle = 'na objednávkách v koši';
        break;
      case 'all':
      default:
        $statusSubTitle = 'na všech objednávkách';
        break;
    }
		$this->view->title = 'Interaktivní přehled objednávek';
    $this->view->statusSubTitle = $statusSubTitle;
		$this->view->form = $form;
	}

	public function carriersAction() {
		$this->view->title = "Dopravci";
    $tProducers = new Table_Producers();
 		$this->view->activeCarriers = $tProducers->getCarriers('active');
 		$this->view->inactiveCarriers = $tProducers->getCarriers('inactive');
	}

	public function trackingNoAction() {
		$this->view->title = "Tracking no.";

		$form = new Orders_TrackingNoForm();

		$tOrders = new Table_Orders();
		$idOrder = (int) $this->_request->getParam('id');
		$rOrder = $tOrders->find($idOrder)->current();
		if ($this->_request->isPost() && $form->isValid($this->_request->getParams())){
			$tOrders->update(array('tracking_no' => $this->_request->getParam('tracking_no')), "id = ".$tOrders->getAdapter()->quote($idOrder));
			$this->_flashMessenger->addMessage("Tracking no nastaveno.");
			$this->redirect("orders/detail/id/".$idOrder);
		}
		elseif($rOrder !== null) {
			$form->tracking_no->setValue($rOrder->tracking_no);
		}
		else {
			$this->redirect("orders");
		}

		$this->view->form = $form;
		$this->render('add');
	}

	public function paymentAcceptDateAction() {
		$this->view->title = "Datum potvrzení platby";
		$this->_helper->layout->setLayout('bootstrap-basic');

		if(!$this->_request->getParam('id')){
			$this->_redirect("orders");
		}

		$form = new Orders_PaymentAcceptDateForm();

		$tOrders = new Table_Orders();
		$idOrder = (int) $this->_request->getParam('id');
		$order = new Orders_Order($idOrder);
		$rOrder = $order->getRow();
		if($rOrder->payment_method == Table_Orders::PM_CASH_ON_DELIVERY) {
			$dateName = 'date_payment_accept';
		} else {
			$dateName = 'date_pay_all';
		}
		if($this->_request->isPost() && $form->isValid($this->_request->getParams())){
			$tOrders->update(array($dateName => $order->fromHRtoISO($this->_request->getParam('meduse_date_payment_accept'))), "id = ".$tOrders->getAdapter()->quote($idOrder));

			$this->_flashMessenger->addMessage("Datum potvrzení bylo uloženo.");
			$this->_redirect("orders/detail/id/".$idOrder);
		} elseif($rOrder !== null && $rOrder->date_payment_accept) {

			$form->meduse_date_payment_accept->setValue($order->fromISOtoHR($rOrder->{$dateName}) );
		}

		$this->view->form = $form;
		$this->render('add');
	}

	  /** Nepouzivana funkce. */
    public function strategicGraphAction(){
        $this->_helper->layout->disableLayout();
    }

    /** Nepouzivana funkce. */
    public function strategicDataAction(){

        $this->_helper->layout->disableLayout();

        $orders = new Table_Orders();
        $db = $orders->getAdapter();

        // 0..11
        $mesice = array('led','úno','bře','dub','kvě','čvn','čvc','srp','zář','říj','lis','pro');
        $pocetZobrazenych = 18;


        $mesiceLabels = array();
        $current = date('m');;
        $year = date('Y');
        for($i = 0; $i <= $pocetZobrazenych; $i++){
            $actual = ($i + ($current - 1)) % 12;
            if(count($mesiceLabels) < 1 || $actual == 0){
                $mesiceLabels[] = $mesice[$actual]." ".$year++;
            } else {
                $mesiceLabels[] = $mesice[$actual];
            }
        }

        $select = $db->select();
        $select->from(
            "orders", array(
                'no',
                'title',
                'strategic_start',
                'strategic_priority',
                'strategic_months',
                'start_month' => new Zend_Db_Expr("MONTH(strategic_start)"),
                'start_year' => new Zend_Db_Expr("YEAR(strategic_start)"),
                'zero_month' => new Zend_Db_Expr("MONTH(CURDATE())"),
                'zero_year' => new Zend_Db_Expr("YEAR(CURDATE())"),
            )
        )->where(
            "status = 'strategic'"
        )->where(
            "strategic_start IS NOT NULL AND strategic_priority IS NOT NULL AND strategic_months IS NOT NULL"
        );// die( $select );
        $rows = $db->fetchAll($select);
        $colors = array('#33CC33','#0000FF','#FF0000','#6363AC','#FFCC00','#660033');

        include_once( 'OpenFlashChart/open-flash-chart.php' );

        $chart = new open_flash_chart();
        $y = new y_axis();
        $y->set_colour("#2779AA");
        $y->set_range( 0, 10, 1 );
        $y->set_grid_colour( '#F2F5F7' );
        $chart->set_y_axis($y);

        $x_labels = new x_axis_labels();
        $x_labels->set_steps( 1 );
        //$x_labels->set_vertical();
        $x_labels->set_labels( $mesiceLabels );
        $x_labels->rotate(45);

        $x = new x_axis();
        $x->set_labels( $x_labels );
        $x->colour('#2779AA');
        $x->set_grid_colour( '#F2F5F7' );
        //$x->set_range(0, 12);
        $chart->set_x_axis( $x );

        $clr = 0;
        foreach($rows as $row){
            $data = array();
            for($i = 1; $i <= $pocetZobrazenych; $i++){

                $startMonth = $row['start_month'] - $row['zero_month'] + (12 * ($row['start_year'] - $row['zero_year']) ) +1;
                $endMonth   = $startMonth + $row['strategic_months'] +1;

                if($i >= $startMonth && $i <= ($endMonth - 1)){
                    $data[] = (int) $row['strategic_priority'];
                } else {
                    $data[] = null;
                }
            }

            $colour = $colors[$clr++ % count($colors)];

            $d = new hollow_dot();
            $d->size(4)->halo_size(0)->colour($colour);
            $d->tooltip($row['title'].", priorita: #val#");

            $line = new line();
            $line->set_default_dot_style($d);
            $line->set_width( 3 );
            $line->set_colour($colour);
            $line->set_values( $data );
            $line->set_key( $row['title'], 10 );

            $chart->add_element( $line );
        }



        $title = new title( 'Strategické objednávky 2011' );
        $title->set_style( "{font-size: 20px; font-family: Arial; font-weight: bold; color: #2779AA; text-align: center;}" );
        $chart->set_title( $title );

        $x_legend = new x_legend( 'Měsíce' );
        $x_legend->set_style( '{font-size: 14px; color: #778877}' );
        $chart->set_x_legend( $x_legend );

        $y_legend = new y_legend( 'Priorita' );
        $y_legend->set_style( '{font-size: 14px; color: #778877}' );
        $chart->set_y_legend( $y_legend );

        $chart->set_bg_colour( '#FFFFFF' );
        echo $chart->toPrettyString();
        return;

    }

  //nepouzivana (stara) akce
	public function addAction(){
    $this->_redirect('/order-edit');
	}

	public function addCarrierAction(){
		$this->_helper->layout->setLayout('bootstrap-basic');
		$this->view->title = "Nový dopravce";

		$form = new Orders_CarrierForm();
    $form->setLegend($this->view->title);

    // formular kontaktni osoba
    $person = new Producers_PersonForm();
    $form->addElement($person->getElement('meduse_producer_deputy'));
		$form->addElement($person->getElement('meduse_producer_phone'));
		$form->addElement($person->getElement('meduse_producer_phone2'));

    $elem = new Meduse_Form_Element_Text('tracking_pattern');
		$elem->setLabel('Tracking pattern:');
    $form->addElement($elem);

		if($this->_request->isPost() && $form->isValid($this->_request->getParams())){

      // vytvoreni dodavatele
      $carrier = new Producers_Carrier();
      $dataCarrier = array(
				'name' => $form->getValue('meduse_carrier_name'),
				'id_producers_ctg' => 0,
				'address' => $this->_request->getParam('meduse_carrier_address'),
				'www' => $this->_request->getParam('meduse_carrier_www'),
				'description' => $this->_request->getParam('meduse_carrier_desc'),
        'tracking_pattern' => $this->_request->getParam('tracking_pattern'),
      );
      $carrier->create($dataCarrier);

      // vlozeni kontaktni osoby
			$dataPerson = array(
				'id_producers' => $carrier->getId(),
				'name' => $this->_request->getParam('meduse_producer_deputy'),
				'phone' => $this->_request->getParam('meduse_producer_phone'),
				'phone2' => $this->_request->getParam('meduse_producer_phone2'),
			);
      $carrier->addPerson($dataPerson);

      // vlozeni zpusobu prepravy
      $dataDelivery = array(
				'name' => 'výchozí',
				'inserted' => new Zend_Db_Expr("NOW()"),
			);
      $carrier->addDelivery($dataDelivery);

			$act = new Table_Actualities();
			$act->add("Vložil nového dopravce '".$form->getValue('meduse_carrier_name')."'.");
			$this->_flashMessenger->addMessage("Dopravce '".$form->getValue('meduse_carrier_name')."' uložen.");

      $this->_redirect($this->view->url(array(
				'controller' => 'producers',
				'action' => 'detail-carrier',
				'id' => $carrier->getId(),
			)));
		}
		$this->view->form = $form;
	}

	public function editCarrierAction(){
		if(!$this->_request->getParam('id')) $this->_redirect('orders');
		$this->_helper->layout->setLayout('layout2');
		$this->view->title = "Úprava dopravce";
		$form = new Orders_CarrierForm();
		if($this->_request->isPost() && $form->isValid($this->_request->getParams())){
			$this->_db->update('orders_carriers', array(
				'name' => $form->getValue('meduse_carrier_name'),
				"address" => $this->_request->getParam("meduse_carrier_address"),
				"www" => $this->_request->getParam("meduse_carrier_www"),
				"description" => $this->_request->getParam("meduse_carrier_desc")
				//'inserted' => new Zend_Db_Expr("NOW()")
			),"id = ".(int)$this->_request->getParam('id'));
			$act = new Table_Actualities();
			$act->add("Upravil dopravce '".$form->getValue('meduse_carrier_name')."'.");
			$this->_flashMessenger->addMessage("Dopravce '".$form->getValue('meduse_carrier_name')."' uložen.");
			$this->_redirect('/orders/carriers');
		} else {
			$form->meduse_carrier_name->setValue($this->_db->fetchOne("SELECT name FROM orders_carriers WHERE id = ?",$this->_request->getParam('id')));
		}
		$this->view->form = $form;
		$this->render('add');
	}

  /**
   * TODO Oddelit formular z kontroleru.
   * @throws \Zend_Date_Exception
   * @throws \Zend_Db_Table_Exception
   * @throws \Zend_Form_Exception
   */
  public function editStrategicParamsAction() {
    if (!$this->_request->getParam('order')) {
      $this->redirect('orders');
    }

    $tOrders = new Table_Orders();
    $rOrders = $tOrders->find($this->_request->getParam('order'))->current();

    $form = new Meduse_FormBootstrap('parts_box', 'Strategické parametry objednávky');
    $form->setLegend('Strategické parametry objednávky');
    $form->setAttrib('class', 'form');

    $element = new Meduse_Form_Element_DatePicker('meduse_orders_start', [
        'jQueryParams' => [
          'dateFormat' => 'dd.mm.yy',
        ],
      ]);
    $element->setRequired();
    $element->setLabel('Datum od');
    $form->addElement($element);

    $element = new Meduse_Form_Element_Select('meduse_orders_priority');
    $element->setRequired();
    $element->addMultiOptions([
      1 => "1 - nízká",
      2 => 2,
      3 => 3,
      4 => 4,
      5 => 5,
      6 => 6,
      7 => 7,
      8 => 8,
      9 => 9,
      10 => "10 - vysoká",
    ]);
    $element->setLabel('Priorita');
    $form->addElement($element);

    $element = new Meduse_Form_Element_Text('meduse_orders_months');
    $element->setLabel('Počet měsíců');
    $element->setRequired();
    $form->addElement($element);

    if ($this->_request->getParam('from') == 'new-form') {
      $element = new Zend_Form_Element_Hidden('from');
      $element->setValue("new-form");
      $form->addElement($element);
    }

    $element = new Meduse_Form_Element_Submit('Odeslat');
    $form->addElement($element);

    if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {

      $date = new Zend_Date($form->getElement('meduse_orders_start')
        ->getValue(), "dd.MM.yyyy");
      $rOrders->strategic_start = $date->toString("yyyy-MM-dd");
      $rOrders->strategic_priority = (int) $form->getElement('meduse_orders_priority')
        ->getValue();
      $rOrders->strategic_months = (int) $form->getElement('meduse_orders_months')
        ->getValue();
      $rOrders->save();

      if ($this->_request->getParam('from') == 'new-form') {
        $this->redirect("orders/select-products/order/" . $rOrders->id);
      }
      else {
        $this->redirect('orders');
      }
    }
    else {
      $date = new Zend_Date($rOrders->strategic_start, "dd.MM.yyyy");
      $form->getElement('meduse_orders_start')
        ->setValue($date->toString("dd.MM.yyyy"));
      $form->getElement('meduse_orders_priority')
        ->setValue($rOrders->strategic_priority === NULL ? '' : $rOrders->strategic_priority);
      $form->getElement('meduse_orders_months')
        ->setValue($rOrders->strategic_months === NULL ? '' : $rOrders->strategic_months);
    }
    $form->init();
    $this->view->form = $form;
  }

    /**
     * @throws Orders_Exceptions_ForbiddenAccess
     * @throws Exception
     */
    public function deleteAction(): void
    {
    $this->_helper->viewRenderer->setNoRender();

    if (!$this->_request->getParam('id')) {
      $this->redirect('orders');
    }

    $order = new Orders_Order($this->_request->getParam('id'));

    if ($order->hasReservedProducts()) {
      $this->_flashMessenger->addMessage('Objednávku nelze odstranit, má rezervované produkty. Kontaktujte vedoucího skladu.');
      $this->redirect($this->view->url(array('action' => 'detail')));
    }

    /** @var array<Invoice_Invoice> $numberedInvoices */
    $numberedInvoices = $order->getInvoices(true);
    $invoiceNumbers = [];
    if ($numberedInvoices) {
        foreach ($numberedInvoices as $invoice) {
            if (!$invoice->isRegular() && !$invoice->isCredit()) {
                continue;
            }
            $invoiceNumbers[$invoice->getId()] = $invoice->getNo();
            $invoice->setNo(null);
        }
    }

    // pridavani aktuality
    $act = new Table_Actualities();
    $act->add("Vyhodil do koše objednávku č. ".$order->getNO());

    $order->delete();

    $this->_flashMessenger->addMessage("Objednávka č. ".$order->getNO()." ".$order->getTitle()." byla vyhozena do koše.");
    if ($invoiceNumbers) {
        array_walk($invoiceNumbers, static function(&$item, $key){
            $item = '<a href="/invoice/prepare/id/' . $key . '" target="_blank">' . $item . '</a>';
        });
        $this->_flashMessenger->addMessage('Těmto fakturám byla odebrána čísla: ' . implode(', ', $invoiceNumbers));
    }
    $this->redirect('orders');
	}

	public function deleteCarrierAction() {
		$this->_helper->viewRenderer->setNoRender();
		if(!$this->_request->getParam('id')) $this->_redirect('orders');
		$rows = $this->_db->fetchAll("SELECT * FROM orders WHERE id_orders_carriers = ".$this->_db->quote((int) $this->_request->getParam('id')));
		if(count($rows) > 0) {
			$this->_flashMessenger->addMessage("Dopravce nelze smazat, již byl přiřazen některým objednávkám.");
			$this->_redirect('/orders/carriers');
		}
		$this->_db->delete("orders_carriers", "id = ".$this->_db->quote((int) $this->_request->getParam('id')));
		$this->_flashMessenger->addMessage("Dopravce vymazán.");
		$this->_redirect('/orders/carriers');
	}

	public function deleteFromTrashAction(){
		if(!$this->_request->getParam('id')) $this->_redirect('orders');
		$order = new Orders_Order($this->_request->getParam('id'));
		//pridavani aktuality
		$act = new Table_Actualities();
		$act->add("Smazal objednávku č. ".$order->getNO());
		$order->deleteFromTrash();
		$this->_flashMessenger->addMessage("Objednávka č. ".$order->getNO().". ".$order->getTitle()." byla vymazána.");
		$this->_redirect('orders');
		$this->_helper->viewRenderer->setNoRender();
	}

	public function deleteItemAction(){
		if(!$this->_request->getParam('order')) $this->_redirect('orders');
		if(!$this->_request->getParam('item')) $this->_redirect('orders');

		$order = new Orders_Order($this->_request->getParam('order'));
		$order->cancelAllPrepared($this->_request->getParam('item'));
		$orders_products = new Table_OrdersProducts();
		$select = $orders_products->select()->where("id_orders =".$this->_request->getParam('order')." AND id_products LIKE '".$this->_request->getParam('item')."'");
		$row = $orders_products->fetchRow($select);
		$orders_products->getAdapter()->delete("orders_products" ,"id = ".$row['id']);
		$order->recalculateExpectedPayment();
		$act = new Table_Actualities();
    if (!$order->isPrivate() && in_array($order->getStatus(), Orders_Order::NOTIFIED_STATES)) {
      $order->getQueue()->add('Produkt ' . $this->_request->getParam('item') . ' odebrán.');
    }
		$act->add("Smazal produkt ".$this->_request->getParam('item')." z objednávky č. ".$order->getNO());
		$this->_flashMessenger->addMessage("Položka objednávky ".$this->_request->getParam('item')." vymazána.");
		$this->_redirect('orders/detail/id/'.$this->_request->getParam('order') . '#items');
		$this->_helper->viewRenderer->setNoRender();
	}

	public function detailAction(){

		if (!$this->_request->getParam('id')) $this->redirect('orders');
		$orders =  new Table_Orders();
		try {
      $order = new Orders_Order($this->_request->getParam('id'));
    }
    catch (Exception $e) {
      $this->_flashMessenger->addMessage($e->getMessage());
      $this->redirect($this->view->url(array('action' => 'index', 'order' => NULL, 'id' => NULL)));
    }
    if ($queue = $order->getQueue(FALSE)) {
		  $queue->send();
    }
    if ($order->isService()) {
      $this->view->data = $order->getServices();
    }
    else {
      $this->view->data = $orders->getSortedOrderParts($this->_request->getParam('id'));
      $this->view->pipeSummary = $orders->getPipeSummary($this->_request->getParam('id'));
    }

    $customer = NULL;
    $customerId = $order->getRow()->id_customers;
    if ($customerId) {
      $customer = new Customers_Customer($customerId);
      $this->view->customer = $customer;
      $this->view->customerName = $customer->getName();
      // dodaci adresa prislusna k objednavce
      $order->address = $order->getAddress();
      // fakturacni adresa prislusna k zakaznikovi
      $order->addressInvoice = $customer->getBillingAddress();
      $addressForm = $order->getChangeAddressForm($this->view->url(). '#infos', TRUE);
      $addressForm->setAction('/orders/change-address');
      $this->view->addressForm = $addressForm;
    }
    $this->view->hsForm = $order->getHSForm();
    $this->view->order = $order;

    $acl = Zend_Registry::get('acl');
    $this->view->acl = $acl;

    $this->view->invoices = $order->getInvoices(!$acl->isAllowed('invoice/show-non-numbered'));
    $this->view->dbRowOrder = $orders->find($this->_request->getParam('id'))->current();
		$this->view->holograms = $this->_db->fetchAll("SELECT * FROM holograms WHERE id_orders = :id", array('id' => $this->_request->getParam('id')));

    $allow_edit = $order->getStatus() != Table_Orders::STATUS_CLOSED && $order->getStatus() != Table_Orders::STATUS_DELETED && $acl->isAllowed('order-edit/index');
    if (isset($_SESSION['Zend_Auth']['user_role'])) {
      foreach($_SESSION['Zend_Auth']['user_role'] as $role) {
        if (in_array($role, array('manager', 'admin'))) {
          $allow_edit = TRUE;
          break;
        }
      }
    }
    $this->view->allow_edit = $allow_edit;
    $this->view->orderHasReservedProducts = $order->hasReservedProducts();
    if ($this->view->orderHasReservedProducts) {
      $this->view->allow_edit_products = FALSE;
      $this->view->allow_edit_note = 'Objednávku nelze plně editovat, protože má rezervované produkty. Pro umožnění plné editace kontaktujte vedoucího skladu.';
    }
    else {
      $this->view->allow_edit_products = TRUE;
    }

    $allow_edit_address = is_null($customer) && $order->getStatus() != Table_Orders::STATUS_CLOSED;
    if (isset($_SESSION['Zend_Auth']['user_role'])) {
      foreach($_SESSION['Zend_Auth']['user_role'] as $role) {
        if (in_array($role, array('manager', 'admin', 'accountant_pipes'))) {
          $allow_edit_address = TRUE;
          break;
        }
      }
    }
    $this->view->allow_edit_address = $allow_edit_address;

    $this->view->allow_invoices = $acl->isAllowed('invoice/index');

    // Baleni.

    $packForm = $order->getPackagingForm();
    $elements = $packForm->getElements();
    /** @var \Zend_Form_Element $element */
    foreach ($elements as $element) {
      $type = $element->getType();
      if ($type != 'Meduse_Form_Element_Submit') {
        $element->setLabel(NULL);
      }
    }
    $packaging = new stdClass();
    $packaging->is_allowed = $acl->isAllowed('orders/packaging-save');
    $packaging->form = $packForm;
    $packaging->count = $order->getPackagingCount();

    $this->view->packaging = $packaging;

    $this->view->packagingItems = [];
    $this->view->packagingVolume = 0;
    $this->view->packagingWeight = 0;

    if ($packaging = $order->getPackaging(TRUE)) {
      $this->view->packagingItems = $packaging['items'];
      $this->view->packagingVolume = $packaging['volume'];
      $this->view->packagingWeight = $packaging['weight'];
    }

    $tPricelists = new Table_Pricelists();
    $pricelistId = $order->getPricelist();
    if ($pricelistId) {
      $this->view->priceList = $tPricelists->find($pricelistId)->current();
    } else {
      $this->view->priceList = NULL;
    }

    $this->view->uploadForm = new Orders_FileUploadForm();
    $this->view->uploadForm->setOrderId($order->getID());
    $this->view->uploadForm->setAction('/orders/file-upload');
    $this->view->files = $order->getFilesList();

    $this->view->cooDatesForm = $order->getCOODatesForm();

    // Tracking.

    $trackingForm = $order->getTrackingForm();
    $elements = $trackingForm->getElements();
    /** @var \Zend_Form_Element $element */
    foreach ($elements as $element) {
      $type = $element->getType();
      if ($type != 'Meduse_Form_Element_Submit') {
        $element->setLabel(NULL);
      }
    }
    $tracking = new stdClass();
    $tracking->form = $trackingForm;
    $tracking->count = $trackingForm->getItemsCount();
    $tracking->codes = $order->getTrackingCodes();
    $tracking->is_allowed = $acl->isAllowed('orders/tracking-save');

    $this->view->tracking = $tracking;

    $regularInvoice = $order->getRegularInvoice();
    $this->view->regular_invoice_id = $regularInvoice ? $regularInvoice->getId() : NULL;

    // Fixni poplatky.
    $this->view->fees = [];
    if ($fees = $order->getFees()) {
      foreach ($fees as $fee) {
        $this->view->fees[] = $fee['name'] . ' (' . Meduse_Currency::format($fee['price'], 'EUR') . ')';
      }
    }
	}

	public function fileUploadAction() {
    $this->_helper->viewRenderer->setNoRender();
    if ($this->_request->isPost()) {
      $received = [];
      $idOrder = $this->getParam('id_orders');
      $upload = new Zend_File_Transfer();
      $files = $upload->getFileInfo();
      foreach ($files as $file => $info) {
        if (!$upload->isUploaded($file)) {
          $this->_flashMessenger->addMessage('Nebyl nahrán žádný soubor.');
          continue;
        }
        if (!$upload->isValid($file)) {
          $this->_flashMessenger->addMessage('Soubor <em>' . $file . '</em> není přípustný.');
          continue;
        }
        $dir = APPLICATION_PATH . Orders_Order::DATA_UPLOADS_DIR . Orders_Order::DATA_UPLOADS_SUBDIR_PREFIX . $idOrder;
        if (!file_exists($dir)) {
          mkdir($dir);
        }
        $filename =  $dir . '/' . $info['name'];
        $upload->addFilter('Rename', ['target' => $filename, 'overwrite' => TRUE], $file);
        $received[] = $info['name'];
      }
      if ($upload->receive()) {
        $auth = new Zend_Session_Namespace('Zend_Auth');
        $table = new Table_OrdersFiles();
        $data = [];
        foreach ($received as $item) {
          $data[] = [
            'filename' => $item,
            'title' => $this->_request->getParam('title'),
            'id_users' => $auth->user_id,
          ];
        }
        $table->addFiles($idOrder, $data);
        $this->_flashMessenger->addMessage('Soubor <em>' . implode(', ', $received) . '</em> byl přidán k objednávce.');
      }
      else {
        $this->_flashMessenger->addMessage('Soubor se nepodařilo k objednávce přidat.');
      }

      $this->redirect('/orders/detail/id/' . $idOrder . '#infos');
    }
  }

  public function fileRemoveAction() {
    $this->_helper->viewRenderer->setNoRender();
    $idOrder = $this->_request->getParam('id');
    $idFile = $this->_request->getParam('fid');
    $order = new Orders_Order($idOrder);
    if ($order->fileDelete($idFile)) {
      $this->_flashMessenger->addMessage('Soubor byl smazán.');
    }
    else {
      $this->_flashMessenger->addMessage('Soubor se nepodařilo smazat.');
    }
    $this->redirect('/orders/detail/id/' . $idOrder . '#infos');
  }

  public function fileDownloadAction() {
    $this->_helper->viewRenderer->setNoRender();
    $idOrder = $this->_request->getParam('id');
    $idFile = $this->_request->getParam('fid');
    $order = new Orders_Order($idOrder);
    if ($file = $order->getFile($idFile)) {
      $dir = APPLICATION_PATH . Orders_Order::DATA_UPLOADS_DIR . Orders_Order::DATA_UPLOADS_SUBDIR_PREFIX . $idOrder;
      $filepath = $dir . '/' . $file['filename'];
      if (file_exists($filepath)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        flush(); // Flush system output buffer
        readfile($filepath);
        die();
      }
      else {
        http_response_code(404);
        die();
      }
    }
    else {
      die("Invalid file name!");
    }
  }

	public function editProductAmountAction(){
		if (!$this->_request->getParam('id')) {
            $this->_redirect('orders');
        }
        $productId = $this->_request->getParam('product', false);
        if (!$productId) {
            $this->_redirect('orders');
        }
		$this->_helper->layout->setLayout('bootstrap-basic');
		$order = new Orders_Order($this->_request->getParam('id'));

		// formular
        $form = new Orders_EditProductAmountForm();
        $form->setLegend('Produkt '.$productId);

		// zpracovani formulare
		if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
            $prodAmount = (int) $form->product_amount->getValue();
            $prodAmount += $order->getFreeItemAmount($productId);
			$order->setProductAmount($productId, $prodAmount);

      //pridavani aktuality
      $act = new Table_Actualities();
      $act->add("Upravil počet produktu " . $productId . " na objednávce č. " . $order->getNO());
      if (!$order->isPrivate() && in_array($order->getStatus(), Orders_Order::NOTIFIED_STATES)) {
        $order->getQueue()
          ->add('Počet produktu ' . $productId . ' = ' . $prodAmount . ' ks');
      }
      $this->_flashMessenger->addMessage("Počet produktu upraven.");
      $this->redirect('orders/detail/id/' . $this->_request->getParam('id') . '#items');
    }
    else {
      $form->getElement('product_amount')
        ->setValue($order->getProductAmount($productId) - $order->getFreeItemAmount($productId));
    }

		$this->view->form = $form;
		$this->view->order_title = $order->getTitle();
        $this->view->order_id = $order->getID();
	}

    /**
     * Edit free item
     */
    public function editFreeItemAmountAction() {
        /**
         * Check @params from url or redirect out
         */
        if (!$this->_request->getParam('id')) {
            $this->_redirect('orders');
        }
        if (!$this->_request->getParam('product')) {
            $this->_redirect('orders');
        }

        /**
         * Set layout
         */
        $this->_helper->layout->setLayout('bootstrap-basic');

        /**
         * Load data
         */
        $order = new Orders_Order($this->_request->getParam('id'));

        /**
         * Form
         */
        $form = new Orders_EditFreeItemAmountForm();
        $form->setLegend('Produkt ' . $this->_request->getParam('product') . ' (kusy zdarma)');

        /**
         *  Send form
         */
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
            // set param
            try {
                $order->setFreeItemAmount($this->_request->getParam('product'), $form->getValue('free_item_amount'));
            } catch (Zend_Exception $e) {
                echo $e->getMessage();
            }

            // add actuality
            try {
                $act = new Table_Actualities();
                $act->add("Upravil počet produktu " . $this->_request->getParam('product') . " na objednávce č. " . $order->getNO());
            } catch (Zend_Exception $e) {
                echo $e->getMessage();
            }
            if (!$order->isPrivate() && in_array($order->getStatus(), Orders_Order::NOTIFIED_STATES)) {
              $order->getQueue()->add('Free počet produktu ' . $this->_request->getParam('product') . ' = ' . $form->getValue('free_item_amount' . ' ks'));
            }

            // message
            $this->_flashMessenger->addMessage("Počet produktu upraven.");

            // redirect out
            $this->_redirect('orders/detail/id/' . $this->_request->getParam('id').'#items');
        } else {
            $form->getElement('free_item_amount')->setValue($order->getFreeItemAmount($this->_request->getParam('product')));
        }

        /**
         *  To view
         */
        $this->view->form = $form;
        $this->view->order_title = $order->getTitle();
        $this->view->order_id = $order->getID();
    }


	public function openAction(){
		$this->getHelper('viewRenderer')->setNoRender();

		$order = new Orders_Order($this->_request->getParam('id'));
		$act = new Table_Actualities();
		$act->add("Otevřel objednávku č. ".$order->getNO());

		$orders = new Table_Orders();
		$orders->openOrder($this->_request->getParam('id'));
    //#611 - 	Oznámení o otevření objendávky
    $tEmails = new Table_Emails();
    $tEmails->sendEmail(Table_Emails::EMAIL_ORDER_OPEN_NOTICE, array('orderId'=>$order->getID()) );
    $this->_flashMessenger->addMessage("Objednávka č. ".$order->getNO()." byla označena jako otevřená.");
		$this->_redirect('orders');
	}

	public function confirmAction(){
		$this->_helper->layout->setLayout('bootstrap-basic');
		$this->view->title = "Potvrzení objednávky";
		$order = new Orders_Order($this->_request->getParam('id'));
		if (is_null($order->getCustomer()->id)) {
			$this->_flashMessenger->addMessage("Objednávka č. " . $order->getNO() . " nebyla potvrzena. Není přířazen zákazník.");
			$this->redirect('orders');
		}
		else {
			$form = new Orders_ConfirmForm();
      $form->id->setValue($order->getRow()->id);
			if ($this->_request->isPost() && $form->isValid($this->_request->getParams())){
				$act = new Table_Actualities();
				$act->add("Potvrdil objednávku č. ".$order->getNO());
				$tEmails = new Table_Emails();
				$tEmails->sendEmail(Table_Emails::EMAIL_ORDER_CONFIRM, array('orderId' => $order->getID()));
				$orders = new Table_Orders();
				$orders->confirmOrder($this->_request->getParam('id'));

				$this->_flashMessenger->addMessage("Objednávka č. ".$order->getNO()." byla potvrzena.");
				$this->redirect('orders');
			}
			$this->view->form = $form;
      $this->view->detailUrl = $this->view->url(['controller' => 'orders', 'action' => 'detail', 'id' => $order->getID()]) . '#invoices';
      $this->view->expectedPayment = $order->getExpectedPayment();
      $this->view->expectedDeposit = $order->getExpectedPaymentDeposit();
      $this->view->expectedCurrency = $order->getExpectedCurrency();
      $this->view->paymentPercentage = round($order->getPaidPercentage() * 100);
      $this->view->isDepositPaid = $order->isDepositPaid();
      $this->view->dateDeposit = $order->getDatePayDeposit();
      $this->view->isPaid = $order->isPaid();
      $this->view->datePayment = $order->getDatePayAll();
      $this->view->paymentMethod = $order->getPaymentMethod(true);
		}
	}

	public function confirmOfferAction() {
		$this->getHelper('viewRenderer')->setNoRender();
		$order = new Orders_Order($this->_request->getParam('id'));
		$order->getRow()->status = Table_Orders::STATUS_NEW;
		$order->getRow()->save();
		$this->_flashMessenger->addMessage("Objednávka č. ".$order->getNO()." byla označena jako předběžná.");
		$this->_redirect('orders');
	}

	public function setNewAction(){
		$this->getHelper('viewRenderer')->setNoRender();

		$order = new Orders_Order($this->_request->getParam('id'));
		$act = new Table_Actualities();
		$act->add("Označil objednávku č. ".$order->getNO()."jako předběžnou.");

		$orders = new Table_Orders();
		$orders->setNewOrder($this->_request->getParam('id'));
        $this->_flashMessenger->addMessage("Objednávka č. ".$order->getNO()." byla označena jako předběžná.");
		$this->_redirect('orders');
	}

	public function selectProductsAction() {

    $this->_helper->layout->setLayout('bootstrap-basic');

		$a = NULL;
		$error = FALSE;

		try {
      $a = new Orders_Order($this->getRequest()->getParam('order'));
    }
    catch (Exception $e) {
      $this->_flashMessenger->addMessage($e->getMessage());
      $error = TRUE;
    }

    if ($error) {
      $this->redirect($this->view->url(array(
        'action' => 'index',
        'order' => NULL,
        'id' => NULL,
      )));
    }

    $tCollections = new Table_Collections();
		$collections = $tCollections->getCollectionsSelect(TRUE)->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    $collections_names = [];
    $collections_qs = [];
		foreach ($collections as $collection) {
      $collections_names[$collection['code']] = $collection;
      $collections_qs[$collection['code']] = 'coll[]=' . $collection['id'];
    }
		$this->view->collections = $collections_names;
		$this->view->collections_qs = implode('&', $collections_qs);

    if ($a->hasReservedProducts()) {
      $this->view->allow_edit = FALSE;
      $this->view->orderid = $a->getID();
    }
    else {
      $this->view->allow_edit = TRUE;
      // kontrola moznosti upravy polozek pouze u strategickych zakazek
      if ($a->getStatus() !== Table_Orders::STATUS_STRATEGIC
        && !Zend_Registry::get('acl')->isAllowed('orders/special/select-not-only-strategic')) {
        $this->_flashMessenger->addMessage('Můžete upravovat položky jen u strategické objednávky.');
        $this->redirect('orders/detail/id/' . $this->getRequest()->getParam('order'));
      }

      $a->getOrderItemsPriceCount();
      $order = new Orders_Order($this->_request->getParam('order'));
      if($this->getRequest()->getParam('act')) {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        //kontrola, jestli je uz polozka vlozena v databazi
        $row = $this->_db->fetchRow("SELECT * FROM orders_products WHERE id_orders = "
          . $this->_db->quote($this->getRequest()->getParam('order'))
          . " AND id_products = " . $this->_db->quote($this->getRequest()->getParam('p')));

        if ($row) {
            if ($this->_request->getParam("a",0) > 0
                  || $this->_request->getParam("fa",0) > 0
                  || $this->_request->getParam("ta",0) > 0)
            {
              $data = array(
                "amount" => $this->_request->getParam("a",0) + $this->_request->getParam("fa",0) + $this->_request->getParam("ta",0),
                "free_amount" => $this->_request->getParam("fa",0),
              );
              $this->_db->update('orders_products', $data, "id_orders = "
                . $this->_db->quote($this->_request->getParam('order'))
                . " AND id_products = " . $this->_db->quote($this->_request->getParam("p")));
              echo 'updated';
            }
            else {
              $this->_db->delete('orders_products', "id_orders = "
                . $this->_db->quote($this->_request->getParam('order'))
                . " AND id_products =" . $this->_db->quote($this->_request->getParam("p")));
              echo 'deleted';
            }
        }
        elseif ($this->_request->getParam("a") > 0
                  || $this->_request->getParam("fa") > 0
                  || $this->_request->getParam("ta",0) > 0)
        { //vkladam
          $hsCode = $this->_db->select()
            ->from('parts', ['hs'])
            ->where('id = ?', $this->_request->getParam('p'))
            ->query()
            ->fetchColumn();
          $data = array(
            'id_orders' => $this->_request->getParam('order'),
            'id_products' => $this->_request->getParam('p'),
            'amount' => $this->_request->getParam('a',0)
              + $this->_request->getParam('fa',0)
              + $this->_request->getParam('ta',0),
            'free_amount' => $this->_request->getParam('fa'),
            'hs_code' => $hsCode ? $hsCode : NULL,
          );
          $this->_db->insert('orders_products',$data);
          echo 'inserted';
        }
        $order->recalculateExpectedPayment();
        if (!$order->isPrivate()) {
          $product_id = $this->_request->getParam('p');
          $amount = $this->_request->getParam('a', 0);
          $amount_free = $this->_request->getParam('fa', 0);
          $msg = 'Produkt ' . $product_id . ': počet = '
            . ($amount + $amount_free) . ' ks, free = ' . $amount_free .  ' ks';
          $order->getQueue()->add($msg);
        }
        die;
      }

      // kontrola, zda-li neni objednavka otevrena
      $status = $order->getStatus();
      $acl = Zend_Registry::get('acl');
      if ($status == Table_Orders::STATUS_OPEN && !$acl->isAllowed('orders/edit-opened-products')) {
          $this->_flashMessenger->addMessage('Nemáte oprávnění editovat položky otevřené zakázky.');
          $this->redirect('/orders/detail/id/' . $order->getID());
      }

      $tReservations = new Table_OrdersProductsParts();
      $this->view->rezerv = $tReservations->getOrdersReservations(array($order->getID()));
      $this->view->form = new Parts_Filter();

      $tOrders = new Table_Orders();
      $tParts = new Table_Parts();

      $collections = $this->getRequest()->getParam('coll', false);
      $this->view->form->coll->setValue($collections);

      $select = $tParts->getProductsSelect($collections);

      if(!Zend_Registry::get('acl')->isAllowed('orders/special/select-not-only-extra-wraps')
        && $this->getRequest()->getParam('ctg', false) != 22) {
        $this->getRequest()->setParam('ctg', 22);
      }

      $ctg = $this->getRequest()->getParam('ctg', false);
      if(!empty($ctg) && $ctg != 'null'){

        if($ctg == Table_PartsCtg::TYPE_SETS) {
          $this->view->sets = true;
        }
        $select->where('pa_ctg.id = ? OR pa_ctg.parent_ctg = '.$ctg, $ctg);
        $this->view->form->ctg->setValue($ctg);
      }
      else {
        $select->where('pa_ctg.id NOT IN (?)', array(Table_TobaccoWarehouse::CTG_TOBACCO, Table_TobaccoWarehouse::CTG_MATERIALS));
        $select->where('pa_ctg.parent_ctg NOT IN (?)', array(Table_TobaccoWarehouse::CTG_TOBACCO, Table_TobaccoWarehouse::CTG_MATERIALS));
      }

      $name = $this->getRequest()->getParam('name', false);
      if(!empty($name) && $name != 'null'){
        $select->where('p.name LIKE ?','%'.$name.'%');
        $this->view->form->name->setValue($name);
      }

      $pId = $this->getRequest()->getParam('id',false);
      if(!empty($pId) && $pId != 'null'){
        $select->where('p.id LIKE ?','%'.$pId.'%');
        $this->view->form->id->setValue($pId);
      }

      $this->view->orderid = $order->getID();
      $this->view->data = $select->query()->fetchAll();

      $this->view->order_items = array();
      $this->view->order_free_items = array();
      $this->view->order_transfer_items = array();

      foreach($tOrders->getSortedOrderParts($order->getID()) as $part) {
        $this->view->order_items[$part['id_products']] = $part['amount'];
        if($part['free_amount'] > 0) {
          $this->view->order_free_items[$part['id_products']] = $part['free_amount'];
        }
      }
    }
	}

	private function getProductsArray($getOrderPartsResultArray, $type = null){
		$result = array();
		foreach($getOrderPartsResultArray as $ctg_name => $parts){
			foreach($parts as $part){
				$result[$part['id']] = $part;
			}
		}
		return $result;
	}

	public function showAllAccessoriesAction(){
                $this->_helper->layout->setLayout('bootstrap-basic');
		if(!$this->_request->getParam('id')) $this->_redirect('orders');
		$orders   =  new Table_Orders();
                $orderRow = $orders->find($this->_request->getParam('id'))->current();
                $this->view->data = $orderRow->getParts();
                $this->view->order = $orderRow;
	}

	/**
	 * Soucasti produktu s checkboxy
	 * @return unknown_type
	 */
	public function productPartsAction(){
    $this->_helper->layout->setLayout('bootstrap-basic');
		if (!$this->_request->getParam('product')) $this->_redirect('orders');
		try {
			$part = new Parts_Part($this->_request->getParam('product'));
			$this->view->part = $part;
		}
    catch (Exception $e) {
			$this->_redirect('parts/detail/id/'.$this->_request->getParam('product'));
		}
	}

	public function proformaAction() {
    $params = $this->getRequest()->getParams();
    $params['wh'] = Table_PartsWarehouse::WH_TYPE;
    $this->view->dataGrid = Orders::getProformDataGrid($params);
	}

	public function partsAction(){
		$this->_helper->layout->setLayout('bootstrap-basic');
		$form = new Orders_FilterForm();
		$this->view->title = "Objednávka součástí";

		if ($this->_request->isPost()
      && $form->isValid($this->_request->getParams())) {

			$report = new Orders_PartsReport();
			$report->setFilter($form->getValues());
			$report->setOrder(Orders_PartsReport::ORDER_PARTS);
			$this->view->productions = new Table_Productions();
			$this->view->data = $report->getData();

		}
		$this->view->form = $form;
	}

	/**
	 * Prehled rezervaci objednavky
	 */
	public function prepareAction(){
    $this->_helper->layout->setLayout('bootstrap-basic');
		if(!$this->_request->getParam('id')) $this->_redirect('orders');
		$orders   =  new Table_Orders();
		$order = new Orders_Order($this->_request->getParam('id'));
		$this->view->data = $orders->getOrderParts($this->_request->getParam('id'));
		$this->view->order = $order;
	}

	/**
	 * Rucni rezervace polozky objednavky
	 */
	public function prepareItemAction(){
		if(!$this->_request->getParam('order')) $this->_redirect('orders');
		if(!$this->_request->getParam('item')) $this->_redirect('orders');

		$product = new Parts_Part($this->_request->getParam('item'));
		$order   = new Orders_Order($this->_request->getParam('order'));
		$categories      = new Table_PartsCtg();
		$parts_warehouse = new Table_PartsWarehouse();

		if($this->_request->getParam("Odeslat")){
			$orders_products_parts = new Table_OrdersProductsParts();
			$orders_products = new Table_OrdersProducts();

			foreach($this->_request->getParams() as $name => $value){
				if(substr($name, -7) == '_amount'){
					if((int) $value < 1) continue;
					$id = explode('_', $name);
					$id = $id[0];
					try{
						$order->reserveProduct($product->getID(), $value);
						$this->_flashMessenger->addMessage("Všechny součásti produktu byly rezervovány.");
					} catch (Exception $e) {
						$this->_flashMessenger->addMessage("chyba pri chystani soucasti."); //.$e->getMessage()
					}
				}
			}
			$this->_redirect('orders/prepare-item/order/'.$this->_request->getParam('order').'/item/'.$this->_request->getParam('item'));
		}

		$this->view->categories = $categories->getCategories(); //kategorie soucasti produktu
		$this->view->product = $product;
		$this->view->order = $order;
		$this->view->parts_warehouse = $parts_warehouse;
	}

	public function prepareOverviewAction(){
		if(!$this->_request->getParam('order')) $this->_redirect('orders');
		if(!$this->_request->getParam('item')) $this->_redirect('orders');

		$product = new Parts_Part($this->_request->getParam('item'));
		$order   = new Orders_Order($this->_request->getParam('order'));
		$categories      = new Table_PartsCtg();
		$parts_warehouse = new Table_PartsWarehouse();

		$this->view->categories = $categories->getCategories(); //kategorie soucasti produktu
		$this->view->product = $product;
		$this->view->order = $order;
		$this->view->parts_warehouse = $parts_warehouse;
	}

	/**
	 * Rezervace vsech produktu jedne polozky objednavky
	 */
	public function prepareAllAction(){
		if(!$this->_request->getParam('order')) $this->_redirect('orders');
		if(!$this->_request->getParam('item')) $this->_redirect('orders');

		$order   = new Orders_Order($this->_request->getParam('order'));
		$part = new Parts_Part($this->_request->getParam('item'));

		try{
			$order->reserveAll($part->getID());
			$this->_flashMessenger->addMessage("Všechny součásti produktu byly rezervovány a vyřazeny ze skladu.");
		} catch (Parts_Exceptions_LowAmount $e) {
			$this->_flashMessenger->addMessage("Na skladu neni dostatek součásti ".$e->getMessage());
		} catch (Exception $e) {
			$this->_flashMessenger->addMessage("Nepodařilo se rezervovat všechny součásti produktu. ".$e->getMessage());
		}

		$this->_request->setParams(array());
		$this->_redirect('orders/prepare/id/'.$this->_request->getParam('order'));
		$this->_helper->viewRenderer->setNoRender();
	}

	/**
	 * rezervace vsech soucasti vsech polozek objednavky
	 */
	public function prepareAllOrderItemsAction(){
		if(!$this->_request->getParam('id')) $this->_redirect('orders');
		$orders   = new Table_Orders();
		$order    = new Orders_Order($this->_request->getParam('id'));
		$parts_warehouse = new Table_PartsWarehouse();
		$db = Zend_Registry::get('db');

		//existuje uz rezervace?
		if(
			$db->fetchAll("SELECT * FROM `orders_products_parts` WHERE `id_orders_products` IN (
				SELECT `id`	FROM `orders_products` WHERE `id_orders` =".$order->getID()."
			)")
		){
			$this->_flashMessenger->addMessage("Některé součásti objednávky už jsou rezervovány. Zrušte rezervaci objednávky a pak ji znovu rezervujte.");
			$this->_redirect('orders/prepare/id/'.$this->_request->getParam('id'));
			return;
		}

		$select = $this->_db->select();
		$select->from(
			array('op' => 'orders_products'),
			array(
				"part_amount" => 'amount',
			)
		)->joinLeft(
			array('p' => 'parts'),
			"p.id = op.id_products",
			array(
				"part" => "id",
				"product" => "id",
				"part_name" => "name",
			)
		)->where(
			"op.id_orders = ?", $order->getID()
		);
		$rows = $db->fetchAll($select);

		$missing = array();
		//chybi soucasti?
		foreach($rows as $row){
			if($row['part_amount'] > ($wh_amount = $parts_warehouse->getAmount($row['part']))){
				//echo $row['part_amount']." > ".$wh_amount."<br>";
				$missing[] = array('id' => $row['part'], 'amount' => ($row['part_amount'] - $wh_amount), 'name' => $row['part_name']);
			} elseif($row['part_amount'] > ($base_amount = $parts_warehouse->getBaseAmount($row['part']))) {
				$missing[] = array('id' => $row['part'], 'amount' => $base_amount, 'extern' => ($wh_amount - $base_amount), 'order' => $row['part_amount'], 'name' => $row['part_name']);
			}
		}

		if(count($missing) > 0){
			$message = "<b>Rezervaci nelze provézt. Skladem nejsou tyto součásti:</b><br /><br />";
			foreach($missing as $part){
				if(substr($part['name'],0,4) == 'OBAL'){
					$message .= "<a href='".$this->view->baseUrl()."/wraps/detail/id/".$part['id']."'>".$part['id']."</a> ".$part['name']." - chybí ".$part['amount']." ks<br />";
				}elseif(isset($part['extern'])){
					$message .= "<a href='".$this->view->baseUrl()."/parts/detail/id/".$part['id']."'>".$part['id']."</a> ".$part['name']." - externi: ".$part['extern']." ks, skladem: ".$part['amount']." ks, objednávka: ".$part['order']." ks<br />";
				} else {
					$message .= "<a href='".$this->view->baseUrl()."/parts/detail/id/".$part['id']."'>".$part['id']."</a> ".$part['name']." - chybí ".$part['amount']." ks<br />";
				}
			}
			$this->_flashMessenger->addMessage($message);
		} else {
			$rows = $db->fetchAll($orders->getAllOrderProductParts(array($order->getID())));
			$db->beginTransaction();
			try{
				foreach($rows as $row){
					$order->reservePart($row['product'], $row['part'], $row['part_amount']);
				}
				$db->commit();
				$this->_flashMessenger->addMessage("Rezervace součástí provedena. Objednávku je možné <b><a href='/orders/expedition/order/".$order->getID()."'>expedovat</a></b>.");
			} catch (Exception $e){
				$db->rollback();
				throw $e;
				$this->_flashMessenger->addMessage("Rezervace součástí se nepovedla.".$e->getMessage());
			}

		}

		$this->_redirect('orders/prepare/id/'.$this->_request->getParam('id'));
		$this->_helper->viewRenderer->setNoRender();
	}

	public function cancelPreparedAction(){
		if(!$this->_request->getParam('order')) $this->_redirect('orders');
		if(!$this->_request->getParam('item')) $this->_redirect('orders');
		if(!$this->_request->getParam('part')) $this->_redirect('orders');

		$order   = new Orders_Order($this->_request->getParam('order'));
		$part    = new Parts_Part($this->_request->getParam('part'));
		$product = new Parts_Part($this->_request->getParam('item'));

		if($order->getReadyProductAmount($product->getID()) > 0){
			try{
				$order->cancelPrepared($product->getID(), $part->getID());
				$this->_flashMessenger->addMessage("Rezervace zrušena. Součásti jsou zpátky na skladě.");
			} catch (Exception $e) {
				$this->_flashMessenger->addMessage("Rezervaci se nepodarilo zrusit.".$e->getMessage());
			}
		}
		$this->_redirect('orders/prepare-item/order/'.$this->_request->getParam('order').'/item/'.$this->_request->getParam('item'));
		$this->_helper->viewRenderer->setNoRender();
	}

	public function cancelAllPreparedAction(){
		if(!$this->_request->getParam('order')) $this->_redirect('orders');
		if(!$this->_request->getParam('item')) $this->_redirect('orders');

		$order   = new Orders_Order($this->_request->getParam('order'));
		$product = new Parts_Part($this->_request->getParam('item'));

		try{
			$order->cancelAllPrepared($product->getID());
			$this->_flashMessenger->addMessage("Rezervace zrušena. Součásti jsou zpátky na skladě.");
		} catch (Exception $e) {
			$this->_flashMessenger->addMessage("Rezervaci se nepodarilo zrusit.".$e->getMessage());
		}
		$this->_redirect('orders/prepare/id/'.$this->_request->getParam('order'));
		$this->_helper->viewRenderer->setNoRender();
	}

	/**
	 * Zrusi vsechny rezervovane soucasti vsech polozek objednavky
	 * @return unknown_type
	 */
	public function cancelAllOrderItemsAction(){
		if(!$this->_request->getParam('id')) $this->_redirect('orders');
		$orders   =  new Table_Orders();
		$order = new Orders_Order($this->_request->getParam('id'));
		$orders_parts = $orders->getOrderParts($this->_request->getParam('id'));

		$mess = '';
		foreach($orders_parts as $ctg_name => $category){
			foreach($category as $row){
				try{
					$order->cancelAllPrepared($row['id']);
					$mess = "Rezervace zrušena.";
				} catch (Parts_Exceptions_LowAmount $e) {
					$this->_flashMessenger->addMessage($e->getMessage());
					return;
				}
			}
		}

		if($mess != '')$this->_flashMessenger->addMessage($mess);

		$this->_redirect('orders/prepare/id/'.$this->_request->getParam('id'));
		$this->_helper->viewRenderer->setNoRender();
	}

	public function expeditionAction() {

    $orderId = $this->_request->getParam('order');
    if (!$orderId) {
      $this->redirect('orders');
    }

    $form = new Orders_ExpeditionForm();

    $order = new Orders_Order($orderId);

    $products = $order->getProducts();

    if ($this->_request->isPost() || empty($products)) {

      $tableOrders = new Table_Orders();
      $rowOrder = $tableOrders->find($order->getID())->current();
      /**
       * @see https://bitbucket.org/zdendaf/meduse-wh/issues/245/
      if (
        !$order->isService() &&
        $order->getDatePayAll() === NULL &&
        $rowOrder->payment_method != Table_Orders::PM_CASH_ON_DELIVERY &&
        $rowOrder->payment_method != Table_Orders::PM_CASH &&
        $rowOrder->payment_method != Table_Orders::PM_FREE_CLAIM &&
        $rowOrder->payment_method != Table_Orders::PM_FREE_PRESENTATION
      ) {
        $this->_flashMessenger->addMessage("Objednávku nelze expedovat, není vyplněno datum zaplacení.");
        $this->redirect('orders');
      }
      */
      // Zjistit oprávnění k expedici - oprávnění pro exped. prázdných obj.

      if (Zend_Registry::get('acl')->isAllowed('orders/special/actions')) {
        $allow_process = TRUE;
      }
      elseif (Zend_Registry::get('acl')->isAllowed('orders/special/action-close-empty') && empty($products)) {
        $allow_process = TRUE;
      }
      else {
        $allow_process = FALSE;
      }

      if ($allow_process) {
        try {

          // samotna expedice
          $order->processExpedition();

          // presun do zakazniceho skladu
          $warehouseId = (int) $this->_request->getParam('warehouse_id');
          if ($warehouseId) {
            $wh = new Customers_Warehouse($warehouseId);
            $wh->addProducts($order);
          }

          //pridavani aktuality
          $act = new Table_Actualities();
          $act->add("Expedoval objednávku č. ".$order->getNO());
          // send email
          $tEmails = new Table_Emails();
          if ($rowOrder->id_orders_carriers == Table_OrdersCarriers::EX_WORKS) {
            $tEmails->sendEmail(Table_Emails::EMAIL_ORDER_EXPEDITION_NOTICE_EX_WORKS, array('orderId' => $order->getID()));
          }
          else {
            $tEmails->sendEmail(Table_Emails::EMAIL_ORDER_EXPEDITION_NOTICE, array('orderId' => $order->getID()));
          }
          $this->_flashMessenger->addMessage("Objednávka byla expedována. Rezervované součásti byly zrušeny.");
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage("Objednávku se nepodařilo expedovat. <br>".$e->getMessage());
        }
      }
      else {
        $this->_flashMessenger->addMessage("Nemáte dostatečné oprávnění pro požadovanou akci.");
      }
      $this->redirect('orders');
    }
    else {
      $form->populate(array('order_id' => $orderId));
      $this->view->form = $form;
    }
	}

	/**
	 * Vraci na sklad zbozi expedovane objednavky a nastavuje ji jako zavrenou
	 * Ma smysl u expedovanych objednavek, ktere byly doruceny na dobirku
	 */
	public function returnAction() {
		if(!$this->_request->getParam('id')) $this->_redirect('orders');

		$order   = new Orders_Order($this->_request->getParam('id'));
		try{
			$order->returnFromClosed();
			//pridavani aktuality
			$act = new Table_Actualities();
			$act->add("Vrátil na sklad objednávku č. ".$order->getNO());
			$this->_flashMessenger->addMessage("Objednávka byla vrácena na sklad.");
		} catch ( Exception $e ) {
			$this->_flashMessenger->addMessage("Objednávku se nepodařilo vrátit. <br>".$e->getMessage());
		}

		$this->_redirect('orders');
		$this->_helper->viewRenderer->setNoRender();
	}

	public function getHologramForm() {
		$form = new Meduse_Form('parts_box', 'Přiřadit hologramy');

		$select = new Meduse_Form_Element_Select('hologram_type');
		$select->setLabel('Druh hologramu');
		$select->addMultiOptions($this->_db->fetchPairs("SELECT id, name FROM holograms_types ORDER BY name"));
		$form->addElement($select);

		$element = new Meduse_Form_Element_Text('hologram_amount');
		$element->setRequired(true);
		$element->setLabel('Počet');
		$form->addElement($element);

		$element = new Meduse_Form_Element_Submit('Odeslat');
		$form->addElement($element);

		return $form;
	}

	public function assignHologramsAction() {

            if ($this->_request->getParam('submit') == 'Uložit') {
                $order = new Orders_Order($this->_request->getParam('order_id'));
                $part = new Parts_Part($this->_request->getParam('part_id'));
                $order->unassignAllHolograms($part);
                $order->assignHolograms($this->_request->getParam('holograms'), $part);
                $this->_redirect('orders/show-holograms/id/' . $order->getID());

            }

            if(!$this->_request->getParam('id')) {
                $this->_redirect('orders');
            } else {
                $order = new Orders_Order($this->_request->getParam('id'));
            }
            if(!$this->_request->getParam('part')) {
                $this->_redirect('orders/show-holograms/' . $order->getID());
            } else {
                $part = new Parts_Part($this->_request->getParam('part'));
            }
            $this->view->assigned = $order->getAssignedHolograms($part->getID());
            $this->view->holograms = $order->getAvailableHolograms($part->getID());
            $this->view->order = $order;
            $this->view->part = $part;
            $this->_helper->layout->setLayout('bootstrap-basic');
            $this->view->title = "Správa hologramů k objednávce a součásti";
	}

	public function showHologramsAction() {
            if(!$this->_request->getParam('id')) $this->_redirect('orders');
            $order = new Orders_Order($this->_request->getParam('id'));
            $this->view->order = $order;
            $this->view->parts = $order->getHologramProducts();


            foreach ($this->view->parts as $key => $p) {
                $part = new Parts_Part($p['id']);
                $this->view->parts[$key]['holograms'] = $order->getAssignedHolograms($part->getID());
            }

            $this->_helper->layout->setLayout('bootstrap-basic');
            $this->view->title = "Přehled hologramů k objednávce";
	}

	public function hologramsAction() {

            $form = new Holograms_FilterForm();

            $form->getElement('collection')->setValue($this->_request->getParam('collection'));
            $form->getElement('order_no')->setValue($this->_request->getParam('order_no'));
            $form->getElement('parts_id')->setValue($this->_request->getParam('parts_id'));
            $form->getElement('unassigned')->setValue($this->_request->getParam('unassigned'));

            $this->view->form = $form;

            $this->view->holograms = Holograms_Hologram::getAllHolograms(array(
                'collection' => $this->_request->getParam('collection'),
                'order_no' => $this->_request->getParam('order_no'),
                'parts_id' => $this->_request->getParam('parts_id'),
                'unassigned' => $this->_request->getParam('unassigned'),
            ));

            $importForm = new Holograms_ImportForm();
            $this->view->import_form = $importForm;

            $this->_helper->layout->setLayout('bootstrap-basic');
            $this->view->title = "Přehled všech hologramů";
	}

	public function newTemplateAction() {

    $this->view->title = "Nová šablona";

		if(!$this->_request->getParam('id')) $this->_redirect('orders');
		$idOrder = (int) $this->_request->getParam('id');
		$order = new Orders_Order($idOrder);

		$form = new Orders_TemplateForm;
		$form2 = new Addresses_Form();

		$tTempletes = new Table_OrdersTemplates;
		$tTempletes->mapFromDb($form, $idOrder);
		$addrId = $tTempletes->mapAddressFromDb($form2, $idOrder);
		$form->address->setValue($addrId);
		$form->addElements($form2->getElements());

		if($this->_request->isPost() && $form->isValid($this->_request->getParams())){

			$addrId = (int) $form->address->getValue();

			//update adresy podle id
			$data = array(
				'company' =>       $form->getValue("address_company"),
				'person' =>        $form->getValue("address_person"),
				'phone' =>         $form->getValue("address_phone"),
				'street' =>        $form->getValue("address_street"),
				'pop_number' =>    $form->getValue("address_pop_number"),
				'orient_number' => $form->getValue("address_orient_number"),
				'city' =>          $form->getValue("address_city"),
				'zip' =>           $form->getValue("address_zip"),
				'country' =>       $form->getValue("address_country"),
			);
			if($addrId){
				$this->_db->update('addresses', $data, "id = ".$addrId);
			} else {
				$addrId = $this->_db->insert('addresses', $data);
			}

			//vytvoreni nove sablony
			$row = $tTempletes->createRow();
			$row->id_orders          = $form->order->getValue();
			$row->id_addresses       = $addrId;
			$row->id_customers       = $order->getRow()->id_customers;
			$row->id_orders_carriers = $form->meduse_orders_carrier->getValue();
			$row->title              = $form->meduse_orders_name->getValue();
			$row->description        = $form->meduse_orders_desc->getValue();
			$row->purchaser			     = $form->meduse_orders_purchaser->getValue();
			$row->payment_method     = $form->meduse_orders_payment_method->getValue();
      $row->type               = $form->meduse_orders_type->getValue();
			$row->inserted           = new Zend_Db_Expr("NOW()");

			$row->save();

			$this->_redirect($this->view->url(array('action' => 'templates')));
		}

		$this->view->form = $form;
		$this->render('add');
	}

	public function templatesAction() {
		$this->_helper->layout->setLayout('bootstrap-basic');
                //$this->_helper->layout->setLayout('layout2');
		$this->view->title = "Šablony";

		$tTempletes = new Table_OrdersTemplates;
		$this->view->data = $tTempletes->getList();
	}

	public function addFromTemplateAction() {
		if (!$this->_request->getParam('id')) {
      $this->_redirect('orders');
    }
		$tTempletes = new Table_OrdersTemplates;
		$template = $tTempletes->find($this->_request->getParam('id'))->current();
		if (!$template) {
      $this->_redirect('orders');
    }
    $orders = new Table_Orders();
    $no = $orders->getNextNo(Parts_Part::WH_TYPE_PIPES);

		$id = $orders->addOrder(
			$no,
			$template->title,
			null,
			$template->description,
			$template->id_orders_carriers,
			null,
			$template->purchaser,
			null,
			null,
			'CZK',
			$template->payment_method,
      $template->type,
      1
		);

		$order = new Orders_Order($id);

    if ($order->isService()) {
      $tServices = new Table_OrdersServices();
      $select = $tServices->select()->where('id_orders = ?', $template->id_orders);
      $services = $this->_db->fetchAll($select);
      foreach ($services as $service) {
        $service["id_orders"] = $id;
        unset($service["id"]);
        $tServices->insert($service);
      }
    }
    else {
      $tOrdersProduct = new Table_OrdersProducts();
      $select = $tOrdersProduct
          ->select()
          ->where('id_orders = ?', $template->id_orders);
      $products = $this->_db->fetchAll($select);
      foreach ($products as $product) {
          $product["id_orders"] = $id;
          unset($product["id"]);
          unset($product["last_change"]);
          $tOrdersProduct->insert($product);
      }
    }
    if ($template->id_addresses) {
			$order->getRow()->id_addresses = $template->id_addresses;
			$order->getRow()->id_customers = $template->id_customers;
			$order->getRow()->save();
		}
    if ($order->isService()) {
      $this->_redirect('orders/detail/id/' . $order->getID());
    }
    else {
      $this->_redirect('orders/select-products/order/' . $order->getID());
    }
	}



        public function deleteTemplateAction() {
            $tTempletes = new Table_OrdersTemplates();
            $template = $tTempletes->find($this->_request->getParam('id'))->current();
            $template->delete();
            $this->_redirect($this->view->url(array('action' => 'templates')));
        }

        public function compareAction()
        {
            /**
             * Layout
             */
            $this->_helper->layout->setLayout('bootstrap-basic');


            /**
             * Set title
             */
            $this->view->title = "Porovnání objednávek STRATEGICKÉ vs. PŘEDBĚŽNÉ";


            /**
             * Function - Build data array
             *
             * @param array $orders_data_arr array of products
             * @return array
             */
            function buildDataArray($orders_data_arr)
            {
                $arrProducts = array();

                if(count($orders_data_arr) > 0) {
                    foreach($orders_data_arr as $ord)
                    {
                        $tOrders = new Table_Orders();
                        $items_data = $tOrders->getSortedOrderParts($ord['id']);
                        $items_data = $items_data->toArray();

                        if(count($items_data) > 0) {
                            foreach($items_data as $item) {
                                if(!array_key_exists($item['id_products'], $arrProducts)) {
                                    $arrProducts[$item['id_products']] = array(
                                            'p_id' => $item['id_products'],
                                            'p_cnt' => (int)$item['amount'],
                                            'p_name' => $item['name'],
                                        );
                                }
                                else {
                                    $arrProducts[$item['id_products']]['p_cnt'] += $item['amount'];
                                }
                            }
                        }
                    }
                }

                return $arrProducts;
            }


            /**
             * Load data of orders
             */
            // strategic
            $tOrders = new Table_Orders();
            $str_orders = $tOrders->fetchAll("status = 'strategic'");
            $str_orders_data = $str_orders->toArray();

            // pre-orders
            $tOrders = new Table_Orders();
            $pre_orders = $tOrders->fetchAll("status = 'new'");
            $pre_orders_data = $pre_orders->toArray();


            /**
             * Build data into array
             */
            $arrStrategicProducts = buildDataArray($str_orders_data);
            $arrPreOrderProducts  = buildDataArray($pre_orders_data);


            /**
             * Get different items
             */
            $arrStrategicProductsAlone = array_diff_assoc($arrStrategicProducts, $arrPreOrderProducts);
            $arrPreOrderProductsAlone = array_diff_assoc($arrPreOrderProducts, $arrStrategicProducts);


            /**
             * Add missing items for both array
             * both array must have the same length
             */
            // add missing items into strategic array
            foreach($arrPreOrderProductsAlone as $key => $val)
            {
                $arrStrategicProducts[$key] = array('p_id' => $val['p_id'], 'p_cnt' => 0, 'p_name' => $val['p_name']);
            }

            // add missing items into preOrder array
            foreach($arrStrategicProductsAlone as $key => $val)
            {
                $arrPreOrderProducts[$key] = array('p_id' => $val['p_id'], 'p_cnt' => 0, 'p_name' => $val['p_name']);
            }


            /**
             * Sort array's assoc by key
             */
            ksort($arrStrategicProducts);
            ksort($arrPreOrderProducts);


            /**
             * To view
             */
            $this->view->strategic_products = $arrStrategicProducts;
            $this->view->preorder_products = $arrPreOrderProducts;


        }


        public function ajaxGetStrategicOrdersAction()
        {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $id = $this->_getParam('id');

            $html_content = '';

            if(!empty($id)) {
                $tOrders = new Table_Orders();
                $str_orders = $tOrders->fetchAll("status = 'strategic'");
                $str_orders_data = $str_orders->toArray();

                if(count($str_orders_data) > 0) {
                    $i = 0;
                    foreach($str_orders_data as $order)
                    {
                        $tOrders = new Table_Orders();
                        $items_data = $tOrders->getSortedOrderParts($order['id']);
                        $items_data = $items_data->toArray();

                        if(count($items_data)) {
                            foreach($items_data as $product)
                            {
                                // is requested product
                                if($product['id_products'] == $id) {
                                    // make html body
                                    if(strlen($str_orders_data[$i]['title']) > 14) {
                                        $orderTitle = substr($str_orders_data[$i]['title'],0,14).'...';
                                    } else {
                                        $orderTitle = $str_orders_data[$i]['title'];
                                    }

                                    $html_content .= '<input class="item-change" data=\'{"pid": "' . $id . '", "oid": "' . $str_orders_data[$i]['id'] . '", "newVal": ""}\' style="width: 40px;" type="text" value="' . $product['amount'] . '"> &nbsp;<a href="/orders/detail/id/' . $str_orders_data[$i]['id'] . '" title="'.$str_orders_data[$i]['title'].'" target="_blank">č.' . $str_orders_data[$i]['no'] . ' - ' . $orderTitle . '</a><br>';
                                }
                            }
                        }

                        $i++;
                    }
                }

                $html_content .= (empty($html_content)) ? '<span style="color: red;">Položka není v žádné objednávce</span>' : null;

                // add submit button
                $html_content .= '<button class="btn confirm-btn" data-product="' . $id . '">Zavřít</button>';

                echo $html_content;
            }
        }

        public function ajaxSetStrategicOrdersAmountAction()
        {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $params = (object) $this->_getAllParams();

            if(!empty($params->pid) && !empty($params->oid) && !empty($params->newVal)) {
                $tOrdersProducts = new Table_OrdersProducts();
                $tOrdersProducts->updateAmount($params);

                $tOrders = new Table_Orders();
                $str_orders = $tOrders->fetchAll("status = 'strategic'");
                $str_orders_data = $str_orders->toArray();

                $new_amount = 0;

                if(count($str_orders_data) > 0) {
                    $i = 0;
                    foreach($str_orders_data as $order)
                    {
                        $tOrders = new Table_Orders();
                        $items_data = $tOrders->getSortedOrderParts($order['id']);
                        $items_data = $items_data->toArray();

                        if(count($items_data)) {
                            foreach($items_data as $product)
                            {
                                // is requested product
                                if($product['id_products'] == $params->pid) {
                                    $new_amount += $product['amount'];
                                }
                            }
                        }
                        $i++;
                    }
                }



                $this->_helper->json(array('result' => 'ok', 'amount' => $new_amount));
            }
        }

		public function notifyAction() {
			$id_order = $this->_request->getParam('order_id');
			$id_customer = $this->_request->getParam('customer_id');
			$email_subject = $this->_request->getParam('email_subject');
			$email_body = $this->_request->getParam('email_body');
			$email_to = $this->_request->getParam('email_to');

			if ($id_order && $id_customer) {
				$tCustomers = new Table_Customers();
				$tCustomers->update(array('email' => $email_to), 'id = ' . $tCustomers->getAdapter()->quote($id_customer) . ' AND email IS NULL');

				$email = new Table_Emails();
				$email->sendEmail(Table_Emails::EMAIL_ORDER_EXPEDITION_CUSTOMER_NOTICE, array(
					'orderId' => $id_order,
					'subject' => $email_subject,
					'body' => $email_body,
					'to' => $email_to,
				));
			}
			$this->_redirect($this->view->url(array('action' => null)));
		}

        public function getCalculatorDataAction() {
          $this->_helper->layout()->disableLayout();
          $this->_helper->viewRenderer->setNoRender(true);
          $order = new Orders_Order($this->_request->getParam('order'));
          $data = $order->getOrderItemsPriceCount();
          $rate = $order->getExpectedPaymentRate();
          $currency = $order->getExpectedPaymentCurrency();
          $price = $data['CntEur'];
          $this->_helper->json(array('result' => array(
            'price_czk' => Meduse_Currency::format($price * $rate),
            'price_eur' => Meduse_Currency::format($price),
            'rate' => Meduse_Currency::format($rate),
            'currency' => $currency,
          )));
        }


		/**
		 * TODO: tuto funkci pridat/presunout do CRONu
		 */
		public function mailReportAction() {

			// potvrzene a otevrene objednavky
			$tOrders = new Table_Orders();
			$select = $tOrders->select()->where('status = :confirmed OR status = :open');
			$orders = $tOrders->getAdapter()->fetchAll($select, array(
				'confirmed' => Table_Orders::STATUS_CONFIRMED,
				'open' => Table_orders::STATUS_OPEN,
			));

			// soucasti pro kazdou objednavku
			foreach ($orders as $order) {
				$report = new Orders_PartsReport();
				$report->setOrder(Orders_PartsReport::ORDER_PARTS);
				$report->setFilter(array(
					$order['id'] => 'on',
					'coll' => array('1', '2', '3', '4'),
					'ctg' => 'null',
				));
				$dataOrders[$order['id']]['order'] = $order;
				$dataOrders[$order['id']]['report'] = $report->getData();
				$aOrders[$order['id']] = 'on';
			}
			$this->view->dataOrders = $dataOrders;

			// celkem - soucasti pro vsechny objednavky
			$aOrders['coll'] = array('1', '2', '3', '4');
			$aOrders['ctg'] = 'null';
			$report = new Orders_PartsReport();
			$report->setOrder(Orders_PartsReport::ORDER_PARTS);
			$report->setFilter($aOrders);
			$this->view->dataAll = $report->getData();

			// sestaveni mailu a odeslani
			$email_body = $this->view->render('orders/mail-report.phtml');
			$email = new Table_Emails();
			$email->sendEmail(Table_Emails::EMAIL_MISSING_PARTS_DAILY_SUMMARY, array(
				'body' => $email_body,
			));

			// nevykreslujeme zadnou sablonu
			$this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
		}


    public function changeAddressAction() {

      $orderId = $this->_request->getParam('order');
      $addressId = $this->_request->getParam('address');
      $type = $this->_request->getParam('type', 'shipping');

      if (is_null($orderId)) {
        $this->_flashMessenger->addMessage('Nebylo zadáno číslo objednávky.');
      }
      elseif (is_null($addressId)) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID adresy.');
      }
      else {
        $order = new Orders_Order($orderId);
        if ($type == 'shipping') {
          $address = new Addresses_Address($addressId);
          $order->setAddress($address);
        }
        else {
          $customer = $order->getCustomer(TRUE);
          $customer->setBillingAddress($addressId);
        }
        $this->_flashMessenger->addMessage('Adresa byla změněna');
      }
      $destination = $this->_request->getParam('destination', 'orders');
      $this->redirect($destination);
			$this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(TRUE);
    }

  public function packagingSaveAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    if ($this->_request->isPost()) {
      try {
        $ordersId = $this->_request->getParam('id_orders');
        $order = new Orders_Order($ordersId);
        $count = $this->_request->getParam('count', 0);
        for ($it = 0; $it < $count; $it++) {
          $id = $this->_request->getParam('id_' . $it);
          $remove = $this->_request->getParam('remove_' . $it) == 'y';
          $data = [
            'type' => $this->_request->getParam('type_' . $it),
            'width' => (int) $this->_request->getParam('width_' . $it),
            'depth' => (int) $this->_request->getParam('depth_' . $it),
            'height' => (int) $this->_request->getParam('height_' . $it),
            'weight_brutto' => (float) $this->_request->getParam('weight_' . $it),
          ];
          if (!$id && $remove) {
            // Nova polozka oznacena ihned ke smazani.
            continue;
          }
          elseif ($id && $remove) {
            // Existujici polozka oznacena ke smazani.
            $order->removePackaging($id);
          }
          elseif ($id) {
            // Existujici polozka.
            $order->updatePackaging($id, $data);
          }
          else {
            // Nova polozka.
            $order->insertPackaging($data);
          }
        }
        $table = new Table_Emails();
        $table->sendEmail(Table_Emails::EMAIL_NOTICE_ORDER_PACKAGING, [
          'order' => $order, 'type' => 'items',
        ]);
        $this->_flashMessenger->addMessage('Položky balení objednávky byly uloženy.');
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
      }
      $this->redirect('/orders/detail/id/' . $ordersId . '#packaging');
    }
    die('Nezadano ID objednavky');
  }

  public function saveCooDatesAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    if ($this->_request->isPost()) {
      $orderId = $this->_request->getParam('id_orders');
      $confirm = $this->_request->getParam('coo_date_confirm');
      $validity = $this->_request->getParam('coo_date_validity');
      $dateConfirm = $confirm ? new Meduse_Date($confirm, 'd.m.Y') : NULL;
      $dateValidity = $validity ? new Meduse_Date($validity, 'd.m.Y') : NULL;
      $order = new Orders_Order($orderId);
      $order->setCOODates($dateConfirm, $dateValidity);
      $this->_flashMessenger->addMessage('COO data byla nastavena');
      $this->redirect('orders/detail/id/' . $orderId . '#infos');
    }
  }

  public function trackingSaveAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);
    if ($this->_request->isPost()) {
      try {
        $ordersId = $this->_request->getParam('tracking_id_orders');
        $order = new Orders_Order($ordersId);
        $count = $this->_request->getParam('count_tracking', 0);
        for ($it = 0; $it < $count; $it++) {
          $id = $this->_request->getParam('tracking_id_' . $it);
          $remove = $this->_request->getParam('tracking_remove_' . $it) == 'y';
          $data = [
            'code' => $this->_request->getParam('tracking_code_' . $it),
            'link' => $this->_request->getParam('tracking_link_' . $it),
          ];
          if (!$id && $remove) {
            // Nova polozka oznacena ihned ke smazani.
            continue;
          }
          elseif ($id && $remove) {
            // Existujici polozka oznacena ke smazani.
            $order->removeTrackingCode($id);
          }
          elseif ($id) {
            // Existujici polozka.
            $order->updateTracingCode($id, $data);
          }
          else {
            // Nova polozka.
            $order->insertTrackingCode($data);
          }
        }
        $table = new Table_Emails();
        $table->sendEmail(Table_Emails::EMAIL_NOTICE_ORDER_PACKAGING, [
          'order' => $order,
          'type' => 'tracking'
        ]);
        $this->_flashMessenger->addMessage('Tracking kódy objednávky byly uloženy.');
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
      }
      $this->redirect('/orders/detail/id/' . $ordersId . '#packaging');
    }
    die('Nezadano ID objednavky');
  }


  public function ajaxHsSaveAction() {

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $orderId = $this->_request->getParam('orderId');
    $productId = $this->_request->getParam('productId');
    $hsCode = $this->_request->getParam('hsCode');

    if ($orderId && $productId && $hsCode) {
      try {
        $db = Zend_Registry::get('db');
        $tOrdersProducts = new Table_OrdersProducts();
        $tOrdersProducts->update(array('hs_code' => $hsCode),
          'id_products = ' . $db->quote($productId)
          . ' AND id_orders = ' . $db->quote($orderId));
        $this->_helper->json(array(
          'status' => 'ok',
          'message' => 'HS kód byl v objednávce změněn.',
        ));
      }
      catch (Exception $e) {
        $this->_helper->json(array(
          'status' => 'fail',
          'message' => $e->getMessage(),
        ));
      }
    }
    else {
      $this->_helper->json(array(
        'status' => 'fail',
        'message' => 'Nebyly zadány povinné parametry.',
      ));
    }
  }

  public function availableHsAction() {

    /** @var \Zend_Db_Adapter_Pdo_Mysql $db */
    $db = Zend_Registry::get('db');

    $form = new Orders_AvailableHsCodesForm('available_hs_codes');

    $redirect = false;

    if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {

      $data = $form->getValidValues($this->_request->getParams());
      $hsCode = $data['code'];

      $sql = "SELECT * FROM hs_codes WHERE code = :code";
      if ($db->query($sql, [':code' => $hsCode])->fetch(Zend_Db::FETCH_ASSOC)) {
        unset($data['code']);
        $db->update('hs_codes', $data, 'code = ' . $db->quote($hsCode));
        $this->_flashMessenger->addMessage('HS kód byl změněn.');
      }
      else {
        $db->insert('hs_codes', $data);
        $this->_flashMessenger->addMessage('HS kód byl přidán.');
      }
      $redirect = true;
    }

    elseif ($hsCode = $this->_request->getParam('delete')) {
      $db->delete('hs_codes', 'code = ' . $db->quote($hsCode));
      $this->_flashMessenger->addMessage('HS kód byl odstraněn.');
      $redirect = true;
    }

    elseif ($hsCode = $this->_request->getParam('edit')) {
      $sql = "SELECT * FROM hs_codes WHERE code = :code";
      if ($result = $db->query($sql, [':code' => $hsCode])->fetch(Zend_Db::FETCH_ASSOC)) {
        $form->populate($result);
      }
    }

    if ($redirect) {
      $this->redirect($this->view->url(['delete' => null, 'edit' => null]));
    }

    $sql = "SELECT * FROM hs_codes ORDER BY weight, code";
    $hsCodes = $db->query($sql)->fetchAll(Zend_Db::FETCH_ASSOC);

    $this->view->title = 'Seznam dostupných HS kódů';
    $this->view->form = $form;
    $this->view->hs_codes = $hsCodes;

  }

  public function ajaxChangeExpeditionDateAction() {

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $orderId = (int) $this->_request->getParam('orderId');
      $expDateStr = (string) $this->_request->getParam('dateExp');

      try {
        $expDate = new Meduse_Date($expDateStr, 'd.m.Y');
        $order = new Orders_Order($orderId);
        $order->setDateExp($expDate);
        $this->_helper->json([
          'status' => TRUE,
          'orderId' => $order->getID(),
          'dateExp' => Meduse_Date::dbToForm($order->getDateExp()),
          'message' => 'Datum expedice bylo změněno.',
        ]);
      }
      catch (Exception $e) {
        $this->_helper->json([
          'status' => false,
          'orderId' => $orderId,
          'dateExp' => $expDateStr,
          'message' => $e->getMessage(),
        ]);
      }
      exit;
  }
  
  private function getDataTabs($active = NULL, $filterTabs = []): array {
  
    $tabs = [];
  
    if (!$active) {
      $active = empty($filterTabs) ? 'open' : reset($filterTabs);
    }
    
    $availableTabs = array(
      'strategic' => array(
        'href' => '?tab=strategic',
        'title' => 'Strategické',
        'active' => $active == 'strategic',
      ),
      'offer' => array(
        'href' => '?tab=offer',
        'title' => 'Nabídky',
        'active' => $active == 'offer',
      ),
      'new' => array(
        'href' => '?tab=new',
        'title' => 'Předběžné',
        'active' => $active == 'new',
      ),
      'confirmed' => array(
        'href' => '?tab=confirmed',
        'title' => 'Potvrzené',
        'active' => $active == 'confirmed',
      ),
      'open' => array(
        'href' => '?tab=open',
        'title' => 'Otevřené',
        'active' => $active == 'open',
      ),
      'total' => array(
        'href' => '?tab=total',
        'title' => 'Celkové počty',
        'active' => $active == 'total',
      ),
      'closed' => array(
        'href' => '?tab=closed',
        'title' => 'Expedované',
        'active' => $active == 'closed',
      ),
      'trash' => array(
        'href' => '?tab=trash',
        'title' => 'Koš',
        'active' => $active == 'trash',
      ),
    );
  
    // Kontrola opravneni
    if (Utils::isDisallowed('orders/special/strategic')){
      unset($availableTabs['strategic']);
    }
    if (Utils::isDisallowed('orders/special/all-sets-counts')){
      unset($availableTabs['total']);
    }
    if (Utils::isDisallowed('orders/special/trash')){
      unset($availableTabs['trash']);
    }
  
    if (empty($filterTabs)) {
      return $availableTabs;
    }
  
    foreach ($availableTabs as $key => $tab) {
      if (in_array($key, $filterTabs)) {
        $tabs[$key] = $tab;
      }
    }
  
    return $tabs;
  }

  public function feesListAction() {
      $tFees = new Table_ApplicationFees();
      $fees = $tFees->list();
      $this->view->title = 'Seznam fixních poplatků';
      $this->view->fees = $fees;
  }

  /**
   * @throws Zend_Form_Exception
   */
  public function feesAddAction() {
    $form = new Orders_FeeForm();
    $form->setLegend('Vložení nového fixního poplatku');

    if ($this->_request->isPost()) {
      $this->feesSave($form);
    }

    $this->view->form = $form;
    $this->renderScript('orders/fees-add-edit.phtml');
  }

  /**
   * @throws Zend_Form_Exception
   */
  public function feesEditAction() {

    if (!$feeId = (int) $this->_request->getParam('id', 0)) {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID poplatku.');
      $this->redirect('/orders/fees-list');
    }

    $form = new Orders_FeeForm();
    $form->setLegend('Editace fixního poplatku');

    if ($this->_request->isPost()) {
      $feeData = $this->feesSave($form);
    }
    else {
      $row = null;
      $tFees = new Table_ApplicationFees();
      try {
        $row = $tFees->find($feeId)->current();
      }
      catch (Zend_Db_Table_Exception $e) {
        $this->_flashMessenger->addMessage('Nepodařilo se poplatek načíst. Více info v logu.');
        Utils::logger()->err($e->getMessage());
        $this->redirect('/orders/fees-list');
      }

      if (!$row) {
        $this->_flashMessenger->addMessage('Poplatek s ID = ' . $feeId . ' neexistuje.');
        $this->redirect('/orders/fees-list');
      }

      $feeData = $row->toArray();
    }

    $form->populate($feeData);

    $this->view->form = $form;
    $this->renderScript('orders/fees-add-edit.phtml');
  }
  public function feesEnableAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    if (!$feeId = (int) $this->_request->getParam('id', 0)) {
      $message = 'Nebylo zadáno ID poplatku.';
    }
    else {
      $enable = (bool) $this->_request->getParam('status', 1);
      $tFees = new Table_ApplicationFees();
      if ($tFees->setEnabled($feeId, $enable)) {
        $message = 'Poplatek byl ' . ($enable ? 'aktivován' : 'deaktivován') . '.';
      }
      else {
        $message = 'Poplatek se nepodařilo ' . ($enable ? 'aktivovat' : 'deaktivovat') . '.';
      }
    }
    $this->_flashMessenger->addMessage($message);
    $this->redirect('/orders/fees-list');
  }

  /**
   * @throws Zend_Form_Exception
   */
  private function feesSave(Orders_FeeForm $form): array
  {
      $params = $this->_request->getParams();
      if (!$form->isValid($params)) {
        return $params;
      }
      $data = $form->getValidValues($params);
      $data['is_coo'] = (int) ($data['is_coo'] === 'y');
      $data['is_iv'] = (int) ($data['is_iv'] === 'y');
      $data['enabled'] = (int) ($data['enabled'] === 'y');

      $tFees = new Table_ApplicationFees();
      if ($data['id']) {
        $id = (int) $data['id'];
        unset($data['id']);
        $done = (bool) $tFees->update($data, 'id = ' . $tFees->getAdapter()->quote($id));
        $message = 'Poplatek byl aktualizován.';
      }
      else {
        unset($data['id']);
        $done = (bool) $tFees->insert($data);
        $message = 'Poplatek byl vložen.';
      }

      if ($done) {
        $this->_flashMessenger->addMessage($message);
        $this->redirect('/orders/fees-list');
      }

      return $form->getValidValues($params);
  }

}
