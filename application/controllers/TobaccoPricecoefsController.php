<?php

/**
 * Koeficienty na množstevní slevu.
 */
class TobaccoPricecoefsController extends Meduse_Controller_Action {
    
  public function init() {
    $this->_helper->layout->setLayout('bootstrap-tobacco');
  }

  /**
   * Zobrazuje skupinu množstevní slevy, její hranice a možné operace s nimi.
   */
  public function indexAction() {
    $tCoefs = new Table_PricesCoefs();
    $this->view->coefs = $tCoefs->getPoints();
  }

  
  public function editAction() {
    $form = new Tobacco_Pricelists_CoefsForms();
    $id = $this->getRequest()->getParam('id');
    if ($id) {
      $tCoefs = new Table_PricesCoefs();
      $data = $tCoefs->getPoint($id);
      if ($data) {
        $form->populate($data);
        $form->setLegend('Editace bodu množstevní slevy');
        $this->view->id = $id;
      }
      else {
        $this->_flashMessenger->addMessage('Bod nenalezen.');
        $this->_redirect('/tobacco-pricecoefs/');
      }
    }
    else {
      $form->setLegend('Přidání bodu množstevní slevy');
    }    
    $this->view->form = $form;
  }
  
  public function saveAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    if ($this->getRequest()->isPost()) {
      $data = $this->getRequest()->getParams();
      $form = new Tobacco_Pricelists_CoefsForms();
      $tCoef = new Table_PricesCoefs();
      if($form->isValid($data)) {
        if ($data['id']) {
          $point = $tCoef->getPoint($data['id']);
          if ($point) {
            $tCoef->update(array(
                'point' => $data['point'], 
                'id_pricelists_b2b' => $data['pricelist_b2b'],
                'id_pricelists_b2c' => $data['pricelist_b2c'],
            ), "id = " . $data['id']);            
            $this->_redirect('/tobacco-pricecoefs');
          }
          else {
            $this->_flashMessenger->addMessage('Data nebyla upravena.');
            $this->_redirect('/tobacco-pricecoefs/');
          }
        } else {          
          $point = $tCoef->findPoint($data['point'], true);
          if (!$point) {
            $tCoef->insert(array(
              'point' => $data['point'], 
              'id_pricelists_b2b' => $data['pricelist_b2b'],
              'id_pricelists_b2c' => $data['pricelist_b2c'],
            ));
            $this->_flashMessenger->addMessage('Bod byl úspěšně přidán.');
          } else {
            $this->_flashMessenger->addMessage('Bod již existuje.');
          }  
          $this->_redirect('/tobacco-pricecoefs/');
        }
      } else {
        $this->_flashMessenger->addMessage('Odeslaná data nejsou platná.');
        $this->_redirect('/tobacco-pricecoefs/');
      }
    }
  }
  
  public function deleteAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    $request = $this->getRequest();
    if ($request->isPost() && !is_null($id = $request->getParam('id', null))) {
      $tCoef = new Table_PricesCoefs();
      $tCoef->delete('id = ' . $id);
      $this->_flashMessenger->addMessage('Bod byl odebrán.');
    }
    $this->_redirect('/tobacco-pricecoefs/');
  }
}