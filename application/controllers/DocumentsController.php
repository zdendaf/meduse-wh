<?php

class DocumentsController extends Meduse_Controller_Action {

  public function init() {
    $this->_helper->layout->setLayout('bootstrap-basic');
    $this->view->title = "Procesy (dokumentace)";
  }

  public function indexAction() {
    $this->view->docs = Documents::findAll();
  }

  public function addAction() {
    $this->view->title = 'Vložení nového dokumentu';
    $this->view->form = Documents::getForm($this->view->title);
    if ($this->_request->isPost()) {
      if (!$this->saveDocument()) {
        $this->view->form->populate($this->_request->getParams());
      }
    }
    $this->renderScript('documents/form.phtml');
  }

  private function checkId() {
    if (!$id = $this->_request->getParam('id')) {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID dokumentu.');
      $this->redirect($this->view->url(['action' => 'index', 'id' => NULL]));
    }
    return $id;
  }

  public function downloadAction() {
    $this->_helper->viewRenderer->setNoRender();
    if (!$fid = $this->_request->getParam('fid')) {
      $this->_flashMessenger->addMessage('Nebylo zadano FID souboru.');
      $this->redirect($this->view->url(['action' => 'index', 'fid' => NULL]));
    }
    if (!$fileInfo = Documents::getFileInfo($fid)) {
      $this->_flashMessenger->addMessage("Data k souboru FID = $fid nebyla nenalazena.");
      $this->redirect($this->view->url(['action' => 'index', 'fid' => NULL]));
    }
    if (!file_exists($fileInfo['filepath'])) {
      $this->_flashMessenger->addMessage("Soubor '{$fileInfo['filepath']}' nebyl nalezen.");
      $this->redirect($this->view->url(['action' => 'index', 'fid' => NULL]    ));
    }
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($fileInfo['filename']) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($fileInfo['filepath']));
    flush(); // Flush system output buffer
    readfile($fileInfo['filepath']);
    exit(200);
  }

  public function editAction() {
    $id = $this->checkId();
    $this->view->title = 'Editace infa o dokumentu';
    $doc = new Documents_Document($id);
    $form = $doc->getEditForm($this->view->title);
    if ($this->_request->isPost()) {
      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        $data = $form->getValidValues($params);
        $doc->setTitle($data['title'], FALSE)
          ->setDescription($data['description'], FALSE)
          ->save();
        $this->_flashMessenger->addMessage('Změny byly uloženy.');
        $this->redirect($this->view->url(['action' => 'detail']));
      }
    }
    $this->view->form = $form;
    $this->renderScript('documents/form.phtml');
  }

  public function addVersionAction() {
    $id = $this->checkId();
    $this->view->title = 'Vložení nové verze';
    $doc = new Documents_Document($id);
    $form = $doc->getAddVersionForm($this->view->title);
    if ($this->_request->isPost()) {
      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        $this->saveFile($id);
        $this->redirect($this->view->url(['action' => 'detail']));
      }
    }
    $this->view->form = $form;
    $this->renderScript('documents/form.phtml');
  }
  
  protected function saveDocument() {
    $form = Documents::getForm();
    try {
      $params = $this->_request->getParams();
      if ($form->isValid($params)) {
        $data = $form->getValidValues($params);
        $id = $data['id'] ? (int) $data['id'] : NULL;
        $title = $data['title'];
        $description = $data['description'] ?: NULL;
        $this->saveFile($id, $title, $description);
        $this->redirect($this->view->url(['action' => 'index']));
        return TRUE;
      }
      else {
        $this->_flashMessenger->addMessage('Zadaná data nejsou validní.');
        return FALSE;
      }
    }
    catch (Zend_Form_Exception $e) {
      Utils::logger()->err($e->getMessage());
      $this->_flashMessenger->addMessage('Něco se nepovedlo. Viz systémový log.');
      return FALSE;
    }
  }

  protected function saveFile($id = NULL, $title = NULL, $description = NULL) {
    $result = Documents::filesUpload($id, $title, $description);
    if (is_array($result)) {
      foreach ($result as $message) {
        $this->_flashMessenger->addMessage($message);
      }
    }
    if ($result === TRUE) {
      $this->_flashMessenger->addMessage('Dokument byl uložen.');
    }
  }

  public function detailAction() {
    if (!$id = $this->_request->getParam('id')) {
      $this->_flashMessenger->addMessage('Nebylo zadáno ID dokumentu.');
      $this->redirect($this->view->url(['action' => 'index', 'id' => NULL]));
    }
    $doc = new Documents_Document($id);
    $this->view->title = $doc->getTitle();
    $this->view->description = $doc->getDescription();
    $this->view->user_name = $doc->getUserName();
    $this->view->last_version = $doc->getLastVersion();
    $this->view->files = $doc->getFiles();
    $this->view->files_count = count($this->view->files);
  }

  public function deleteAction() {
    $id = $this->checkId();
    $doc = new Documents_Document($id);
    $undo = (bool) $this->getParam('undo', 0);
    if ($undo) {
      $message = "Dokument '{$doc->getTitle()}' byl obnoven.";
      $doc->setDeleted(FALSE);
    }
    else {
      $doc->setDeleted(TRUE);
      $message = "Dokument '{$doc->getTitle()}' byl smazán. <a href='{$this->view->url(['undo' => 1])}'>Vrátit</a>";
    }
    $this->_flashMessenger->addMessage($message);
    $this->redirect($this->view->url(['action' => 'index', 'id' => NULL, 'undo' => NULL]));
  }
}