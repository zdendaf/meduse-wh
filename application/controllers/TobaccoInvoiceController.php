<?php

  class TobaccoInvoiceController extends Meduse_Controller_Action {

    protected $db = NULL;

    public function init() {
      $this->db = Zend_Registry::get('db');
      $this->_helper->layout->setLayout('bootstrap-tobacco');
    }

    public function indexAction() {
      $filter = new Invoice_Filter();
      $filter->setAction($this->view->url(array('page' => null)));

      $tInvoices = new Table_Invoices();

      $acl = Zend_Registry::get('acl');

      if ($this->_request->getParam('new')) {
        Zend_Registry::get('invoice_filter')->title = null;
        Zend_Registry::get('invoice_filter')->customer_type = null;
        Zend_Registry::get('invoice_filter')->no = null;
        Zend_Registry::get('invoice_filter')->type = null;
        Zend_Registry::get('invoice_filter')->region = null;
        Zend_Registry::get('invoice_filter')->language = null;
        Zend_Registry::get('invoice_filter')->inserted_from = null;
        Zend_Registry::get('invoice_filter')->inserted_to = null;
        $this->redirect($this->view->url(array('page' => null, 'new' => null)));
      }

      $invoice_filter = Zend_Registry::get('invoice_filter');
      if ($this->_request->isPost()) {
        $invoice_filter->title         = $this->_request->getParam('title');
        $invoice_filter->customer_type = $this->_request->getParam('customer_type');
        $invoice_filter->no            = $this->_request->getParam('no');
        $invoice_filter->type          = $this->_request->getParam('type');
        $invoice_filter->region        = $this->_request->getParam('region');
        $invoice_filter->language      = $this->_request->getParam('language');
        $invoice_filter->inserted_from = $this->_request->getParam('inserted_from');
        $invoice_filter->inserted_to   = $this->_request->getParam('inserted_to');
        Zend_Registry::set('invoice_filter', $invoice_filter);
      }

      //uchování dat ve formuláři
      $filter->isValid(array(
          'title'         => $invoice_filter->title,
          'customer_type' => $invoice_filter->customer_type,
          'no'            => $invoice_filter->no,
          'type'          => $invoice_filter->type,
          'region'        => $invoice_filter->region,
          'language'      => $invoice_filter->language,
          'inserted_from' => $invoice_filter->inserted_from,
          'inserted_to'   => $invoice_filter->inserted_to,
      ));
      $this->view->filter = $filter;

      $select = $tInvoices->getFilteredInvoicesSelect([
        'wh' => Table_TobaccoWarehouse::WH_TYPE,
        'only_numbered' => ! Utils::isAllowed('tobacco-invoice/show-non-numbered'),
        'only_closed' => Utils::isDisallowed('tobacco-invoice/download-not-closed'),
        'filter' => $invoice_filter,
      ]);
      $gridArray = array(
        'css_class' => 'table table-hover',
        'curr_items_per_page' => 25,
        'curr_sort' => 'inserted',
        'curr_sort_order' => ZGrid::ORDER_DESC,
        'columns' => array(
          'order_no' => array(
            'header' => _('Obj. č.'),
            'sorting' => true,
            'renderer' => 'url',
            'url' => $this->view->url(array(
              'module' => 'default', 'controller' => 'tobacco-orders',
              'action' => 'detail', 'id' => '{order_id}', 'sort' => null, 'sorder' => null
            )) . '#invoices',
          ),
          'title' => array(
            'header' => _('Název'),
            'sorting' => true,
            'renderer' => 'url',
            'url' => $this->view->url(array(
              'module' => 'default', 'controller' => 'tobacco-orders',
              'action' => 'detail', 'id' => '{order_id}', 'sort' => null, 'sorder' => null
            )) . '#invoices',
          ),
          'invoiced_total' => array(
            'header' => _('Částka'),
            'header_class' => 'text-right',
            'sorting' => FALSE,
            'renderer' => 'currency',
            'currency_options' => array(
              'currency_value' => 'invoiced_total',
              'currency_symbol' => 'currency',
              'rounding_precision' => 2,
            ),
            'css_class' => 'text-right',
          ),
          'customer_type' => array(
            'header' => _('ZK'),
            'sorting' => true,
          ),
          'invoice_no' => array(
            'header' => _('Číslo'),
            'sorting' => true,
          ),
          'invoice_type' => array(
            'header' => _('Typ faktury'),
            'sorting' => true,
          ),
          'region' => array(
            'header' => _('Region'),
            'sorting' => true,
          ),
          'language' => array(
            'header' => _('Jazyk'),
            'sorting' => true,
          ),
          'inserted' => array(
            'header' => _('Vloženo'),
            'sorting' => true,
            'renderer' => 'date'
          ),
          'locked' => array(
            'header' => '',
            'renderer' => 'condition',
            'condition_value' => 'n',
            'condition_true' => array(
              'renderer' => 'Action',
              'type' => 'edit',
              'url' => $this->view->url(array(
                'module' => 'default' , 'controller' => 'tobacco-invoice' ,
                            'action' => 'prepare', 'id' => '{id}' , 'sort' => null, 'sorder' => null
              ))
            ), null, true
          ),
          'PDF' => array(
            'header' => 'Faktura',
            'renderer' => 'url',
            'url' => $this->view->url(array(
              'module' => 'default', 'controller' => 'tobacco-invoice',
              'action' => 'download', 'id' => '{id}', 'sort' => null, 'sorder' => null
            )), null, true
          ),
          'registered' => array(
            'header' => '',
            'renderer' => 'condition',
            'condition_value' => 'y',
            'condition_true' => array(
              'renderer' => 'Bootstrap_Button',
              'icon' => 'icon-warning-sign',
              'url' => $this->view->url(array(
                'module' => 'default' , 'controller' => 'tobacco-invoice' ,
                            'action' => 'deregister', 'id' => '{id}' , 'sort' => null, 'sorder' => null
              )),
            ), null, true
          ),
          'payment' => array(
            'header' => 'Úhrada',
            'css_style' => 'text-align: center',
            'renderer' => 'condition',
            'condition_value' => 'u',
            'condition_true' => array(
              'renderer' => 'Bootstrap_Button',
              'icon' => 'icon-plus',
              'url' => $this->view->url(array(
                'module' => 'default',
                'controller' => 'tobacco-invoice',
                'action' => 'payment-add',
                'invoice' => '{id}',
                'id' => NULL,
                'sort' => NULL,
                'sorder' => NULL,
              )),
            ),
            null, true
          ),
        )
      );

      if (!$acl->isAllowed('tobacco-invoice/prepare')) {
        unset($gridArray['columns']['locked']);
      }
    if (!$acl->isAllowed('tobacco-invoice/deregister')) {
      unset($gridArray['columns']['registered']);
    }
      if (!$acl->isAllowed('tobacco-orders/detail')) {
        unset($gridArray['columns']['order_no']['renderer']);
        unset($gridArray['columns']['title']['renderer']);
      }
      if (!$acl->isAllowed('tobacco-customers/detail')) {
        unset($gridArray['columns']['company']['renderer']);
      }
      $grid = new ZGrid($gridArray);

      $grid->setSelect($select);
      $grid->setRequest($this->getRequest()->getParams());
      $this->view->grid = $grid->render();
    }

  /*
   * Přehled neuhrazených faktur po splatnosti
   */
  public function unpaidAction() {
    $this->view->title = 'Přehled neuhrazených faktur po splatnosti';
    $table = new Table_Invoices();
    $this->view->invoices = $table->getPaymentsStats(array(
      'overdue' => TRUE,
      'unpaid' => TRUE,
      'wh' => Table_TobaccoWarehouse::WH_TYPE,
      ));
  }


    // TODO: Dekomponovat funkci, pouzit k ukladani objekt.
    public function prepareAction():void {
      $messages = [];
      $form = new Tobacco_Invoice_Form();

      if ($this->_request->isPost()) {

        // formular je validni - vlozime nebo updatujeme
        if ($form->isValid($this->_request->getPost())) {

          $invoiceId = (int) $this->getRequest()->getParam('id');
          $orderId = (int) $this->_request->getParam('order');

          // kontrola unikatnosti cisla faktury
          $table = new Table_Invoices();
          $no = $this->_request->getParam('invoice_no', FALSE) ?
            $this->_request->getParam('invoice_no') : $this->_request->getParam('proform_no');
          $type = $this->_request->getParam('invoice_type');
          $id = $table->isFree($no, $type, Table_TobaccoWarehouse::WH_TYPE);

          if ($id === TRUE || $id == $this->_request->getParam('id')) {

            $authNamespace = new Zend_Session_Namespace('Zend_Auth');

            $issueDate = $this->_request->getParam('issue_date')?
              new Meduse_Date($this->_request->getParam('issue_date'), 'Y-m-d') :
              new Meduse_Date();
            $duzp = $this->_request->getParam('duzp')?
              new Meduse_Date($this->_request->getParam('duzp'), 'Y-m-d') :
              new Meduse_Date();
            $dueDate = $this->_request->getParam('due_date')?
              new Meduse_Date($this->_request->getParam('due_date', 'Y-m-d')) : NULL;

            // Nastavení data splatnosti u regular faktury.
            if ($type === Table_Invoices::TYPE_REGULAR) {
              $maxDate = new Meduse_Date($issueDate);
              if ($invoiceId) {
                $invoice = new Invoice_Invoice($invoiceId);
                $due = $invoice->getOrder()->getDue();
              }
              else {
                $order = new Tobacco_Orders_Order($orderId);
                $due = $order->getDue();
              }
              $maxDate->addDay($due);
              if (!$dueDate || ($maxDate < $dueDate && !Zend_Registry::get('acl')->isAllowed('tobacco-invoice/overdue'))) {
                $dueDate = $maxDate;
              }
            }

            //existuje faktura?
            if ($invoiceId) {
              $data = array(
                'mask_zero_prices' => $this->_request->getParam('mask_zero_prices', 'n'),
                'invoice_price'          => $this->_request->getParam('invoice_price', 0),
                'rounding'               => $this->_request->getParam('rounding', 'n'),
                'show_vat_summary'       => $this->_request->getParam('show_vat_summary', 'y'),
                'allow_delivery_addr' => $this->_request->getParam('allow_delivery_addr', 'n'),
                'delivery_text' => $this->_request->getParam('delivery_text', null),
                'weight' => $this->_request->getParam('weight'),
                'type' => $type,
                'no' => $no,
                'currency' => $this->_request->getParam('currency'),
                'rate' => $this->_request->getParam('rate'),
                'use_actual_rate'        => $this->_request->getParam('use_actual_rate'),
                'language' => $this->_request->getParam('language'),
                'packaging' => $this->_request->getParam('packaging'),
                'packaging2' => $this->_request->getParam('packaging2'),
                'additional_discount' => (int) $this->_request->getParam('additional_discount'),
                'additional_discount_include' => $this->_request->getParam('additional_discount_include', 'n'),
                'issue_date' => $issueDate->toString('Y-m-d'),
                'duzp' => $duzp->toString('Y-m-d'),
                'due_date' => is_null($dueDate)? NULL : $dueDate->get('Y-m-d'),
                'text1' => $this->_request->getParam('text1'),
                'add_address1_head' => $this->_request->getParam('add_address1_head') ? $this->_request->getParam('add_address1_head') : null,
                'add_address1_data' => $this->_request->getParam('add_address1_data') ? $this->_request->getParam('add_address1_data') : null,
                'add_address2_data' => $this->_request->getParam('add_address2_data') ? $this->_request->getParam('add_address2_data') : null,
                'id_accounts' => $this->_request->getParam('id_accounts'),
                'show_excise' => $this->_request->getParam('show_excise', 'n'),
                'export' => $this->_request->getParam('export', 'n'),
              );

              $this->_db->update('invoice', $data, 'id  = ' . $invoiceId);
            }
            else {
              $data = array(
                'id_wh_type' => Table_TobaccoWarehouse::WH_TYPE,
                'id_orders' => $orderId,
                'id_users' => $authNamespace->user_id,
                'mask_zero_prices' => $this->_request->getParam('mask_zero_prices', 'n'),
                'rounding'               => $this->_request->getParam('rounding', 'n'),
                'invoice_price'          => $this->_request->getParam('invoice_price', 0),
                'show_vat_summary'       => $this->_request->getParam('show_vat_summary', 'y'),
                'allow_delivery_addr' => $this->_request->getParam('allow_delivery_addr', 'n'),
                'delivery_text' => $this->_request->getParam('delivery_text', null),
                'weight' => $this->_request->getParam('weight'),
                'type' => $type,
                'no' => $no,
                'currency' => $this->_request->getParam('currency'),
                'rate' => $this->_request->getParam('rate'),
                'use_actual_rate'        => $this->_request->getParam('use_actual_rate'),
                'language' => $this->_request->getParam('language'),
                'packaging' => $this->_request->getParam('packaging'),
                'packaging2' => $this->_request->getParam('packaging2'),
                'additional_discount' => (int) $this->_request->getParam('additional_discount'),
                'additional_discount_include' => $this->_request->getParam('additional_discount_include', 'n'),
                'issue_date' => $issueDate->toString('Y-m-d'),
                'duzp' => $duzp->toString('Y-m-d'),
                'due_date' => is_null($dueDate)? NULL : $dueDate->get('Y-m-d'),
                'text1' => $this->_request->getParam('text1'),
                'add_address1_head' => $this->_request->getParam('add_address1_head') ? $this->_request->getParam('add_address1_head') : null,
                'add_address1_data' => $this->_request->getParam('add_address1_data') ? $this->_request->getParam('add_address1_data') : null,
                'add_address2_data' => $this->_request->getParam('add_address2_data') ? $this->_request->getParam('add_address2_data') : null,
                'id_accounts' => $this->_request->getParam('id_accounts'),
                'show_excise' => 'y',
                'export' => $this->_request->getParam('export', 'n'),
                // 'show_vat_summary' => 'y',
                'inserted' => new Zend_Db_Expr('NOW()')
              );
              $this->_db->insert('invoice', $data);
              $invoiceId = $this->_db->lastInsertId();
            }

            $invoice = new Invoice_Invoice($invoiceId);
            $invoice->getInvoicedPrice();

            $this->_flashMessenger->addMessage('Faktura byla uložena.');
            $this->redirect($this->view->url(array(
                'controller' => 'tobacco-invoice',
                'id' => $invoiceId,
                'order' => null)));
            return;
          }
          else {
            $messages[] = 'Číslo faktury ' . $no . ' jiz existuje.';
          }
        }
      }

      $tInvoice = new Table_Invoices();
      $invoiceId = $this->_request->getParam('id', null);

      // zalozeni nové z objednávky
      if (!$invoiceId) {
        $type = $this->_request->getParam('type');
        $orderId = $this->getRequest()->getParam('order', null);
        if (!$orderId) {
          $this->redirect('/tobacco-orders');
        }
        $invoiceId = $tInvoice->getInvoiceIdByOrder($orderId, $type);
        try {
          if ($invoiceId) {
            $invoice = new Tobacco_Invoice_Invoice($invoiceId);
          }
          else {
            $order = new Orders_Order($orderId);
            if (!$order->hasItems()) {
              $this->_flashMessenger->addMessage('Nelze vystavit fakturu, objednávka nemá žádné položky.');
              $this->redirect('/tobacco-orders/detail/id/' . $orderId);
            }
            $invoice = new Tobacco_Invoice_Invoice();
            // nastavení typu faktury z objednávky
            $invoiceId = $invoice->setOrder($orderId, $type);
            $this->redirect('/tobacco-invoice/prepare/id/' . $invoiceId);
          }
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
          $this->redirect('/tobacco-orders/detail/id/' . $orderId);
        }
      }
      else {
        try {
          $invoice = new Tobacco_Invoice_Invoice($invoiceId);
          $orderId = $invoice->getOrderId();
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
          $this->redirect('/tobacco-invoice');
        }
      }

      // upozorneni na vystaveni pokladniho dokladu
      $this->view->noticeCashReceipt = (
        $invoice->isRegular() &&
        $invoice->getOrder()->getPaymentMethod() == Table_Orders::PM_CASH
      );
      // upozorneni na vystaveni prepravniho dokladu
      $this->view->noticeTAXLiability = $invoice->isTransferredTaxLiability();

      // kontrola uzamčení
      if ($invoice->isLocked() && !in_array('accountant_tobacco', $_SESSION['Zend_Auth']['user_role'])) {
        $this->_flashMessenger->addMessage('Fakturu nelze upravovat, již je v účetní evidenci.');
        $this->redirect('/tobacco-invoice');
      }

      // vyplneni nastavení faktury --------------------------------------------
      if ($this->_request->isPost()) {
        $form->populate($this->_request->getPost());
      }
      else {
        $form->mapFromRow($invoice->getRow());
        $curr_invoice = $invoice->getCurrency();
        $curr_order = $invoice->getOrder()->getExpectedPaymentCurrency();
        if ($curr_invoice != $curr_order) {
          $form->currency->setValue($curr_order);
          $messages[] = 'Měna v objednávce (' . $curr_order
            . ') byla změněna a neodpovídá měně na faktuře (' . $curr_invoice
            . '). Přeuložte prosím tuto fakturu.';
        }
      }

      // defaultni pole
      if ($invoice->getRegion() == Table_Addresses::REGION_NON_EU) {
        // vycisleni DPH
        $form->removeElement('show_vat_summary');
        $elem = new Zend_Form_Element_Hidden('show_vat_summary');
        $elem->setValue('n');
        $form->addElement($elem);
        // maskovani nulovych cen
        $form->removeElement('mask_zero_prices');
        $elem = new Zend_Form_Element_Hidden('mask_zero_prices');
        $elem->setValue('y');
        $form->addElement($elem);
      }
      if ($invoice->getCountry() == Table_Addresses::COUNTRY_CZ) {
        // vycisleni DPH
        $form->removeElement('show_vat_summary');
        $elem = new Zend_Form_Element_Hidden('show_vat_summary');
        $elem->setValue('y');
        $form->addElement($elem);
      }

      if ($invoice->orderIsService()) {
        $form->removeElement('packaging');
        $form->removeElement('weight');
        $form->removeElement('allow_delivery_addr');
        $form->removeElement('delivery_text');
        $form->removeElement('show_excise');
      }

      if ($invoice->isProform()) {
        $form->removeElement('duzp');
      }

      $this->view->invoiceId = $invoiceId;
      $this->view->invoiceType = $invoice->getType();
      $this->view->items = $tInvoice->getCustomItems($invoiceId);
      $this->view->form = $form;
      $this->view->orderId = $invoice->getOrderId();
      $this->view->orderCurrency = $invoice->getOrder()->getExpectedPaymentCurrency();
      $this->view->orderIsService = $invoice->getOrder()->isService();
      $this->view->paymentMethod = $invoice->getOrder()->getPaymentMethod();
      $this->view->due = $invoice->getOrder()->getDue();
      $this->view->overdue = Zend_Registry::get('acl')->isAllowed('tobacco-invoice/overdue');
      $this->view->isRegular = $invoice->isRegular();
      $this->view->versions = $invoice->getVersions();
      $this->view->invoiceIsDeletable = $invoice->isDeletable();
      $this->view->verifiedVat = $invoice->getOrder()->getCustomer(TRUE)->isVerifiedVatNo();

      $customItemForm = new Invoice_CustomItemForm();
      $customItemForm->removeElement('vat_included');
      $customItemForm->setAction('/tobacco-invoice/add-custom-item');
      $customItemForm->populate(array('id_invoice' => $invoiceId));
      $this->view->customItemForm = $this->view->partial('tobacco-invoice/custom-item.phtml', array('form' => $customItemForm));

      try {
        $itemLines = $invoice->getItemLines();
      }
      catch (Parts_Exceptions_NoPricelist $e) {
        $order = new Orders_Order($orderId);
        $url = $this->view->url(array(
          'controller' => 'tobacco-pricelists', 'action' => 'assign',
          'customer' => $order->getCustomer()->id, 'order' => $orderId,
        ));
        $this->_flashMessenger->addMessage('Zákazník nemá přiřazený platný ceník. '
          . '<a class="btn btn-primary btn-small" href="' . $url . '">'
          . '<i class="icon icon-white icon-list"></i> Přiřadit</a>');
        $this->redirect('tobacco-orders/detail/id/' . $orderId);
      }
      $itemLinesForm = new Invoice_ItemLinesForm();
      $itemLinesForm->setInvoiceId($invoice->getId());
      $itemLinesForm->fill($itemLines);
      $this->view->itemLinesForm = $itemLinesForm;
      $this->view->itemLines = $itemLines;

      $this->view->acl = Zend_Registry::get('acl');

      // odpocet uhrad zaloh
      try {
        $this->view->deductForm = new Invoice_DeductForm(array('id_invoice' => $invoiceId));
      }
      catch (Invoice_Exceptions_NoPayments $e) {
        $this->view->deductForm = NULL;
      }
      catch (Invoice_Exceptions_BadType $e) {
        $this->view->deductForm = FALSE;
      }

      $this->view->message = implode('<br>', $messages);
    }

    /**
     * akce generovani faktury ve formatu PDF
     */
    public function generateAction() {
      $this->_helper->layout()->disableLayout();
      $id = $this->getRequest()->getParam('id');
      if (is_null($id)) {
        $this->redirect('/tobacco-invoice');
      }

      try {
        $invoice = new Tobacco_Invoice_Invoice($id);

        // kontrola dopravy
        if (!$invoice->orderIsService() && !$invoice->isShippingCustomItem()) {
          $this->_flashMessenger->addMessage('Nelze generovat fakturu, chybí přepravní náklady.');
          $this->redirect($this->view->url(array('action' => 'prepare', 'id' => $invoice->getId())));
          exit;
        }

        /*
        if ($invoice->isRegistered()) {
          $tEmails = new Table_Emails();
          $tEmails->sendEmail($tEmails::EMAIL_INVOICE_CHANGE, array('invoice' => $invoice));
        }
        */

        $version = $invoice->getLastVersion();
        $chunks = explode('/', $version['filename']);
        $this->view->filename = array_pop($chunks);

        $order = $invoice->getOrder();

        $rate = $invoice->getRate();
        $currency = $invoice->getCurrency();
        $price = $invoice->getInvoicedPrice();

        if ($invoice->getType() == Table_Invoices::TYPE_REGULAR && $order->getPaymentMethod() == Table_Orders::PM_CASH) {

          $tEmail = new Table_Emails();
          $tEmail->sendEmail(Table_Emails::EMAIL_INVOICE_IN_CASH_NOTICE, $invoice);

          // Pokud k regular fakture neexistuji uhrady a platba je v hotovosti, vytvorime ihned uhradu.
          if (!$invoice->getPayments()) {
            $issue = $invoice->getIssueDate();
            $data = array(
              'id_invoice' => $invoice->getId(),
              'id_wh_type' => $invoice->getWHType(),
              'amount' => $currency == Table_Invoices::CURRENCY_EUR ? round($price, 2) : round($price, 0),
              'currency' => $currency,
              'rate' => $rate,
              'date' => $issue ? $issue : new Zend_Db_Expr('now()'),
            );
            $payment = new Invoice_Payment($data);
            $payment->save();
            $tEmail->sendEmail(Table_Emails::EMAIL_PAYMENT_NOTICE_ADD, $payment);
          }
        }

        $expected = $invoice->getInvoicedPrice(TRUE, TRUE);
        $order->setExpectedPayment($expected, $rate, $currency);
        $order->setExpectedPaymentRate($rate);

        $this->view->pdf = $invoice->generatePdf();
      }

      catch (Invoice_Exceptions_ZeroProductPrice $exception) {
        $this->_flashMessenger->addMessage($exception->getMessage());
        $this->redirect($this->view->url(array('action' => 'prepare')));
      }

      catch (Exception $e) {
        $this->getHelper('flashMessenger')->addMessage($e->getMessage());
        $this->redirect('/tobacco-invoice');
      }
    }

    public function downloadAction() {
      $this->_helper->layout()->disableLayout();
      $id = $this->getRequest()->getParam('id');

      try {
        if (is_null($id)) {
          throw new Exception('Nebylo zadáno ID faktury.');
        }

        $invoice = new Tobacco_Invoice_Invoice($id);

        $order = $invoice->getOrder();
        if ($order && !$order->isClosed() && Utils::isDisallowed('tobacco-invoice/download-not-closed')) {
          throw new Exception('Lze stáhnout faktury pouze k expedovaným objednávkám.');
        }

        if (is_null($this->_request->getParam('vid'))) {
          if (!$version = $invoice->getLastVersion($order && $order->isClosed())) {
            throw new Invoice_Exceptions_NotValid('Faktura č. ' . $invoice->getNo() . ' není aktuální, je potřeba vygenerovat novou verzi.');
          }
        } else {
          $version = $invoice->getVersion($this->_request->getParam('vid'));
        }

        if (is_null($version)) {
          throw new Exception('Faktura č. ' . $invoice->getNo() . ' není vygenerovaná.');
        }

        if (Utils::hasCurrentUserRole('accountant_pipes')) {
          $invoice->register();
        }

        $pdf = Meduse_Pdf_Invoice::load($version['filename']);
        $explode = explode('/', $version['filename']);
        $this->view->filename = array_pop($explode);
        $this->view->pdf = $pdf;
        $this->render('generate');
      } catch (Exception $e) {

        if ($e instanceof Zend_Pdf_Exception) {
          $message = 'Soubor ' . $version['filename'] . ' nelze otevřít pro čtení.';
        }
        else {
          $message = $e->getMessage();
        }
        Utils::logger()->warn($message);
        $this->_flashMessenger->addMessage($message);
        $this->redirect($_SERVER['HTTP_REFERER']);
      }
    }

    public function saveItemTextsAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      if ($this->getRequest()->isPost()) {
        $data = $this->getRequest()->getParams();
        $invoiceId = $data['invoice_id'];
        unset($data['controller']);
        unset($data['action']);
        unset($data['module']);
        unset($data['invoice_id']);
        $invoice = new Tobacco_Invoice_Invoice($invoiceId);

        $texts = array();
        $prices = array();

        foreach($data as $name => $value) {
          list($item, $idx) = explode('_', $name, 2);
          if ($item == 'item') {
            if (isset($data['type_' . $idx]) && $data['type_' . $idx] == 2) {
              continue;
            }
            $int = (int) $idx;
            if ($int > 0) {
              if (isset($data['item_idx_' . $idx])) {
                $texts[$data['item_idx_' . $idx]][] = $value;
              }
            }
          }
          elseif ($item == 'price') {
            if (isset($data['type_' . $idx]) && $data['type_' . $idx] == 2) {
              continue;
            }
            $int = (int) $idx;
            if ($int > 0) {
              if (isset($data['item_idx_' . $idx])) {
                if (!isset($prices[$data['item_idx_' . $idx]])) {
                  $prices[$data['item_idx_' . $idx]] = [];
                }
                $prices[$data['item_idx_' . $idx]]['price'] = Meduse_Currency::parse($value);
                $prices[$data['item_idx_' . $idx]]['vat_included'] = FALSE;
              }
            }
          }
        }

        $invoice->saveItemTexts($texts);
        $invoice->saveItemPrices($prices);
        $this->_flashMessenger->addMessage('Produktové položky byly uloženy.');
        $this->redirect('/tobacco-invoice/prepare/id/' . $invoiceId);
      }
      $this->redirect('/tobacco-invoice');
    }

    public function resetPricesAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $id = $this->_request->getParam('id');
      if ($id) {
        $invoice = new Invoice_Invoice($id);
        $invoice->resetItemsPrices();
        $this->_flashMessenger->addMessage('Ceny byly resetovány dle ceníku');
        $this->redirect('tobacco-invoice/prepare/id/' . $id);
      }
      $this->_flashMessenger->addMessage('Nebylo zadáno ID faktury.');
      $this->redirect('tobacco-invoice');
    }

    public function resetTextsAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $id = $this->_request->getParam('id');
      if ($id) {
        $invoice = new Invoice_Invoice($id);
        $invoice->resetItemsTexts();
        $this->_flashMessenger->addMessage('Texty položek byly nastaveny na výchozí hodnoty.');
        $this->_redirect('tobacco-invoice/prepare/id/' . $id);
      }
      $this->_flashMessenger->addMessage('Nebylo zadáno ID faktury.');
      $this->_redirect('tobacco-invoice');
    }

    public function addCustomItemAction() {
        $this->_helper->layout()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $form = new Invoice_CustomItemForm();
            $form->populate($this->getRequest()->getParams());
            $data = $form->getValues(TRUE);
            $tItems = new Table_InvoiceCustomItems();
            $id = $data['id_item'];
            unset($data['id_item']);
            if ($id) {
                $tItems->update($data, 'id = ' . $id);
                $this->_flashMessenger->addMessage('Volitelná položka byla aktualizována.');
            } else {
                $tItems->insert($data);
                $this->_flashMessenger->addMessage('Volitelná položka byla přidána.');
            }
            $invoice = new Invoice_Invoice($data['id_invoice']);
            $invoice->recalculateInvoicedPrice();
            $this->_redirect('/tobacco-invoice/prepare/id/' . $data['id_invoice']);
        } else {
            $this->_redirect('/tobacco-invoice');
        }
    }

    public function customTextsAction() {
        $tTexts = new Table_InvoiceCustomTexts();
        $this->view->texts = $tTexts->fetchAll('id_wh_type = '. Table_TobaccoWarehouse::WH_TYPE)->toArray();
    }

    public function customTextsEditAction() {
        $data = array();
        $form = new Invoice_CustomTextsForm();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $data = array(
                    'id'          => $this->getRequest()->getParam('id', null),
                    'id_wh_type'  => Table_TobaccoWarehouse::WH_TYPE,
                    'description' => substr(
                            $this->getRequest()->getParam('description'), 0,
                            Table_InvoiceCustomTexts::LENGTH_DESCRIPTION),
                    'type'        => $this->getRequest()->getParam('type'),
                    'region'      => $this->getRequest()->getParam('region'),
                    'language'    => $this->getRequest()->getParam('language'),
                    'text'        => substr(
                            $this->getRequest()->getParam('text'), 0,
                            Table_InvoiceCustomTexts::LENGTH_TEXT)
                );
                $tTexts = new Table_InvoiceCustomTexts();
                if ($data['id']) {
                    if ($tTexts->update($data, 'id = ' . $data['id'])) {
                        $this->_flashMessenger->addMessage('Text byl upraven.');
                    }
                } else {
                    unset($data['id']);
                    if ($tTexts->insert($data)) {
                        $this->_flashMessenger->addMessage('Text byl uložen.');
                    }
                }
                $this->_redirect('/tobacco-invoice/custom-texts');
                exit;
            } else {
                $form->populate($data);
            }
        } else {
            $id = $this->getRequest()->getParam('id', null);
            if (is_null($id)) {
                $form->setLegend('Vložení nového uživatelského textu');
            } else {
                $form->setLegend('Editace uživatelského textu');
                $tTexts = new Table_InvoiceCustomTexts();
                $data = $tTexts->find($id)->current()->toArray();
                $form->populate($data);
            }
        }
        $this->view->form = $form;
    }

    public function customTextsRemoveAction() {
        if ($this->getRequest()->isPost()) {
            $id = (int) $this->getRequest()->getParam('id');
            if ($id) {
                $tTexts = new Table_InvoiceCustomTexts();
                if ($tTexts->delete('id = ' . $id)) {
                    $this->_flashMessenger->addMessage('Text byl vymazán');
                }
            }
        }
        $this->_redirect('/tobacco-invoice/custom-texts');
    }


    public function customTextsGetJsonAction() {
        $tTexts = new Table_InvoiceCustomTexts();
        $id = $this->getRequest()->getParam('id', null);
        if (is_null($id)) {
            $order = $this->getRequest()->getParam('order');
            $language = $this->getRequest()->getParam('language');
            $order = new Orders_Order($order);
            $customer = $order->getCustomer();
            $type = $customer->type;
            $tAddress = new Table_Addresses();
            $address = $tAddress->getAddress($order->_address);
            $region = $address['region'];
            $select = $tTexts->select()->setIntegrityCheck(false);
            $select->where('type = ?', $type);
            $select->where('id_wh_type = ?', Table_TobaccoWarehouse::WH_TYPE);
            $select->where('region = ?', $region);
            $select->where('language = ?', $language);
            $items = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
        } else {
            $items = $tTexts->find($id)->toArray();
        }
        $json = json_encode($items);
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        echo $json;
    }

    public function jsonGetCustomItemAction() {
      $item = array();
      $id = $this->getRequest()->getParam('id', null);
      if (!is_null($id)) {
          $tItems = new Table_InvoiceCustomItems();
          $item = $tItems->find($id)->current()->toArray();
      }
      $json = json_encode($item);
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      echo $json;
    }

    public function removeCustomItemAction() {
      $this->_helper->layout()->disableLayout();
      $invoiceId = $this->getRequest()->getParam('invoice');
      $id = $this->getRequest()->getParam('id', null);
      if (!is_null($id)) {
          $tItems = new Table_InvoiceCustomItems();
          $tItems->delete('id = ' . $id);
      }
      $invoice = new Invoice_Invoice($invoiceId);
      $invoice->recalculateInvoicedPrice();
      $this->_redirect('/tobacco-invoice/prepare/id/' . $invoiceId);
    }

    public function createCreditNoteAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      // otestování zadání ID faktury
      if($invoiceID = $this->_request->getParam('id')) {
        // otestování, jestli vstupní faktura existuje
        try {
          $invoice = new Tobacco_Invoice_Invoice($invoiceID);

          // přesměrování na existující dobropis
          $tInvoice = new Table_Invoices();
          $existing_credit = $tInvoice->select()
            ->where('id_orders = ?', $invoice->getOrderId())
            ->where('type = ?', Table_Invoices::TYPE_CREDIT)
            ->query()->fetch();

          if ($existing_credit) {
            $this->_flashMessenger->addMessage('Dobropis již existuje, bylo na něj jen přesměrováno');
            $this->redirect('/tobacco-invoice/prepare/id/' . $existing_credit['id']);
          }

          // generováné nového dobropisu
          try {
            $credit_note = $invoice->generateCreditNote();
            $this->redirect('/tobacco-invoice/prepare/id/' . $credit_note->getId());
          }
          catch (Exception $e) {
            $this->_flashMessenger->addMessage($e->getMessage());
            $this->redirect('/tobacco-invoice/prepare/id/' . $invoiceID);
          }
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
          $this->redirect('/tobacco-invoice');
        }


      }
      else {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID vstupní faktury');
        $this->redirect('/tobacco-invoice');
      }
    }

    public function deregisterAction() {
      $id = $this->getParam('id');
      if ($id) {
        $invoice = new Tobacco_Invoice_Invoice($id);
        $invoice->deregister();
        $this->_flashMessenger->addMessage('Faktura č. ' . $invoice->getNo() . ' byla znovu odemknuta pro úpravy.');
      }
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $this->redirect($this->view->url(array('action' => NULL, 'id' => NULL)));
    }


    public function paymentsAction() {
      $params = array(
        'sort' => $this->getParam('sort'),
        'sorder' => $this->getParam('sorder'),
        'page' => $this->getParam('page'),
      );
      $payments = Invoice_Payment::getPaymentsGrid(Parts_Part::WH_TYPE_TOBACCO, $params);
      $this->view->payments = $payments->render();
      $this->view->title = 'Přijaté úhrady – přehled';
      $this->render('invoice/payments', NULL, TRUE);
    }

   public function paymentAddAction() {
      $title = 'Nová úhrada';

      if ($this->_request->isPost()) {
        $back = $this->_request->getParam('back');
        $redirect = is_null($back) ? $this->view->url(array('action' => 'payments', 'invoice' => NULL)) : base64_decode($back);
        $data = array(
          'id_invoice' => $this->getParam('id_invoice'),
          'amount' => $this->getParam('amount'),
          'currency' => $this->getParam('currency'),
          'rate' => $this->getParam('rate'),
          'date' => Meduse_Date::formToDb($this->getParam('date')),
          'no' => $this->getParam('no'),
          'id_wh_type' => $this->getParam('wh'),
        );

        $payment = new Invoice_Payment($data);
        try {
          $payment->save();
          $tEmail = new Table_Emails();
          $tEmail->sendEmail(Table_Emails::EMAIL_PAYMENT_NOTICE_ADD, $payment);
        }
        catch(Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
        }
        $this->redirect($redirect);
      }

      $form = new Invoice_PaymentForm(array('id_invoice' => $this->getParam('invoice')));
      $form->setLegend($title);
      $this->view->title = $title;
      $this->view->form = $form;
      $this->render('invoice/payment-form', NULL, TRUE);
    }

   public function paymentEditAction() {
      $title = 'Změna úhrady';
      $form = new Invoice_PaymentForm(array('id_payment' => $this->getParam('id')));
      $form->setLegend($title);

      if ($this->_request->isPost()) {
        $data = array(
          'id' => $this->getParam('id_payment'),
          'id_invoice' => $this->getParam('id_invoice'),
          'amount' => $this->getParam('amount'),
          'currency' => $this->getParam('currency'),
          'rate' => $this->getParam('rate'),
          'date' => Meduse_Date::formToDb($this->getParam('date')),
          'no' => $this->getParam('no'),
          'id_wh_type' => $this->getParam('wh'),
        );

        $payment = new Invoice_Payment($data['id']);
        $payment->setData($data);
        try {
          $payment->save();
          $tEmail = new Table_Emails();
          $tEmail->sendEmail(Table_Emails::EMAIL_PAYMENT_NOTICE_CHANGE, $payment);
          $this->redirect($this->view->url(array('action' => 'payments', 'invoice' => NULL)));
        }
        catch(Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
          $this->redirect($this->view->url(array()));
        }
      }

      $this->view->title = $title;
      $this->view->form = $form;
      $this->render('invoice/payment-form', NULL, TRUE);
    }

    public function paymentGetRecipeAction() {
      $this->_helper->layout()->disableLayout();
      $payment = new Invoice_Payment($this->getParam('id'));
      $pdf = $payment->getRecipe();
      $this->view->filename = 'Daňový doklad k přijaté platbě ' . $payment->getNo();
      $this->view->pdf = $pdf;
      $this->render('invoice/generate', NULL, TRUE);
    }

    public function paymentRemoveAction() {
      $id = $this->getParam('id');
      $back = $this->_request->getParam('back');
      $redirect = is_null($back) ? $this->view->url(array('action' => 'payments', 'id' => NULL)) : base64_decode($back);
      $payment = new Invoice_Payment($id);
      $order = $payment->getOrder();
      $removed = FALSE;
      $message = '';
      try {
        $removed = Invoice_Payment::removePayment($id);
      }
      catch (Zend_Db_Statement_Exception $e) {
        $message = 'Úhrada je použitá jako odpočet ve faktuře.';
      }
      if ($removed) {
        $order->checkPayments();
        $tEmail = new Table_Emails();
        $tEmail->sendEmail(Table_Emails::EMAIL_PAYMENT_NOTICE_REMOVE, $payment);
        $this->_flashMessenger->addMessage('Úhrada byla odstraněna.');
      }
      else {
        if ($message) {
          $message = 'Úhradu se nepodařilo odstranit. ' . $message;
        }
        $this->_flashMessenger->addMessage($message);
      }
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $this->redirect($redirect);
    }

    public function deleteAction() {

      $id = $this->_request->getParam('id');
      if ($id) {
        try {
          $invoice = new Invoice_Invoice($id);
          $no = $invoice->getNo();
          $no = $no ? ' č. ' . $no . ' ' : ' ';
          if ($this->_request->isPost()) {
            $invoice->delete();
            $invoice->getCustomer()->recalculateBudgets();
            $this->_flashMessenger->addMessage('Faktura' . $no . 'byla odstraněna ze systému.');
            $this->redirect($this->view->url(array('action' => 'index', 'id' => NULL)));
          }
          else {
            $form = new Invoice_DeleteForm();
            $form->setLegend('Opravdu chcete smazat fakturu' . $no . '?');
            $form->getElement('id')->setValue($id);
            $this->view->form = $form;
            $this->view->id = $id;
            $this->view->title = 'Odstranění faktury' . $no;
          }
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage('Fakturu se nepodařilo odstranit: ' . $e->getMessage());
          $this->redirect($this->view->url(array('action' => 'prepare')));
        }
      }
      else {
        $this->_flashMessenger->addMessage('Nebyl zadaný identifikátor.');
        $this->redirect($this->view->url(array('action' => 'index', 'id' => NULL)));
      }
    }

    public function ajaxSaveDeductAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $invoiceId = $this->_request->getParam('id');
      $paymentId = $this->_request->getParam('id_payment');
      $deductionId = $this->_request->getParam('id_deduction');
      $amount = $this->_request->getParam('amount');
      $payment = new Invoice_Payment($paymentId);
      try {
        $deductionId = $payment->setDeductedInvoice($invoiceId, $deductionId, $amount);
        $data = array('status' => TRUE, 'message' => 'Uloženo.', 'id_deduction' => $deductionId ? $deductionId : '');
      }
      catch(Exception $e) {
        $data = array('status' => FALSE, 'message' => $e->getMessage());
      }
      $this->_helper->json($data);
    }

    public function loadShipmentFromOrderAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      $invoiceId = $this->_request->getParam('id');
      try {
        if (!$invoiceId) {
          throw new Exception('Neni zadano ID faktury.');
        }
        $invoice = new Tobacco_Invoice_Invoice($invoiceId);
        $invoice->loadShipmentFromOrder();
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
      }
      $this->redirect('/tobacco-invoice/prepare/id/' . $invoiceId);
    }


    public function paymentMoveToAction() {
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $paymentId = $this->_request->getParam('id');
      $invoiceId = $this->_request->getParam('to');
      $back = $this->_request->getParam('back');

      $process = TRUE;

      if (!$paymentId) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID úhrady.');
        $process = FALSE;
      }

      if (!$invoiceId) {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID faktury.');
        $process = FALSE;
      }

      if (!$payment = new Invoice_Payment($paymentId)) {
        $this->_flashMessenger->addMessage("Úhrada s ID = $paymentId nebyla nalezena");
        $process = FALSE;
      }

      if ($process) {
        try {
          if ($payment->moveTo($invoiceId)) {
            $payment->getOrder()->checkPayments();
            $this->_flashMessenger->addMessage('Úhrada byla přesunuta.');

          }
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
        }
      }

      $this->redirect($back ? base64_decode($back) : $this->view->url(['controller' => 'tobacco-invoice', 'action' => 'index']));
    }

  }