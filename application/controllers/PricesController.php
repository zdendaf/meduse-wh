<?php

class PricesController extends Meduse_Controller_Action {

  public function editAction() {
    $this->_helper->layout->setLayout('bootstrap-basic');
    $partId = $this->getRequest()->getParam('part');
    try {
      $part = new Parts_Part($partId);
    }
    catch (Exception $e) {
      $this->_flashMessenger->addMessage('Nepodařilo se načíst produkt <em>' . $partId . '</em>.');
      $this->redirect('/parts');
    }
    if (!$part->isProduct()) {
      $this->_flashMessenger->addMessage('Součást <em>' . $partId . '</em> není produktem.');
      $this->redirect('/parts/detail/id/' . $partId);
    }

    // Načtení ceny z master ceníku.
    $tPrice = new Table_Prices();
    $prices = array_filter($tPrice->getProductPrices($partId, TRUE), function($item){
      $mapping = Pricelists::getMapping(0, true);
      $idPriceList = reset($mapping);
      return $item['id'] == $idPriceList;
    });
    $this->view->prices = $prices;
    //$this->view->prices = $tPrice->getProductMasterPrice($partId, TRUE);
    $this->view->precision = $part->getCtg() == Table_PartsCtg::TYPE_SETS ? 0 : 2;

    $this->view->cover = $part->getCoverCost(true) * 100;

    $this->view->cost_nett_czk = $part->getCost(false);
    $this->view->cost_nett_eur = Utils::toEur($this->view->cost_nett_czk);

    $this->view->cost_czk = $part->getCost();
    $this->view->cost_eur = Utils::toEur($this->view->cost_czk);

    $this->view->part = $part;
    $this->view->action = $this->view->url([
      'controller' => 'prices',
      'action' => 'save',
      'back' => $this->_request->getParam('back'),
    ]);
  }

  public function saveAction() {
    if (!$this->getRequest()->isPost()) {
      $this->redirect('/prices');
    }
    $params = $this->getRequest()->getPost();
    $tPrices = new Table_Prices();
    foreach ($params as $name => $value) {
      list($base, $id) = explode('_', $name, 2);
      if ($base != 'price') {
        continue;
      }
      $price = (float) $value;
      if ($price > 0) {
        $tPrices->savePrice($params['part'], '', $id, $price, TRUE);
      }
      else {
        $tPrices->delete('id_parts = "' . $params['part'] . '" AND id_pricelists = ' . $id);
      }
    }
    $this->_flashMessenger->addMessage('Ceny byly uloženy');
    if ($back = $this->_request->getParam('back')) {
      $backUrl = base64_decode($back);
    }
    else {
      $backUrl = '/parts/detail/id/' . $params['part'];
    }
    $this->redirect($backUrl);
  }

}
