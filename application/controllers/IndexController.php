<?php
class IndexController extends Meduse_Controller_Action{

    public function indexAction(){
        $this->view->title = "meduse warehouse";
        $authNamespace = new Zend_Session_Namespace('Zend_Auth');
        foreach($this->_flashMessenger->getMessages() as $mess){
            $this->_flashMessenger->addMessage($mess);
        }
        /*
        $ual = new Table_UsersActionLog();
        $lastAction = $ual->getLastURL($authNamespace->username);
        if ($lastAction) {
          $this->redirect($lastAction);
          exit;
        }
        */
        $dests = array(
          'manager'            => '/orders/',
          'seller'             => '/orders/',
          'all_seller'         => '/orders/',
          'tobacco_head'       => '/tobacco/warehouse/',
          'tobacco_whman'      => '/tobacco/warehouse-daily-packing/',
          'tobacco_seller'     => '/tobacco-orders/',
          'admin'              => '/parts/warehouse/',
          'head_warehouseman'  => '/parts/warehouse/',
          'warehouseman'       => '/parts/warehouse/',
          'brigadier'          => '/parts/warehouse/',
          'issue_reader'       => '/issues/',
          'wh_editor'          => '/parts/overview/',
          'extern_wh'          => '/extern/',
          'accountant_pipes'   => '/invoice/',
          'accountant_tobacco' => '/tobacco-invoice/',
          'dept_leader'        => '/employees/admin/',
          'employee'           => '/employees/',
        );

        foreach ($dests as $role => $dest) {
          if (in_array($role, $authNamespace->user_role)) {
            $db = Zend_Registry::get('db');
            $db->update('users_profile', array(
              'id_wh_type' => strpos($dest, '/tobacco') == 0 ? 2 : 1
              ), 'id_users = ' . $authNamespace->user_id);
            $this->redirect($dest);
            exit;
          }
        }
        throw new Exception('Neznama role: ' . $role);
    }

    public function extendAction(){
        //prodlouzeni timeoutu
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $authNamespace = new Zend_Session_Namespace('Zend_Auth');
        $authNamespace->setExpirationSeconds(60*60);
    }

    public function reportBugAction() {
        //send mail

        if($this->getRequest()->isPost()) {
            $form = new BugReportForm();
            if($form->isValid($this->getRequest()->getParams())) {
                $authNamespace = new Zend_Session_Namespace('Zend_Auth');
                $data = $form->getValues();
                $tIssues = new Table_Issues();
                $tIssues->insert(array(
                    'status' => Table_Issues::STATUS_NEW,
                    'id_users' => intval($authNamespace->user_id),
                    'summary' => $data["bug_title"],
                    'description' => $data["bug_desc"],
                    'page' => $data["bug_page"],
                    'browser' => $data["bug_browser"],
                    'inserted' => date('Y-m-d H:i:s')
                ));
                $tEmails = new Table_Emails();
                $tEmails->sendEmail(Table_Emails::EMAIL_BUG_REPORT, $form->getValues());
            }
        }

        $this->_flashMessenger->addMessage("Problém nahlášen.");
        $this->_redirect('/issues');
    }

    public function switchWarehouseAction() {
      	$acl = Zend_Registry::get('acl');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $tUser = new Table_Users();
        $user = $tUser->getUserByName(Zend_Auth::getInstance()->getIdentity());
        $db = Zend_Registry::get('db');
        $wh = $this->_request->getParam('wh');
        switch ($wh) {
            case 'employees':
                //$db->update('users_profile', array('id_wh_type' => Parts_Part::WH_TYPE_TOBACCO), 'id_users = ' . $user['id']);
                if ($acl->isAllowed('employees/index')) {
                  $this->_redirect('/employees/');
                }
                elseif($acl->isAllowed('employees/account-report')) {
                  $this->_redirect('/employees/account-report');
                }
                break;
            case 'tobacco':
                $db->update('users_profile', array('id_wh_type' => Parts_Part::WH_TYPE_TOBACCO), 'id_users = ' . $user['id']);
                if ($acl->isAllowed('tobacco-orders/index')) {
                  $this->_redirect('/tobacco-orders/');
                }
                elseif ($acl->isAllowed('tobacco-invoice/index')) {
                  $this->_redirect('/tobacco-invoice/');
                }
                break;
            case 'meduse':
            default:
                $db->update('users_profile', array('id_wh_type' => Parts_Part::WH_TYPE_PIPES), 'id_users = ' . $user['id']);
                if ($acl->isAllowed('orders/index')) {
                  $this->_redirect('/orders/');
                }
                elseif ($acl->isAllowed('invoice/index')) {
                  $this->_redirect('/invoice/');
                }
                break;
            }
    }
}
