<?php
/**
 * Description of PackingsController
 *
 * @author Vojtěch Mrkývka
 */
class PackingsController extends Meduse_Controller_Action{
  public function init() {
    $this->_helper->layout->setLayout('bootstrap-basic');
    parent::init();
  }
  
  public function packAction() {
    //Je zadáno ID objednávky?
    if(!$this->_request->getParam('id')) {
      $this->_flashMessenger->addMessage('Nebylo zadáno číslo objednávky.');
      $this->_redirect('orders');
    }
    
    $order_no = $this->_request->getParam('id');
    $order = new Orders_Order($order_no);
    
    //Je objednávka připravena?
    if(!$order->isReady()) {
      $this->_flashMessenger->addMessage('Objednávka není připravena.');
      $this->_redirect('orders');
    }
    
    //průběžné uložení
    $post = $this->_request->getPost();
    $tPackingsSaves = new Table_PackingsSaves();
    
    if (array_key_exists('columns-json' ,$post)) {
      $tPackingsSaves->save($post['order_id'], $post['columns-json']);
    }
    
    $tParts = new Table_Parts();
    $orders = new Table_Orders();
    
    //krabice
    $this->view->boxes     = $tParts->getProductsByCategory(Table_PartsCtg::TRANSPORT_BOXES)->query()->fetchAll();
    $this->view->palettes  = $tParts->getProductsByCategory(Table_PartsCtg::PALETTES)->query()->fetchAll();
    
    //formulář na rozdělení
    $this->view->splitForm = new Packings_SplitForm();
    
    //získání produktů
    if ($config = $tPackingsSaves->loadLast($order_no)->fetch()) {
      $data = json_decode($config['json']);
      $this->view->json_prod = $data->products;
      $this->view->json_bxes = $data->boxes;
      $this->view->json_plts = $data->palettes;
    } else {
      ///produkty z objednávky
      $this->view->products = $orders->getSortedOrderParts($order_no);
    }
    
    //kategorie k jednotlivému rozepsání (s kódem)
    $this->view->products_single_ctgs = array(Table_PartsCtg::TYPE_SETS, Table_PartsCtg::EXTRA_ACCESSORIES);
    
    //zjištění, zdali se nejedná o set s boxem
    $this->view->tPartsSubparts = new Table_PartsSubparts();
    
    //box jako součást balení
    $this->view->products_with_box = Table_PartsCtg::TRANSPORT_BOXES;
    
    //ID objednávky
    $this->view->order_id = $this->_request->getParam('id');
    
    //TESTOVÁNÍ VÝSTUPU
    $json = $this->_request->getPost('columns-json');
    if ($json) {$this->view->post = $json;}
  }
}
