<?php
class PartsController extends Meduse_Controller_Action{

    const EXTRA_ACCESORIES = 3; //id z db
    protected $db = NULL;


    public function init(){
        // load database
        $this->db = Zend_Registry::get('db');
        // nastaveni layoutu
        $this->_helper->layout->setLayout('bootstrap-basic');
    }


    public function selectWarehouseAction(){

    }

    public function indexAction() { //pridavani soucasti
      $this->_helper->layout->setLayout('bootstrap-basic');
      $this->view->title = "Vložení nové skladové položky";

      $coverTable = new Table_ApplicationCoverCost();
      $cover_cost_default = $coverTable->getCoverCost();

      $form = new Parts_Form();
      $form->setLegend($this->view->title);

      if ($this->_request->getParam('Odeslat')) {
        if ($form->isValid($this->_request->getPost())) {
          $tParts = new Table_Parts();
          $tWarehouse = new Table_PartsWarehouse();
          $description = $form->meduse_parts_desc->getValue();
          $version = $form->meduse_parts_version->getValue();

          if (empty($description)) {
            $description = new Zend_Db_Expr('NULL');
          }

          if (empty($version)) {
            $version = new Zend_Db_Expr('NULL');
          }

          $newId = $this->_request->getParam('meduse_parts_id');
          $i = 0;
          while (!is_numeric($newId[$i]) && $i < strlen($newId)) {
            $newId[$i] = strtoupper($newId[$i]);
            $i++;
          }
          $collections = $this->_request->getParam('meduse_parts_collection', false);

          $data = array(
            'id' => $newId,
            'id_wh_type' => Table_PartsWarehouse::WH_TYPE,
            'id_parts_ctg' => $this->_request->getParam('meduse_parts_category'),
            'name' => $this->_request->getParam('meduse_parts_name'),
            'description' => $description,
            'version' => $version,
            'semifinished' => $this->_request->getParam('meduse_parts_semifinished'),
            'set' => $this->_request->getParam('meduse_parts_set'),
            'cover_cost' => $this->_request->getParam('meduse_parts_cover_cost_default') == 'y' ? NULL : (float) trim($this->_request->getParam('meduse_parts_cover_cost')),
            'hs' => trim($this->_request->getParam('meduse_parts_hs')),
            'measure' => $this->_request->getParam('meduse_parts_wh_measure'),
          );

          if ($this->_request->getParam('meduse_parts_product') == 'y') {
            $data['product'] = 'y';
            $data['designated'] = Table_Customers::TYPE_B2C;
          }
          try {
            $tParts->insert($data);
            $tWarehouse->insert(array(
              "id_parts" => $this->_request->getParam('meduse_parts_id'),
              "id_users" => 1, //TODO smazat sloupec
              "amount" => 0,
            ));
            $db = $tParts->getAdapter();

            if ($collections) {
              foreach ($collections as $coll) {

                $db->insert('parts_collections', array(
                  'id_parts' => $this->_request->getParam('meduse_parts_id'),
                  'id_collections' => $coll
                ));
              }
            }

            if ($this->_request->getParam('meduse_parts_producer', false)) {
              $db->insert('producers_parts', array(
                'id_producers' => $this->_request->getParam('meduse_parts_producer'),
                'id_parts' => $this->_request->getParam('meduse_parts_id'),
                'price' => trim(str_replace(',', '.', $this->_request->getParam('meduse_parts_price'))),
                'inserted' => new Zend_Db_Expr('NOW()')
              ));
            }
          } catch (Exception $e) {
            $this->_flashMessenger->addMessage("Položka skladu '" . $this->_request->getParam('meduse_parts_id') . " - " . $this->_request->getParam('meduse_parts_name') . "' nebyla uložena.<br />Ujistěte se, že už v databázi neexistuje součást se stejným ID.<br />" . $e->getMessage());
            $this->redirect('parts');
          }
          if (($this->_request->getParam('meduse_parts_multipart')) == 'y') {
            $this->redirect("parts/select-subparts/part/" . $this->_request->getParam('meduse_parts_id'));
          } else {
            //pridavani aktuality
            $act = new Table_Actualities();
            $act->add("Vložil do skladu novou součást:<br /> " . $this->_request->getParam('meduse_parts_id') . " - " . $this->_request->getParam('meduse_parts_name') . ".");
            $this->_flashMessenger->addMessage("Položka skladu '" . $this->_request->getParam('meduse_parts_name') . "' byla uložena.");
            if ($this->_request->getParam('meduse_parts_wh_type') == Parts_Part::WH_TYPE_TOBACCO) {
              $this->redirect('/tobacco/detail/id/' . $this->_request->getParam('meduse_parts_id'));
            } else {
              $this->redirect('/parts/detail/id/' . $this->_request->getParam('meduse_parts_id') . '?cnt=true');
            }
          }
        }
      }

        $this->view->form = $form;
        $this->view->cover_cost_default = $cover_cost_default;

        $this->view->hsForm = new Parts_HsForm();

    }
    // todo: db dotaz presunout z controlleru
    public function overviewAction() {

      if ($this->_request->getParam('setExport')) {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $part = new Parts_Part($this->_request->getParam('id'));
        $row = $part->getRow();
        if ($row->export == 'y') {
          $row->export = 'n';
        } else {
          $row->export = 'y';
        }
        $row->save();
        return;
      } else {
        $this->_helper->layout->setLayout('bootstrap-basic');
      }

      $parts = new Table_Parts();
      $this->view->data = false;

      $form = new Parts_FilterForm();
      $form->setLegend('Zobrazit součásti');
      $form->removeElement('wh');
      $form->removeElement('no_multiparts');
      $db = Zend_Registry::get('db');
      $this->view->title = "Přehled skladu";

      if ($this->_request->isPost()) {
        foreach ($this->_request->getParams() as $name => $val) {
          if (preg_match("/^part_*/", $name)) {
            $partId = substr($name, 5);
            $part = new Parts_Part($partId);
            $val = str_replace(',', '.', $val);
            if ($val > 0 && $part->getPrice() != $val) {
              $part->setPrice($val);
              $part->refreshPrice();
            }
          }
        }
        $this->_flashMessenger->addMessage("Uloženo.");

        $params = '?Hledat=Hledat&';
        if ($this->_request->getParam('coll', 'null') != 'null') {
          $colls = $this->_request->getParam('coll');
          if (is_array($colls)) {
            foreach ($colls as $coll) {
              $params .= "coll[]=" . $coll . "&";
            }
          }
        }
        if ($this->_request->getParam('ctg') != 'null') {
          $params .= "ctg=" . $this->_request->getParam('ctg') . "&";
        }
        if ($this->_request->getParam('name') != '') {
          $params .= "name=" . $this->_request->getParam('name') . "&";
        }
        if ($this->_request->getParam('id') != '') {
          $params .= "id=" . $this->_request->getParam('id') . "&";
        }
        if ($this->_request->getParam('producer') != '') {
          $params .= "producer=" . $this->_request->getParam('producer') . "&";
        }
        $this->_redirect('parts/overview/' . $params);
      }

      $this->view->show = true;
      if ($this->_request->getParam('Hledat') == 'Hledat') { //zadane hleadani
        $form->getElement('coll')->setValue($this->_request->getParam('coll'));
        $form->getElement('ctg')->setValue($this->_request->getParam('ctg'));
        $form->getElement('name')->setValue($this->_request->getParam('name'));
        $form->getElement('id')->setValue($this->_request->getParam('id'));
        $form->getElement('prod')->setValue($this->_request->getParam('prod'));
        $form->getElement('producer')->setValue($this->_request->getParam('producer'));

        $db = Zend_Registry::get('db');
        $subSelect = new Zend_Db_Select($db);
        $subSelect
          ->from('parts_collections')
          ->where('id_parts = p.id')
          ->where('id_collections IN (?)', $this->_request->getParam('coll'));

        $select = $parts->getDatalist(true);
        if ($this->_request->getParam('coll')) {
          $select->where('EXISTS(' . $subSelect . ')');
        }

        if ($this->_request->getParam('ctg', 'null') != 'null') {
          $select->where("p.id_parts_ctg = ?", $this->_request->getParam('ctg'));
        }

        if ($this->_request->getParam('name', '') != '') {
          $select->where("p.name LIKE ?", "%" . $this->_request->getParam('name') . "%");
        }

        if ($this->_request->getParam('id', '') != '') {
          $select->where("p.id = ?", $this->_request->getParam('id'));
        }

        if ($this->_request->getParam('id', '') != '') {
          $select->where("p.id = ?", $this->_request->getParam('id'));
        }

        if ($this->_request->getParam('prod', '') == 'y') {
          $select->where("p.product = 'y'");
        }

        $producerId = (int) $this->_request->getParam('producer', false);
        if ($producerId) {
          $select->where("EXISTS(SELECT * FROM producers_parts pp WHERE pp.id_parts = p.id AND id_producers = " . $producerId . ")");
        }

        $select->where('p.id_wh_type = ?', Pipes_Parts_Part::WH_TYPE);

        $this->view->data = $db->fetchAll($select);
        $this->view->show = false;
      }
      $this->view->form = $form;
    }

    public function warehouseAction(){

        $this->_helper->layout->setLayout('bootstrap-basic');
        $this->view->wh_type = 'base';
        $this->view->extern = false;

        $parts_warehouse = new Table_PartsWarehouse();
        $orders_products_parts = new Table_OrdersProductsParts();
        $table_parts = new Table_Parts();
        $this->view->title = "Sklad";

        $form = new Parts_FilterForm();
        $form->setLegend('Zobrazit součásti');
        $this->view->show = true;
        if ($this->_request->getParam('new')){
            Zend_Registry::get('parts_warehouse')->last_search = null;
            $this->_redirect('parts/warehouse');
        } elseif($this->_request->getParam('Hledat') == 'Hledat'){ //zadane hleadani
            $this->view->show = false;
            $form->getElement('wh')->setValue($this->_request->getParam('wh'));
            $form->getElement('coll')->setValue($this->_request->getParam('coll'));
            $form->getElement('ctg')->setValue($this->_request->getParam('ctg'));
            $form->getElement('name')->setValue($this->_request->getParam('name'));
            $form->getElement('id')->setValue($this->_request->getParam('id'));
            $form->getElement('prod')->setValue($this->_request->getParam('prod'));
            $form->getElement('producer')->setValue($this->_request->getParam('producer'));
            $form->getElement('no_multiparts')->setValue($this->_request->getParam('no_multiparts'));

                        switch ($this->_request->getParam('wh')) {
                            case 'null':
                                $this->view->wh_type = 'all';
                                $this->view->extern = false;
                                break;
                            case 'base':
                                $this->view->wh_type = 'base';
                                $this->view->extern = false;
                                break;
                            default:
                                $this->view->wh_type = 'ext';
                                $this->view->extern = true;
                                break;
                        }

            $this->view->wh_data = $parts_warehouse->getFilteredWarehouseStatus(
                $this->_request->getParam('wh'),
                $this->_request->getParam('coll'),
                $this->_request->getParam('ctg'),
                $this->_request->getParam('name'),
                $this->_request->getParam('id'),
                $this->_request->getParam('prod'),
                $this->_request->getParam('producer'),
                $this->_request->getParam('no_multiparts')
            );

            Zend_Registry::get('parts_warehouse')->last_search = "wh=".$this->_request->getParam('wh').
                "|coll=".serialize($this->_request->getParam('coll')).
                "|ctg=".$this->_request->getParam('ctg').
                "|name=".$this->_request->getParam('name').
                "|id=".$this->_request->getParam('id').
                "|prod=".$this->_request->getParam('prod').
                "|producer=".$this->_request->getParam('producer').
                "|no_multiparts=".$this->_request->getParam('no_multiparts');
        } elseif(!is_null(Zend_Registry::get('parts_warehouse')->last_search)) { //posledni hledani ze session
            $this->view->show = false;
            $last_search = Zend_Registry::get('parts_warehouse')->last_search;
            $last_search = explode('|', $last_search);
            $search_params = array();
            foreach($last_search as $value){
                $tmp = explode('=',$value);
                if($tmp[0] == 'coll'){
                    $search_params[$tmp[0]] = unserialize($tmp[1]);
                } else {
                    $search_params[$tmp[0]] = $tmp[1];
                }

            }
            foreach($search_params as $name => $value){
                $form->getElement($name)->setValue($value);
            }

            $this->view->wh_data = $parts_warehouse->getFilteredWarehouseStatus(
                $search_params['wh'],
                $search_params['coll'],
                $search_params['ctg'],
                $search_params['name'],
                $search_params['id'],
                $search_params['prod'],
                $search_params['producer'],
                $search_params['no_multiparts']
            );

        }

        if($this->_request->getParam('Odeslat') == 'Odeslat'){
            $db = Zend_Registry::get('db');
            $text = '';
            $preferCentral = (bool) $this->_request->getParam('prefer_central', false);
            try{
                foreach($this->_request->getUserParams() as $name => $value){
                    if(substr($name, -7) == '_amount'){
                        if(preg_match('/^[0123456789]*$/', $value) && $value > 0){
                            $db->beginTransaction();
                            $id = explode('_', $name);
                            $id = $id[0];
                            $mess = $parts_warehouse->addPart($id, $value, $preferCentral);
                            if($mess != '') $mess = $mess."<br />";
                            $text .= $id." ".$value."x, $mess";
                            $db->commit();
                        }
                    }
                }
                $this->_flashMessenger->addMessage('Vloženo: '.$text);
                //pridavani aktuality
                $act = new Table_Actualities();
                $act->add("Vložil do skladu:<br />$text");
                $this->_redirect('parts/warehouse');
            } catch (Parts_Exceptions_LowAmount $e) {
                $db->rollback();
                $this->_flashMessenger->addMessage($e->getMessage());
                $this->_redirect('parts/warehouse');
            } catch (Parts_Exceptions_MultipleExternWH $e) {
                $db->rollback();
                $this->_flashMessenger->addMessage($e->getMessage());
                $this->_redirect('parts/warehouse');
            } catch (Exception $e) {
                $db->rollback();
                throw $e;
                $this->_flashMessenger->addMessage("Stav skladu se nepodařilo změnit.".$e->getMessage());
                $this->_redirect('parts/warehouse');
            }
        }

        $this->view->form = $form;
        $this->view->dialogForm = new Parts_DialogForm();
        $this->view->simple_parts = $table_parts->getSimplePartsArray();
        $this->view->ready_amounts = $orders_products_parts->getAllAmounts();
    }


    public function selectSubpartsAction()
    {
        // set layout
        $this->_helper->layout->setLayout('bootstrap-basic');

        /**
         * Get param
         */
        $id = $this->_request->getParam('part');

        /**
         * Form
         */
        $form = new Parts_SelectSubpartsForm();
        $form->setLegend('Filtr');

        /**
         * To view
         */
        $this->view->part = new Parts_Part($id);
        $this->view->form = $form;
    }

    public function selectSubpartsListAction() {

        $this->_helper->layout->disableLayout();
        $part = $this->_request->getParam('part');
        $this->view->part = new Parts_Part($part);
        $tParts = new Table_Parts();

        if($this->_request->getParam('add')){
            //pridavani podsoucasti
            $newSubpart = new Parts_Part($this->_request->getParam('add'));
            $tSubparts = new Table_PartsSubparts();

            $row = $tSubparts->fetchRow($tSubparts->select()->where('id_multipart = ?', $this->view->part->getID())->where('id_parts = ?', $newSubpart->getID()));
            if($row !== null){
                //zmena poctu
                $tSubparts->update(array(
                    "amount" => $this->_request->getParam("amount")
                ),"id_multipart = '".$this->view->part->getID()."' AND id_parts = '".$newSubpart->getID()."'" );
            } else {
                $data = array(
                    "id_multipart" => $this->view->part->getID(),
                    "id_parts" => $newSubpart->getID(),
                    "amount" => $this->_request->getParam("amount"),
                );
                $tSubparts->insert($data);
                $tParts->update(array("multipart" => 'y'), "id LIKE '".$this->view->part->getID()."'");
            }



        }

        $ctg  = (int) $this->_request->getParam('ctg');
        $name = $this->_request->getParam('name');
        $coll = $this->_request->getParam('coll');

        if(empty($ctg) && empty($name) && empty($coll)){
            echo "<br />- nic nevybráno - <br /><br />";
            return;
        }


        $tCtg = new Table_PartsCtg();

        $subParts = $tParts->getSubpartsById($part);
        $subPartsArray = array();
        foreach($subParts as $tmpPart){
            $subPartsArray[$tmpPart['id_parts']] = $tmpPart['amount'];
        }
        $this->view->subparts = $subPartsArray;

        //kategorie
        $this->view->ctg = $tCtg->getCategoryName($ctg);

        $select = $tParts->select();
        if($ctg > 0) {
            $select->where("id_parts_ctg = ? OR id_parts_ctg IN (SELECT id FROM parts_ctg WHERE parent_ctg = $ctg)", $ctg);
        }
        if(!empty($name)){
          $select->where("name LIKE ? or id LIKE ?", '%' . $name . '%', '%' . $name . '%');
        }
        if(!empty($coll)){
            $subSelect = new Zend_Db_Select($select->getAdapter());
            $subSelect
              ->from('parts_collections')
              ->where('id_parts = parts.id')
              ->where('id_collections IN (?)', $coll);
            $select->where("EXISTS($subSelect)");
        }
        $select->where('parts.id <> ?', $part);
        $rows = $tParts->fetchAll($select);
        
        $this->view->data = $rows;
    }

    public function detailAction(){
        // set layout
        $this->_helper->layout->setLayout('bootstrap-basic');

        /**
         * Get ID
         */
        $id = $this->_request->getParam('id');
        $cnt = $this->_request->getParam('cnt');

        /**
         * Models
         */
        try {
          $part = new Pipes_Parts_Part($id);
        }
        catch (Parts_Exceptions_BadWH $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
          $this->_redirect('parts/warehouse');
        }
        $partsForm = new Parts_FilterForm();

        /**
         * Tables
         */
        $tPartsWarehouse = new Table_PartsWarehouse();
        $tPartsOperations = new Table_PartsOperations();
        $tProductions = new Table_Productions();
        $tDph = new Table_ApplicationDph();
        $tEans = new Table_Eans();

        /**
         * Recalculate price
         */
        if($cnt){

            $oldNettoPrice = $part->getPrice() - $part->getOperationSum();
            $part->setPrice($oldNettoPrice);
            $part->refreshOperations(TRUE);
            $part->refreshPrice();
        }

        /**
         * Set extern warehouses
         */
        if($tPartsWarehouse->getAmount($part->getID()) != $tPartsWarehouse->getBaseAmount($part->getID())){
            $pwh = new Table_PartsWarehouseExtern();
            $pwh_overview = $pwh->getOverview($part->getID());
        }

        /**
         * Load form
         */
        $form = new Meduse_FormBootstrap();


        /**
         * Fill checkboxes for collection
         */
        $activeCollections = $this->_db->fetchCol("SELECT c.id FROM collections AS c JOIN parts_collections AS pc ON c.id = pc.id_collections AND pc.id_parts = ".$this->_db->quote($part->getID()));
        $form->addElement($partsForm->coll);
        if(count($activeCollections) > 0){
            $form->populate(array('coll' => $activeCollections));
        }

        /**
         * ACL
         */
        if(!Zend_Registry::get('acl')->isAllowed('parts/special/collections')){
            $form->getElement('coll')->setAttrib('disabled', 'disabled');
        }

        /**
         * EAN a slevové kategorie
         */
        if ($part->isProduct()) {
            $this->view->ean = $tEans->getCode($part->getID());
            $this->view->pricelists_ctg = $part->getPricelistsCtg();
        }

        /**
         * To view
         */
        $this->view->title = $part->getID()." ".$part->getName();
        $this->view->actual_dph = $tDph->getActualDph();
        $this->view->coll = $form;
        $this->view->part = $part;
        $this->view->amount = $tPartsWarehouse->getAmount($part->getID());
        $this->view->operationsSum = $tPartsOperations->getOperationsSum($part->getID());
        $this->view->operations = $tPartsOperations->getAdapter()->fetchAll($tPartsOperations->getOperations($part->getID()));
        $this->view->productions = $tProductions->getProductions($part->getID());
        $this->view->extern_warehouses = (isset($pwh_overview)) ? $pwh_overview : null;

      // Určenost produktu.
      try {
        $designatedFor = $part->getDesignatedFor() ?? Table_Customers::TYPE_B2C;
        $designatedForToggle = $designatedFor === Table_Customers::TYPE_B2B ? Table_Customers::TYPE_B2C : Table_Customers::TYPE_B2B;
      }
      catch (Parts_Exceptions_PartIsNotProduct $e) {
        $designatedFor = NULL;
        $designatedForToggle = NULL;
      }
      $this->view->designatedFor = $designatedFor;
      $this->view->designatedForToggle = $designatedForToggle;
    }

    /**
     * Recalculate price
     */
    public function recalculateAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      $id = $this->_request->getParam('id');
      if ($id) {
        try {
          $part = new Pipes_Parts_Part($id);
          $part->recalculate();
          $this->_flashMessenger->addMessage('Cena byla přepočítána.');
        }
        catch (Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
        }
        $this->redirect($this->view->url(array('action' => 'detail')));
      }
      else {
        $table = new Table_Parts();
        if ($parts = $table->checkCosts()) {
          $recalculated = [];
          foreach ($parts as $partId => $item) {
            $part = new Parts_Part();
            $part->recalculate();
            $recalculated[] = $partId;
          }
          if ($recalculated) {
            $this->_flashMessenger->addMessage('Bylo přepočítáno '
              . count($recalculated) . ' položek: '
              . implode(', ', $recalculated));
          }
        }
        $this->redirect($this->view->url(array('action' => 'check-costs')));
      }
    }

    public function trashAction() {
        /**
         * Set title
         */
        $this->view->title = "Koš součástí";

        /**
         * Trash data
         */
        $data = Parts::getTrashData(Parts_Part::WH_TYPE_PIPES);

        /**
         * To view
         */
        $this->view->data = $data;
    }

    public function ajaxSetCollectionAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if($this->_request->getParam('coll') && $this->_request->getParam('part')){
            $part = new Parts_Part($this->_request->getParam('part'));
            $part->toggleCollection($this->_request->getParam('coll'));
        }
    }

    public function toggleSetAction(){
        $this->_helper->viewRenderer->setNoRender();
        if(!$this->_request->getParam('id')) $this->_redirect('parts/warehouse');

        try{
            $part = new Parts_Part($this->_request->getParam('id'));
            $part->toggleSet();
            $act = new Table_Actualities();
            $act->add("Nastavil vlastnost set součásti pro ".$part->getID());
            $this->_flashMessenger->addMessage("Položka skladu byla úspěšně upravena.");
        } catch ( Exception $e ) {
            $this->_flashMessenger->addMessage("Položku skladu se nepodařilo upravit.");
        }
        $this->_redirect('parts/detail/id/'.$this->_request->getParam('id'));
    }

    public function toggleSemifinishedAction(){
        $this->_helper->viewRenderer->setNoRender();
        if(!$this->_request->getParam('id')) $this->_redirect('parts/warehouse');

        try{
            $part = new Parts_Part($this->_request->getParam('id'));
            $part->toggleSemifinished();
            $act = new Table_Actualities();
            $act->add("Nastavil vlastnost polotovar součásti pro ".$part->getID());
            $this->_flashMessenger->addMessage("Položka skladu byla úspěšně upravena.");
        } catch ( Exception $e ) {
            $this->_flashMessenger->addMessage("Položku skladu se nepodařilo upravit.");
        }
        $this->_redirect('parts/detail/id/'.$this->_request->getParam('id'));
    }

    public function toggleProductAction(): void {
        $this->_helper->viewRenderer->setNoRender();
        if(!$this->_request->getParam('id')) {
            $this->redirect('parts/warehouse');
        }

        $part = null;
        try {
            $part = new Parts_Part($this->_request->getParam('id'));
            $part->toggleProduct();
            $act = new Table_Actualities();
            $act->add("Nastavil vlastnost produkt pro " . $part->getID());
            $this->_flashMessenger->addMessage("Položka skladu byla úspěšně upravena.");
        } catch (Parts_Exceptions_InActiveOrders $e) {
            $message = 'Nelze odstranit příznak produktu, protože je součástí těchto aktivních objednávek:<ul>';
            foreach ($e->orders as $item) {
                $url = $this->view->url(['controller' => 'orders', 'action' => 'detail', 'id' => $item['id']]);
                $message .= sprintf('<li><a href="%s" target="_blank">č.%d</a>: %s (%d ks)</li>', $url, $item['no'], $item['title'], $item['amount']);
            }
            $message .= '</ul>';

            $this->_flashMessenger->addMessage($message);
        } catch (Exception $e) {
            $this->_flashMessenger->addMessage("Položku skladu se nepodařilo upravit.");
        }
		if ($part !== null && $part->isTobacco()) {
            $this->redirect('tobacco/detail/id/' . $this->_request->getParam('id'));
        }
        else {
            $this->redirect('parts/detail/id/' . $this->_request->getParam('id'));
        }
    }

    public function editAction() {
      $this->_helper->layout->setLayout('bootstrap-basic');
      if (!$this->_request->getParam('id'))
        $this->redirect('parts/overview');
      $this->view->id = $this->_request->getParam('id');
      $form = new Parts_EditForm();
      $form->setLegend('Editace položky ' . $this->_request->getParam('id'));
      $form->removeElement('meduse_parts_collection');
      $part = new Parts_Part($this->_request->getParam('id'));

      if (!$part->isProduct()) {
        $form->removeElement('meduse_parts_hs');
        $form->removeElement('meduse_parts_ean');
        $form->removeElement('meduse_parts_cover_cost');
        $form->removeElement('meduse_parts_cover_cost_default');
      }

      $allowPriceEdit = false; //vedouci skladnik muze menit jen ceny NE-produktu
      if ($part->isProduct()) {
        if (
          isset($_SESSION['Zend_Auth']['user_role']) && (
            in_array('manager', $_SESSION['Zend_Auth']['user_role']) ||
            in_array('admin', $_SESSION['Zend_Auth']['user_role'])
          )
        ) {
          $allowPriceEdit = true;
        }
      }
      if (
        isset($_SESSION['Zend_Auth']['user_role']) && (
          in_array('manager', $_SESSION['Zend_Auth']['user_role']) ||
          in_array('head_warehouseman', $_SESSION['Zend_Auth']['user_role']) ||
          in_array('admin', $_SESSION['Zend_Auth']['user_role'])
        )
      ) {
        $allowPriceEdit = true;
      }
      // Cena soucasti
      if (Zend_Registry::get('acl')->isAllowed('parts/detail/price') && $allowPriceEdit) {

        if (!$part->getProducer()) {
          $form->removeElement('meduse_parts_price');

        }

        if ($part->isProduct()) {
          $form->getElement('Odeslat')->setOrder(60);
        } else {
          if (!is_null($form->getElement('meduse_parts_price'))) {
            $form->getElement('meduse_parts_price')->setOrder(6);
          }
          $form->getElement('Odeslat')->setOrder(60);
        }
      }

      if (!Zend_Registry::get('acl')->isAllowed('parts/price-cover')) {
        $form->removeElement('meduse_parts_cover_cost');
        $form->removeElement('meduse_parts_cover_cost_default');
      }

      if ($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())) {
        $data = array(
          'id_wh_type' => $form->meduse_parts_wh_type->getValue(),
          'id_parts_ctg' => $form->meduse_parts_category->getValue(),
          'name' => $form->meduse_parts_name->getValue(),
          'description' => $form->meduse_parts_desc->getValue(),
          'version' => $form->meduse_parts_version->getValue(),
          'measure' => $form->meduse_parts_wh_measure->getValue()
        );

        if ($part->isProduct()) {
          $data['hs'] = $form->meduse_parts_hs->getValue();
          if (Zend_Registry::get('acl')->isAllowed('parts/price-cover')) {
            $cover_cost = $form->meduse_parts_cover_cost->getValue();
            if ($form->meduse_parts_cover_cost_default->getValue()=='y') {
              $data['cover_cost'] = NULL;
            } else {
              $data['cover_cost'] = Meduse_Currency::parse($cover_cost);
            }
          }
        }
        // Cena soucasti
        if (Zend_Registry::get('acl')->isAllowed('parts/detail/price') && $allowPriceEdit) {
          if ($part->getProducer()) {
            $price = ($part->getProducer()) ? $form->meduse_parts_price->getValue() : 0;
            $part->setProducerPrice($price);
          }
        }

        $parts = new Table_Parts();
        $ean = new Table_Eans();

        try {
          if ($part->isProduct()) {
            if ($form->meduse_parts_ean->getValue()) {
              $ean->addCode($this->_request->getParam('id'), $form->meduse_parts_ean->getValue());
            } else {
              $ean->removeCode($this->_request->getParam('id'));
            }
          }
          $parts->update($data, "id LIKE '" . $this->_request->getParam('id') . "'");
          if (Zend_Registry::get('acl')->isAllowed('parts/detail/price') && $allowPriceEdit) {
            $part->refreshPrice();
          }
          $this->_flashMessenger->addMessage("Položka skladu upravena.");
        }
        catch (Zend_Db_Statement_Exception $e) {
          $this->_flashMessenger->addMessage($e->getMessage());
          $this->redirect('parts/edit/id/' . $this->_request->getParam('id'));
        }
        catch (Exception $e) {
          throw $e;
          $this->_flashMessenger->addMessage("Položka skladu se nepodařilo upravit.");
        }
        if ($part->isTobacco())
          $this->redirect('tobacco/detail/id/' . $this->_request->getParam('id'));
        else
          $this->redirect('parts/detail/id/' . $this->_request->getParam('id') . '/cnt/1');
      }

      $form->meduse_parts_wh_type->setValue($part->getWarehouseTypeId());
      $form->meduse_parts_wh_measure->setValue($part->getRow()->measure);
      $form->meduse_parts_category->setValue($part->getCtg());
      $form->meduse_parts_name->setValue($part->getName());
      $form->meduse_parts_desc->setValue($part->getDesc());
      $form->meduse_parts_version->setValue($part->getVersion());
      if ($part->isProduct()) {
        $form->meduse_parts_hs->setValue($part->getRow()->hs);
        $ean = new Table_Eans();
        if (is_object($ean->getCode($this->_request->getParam('id')))) {
          $form->meduse_parts_ean->setValue($ean->getCode($this->_request->getParam('id'))->code);
        }
        if (Zend_Registry::get('acl')->isAllowed('parts/price-cover')) {
          if (is_string($part->getCoverCost(FALSE, FALSE)) AND strlen($part->getCoverCost(FALSE, FALSE))=='0') {
            $form->meduse_parts_cover_cost->setValue('');
            $form->meduse_parts_cover_cost->setAttrib('disabled', 'disabled');
            $form->meduse_parts_cover_cost_default->setValue('y');
          } else {
            $form->meduse_parts_cover_cost->setValue($part->getCoverCost(FALSE, FALSE));
          }
        }
      }

      // Cena soucasti
      if (Zend_Registry::get('acl')->isAllowed('parts/detail/price') && $part->getProducer() && $allowPriceEdit) {
        $form->meduse_parts_price->setValue($part->getProducerPrice());
      }

      $coverTable = new Table_ApplicationCoverCost();
      $this->view->cover_cost_default = $coverTable->getCoverCost();
      $this->view->form = $form;

      $this->view->hsForm = new Parts_HsForm();
    }

    public function editSubphaseAction(){
        if(!$this->_request->getParam('part')) $this->_redirect('parts/overview');
        if(!$this->_request->getParam('id')) $this->_redirect('parts/overview');
        $this->_helper->layout->setLayout('bootstrap-basic');
        $this->view->title = "Úprava mezifáze";
        $part = new Parts_Part($this->_request->getParam('part')); //validace id
        $form = new Parts_SubphaseForm();
        $form->setLegend($this->view->title);
        $form->id_parts->setValue($this->_request->getParam('part'));

        if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
            $parts_subphases = new Table_PartsSubphases();
            $parts_subphases->update(array(
                'id_parts' => $this->_request->getParam('part'),
                'name' => $form->meduse_subphase_name->getValue(),
                'description' => $form->meduse_subphase_desc->getValue(),
                'amount' => $form->meduse_subphase_amount->getValue(),
            ), "id = ".$parts_subphases->getAdapter()->quote($this->_request->getParam('id')));
            $this->_flashMessenger->addMessage("Mezifáze ".$form->meduse_subphase_name->getValue()." upravena.");
            $this->_redirect('parts/detail/id/'.$this->_request->getParam('part'));
        } else {
            $parts_subphases = new Table_PartsSubphases();
            $select = $parts_subphases->select()->where("id = ?", $this->_request->getParam('id'));
            $row = $parts_subphases->fetchRow($select);
            $form->meduse_subphase_name->setValue($row->name);
            $form->meduse_subphase_desc->setValue($row->description);
            $form->meduse_subphase_amount->setValue($row->amount);
        }

        $this->view->form = $form;

        return $this->render('add-subphase');
    }

    public function editSubphaseAmountAction(){
        if(!$this->_request->getParam('id')) $this->_redirect('parts/overview');
        if(!$this->_request->getParam('part')) $this->_redirect('parts/overview');
        $this->_helper->layout->setLayout('bootstrap-basic');
        $part = new Parts_Part($this->_request->getParam('part'));
        $subphases = $part->getSubphases();
        $form = new Parts_EditSubphaseAmountForm();
        $form->setLegend("Upravit množství mezifáze");
        $form->getElement("meduse_subphase_amount")->setValue($subphases[$this->_request->getParam('id')]['amount']);

        if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
            $parts_subphases = new Table_PartsSubphases();
            $parts_subphases->update(array('amount' =>  $form->meduse_subphase_amount->getValue()), "id = ".$this->_request->getParam('id'));
            $this->_flashMessenger->addMessage("Počet mezifáze upraven.");
            $this->_redirect('parts/detail/id/'.$this->_request->getParam('part'));
        }

        $this->view->form = $form;

        return $this->render('add-subphase');

    }

    public function deleteSubphaseAction(){
        if(!$this->_request->getParam('id')) $this->_redirect('parts/overview');
        if(!$this->_request->getParam('part')) $this->_redirect('parts/overview');
        try{
            $parts_subphases = new Table_PartsSubphases();
            $parts_subphases->delete("id = ".$parts_subphases->getAdapter()->quote($this->_request->getParam('id')) );
            $this->_flashMessenger->addMessage("Mezifáze ostraněna.");
        } catch ( Exception $e ) {
            $this->_flashMessenger->addMessage("Mezifázi se nepodařilo odstranit.".$e->getMessage());
        }
        $this->_redirect('parts/detail/id/'.$this->_request->getParam('part'));
        $this->_helper->viewRenderer->setNoRender();
    }

    public function deleteSubPartAction(){
        if(!$this->_request->getParam('part')) $this->_redirect('parts/overview');
        if(!$this->_request->getParam('subpart')) $this->_redirect('parts/overview');
        $parts_warehouse = new Table_PartsWarehouse();
        $part = new Parts_Part($this->_request->getParam('part'));
        $part->deleteSubPart($this->_request->getParam('subpart'));
        $part->checkMultipart();
        if(($wh_amount = $parts_warehouse->getAmount($part->getID())) > 0){
            $this->_flashMessenger->addMessage("Podsoučást ".$this->_request->getParam('subpart')." z ".$part->getID()." ostraněna. Ale součást ".$part->getID()." byla {$wh_amount}x skladem, zvažte naskladnění odstraněné podsoučásti ve stejném množství.");
            $this->_flashMessenger->addMessage("<br />nebo můžete zvolit možnost: <br /><br /><a href='".$this->view->url(array('controller'=>'parts','action'=>'detail', 'id' => $this->_request->getParam('part')), null, true, true)."'>Nepřidávat na sklad</a>");
            $this->_redirect('parts/add-part/part/'.$this->_request->getParam('subpart')."/super/".$this->_request->getParam('part')."/amount/".$this->_request->getParam('amount'));
        } else {
            $this->_flashMessenger->addMessage("Podsoučást ".$this->_request->getParam('subpart')." ostraněna.");
            $this->_redirect('parts/detail/id/'.$this->_request->getParam('part'));
        }
        $this->_helper->viewRenderer->setNoRender();
    }

    /**
     * akce, ktera vznikla kvuli tomu, ze bylo treba naskladnovat soucasti potom co se odmazou
     * jako obsazene podsoucasti z jine soucasti
     */
    public function addPartAction(){

        if (!$this->_request->getParam('part')) {
          $this->_redirect('parts/overview');
        }
        $part = new Parts_Part($this->_request->getParam('part'));
        $parts_warehouse = new Table_PartsWarehouse();


        if ($this->_request->getParam('super')) {
            $super_amount = $parts_warehouse->getAmount($this->_request->getParam('super'));
        }
        else {
            $super_amount = 0;
        }

        if ($part->getSubparts() !== NULL) {
            $this->view->has_subparts = TRUE;
            $this->view->part_id = $part->getID();
        }

        $form = new Parts_AddPartForm(array('data' => array(
          'id' => $this->_request->getParam('part'),
          'super_amount' => $super_amount,
          'amount' => $this->_request->getParam('amount'),
          )
        ));



        if ($this->_request->isPost() && $form->isValid($this->_request->getParams())){
            try {
              $parts_warehouse->addPart($part->getID(), $form->meduse_add_amount->getValue());
              //pridavani aktuality
              $act = new Table_Actualities();
              $act->add("Přidal na sklad ".$form->meduse_add_amount->getValue()." ks ".$part->getID());
              $this->_flashMessenger->addMessage("Přidáno ".$form->meduse_add_amount->getValue()." ks ".$part->getID());
            }
            catch (Parts_Exceptions_LowAmount $e) {
              $this->_flashMessenger->addMessage("Součást nebyla naskladněna. "
                . $e->getMessage());
            }
            $this->_redirect('parts/detail/id/'.$this->_request->getParam('part'));
        }
        $this->view->form = $form;
        $this->render('add-production');
    }

    public function deletePartAction(){
      if (!$partId = $this->_request->getParam('id')) {
        $this->redirect('parts/overview');
      }
      $part = new Parts_Part($partId);
      $affectedOrders = [];
      try {
        $affectedOrders = $part->delete();
      }
      catch (Parts_Exceptions_InReservations $e) {
        $amounts = $part->getReservationsAmount();
        $ordersIds = array_keys($amounts);
        $orders = array();
        foreach ($ordersIds as $id) {
          $order = new Orders_Order($id);
          $orders[] = '<a href="/orders/prepare/id/' . $order->getID() . '">'
            . 'obj. č.' . $order->getNO() . ' – ' . $order->getTitle() . '</a>';
        }
        $msg = "Položku skladu $partId se nepodařilo odstranit, protože je rezervovaná v těchto objednávkách:<br>"
          . implode('<br>', $orders);
        $this->_flashMessenger->addMessage($msg);
        $this->redirect('parts/detail/id/' . $partId);
      }
      catch (Parts_Exceptions_OnStock $e) {
        $msg = "Položku skladu ID = $partId se nepodařilo odstranit, protože je stále na skladě.";
        $this->_flashMessenger->addMessage($msg);
        $this->redirect('parts/detail/id/' . $partId);
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage("Položku skladu ID = $partId se nepodarilo odstranit. "
          . $e->getMessage());
        $this->redirect('parts/detail/id/' . $partId);
      }
      $this->_flashMessenger->addMessage("Položka skladu ID = $partId byla ostraněna.");
      if ($affectedOrders) {
        $message = 'Zároveň byl tento produkt odebrán z následujících objednávek: <ul>';
        foreach ($affectedOrders as $item) {
          $url = $this->view->url(['controller' => 'orders', 'action' => 'detail', 'id' => $item['id']]);
          $message .= "<li><a href='{$url}'>#{$item['no']}</a>: {$item['title']}</li>";
        }
        $message .= '</ul>';
        $this->_flashMessenger->addMessage($message);
      }
      $this->redirect('parts/trash');
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
    }

    public function restorePartAction() {

      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();

      if (!$partId = $this->_request->getParam('id')) {
        $this->redirect('parts/trash');
      }

      /** @var \Zend_Db_Adapter_Abstract $db */
      $db = Zend_Registry::get('db');
      try {
        $db->beginTransaction();
        $db->update('parts', ['deleted' => 'n'], 'id = ' . $db->quote($partId));
        $db->insert('parts_warehouse', ['id_parts' => $partId, 'id_users' => Utils::getCurrentUserId(), 'amount' => 0]);
        $db->commit();
        $this->_flashMessenger->addMessage("Součást $partId byla obnovena z koše.");
        $this->redirect('parts/detail/id/' . $partId);
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage("Součást $partId se nepodařilo obnovit. " . $e->getMessage());
        $this->redirect('parts/trash');
      }
    }

    public function deleteProductionsAction(){
        if(!$this->_request->getParam('id')) $this->_redirect('parts/productions');
        $db = Zend_Registry::get('db');
        $db->delete('parts_productions', 'id = '.$db->quote($this->_request->getParam('id')));
        $this->_flashMessenger->addMessage("Výroba smazána.");
        $this->_redirect('parts/productions');
        $this->_helper->viewRenderer->setNoRender();
    }

    public function addSubphaseAction(){
        $this->_helper->layout->setLayout('bootstrap-basic');
        if(!$this->_request->getParam('part')) $this->_redirect('parts/overview');
        $this->view->title = "Přidat mezifázi";
        $part = new Parts_Part($this->_request->getParam('part')); //validace id
        $form = new Parts_SubphaseForm();
        $form->setLegend($this->view->title);
        $form->id_parts->setValue($this->_request->getParam('part'));

        if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
            $parts_subphases = new Table_PartsSubphases();
            $parts_subphases->insert(array(
                'id_parts' => $this->_request->getParam('part'),
                'name' => $form->meduse_subphase_name->getValue(),
                'description' => $form->meduse_subphase_desc->getValue(),
                'amount' => $form->meduse_subphase_amount->getValue()
            ));
            $this->_flashMessenger->addMessage("Mezifáze ".$form->meduse_subphase_name->getValue()." byla úspěšně vložena.");
            $this->_redirect('parts/detail/id/'.$this->_request->getParam('part'));
        }

        $this->view->form = $form;
    }

    public function addProductionAction() {
      $this->_helper->layout->setLayout('bootstrap-basic');
      $this->view->title = "Zadavání výroby";

      if ($this->_request->getParam('part')) {

        $this->_redirect($this->view->url(array('action' => 'overview', 'controller' => 'parts'), null, true));
      } elseif ($this->_request->getParam('operation')) {
        //zadavani operace
        //kontrola prirazeni dodavatele
        $operation = $this->_db->fetchRow("SELECT * FROM parts_operations WHERE id = :id", array('id' => $this->_request->getParam('operation')));
        if ($operation && empty($operation['id_producers'])) {
          $this->_flashMessenger->addMessage("Zpracování operace nelze zadat. Operace nemá přiřazeného dodavatele.");
          $this->_redirect($this->view->url(array('action' => 'edit-operation', 'controller' => 'parts', 'part' => $operation['id_parts'], 'id' => $operation['id']), null, true));
        }

        //kontrola prirazeni externiho skladu dodavateli
        if ($operation && !empty($operation['id_producers'])) {
          $prdcr = $this->_db->fetchRow("SELECT * FROM producers WHERE id = :id", array('id' => $operation['id_producers']));
          if ($prdcr && empty($prdcr['id_extern_warehouses'])) {
            $this->_flashMessenger->addMessage("Zpracování operace nelze zadat. Dodavatel nemá přiřazen externí sklad.");
            $this->_redirect($this->view->url(array('action' => 'edit', 'controller' => 'producers', 'id' => $operation['id_producers']), null, true));
          }
        }
      } else {
        //zadavam bud vyrobu soucasti nebo operace, jinak nic
        $this->_redirect($this->view->url(array('action' => 'overview', 'controller' => 'parts'), null, true));
      }

      $form = new Parts_ProductionsForm();
      $form->setLegend($this->view->title);
      $form->removeElement('meduse_production_ordering');
      $form->removeElement('meduse_production_delivery');

      $date = new Zend_Date();

      if ($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())) {
        if ($idPart = $this->_request->getParam('part')) {
          $part = new Parts_Part($idPart);
          $this->_redirect($this->view->url(array('action' => 'show-order', 'controller' => 'producers', 'p_' . $part->getID() => (int) $form->getElement('meduse_production_amount')->getValue()), null, true));
        } else {
          $this->_redirect($this->view->url(array('action' => 'show-order', 'controller' => 'producers', 'o_' . $this->_request->getParam('operation') => (int) $form->getElement('meduse_production_amount')->getValue()), null, true));
        }
      }

      $this->view->form = $form;
    }

  public function editProductionsAction() {
    // set layout
    $this->_helper->layout->setLayout('bootstrap-basic');

    /**
     * Set title
     */
    $this->view->title = "Úprava množství výroby";

    /**
     * Check
     */
    if (!$this->_request->getParam('production')) {
      $this->redirect('parts/overview');
    }

    /**
     * Load form
     */
    $form = new Parts_ProductionsForm();
    $form->setLegend($this->view->title);
    if ($form->meduse_production_ordering) {
      $form->removeElement('meduse_production_ordering');
    }
    if ($form->meduse_production_delivery) {
      $form->removeElement('meduse_production_delivery');
    }

    if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {
      $rProduction = $this->_db->fetchRow("SELECT * FROM productions_parts WHERE id = :id", ['id' => $this->_request->getParam('production')]);
      $old = $rProduction['amount'];
      $new = $form->getElement('meduse_production_amount')->getValue();
      $change = $new - $old;

      $this->_db->update('productions_parts', [
        'amount' => $form->getElement('meduse_production_amount')->getValue(),
        'amount_remain' => new Zend_Db_Expr("(amount_remain + $change)"),
      ], "id = " . $this->_db->quote((int) $this->_request->getParam('production')));
      $this->_flashMessenger->addMessage("Výroba uložena.");
      $this->redirect('parts/productions');
    }
    else {
      if (!$this->_request->isPost()) {
        $rProduction = $this->_db->fetchRow("SELECT * FROM productions_parts WHERE id = :id", ['id' => $this->_request->getParam('production')]);
        if ($rProduction) {
          $form->meduse_production_amount->setValue(intval($rProduction['amount']));
        }
        else {
          if (!$this->_request->getParam('production')) {
            $this->redirect('parts/overview');
          }
        }
      }
    }
    $form->setDescription("Úprava množství výroby " . $rProduction['id_parts']);
    $this->view->form = $form;
    return $this->render('add-subphase');
  }

  public function productionsAction() {
    $this->_helper->layout->setLayout('bootstrap-basic');
    $this->view->title = "Výroba";

    $filter = new Producers_ProductionSearchForm;
    $filter->removeElement('name');
    $filter->populate($this->getRequest()->getParams());

    $this->view->filter = $filter;

    if ($this->_request->isPost()) {
      $this->redirect($this->view->url([
        'producer' => $this->getRequest()
          ->getParam('prdcr', NULL),
      ]));
    }

    $select = $this->_db->select()
      ->from(['p' => 'productions'], [
        'id',
        'date_ordering',
        'date_delivery',
        'id_producers',
      ])
      ->joinLeft(['u' => 'users'], 'p.id_users = u.id', [
        'username' => new Zend_Db_Expr('ifnull(u.name_full, \'n/a\')'),
      ])
      ->joinLeft(['prod' => 'producers'], "p.id_producers = prod.id", [
        'producer' => new Zend_Db_Expr('IFNULL(prod.name, \'bez dodavatele\')'),
      ])
      ->joinLeft(['pp' => 'productions_parts'], 'p.id = pp.id_productions', [])
      ->joinLeft(['ppa' => 'producers_parts'], 'ppa.id_parts = pp.id_parts', [])
      ->joinLeft(['pdp' => 'productions_deliveries_parts'], 'pp.id = pdp.id_productions_parts', [
        'amount_remain' => new Zend_Db_Expr('pp.amount - IFNULL(SUM(pdp.delivery_amount), 0)'),
        'cost' => new Zend_Db_Expr('(pp.amount - IFNULL(SUM(pdp.delivery_amount), 0)) * ppa.price'),
      ])
      ->joinLeft(['po' => 'parts_operations'], 'pp.id_parts_operations = po.id', ['operation_name' => new Zend_Db_Expr('IF(po.name IS NULL, \'objednávka\', CONCAT(\'operace: \', po.name))')])
      ->where('p.id_wh_type = ?', Parts_Part::WH_TYPE_PIPES)
      ->group('pp.id');

    if ($this->getRequest()->getParam('show') != 'all' && $this->getRequest()
        ->getParam('show') != 'y') {
      $select->where("status = 'open'");
    }

    if ($this->getRequest()->getParam('producer')) {
      $select->where("prod.id = ?", $this->getRequest()->getParam('producer'));
    }

    $main_select = $this->_db->select()->from(['s' => $select], [
        'id',
        'username',
        'date_ordering',
        'date_delivery',
        'id_producers',
        'producer',
        'operation_name',
        'amount_remain' => new Zend_Db_Expr('IFNULL(SUM(s.amount_remain), 0)'),
        'costs' => new Zend_Db_Expr('IFNULL(SUM(s.cost), 0)'),
      ])->group('s.id');

    $grid = new ZGrid([
      'title' => _(""),
      'allow_add_link' => FALSE,
      'css_class' => 'table table-hover table-condensed',
      'columns' => [
        'producer' => [
          'header' => _('Dodavatel'),
          'css_style' => 'text-align: left',
          'sorting' => TRUE,
          'renderer' => 'url',
          'url' => $this->view->url([
            'module' => 'default',
            'controller' => 'producers',
            'action' => 'detail',
            'id' => '{id_producers}',
          ], NULL, TRUE),
        ],
        'username' => [
          'header' => 'Zadal',
          'sorting' => TRUE,
        ],
        'operation_name' => [
          'header' => _('Zdroj'),
          'sorting' => TRUE,
        ],
        'date_ordering' => [
          'header' => _('Datum zadání'),
          'sorting' => TRUE,
          'renderer' => 'date',
        ],
        'date_delivery' => [
          'header' => _('Datum dodání'),
          'sorting' => TRUE,
          'renderer' => 'date',
        ],
        'amount_remain' => [
          'header' => _('Chybí [ks]'),
          'header_class' => 'text-right',
          'sorting' => FALSE,
          'renderer' => 'currency',
          'currency_options' => [
            'currency_value' => 'amount_remain',
            'rounding_precision' => 0,
          ],
          'css_style' => 'text-align: right',
          'sum' => TRUE,
          'sum_unit' => 'Kč',
        ],
        'costs' => [
          'header' => _('Hodnota [Kč]'),
          'header_class' => 'text-right',
          'sorting' => FALSE,
          'renderer' => 'currency',
          'currency_options' => [
            'currency_value' => 'costs',
            'rounding_precision' => 2,
          ],
          'css_style' => 'text-align: right',
          'sum' => TRUE,
          'sum_unit' => 'Kč',
        ],
        'zobrazit výrobu' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'url',
          'type' => 'assign',
          'url' => $this->view->url([
            'module' => 'default',
            'controller' => 'producers',
            'action' => 'show-production',
            'id' => '{id}',
          ], NULL, TRUE),
        ],
      ],
    ]);
    $grid->setSelect($main_select);
    $grid->setRequest($this->getRequest()->getParams());
    $this->view->grid = $grid;
  }

  public function operationsAction() {
    $this->_helper->viewRenderer->setNoRender();

        switch($this->_request->getParam('operation')){
            case 'reduce':
                if($this->_request->getParam('wh') == 'base'){
                    $this->_forward("reduce-amount", null, null, array('meduse_reduce_amount' => $this->_request->getParam('amount')));
                } else {
                    $this->_forward("reduce-extern-amount", null, null, array('meduse_reduce_amount' => $this->_request->getParam('amount')));
                }
                break;
            case 'insert':
                if($this->_request->getParam('wh') == 'base'){
                    $this->_forward("warehouse", null, null, array($this->_request->getParam('part').'_amount' => $this->_request->getParam('amount')));
                    return;
                } else {
                    //vkladani na externi sklad

                    try{
                        $parts_warehouse = new Table_PartsWarehouse();
                        $text = $parts_warehouse->addPart($this->_request->getParam('part'), (int) $this->_request->getParam('amount'));
                        $this->_flashMessenger->addMessage('Vloženo: '.$this->_request->getParam('amount')."x ".$this->_request->getParam('part')."<br />".$text);
                        //pridavani aktuality
                        $act = new Table_Actualities();
                        $act->add("Vložil do skladu:<br />$text");
                        $this->_forward("extern-move", null, null, array(
                            'meduse_extern_amount' => $this->_request->getParam('amount'),
                            "meduse_extern_warehouse" => $this->_request->getParam('wh'),
                            'part'=>$this->_request->getParam('part'),
                            'meduse_extern_part'=>$this->_request->getParam('part')
                        ));
                    } catch (Parts_Exceptions_LowAmount $e) {
                        $this->_flashMessenger->addMessage($e->getMessage());
                        $this->_redirect('parts/warehouse');
                    }
                }
                break;
            case 'insert-noSub':
                //vkladani bez odectu podsoucasti

                    if(!$this->_request->getParam('part') || !$this->_request->getParam('amount') || !$this->_request->getParam('wh')){
                        $this->_flashMessenger->addMessage("Operace se nezdařila.");
                        $this->_redirect('parts/warehouse');
                    }

                    $id = $this->_request->getParam('part');
                    $amount = (int) $this->_request->getParam('amount');
                    $tWarehouse = new Table_PartsWarehouse();
                    $tWarehouse->insertPart($id, $amount);
                    $this->_flashMessenger->addMessage("$amount ks $id bylo vloženo bez odečtu podsoučástí na centrální sklad.");

                    $act = new Table_Actualities();
                    $act->add("Vložil do skladu: $amount ks $id bez odečtu podsoučástí.");

                    if($this->_request->getParam('wh') != 'base'){
                        $this->_forward("extern-move", null, null, array(
                            'meduse_extern_amount' => $this->_request->getParam('amount'),
                            "meduse_extern_warehouse" => $this->_request->getParam('wh'),
                            'part'=>$this->_request->getParam('part'),
                            'meduse_extern_part'=>$this->_request->getParam('part')
                        ));
                        return;
                    }

                    $this->_redirect('parts/warehouse');
                break;
            default:
                $this->_flashMessenger->addMessage("Neznama akce");
                $this->_redirect('parts/warehouse');
        }
    }

    /**
     * Vyrazovani soucasti z centralniho skladu
     */
    public function reduceAmountAction()
    {
        // set layout
        $this->_helper->layout->setLayout('bootstrap-basic');

        if(!$this->_request->getParam('part')) $this->_redirect('parts/overview');
        $part = new Parts_Part($this->_request->getParam('part'));

        if($part->getSubparts() !== NULL){
            $this->view->has_subparts = true;
            $this->view->part_id = $part->getID();
        }

        $parts_warehouse = new Table_PartsWarehouse();

        $form = new Parts_ReduceAmountForm();
        $form->setLegend('Vyřazení součásti '.$this->_request->getParam('part').' z centrálního skladu');
        $form->getElement('meduse_reduce_amount')->setDescription('(skladem '.$parts_warehouse->getAmount($part->getID()).' ks)');

        if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
            try{
                $parts_warehouse->reduceCentralAmount($part->getID(), $form->meduse_reduce_amount->getValue());
                //pridavani aktuality
                $act = new Table_Actualities();
                $act->add("Vyřadil z centrálního skladu ".$form->meduse_reduce_amount->getValue()." ks ".$part->getID());
                $this->_flashMessenger->addMessage("Vyřazeno ".$form->meduse_reduce_amount->getValue()." ks ".$part->getID()." z centrálního skladu.");
                //$this->_redirect('parts/detail/id/'.$this->_request->getParam('part'));
                $this->_redirect('parts/warehouse');
            } catch (Parts_Exceptions_LowAmount $e) {
                $this->_flashMessenger->addMessage("V centrálním skladu je méně kusů součásti, než má být vyřazeno. Zadejte menší číslo.");
                $this->_redirect('parts/reduce-amount/part/'.$this->_request->getParam('part'));
            }
        }

        $this->view->form = $form;

    }

    public function reduceExternAmountAction(){
        $this->_helper->layout->setLayout('bootstrap-basic');
        if(!$this->_request->getParam('part')) $this->_redirect('parts/overview');
        if(!$this->_request->getParam('wh')) $this->_redirect('parts/overview');

        $part = new Parts_Part($this->_request->getParam('part'));
        $this->view->title = $part->getID() . ' ' . $part->getName();

        if($part->getSubparts() !== NULL){
            $this->view->has_subparts = true;
            $this->view->part_id = $part->getID();
        }

        $parts_warehouse = new Table_PartsWarehouse();
        $extern_wh = new Table_PartsWarehouseExtern();

        $form = new Parts_ReduceExternForm('parts_box');
        $form->setLegend('Vyřazení součásti '.$this->_request->getParam('part').' z externího skladu');
        $form->getElement('meduse_reduce_amount')->setValue($extern_wh->getAmount((int) $this->_request->getParam('wh'), $part->getID()));
        if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
            try{
                $parts_warehouse->reduceExternAmount($part->getID(), (int) $form->meduse_reduce_amount->getValue(), (int) $this->_request->getParam('wh'));
                //pridavani aktuality
                $act = new Table_Actualities();
                $act->add("Vyřadil z externiho skladu ".$form->meduse_reduce_amount->getValue()." ks ".$part->getID());
                $this->_flashMessenger->addMessage("Vyřazeno ".$form->meduse_reduce_amount->getValue()." ks ".$part->getID()." z externího skladu.");
                $this->_redirect('parts/warehouse');
            } catch (Parts_Exceptions_LowAmount $e) {
                $this->_flashMessenger->addMessage("V externím skladu je méně kusů součásti, než má být vyřazeno. Zadejte menší číslo.");
                $this->_redirect('parts/reduce-extern-amount/part/'.$this->_request->getParam('part').'/wh/'.$this->_request->getParam('wh'));
            }
        }

        $this->view->form = $form;

        return $this->render('add-production');
    }

    public function breakAction(){
        // set layout
        $this->_helper->layout->setLayout('bootstrap-basic');

        if(!$this->_request->getParam('part')) $this->_redirect('parts/overview');

        $part = new Parts_Part($this->_request->getParam('part'));
        $parts_warehouse = new Table_PartsWarehouse();

        $form = new Parts_BreakForm();
        $form->setLegend('Rozložení součásti '.$this->_request->getParam('part').' zpátky na sklad');
        $form->getElement('meduse_break_amount')->setValue($parts_warehouse->getAmount($part->getID()));

        if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
            $db = $parts_warehouse->getAdapter();
            try{
                $db->beginTransaction();
                $amount = (int) $form->meduse_break_amount->getValue();
                $parts_warehouse->removePart($part->getID(), $amount);

                $list = '<br />Vloženo:';
                foreach($part->getSubparts() as $subpart){
                    $parts_warehouse->insertPart($subpart->getID(), $amount*$subpart->getAmount());
                    $list .= '<br />'.$subpart->getID().' '.($amount*$subpart->getAmount()).'x';
                }

                //pridavani aktuality
                $act = new Table_Actualities();
                $act->add("Rozložil ".$form->meduse_break_amount->getValue()." ks součásti ".$part->getID());
                $this->_flashMessenger->addMessage("Rozloženo ".$form->meduse_break_amount->getValue()." ks ".$part->getID().$list);
                $db->commit();
                $this->_redirect('parts/detail/id/'.$this->_request->getParam('part'));
            } catch (Parts_Exceptions_LowAmount $e) {
                $db->rollBack();
                $this->_flashMessenger->addMessage("Bylo zadáno příliš vysoké číslo.");
            }
        }

        $this->view->form = $form;

        return $this->render('add-production');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    //  externi sklady

    public function externAddAction(){
        // set layout
        $this->_helper->layout->setLayout('bootstrap-basic');

        /**
         * Load form
         */
        $form = new Parts_ExternForm();
        $form->setLegend('Přidat externí sklad');

        if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
            $this->db->insert('extern_warehouses', array(
                'name' => $this->_request->getParam('meduse_extern_name'),
                'description' => (strlen($this->_request->getParam('meduse_extern_desc')) > 0 ? $this->_request->getParam('meduse_extern_desc') : new Zend_Db_Expr('NULL'))
            ));
            $this->_flashMessenger->addMessage("Vložen nový externí sklad '".$this->_request->getParam('meduse_extern_name')."'");
            $this->_redirect('parts/extern-overview');
        }

        $this->view->form = $form;

        return $this->render('add-subphase');
    }

    public function externEditAction(){
        // set layout
        $this->_helper->layout->setLayout('bootstrap-basic');

        /**
         * Get params sent
         */
        if(!$this->_request->getParam('id')) $this->_redirect('parts/extern-overview');

        // check record
        if(!($row = $this->db->fetchRow("SELECT * FROM extern_warehouses WHERE id = ?", $this->_request->getParam('id')))){
            throw new Exception("Externi '".$this->_request->getParam('id')."' sklad nenalezen.");
        }

        /**
         * Load form
         */
        $form = new Parts_ExternForm();
        $form->setLegend('Upravit externí sklad');

        if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
            /**
             * Update record
             */
            $this->db->update('extern_warehouses', array(
                'name' => $this->_request->getParam('meduse_extern_name'),
                'description' => (strlen($this->_request->getParam('meduse_extern_desc')) > 0 ? $this->_request->getParam('meduse_extern_desc') : new Zend_Db_Expr('NULL'))
            ), "id = ".$this->_request->getParam('id'));

            // message and redirect
            $this->_flashMessenger->addMessage("Sklad '".$this->_request->getParam('meduse_extern_name')."' upraven.");
            $this->_redirect('parts/extern-overview');
        }
        else {
            // populate
            $form->meduse_extern_name->setValue($row['name']);
            $form->meduse_extern_desc->setValue($row['description']);
        }

        /**
         * To view
         */
        $this->view->form = $form;

        return $this->render('add-subphase');
    }

    public function externOverviewAction(){
        // set layout
        $this->_helper->layout->setLayout('bootstrap-basic');

        // get extern warehouses
        $tExternWarehouses = new Table_ExternWarehouses();
        $data = $tExternWarehouses->fetchAll();
        $data = $data->toArray();

        /**
         * To view
         */
        $this->view->data = $data;
    }

    /**
     * Presouvani soucasti z hlavniho skladu do externiho skladu
     */
    public function externMoveAction()
    {
        // set layout
        $this->_helper->layout->setLayout('bootstrap-basic');

        if(!$this->_request->getParam('part')) $this->_redirect('parts/extern-overview');
        $part = new Parts_Part($this->_request->getParam('part')); //validace

        $form = new Parts_ExternMoveForm();
        $form->setLegend('Přesunout na externí sklad');

        if($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())){
            $pwe = new Table_PartsWarehouseExtern();
            $pwe->insertPart(
                $form->meduse_extern_warehouse->getValue(), //sklad
                $part->getID(), //soucast
                $form->meduse_extern_amount->getValue() //pocet
            );
            $db = Zend_Registry::get('db');
            $name = $db->fetchOne("SELECT name FROM extern_warehouses WHERE id = ".$db->quote($form->meduse_extern_warehouse->getValue()));

            $this->_flashMessenger->addMessage($form->meduse_extern_amount->getValue()."ks ".$part->getID()." bylo přesunuto do externího skladu '".$name."'.");
            //$this->_redirect('parts/detail/id/'.$part->getID());
            $this->_redirect('parts/warehouse');
        }

        $form->meduse_extern_part->setValue($part->getID());
        $this->view->form = $form;

        return $this->render('add-subphase');
    }

    /**
     * Presouvani soucasti z hlavniho skladu do externiho skladu
     * @return unknown_type
     */
    public function externMoveBackAction(){
        $this->_helper->layout->setLayout('bootstrap-basic');
        if (!$this->_request->getParam('part')) {
          $this->_redirect('parts/extern-overview');
        }
        $part = new Parts_Part($this->_request->getParam('part')); //validace
        $this->view->title = $part->getID() . ' ' . $part->getName();
        $form = new Parts_ExternMoveForm();
        $form->setMoveback();
        $form->setLegend('Přesunout zpět z externího skladu');
        $pwe = new Table_PartsWarehouseExtern();

        if ($this->_request->getParam('Odeslat')
          && $form->isValid($this->_request->getParams())){

            $pwe->removePart(
                $form->meduse_extern_warehouse->getValue(), //sklad
                $part->getID(), //soucast
                $form->meduse_extern_amount->getValue() //pocet
            );
            $this->_flashMessenger->addMessage($form->meduse_extern_amount->getValue()."ks ".$part->getID()." bylo přesunuto zpět z externího skladu.");
            $this->_redirect('parts/detail/id/'.$part->getID());
        }

        $form->meduse_extern_part->setValue($part->getID());
        $form->meduse_extern_warehouse->setValue((int) $this->_request->getParam('extern'));
        $form->meduse_extern_amount->setValue($pwe->getAmount((int) $this->_request->getParam('extern'), $part->getID()));
        $this->view->form = $form;

        return $this->render('add-production');
    }

    //  externi sklady konec
    ///////////////////////////////////////////////////////////////////////////////////////////

    public function selectProducerAction()
    {
        //set layout
        $this->_helper->layout->setLayout('bootstrap-basic');

        if(!$this->_request->getParam('part')) $this->_redirect('parts/warehouse');

        $part = new Parts_Part($this->_request->getParam('part'));
        $this->view->part = $part->getID();

        $table = new Table_Producers();
        $this->view->data = $table->getDataList();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // operace

    public function addOperationAction(){
      //Set layout
      $this->_helper->layout->setLayout('bootstrap-basic');

      //Parameter missing --> redirect
      if(!$this->_request->getParam('part')) $this->_redirect('parts/warehouse');

      //Load Operation form
      $form = new Parts_OperationForm();
      $form->setLegend("Nová operace");

      // Load table Parts_Part
      $part = new Parts_Part($this->_request->getParam('part'));
      $oldNettoPrice = $part->getPrice() - $part->getOperationSum();

      //ACL
      if (Zend_Registry::get('acl')->isAllowed('parts/detail/price') !== true) {
        $form->removeElement('meduse_operation_price');
      }

      //Send form
      if ($this->_request->isPost()
        && $form->isValid($this->_request->getParams())) {

        // set values
        $price = str_replace(',', '.', $form->getValue('meduse_operation_price'));
        $name = $form->getValue('meduse_operation_name');
        $producer_id = $form->getValue('meduse_operation_producer');
        $time = $form->getValue('meduse_operation_time');

        if ($producer_id == Table_Producers::MEDUSE_ID || $producer_id == Table_Producers::MEDUSE_ELEKTRO_ID) {
          $tRate = new Table_ApplicationOperationsRate();
          $rate = $tRate->getRate();
          $price = $time * $rate / 60;
        } else {
          $time = 0;
        }

        if (!Zend_Registry::get('acl')->isAllowed('parts/detail/price')) {
          $price = 0;
          $message = 0;
        } else {
          $message = ", cena všech položek skladu, které obsahují tuto součást byla přepočítána";
        }

        // Save to db
        $tPartsOperations = new Table_PartsOperations();
        $rPartsOperations = $tPartsOperations->createRow(array(
          "id_parts" => $part->getID(),
          "name" => $name,
          "price" => $price,
          "time" => empty($time)? 0 : $time,
          "id_producers" => empty($producer_id)? new Zend_Db_Expr('NULL') : $producer_id,
          "inserted" => new Zend_Db_Expr('NOW()')
        ));
        $rPartsOperations->save();

        $part->setPrice($oldNettoPrice);
        $part->refreshOperations();
        $part->refreshPrice();

        // System message
        $this->_flashMessenger->addMessage("Operace uložena " . $message . ".");

        // Redirect
        $this->_redirect('parts/detail/id/' . $part->getID());
      }

      // To view
      $this->view->title = "Nová operace";
      $this->view->form = $form;

      //Render
      $this->render('add-subphase');
    }

    public function editOperationAction(){

      $this->_helper->layout->setLayout('bootstrap-basic');

      $id = $this->_request->getParam('id');
      $part = $this->_request->getParam('part');
      if(!$part || !$id) $this->_redirect('parts/warehouse');

      $form = new Parts_OperationForm();
      $form->setLegend("Změna operace");

      $part = new Parts_Part($part);
      $oldNettoPrice = $part->getPrice() - $part->getOperationSum();

      $tPartsOperations = new Table_PartsOperations();
      $row = $tPartsOperations->find($id)->current();

      if ($this->_request->isPost()
        && $form->isValid($this->_request->getParams())) {

        // set values
        $row->name = $form->getValue('meduse_operation_name');
        $time = $form->getValue('meduse_operation_time');
        $price = (float) str_replace(',', '.', $form->getValue('meduse_operation_price'));
        $producer_id = $form->getValue('meduse_operation_producer');

        if ($producer_id == Table_Producers::MEDUSE_ID || $producer_id == Table_Producers::MEDUSE_ELEKTRO_ID) {
          $tRate = new Table_ApplicationOperationsRate();
          $rate = $tRate->getRate();
          $price = $time * $rate / 60;
        } else {
          $time = 0;
        }

        // price
        if(Zend_Registry::get('acl')->isAllowed('parts/detail/price')) {
            $row->price = $price;
            $message = ", cena všech položek skladu, které obsahují tuto součást byla přepočítána";
        }

        // producer id
        if($producer_id == null){
            $row->id_producers = new Zend_Db_Expr("NULL");
        } else {
            $row->id_producers = $producer_id;
        }

        // time
        $row->time = $time;

        $row->save();

        $part->setPrice($oldNettoPrice);
        $part->refreshOperations();
        $part->refreshPrice();

        $this->_flashMessenger->addMessage("Operace uložena" . (isset($message) ? $message : '') . ".");
        $producerId = $this->_request->getParam('producer', null);
        if (is_null($producerId)) {
          $this->_redirect('parts/detail/id/' . $part->getID());
        } else {
          $this->_redirect('producers/detail/id/' . $producerId);
        }
      } else {
        $form->meduse_operation_name->setValue($row->name);
        $form->meduse_operation_price->setValue($row->price);
        $form->meduse_operation_time->setValue($row->time);
        $form->meduse_operation_producer->setValue($row->id_producers);
      }

      if(Zend_Registry::get('acl')->isAllowed('parts/detail/price') !== true){
        $form->removeElement('meduse_operation_price');
      }

      $this->view->title = "Změna operace";
      $this->view->form = $form;

      $this->render('add-subphase');
    }

    public function deleteOperationAction(){
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
      if(!$this->_request->getParam('part')) $this->_redirect('parts/warehouse');
      if(!$this->_request->getParam('id')) $this->_redirect('parts/warehouse');
      $part = new Parts_Part($this->_request->getParam('part'));
      $oldNettoPrice = $part->getPrice() - $part->getOperationSum();

      $tPartsOperations = new Table_PartsOperations();
      $tPartsOperations->delete("id = ".$tPartsOperations->getAdapter()->quote(
        $this->_request->getParam('id')));

      $part->setPrice($oldNettoPrice);
      $part->refreshOperations();
      $part->refreshPrice();

      $this->_flashMessenger->addMessage("Operace odstraněna, cena všech položek skladu, které obsahují tuto součást byla přepočítána.");
      $producerId = $this->_request->getParam('producer', null);
      if (is_null($producerId)) {
        $this->_redirect('parts/detail/id/' . $part->getID());
      } else {
        $this->_redirect('producers/detail/id/' . $producerId);
      }
    }

    // operace - konec
    ///////////////////////////////////////////////////////////////////////////////////////////

  /**
   * @throws Parts_Exceptions_BadWH
   * @throws Zend_Db_Table_Exception
   * @throws Zend_Form_Exception
   * @throws Zend_Db_Adapter_Exception
   * @throws Parts_Exceptions_IdNotExists
   */
  public function saveAsAction(){

    if (!$this->_request->getParam('id')) {
      $this->redirect('parts/warehouse');
    }

    $part = new Parts_Part($this->_request->getParam('id'));
    $this->view->title = 'Uložit ' . $part->getID() . ' jako...';
    $form = new Parts_SaveAsForm();
    $form->setLegend($this->view->title);
    $this->view->form = $form;

    if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {
      try {
        $part->saveAs(
          $part->getID(),
          $form->getValue('meduse_parts_id'),
          $form->getValue('meduse_parts_name'),
          $form->getValue('meduse_parts_desc')
        );
      }
      catch (Parts_Exceptions_IdAlreadyExists $e) {
        $this->_flashMessenger->addMessage('ID \'' . $form->getValue('meduse_parts_id') . '\' nelze použít, již existuje.');
        $this->redirect('parts/save-as/id/' . $form->getValue('meduse_parts_id'));
      }

      $this->_flashMessenger->addMessage('Součást ' . $this->_request->getParam('id') . ' byla uložena jako  ' . $form->getValue('meduse_parts_id') . '.');
      $this->redirect('parts/detail/id/' . $form->getValue('meduse_parts_id'));
    }
    else {
      $form->getElement('meduse_parts_name')->setValue($part->getName());
    }
    return $this->render('add-subphase');
  }

    public function historyAction(){
        $tWarehouse = new Table_PartsWarehouse();
        $this->view->title = 'Historie skladu';
        if($this->_request->getParam('id')){
            $this->view->id = true;
            $this->view->data = $tWarehouse->getHistory($this->_request->getParam('id'));
        } else {
            $this->view->data = $tWarehouse->getHistory();
        }
        $this->_helper->layout->setLayout('bootstrap-basic');
    }

    public function historyBackAction(){
        if($this->_request->isPost()){
            $this->_helper->layout->setLayout('bootstrap-basic');
            $histIds = array();
            foreach($this->_request->getParams() as $name => $value) {
                if(preg_match("/^back_*/", $name)){
                    $histId = substr($name, 5);
                    $histIds[] = (int)$histId;
                }
            }
            $where = implode(',', $histIds);
            $this->view->data = $this->_db->fetchAll("SELECT pwh.*, p.name FROM parts_warehouse_history pwh JOIN parts p ON pwh.id_parts = p.id WHERE pwh.id IN ($where)");

            if($this->_request->getParam('confirm')){
                $tWarehouse = new Table_PartsWarehouse();
                foreach($this->view->data as $row) {
                    if($row['amount'] < 0){
                        $tWarehouse->insertPart($row['id_parts'], abs($row['amount']));
                    } else {
                        if($row['amount'] > $tWarehouse->getBaseAmount($row['id_parts']) && $row['amount'] <= $tWarehouse->getAmount($row['id_parts'])){
                            $tWarehouse->removePart($row['id_parts'], $row['amount']);
                        } else if ($row['amount'] > $tWarehouse->getBaseAmount($row['id_parts'])) {
                            $tWarehouse->reduceCentralAmount($row['id_parts'], $row['amount']);
                        } else {
                            //throw new Exception("Neni mozne vyradit ".$row['amount']." ks ".$row['id_parts'].".");
                            $this->_flashMessenger->addMessage("Není možné vyřadit ".$row['amount']." ks ".$row['id_parts'].".");
                            $this->_redirect($this->view->url(array('action' => 'history', 'controller' => 'parts'),null,true));
                            return;
                        }
                    }
                    $this->_db->update("parts_warehouse_history",array('note' => 'Vráceno'),"id_parts = ".$this->_db->quote($row['id_parts'])."AND id > ".$row['id']);
                }
                $this->_flashMessenger->addMessage("Vráceno.");
                $this->_redirect($this->view->url(array('action' => 'history', 'controller' => 'parts'),null,true));
            }
        } else {
            $this->_redirect($this->view->url(array('action' => 'history', 'controller' => 'parts'),null,true));
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    // kolekce - manipulace

  public function collectionsAction() {
    /**
     * Set title
     */
    $this->view->title = "Kolekce";

    /**
     * Collections data
     */
    $tCollections = new Table_Collections();
    $select = $tCollections->getCollectionsSelect();

    $grid = new ZGrid([
      'add_link_url' => $this->view->url([
        'module' => 'default',
        'controller' => 'parts',
        'action' => 'add-collection',
      ], NULL, TRUE),
      'allow_add_link' => TRUE,
      'add_link_title' => _('Přidat kolekci'),
      'css_class' => 'table table-hover table-condensed',
      'columns' => [
        'name' => [
          'header' => _('Název'),
          'css_style' => '',
          'sorting' => TRUE,
        ],
        'code' => [
          'header' => _('Kód'),
          'sorting' => TRUE,
        ],
        'is_pipes' => [
          'header' => _('Pipes'),
          'css_style' => '',
          'sorting' => FALSE,
        ],
        'abbr' => [
          'header' => _('Zkrácený název'),
          'css_style' => '',
          'sorting' => FALSE,
        ],
        'delete' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'action',
          'type' => 'delete',
          'url' => $this->view->url([
            'module' => 'default',
            'controller' => 'parts',
            'action' => 'delete-collection',
            'id' => '{id}',
          ], NULL, TRUE),
        ],
        'edit' => [
          'header' => '',
          'css_style' => 'text-align: center',
          'renderer' => 'action',
          'type' => 'edit',
          'url' => $this->view->url([
            'module' => 'default',
            'controller' => 'parts',
            'action' => 'edit-collection',
            'id' => '{id}',
          ], NULL, TRUE),
        ],
      ],
    ]);
    $grid->setSelect($select);
    $grid->setRequest($this->getRequest()->getParams());
    $this->view->grid = $grid;


    /**
     * To view
     */
    $this->view->data = $tCollections->fetchAll();
  }

  public function addCollectionAction() {
    if ($this->_request->getParam('edit')) {
      $this->view->title = "Upravit kolekci";
    }
    else {
      $this->view->title = "Nová kolekce";
    }

    $form = new Parts_AddCollectionsForm();
    $form->setLegend('Nová kolekce');

    if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {
      $this->_db->beginTransaction();
      try {
        $tCollection = new Table_Collections();
        if ($this->_request->getParam('edit')) {
          $tCollection->update([
            'name' => $this->_request->getParam('collection_name'),
            'code' => $this->_request->getParam('collection_code'),
            'abbr' => $this->_request->getParam('collection_abbr'),
            'is_pipes' => $this->_request->getParam('collection_is_pipes'),
          ], 'id = ' . $tCollection->getAdapter()
              ->quote((int) $this->_request->getParam('id')));
        }
        else {
          $tCollection->insert([
            'name' => $this->_request->getParam('collection_name'),
            'code' => $this->_request->getParam('collection_code'),
            'abbr' => $this->_request->getParam('collection_abbr'),
            'is_pipes' => $this->_request->getParam('collection_is_pipes'),
          ]);
        }
      }
      catch (Zend_Exception $e) {
        $this->_db->rollBack();
        throw new Exception($e->getMessage());
      }
      $this->_db->commit();
      $this->_flashMessenger->addMessage("Kolekce uložena");
      $this->redirect('parts/collections');
    }
    else {
      if ($this->_request->getParam('edit')) {
        $tCollections = new Table_Collections();
        $rColl = $tCollections->find($this->_request->getParam('id'))
          ->current();
        if ($rColl !== NULL) {
          $form->collection_name->setValue($rColl->name);
          $form->collection_code->setValue($rColl->code);
          $form->collection_abbr->setValue($rColl->abbr);
          $form->collection_is_pipes->setValue($rColl->is_pipes);
        }
      }
    }
    $this->view->form = $form;
    $this->render('add-subphase');
  }

  public function editCollectionAction() {
    $this->forward("add-collection", NULL, NULL, ['edit' => TRUE]);
  }

  public function deleteCollectionAction() {
    if (!$this->_request->getParam('id')) {
      $this->redirect('parts/collections');
    }
    $tCollections = new Table_Collections();

    $select = $tCollections->select()->setIntegrityCheck(FALSE);
    $rows = $tCollections->fetchAll($select->from("parts_collections")
      ->where("id_collections = ?", $this->_request->getParam('id')));
    if (count($rows) > 0) {
      $this->_flashMessenger->addMessage("Nelze odstranit, minimálně jedna součást je již přiřazena k této kolekci.");
    }
    else {
      $tCollections->delete("id = " . $tCollections->getAdapter()
          ->quote($this->_request->getParam('id')));
      $this->_flashMessenger->addMessage("Kolekce odstraněna");
    }
    $this->redirect('parts/collections');
  }

    // kolekce - manipulace - konec
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // kategorie - manipulace

  public function categoriesAction() {
    $this->_helper->layout->setLayout('bootstrap-basic');
    $this->view->title = "Kategorie součástí";
    $tCtg = new Table_PartsCtg();
    $this->view->data = $tCtg->getCategoriesWithPartsSum(1);
    if ($this->getRequest()->isPost()) {
      foreach ($this->getRequest()->getParams() as $name => $val) {
        if (preg_match("/^order_/", $name)) { //musi zacinat "order_"
          $id = (int)substr($name, 6);
          $tCtg->update(['ctg_order' => (int) $val], 'id = ' . $id);
        }
      }
      $this->redirect('/parts/categories');
    }
  }

  public function addCategoryAction() {

    $this->_helper->layout->setLayout('bootstrap-basic');

    if ($this->_request->getParam('edit')) {
      $this->view->title = "Upravit kategorii";
    } elseif ($this->_request->getParam('group')) {
      $this->view->title = "Nová skupina";
    } elseif ($this->_request->getParam('group-edit')) {
      $this->view->title = "Upravit skupinu";
    } else {
      $this->view->title = "Nová kategorie";
    }

    $form = new Parts_CategoryForm();
    $form->setLegend($this->view->title);

    if ($this->_request->getParam('group') || $this->_request->getParam('group-edit')) {
      $form->removeElement('category_parent');
    }

    if (!$this->_request->getParam('group')) {
      $form->removeElement('group');
    }

    // Send form.
    $tCtg = new Table_PartsCtg();
    if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {

      if ($this->_request->getParam('edit')) {
        $rCtg = $tCtg->find($this->_request->getParam('edit'))->current();
        $rCtg->name = $form->category_name->getValue();
        if ($rCtg->parent_ctg !== '0') {
          $prt = $form->category_parent->getValue();
          $rCtg->parent_ctg = empty($prt) ? new Zend_Db_Expr('NULL') : $prt;
        }
        $rCtg->save();
      } else {
        $row = $tCtg->createRow();
        $row->id_wh_type = 1;
        $row->name = $form->category_name->getValue();
        $row->ctg_order = 1;
        if ($this->_request->getParam('group')) {
          $row->parent_ctg = new Zend_Db_Expr('NULL');
        } else {
          $prt = $form->category_parent->getValue();
          $row->parent_ctg = empty($prt) ? new Zend_Db_Expr('NULL') : $prt;
        }
        $row->save();
      }
      $this->_flashMessenger->addMessage("Uloženo.");
      $this->redirect('parts/categories');
    } else if ($this->_request->getParam('edit')) {
      $rCtg = $tCtg->find($this->_request->getParam('edit'))->current();
      if ($rCtg !== null) {
        $form->category_name->setValue($rCtg->name);
        if ($form->category_parent) {
          $form->category_parent->setValue($rCtg->parent_ctg);
        }
      }
    }

    $this->view->form = $form;

    $this->render('add-subphase');
  }

  public function categoryDeleteAction() {
    if ($this->_request->getParam('id')) {
      $tCtg = new Table_PartsCtg();
      $tCtg->delete("id = " . $this->_db->quote($this->_request->getParam('id')));
      $this->_flashMessenger->addMessage("Odstraněno.");
    }
    $this->redirect('parts/categories');
  }

    // kategorie - manipulace - konec
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////


        public function partsAvailableAction()
        {
            $this->_helper->layout->setLayout('layout2');

            /**
             * Load form
             */
            $form = new Parts_PartsAvailableForm();


            /**
             * Send form
             */
            if($this->_request->isPost()
                    && $form->isValid($this->_request->getParams())) {

                        $part_id = $form->getValue('part_id');

                        $tPartsAvailable = new Parts_PartsAvailable($part_id);
                        $partsAvailable = $tPartsAvailable->checkProductAmount();
                        $this->view->result = $partsAvailable;

                        # var_dump($partsAvailable);
                        # var_dump($tPartsAvailable->getPartsSummary());
            }


            /**
             * To view
             */
            $this->view->title = 'Dostupnost produktu';
            $this->view->form = $form;
        }

        public function ajaxGetRelatedOrdersAction() {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $part_id = $this->_request->getParam('id');
			$order_ids = explode(' ', $this->_request->getParam('orders'));
            if ($part_id && !empty($order_ids)) {
                $db = Zend_Registry::get('db');
                $qid = $db->quote($part_id);
                $select = "SELECT id, `no`, title, deadline, sum(amount) AS amount FROM ((
                                SELECT o.id, o.`no`, o.title, o.date_deadline AS deadline, op.amount
                                FROM orders_products AS op
                                RIGHT JOIN orders AS o ON op.id_orders = o.id
                                RIGHT JOIN parts AS p ON op.id_products = p.id
                                WHERE o.id IN (" . implode(', ', $order_ids) . ")
                                AND p.id = $qid
                            ) UNION (
                                SELECT o.id, o.`no`, o.title, o.date_deadline AS deadline, SUM(op.amount * sp.amount) AS amount
                                FROM orders_products AS op
                                RIGHT JOIN orders AS o ON op.id_orders = o.id
                                RIGHT JOIN parts AS p ON op.id_products = p.id
                                RIGHT JOIN parts_subparts AS sp ON p.id = sp.id_multipart
                                WHERE o.id IN (" . implode(', ', $order_ids) . ")
                                AND p.id IN (SELECT id_multipart
                                FROM parts_subparts
                                WHERE id_parts = $qid)
                                AND sp.id_parts = $qid
                                GROUP BY o.id
                            )) AS tbl GROUP BY id ORDER BY deadline;";
                $orders = $db->fetchAll($select);
                if ($orders) {
                    $this->_helper->json(array(
                        'result' => 'ok',
                        'message' => 'Byly nalezeny tyto objednávky:',
                        'data' => $orders
                    ));
                } else {
                    $this->_helper->json(array(
                        'result' => 'ko',
                        'message' => 'Nebyly nalezeny žádné objednávky.'
                    ));
                }
            } else {
                $this->_helper->json(array(
                    'result' => 'ko',
                    'message' => 'Nebylo zadáno ID součásti.'
                ));

            }

        }

        public function getBarcodeAction() {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $part_id = $this->_request->getParam('id');
            if ($part_id) {
                $tEans = new Table_Eans();
                $tEans->getBarcode($part_id);
            }
        }


        public function ajaxGetNextPartIdAction()
        {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            // get params
            $collection_id = $this->_request->getParam('col');
            $category_id = $this->_request->getParam('ctg');

            // set collection letter
            switch($collection_id)
            {
                case '1':
                    $letter = 'C';
                    break;
                case '2':
                    $letter = 'S';
                    break;
                case '3':
                    $letter = 'N';
                    break;
                default :
                    $letter = null;
                    break;
            }

            // throw error
            if(empty($letter)) {
                throw new Exception('error:: Latter is empty');
            }

            // is accessory join A
            $prefix = (in_array($category_id, array(31, 1,2,3,4,5,6,7,8))) ? 'A' . $letter : $letter;

            // get latest part id
            $tParts = new Table_Parts();
            $data = $tParts->getLatestPartID($prefix);

            // make new available part id
            $id_parts = preg_split("/(\d+)/", $data[0]['id'], -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            $increased_number = str_pad(($id_parts[1] + 1), 2, '0', STR_PAD_LEFT);
            $data = $id_parts[0] . $increased_number;

            $this->_helper->json(array('data' => $data));
        }

        public function ajaxCheckPartIdAction()
        {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $part_id = $this->_request->getParam('id');

            $result = false;
            if(!empty($part_id)) {
                $tParts = new Table_Parts();
                $result = $tParts->checkIdExist($part_id);
            }

            $this->_helper->json(array('result' => $result));
        }

		/**
		 * akce pro kontrolu zpozdene vyroby
		 * TODO: cron
		 */
		public function checkDelayedAction() {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
			$tProductions = new Table_Productions();
			$delayed = $tProductions->getDelayedProduction();
			foreach ($delayed as $production) {
				if (!$production['date_notify']) {
					$tProductions->notifyDelayedProduction($production['id']);
				}
			}
		}

    /**
     * Kontrola vstupních cen - nákladů podsoučástí aktivních produktů.
     */
    public function checkCostsAction() {
      $this->_helper->layout->setLayout('bootstrap-basic');
      $this->view->title = 'Kontrola nákladů součástí';
      $tParts = new Table_Parts();
      $this->view->parts = $tParts->checkCosts(1);
      $this->view->message = '';

      // Pokud existují součásti s nulovými náklady a pokud akci provedl někdo
      // jiný než vedoucí výroby, odešle se vedoucímu výroby email.
      $authNamespace = new Zend_Session_Namespace('Zend_Auth');
      if ($this->view->parts && !in_array('head_warehouseman', $authNamespace->user_role) && !in_array('admin', $authNamespace->user_role)) {
        $body = $this->view->render('parts/check-costs-email.phtml');
        $tEmails = new Table_Emails();
        $tEmails->sendEmail(Table_Emails::EMAIL_NOTICE_ZERO_COSTS, $body);
        $this->view->message = 'Vedoucímu výroby bylo odesláno upozornění emailem.';
      }
    }

    public function productsOverviewAction() {
      $this->view->title = 'Přehled produktů';
      $this->view->products = Parts::getProducts();
    }

    public function batchThrowingAction() {
      $this->view->title = 'Hromadné vyřazení součástí';
      $this->view->phase = $this->_request->getParam('phase', 'insert');

      if ($this->_request->isPost()) {

        if ($this->view->phase === 'confirm') {
          $parts_ids = $this->_request->getParam('parts_ids');
          $parts = $this->view->parts = $this->batchThrowingParseIds($parts_ids);
          $this->view->form = new Parts_BatchThrowingConfirmForm(['parts' => $parts]);
        }

        elseif ($this->view->phase === 'perform') {
          $parts_ids = [];
          $pwe = new Table_PartsWarehouseExtern();
          $params = $this->_request->getParams();
          foreach ($params as $key => $value) {
            [$prefix, $partId] = explode('_', $key);
            if ($prefix === 'amount') {
              $pwe->insertPart(
                $pwe::THROWN_PARTS_WAREHOUSE_ID,
                $partId,
                $value
              );
              $parts_ids[] = $partId;
            }
          }
          $this->_flashMessenger->addMessage('Do skladu vyřazených součástí ' .
            'bylo přesunuty tyto součásti (' . count($parts_ids) . '): ' .
            implode(', ', $parts_ids));
          $this->redirect($this->view->url(['action' => 'warehouse']));
        }
      }

      else {
        $this->view->form = new Parts_BatchThrowingForm();
      }

    }

    private function batchThrowingParseIds($parts_ids) {
      $matches = [];
      $parts = [];
      $found = preg_match_all('/[^\s,]+/', $parts_ids, $matches);
      if ($found && isset($matches[0])) {

        $table = new Table_PartsWarehouseExtern();
        $rows = $table->getPartsForThrow($matches[0]);

        foreach ($matches[0] as $part_id) {
          if (isset($rows[$part_id])) {
            $parts[$part_id] = $rows[$part_id];
          }
          else {
            $parts[$part_id] = [
              'id_parts' => $part_id,
              'name' => '! součást nenalezena',
              'amount' => NULL,
              'throw_amount' => 0
            ];
          }
        }
      }
      return $parts;
    }

    public function toggleDesignatedForAction() {
      $this->_helper->viewRenderer->setNoRender();
      if (!$partId = $this->_request->getParam('id')) {
        $msg = 'Nebylo zadáno ID produktu';
        $this->_flashMessenger->addMessage($msg);
        Utils::logger()->warn($msg);
        $this->redirect($this->view->url(['action' => 'parts-overview']));
      }
      try {
        $part = new Parts_Part($partId);
        $designatedFor = $part->isDesignatedForB2b() ? Table_Customers::TYPE_B2C : Table_Customers::TYPE_B2B;
        $part->setDesignatedFor($designatedFor);
        $message = "Bylo nastaveno užití produktu $partId pro $designatedFor zákazníky";
        Utils::addActuality($message);
        $this->_flashMessenger->addMessage($message);
      }
      catch (Exception $e) {
        $this->_flashMessenger->addMessage($e->getMessage());
      }
      $this->redirect($this->view->url(['action' => 'detail']));
    }

}
