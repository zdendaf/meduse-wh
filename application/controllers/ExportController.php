<?php
class ExportController extends Meduse_Controller_Action{

	const DOWNLOAD_PATH = '/export/';

	public function indexAction(){
		//odkazy na exporty
    $this->_helper->layout->setLayout('bootstrap-basic');
		$this->view->title = "Export";

		if ($this->_request->getParam('download')) {
			$this->view->download = Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH . '/' . $this->_request->getParam('download');
		}

    $dir = dir(Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH);
    $entries = array();
    while (FALSE !== ($entry = $dir->read())) {
      if ($entry != '.' && $entry != '..') {
        $filepath = Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH . $entry;
        $time = filemtime($filepath);
        if (key_exists($time, $entries)) {
          $time++;
        }
        $entries[$time] = array(
          'name' => $entry,
          'size' => $this->human_filesize(filesize($filepath)),
          'time' => $time,
        );
      }
    }
    ksort($entries);
    $this->view->entries = array_reverse($entries);
	}

	public function partsWarehouseAction(){
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
    $export_string = Parts::getPartsCSV(Zend_Registry::get('acl')->isAllowed('parts/detail/price'), $this->getParam('reservations'));
		$file_name = "export_sklad_soucasti_".date('d-m-Y');
		$this->_response->clearAllHeaders();
		$this->_response->setHeader('Content-Description', 'File Transfer');
		$this->_response->setHeader('Expires','0',true);
		$this->_response->setHeader('Pragma','public',true);
		$this->_response->setHeader('Cache-Control','must-revalidate, post-check=0, pre-check=0',true);
		$this->_response->setHeader('Content-Type', 'text/csv; charset=windows-1250');
		$this->_response->setHeader('Content-Disposition', 'attachment; filename="' . $file_name . '.csv"', true);
		$this->_response->setBody($export_string);
	}

	public function ordersAction(){
		$db = Zend_Registry::get('db');
		$select = $db->select();
		$select->from('orders')->where("status IN ('new', 'open')");
		$orders = $db->fetchAll($select);
		$this->view->orders = $orders;
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$orders_items = array();
		foreach($orders as $order){
			$select = $db->select();
			$select->from(array('op' => 'orders_products'))
			->joinLeft(array('p' => 'parts'), "op.id_products = p.id")
			->where("id_orders = ?", $order['id']);
			$orders_items[$order['id']] = $db->fetchAll($select);
		}
		$this->view->orders_items = $orders_items;

		$text_delimiter = '"';
		$field_delimiter = ';';
		$line_delimiter = "\n";
		$export_string = $text_delimiter . $text_delimiter . $field_delimiter
      . $text_delimiter . 'EXPORT OBJEDNÁVEK K DATU ' . date('d.m.Y')
      . $text_delimiter . $field_delimiter . $line_delimiter . $line_delimiter;

		$export_string .=
			 $text_delimiter."Číslo".$text_delimiter.$field_delimiter.
			 $text_delimiter."Název".$text_delimiter.$field_delimiter.
			 $text_delimiter."Datum přijetí".$text_delimiter.$field_delimiter.
			 $text_delimiter."Datum otevření".$text_delimiter.$field_delimiter.
			 $text_delimiter."Deadline".$text_delimiter.$field_delimiter.
			 $text_delimiter."Poznámka".$text_delimiter.$field_delimiter.
			 $line_delimiter;
		foreach($orders as $order){
			$export_string .=
				 $text_delimiter.$order['no'].$text_delimiter.$field_delimiter.
				 $text_delimiter.$order['title'].$text_delimiter.$field_delimiter.
				 $text_delimiter.$this->view->dateFromDb($order['date_request']).
         $text_delimiter.$field_delimiter.
				 $text_delimiter.$this->view->dateFromDb($order['date_open']).
         $text_delimiter.$field_delimiter.
				 $text_delimiter.$this->view->dateFromDb($order['date_deadline']).
         $text_delimiter.$field_delimiter.
				 $text_delimiter.$order['description'].$text_delimiter.$field_delimiter.
				 $line_delimiter;
		}

		foreach($orders as $order){
			$export_string .= $line_delimiter.$line_delimiter.
				$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.
        "OBJEDNÁVKA:".$text_delimiter.$field_delimiter.$line_delimiter.
				$text_delimiter."Číslo:".$text_delimiter.$field_delimiter.
        $text_delimiter.$order['no'].$text_delimiter.$line_delimiter.
				$text_delimiter."Název:".$text_delimiter.$field_delimiter.$text_delimiter.
        $order['title'].$text_delimiter.$line_delimiter.
				$text_delimiter."Datum přijetí:".$text_delimiter.$field_delimiter.
        $text_delimiter.$this->view->dateFromDb($order['date_request']).
        $text_delimiter.$line_delimiter.
				$text_delimiter."Otevřeno:".$text_delimiter.$field_delimiter.
        $text_delimiter.$this->view->dateFromDb($order['date_open']).
        $text_delimiter.$line_delimiter.
				$text_delimiter."Deadline:".$text_delimiter.$field_delimiter.
        $text_delimiter.$this->view->dateFromDb($order['date_deadline']).
        $text_delimiter.$line_delimiter.
				$text_delimiter."Poznámka:".$text_delimiter.$field_delimiter.
        $text_delimiter.$order['description'].$text_delimiter.
				$line_delimiter;
			$i=0;
			$export_string .= $line_delimiter;
			$export_string .=
				$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter.
        "POLOŽKY OBJEDNÁVKY:".$text_delimiter.$field_delimiter.$line_delimiter.
				$text_delimiter."ID".$text_delimiter.$field_delimiter.
				$text_delimiter."Název".$text_delimiter.$field_delimiter.
				$text_delimiter."Množství".$text_delimiter.$field_delimiter.
				$line_delimiter;
			foreach($orders_items[$order['id']] as $order_item){
				$class = ($i++ % 2) ? " class='odd'" : '';
				$export_string .=
					$text_delimiter.$order_item['id_products'].$text_delimiter.$field_delimiter.
					$text_delimiter.$order_item['name'].$text_delimiter.$field_delimiter.
					$text_delimiter.$order_item['amount'].$text_delimiter.$field_delimiter.
					$line_delimiter;
			}
		}
		$export_string .= $line_delimiter.$line_delimiter.$line_delimiter;

		$file_name = "export_objednavek_".date('d-m-Y');

		$this->_response->clearAllHeaders();
		$this->_response->setHeader('Content-Description', 'File Transfer');
		$this->_response->setHeader('Expires','0',true);
		$this->_response->setHeader('Pragma','public',true);
		$this->_response->setHeader('Cache-Control','must-revalidate, post-check=0, pre-check=0', true);
		$this->_response->setHeader('Content-Type', 'text/csv; charset=windows-1250');
		$this->_response->setHeader('Content-Disposition', 'attachment; filename="'
      .$file_name.'.csv"',true);
		$this->_response->setBody($export_string);
	}

	public function orderAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if($this->getRequest()->getParam('id')){

			$order = $this->_db->fetchRow("SELECT o.*, (SELECT `name` FROM orders_carriers WHERE id = o.id_orders_carriers ) AS carrier FROM orders AS o WHERE id = :id", array('id' => $this->getRequest()->getParam('id')));

			$text_delimiter = '"';
			$field_delimiter = ';';
			$line_delimiter = "\n";
			$export_string =
				$text_delimiter."Číslo".$text_delimiter.$field_delimiter.
				$text_delimiter."Název".$text_delimiter.$field_delimiter.
				$text_delimiter."Dopravce".$text_delimiter.$field_delimiter.
				$text_delimiter."Datum přijetí".$text_delimiter.$field_delimiter.
				$text_delimiter."Datum otevření".$text_delimiter.$field_delimiter.
				$text_delimiter."Deadline".$text_delimiter.$field_delimiter.
				$text_delimiter."Poznámka".$text_delimiter.$field_delimiter.
				$line_delimiter;
			$export_string .=
				$text_delimiter.$order['no'].$text_delimiter.$field_delimiter.
				$text_delimiter.$order['title'].$text_delimiter.$field_delimiter.
				$text_delimiter.$order['carrier'].$text_delimiter.$field_delimiter.
				$text_delimiter.$this->view->dateFromDb($order['date_request']).$text_delimiter.$field_delimiter.
				$text_delimiter.$this->view->dateFromDb($order['date_open']).$text_delimiter.$field_delimiter.
				$text_delimiter.$this->view->dateFromDb($order['date_deadline']).$text_delimiter.$field_delimiter.
				$text_delimiter.$order['description'].$text_delimiter.$field_delimiter.
				$line_delimiter;

			$export_string .=
				$line_delimiter.$text_delimiter.$text_delimiter.$field_delimiter.$text_delimiter."POLOŽKY OBJEDNÁVKY:".$text_delimiter.$field_delimiter.$line_delimiter.
				$text_delimiter."ID".$text_delimiter.$field_delimiter.
				$text_delimiter."Název".$text_delimiter.$field_delimiter.
				$text_delimiter."Množství".$text_delimiter.$field_delimiter.
				$text_delimiter."Cena bez DPH".$text_delimiter.$field_delimiter.
				$text_delimiter."Cena včetně DPH".$text_delimiter.$field_delimiter.
				$line_delimiter;

			$select = $this->_db->select();
			$select->from(array('op' => 'orders_products'))
			->joinLeft(array('p' => 'parts'), "op.id_products = p.id")
			->where("id_orders = ?", $order['id']);
			$orders_items = $this->_db->fetchAll($select);

			foreach($orders_items as $order_item){
				$export_string .=
					$text_delimiter.$order_item['id_products'].$text_delimiter.$field_delimiter.
					$text_delimiter.$order_item['name'].$text_delimiter.$field_delimiter.
					$text_delimiter.$order_item['amount'].$text_delimiter.$field_delimiter.
					$text_delimiter.$order_item['amount']*$order_item['end_price'].$text_delimiter.$field_delimiter.
					$text_delimiter.($order_item['amount']*$order_item['end_price']*'1.2').$text_delimiter.$field_delimiter.
					$line_delimiter;
			}

			$file_name = "objednávka_".$order['no']."_".$order['title'];
			$this->_response->clearAllHeaders();
			$this->_response->setHeader('Content-Description', 'File Transfer');
			$this->_response->setHeader('Expires','0',true);
			$this->_response->setHeader('Pragma','public',true);
			$this->_response->setHeader('Cache-Control','must-revalidate, post-check=0,pre-check=0',true);
			$this->_response->setHeader('Content-Type', 'text/csv; charset=windows-1250');
			$this->_response->setHeader('Content-Disposition', 'attachment; filename="'.$file_name.'.csv"',true);
			$this->_response->setBody($export_string);
		}
	}

  public function partsWarehouseExcelAction(){
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout->disableLayout();
    $reservations = $this->getParam('reservations') === 'true';
    $thrownParts = $this->getParam('thrown-parts') === 'true';
    $objPHPExcel = Parts::getPartsXLSX(Zend_Registry::get('acl')
      ->isAllowed('parts/detail/price'), $reservations, $thrownParts);
    if ($thrownParts) {
      $filename = 'export_skladu_vyrazenych_soucasti-' . date('ymd') . '.xlsx';
    }
    else {
      $filename = 'export_skladu_soucasti-' . date('ymd') . ($reservations ? '-vcetne_rezervaci' : '') . '.xlsx';
    }
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $objWriter->setOffice2003Compatibility(TRUE);
    $objWriter->save(Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH . $filename);
    $this->redirect($this->view->url([
      'action' => 'download',
      'filename' => $filename,
    ]));
  }

  public function downloadAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
    $ext = $this->_request->getParam('ext', '');
		$filename = $this->_request->getParam('filename') . ($ext ? '.' . $ext : '');
		if (!empty($filename)) {
			$filepath = Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH . $filename;
			$fp = fopen($filepath, 'rb');
			$this->_response->clearAllHeaders();
			$this->_response->setHeader('Content-Description', 'File Transfer');
			$this->_response->setHeader('Expires','0',true);
			$this->_response->setHeader('Pragma','public',true);
			$this->_response->setHeader('Cache-Control','must-revalidate, post-check=0,pre-check=0', true);
			$tokens = explode('.', $filename);
			switch (array_pop($tokens)) {
				case 'xls':  $type = 'application/vnd.ms-excel'; break;
				case 'xlsx': $type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'; break;
        case 'pdf': $type = 'application/pdf'; break;
				default: $type = '';
			}
			if ($type) {
        $this->_response->setHeader('Content-Type', $type);
      }
			$this->_response->setHeader('Content-Length',  filesize($filepath));
			$this->_response->setHeader('Content-Disposition', 'attachment; filename="' . $filename, true);
			fpassthru($fp);
			fclose($fp);

			if($this->_request->getParam('unlink') == 1) {
			  unlink($filepath);
      }
		}
	}

  private function human_filesize($bytes, $decimals = 2) {
    $sz = 'BKMGTP';
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f ", $bytes / pow(1024, $factor)) . substr($sz, $factor, 1);
  }


  public function productsCostsAction() {
	  $products = array();
    $table = new Table_Parts();
	  $productId = $this->_request->getParam('id');
    if ($productId) {
      $products[] = $table->select()->where('id = ?', $productId)->query()->fetch();
    }
    else {
      $products = $table->getProductsByCategory(Table_PartsCtg::TYPE_SETS)->query()->fetchAll();
    }
    if ($products) {
      $data = array();
      foreach ($products as $item) {
        $data[$item['id']] = array(
          'id' => $item['id'],
          'name' => $item['name'],
          'category' => isset(Table_PartsCtg::$ctgToString[$item['id_parts_ctg']]) ? Table_PartsCtg::$ctgToString[$item['id_parts_ctg']] : '',
          'price' => $item['price'] - $item['operations_cost'],
          'hs' => $item['hs'],
          'amount' => 1,
          'subparts' => $table->getTree($item['id'], FALSE),
        );
      }
    }

    if ($data) {
      switch ($this->_request->getParam('output')) {
        case 'xlsx':
          $objPHPExcel = $this->getTreeXLSX($data, FALSE);
          $filename = count($data) > 1 ? "skladba" : "skladba_" . $productId;
          $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
          $objWriter->setOffice2003Compatibility(TRUE);
          $objWriter->save(Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH . $filename . '.xlsx');
          $this->redirect($this->view->url(array(
            'action' => 'download',
            'filename' => $filename,
            'ext' => 'xlsx',
            'unlink' => 1,
            'id' => NULL,
            'output' => NULL,
          )));
          break;
        case 'pdf':
          $filename = count($data) > 1 ? "skladba" : "skladba_" . $productId;
          $doc = $this->getTreePDF($data);
          $doc->save(Zend_Registry::get('config')->data->path . self::DOWNLOAD_PATH . $filename . ".pdf");
          $this->redirect($this->view->url(array(
            'action' => 'download',
            'filename' => $filename,
            'ext' => 'pdf',
            'unlink' => 1,
            'id' => NULL,
            'output' => NULL,
          )));
          break;
        default:
          $this->_helper->layout->setLayout('bootstrap-basic');
          $this->view->title = 'Materiálová skladba';
          $this->view->data = $data;
          break;
      }
    }
  }
  private function getTreeXLSX($data, $withOperations = TRUE) {
    $doc = new PHPExcel();
    $doc->removeSheetByIndex();
    foreach ($data as $item) {
      $sheet = $doc->addSheet(new PHPExcel_Worksheet())->setTitle($item['id']);

      $sheet->getCellByColumnAndRow(0, 1)->setValue('ID');
      $sheet->getCellByColumnAndRow(1, 1)->setValue($item['id']);
      $sheet->getCellByColumnAndRow(0, 2)->setValue('Název');
      $sheet->getCellByColumnAndRow(1, 2)->setValue($item['name']);
      $sheet->getCellByColumnAndRow(0, 3)->setValue('Kategorie');
      $sheet->getCellByColumnAndRow(1, 3)->setValue($item['category']);
      $sheet->getCellByColumnAndRow(0, 4)->setValue('HS');
      $sheet->getCellByColumnAndRow(1, 4)->setValue($item['hs']);
      $sheet->getCellByColumnAndRow(0, 5)->setValue('Cena');
      $sheet->getCellByColumnAndRow(1, 5)->setValue($item['price']);

      $offCol = 0; // start
      $offRow = 7; // start
      $col = $offCol;
      $row = $offRow;
      $col = $this->getTreeXLSX_printID($sheet, $item, $col, $row) + 1;

      $row = $offRow;
      $this->getTreeXLSX_printInfos($sheet, $item, $col, $row);

    }

    return $doc;
  }
  private function getTreeXLSX_printID(PHPExcel_Worksheet $sheet, $tree, $col, &$row) {
    $tabs = $col;
    $sheet->getCellByColumnAndRow($col, $row)->setValue($tree['id']);
    if (!empty($tree['subparts'])) {
      foreach ($tree['subparts'] as $subtree) {
        $row++;
        $tabs = max($tabs, $this->getTreeXLSX_printID($sheet, $subtree, $col + 1, $row));
      }
      return $tabs;
    }
    else {
      return $col;
    }
  }
  private function getTreeXLSX_printInfos(PHPExcel_Worksheet $sheet, $tree, $col, &$row) {
    $sheet->getCellByColumnAndRow($col + 0, $row)->setValue($tree['amount']);
    $sheet->getCellByColumnAndRow($col + 1, $row)->setValue($tree['price'])->getStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
    $sheet->getCellByColumnAndRow($col + 2, $row)->setValue($tree['name']);
    if (!empty($tree['subparts'])) {
      foreach ($tree['subparts'] as $subtree) {
        $row++;
        $this->getTreeXLSX_printInfos($sheet, $subtree, $col, $row);
      }
    }
  }

  private function getTreePDF($data) {
    $doc = new Meduse_Pdf(Meduse_Pdf::BLANK_PDF_PATH, NULL, TRUE);
    $col = 20;
    $row = 800;
    foreach ($data as $item) {
      $doc->fillTitle($item['id'] . ' – ' .  $item['name'], array($col, $row));
      $row -= 20;
      $doc->fillText(array('Kategorie: ' .  $item['category'], 'HS: ' .  $item['hs'], 'Cena: ' .  $item['price']), array($col, $row));
      list($col, $row) = $doc->getCoords();
      $row -= 20;
      $this->getTreePDF_print($doc, $item, $col, $row);
      $doc->pages[] = new Zend_Pdf_Page($doc->pages[0]);
      $doc->setPageNumber($doc->getPageNumber()+1);
      $col = 20;
      $row = 800;
    }
    return $doc;
  }

  private function getTreePDF_print(Meduse_Pdf $doc, $tree, $col, &$row) {
    $tabs = $col;
    if ($row < 10) {
      $doc->pages[] = new Zend_Pdf_Page($doc->pages[0]);
      $doc->setPageNumber($doc->getPageNumber()+1);
      $row = 800;
    }
    $doc->fillText($tree['id'] . ' - ' . $tree['name'], array($col, $row));
    $doc->fillText($tree['amount'] . ' ks', array(510, $row), Meduse_Pdf::TEXT_ALIGN_RIGHT);
    $doc->fillText(Meduse_Currency::format($tree['price'], ' Kč', 2, FALSE), array(570, $row), Meduse_Pdf::TEXT_ALIGN_RIGHT);
    if (!empty($tree['subparts'])) {
      foreach ($tree['subparts'] as $subtree) {
        list(, $row) = $doc->getCoords();
        $tabs = max($tabs, $this->getTreePDF_print($doc, $subtree, $col + 10, $row));
      }
      return $tabs;
    }
    else {
      return $col;
    }
  }
}
