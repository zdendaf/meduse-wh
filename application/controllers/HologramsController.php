<?php
/**
 * Description of HologramsController
 *
 * @author Zdeněk
 */
class HologramsController extends Meduse_Controller_Action{

    public function indexAction(){
           
            $form = new Holograms_FilterForm();

            $form->getElement('collection')->setValue($this->_request->getParam('collection'));
            $form->getElement('order_no')->setValue($this->_request->getParam('order_no'));
            $form->getElement('parts_id')->setValue($this->_request->getParam('parts_id'));
            $form->getElement('unassigned')->setValue($this->_request->getParam('unassigned'));            
            
            $this->view->form = $form;

            $this->view->holograms = Holograms_Hologram::getAllHolograms(array(
                'collection' => $this->_request->getParam('collection'),
                'order_no' => $this->_request->getParam('order_no'),
                'parts_id' => $this->_request->getParam('parts_id'),
                'unassigned' => $this->_request->getParam('unassigned')
            ));

            $importForm = new Holograms_ImportForm();
            $this->view->import_form = $importForm;
            
            $this->_helper->layout->setLayout('bootstrap-basic');
            $this->view->title = "Přehled všech hologramů";        
    }    
    
    public function importAction() {
        if ($this->_request->getParam('send') == 'Importovat') {
            $colId = ($this->_request->getParam('collections') == '0')? 
                    null : $this->_request->getParam('collections');
            if (is_null($colId)) {
                $this->view->collection = array('name' => 'bez kolekce');
            } else {
                $tCollections = new Table_Collections();
                $this->view->collection = $tCollections->find($colId)->current();
            }
            $serials = explode("\r\n", $this->_request->getParam('serials'));
            $this->view->imported = array();
            $this->view->not_imported = array();
            foreach ($serials as $serial) {
                try {
                    $hologram = new Holograms_Hologram(array(
                        'serial' => $serial,
                        'category' => $colId
                    ));
                    $this->view->imported[] = $serial;
                } catch (Zend_Exception $e) {
                    $this->view->not_imported[] = $serial;
                }
            }
            $importForm = new Holograms_ImportForm();
            $this->view->import_form = $importForm;            
            
            $this->_helper->layout->setLayout('bootstrap-basic');
            $this->view->title = "Výsledek importu";   
        }
        else {
            $this->_redirect('/holograms');
        }
        
    }
	
	public function deleteAction() {
		try {
			$hologram = new Holograms_Hologram(array('id' => $this->_request->getParam('id')));
			$tHolograms = new Table_Holograms();
			$retValue = $tHolograms->delete('id = ' . $tHolograms->getAdapter()->quote($this->_request->getParam('id')));
			if ($retValue) {
				$this->_flashMessenger->addMessage('Hologram "' . $hologram->getCode() .'" byl vymazán.');
			} else {
				$this->_flashMessenger->addMessage('Hologram "' . $hologram->getCode() . '" se nepodařilo vymazat.');
			}
		} catch (Exception $e) {
			$this->_flashMessenger->addMessage('Hologram se nepodařilo vymazat.');
		}
		$this->_redirect('/holograms');
	}
  
  /**
   * Vraci dostupna seriova cisla v pozadovanem poctu pro jednotlive kolekce. 
   * URL dotazu 
   *    /holograms/ajax-get-available/1/{pocet_CR}/2/{pocet_SE}/3/{pocet_NA}/4/{pocet_SN}
   * kde 
   *    CR ... Craft Collection   (ID=1)
   *    SE ... Sepia Collection   (ID=2)
   *    NA ... Nautila Collection (ID=3)
   *    SN ... Sniper Collection  (ID=4)
   */
  public function ajaxGetAvailableAction() {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);
    $tCollections = new Table_Collections();
    $collections = $tCollections->getCollectionsSelect(TRUE)->query(Zend_DB::FETCH_ASSOC)->fetchAll();
    $counts = [];
    foreach ($collections as $collection) {
      $counts[$collection['code']] = $this->_request->getParam($collection['code'], 0);
    }
    $table = new Table_Holograms();
    $serials = $table->getAvailable($counts);
    $data = array();
    if ($serials) {
      foreach ($serials as $serial) {
        $data[$serial['collection']][] = $serial['serial'];
      }
    }
    $this->_helper->json($data);
  }
}