<?php

class PdfController extends Meduse_Controller_Action {

	public function  init() {
		parent::init();
	}

	public function productionAction() {

		$productions = new Table_Productions;
		$TProducers = new Table_Producers();
		$this->view->producer_data = $productions->getDetails($this->_request->getParam('id'));
		$this->view->title .= $this->view->producer_data['name'];
		$this->view->parts = $productions->getParts($this->_request->getParam('id'));

		foreach($this->view->parts as $key => $part) { //Zend_Debug::dump($this->view->details); die;
			$this->view->parts[$key]['p_id'] = $TProducers->getPartId($this->view->producer_data->id_producers, $part['id']);
			$this->view->parts[$key]['p_name'] = $TProducers->getPartName($this->view->producer_data->id_producers, $part['id']);
		}

		//TODO: dodaci listy
		//$this->view->deliveries = $productions->getDeliveries($this->_request->getParam('id'));

		$dompdf = new DOMPDF();
    $html = $this->view->render('pdf/production.phtml');

    $dompdf->set_paper('letter');
    $dompdf->load_html($html);
    $dompdf->render();
    $dompdf->stream('Výroba_'.str_replace(' ', '_', $this->view->producer_data->name)."_".date("d.m.Y"));
    die;
	}

}
