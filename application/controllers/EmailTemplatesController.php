<?php

class EmailTemplatesController extends Meduse_Controller_Action{
	
	public function init() {
    $this->_helper->layout->setLayout('bootstrap-basic');
  }
  
  public function indexAction() {
		$this->view->title = 'Správa e-mailových šablon';		
		$tEmailTemplates = new Table_EmailTemplates();
		$this->view->templates = $tEmailTemplates->fetchAll('id_wh_type = 1')->toArray();
	}
	
	public function addAction() {
		$this->view->title = 'Vložení nové e-mailové šablony';
		$form = new EmailTemplates_Form();
		$form->setAction($this->view->url(array('action' => 'save')));
		$this->view->form = $form;
	}

	public function editAction() {
		$tid = (int) $this->_request->getParam('id');
		$this->view->title = 'Úprava e-mailové šablony';		
		$emailTemplates = new Table_EmailTemplates();
		$template = $emailTemplates->find($tid)->current()->toArray();
		$form = new EmailTemplates_Form();
		$form->setAction($this->view->url(array('action' => 'save')));
		$form->populate(array(
			'email_template_id' => $template['id'],
			'email_template_name' => $template['name'],
			'email_template_subject' => $template['subject'],
			'email_template_body' => $template['body']
		));
		$this->view->form = $form;
	}	
	
	
	public function saveAction() {
		$params = $this->_request->getParams();
		$emailTemplates = new Table_EmailTemplates();
		if ($params['email_template_id']) {
			// update existujici sablony
			$emailTemplates->update(array(
				'name' => $params['email_template_name'],
				'subject' => $params['email_template_subject'],
				'body' => $params['email_template_body']				
			), 'id = ' . $emailTemplates->getAdapter()->quote($params['email_template_id']));
		} else {
			// vytvoreni nove sablony
			$emailTemplates->insert(array(
				'name' => $params['email_template_name'],
				'subject' => $params['email_template_subject'],
				'body' => $params['email_template_body']
			));
		}
		$this->_redirect($this->view->url(array('action' => 'index')));
	}
	
	public function deleteAction() {
		$tid = $this->_request->getParam('id');
		$emailTeplates = new Table_EmailTemplates();
		$result = $emailTeplates->delete('id = ' . $emailTeplates->getAdapter()->quote($tid));
		$this->_flashMessenger = $result ? 'Šablona byla vymazána' : 'Šablonu se nepodařilo vymazat';
		$this->_redirect($this->view->url(array('action' => 'index')));
	}
	
	public function ajaxGetTemplateAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$tid = $this->_request->getParam('id');
		$emailTemplates = new Table_EmailTemplates();
		$template = $emailTemplates->find($tid)->current();
    
    //proměnné v těle e-mailu
    $oid = $this->_request->getParam('oid');
    if ($oid) {
      // z tabulky orders
      $tOrders = new Table_Orders();
      $order = $tOrders->find($oid)->current()->toArray();
      while (preg_match('/\[\[([^:\]]*)\]\]/', $template['body'], $matches)) {        
        $template['body'] = str_replace($matches[0], $order[$matches[1]], $template['body']);
      }
      
      // z tabulky addresses
      $tAddresses = new Table_Addresses;
      $address = $tAddresses->find($order['id_addresses'])->current()->toArray();
      while (preg_match('/\[\[addresses:([^\]]*)\]\]/', $template['body'], $matches)) {
        $template['body'] = str_replace($matches[0], $address[$matches[1]], $template['body']);
      }
    }
    
		if ($template) {
			echo json_encode($template->toArray());
		} else {
			echo json_encode(array(
				'id' => 0,
				'name' => '',
				'subject' => '',
				'body' => '',
				'changed' => '0000-00-00 00:00:00'
			));
		}
	}
}