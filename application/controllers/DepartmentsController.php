<?php

class DepartmentsController extends Meduse_Controller_Action {

  public function init() {
    $this->_helper->layout->setLayout('bootstrap-employees');
    parent::init();
  }

	public function indexAction() {
    $tDepts = new Table_Departments();
    $gDepts = $tDepts->getGrid(array(
      'css_class' => 'table table-bordered table-hover',
      'add_link_url' => $this->view->url(array('action' => 'add')),
      'allow_add_link' => Zend_Registry::get('acl')->isAllowed('departments/add'),
      'add_link_title' => _('Přidat oddělení'),
      'columns' => array(
        'name' => array(
          'header' => 'název'
        ),
        'leader' => array(
          'header' => 'vedoucí',
        ),
        /* @TODO dodělat budget
        'budget' => array(
          'header' => '',
          'renderer' => 'bootstrap_action',
          'type' => 'add',
          'url' => $this->view->url(array('action' => 'budget', 'id' => '{id}')),
        ),
         */
        'edit' => array(
          'header' => '',
          'renderer' => 'bootstrap_action',
          'type' => 'edit',
          'url' => $this->view->url(array('action' => 'edit', 'id' => '{id}')),
        )
      )
    ));
    $this->view->grid = $gDepts->render();
  }


  public function addAction() {
    $form = new Departments_EditForm();
    $form->setLegend('Nové oddělení');
    if ($this->_request->isPost()) {
      $data = $this->_request->getParams();
      if ($form->isValid($data)) {
        $tDepts = new Table_Departments();
        $tDepts->insert(array(
          'name' => $data['name'],
          'id_wh_type' => $data['id_wh_type'],
          'id_users' => $data['id_users'],
        ));
        $this->_flashMessenger->addMessage('Oddělení "' . $data['name'] . '" bylo uloženo.');
        $this->_redirect($this->view->url(array('action' => 'index')));
        exit;
      } else {
        $form->populate($data);
      }
    }
    $this->view->form = $form;
    $this->renderScript('departments/edit.phtml');
  }

  public function editAction() {
    $form = new Departments_EditForm();
    $form->setLegend('Editace oddělení');
    $tDepts = new Table_Departments();
    $data = array();
    if ($this->_request->isPost()) {
      $data = $this->_request->getParams();
      if ($form->isValid($data)) {
        $tDepts->update(array(
          'name' => $data['name'],
          'id_wh_type' => $data['id_wh_type'],
          'id_users' => $data['id_users'],
        ), 'id = ' . $data['id']);
        $this->_flashMessenger->addMessage('Oddělení "' . $data['name'] . '" bylo upraveno.');
        $this->_redirect($this->view->url(array('action' => 'index', 'id' => NULL)));
        exit;
      }
    } else {
      $data = $tDepts->find($this->_request->getParam('id'))->current()->toArray();
    }
    $form->populate($data);
    $this->view->form = $form;
  }

  public function removeAction() {
    die('Departments remove');
  }

  public function budgetAction() {

    $departmentId = $this->_request->getParam('id');
    $tDepts = new Table_Departments();
    $dept = $tDepts->find($departmentId)->current()->toArray();

    $tBudgets = new Table_Budgets();
    $gBudgets = $tBudgets->getGrid(array(

      'css_class' => 'table table-bordered table-hover',
      'add_link_url' => $this->view->url(array(
        'action' => 'budget-add',
        'id' => $departmentId
      )),
      'allow_add_link' => Zend_Registry::get('acl')->isAllowed('departments/budget-add'),
      'add_link_title' => _('Přidat ropočet'),
      'columns' => array(
        'from' => array(
          'header' => 'od',
          'renderer' => 'date',
        ),
        'to' => array(
          'header' => 'do',
          'renderer' => 'date',
        ),
        'amount' => array(
          'header' => 'částka',
          'renderer' => 'currency',
          'currency_options' => array(
            'currency_value' => 'amount',
          ),
        ),
        'edit' => array(
          'header' => '',
          'renderer' => 'bootstrap_action',
          'type' => 'edit',
          'url' => $this->view->url(array('action' => 'budget-edit', 'budget' => '{id}')),
        )
      )
    ), $departmentId);
    $this->view->department = $dept;
    $this->view->grid = $gBudgets->render();
  }

  public function budgetAddAction() {

    Zend_Date::setOptions(array('format_type' => 'php'));

    $form = new Departments_BudgetForm();
    $form->setLegend('Nový rozpočet');

    if ($this->_request->isPost()) {
      $data = $this->_request->getParams();
      if ($form->isValid($data)) {
        $from = new Zend_Date($data['from'], 'd.m.Y');
        $to = new Zend_Date($data['to'], 'd.m.Y');
        $tBudget = new Table_Budgets();
        $tBudget->insert(array(
          'id_departments' => $data['id'],
          'from' => $from->toString('Y-m-d'),
          'to' => $to->toString('Y-m-d'),
          'amount' => $data['amount'],
        ));
        $this->_flashMessenger->addMessage('Rozpočet byl uložen');
        $this->_redirect($this->view->url(array('action' => 'budget', 'id' => $data['id'])));
      } else {
        $form->populate($data);
      }
    }

    $this->view->form = $form;
    $this->renderScript('departments/budget-edit.phtml');
  }


  public function budgetEditAction() {

    Zend_Date::setOptions(array('format_type' => 'php'));

    $form = new Departments_BudgetForm();
    $form->setLegend('Úprava rozpočtu');

    $budgetId = $this->_request->getParam('budget');
    $deptId = $this->_request->getParam('id');
    $data = array();
    $tBudget = new Table_Budgets();

    if ($this->_request->isPost()) {
      $data = $this->_request->getParams();
      if ($form->isValid($data)) {
        $from = new Zend_Date($data['from'], 'd.m.Y');
        $to = new Zend_Date($data['to'], 'd.m.Y');
        $tBudget->update(array(
          'from' => $from->toString('Y-m-d'),
          'to' => $to->toString('Y-m-d'),
          'amount' => $data['amount'],
        ), 'id = ' . $budgetId . ' AND id_departments = ' . $deptId);
        $this->_flashMessenger->addMessage('Rozpočet byl změněn');
        $this->_redirect($this->view->url(array('action' => 'budget', 'id' => $data['id'])));
      }
    } else {
      $data = $tBudget->find($budgetId)->current()->toArray();
    }
    $form->populate($data);
    $this->view->form = $form;
    $this->renderScript('departments/budget-edit.phtml');
  }
}