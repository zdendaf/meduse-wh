<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class TobaccoProducersController extends Meduse_Controller_Action {

    public function init() {
      $this->_helper->layout->setLayout('bootstrap-tobacco');
    }

    public function carriersAction() {
      $this->view->title = "Dopravci";
      $tProducers = new Table_Producers();
      $this->view->active_carriers = $tProducers->getCarriers('active', Parts_Part::WH_TYPE_TOBACCO);
      $this->view->inactive_carriers = $tProducers->getCarriers('inactive', Parts_Part::WH_TYPE_TOBACCO);
    }

  	public function detailCarrierAction(){
      $carrier = new Producers_Carrier($this->_request->getParam('id'));
      $tDPH = new Table_ApplicationDph();
      $this->view->vat = $tDPH->getActualDph();
      $this->view->carrier = $carrier;
      $this->view->title = 'Dopravce ' . $carrier->getName();
    }

    public function addCarrierAction() {

      $this->view->title = "Nový dopravce";

      $form = new Orders_CarrierForm();
      $form->setLegend($this->view->title);

      // formular kontaktni osoba
      $person = new Producers_PersonForm();
      $form->addElement($person->getElement('meduse_producer_deputy'));
      $form->addElement($person->getElement('meduse_producer_phone'));
      $form->addElement($person->getElement('meduse_producer_phone2'));

      if($this->_request->isPost() && $form->isValid($this->_request->getParams())){

        // vytvoreni dodavatele
        $carrier = new Producers_Carrier();
        $dataCarrier = array(
          'id_wh_type' => Parts_Part::WH_TYPE_TOBACCO,
          'name' => $form->getValue('meduse_carrier_name'),
          'ident_no' => $form->getValue('meduse_carrier_ident_no'),
          'id_producers_ctg' => 0,
          'address' => $this->_request->getParam('meduse_carrier_address'),
          'www' => $this->_request->getParam('meduse_carrier_www'),
          'description' => $this->_request->getParam('meduse_carrier_desc'),
        );
        $carrier->create($dataCarrier);

        // vlozeni kontaktni osoby
        $dataPerson = array(
          'id_producers' => $carrier->getId(),
          'name' => $this->_request->getParam('meduse_producer_deputy'),
          'phone' => $this->_request->getParam('meduse_producer_phone'),
          'phone2' => $this->_request->getParam('meduse_producer_phone2')
        );
        $carrier->addPerson($dataPerson);

        // vlozeni zpusobu prepravy
        $dataDelivery = array(
          'name' => 'výchozí',
          'inserted' => new Zend_Db_Expr("NOW()")
        );
        $carrier->addDelivery($dataDelivery);

        $act = new Table_Actualities();
        $act->add("Vložil nového dopravce '".$form->getValue('meduse_carrier_name')."'.");
        $this->_flashMessenger->addMessage("Dopravce '".$form->getValue('meduse_carrier_name')."' uložen.");

        $this->_redirect($this->view->url(array(
          'controller' => 'tobacco-producers',
          'action' => 'detail-carrier',
          'id' => $carrier->getId(),
        )));
      }
      $this->view->form = $form;
    }

    public function editCarrierAction() {

			$this->view->title = 'Editace dopravce';

			$table = new Table_Producers();

			if( !($id = $this->_request->getParam('id')) || count($table->find($id)) < 1 ) return $this->_redirect('producers');

			$form = new Producers_Form();
			$form->setLegend($this->view->title);

			$this->view->title = 'Úprava dopravce';
			$form->setDescription("Úprava dopravce");
			$form->removeElement('meduse_producer_warehouse');
			$form->removeElement('meduse_producer_activity');
			$form->removeElement('meduse_producer_category');
			$form->addElement(new Zend_Form_Element_Hidden('meduse_producer_carriers'));

			if($this->_request->getParam('Odeslat') && $this->_request->isPost()){

				if($form->isValid($this->_request->getParams())){

					$wheId = $this->_request->getParam("meduse_producer_warehouse");

					$data = array(
						"name" => $this->_request->getParam("meduse_producer_name"),
						"ident_no" => $this->_request->getParam("meduse_producer_ident_no"),
						"address"	=> $this->_request->getParam("meduse_producer_address"),
						"www" => $this->_request->getParam("meduse_producer_www"),
						"activity" => $this->_request->getParam("meduse_producer_activity"),
						"id_extern_warehouses" => empty($wheId) ? new Zend_Db_Expr('NULL') : $wheId,
						"description"	=> $this->_request->getParam("meduse_producer_desc"),
						"id_producers_ctg" => $this->_request->getParam("meduse_producer_category"),
						"is_active" => $this->_request->getParam("meduse_producer_is_active")
					);

					if($this->_request->getParam("meduse_producer_warehouse")){
						$data['id_extern_warehouses'] = $this->_request->getParam("meduse_producer_warehouse");
					}

					$table->edit($data, $id);

					$act = new Table_Actualities();
					$act->add("Upravil dopravce '".$this->_request->getParam("meduse_producer_name")."'.");

					$this->_flashMessenger->addMessage("Dopravce upraven.");
					return $this->_redirect('tobacco-producers/detail-carrier/id/'.$id);
				}

			}
			else {

				$row = $table->find($id)->current();
				if(!empty($row->id_extern_warehouses)){
					$whe = new Table_PartsWarehouseExtern();
					$form->meduse_producer_warehouse->addMultiOption($row->id_extern_warehouses, $whe->getWhName($row->id_extern_warehouses));
					$form->meduse_producer_warehouse->setValue($row->id_extern_warehouses);

				}

				//plneni z databaze
				$row = $table->find($id)->current();
				$data = array(
					'meduse_producer_name'	    => $row->name,
          'meduse_producer_ident_no'	=> $row->ident_no,
					'meduse_producer_address'   => $row->address,
					'meduse_producer_www'	      => $row->www,
					'meduse_producer_desc'	    => $row->description,
					'meduse_producer_activity'  => $row->activity,
					"meduse_producer_category"  => $row->id_producers_ctg,
					"meduse_producer_is_active" => (($row->is_active == 'y') ? true : false),
					'meduse_producer_carriers' => $row->id_orders_carriers
				);

					$form->removeElement('meduse_producer_warehouse');
					$form->removeElement('meduse_producer_activity');
					$form->addElement(new Zend_Form_Element_Hidden('meduse_producer_carriers'));
					$data['meduse_producer_category'] = Table_Producers::CTG_CARRIERS;
				$form->populate($data);
			}

			$this->view->form = $form;
			$this->render('add-carrier');
		}

    public function addPersonAction() {
      if (!$this->_request->getParam('id')) {
        return $this->_redirect('producers');
      }

      $id_producers = (int) $this->_request->getParam('id');

      $table = new Table_Producers();
      $isCarrier = $table->isCarrier($id_producers);
      $this->view->title = ($isCarrier)? 	"Přidat kontakt - dopravce" : "Přidat kontakt - výrobce";

      $form = new Producers_PersonForm();
      $form->setLegend($this->view->title);

      // is carrier remove fields
      if($isCarrier) {
          $form->removeElement('meduse_producer_email');
          $form->removeElement('meduse_producer_position');
          $form->removeElement('meduse_producer_position_select');
      }

      if($this->_request->getParam('Odeslat') && $this->_request->isPost() && $form->isValid($this->_request->getParams())){

        //vlozeni kontaktni osoby
        $table2 = new Table_ProducersPersons();
        $table2->add(array(
          'id_producers' => $id_producers,
          'name'		=> $this->_request->getParam('meduse_producer_deputy'),
          'phone'		=> $this->_request->getParam('meduse_producer_phone'),
          'phone2'	=> $this->_request->getParam('meduse_producer_phone2'),
          'email'		=> $this->_request->getParam('meduse_producer_email'),
          'position_old' => $this->_request->getParam('meduse_producer_position'),
          'position' => $this->_request->getParam('meduse_producer_position_select')
        ));

        if ($isCarrier) {
          $act = new Table_Actualities();
          $act->add("Přidal dopravci nový kontakt '".$this->_request->getParam("meduse_producer_deputy")."'.");
          $this->_flashMessenger->addMessage("Dopravce uložen.");
          return $this->_redirect('tobacco-producers/detail-carrier/id/'.$id_producers);
        }
        else {
          $act = new Table_Actualities();
          $act->add("Přidal dodavateli nový kontakt '".$this->_request->getParam("meduse_producer_deputy")."'.");
          $this->_flashMessenger->addMessage("Dodavatel uložen.");
          return $this->_redirect('tobacco-producers/detail/id/'.$id_producers);
        }
      }
      $this->view->form = $form;
      $this->render('add');
    }

    public function editPersonAction() {

      $isCarrier = $this->_getParam('carrier');

      $form = new Producers_PersonForm();
      $form->setLegend("Upravit kontakt");

      // is carrier remove fields
      if(isset($isCarrier)) {
        $form->removeElement('meduse_producer_email');
        $form->removeElement('meduse_producer_position');
        $form->removeElement('meduse_producer_position_select');
      }

      $table = new Table_ProducersPersons();

      if (!($id = $this->_request->getParam('id')) || count($table->find($id)) < 1) {
        return $this->_redirect('producers');
      }

      if ($this->_request->getParam('Odeslat') && $this->_request->isPost()) {

        $table->edit(array(
          'name'		=> $this->_request->getParam('meduse_producer_deputy'),
          'phone'		=> $this->_request->getParam('meduse_producer_phone'),
          'phone2'	=> $this->_request->getParam('meduse_producer_phone2'),
          'email'		=> $this->_request->getParam('meduse_producer_email'),
          'position_old'	=> $this->_request->getParam('meduse_producer_position'),
          'position'	=> $this->_request->getParam('meduse_producer_position_select')
        ),$id);

        $this->_flashMessenger->addMessage("Kontakt byl upraven");
        $row = $table->find($id)->current();

        $act = new Table_Actualities();
        $table = new Table_Producers();

        if ($table->isCarrier($row->id_producers)) {
          $act->add("Upravil kontakt dopravce '".$this->_request->getParam("meduse_producer_name")."'.");
          return $this->_redirect('tobacco-producers/detail-carrier/id/'.$row->id_producers);
        }
        else {
          $act->add("Upravil kontakt dodavatele '".$this->_request->getParam("meduse_producer_name")."'.");
          return $this->_redirect('tobacco-producers/detail/id/'.$row->id_producers);
        }
      }
      else {
        //plneni z databaze
        $row = $table->find($id)->current();
        $data = array(
          'meduse_producer_deputy'	=> $row->name,
          'meduse_producer_phone'		=> $row->phone,
          'meduse_producer_phone2'	=> $row->phone2,
          'meduse_producer_email'		=> $row->email,
          'meduse_producer_position'	=> $row->position_old,
          'meduse_producer_position_select' => $row->position
        );
        $form->populate($data);
      }

      $this->view->form = $form;
      return $this->render('add');
    }

    public function addCarrierMethodAction() {
      $form = new Orders_CarrierMethodForm();
      $form->removeElement('is_active');
      if ($this->_request->isPost()) {
        $data = $this->_request->getParams();
        if ($form->isValid($data)) {
          $carrier = new Producers_Carrier($form->getValue('id_producers'));
          $data = array(
            'name' => $form->getValue('name'),
            'default_price' => $form->getValue('default_price') ? (float) $form->getValue('default_price') : NULL,
            'is_pos' => $form->getValue('is_pos'),
          );
          $carrier->addDelivery($data);
          $this->_flashMessenger->addMessage('Metoda byla přidána');
          $this->_redirect($this->view->url(array('action' => 'detail-carrier')));
        }
        else {
          $form->populate($data);
        }
      }
      $form->setLegend('Přidání doručovací metody');
      $form->setProducer($this->_request->getParam('id'));
      $this->view->form = $form;
      $this->renderScript('producers/edit-carrier-method.phtml');
    }
    public function editCarrierMethodAction() {
      $tDPH = new Table_ApplicationDph();
      $dph = $tDPH->getActualDph();
      $form = new Orders_CarrierMethodForm();
      if ($this->_request->isPost()) {
        $data = $this->_request->getParams();
        if ($form->isValid($data)) {
          $carrier = new Producers_Carrier($form->getValue('id_producers'));
          $formData = array(
            'id' => $form->getValue('id'),
            'id_producers' => $form->getValue('id_producers'),
            'name' => $form->getValue('name'),
            'default_price' => $form->getValue('default_price') ? (float) $form->getValue('default_price') : NULL,
            'is_active' => $form->getValue('is_active'),
            'is_pos' => $form->getValue('is_pos'),
          );
          $carrier->updateDelivery($formData);
          $this->_flashMessenger->addMessage('Metoda byla upravena.');
          $this->redirect($this->view->url(array('action' => 'detail-carrier', 'id' => $formData['id_producers'], 'pid' => NULL)));
        }
        else {
          $form->populate($data, $dph);
        }
      }
      else {
        $methodId = $this->_request->getParam('id');
        $tOrdersCarriers = new Table_OrdersCarriers();
        $data = $tOrdersCarriers->find($methodId)->current()->toArray();
        $form->populate($data, $dph);
      }
      $form->setLegend('Editace doručovací metody');
      $this->view->form = $form;
      $this->view->vat = $dph;
    }

    public function removePersonAction() {
      $this->_helper->layout->disableLayout();
      $person_id = $this->_request->getParam('id');
      $row = FALSE;
      if ($person_id) {
        $table = new Table_ProducersPersons();
        $row = $table->fetchRow('id = ' . $person_id);
        if ($table->delete('id = ' . $table->getAdapter()->quote($person_id))) {
          $this->_flashMessenger->addMessage('Kontakt byl odstraněn.');
        }
        else {
          $this->_flashMessenger->addMessage('Kontakt nebyl odstraněn.');
        }
      }
      else {
        $this->_flashMessenger->addMessage('Nebylo zadáno ID kontaktu.');
      }
      if ($row) {
        $this->_redirect('/tobacco-producers/detail-carrier/id/' . $row['id_producers']);
      } else {
        $this->_redirect('/tobacco-producers/carriers');
      }
    }

    public function ajaxGetCarrierMethodPriceAction() {
      $this->_helper->layout->disableLayout();
      $price = NULL;
      $mid = $this->_request->getParam('mid');
      if ($mid) {
        $tCarriers = new Table_OrdersCarriers();
        $row = $tCarriers->find($mid)->current()->toArray();
        $this->_helper->json(array('status' => TRUE, 'result' => $row));
      }
      else {
        $this->_helper->json(array('status' => FALSE));
      }

    }


    public function indexAction(){
      $this->view->filter = new Producers_SearchForm;
      $this->view->filter->isValid($this->getRequest()->getParams());
      $this->view->title = "Dodavatelé";

      $table = new Table_Producers();
      $conditions = array();
      if ($this->_request->getParam('Hledat')) {
        if ($this->getRequest()->getParam('name', FALSE)) {
          $conditions[] = array('p.name LIKE ?' => '%' . $this->_request->getParam('name') . '%');
        }
        if ($this->getRequest()->getParam('category', FALSE)) {
          $conditions[] = array('p.id_producers_ctg = ?' => $this->_request->getParam('category'));
        }
      }
      $this->view->data = $table->getProducers(Table_TobaccoWarehouse::WH_TYPE, FALSE, $conditions);
    }

    public function addAction() {
      $this->view->title = 'Vložení dodavatele';
      $this->view->form = new Tobacco_Producers_Form();
      $this->view->form->setLegend($this->view->title);
      $this->view->form->getElement('is_active')->setValue(TRUE);
      if ($this->_request->isPost()) {
        $this->saveProducer(NULL);
      }
    }

    public function activateAction() {

      if (! $producerId = $this->_request->getParam('id')) {
        $message = 'ID výrobce nebylo zadáno.';
        $this->_flashMessenger->addMessage($message);
        $this->redirect($this->view->url([
          'controller' => 'tobacco-producers',
          'action' => 'index',
          'status' => null,
        ]));
      }

      $status = (bool) $this->_request->getParam('status', 1);
      $producer = new Producers_Producer($producerId);
      $producer->setActive($status);
      $message = $producer->getName() . ($status ? ' byl aktivován' : ' byl deaktivován');
      $this->_flashMessenger->addMessage($message);

      if ($back = $this->_request->getParam('back')) {
        $backUrl = base64_decode($back);
      }
      else {
        $backUrl = $this->view->url([
          'controller' => 'tobacco-producers',
          'action' => 'detail',
          'id' => $producerId,
          'status' => null
        ]);
      }

      $this->redirect($backUrl);
    }

    public function editAction() {
      $this->view->title = 'Editace dodavatele';
      $this->view->form = new Tobacco_Producers_Form();
      $this->view->form->setLegend($this->view->title);
      if ($this->_request->isPost()) {
        $this->saveProducer($this->_request->getParam('id'));
      }
      else {
        $table = new Table_Producers();
        $data = $table->find($this->_request->getParam('id'))->current()->toArray();
        $this->view->form->populate($data);
      }
      $this->renderScript('producers/add.phtml');
    }

    private function saveProducer($id = NULL) {
      $data = $this->_request->getParams();
      if ($this->view->form->isValid($data)) {
        $table = new Table_Producers();
        $data['id_wh_type'] = Table_TobaccoWarehouse::WH_TYPE;
        unset($data['controller']);
        unset($data['action']);
        unset($data['module']);
        unset($data['save']);
        if ($id) {
          if ($table->update($data, 'id = ' . $id)) {
            $this->_flashMessenger->addMessage('Dodavatel upraven');
            $this->_redirect($this->view->url(array('action' => NULL, 'id' => NULL)));
          }
        }
        else {
          if ($table->add($data)) {
            $this->_flashMessenger->addMessage('Nový dodavatel vložen');
            $this->_redirect($this->view->url(array('action' => NULL)));
          }
        }
      }
      $this->view->form->populate($data);
    }

  }