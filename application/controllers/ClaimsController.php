<?php

class ClaimsController extends Meduse_Controller_Action
{

  public function indexAction()
  {
    $this->_helper->layout->setLayout('bootstrap-basic');
    $this->view->title = "Reklamace";
    $parts_claims = new Table_PartsClaims();
    $this->view->data = $parts_claims->getDatalist();
  }

  public function addAction() {
    $this->_helper->layout->setLayout('bootstrap-basic');
    $form = new Claims_Form();
    $this->view->title = "Reklamace";

    if ($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())) {

      $db = Zend_Registry::get('db');
      try {
        $db->beginTransaction();
        if ($form->meduse_claim_amount->getValue() < 1) {
          throw new Exception('0 je malo');
        }

        new Parts_Part($form->meduse_claim_id_parts->getValue()); // existuje id ?

        $parts_claims = new Table_PartsClaims();
        $parts_warehouse = new Table_PartsWarehouse();
        $dateFrom = new Meduse_Date($form->meduse_claim_date_from->getValue(), "d.MM.yyyy");
        if ($form->meduse_claim_date_to->getValue() != '') {
          $dateTo = new Meduse_Date($form->meduse_claim_date_to->getValue(), "d.MM.yyyy");
          $dateTo = $dateTo->toString("Y-m-d");
        } else {
          $dateTo = new Zend_Db_Expr("NULL");
        }
        $parts_warehouse->removePart($form->meduse_claim_id_parts->getValue(), $form->meduse_claim_amount->getValue());
        $parts_claims->add(
          $form->meduse_claim_id_parts->getValue(),
          $form->meduse_claim_amount->getValue(),
          $form->meduse_claim_name->getValue(),
          $form->meduse_claim_desc->getValue(),
          $dateFrom->toString("Y-m-d"),
          $dateTo
        );
        $db->commit();
        $this->_flashMessenger->addMessage("Reklamace uložena.");
        $this->redirect('claims');
      } catch (Parts_Exceptions_LowAmount $e) {
        $db->rollback();
        $this->view->notice[] = "Takové množství " . $form->meduse_claim_id_parts->getValue() . " není skladem.";
      } catch (Exception $e) {
        $db->rollback();
        $this->view->notice[] = "Reklamaci se nepodařilo vložit. Zkontrolujte údaje.";
      }
    }
    $this->view->form = $form;
  }

  public function addWrapAction()
  {
    $this->_helper->layout->setLayout('bootstrap-basic');
    $form = new Claims_Form();
    $form->setLegend('Vložení reklamace obalu');
    $form->meduse_claim_id_parts->setLabel('ID obalu');

    $wraps = new Table_Wraps();
    if ($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getParams())) {
      if ($wraps->find($form->meduse_claim_id_parts->getValue())->current()) {


        $db = Zend_Registry::get('db');
        try {
          $db->beginTransaction();
          if ($form->meduse_claim_amount->getValue() < 1) {
            throw new Exception('0 je malo');
          }
          $wraps_claims = new Table_WrapsClaims();
          $wraps_warehouse = new Table_WrapsWarehouse();
          $dateFrom = new Zend_Date($form->meduse_claim_date_from->getValue(), "d.MM.yyyy");
          if ($form->meduse_claim_date_to->getValue() != '') {
            $dateTo = new Meduse_Date($form->meduse_claim_date_to->getValue(), "d.MM.yyyy");
            $dateTo = $dateTo->toString("Y-m-d");
          } else {
            $dateTo = new Zend_Db_Expr("NULL");
          }
          $wraps_warehouse->removeWrap($form->meduse_claim_id_parts->getValue(), $form->meduse_claim_amount->getValue());
          $wraps_claims->add(
            $form->meduse_claim_id_parts->getValue(),
            $form->meduse_claim_amount->getValue(),
            $form->meduse_claim_name->getValue(),
            $form->meduse_claim_desc->getValue(),
            $dateFrom->toString("Y-m-d"),
            $dateTo
          );
          $db->commit();
          $this->_flashMessenger->addMessage("Reklamace uložena.");
          $this->redirect('claims');
        } catch (Parts_Exceptions_LowAmount $e) {
          $db->rollback();
          $this->view->notice[] = "Takové množství " . $form->meduse_claim_id_parts->getValue() . " není skladem.";
        } catch (Exception $e) {
          $db->rollback();
          $this->view->notice[] = "Reklamaci se nepodařilo vložit. Zkontrolujte údaje.";
        }


      } else {
        $this->view->notice[] = "Obal '" . $form->meduse_claim_id_parts->getValue() . "' nebyl nalezen v systému. Zkontrolujte jestli je správně zadáno ID.";
      }
    }

    $this->view->form = $form;
    return $this->render('add');
  }

  public function deleteAction()
  {
    if (!$this->_request->getParam('id')) {
      $this->redirect('claims');
    }
    $parts_claims = new Table_PartsClaims();
    $parts_warehouse = new Table_PartsWarehouse();

    $db = Zend_Registry::get('db');
    try {
      $db->beginTransaction();
      $select = $parts_claims->select()
        ->where("id = ?", $this->_request->getParam('id'));
      $row = $parts_claims->fetchRow($select);
      $parts_claims->remove($this->_request->getParam('id'));

      $part = new Parts_Part($row['id_parts']);
      if (!$part->isDeleted()) {
        $parts_warehouse->insertPart($row['id_parts'], $row['amount']);
        $msg = "Reklamace byla ostraněna.<br />Do skladu bylo vloženo "
          . $row['amount'] . " ks součástí " . $row['id_parts'];
      } else {
        $msg = "Reklamace byla ostraněna.<br />Stav skladu se nezměnil, součást "
          . $row['id_parts'] . " je v koši.";
      }

      $db->commit();
      $this->_flashMessenger->addMessage($msg);
    } catch (Exception $e) {
      $db->rollback();
      $this->_flashMessenger->addMessage("Reklamaci se nepodařilo smazat: " . $e->getMessage());
    }
    $this->redirect('claims');
    $this->_helper->viewRenderer->setNoRender();
  }

  public function deleteWrapAction()
  {
    if (!$this->_request->getParam('id')) $this->redirect('claims');
    $wraps_claims = new Table_WrapsClaims();
    $wraps_warehouse = new Table_WrapsWarehouse();

    $db = Zend_Registry::get('db');
    try {
      $db->beginTransaction();
      $select = $wraps_claims->select()->where("id = ?", $this->_request->getParam('id'));
      $row = $wraps_claims->fetchRow($select);
      $wraps_claims->remove($this->_request->getParam('id'));
      $wraps_warehouse->addPart($row['id_wraps'], $row['amount']);
      $db->commit();
      $this->_flashMessenger->addMessage("Reklamace obalu ostraněna.<br />Do skladu bylo vloženo " . $row['amount'] . " ks obalu " . $row['id_wraps']);
    } catch (Exception $e) {
      $db->rollback();
      $this->_flashMessenger->addMessage("Reklamaci se nepodařilo smazat." . $e->getMessage());
    }
    $this->redirect('claims');
    $this->_helper->viewRenderer->setNoRender();
  }

  public function editAction()
  {
    $this->_helper->layout->setLayout('bootstrap-basic');
    if (!$this->_request->getParam('id')) $this->redirect('claims');
    $form = new Claims_EditForm();
    $parts_claims = new Table_PartsClaims();
    $claim = $parts_claims->find($this->_request->getParam('id'))->current();

    if ($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getPost())) {
      $dateTo = new Meduse_Date($form->meduse_claim_date_to->getValue(), "d.MM.yyyy");
      $dateTo = $dateTo->toString("Y-m-d");
      $parts_claims->update(array(
        "name" => $this->_request->getParam('meduse_claim_name'),
        "description" => $this->_request->getParam('meduse_claim_desc'),
        "date_to" => $dateTo
      ), "id = " . $this->_request->getParam('id'));
      $this->_flashMessenger->addMessage("Reklamace upravena.");
      $this->redirect('claims');
    } else {
      $dateTo = new Meduse_Date($claim['date_to'], "yyyy-MM-dd");
      $form->meduse_claim_name->setValue($claim['name']);
      $form->meduse_claim_desc->setValue($claim['description']);
      $form->meduse_claim_date_to->setValue($dateTo->toString("d.m.Y"));
    }

    $this->view->form = $form;
    $this->view->id = $this->_request->getParam('id');
    return $this->render('add');
  }

  public function editWrapAction()
  {
    $this->_helper->layout->setLayout('bootstrap-basic');
    if (!$this->_request->getParam('id')) $this->redirect('claims');
    $form = new Claims_EditForm();
    $wraps_claims = new Table_WrapsClaims();
    $claim = $wraps_claims->find($this->_request->getParam('id'))->current();

    if ($this->_request->getParam('Odeslat') && $form->isValid($this->_request->getPost())) {
      $dateTo = new Meduse_Date($form->meduse_claim_date_to->getValue(), "d.MM.yyyy");
      $dateTo = $dateTo->toString("Y-m-d");
      $wraps_claims->update(array(
        "name" => $this->_request->getParam('meduse_claim_name'),
        "description" => $this->_request->getParam('meduse_claim_desc'),
        "date_to" => $dateTo
      ), "id = " . $this->_request->getParam('id'));
      $this->_flashMessenger->addMessage("Reklamace upravena.");
      $this->redirect('claims');
    } else {
      $dateTo = new Meduse_Date($claim['date_to'], "yyyy-MM-dd");
      $form->meduse_claim_name->setValue($claim['name']);
      $form->meduse_claim_desc->setValue($claim['description']);
      $form->meduse_claim_date_to->setValue($dateTo->toString("d.m.Y"));
    }

    $this->view->form = $form;
    $this->view->id = $this->_request->getParam('id');
    return $this->render('add');
  }
}
