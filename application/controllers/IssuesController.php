<?php

class IssuesController extends Meduse_Controller_Action {
	
	public function indexAction() {
		$this->_helper->layout->setLayout('bootstrap-basic');
		$tIssues = new Table_Issues();
		$this->view->issues = $tIssues->getAllIssues();
	}
	
	public function detailAction() {
		$id = $this->_request->getParam('id');
		if (is_null($id)) {
			$this->_redirect('/issues');
		}		
		$this->_helper->layout->setLayout('bootstrap-basic');
		$tIssues = new Table_Issues();
		$tComments = new Table_IssuesComments();
		
		$issue = $tIssues->getIssue($id);
		if (!$issue) {
			$this->_flashMessenger->addMessage('Problém #' . $id . ' nenalezen.');
			$this->_redirect('/issues');
		}
		$this->view->issue = $issue;
		$this->view->comments = $tComments->getComments($id);
		$form = new Issues_CommentForm();
		$form->setAction('/issues/save-comment');
		$form->getElement('id_issues')->setValue($id);
		$this->view->form = $form;
		
		$acl = Zend_Registry::get('acl');
		if ($acl->isAllowed('issues/change-status')) { 
			$formStatus = new Issues_StatusForm();
			$formStatus->setAction('/issues/change-status');
			$formStatus->getElement('id_issues')->setValue($id);
			$formStatus->getElement('status')->setValue($issue['status']);
			$this->view->formStatus = $formStatus;
		}
	}
	
	public function saveCommentAction() {
		$params = $this->_request->getParams();
		if ($params['id_users'] && $params['id_issues'] && $params['body']) {
			$tComments = new Table_IssuesComments();
			$cid = $tComments->insert(array(
				'id_users' => $params['id_users'],
				'id_issues' => $params['id_issues'],
				'body' => $params['body']
			));
			$this->_flashMessenger->addMessage('Komentář byl uložen.');
			
            // posledni = pridany komentar k odeslani mailem
			$comments = $tComments->getComments($params['id_issues']);
			$data = array_pop($comments);
			$tIssues = new Table_Issues();
			$tIssues->update(array('changed' => new Zend_Db_Expr('NOW()')), 'id = ' . $params['id_issues']);
			$issue = $tIssues->getIssue($params['id_issues']);
			$data['summary'] = $issue['summary'];
            
            // komu se ma mail odeslat
            $tUsers = new Table_Users();
            $reporter = $tUsers->getUser($issue['id_users'], FALSE);
            $commenter = $tUsers->getUser($data['id_users'], FALSE);
            $data['mailto'] = array(
                $reporter['email']? $reporter['email'] : EmailFactory::EMAIL_MANAGER,
                $commenter['email']? $commenter['email'] : EmailFactory::EMAIL_MANAGER 
            );
            //Zend_Debug::dump($data); die;
			// odeslani mailu
			$tEmails = new Table_Emails();
			$tEmails->sendEmail(Table_Emails::EMAIL_ISSUE_COMMENT, $data);			
		} else {
			$this->_flashMessenger->addMessage('Komentář nebyl uložen.');			
		}
		$this->_redirect('/issues/detail/id/' . $params['id_issues']);			
	}
	
	public function changeStatusAction() {
		$params = $this->_request->getParams();
		if ($params['id_issues'] && $params['status']) {
			$tIssues = new Table_Issues();
			$tIssues->update(array('status' => $params['status']), 'id = ' . $params['id_issues']);
			$this->_flashMessenger->addMessage('Změna stavu proběhla.');
			if ($params['status'] == Table_Issues::STATUS_CLOSED || $params['status'] == Table_Issues::STATUS_REJECTED) {
				$issue = $tIssues->getIssue($params['id_issues']);
				$issue['id_issues'] = $params['id_issues'];
                $tUsers = new Table_Users();
                $user = $tUsers->getUser($issue['id_users'], FALSE);
                $issue['mailto'] = $user['email']? $user['email'] : EmailFactory::EMAIL_MANAGER;
                //Zend_Debug::dump($issue); die;
				$tEmails = new Table_Emails();
				$tEmails->sendEmail(Table_Emails::EMAIL_ISSUE_CLOSE, $issue);
			}
		} else {
			$this->_flashMessenger->addMessage('Změna stavu neproběhla.');			
		}
		$this->_redirect('/issues/detail/id/' . $params['id_issues']);
	}
}


