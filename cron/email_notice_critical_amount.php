<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script email_notice_critical_amount.php at "
  . date('Y-m-d H:i:s') . "\n";


$tPartsWarehouse = new Table_PartsWarehouse();
$tMacerations = new Table_Macerations();


//sestavení emailů

///suroviny
$notice_materials = '';
foreach ($tPartsWarehouse->getCriticalMaterialsTobacco() as $critMatTobacco) {
  $notice_materials .= '<li>' . $critMatTobacco['name'] . ' (' . $critMatTobacco['id_parts'] . '): ' . $critMatTobacco['amount']/1000 . ' kg</li>';
}
foreach ($tPartsWarehouse->getCriticalMaterialsFlavors() as $critMatFlavors) {
  $notice_materials .= '<li>' . $critMatFlavors['name'] . ' (' . $critMatFlavors['id_parts'] . '): ' . $critMatFlavors['amount']/1000 . ' kg</li>';
}
foreach ($tPartsWarehouse->getCriticalMaterialsOther() as $critMatOther) {
  $notice_materials .= '<li>' . $critMatOther['name'] . ' (' . $critMatOther['id_parts'] . '): ' . $critMatOther['amount']/1000 . ' kg</li>';
}

///maceráty
$notice_macerations = '';
foreach ($tMacerations->getCritical() as $critMac) {
  $notice_macerations .= '<li>' . $critMac['name'] . ' (' . $critMac['batch'] . '): ' . $critMac['amount_current'] . ' kg</li>';
}

///produkty
$notice_products = '';
/*
foreach ($tPartsWarehouse->getCriticalTobaccoNFS() as $critProdNFS) {
  $notice_products .= '<li>' . $critProdNFS['name'] . ' (' . $critProdNFS['id_parts'] . '): ' . $critProdNFS['amount'] . ' ks</li>';
}
*/

///obaly - etikety
$notice_packaging_labels = '';
foreach ($tPartsWarehouse->getCriticalPackaging(Table_TobaccoWarehouse::CTG_PACKAGING_LABELS) as $critPack) {
  $notice_packaging_labels .= '<li>' . $critPack['name'] . ' (' . $critPack['id_parts'] . '): ' . $critPack['amount'] . ' ks</li>';
}

$notice_packaging_labels_box_010 = '';
foreach ($tPartsWarehouse->getCriticalPackaging(Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_BOX_010) as $critPack) {
  $notice_packaging_labels_box_010 .= '<li>' . $critPack['name'] . ' (' . $critPack['id_parts'] . '): ' . $critPack['amount'] . ' ks</li>';
}

$notice_packaging_labels_box_050 = '';
foreach ($tPartsWarehouse->getCriticalPackaging(Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_BOX_050) as $critPack) {
  $notice_packaging_labels_box_050 .= '<li>' . $critPack['name'] . ' (' . $critPack['id_parts'] . '): ' . $critPack['amount'] . ' ks</li>';
}

$notice_packaging_labels_sachet_010 = '';
foreach ($tPartsWarehouse->getCriticalPackaging(Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_SACHET_010) as $critPack) {
  $notice_packaging_labels_sachet_010 .= '<li>' . $critPack['name'] . ' (' . $critPack['id_parts'] . '): ' . $critPack['amount'] . ' ks</li>';
}


///obaly - sáčky
$notice_packaging_sachets = '';
foreach ($tPartsWarehouse->getCriticalPackaging(Table_TobaccoWarehouse::CTG_PACKAGING_SACHETS) as $critPack) {
  $notice_packaging_sachets .= '<li>' . $critPack['name'] . ' (' . $critPack['id_parts'] . '): ' . $critPack['amount'] . ' ks</li>';
}

///obaly - krabicky
$notice_packaging_boxes = '';
foreach ($tPartsWarehouse->getCriticalPackaging(Table_TobaccoWarehouse::CTG_PACKAGING_BOXES) as $critPack) {
  $notice_packaging_boxes .= '<li>' . $critPack['name'] . ' (' . $critPack['id_parts'] . '): ' . $critPack['amount'] . ' ks</li>';
}

///obaly - ost.
$notice_packaging_other = '';
foreach ($tPartsWarehouse->getCriticalPackaging(Table_TobaccoWarehouse::CTG_PACKAGING_OTHER) as $critPack) {
  $notice_packaging_other .= '<li>' . $critPack['name'] . ' (' . $critPack['id_parts'] . '): ' . $critPack['amount'] . ' ks</li>';
}

///kolky
$notice_stamps = '';
foreach ($tPartsWarehouse->getCriticalStamps() as $critStp) {
  $notice_stamps .= '<li>' . $critStp['name'] . ' (' . $critStp['id_parts'] . '): ' . $critStp['amount'] . ' ks</li>';
}

///doplňky
$notice_accessories = '';
foreach ($tPartsWarehouse->getCriticalAccessories() as $critAcs) {
  $notice_accessories .= '<li>' . $critAcs['name'] . ' (' . $critAcs['id_parts'] . '): ' . $critAcs['amount'] . ' ks</li>';
}

$notice = (empty($notice_materials) ? '' :
          '<p><i>Suroviny:</i><ul>' . $notice_materials . '</ul></p>') .
          (empty($notice_macerations) ? '' :
          '<p><i>Maceráty:</i><ul>' . $notice_macerations . '</ul></p>') .
          (empty($notice_products) ? '' :
          '<p><i>Produkty:</i><ul>' . $notice_products . '</ul></p>') .

          (empty($notice_packaging_labels) ? '' :
          '<p><i>' . Table_TobaccoWarehouse::$ctgToString[Table_TobaccoWarehouse::CTG_PACKAGING_LABELS]
            . ':</i><ul>' . $notice_packaging_labels . '</ul></p>') .
          (empty($notice_packaging_labels_box_010) ? '' :
          '<p><i>' . Table_TobaccoWarehouse::$ctgToString[Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_BOX_010]
            . ':</i><ul>' . $notice_packaging_labels_box_010 . '</ul></p>') .
          (empty($notice_packaging_labels_box_050) ? '' :
          '<p><i>' . Table_TobaccoWarehouse::$ctgToString[Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_BOX_050]
            . ':</i><ul>' . $notice_packaging_labels_box_050 . '</ul></p>') .
          (empty($notice_packaging_labels_sachet_010) ? '' :
            '<p><i>' . Table_TobaccoWarehouse::$ctgToString[Table_TobaccoWarehouse::CTG_PACKAGING_LABELS_SACHET_010]
            . ':</i><ul>' . $notice_packaging_labels_sachet_010 . '</ul></p>') .

          (empty($notice_packaging_sachets) ? '' :
          '<p><i>Sáčky:</i><ul>' . $notice_packaging_sachets . '</ul></p>') .
          (empty($notice_packaging_boxes) ? '' :
          '<p><i>Krabičky:</i><ul>' . $notice_packaging_boxes . '</ul></p>') .
          (empty($notice_packaging_other) ? '' :
          '<p><i>Ostatní obal. materiál:</i><ul>' . $notice_packaging_other . '</ul></p>') .
          (empty($notice_stamps) ? '' :
          '<p><i>Kolky:</i><ul>' . $notice_stamps . '</ul></p>') .
          (empty($notice_accessories) ? '' :
          '<p><i>Doplňky:</i><ul>' . $notice_accessories . '</ul></p>');

if (empty($notice)) {
  $notice = '<p>Žádná z položek nedosáhla krit. množství.</p>';
}
else {
  $notice = '<p>Report kritického množství ze dne <strong>' . date('d. m. Y') . '</strong>:</p>' . $notice;
}

// sestaveni mailu a odeslani
$email = new Table_Emails();
$email->sendEmail(Table_Emails::EMAIL_NOTICE_CRITICAL_AMOUNT, array(
      'body' => $notice
));

echo "Finished script email_notice_critical_amount.php at "
  . date('Y-m-d H:i:s') . "\n";