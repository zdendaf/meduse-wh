<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script recalculate_medite.php at "
  . date('Y-m-d H:i:s') . "\n";

echo "Recalulating of ";
$tParts = new Table_Parts();
$select = $tParts->getProductsByCategory(Table_TobaccoWarehouse::CTG_TOBACCO_NFS , Tobacco_Parts_Part::WH_TYPE);
$rows = $select->query()->fetchAll();
$it = 0;
$time = 0;
$start_all = time();
$count = count($rows);
echo $count . " Medite products has been started.\n\n";
if ($rows) {
  foreach ($rows as $row) {
    $start = time();
    echo "Product " . $row['id']  . " ... ";
    $part = new Tobacco_Parts_Part($row['id']);
    $part->refreshPrice();
    $it++;
    $stop = time();
    $time += $stop - $start;
    echo "OK in " . ($stop - $start) . " seconds, estimated time left is " . round((($time / $it * $count) - ($stop - $start_all))/ 60) . " minutes \n";
  }
}
$stop = time();
echo "Recalulating of " . $it . " Medite products has been finished in " . round(($stop - $start_all) / 60) . " minutes.\n\n";

echo "Finished script recalculate_medite.php at "
  . date('Y-m-d H:i:s') . "\n";
