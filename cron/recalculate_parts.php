<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

$handle = NULL;
$arguments = getopt('i');
if (isset($arguments['i'])) {
  $handle = fopen('php://stdin', 'r');
}
else {
  echo 'Start processing script recalculate_parts.php at ' . date('Y-m-d H:i:s') . PHP_EOL;
}

$table = new Table_Parts();
echo 'Gathering costs information about all parts... ';

try {
  $parts = $table->checkCosts();
  echo 'Done.' . PHP_EOL;
}
catch (Exception $e) {
  Utils::logger()->err($e->getMessage());
  echo 'Error. Check the system log.';
  $parts = NULL;
}

if ($parts) {
  $total = count($parts);
  echo 'There are ' . $total . ' parts with wrong precomputed costs.' . PHP_EOL;

  if ($handle) {
    $cmd = NULL;
    while (!in_array($cmd, ['y', 'n'], TRUE)) {
      echo 'Do you want to continue? [y/n]: ';
      $cmd = trim(fgets($handle));
      if ($cmd === 'n') {
        fclose($handle);
        die('Canceled.');
      }
    }
  }

  $count = 0;
  $it = 0;
  foreach ($parts as $partId => $item) {
    $it++;
    if ($handle) {
      $cmd = NULL;
      while (!in_array($cmd, ['y', 'n'], TRUE)) {
        echo PHP_EOL . 'Part ' . $partId . ' (' . $item['name'] . ') has wrong costs: ' . PHP_EOL;
        echo '  price = ' . Meduse_Currency::format($item['price'], 'CZK', 2, FALSE);
        echo '  inputs = ' . Meduse_Currency::format($item['inputs'], 'CZK', 2, FALSE);
        echo '  operations = ' . Meduse_Currency::format($item['operations'], 'CZK', 2, FALSE);
        echo '  costs = ' . Meduse_Currency::format($item['costs'], 'CZK', 2, FALSE) . PHP_EOL;
        echo '  Do you want to recalculate it? [y/n]: ';
        $cmd = trim(fgets($handle));
        if ($cmd === 'n') {
          echo 'Skipping.' . PHP_EOL;
          continue 2;
        }
      }
    }
    $count++;
    echo 'Recalculating part ' . $partId . ' (' . $it . '/' . $total . ')... ';
    $part = new Pipes_Parts_Part($partId);
    $part->recalculate();
    echo 'Done: ';
    echo Meduse_Currency::format($item['costs'], 'CZK', 2, FALSE) . ' >> ' . Meduse_Currency::format($part->getPrice(), 'CZK', 2, FALSE) . PHP_EOL;
  }
  echo 'All parts with wrong costs were recalculated (' . $count . ').' . PHP_EOL;
}
else {
  echo 'There is no part with wrong costs.' . PHP_EOL;
}
if ($handle) {
  fclose($handle);
}
else {
  echo 'Finished script recalculate_parts.php at ' . date('Y-m-d H:i:s') . PHP_EOL;
}
