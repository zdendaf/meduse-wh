<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script parts_batch_repair.php at "
  . date('Y-m-d H:i:s') . "\n";

$tpb = new Table_PartsBatch();

$select = $tpb->select()->setIntegrityCheck(FALSE)
  ->from(array('p' => 'parts_batch'), array('id_parts', 'batch'))
  ->joinLeft(array('w' => 'parts_warehouse'), 'p.id_parts = w.id_parts', array('wh_amount' => 'amount'))
  ->group(array('batch', 'id_parts'));
$result = $select->query()->fetchAll();

if ($result) {
  $count = 0;
  echo 'Transaction starting.' . "\n";
  $tpb->getAdapter()->beginTransaction();
  foreach ($result as $row) {
    echo 'part ' . $row['id_parts'] . ' batch ' . $row['batch'] . ': ';
    echo 'updating... ';
    $tpb->delete(array(
      'id_parts = ' . $tpb->getAdapter()->quote($row['id_parts']),
      'batch = ' . $tpb->getAdapter()->quote($row['batch']),
    ));
    echo $row['wh_amount'] . ' piece(s)' . "\n";
    if ($row['wh_amount'] > 0) {
      $tpb->insert(array(
        'id_parts' => $row['id_parts'],
        'batch' => $row['batch'],
        'amount' => $row['wh_amount'],
      ));
    }
    $count++;
  }
  $tpb->getAdapter()->commit();
  echo 'Transaction was commited. ' . $count . ' item(s) was repaired.' . "\n";
}

echo "Finished script parts_batch_repair.php at "
  . date('Y-m-d H:i:s') . "\n";