<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script fetch_dsi_records.php at "
  . date('Y-m-d H:i:s') . "\n";

  $terminals = array(
    new Alveno_Dsi('meduse'),
    new Alveno_Psi('medite'),
    new Alveno_Psi('meduse_vyroba'),
  );

  foreach ($terminals as $term) {

    $term->getNewRecords();
    $tWorks = new Table_EmployeesRecords();
    $records = $term->fetchAllRecords();
    if (!$term->distStates()) {
      $tWorks->preprocessRecords($records);
    }
    if ($records) {
      foreach ($records as $record) {
        $tWorks->insert($record);
      }
    }

    $affected = $term->getAffected();
    if ($affected) {
      foreach ($affected as $employeeId => $dates) {
        foreach ($dates as $date) {
          new Employees_Reward(new Employees_Employee($employeeId), new Meduse_Date($date . '-01'));
        }
      }
    }

    $messages = $term->fetchAllMessages();
    if ($messages) {
      foreach ($messages as $message) {
        echo $message . "\n";
      }
    }
  }


echo "Finished script fetch_dsi_records.php at "
  . date('Y-m-d H:i:s') . "\n";
