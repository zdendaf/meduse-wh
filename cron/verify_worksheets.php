<?php
/**
 * Load bootstrap and database
 */

exit; // pozadavek na neposilani upozorneni na spatne vyplneny worksheet

include_once 'backend_bootstrap.php';

echo "Start processing script verify_worksheets.php at "
  . date('Y-m-d H:i:s') . "\n";

Zend_Layout::startMvc(array('layoutPath'=>'../application/layouts'));

$view = new Zend_View();
$view->setScriptPath(APP_PATH . './views/scripts');

$tDept = new Table_Departments();
$tUsers = new Table_Users();

$tEmployees = new Table_Employees();
$employees = $tEmployees->fetchAll();
$date = new Meduse_Date();
$date->subWeek(1);

$emails = array();

foreach ($employees as $employee) {
  $objEmployee = new Employees_Employee($employee->id);
  if (!$objEmployee->isActive()) {
    continue;
  }
  $status = $objEmployee->verifyWorkSheet($date, 7);
  echo $date->toString('d/m/Y') . ' + 7 ' . $objEmployee->getLastName() . ': ' . $status . "\n";

  if (!$status) {
    $userId = $objEmployee->getUserId();
    if ($userId) {
      $user = $tUsers->getUser($userId, FALSE);
      $toLeader = FALSE;
    }
    else {
      $deptRow = $tDept->find($objEmployee->getDepartmentId())->current();
      $user = $tUsers->getUser($deptRow->id_users, FALSE);
      $toLeader = TRUE;
    }

    if (!isset($emails[$user['email']])) {
      $emails[$user['email']] = array(
        'user' => array(),
        'leader' => array(),
      );
    }

    if ($toLeader) {
      $emails[$user['email']]['leader'][] = array(
        'full_name' => $objEmployee->getFullName(),
        'linkurl' => 'https://warehouse.medusepipes.com/employees/worksheet/id/' . $objEmployee->getId() . '/date/' . $date->toString('Y-m'),
      );
    }
    else {
      $emails[$user['email']]['user'] = array(
        'full_name' => $objEmployee->getFullName(),
        'linkurl' => 'https://warehouse.medusepipes.com/employees/worksheet/id/' . $objEmployee->getId() . '/date/' . $date->toString('Y-m'),
      );
    }
  }
}

$tEmail = new Table_Emails();
$view->date = $date->toString('d.m.Y');
foreach ($emails as $email => $data) {
  if (count($data['leader'])) {
    $view->workers = $data['leader'];
    $body = $view->render('employees/mail-verify-worksheet-leader.phtml');
    $tEmail->sendEmail(Table_Emails::EMAIL_VERIFY_WORKSHEET_NOTICE, array(
      'body' => $body, 'email' => $email));
  }
  if ($data['user']) {
    $view->full_name = $data['user']['full_name'];
    $view->linkurl = $data['user']['linkurl'];
    $body = $view->render('employees/mail-verify-worksheet-employee.phtml');
    $tEmail->sendEmail(Table_Emails::EMAIL_VERIFY_WORKSHEET_NOTICE, array(
      'body' => $body, 'email' => $email));
  }
}

echo "Finished script verify_worksheets.php at "
  . date('Y-m-d H:i:s') . "\n";