<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';
Zend_Layout::startMvc(array('layoutPath'=>'../application/layouts'));

echo "Start processing script works_convert at "
  . date('Y-m-d H:i:s') . "\n";

define('WORKS_COVERT_MONTHS', 6);

$tEmployees = new Table_Employees();
$employees = $tEmployees->fetchAll();
$date = new Meduse_Date();
for ($i = 0; $i < WORKS_COVERT_MONTHS; $i++) {
  $date->subMonth(1);
  echo "Processing " . $date->toString('Y-m') . ":\n";
  foreach ($employees as $employee) {
    $objEmployee = new Employees_Employee($employee->id);
    echo "\t- worker " . $objEmployee->getLastName() . " (ID " . $employee->id . ")\n";
    $work = new Employees_WorkOld($objEmployee, $date);
    $sheet = $work->getSheetOld();
    $sheet->saveToDB();
  }
}

echo "Finished script works_convert.php at "
  . date('Y-m-d H:i:s') . "\n";