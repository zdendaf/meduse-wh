<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script email_notice_missing_parts.php at "
  . date('Y-m-d H:i:s') . "\n";

$view = new Zend_View();
$view->setScriptPath(APP_PATH . './views/scripts');

// IDs kolekci dymek
$tCollections = new Table_Collections();
$colls = $tCollections->getAllCollectionsIds();

// potvrzene a otevrene objednavky
$tOrders = new Table_Orders();
$select = $tOrders->select()->where('status = :confirmed OR status = :open');
$orders = $tOrders->getAdapter()->fetchAll($select, array(
  'confirmed' => Table_Orders::STATUS_CONFIRMED,
  'open' => Table_orders::STATUS_OPEN
));

// soucasti pro kazdou objednavku
$dataOrders = array();
foreach ($orders as $order) {
  $report = new Orders_PartsReport();
  $report->setOrder(Orders_PartsReport::ORDER_PARTS);
  $report->setFilter(array(
    $order['id'] => 'on',
    'coll' => $colls,
    'ctg' => 'null'
  ));
  $report->setShowRows($report::SHOW_MISSING_ONLY);
  $dataOrders[$order['id']]['order'] = $order;
  $dataOrders[$order['id']]['report'] = $report->getData(TRUE);
  $aOrders[$order['id']] = 'on';
}
$view->dataOrders = $dataOrders;

// celkem - soucasti pro vsechny objednavky
$aOrders['coll'] = $colls;
$aOrders['ctg'] = 'null';
$report = new Orders_PartsReport();
$report->setOrder(Orders_PartsReport::ORDER_PARTS);
$report->setFilter($aOrders);
$report->setShowRows($report::SHOW_MISSING_ONLY);
$view->dataAll = $report->getData(TRUE);

// sestaveni mailu a odeslani
$email_body = $view->render('orders/mail-report.phtml');
$email = new Table_Emails();
$email->sendEmail(Table_Emails::EMAIL_MISSING_PARTS_DAILY_SUMMARY, array(
  'body' => $email_body
));

echo "Finished script email_notice_missing_parts.php at "
  . date('Y-m-d H:i:s') . "\n";