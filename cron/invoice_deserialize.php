<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script invoice_deserialize.php at " 
  . date('Y-m-d H:i:s') . "\n";

/* 
 * Budeme postupne prochazet tabuli INVOICE. 
 * 
 * Pokud je serializace:
 * - deserializujeme texty a ceny
 * - ulozime ceny a texty do INVOICE_ITEMS.
 * 
 * Pokud neni serializace:
 * - najdeme cenik k objednavce
 * - projdeme objednavky a ke kazde polozce nalezneme popis a cenikovou cenu
 * - cenu a text ulozime do tabulky INVOICE_ITEMS.
 */

$tItems = new Table_InvoiceItems();
$tItems->getAdapter()->query('TRUNCATE invoice_items');
$tOrderProducts = new Table_OrdersProducts();
$tInvoices = new Table_Invoices();
$tPrices = new Table_Prices(); 
$invoices = $tInvoices->fetchAll();


foreach ($invoices as $invoice) {
  $rows = array();

  if ($invoice->item_lines) {
    $texts = unserialize($invoice->item_lines);
    foreach ($texts as $id => $text) {
      $rows[$id]['text'] = $text[0];
      $rows[$id]['text_free'] = isset($text[1]) ? $text[1] : $text[0];
    }
  } else {
    $products = $tOrderProducts->select()
      ->setIntegrityCheck(false)
      ->from(array('op' => 'orders_products'), array('id_products'))
      ->joinLeft(array('p' => 'parts'), 'p.id = op.id_products', array('name'))
      ->where('id_orders = ?', $invoice->id_orders)
      ->query()
      ->fetchAll();
    if ($products) {
      foreach ($products as $product) {
        $rows[$product['id_products']]['text'] = $product['name'];
        $rows[$product['id_products']]['text_free'] = $product['name'];
      }
    }
  }
  if ($invoice->item_prices) {
    $prices = unserialize($invoice->item_prices);
    foreach ($prices as $id => $price) {
      $rows[$id]['price'] = (float) $price[0];
      $rows[$id]['price_free'] = isset($price[1]) ? (float) $price[1] : (float) $price[0];
    }
  } else {
    $products = $tOrderProducts->fetchAll('id_orders = ' . $invoice->id_orders); 
    foreach($products as $product) {
        if ($invoice->id_pricelists) {
          $rPrice = $tPrices->fetchRow(
            'id_pricelists = ' . $invoice->id_pricelists
            . ' AND id_parts = "' . $product->id_products . '"');
          $price = $rPrice['price'];
        } else {
          $price = 0;
        }
        $rows[$product->id_products]['price'] = (float) $price;
        $rows[$product->id_products]['price_free'] = (float) $price;
    }
  }
  if ($rows) {
    echo "Invoice {$invoice->id} ... ";
    $i = 0;
    foreach($rows as $id => $row) {
      $row = $tItems->createRow(array(
        'id_invoice' => $invoice->id,
        'id_orders' => $invoice->id_orders,
        'id_parts' => $id,
        'text' => $row['text'],
        'text_free' => $row['text_free'],
        'price' => $row['price'],
        'price_free' => $row['price_free'],
      ));
      $row->save();
      $i++;
    }
    echo "{$i} record(s) \n";
  }
}
     
echo "Finished script invoice_deserialize.php at " 
  . date('Y-m-d H:i:s') . "\n";