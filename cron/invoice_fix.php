<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script invoice_fix_vat_summary.php at "
  . date('Y-m-d H:i:s') . "\n";

$table = new Table_Invoices();
$rows = $table->select()->where('invoiced_total > 0')->query(Zend_Db::FETCH_OBJ)->fetchAll();
if ($rows) {
  foreach($rows as $row) {
    echo('Fixing invoice ID = ' . $row->id . ' ... ');
    $invoice = new Invoice_Invoice($row->id);
    $invoice->setShowVatSummary($row->show_vat_summary == 'y');
    $invoice->recalculateInvoicedPrice();
    echo("Done. Show VAT = " . ($invoice->showVatSummary() ? 'TRUE' : 'FALSE')
      . ' Invoiced price: ' . $invoice->getInvoicedPrice(FALSE) . "\n");
  }
}

echo "Finished script invoice_fix_vat_summary.php at "
  . date('Y-m-d H:i:s') . "\n";
