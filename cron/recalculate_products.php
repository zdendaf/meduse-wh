<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script recalculate_products.php at "
  . date('Y-m-d H:i:s') . "\n";

echo "Recalulating of ";
$tParts = new Table_Parts();
$products = $tParts->getProducts(Pipes_Parts_Part::WH_TYPE);
$visited = array();
$it = 0;
$time = 0;
$start_all = time();
$count = count($products);
echo $count . " Meduse products has been started.\n\n";
if ($products) {
  foreach ($products as $p) {
    $start = time();
    echo "Product " . $p['id']  . " ... ";
    if (!in_array($p['id'], $visited)) {
      $part = new Pipes_Parts_Part($p['id']);
      $part->recalculate($visited);
    }
    $it++;
    $stop = time();
    $time += $stop - $start;
    echo "OK in " . ($stop - $start) . " seconds, estimated time left is " . round((($time / $it * $count) - ($stop - $start_all))/ 60) . " minutes \n";
  }
}
echo "Recalulating of " . $it . " Meduse products has been finished in " . round(($stop - $start_all) / 60) . " minutes.\n\n";

echo "Finished script recalculate_products.php at "
  . date('Y-m-d H:i:s') . "\n";
