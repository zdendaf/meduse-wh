<?php

  /**
   * Load bootstrap and database
   */
  include_once 'backend_bootstrap.php';

  echo "Start processing script email_notice_pre_orders.php at "
  . date('Y-m-d H:i:s') . "\n";

  /**
   * Get all pre-orders
   */
  $tOrders = new Table_Orders();
  $orders = $tOrders->fetchAll("status = 'new' AND id_wh_type='1'");
  $orders_data = $orders->toArray();

  $data14 = array();
  $data21 = array();

  foreach ($orders_data as $ord) {
    // get diff in days
    $today = time(); // or your date as well
    $date_req = strtotime($ord['date_request']);
    $datediff = $today - $date_req;
    $days_between = floor($datediff / (60 * 60 * 24));

    if ($days_between >= 21) {
      $data21[] = $ord;
    }
    if ($days_between >= 14 && $days_between < 21) {
      $data14[] = $ord;
    }
  }

  $view = new Zend_View();
  $view->setScriptPath(APP_PATH . './views/scripts');
  $view->data14 = $data14;
  $view->data21 = $data21;
  $body = $view->render('orders/mail-pre-order.phtml');
 
  // table emails
  $tEmail = new Table_Emails();
  $tEmail->sendEmail(Table_Emails::EMAIL_PRE_ORDER_FULL_NOTICE, array('body' => $body));

  echo "Finished script email_notice_pre_orders.php at "
  . date('Y-m-d H:i:s') . "\n";