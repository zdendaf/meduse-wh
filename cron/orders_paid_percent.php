<?php
include_once 'backend_bootstrap.php';
echo "Script started at " . date('Y-m-d H:i:s') . "\n";

/** @var Zend_Db_Adapter_Abstract $db */
try {
  $db = Zend_Registry::get('db');
} catch (Zend_Exception $e) {
  die($e->getMessage());
}

$items = $db->select()
  ->from('orders', ['id', 'no', 'title'])
  ->where('status in (?)', [
    Table_Orders::STATUS_NEW,
    Table_Orders::STATUS_CONFIRMED,
    Table_Orders::STATUS_OPEN,
    Table_Orders::STATUS_CLOSED,
  ])
  ->where('paid_percentage IS NULL')
  ->query(Zend_Db::FETCH_ASSOC)
  ->fetchAll();
$count = count($items);
echo "There are $count orders with empty paid percentage value. " . PHP_EOL;
if ($items) {
  echo "Recalculating paid percentage for order: ";
  foreach ($items as $item) {
    echo PHP_EOL . '- ' . $item['no'] . ': ' . $item['title'] . '... ';
    try {
      $order = new Orders_Order($item['id']);
      $percent = $order->getPaidPercentage();
      echo round($percent * 100) . '% [ok]';
    }
    catch (Exception|Orders_Exceptions_ForbiddenAccess $e) {
      echo $e->getMessage() . ' [failed]';
    }
  }
}

echo PHP_EOL . "Script finished at " . date('Y-m-d H:i:s') . "\n";
