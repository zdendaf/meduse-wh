<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script parts_export.php at "
  . date('Y-m-d H:i:s') . "\n";

echo "Generating Meduse export of active parts... ";
$objPHPExcel = @Parts::getPartsXLSX(TRUE, FALSE, FALSE, Table_PartsWarehouse::WH_TYPE);
$filename1 = 'export_skladu_soucasti-' . date('ymd') . '.xlsx';
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->setOffice2003Compatibility(TRUE);
$objWriter->save(Zend_Registry::get('config')->data->path . '/export/' . $filename1);
echo "Done. File: " . $filename1 . "\n";

echo "Generating Meduse export of thrown parts... ";
$objPHPExcel = @Parts::getPartsXLSX(TRUE, FALSE, TRUE, Table_PartsWarehouse::WH_TYPE);
$filename3 = "Export skladu vyrazenych soucasti - " . date('Ymd Hi') . '.xlsx';
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->setOffice2003Compatibility(TRUE);
$objWriter->save(Zend_Registry::get('config')->data->path . '/export/' . $filename3);
echo "Done. File: " . $filename3 . "\n";

echo "Sending info email... ";
$email = new Table_Emails();
$email->sendEmail(Table_Emails::EMAIL_NOTICE_PARTS_EXPORT, array(
  'filenames' => array($filename1, $filename3)
));
echo "Done.\n";

echo "Generating Medite stock export... ";
$objPHPExcel = @Reports::generateCompleteTobaccoExport();
$filename1 = "export_medite_".date('d-m-Y') . ".xlsx";
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->setOffice2003Compatibility(TRUE);
$objWriter->save(Zend_Registry::get('config')->data->path . '/export-medite/' . $filename1);
echo "Done. File: " . $filename1 . "\n";

echo "Sending info email... ";
$email = new Table_Emails();
$email->sendEmail(Table_Emails::EMAIL_NOTICE_PARTS_EXPORT_MEDITE, array(
  'filenames' => array($filename1)
));
echo "Done.\n";

echo "Finished script parts_export.php at "
  . date('Y-m-d H:i:s') . "\n";
