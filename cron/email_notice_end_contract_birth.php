<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script email_notice_end_emp_contract.php at "
  . date('Y-m-d H:i:s') . "\n";

Zend_Layout::startMvc(array('layoutPath'=>'../application/layouts'));

$view = new Zend_View();
$view->setScriptPath(APP_PATH . './views/scripts');

$tEmployees = new Table_Employees();
$employees = $tEmployees->fetchAll('active = "y"');

$date = new Meduse_Date();
$date->addDay(30);

$bDate = new Meduse_Date();
$bDate->addDay(7);

$tEmail = new Table_Emails();

$objEmployee = NULL;
$endDate = NULL;

$usersTable = new Table_Users();
$deptTable = new Table_Departments();

foreach ($employees as $employee) {

  $objEmployee = new Employees_Employee($employee->id);
  $end = $objEmployee->getEnd();
  if ($end) {
    $endDate = new Meduse_Date($end);
    if ($endDate->toString('Ymd') == $date->toString('Ymd')) {
      echo $objEmployee->getFullName() . ' - konec smlouvy: ' . $endDate->toString('d.m.Y') . "\n";
      $view->date = $endDate->toString('d.m.Y');
      $view->full_name = $objEmployee->getFullName();
      $view->worker_type_str = $objEmployee->isSupplier() ? 'živnostníka' : 'zaměstnance';
      $body = $view->render('employees/mail-end-contract.phtml');
      $tEmail->sendEmail(Table_Emails::EMAIL_END_EMP_CONTRACT_NOTICE, array('body' => $body, 'full_name' => $view->full_name));
    }
  }

  $bday = $objEmployee->getBirthday(FALSE);
  if ($bday) {
    if ($bday->toString('md') == $bDate->toString('md')) {
      $headMail = NULL;
      echo $objEmployee->getFullName() . ' - narozeniny: ' . $bday->toString('d.m.Y') . "\n";
      if ($deptId = $objEmployee->getDepartmentId()) {
        $deptRow = $deptTable->find($deptId)->current();
        $headId = $deptRow->id_users;
        $headRow = $usersTable->find($headId)->current();
        $headMail = $headRow->email;
      }
      $view->date = $bDate->toString('d.m.Y');
      $view->age = $bDate->get('Y') - $bday->get('Y');
      $view->full_name = $objEmployee->getFullName();
      $body = $view->render('employees/mail-birthday.phtml');
      $tEmail->sendEmail(Table_Emails::EMAIL_BIRTHDAY_NOTICE, array(
        'body' => $body,
        'full_name' => $view->full_name,
        'head_mail' => $headMail,
        ));
    }
  }
}

echo "Finished script email_notice_end_emp_contract.php at "
  . date('Y-m-d H:i:s') . "\n";