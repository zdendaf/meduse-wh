<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script email_notice_production_delayed.php at " 
  . date('Y-m-d H:i:s') . "\n";

$tProductions = new Table_Productions();
$delayed = $tProductions->getDelayedProduction();
foreach ($delayed as $production) {
  if (!$production['date_notify']) {
    $tProductions->notifyDelayedProduction($production['id']);
  }
}
     
echo "Finished script email_notice_production_delayed.php at " 
  . date('Y-m-d H:i:s') . "\n";