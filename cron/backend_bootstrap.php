<?php
/**
 * Backend bootstrap
 * for cron
 */

define('APP_PATH', '/mnt/data/accounts/m/medusepipes/data/www/warehouse/application/');

error_reporting(E_ALL ^ E_DEPRECATED);
ini_set('display_errors', 1);
date_default_timezone_set('Europe/Prague');

set_include_path(
  APP_PATH . '.' . PATH_SEPARATOR .
  APP_PATH . '../library/' . PATH_SEPARATOR .
  APP_PATH . '../library/codeplex' . PATH_SEPARATOR .
  APP_PATH . '../application/models' . PATH_SEPARATOR .
  get_include_path());
include "Zend/Loader.php";
Zend_Loader::registerAutoload();
Zend_Session::start();


$config = new Zend_Config_Ini(APP_PATH . '../application/config.ini', 'folders');
$registry = Zend_Registry::getInstance();
$registry->set('folders', $config);
$config = new Zend_Config_Ini(APP_PATH . '../application/config.ini', 'general');
$registry->set('config', $config);


$db = Zend_Db::factory($config->db);
$registry->set('db', $db);
Zend_Db_Table::setDefaultAdapter($db);

Zend_Registry::set('version', $config->version);

// Logovani do aplikačního logu.
try {
  $logFilename = $config->dirs->logs . '/application.log';
  if (!$stream = @fopen($logFilename, 'a', FALSE)) {
    throw new Exception('Nelze zapisovat do aplikačního logu "' . $logFilename . '".');
  }
  $logger = new Zend_Log(new Zend_Log_Writer_Stream($stream));
  Zend_Registry::set('logger', $logger);
}
catch (Exception $e) {
  die($e->getMessage());
}

$acl = new Meduse_Acl();
Zend_Registry::set('acl', $acl);
