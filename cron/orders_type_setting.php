<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script orders_type_setting.php at "
  . date('Y-m-d H:i:s') . "\n";

$tOrders = new Table_Orders;

// pruchod pres vsechny objednavky ve wh dymek a nastaveni podle prirazenych produktu
echo 'Updating orders in WH pipes... ';
$select = $tOrders->select()->setIntegrityCheck(FALSE)->distinct(TRUE);
$select->from(array('o' => 'orders'), array('id'));
$select->joinLeft(array('p' => 'orders_products'), 'p.id_orders = o.id', array('type' => new Zend_Db_Expr("IF(ISNULL(p.id), 'service', 'products')")));
$select->where('id_wh_type = 1');
$result = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
$count = count($result);
if ($count) {
  foreach ($result as $row) {
    $tOrders->update(array('type' => $row['type']), 'id = ' . $row['id']);
  }
}
echo $count . ' was updated.' . "\n";

// nastaveni vsech objednavek ve wh tabaku na produktovou
echo 'Updating orders in WH tobacco... ';
$count = $tOrders->update(array('type' => 'products'), 'id_wh_type = 2');
echo $count . ' was updated.' . "\n";

echo "Finished script orders_type_setting.php at "
  . date('Y-m-d H:i:s') . "\n";