<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

$date = new DateTime();
echo "Start processing script pricelists_export.php at "
  . $date->format('Y-m-d H:i:s') . "\n";

$path = Zend_Registry::get('config')->data->pricelists_export_path;
if (!file_exists($path)) {
  mkdir($path);
}

$filename = $path . '/.timestamp.txt';
file_put_contents($filename, $date->getTimestamp());

$table = new Table_Pricelists();
if ($pricelists = $table->getPricelistsForExport(Table_TobaccoWarehouse::WH_TYPE)) {
  foreach ($pricelists as $abbr => $list) {
    $data = array();
    $filename = $path . '/' . strtr($abbr, ' ', '_') . '.csv';
    $data[] = 'SKU,1+' . PHP_EOL;
    foreach ($list as $item) {
      $data[] = $item['sku'] . ',' . $item['price'] . PHP_EOL;
    }
    file_put_contents($filename, $data);
  }
}

if ($assings = $table->getPricelistsAssignsForExport(Table_TobaccoWarehouse::WH_TYPE)) {
  $data = array();
  $filename = $path . '/assigns.csv';
  foreach ($assings as $assing) {
    $data[] = str_replace(' ', '', $assing['ic']) . ',' . strtr($assing['pl'], ' ', '_') . PHP_EOL;
  }
  file_put_contents($filename, $data);
}

echo "Finished script pricelists_export.php at "
  . date('Y-m-d H:i:s') . "\n";
