<?php
/**
 * Load bootstrap and database
 */
include_once 'backend_bootstrap.php';

echo "Start processing script email_unpaid_invoices.php at " . date('Y-m-d H:i:s') . "\n";

// kontrolujeme a odesilame notifikace zvlast pro sklad dymek a zvlast pro sklad tabaku
foreach (array(Table_PartsWarehouse::WH_TYPE, Table_TobaccoWarehouse::WH_TYPE) as $wh) {
  $list = array();
  $table = new Table_Invoices();
  if ($unpaid = $table->getPaymentsStats(array('overdue' => TRUE, 'unpaid' => TRUE, 'wh' => $wh))) {
    foreach ($unpaid as $row) {
      if (!isset($list[$row->owner_email])) {
        $list[$row->owner_email] = array();
      }
      $list[$row->owner_email][] = $row;
    }
    $email = new Table_Emails();
    foreach ($list as $owner => $stats) {
      // odeslani emailu vlastnikum
      $email->sendEmail(Table_Emails::EMAIL_NOTICE_UNPAID_INVOICES, array(
        'stats' => $stats, 'recipient' => $owner, 'wh' => $wh));
    }
  }
}

echo "Finished script email_unpaid_invoices.php at " . date('Y-m-d H:i:s') . "\n";