$(document).ready(function() {
    var time = new Date();
    time.setMinutes(time.getMinutes() + countdown);
    $('#time_countdown').countdown({
            until: time,
            layout: '{hn}{sep}{mnn}{sep}{snn}'
    });
    var offset = $('.navbar').height();
    $("table.sticky").stickyTableHeaders({fixedOffset: offset, cacheHeaderHeight: true});
});

