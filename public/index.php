<?php

error_reporting(E_ALL ^ E_DEPRECATED);
ini_set('display_errors', 1);
date_default_timezone_set('Europe/Prague');

set_include_path('.'
	   . PATH_SEPARATOR . '../library'
     . PATH_SEPARATOR . '../library/codeplex'
     . PATH_SEPARATOR . '../application/models'
     . PATH_SEPARATOR . get_include_path());
require_once 'dompdf/dompdf_config.inc.php';
require_once 'dompdf/include/autoload.inc.php';
spl_autoload_register('DOMPDF_autoload');

require_once "Zend/Loader.php";
Zend_Loader::registerAutoload();
Zend_Session::start();

define('APPLICATION_PATH', realpath('../application'));
define('PROJECT_PATH', realpath('..'));

$config = new Zend_Config_Ini('../application/config.ini', 'folders');
$registry = Zend_Registry::getInstance();
$registry->set('folders', $config);
$config = new Zend_Config_Ini('../application/config.ini', 'general');
$registry->set('config', $config);

if ($config->version == "PRODUCTION") {
  ini_set('display_errors', 0);
  if($_SERVER["HTTPS"] != "on") {
      header("HTTP/1.1 301 Moved Permanently");
      header("Location: https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
      exit();
  }
}

$parts_warehouse_sess = new Zend_Session_Namespace('parts_warehouse');
if (!isset($parts_warehouse_sess->lastSearch)) {
	$parts_warehouse_sess->lastSearch = null;
}
$registry->set('parts_warehouse', $parts_warehouse_sess);

$tobacco_warehouse_sess = new Zend_Session_Namespace('tobacco_warehouse');
if (!isset($tobacco_warehouse_sess->lastSearch)) {
	$tobacco_warehouse_sess->lastSearch = null;
}
$registry->set('tobacco_warehouse', $tobacco_warehouse_sess);

$tobacco_orders_sess = new Zend_Session_Namespace('tobacco_orders');
if (!isset($tobacco_orders_sess->lastSearch)) {
	$tobacco_orders_sess->lastSearch = null;
}
$registry->set('tobacco_orders', $tobacco_orders_sess);

$invoice_filter_sess = new Zend_Session_Namespace('invoice_filter');
if (!isset($invoice_filter_sess->lastSearch)) {
	$invoice_filter_sess->lastSearch = null;
}
$registry->set('invoice_filter', $invoice_filter_sess);

$tobacco_invoice_filter_sess = new Zend_Session_Namespace('tobacco_invoice_filter');
if (!isset($tobacco_invoice_filter_sess->lastSearch)) {
	$tobacco_invoice_filter_sess->lastSearch = null;
}
$registry->set('tobacco_invoice_filter', $tobacco_invoice_filter_sess);

$db = Zend_Db::factory($config->db);
$registry->set('db', $db);
Zend_Db_Table::setDefaultAdapter($db);

// Logovani do aplikačního logu.
try {
  $logFilename = $config->dirs->logs . '/application.log';
  if (!$stream = @fopen($logFilename, 'a', FALSE)) {
    throw new Exception('Nelze zapisovat do aplikačního logu "' . $logFilename . '".');
  }
  $logger = new Zend_Log(new Zend_Log_Writer_Stream($stream));
  Zend_Registry::set('logger', $logger);
}
catch (Exception $e) {
  die($e->getMessage());
}

Zend_Registry::set('version', $config->version);

try {
  $acl = new Meduse_Acl();
}
catch (Zend_Acl_Exception $e) {
  Utils::logger()->err($e->getMessage() . ' Trace: ' . $e->getTraceAsString());
  die('Chyba v ACL registru. Zkontrolujte aplikační log.');
}

Zend_Registry::set('acl', $acl);

Zend_Registry::set('settings', new Table_Settings());

Zend_Registry::set('actualities', new Table_Actualities());

EmailFactory_Queue::createSession();

$frontendOptions = array('lifetime' => 7200, 'automatic_serialization' => true);
$backendOptions = array('cache_dir' => $config->dirs->cache . '/');
$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
Zend_Registry::set('cache', $cache);

$frontController = Zend_Controller_Front::getInstance();
$frontController->throwExceptions(false);
$frontController->setDefaultControllerName('Actualities');
$frontController->setControllerDirectory('../application/controllers');
Zend_Layout::startMvc(array('layoutPath'=>'../application/layouts'));

$view = new Zend_View();
$view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');
$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
$viewRenderer->setView($view);
Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);


$authNamespace = new Zend_Session_Namespace('Zend_Auth');
if ($authNamespace->expiration) {
  $authNamespace->setExpirationSeconds($authNamespace->expiration * 60);
}

try {
  if (true === $authNamespace->passwordExpired) {
    $request = new Zend_Controller_Request_Http();
    $request->setControllerName('admin');
    $request->setActionName('user-pwd');
    $request->setRequestUri('/admin/user-pwd');
    $frontController->dispatch($request);
  }
  else {
    $frontController->dispatch();
  }
}
catch (Error|Exception $e) {
  $logger->err($e->getMessage() . ' Trace: ' . $e->getTraceAsString());
  if ($config->version == "PRODUCTION") {
    die('Fatal error. See the application log.');
  }
  throw $e;
}

