<?php
const TOKEN = 'wK3pY5oT3vT7kX3rK7wR7qU6qL5cW9aW';
const MAX_TIME = 600;

ini_set('max_execution_time', MAX_TIME);

$token = filter_input(INPUT_GET, 'token', FILTER_SANITIZE_STRING);
if ($token !== TOKEN) {
  header('Forbidden', true, 403);
  print('<pre>403 Forbidden</pre>');
  die(403);
}

$script = filter_input(INPUT_GET, 'script', FILTER_SANITIZE_STRING);
$script = preg_replace('/\.php$/i', '', $script);
$path = '../cron/' . $script  . '.php';

if (!file_exists($path)) {
  header('Not found', true, 404);
  print('<pre>404 Not found</pre>');
  die(404);
}

include $path;

