<?php

error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', 1);
date_default_timezone_set('Europe/London');

set_include_path('.' . PATH_SEPARATOR . '../library/'
     . PATH_SEPARATOR . '../application/models'
     . PATH_SEPARATOR . get_include_path());
include "Zend/Loader.php";
Zend_Loader::registerAutoload();
Zend_Session::start();

$authNamespace = new Zend_Session_Namespace('Zend_Auth');
if(isset($_SESSION['Zend_Auth']['user']))$authNamespace->setExpirationSeconds(20*60); //odhlaseni po 20-ti minutach necinnosti

$auth = Zend_Auth::getInstance();

$config = new Zend_Config_Ini('../application/config.ini', 'folders');
$registry = Zend_Registry::getInstance();
$registry->set('folders', $config);
$config = new Zend_Config_Ini('../application/config.ini', 'general');
$registry->set('config', $config);


$db = Zend_Db::factory($config->db);
$registry->set('db', $db);
Zend_Db_Table::setDefaultAdapter($db);



//Cache Options
/*
$frontendOptions = array('lifetime' => 7200,'automatic_serialization' => true);
$backendOptions = array('cache_dir' => '/var/www/meduse/grid_test/data/cache/');
$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
Zend_Registry::set('cache',$cache);
*/

$frontController = Zend_Controller_Front::getInstance();
$frontController->throwExceptions(false);
//$frontController->setDefaultControllerName('PartsController');
$frontController->setControllerDirectory('../application/controllers');
Zend_Layout::startMvc(array('layoutPath'=>'../application/layouts'));

//try{
	$frontController->dispatch();
/*//} catch (Exception $e) {
	$string = "";
	foreach($frontController->getRequest()->getParams() as $key => $param){
		$string .= "$key => $param || ";
	}
	$db->insert('errors', array(
		'error_data' => $string.$e->getMessage()
	));
	$request = $frontController->getRequest();
	//$request->setParams(array('module' => 'index', 'controller' => 'parts', 'action' => 'index'));
	//$frontController->resetInstance();$frontController->setDefaultModule('index');
	//$frontController->setRequest($request);
}*/
