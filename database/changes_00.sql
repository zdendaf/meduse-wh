
ALTER TABLE `orders`
ADD `id_customers` int(11) NULL AFTER `id_orders_carriers`;

--
-- #24 - NÁZEV a ID součásti dodavatele
--
ALTER TABLE `producers_parts`
ADD `producers_part_id` varchar(10) COLLATE 'utf8_czech_ci' NOT NULL AFTER `id_parts`,
ADD `producers_part_name` varchar(100) COLLATE 'utf8_czech_ci' NOT NULL AFTER `producers_part_id`,
CHANGE `inserted` `inserted` datetime NOT NULL AFTER `id_users`,
ADD `changed` timestamp NOT NULL AFTER `inserted`;

ALTER TABLE `producers` ADD `id_extern_warehouses` INT NULL AFTER `id` ;

ALTER TABLE `productions_parts` ADD `id_parts_operations` INT NULL AFTER `id_parts`;

CREATE TABLE `holograms` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_holograms_types` int NOT NULL,
  `id_orders` int(11) unsigned NULL,
  `hologram` varchar(150) COLLATE 'utf8_czech_ci' NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inserted` datetime NOT NULL,
  FOREIGN KEY (`id_orders`) REFERENCES `orders` (`id`)
) COMMENT='' ENGINE='InnoDB' COLLATE 'utf8_czech_ci';

CREATE TABLE `holograms_types` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(150) COLLATE 'utf8_czech_ci' NOT NULL
) COMMENT='' ENGINE='InnoDB';

ALTER TABLE `orders`
ADD `expected_payment_deposit` decimal(12,2) NULL COMMENT 'zaloha' AFTER `date_payment_accept`,
ADD `carrier_price` decimal(12,2) NULL COMMENT 'cena za dopravu' AFTER `id_orders_carriers`;

-- ------------------------------------------------------------------------
--                zmeny zadavani vyroby - podle pozadavku
--                  mnohem vice komplikovane nez puvodne
-- ------------------------------------------------------------------------

RENAME TABLE `parts_productions` TO `productions` ;

ALTER TABLE `productions`
ADD `id_producers` int(11) NULL AFTER `id_parent`,
ADD FOREIGN KEY (`id_producers`) REFERENCES `producers` (`id`) ON DELETE RESTRICT;

UPDATE productions AS p SET id_producers = (SELECT id_producers FROM producers_parts AS pp WHERE pp.id_parts = p.id_parts LIMIT 1 );

ALTER TABLE `productions` ADD `inserted` datetime NOT NULL AFTER `description`;

CREATE TABLE `productions_parts` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_productions` int(11) NOT NULL,
    `id_parts` varchar(10) COLLATE utf8_czech_ci NOT NULL,
    `amount` int(11) NOT NULL,
    `amount_remain` int(11) NOT NULL,
    `inserted` datetime NOT NULL,
    `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`), KEY `id_parts` (`id_parts`),
    KEY `id_productions` (`id_productions`),
    CONSTRAINT `productions_parts_ibfk_2` FOREIGN KEY (`id_productions`) REFERENCES `productions` (`id`),
    CONSTRAINT `productions_parts_ibfk_1` FOREIGN KEY (`id_parts`) REFERENCES `parts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='polozky vyroby';

CREATE TABLE `productions_deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_productions` int(11) NOT NULL,
  `delivery_date` date NOT NULL,
  `description` varchar(200) COLLATE 'utf8_czech_ci' NOT NULL,
  `inserted` datetime NOT NULL,
  `changed` timestamp NOT NULL,
  FOREIGN KEY (`id_productions`) REFERENCES `productions` (`id`)
) COMMENT='dodaci listy' ENGINE='InnoDB';

CREATE TABLE `productions_deliveries_parts` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_productions_deliveries` int(11) NOT NULL,
    `id_productions_parts` int(11) NOT NULL,
    `delivery_amount` int(11) NOT NULL,
    `inserted` datetime NOT NULL,
    `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `id_productions_deliveries` (`id_productions_deliveries`),
    KEY `id_productions_parts` (`id_productions_parts`),
    CONSTRAINT `productions_deliveries_parts_ibfk_1` FOREIGN KEY (`id_productions_deliveries`) REFERENCES `productions_deliveries` (`id`),
    CONSTRAINT `productions_deliveries_parts_ibfk_2` FOREIGN KEY (`id_productions_parts`) REFERENCES `productions_parts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='polozky dodacich listu';

-- plneni tabulky productions_parts
INSERT INTO productions_parts (SELECT NULL AS id, id AS id_productions, id_parts, amount, amount, NOW() as inserted, NULL as changed FROM `productions`);

UPDATE productions_parts SET amount_remain = amount;

-- plneni tabulky productions_deliveries
INSERT INTO productions_deliveries (SELECT NULL AS id, id AS id_productions, date_delivery as delivery_date, NULL as description, NOW() as inserted, NULL as changed FROM `productions` WHERE status = 'closed');

-- plneni tabulky productions_deliveries_parts
INSERT INTO productions_deliveries_parts(
SELECT NULL as id, pd.id as id_productions_delivery, pp.id as id_productions_parts, p.amount as delivery_amount, NOW() as inserted, NULL as changed
FROM productions AS p
JOIN productions_deliveries AS pd ON p.id = pd.id_productions
JOIN productions_parts AS pp ON pp.id_productions = p.id
);


-- mazani nepouzivanych sloupcu
ALTER TABLE `productions`
DROP `id_parts`,
DROP `id_parent`,
DROP `amount`,
DROP `amount_delivered`;

-- NOVY VIEW
CREATE VIEW w_productions_current AS SELECT p.id as id_productions, id_producers, date_ordering, date_delivery, pp.id_parts, amount, amount_remain
FROM productions AS p
JOIN productions_parts AS pp ON p.id = pp.id_productions
WHERE status = 'open';

-- ZMENY VAZEB:
UPDATE `productions_parts` SET id_productions = 63 WHERE id_productions IN (64,65,66,67,68,69,70,71,72,73);
DELETE FROM `productions` WHERE id IN (64,65,66,67,68,69,70,71,72,73);

UPDATE `productions_parts` SET id_productions = 105 WHERE id_productions IN (106,107,110,111);
DELETE FROM `productions` WHERE id IN (106,107,110,111);


-- ------------------------------------------------------------------------
--                              2011-11-14
-- ------------------------------------------------------------------------

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, 'warehouseman', 'orders/strategic-data', NULL, NOW()),
(NULL, 'warehouseman', 'orders/strategic-graph', NULL, NOW()),
(NULL, 'warehouseman', 'orders/special/strategic', NULL, NOW());

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'orders/proforma', 'definice resource', '2011-11-14 02:58:34'),
(NULL, 'admin', 'orders/proforma', 'prirazeni resource', '2011-11-14 02:58:34'),
(NULL, 'manager', 'orders/proforma', 'prirazeni resource', '2011-11-14 02:58:34');

ALTER TABLE `orders` ADD `proforma_no` INT( 11 ) NULL DEFAULT NULL COMMENT 'cislo faktury' AFTER `purchaser`;

-- ------------------------------------------------------------------------
--                              2011-11-07
-- ------------------------------------------------------------------------

ALTER TABLE `parts_productions`
ADD `id_parent` int(11) NOT NULL AFTER `id_parts`,
ADD `changed` timestamp NOT NULL AFTER `description`;

ALTER TABLE `parts_productions`
CHANGE `status` `status` enum('open','closed','parial') COLLATE 'utf8_czech_ci' NOT NULL COMMENT 'stav vyroby' AFTER `date_delivery`,
COMMENT='vyroba soucasti';

ALTER TABLE `parts_productions`
ADD `amount_delivered` int(10) NOT NULL DEFAULT 0 COMMENT 'pocet dodanych soucasti' AFTER `date_delivery`,
COMMENT='vyroba soucasti';

-- ------------------------------------------------------------------------
--                              2011-10-30
-- ------------------------------------------------------------------------

ALTER TABLE `orders` ADD `date_payment_accept` DATE NULL DEFAULT NULL COMMENT 'pouze u prepravy na dobirku: potvrzení s datem, kdy platba došla na učet' AFTER `date_pay_all` ;

-- ------------------------------------------------------------------------
--                              2011-09-15
-- ------------------------------------------------------------------------

-- tracking no objednavky
ALTER TABLE `orders` ADD `tracking_no` VARCHAR( 100 ) NOT NULL COMMENT 'trackovaci cislo objednavky u dodavatele' AFTER `strategic_months`;

-- "ocekavana platba" podle jury
ALTER TABLE `orders` ADD `expected_payment` DECIMAL( 12, 2 ) NULL DEFAULT NULL COMMENT 'u predbeznych objednavek - ocekavana platba' AFTER `date_pay_all` ,
ADD `expected_payment_currency` VARCHAR( 3 ) NOT NULL DEFAULT 'CZK' COMMENT 'u predbeznych objedavek - mena ocekavane platby' AFTER `expected_payment`;

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'orders/special/expected-payment', 'special. definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'orders/special/expected-payment', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'orders/special/expected-payment', NULL, CURRENT_TIMESTAMP),
(NULL, 'seller', 'orders/special/expected-payment', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'orders/tracking-no', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'orders/tracking-no', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'orders/tracking-no', NULL, CURRENT_TIMESTAMP),
(NULL, 'warehouseman', 'orders/tracking-no', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'parts/collections', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/collections', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/collections', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'parts/add-collection', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/add-collection', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/add-collection', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'parts/delete-collection', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/delete-collection', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/delete-collection', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'parts/edit-collection', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/edit-collection', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/edit-collection', NULL, CURRENT_TIMESTAMP);

-- ------------------------------------------------------------------------
--                              2011-09-15
-- ------------------------------------------------------------------------

UPDATE `parts` SET `set`='y' WHERE `id_parts_ctg` IN (23);

UPDATE `parts` SET `product`='n' WHERE `id_parts_ctg` IN (24,25,26);

-- ------------------------------------------------------------------------
--                              2011-09-11 uz je na ostre
-- ------------------------------------------------------------------------

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'multi-edit/index', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'multi-edit/index', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'multi-edit/index', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'multi-edit/step2', NULL, CURRENT_TIMESTAMP),
(NULL, 'admin', 'multi-edit/step2', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'multi-edit/step2', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'multi-edit/step3', NULL, CURRENT_TIMESTAMP),
(NULL, 'admin', 'multi-edit/step3', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'multi-edit/step3', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'multi-edit/step4', NULL, CURRENT_TIMESTAMP),
(NULL, 'admin', 'multi-edit/step4', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'multi-edit/step4', NULL, CURRENT_TIMESTAMP),
 (NULL, NULL, 'multi-edit/edit-amount', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'multi-edit/edit-amount', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'multi-edit/edit-amount', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'multi-edit/add', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'multi-edit/add', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'multi-edit/add', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'parts/edit-productions', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/edit-productions', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/edit-productions', NULL, CURRENT_TIMESTAMP),
(NULL, 'warehouseman', 'parts/edit-productions', NULL, CURRENT_TIMESTAMP);

-- ------------------------------------------------------------------------
--                              2011-09-09
-- ------------------------------------------------------------------------

ALTER TABLE `parts_warehouse_history` ADD `note` VARCHAR( 100 ) NULL DEFAULT NULL AFTER `critical_amount`;

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'parts/history-back', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/history-back', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/history-back', NULL, CURRENT_TIMESTAMP),
(NULL, 'warehouseman', 'parts/history-back', NULL, CURRENT_TIMESTAMP);

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'parts/history', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/history', NULL, CURRENT_TIMESTAMP);

-- ------------------------------------------------------------------------
--                              2011-08-27
-- ------------------------------------------------------------------------

ALTER TABLE `parts_operations` ADD `id_producers` INT( 11 ) NULL DEFAULT NULL AFTER `id_parts` ;

-- ------------------------------------------------------------------------
--                              2011-08-19
-- ------------------------------------------------------------------------

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'orders/carriers', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'orders/carriers', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'orders/carriers', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'orders/add-carrier', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'orders/add-carrier', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'orders/add-carrier', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'orders/edit-carrier', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'orders/edit-carrier', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'orders/edit-carrier', NULL, CURRENT_TIMESTAMP);


CREATE TABLE `orders_carriers` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 200 ) NOT NULL ,
`inserted` DATETIME NOT NULL
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_czech_ci COMMENT = 'dopravci objednavek';

INSERT INTO `orders_carriers` (`id`, `name`, `inserted`) VALUES
(1, 'DPD dobírka', '2011-08-15 13:33:14'),
(2, 'DHL express', '2011-08-19 13:35:51'),
(3, 'DHL freight', '2011-08-19 13:39:31');

ALTER TABLE `orders` ADD `id_orders_carriers` INT( 11 ) NOT NULL DEFAULT '1' COMMENT 'dopravce' AFTER `id_addresses` ,
ADD INDEX ( `id_orders_carriers` );

CREATE TABLE `parts_operations` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_parts` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL ,
`name` VARCHAR( 100 ) NOT NULL ,
`price` DECIMAL( 10, 2 ) NOT NULL ,
`changed` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`inserted` DATETIME NOT NULL ,
INDEX ( `id_parts` )
);

ALTER TABLE `parts` ADD `old_price` DECIMAL( 10, 2 ) NULL DEFAULT NULL COMMENT 'debug field' AFTER `price`;

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'parts/add-operation', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/add-operation', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/add-operation', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'parts/edit-operation', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/edit-operation', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/edit-operation', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'parts/delete-operation', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/delete-operation', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/delete-operation', NULL, CURRENT_TIMESTAMP);


-- ------------------------------------------------------------------------
--                              2011-08-07
-- ------------------------------------------------------------------------

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, 'warehouseman', 'parts/detail/producer', NULL, CURRENT_TIMESTAMP);

ALTER TABLE `orders` CHANGE `payment_method` `payment_method` ENUM( 'cash_on_delivery', 'cash', 'transfer', 'free_presentation' ) CHARACTER SET utf8 COLLATE utf8_czech_ci NULL DEFAULT NULL COMMENT 'zpusob platby: na dobirku, v hotovosti, prevodem';

-- ------------------------------------------------------------------------
--                              2011-08-07
-- ------------------------------------------------------------------------

CREATE TABLE `parts_collections` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `id_parts` varchar(10) COLLATE utf8_czech_ci NOT NULL,
 `id_collections` int(11) NOT NULL,
 `inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `parts_collections` (`id_parts`,`id_collections`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='vazebni tabulka pro kolekci soucasti';

-- plneni tabulky
INSERT INTO parts_collections (SELECT NULL AS id, `id` AS id_parts, `id_collections` , NOW( ) AS inserted
FROM `parts`
WHERE id_collections IS NOT NULL);

-- plneni pro spolecne soucasti
INSERT INTO parts_collections (SELECT NULL AS id, `id` AS id_parts, 1 AS `id_collections` , NOW( ) AS inserted
FROM `parts`
WHERE id_collections IS NULL);
INSERT INTO parts_collections (SELECT NULL AS id, `id` AS id_parts, 2 as `id_collections` , NOW( ) AS inserted
FROM `parts`
WHERE id_collections IS NULL);

ALTER TABLE `parts_ctg` ADD `parent_ctg` INT NULL DEFAULT NULL AFTER `id`;

INSERT INTO parts_ctg (SELECT NULL as id, 22 as parent_ctg, ctg_name, `description` FROM `wraps_ctg`);

INSERT INTO `parts_ctg` (`id`, `parent_ctg`, `name`, `description`) VALUES (NULL, NULL, 'Accessories', NULL);

UPDATE `parts_ctg` SET `parent_ctg` = '31' WHERE `parts_ctg`.`id` =1;

UPDATE `parts_ctg` SET `parent_ctg` = '31' WHERE `parts_ctg`.`id` =2;

UPDATE `parts_ctg` SET `parent_ctg` = '31' WHERE `parts_ctg`.`id` =3;

UPDATE `parts_ctg` SET `parent_ctg` = '31' WHERE `parts_ctg`.`id` =7;

UPDATE `parts_ctg` SET `parent_ctg` = '31' WHERE `parts_ctg`.`id` =8;

UPDATE `parts_ctg` SET `parent_ctg` = '0' WHERE `parts_ctg`.`id` =31;

UPDATE `parts_ctg` SET `parent_ctg` = '0' WHERE `parts_ctg`.`id` =22;

UPDATE parts SET id_parts_ctg=24 WHERE id IN (SELECT `id` FROM `wraps` WHERE `id_wraps_ctg` = 1);
UPDATE parts SET id_parts_ctg=25 WHERE id IN (SELECT `id` FROM `wraps` WHERE `id_wraps_ctg` = 2);
UPDATE parts SET id_parts_ctg=26 WHERE id IN (SELECT `id` FROM `wraps` WHERE `id_wraps_ctg` = 3);
UPDATE parts SET id_parts_ctg=27 WHERE id IN (SELECT `id` FROM `wraps` WHERE `id_wraps_ctg` = 4);
UPDATE parts SET id_parts_ctg=28 WHERE id IN (SELECT `id` FROM `wraps` WHERE `id_wraps_ctg` = 5);

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'parts/special/collections', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/special/collections', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/special/collections', NULL, CURRENT_TIMESTAMP),
(NULL, NULL, 'parts/ajax-set-collection', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'parts/ajax-set-collection', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'parts/ajax-set-collection', NULL, CURRENT_TIMESTAMP),
(NULL, 'warehouseman', 'parts/ajax-set-collection', NULL, CURRENT_TIMESTAMP),
(NULL, 'seller', 'orders/special/trash', NULL, CURRENT_TIMESTAMP);

ALTER TABLE `parts` CHANGE `id_collections` `id_collections` INT( 11 ) NULL DEFAULT NULL COMMENT 'nepouzivany sloupec - smazat';

UPDATE parts SET `product`='y' WHERE `id_parts_ctg` IN ( 24, 25, 26, 27, 28 );

-- ------------------------------------------------------------------------
--                              2011-08-01
-- ------------------------------------------------------------------------

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES 
(NULL, 'warehouseman', 'producers/show-order', 'na zadost jiriho 1.8.2011 ma pristup i rosta', CURRENT_TIMESTAMP),
(NULL, 'warehouseman', 'producers/parts', 'na zadost jiriho 1.8.2011 ma pristup i rosta', CURRENT_TIMESTAMP);

-- ------------------------------------------------------------------------
--                              2011-07-31
-- ------------------------------------------------------------------------

INSERT INTO orders_products (SELECT NULL as id, id_orders, id_wraps, amount FROM orders_wraps);
TRUNCATE orders_wraps;

ALTER TABLE `orders` CHANGE `status` `status` ENUM( 'new', 'strategic', 'confirmed', 'open', 'closed', 'trash', 'deleted' ) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL DEFAULT 'new' COMMENT 'stav objednavky (nova, potvrzena, otevrena, uzavrena, smazana)';

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'orders/special/trash', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'admin', 'orders/special/trash', NULL, CURRENT_TIMESTAMP),
(NULL, 'manager', 'orders/special/trash', NULL, CURRENT_TIMESTAMP);

UPDATE orders SET status = 'trash' WHERE id IN (97, 113, 124, 135, 141, 145, 156, 157);

-- odmazani objednavek
INSERT INTO orders_products SELECT
NULL as id, id_orders, id_products, amount
FROM `orders_products_history` WHERE `id_orders` IN (SELECT DISTINCT id
FROM orders
WHERE STATUS = 'trash');

DELETE FROM `orders_products_history` WHERE `id_orders` IN (SELECT DISTINCT id
FROM orders
WHERE STATUS = 'trash');
