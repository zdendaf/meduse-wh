--
-- Toto je soubor pro databazove zmeny v roce 2020.
-- Zmeny vkladame na zacatek souboru - ne na konec (nejnovejsi zmeny jsou nahore).
-- Zmenu v tomto souboru nesmi obsahovat cele nazvy tabulek (ne db_name.db_table, jen db_table).
-- Kazda zmena databaze musi byt uvozena komentarem ve tvaru:
--
-- YYYY-MM-DD #123: nazev issue z Bitbucketu

-- 2020-12-17
insert into users_acl (role, resource)
select role, 'tobacco/export-parts' as resource
from users_acl
where resource like 'tobacco/export';

insert into users_acl (role, resource)
select role, 'tobacco/export-maceration' as resource
from users_acl
where resource like 'tobacco/export';

insert into users_acl (role, resource)
select role, 'tobacco/export-download' as resource
from users_acl
where resource like 'tobacco/export';

-- 2020-12-08 HOTFIX: Uprava pomeru maceratu
CREATE TEMPORARY TABLE IF NOT EXISTS tmp_mac AS
SELECT mh.id_macerations,
       SUM(mh.amount) AS changes
FROM macerations_history mh
WHERE mh.id_macerations IN (
    SELECT RIGHT(LEFT(`params`, 6), 3) AS id_macerations
    FROM users_action_log
    WHERE `date` >= '2020-11-01'
      AND `action` LIKE 'tobacco/macerations-save'
      AND params LIKE 'id=___, recipe=%')
GROUP BY mh.id_macerations;

UPDATE macerations m
    JOIN tmp_mac t ON t.id_macerations = m.id AND t.changes >= 0
SET amount_current = changes
WHERE m.id = t.id_macerations;

UPDATE macerations SET amount_current = 8.571 WHERE id = 376; -- eucalyptus

DROP TABLE tmp_mac;


-- 2020-08-03 #262: Složení sudů macerátu
DELETE FROM macerations_parts;
INSERT INTO macerations_parts (id_macerations, id_parts, amount)
SELECT
    m.id AS id_macerations,
    rp.id_parts,
    ROUND (rp.ratio * m.amount_in / rt.total, 3) AS amount
FROM macerations m
    JOIN recipes r ON r.id = m.id_recipes
    JOIN (
        SELECT m.id, SUM(rp.ratio) AS total FROM macerations m
            JOIN recipes r ON r.id = m.id_recipes
            LEFT JOIN recipes_parts rp ON rp.id_recipes = r.id
            GROUP BY m.id
        ) rt ON rt.id = m.id
    LEFT JOIN recipes_parts rp ON rp.id_recipes = r.id
WHERE rp.id_parts IS NOT NULL
ORDER BY m.id, rp.id_parts;

--
-- 2020-07-15 #222: UPC – Stav skladu na konci roku
--
INSERT INTO users_acl (role, resource)
SELECT role, 'tobacco/close-year' AS resource FROM users_acl WHERE resource = 'tobacco/warehouse-snapshot';
INSERT INTO users_acl (role, resource)
SELECT role, 'tobacco/close-year-process' AS resource FROM users_acl WHERE resource = 'tobacco/warehouse-snapshot';


-- NENULOVE STAVY K 31.12.2019
INSERT INTO parts_warehouse_snapshot (id_parts, id_users, amount_wh, amount_real, `date`, note, is_fixed)
SELECT
    p.id AS id_parts,
    11 AS id_users,
    i.amount_past AS amount_wh,
    i.amount_past AS amount_real,
    '2019-12-31' AS `date`,
    'inventura 31/12/2019' AS note,
    'n' AS is_fixed
FROM parts p
         JOIN parts_warehouse w ON w.id_parts = p.id
         JOIN parts_warehouse_snapshot s19 ON s19.id_parts = p.id AND s19.`date` = '2019-01-01'
         LEFT JOIN parts_warehouse_snapshot s20 ON s20.id_parts = p.id AND s20.`date` = '2019-12-31'
         JOIN (
    SELECT
        p.id,
        w.amount - IFNULL(h.`add`, 0) + IFNULL(h.`remove`, 0) AS amount_past
    FROM parts p
             JOIN parts_warehouse w ON w.id_parts = p.id
             LEFT JOIN (
        SELECT id_parts, SUM(IF(amount>0, amount, 0)) AS `add`,  SUM(IF(amount<0, -amount, 0)) AS `remove`
        FROM parts_warehouse_history WHERE YEAR(last_change) = '2020'
        GROUP BY id_parts
    ) h ON h.id_parts = p.id
    WHERE p.id_wh_type = 2 AND p.deleted = 'n'
      AND (p.id_parts_ctg IN (37, 38, 39) OR p.id = 'TS001')
) i ON i.id = p.id
WHERE p.id_wh_type = 2 AND p.deleted = 'n'
  AND (p.id_parts_ctg IN (37, 38, 39) OR p.id = 'TS001')
  AND s20.amount_real IS NULL;

-- NENULOVE STAVY K 01.01.2020
INSERT INTO parts_warehouse_snapshot (id_parts, id_users, amount_wh, amount_real, `date`, note, is_fixed)
SELECT
    p.id AS id_parts,
    11 AS id_users,
    i.amount_past AS amount_wh,
    i.amount_past AS amount_real,
    '2020-01-01' AS `date`,
    'počáteční stav 01.01.2020' AS note,
    'y' AS is_fixed
FROM parts p
         JOIN parts_warehouse w ON w.id_parts = p.id
         JOIN parts_warehouse_snapshot s19 ON s19.id_parts = p.id AND s19.`date` = '2019-01-01'
         LEFT JOIN parts_warehouse_snapshot s20 ON s20.id_parts = p.id AND s20.`date` = '2020-01-01'
         JOIN (
    SELECT
        p.id,
        w.amount - IFNULL(h.`add`, 0) + IFNULL(h.`remove`, 0) AS amount_past
    FROM parts p
             JOIN parts_warehouse w ON w.id_parts = p.id
             LEFT JOIN (
        SELECT id_parts, SUM(IF(amount>0, amount, 0)) AS `add`,  SUM(IF(amount<0, -amount, 0)) AS `remove`
        FROM parts_warehouse_history WHERE YEAR(last_change) = '2020'
        GROUP BY id_parts
    ) h ON h.id_parts = p.id
    WHERE p.id_wh_type = 2 AND p.deleted = 'n'
      AND (p.id_parts_ctg IN (37, 38, 39) OR p.id = 'TS001')
) i ON i.id = p.id
WHERE p.id_wh_type = 2 AND p.deleted = 'n'
  AND (p.id_parts_ctg IN (37, 38, 39) OR p.id = 'TS001')
  AND s20.amount_real IS NULL;

-- NULOVE STAVY K 1.1.2020 - nove produkty v roce 2020
INSERT INTO parts_warehouse_snapshot (id_parts, id_users, amount_wh, amount_real, `date`, note, is_fixed)
SELECT
    p.id AS id_parts,
    11 AS id_users,
    0 AS amount_wh,
    0 AS amount_real,
    '2020-01-01' AS `date`,
    'počáteční stav' AS note,
    'y' AS is_fixed
FROM parts p
         JOIN parts_warehouse w ON w.id_parts = p.id
         LEFT JOIN parts_warehouse_snapshot s19 ON s19.id_parts = p.id AND s19.`date` = '2019-01-01'
         LEFT JOIN parts_warehouse_snapshot s20 ON s20.id_parts = p.id AND s20.`date` = '2020-01-01'
WHERE p.id_wh_type = 2 AND p.deleted = 'n'
  AND (p.id_parts_ctg IN (37, 38, 39) OR p.id = 'TS001')
  AND s19.amount_real IS NULL AND s20.amount_real IS NULL;

-- korekce dle zaverky
UPDATE parts_warehouse_snapshot SET amount_real = 15091, `date` = '2019-12-31' WHERE id_parts = 'TS001' AND `date` = '2019-12-31';
UPDATE macerations m SET m.amount_current = m.amount_current + 2.15 WHERE m.id = 522;

--
-- 2020-06-03 #252: Kontrola prodejů
--
UPDATE parts p SET p.id_parts_ctg = 39
    WHERE p.id_wh_type = 2 AND p.id LIKE 'TK%'
      AND p.id NOT IN ('TK001', 'TK002', 'TK003', 'TK004')
      AND p.id_parts_ctg IS NULL;
UPDATE parts p SET p.id_parts_ctg = 37
    WHERE p.id_wh_type = 2 AND p.id LIKE 'TA%'
      AND p.id_parts_ctg IS NULL;
UPDATE parts p SET p.id_parts_ctg = 36
    WHERE p.id_wh_type = 2 AND p.id LIKE 'TO%'
      AND p.id_parts_ctg IS NULL;
UPDATE parts p SET p.id_parts_ctg = 41
    WHERE p.id_wh_type = 2 AND p.id LIKE 'TSP%'
      AND p.id_parts_ctg IS NULL;

INSERT INTO parts_subparts (id_multipart, id_parts, amount)
    (
        SELECT p.id AS id_multipart, p2.id AS id_parts, 1 AS amount
        FROM parts p
                 LEFT JOIN parts_subparts s ON s.id_multipart = p.id AND s.id_parts NOT IN ('TK001', 'TK002', 'TK003', 'TK004')
                 LEFT JOIN parts p2 ON p2.id = CONCAT('TA', RIGHT(p.id, 6))
        WHERE p.id LIKE 'TK%' AND p.id NOT IN ('TK001', 'TK002', 'TK003', 'TK004') AND id_parts IS NULL
          AND p.id NOT IN ('TK150027', 'TK150028', 'TK150030', 'TK150032', 'TK150034', 'TK150036', 'TK250037', 'TK250057', 'TK250066')
    );

INSERT INTO parts_subparts (id_multipart, id_parts, amount) VALUES
    ('TK150027', 'TA150037', 1),
    ('TK150028', 'TA150038', 1),
    ('TK150030', 'TA150040', 1),
    ('TK150032', 'TA150042', 1),
    ('TK150034', 'TA150044', 1),
    ('TK150036', 'TA150046', 1),
    ('TK250037', 'TA250036', 1),
    ('TK250057', 'TA250051', 1),
    ('TK250066', 'TA250061', 1);

--
-- 2020-05-29 #256: Státní svátek
--
ALTER TABLE `employees_works`
    CHANGE COLUMN `type` `type` ENUM('work','bustrip','bustrip_frgn','hoffice','overtime','vacation',
        'halfvacation','sickness','fmc','funeral','unpaid_vacation','holiday') NULL DEFAULT NULL
        COMMENT 'typ pracovniho zaznamu' COLLATE 'utf8_czech_ci' AFTER `date`;

--
-- 2020-04-23 #253: Nová platební metoda Pays
--
ALTER TABLE `orders`
    CHANGE COLUMN `payment_method` `payment_method`
    ENUM('cash_on_delivery','cash','card','transfer','free_presentation','free_claim','paypal','payscz') NULL DEFAULT NULL
    COMMENT 'zpusob platby: na dobirku, v hotovosti, kartou, prevodem, zdarma prezentace, zdarma clo, PayPal, Pays.cz'
    COLLATE 'utf8_czech_ci' AFTER `proforma_no`;

--
-- 2020-04-20 #250: Produkty více šarží na objednávce
--
CREATE TEMPORARY TABLE tmp
SELECT MAX(id) AS id, id_parts, SUM(amount) AS amount, batch
FROM parts_batch
GROUP BY id_parts, batch
ORDER BY id_parts;
DELETE FROM parts_batch WHERE id NOT IN (SELECT id FROM tmp);
UPDATE parts_batch pb JOIN tmp ON tmp.id = pb.id SET pb.amount = tmp.amount;
DROP TABLE tmp;
INSERT INTO parts_batch (id_parts, batch, amount)
SELECT p.id AS id_parts, CONCAT('L200420', LPAD(m.id_flavor, 3, '0'))  AS batch, pw.amount
FROM parts_warehouse pw
         JOIN parts p ON p.id = pw.id_parts AND p.id_wh_type = 2
         JOIN parts_ctg pc ON pc.id = p.id_parts_ctg AND pc.id = 39
         JOIN parts_subparts ps ON ps.id_multipart = pw.id_parts AND ps.id_parts LIKE 'TA%'
         JOIN parts_mixes pm ON pm.id_parts = ps.id_parts
         JOIN mixes m ON m.id = pm.id_mixes
WHERE pw.amount > 0;
--
-- 2020-04-01 #248: Špatně generovaná čísla šarží produktů
--
UPDATE orders_products_batch opb
    JOIN (SELECT MIN(id_macerations) AS id_macerations, batch FROM batch_mixes bm WHERE LENGTH (batch) = 6 GROUP BY batch) bmx ON bmx.batch = opb.batch
    JOIN macerations m ON m.id = bmx.id_macerations
SET opb.batch = CONCAT(LEFT(m.batch, 7), RIGHT(opb.batch, 3))
WHERE LENGTH(opb.batch) = 6;

UPDATE parts_batch pb
    JOIN (SELECT MIN(id_macerations) AS id_macerations, batch FROM batch_mixes bm WHERE LENGTH (batch) = 6 GROUP BY batch) bmx ON bmx.batch = pb.batch
    JOIN macerations m ON m.id = bmx.id_macerations
SET pb.batch = CONCAT(LEFT(m.batch, 7), RIGHT(pb.batch, 3))
WHERE LENGTH(pb.batch) = 6;

UPDATE batch_mixes bm
    JOIN (SELECT MIN(id_macerations) AS id_macerations, batch FROM batch_mixes bm WHERE LENGTH (batch) = 6 GROUP BY batch) bmx ON bmx.batch = bm.batch
    JOIN macerations m ON m.id = bmx.id_macerations
SET bm.batch = CONCAT(LEFT(m.batch, 7), RIGHT(bm.batch, 3))
WHERE LENGTH(bm.batch) = 6;

--
-- 2020-20-20 #229: COO
--
ALTER TABLE `orders`
    ADD COLUMN `coo_requirement` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'je pozadovano COO?' AFTER `expected_payment_rate`,
    ADD COLUMN `coo_date_confirm` DATE NULL DEFAULT NULL COMMENT 'datum potvrzeni COO' AFTER `coo_requirement`,
    ADD COLUMN `coo_date_validity` DATE NULL DEFAULT NULL COMMENT 'datum platnosti COO' AFTER `coo_date_confirm`,
    ADD COLUMN `invoice_verify_requirement` ENUM('y','n') NULL DEFAULT 'n' COMMENT 'je pozadovano overeni faktury?' AFTER `coo_date_validity`;

INSERT INTO users_acl (role, resource, description)
SELECT role, 'orders/save-coo-dates' AS resource, 'nastaveni datumu COO' AS description
FROM users_acl a WHERE a.resource LIKE 'order-edit/index';

--
-- 2020-03-19 #246: Připojené soubory k zakázce
--
CREATE TABLE `orders_files`
(
    `id`        INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_orders` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na objednavku',
    `id_users`  INT(11) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'cizi klic na uzivatele - autor',
    `created`   TIMESTAMP        NOT NULL DEFAULT current_timestamp() COMMENT 'cas vytvoreni',
    `filename`  VARCHAR(255)     NOT NULL COMMENT 'jmeno souboru' COLLATE 'utf8_czech_ci',
    `title`     VARCHAR(255)     NULL     DEFAULT NULL COMMENT 'popisek souboru' COLLATE 'utf8_czech_ci',
    PRIMARY KEY (`id`),
    INDEX `id_orders` (`id_orders`),
    INDEX `id_users` (`id_users`),
    CONSTRAINT `FK_orders_files_orders` FOREIGN KEY (`id_orders`) REFERENCES `orders` (`id`),
    CONSTRAINT `FK_orders_files_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
)
    COMMENT ='prilozene soubory k objednavkam'
    COLLATE = 'utf8_general_ci'
    ENGINE = InnoDB;

INSERT INTO users_acl (role, resource, description)
SELECT role, 'orders/file-upload' AS resource, 'upload souboru k objednavce' AS description
FROM users_acl a WHERE a.resource LIKE 'order-edit/index';

INSERT INTO users_acl (role, resource, description)
SELECT role, 'orders/file-remove' AS resource, 'upload souboru k objednavce' AS description
FROM users_acl a WHERE a.resource LIKE 'order-edit/index';

INSERT INTO users_acl (role, resource, description)
SELECT role, 'orders/file-download' AS resource, 'upload souboru k objednavce' AS description
FROM users_acl a WHERE a.resource LIKE 'order-edit/index';

INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-orders/file-upload' AS resource, 'upload souboru k objednavce' AS description
FROM users_acl a WHERE a.resource LIKE 'tobacco-order-edit/index';

INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-orders/file-remove' AS resource, 'upload souboru k objednavce' AS description
FROM users_acl a WHERE a.resource LIKE 'tobacco-order-edit/index';

INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-orders/file-download' AS resource, 'upload souboru k objednavce' AS description
FROM users_acl a WHERE a.resource LIKE 'tobacco-order-edit/index';

--
-- 2020-03-06 #236: Přehled produktů pro obchodníka
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'parts/products-overview', 'Prehled produktu'),
('admin', 'parts/products-overview', NULL),
('manager', 'parts/products-overview', NULL),
('sales_director', 'parts/products-overview', NULL),
('seller', 'parts/products-overview', NULL);

--
-- 2020-03-04 #242: Částečný odečet přijaté platby
--
CREATE TABLE `payments_deductions` (
    `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_payments` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic uhrady',
    `deduct_from` INT(11)          NOT NULL COMMENT 'cizi klic na fakturu, ze ktere se odecita',
    `amount`      DOUBLE(13, 5)    NOT NULL COMMENT 'vyse uhrady',
    PRIMARY KEY (`id`),
    INDEX `FK_payments_deductions_payments` (`id_payments`),
    INDEX `FK_payments_deductions_invoice` (`deduct_from`),
    CONSTRAINT `FK_payments_deductions_invoice` FOREIGN KEY (`deduct_from`) REFERENCES `invoice` (`id`),
    CONSTRAINT `FK_payments_deductions_payments` FOREIGN KEY (`id_payments`) REFERENCES `payments` (`id`)
)
COLLATE = 'utf8_czech_ci'
ENGINE = InnoDB;

INSERT INTO payments_deductions (id_payments, deduct_from, amount)
SELECT p.id AS id_payments, p.deduct_from, p.amount FROM payments p WHERE p.deduct_from IS NOT NULL;

ALTER TABLE `payments`
    DROP COLUMN `deduct_from`,
    DROP FOREIGN KEY `FK_payments_invoice_2`;



--
-- 2020-02-13 #234: Rozložení formuláře objednávky materiálu
--
ALTER TABLE `productions_parts`
    CHANGE COLUMN `amount` `amount` DECIMAL(10,3) NOT NULL DEFAULT 0 AFTER `id_parts_operations`,
    CHANGE COLUMN `amount_remain` `amount_remain` DECIMAL(10,3) NOT NULL DEFAULT 0 AFTER `amount`;

--
-- 2020-01-29 #227: Rozměry zásilky
--
INSERT INTO users_acl (role, resource, description) VALUES
('head_warehouseman', 'orders/packaging-save', NULL);

--
-- 2020-02-06 #217: Naklady na objednavky
--
INSERT INTO users_acl (role, resource, description) VALUES
('head_warehouseman', 'producers/parts-show-sums', NULL);

--
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'tobacco/warehouse-daily-packing', 'Denni zadani nabalenych vyrobku'),
('admin', 'tobacco/warehouse-daily-packing', NULL),
('manager', 'tobacco/warehouse-daily-packing', NULL),
('tobacco_whman', 'tobacco/warehouse-daily-packing', NULL);

--
-- 2020-02-04 #227: Rozměry zásilky
--
ALTER TABLE `emails`
    ALTER `to` DROP DEFAULT;
ALTER TABLE `emails`
    CHANGE COLUMN `to` `to` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci' AFTER `type`;

--
-- 2020-01-29 #227: Rozměry zásilky
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'orders/packaging-save', 'Definovat baleni zakazky'),
('admin', 'orders/packaging-save', NULL),
('manager', 'orders/packaging-save', NULL),
('warehouseman', 'orders/packaging-save', NULL);

--
-- 2020-01-24 #227: Rozměry zásilky
--
ALTER TABLE `wraps`
    CHANGE COLUMN `id_packings` `id_packings` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic na exportni jednotku' AFTER `id`,
    CHANGE COLUMN `no` `no` SMALLINT(5) UNSIGNED NULL DEFAULT NULL COMMENT 'cislo boxu v ramci exportni jednotky' AFTER `id_wraps`,
    CHANGE COLUMN `weight_netto` `weight_netto` DECIMAL(10,3) UNSIGNED NULL DEFAULT NULL COMMENT 'cista vaha obsahu' AFTER `no`;
ALTER TABLE `wraps`
    CHANGE COLUMN `id_parts` `id_parts` VARCHAR(11) NULL DEFAULT NULL COMMENT 'cizi klic na obalovou polozku ze skladu' AFTER `id_packings`,
    ADD CONSTRAINT `FK_wraps_packings` FOREIGN KEY (`id_packings`) REFERENCES `packings` (`id`),
    ADD CONSTRAINT `FK_wraps_parts` FOREIGN KEY (`id_parts`) REFERENCES `parts` (`id`),
    ADD CONSTRAINT `FK_wraps_wraps` FOREIGN KEY (`id_wraps`) REFERENCES `wraps` (`id`);

ALTER TABLE `packings`
    ALTER `id_invoice` DROP DEFAULT;
ALTER TABLE `packings`
    CHANGE COLUMN `id_invoice` `id_orders` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na zakazku' AFTER `id`,
    CHANGE COLUMN `weight_netto` `weight_netto` DECIMAL(10,3) UNSIGNED NULL DEFAULT NULL COMMENT 'celkova cista vaha zbozi' AFTER `id_orders`,
    CHANGE COLUMN `date_issue` `date_issue` DATE NULL DEFAULT NULL COMMENT 'datum vystaveni' AFTER `weight_brutto`,
    DROP INDEX `id_invoice`,
    ADD INDEX `id_orders` (`id_orders`),
    ADD CONSTRAINT `FK_packings_orders` FOREIGN KEY (`id_orders`) REFERENCES `orders` (`id`);
ALTER TABLE `packings`
    ADD COLUMN `type` ENUM('box','pallete') NOT NULL COMMENT 'typ' AFTER `id_orders`,
    ADD COLUMN `width` DECIMAL(10,0) UNSIGNED NOT NULL COMMENT 'sirka' AFTER `type`,
    ADD COLUMN `depth` DECIMAL(10,0) UNSIGNED NOT NULL COMMENT 'hloubka' AFTER `width`,
    ADD COLUMN `height` DECIMAL(10,0) UNSIGNED NOT NULL COMMENT 'vyska' AFTER `depth`,
    CHANGE COLUMN `weight_netto` `weight_netto` DECIMAL(10,3) UNSIGNED NULL DEFAULT NULL COMMENT 'celkova cista vaha zbozi' AFTER `height`,
    CHANGE COLUMN `weight_brutto` `weight_brutto` DECIMAL(10,3) UNSIGNED NOT NULL DEFAULT 0.000 COMMENT 'celkova vaha' AFTER `weight_netto`,
    CHANGE COLUMN `date_issue` `date_issue` DATE NULL DEFAULT NULL COMMENT 'datum vystaveni' AFTER `weight_brutto`;

--
-- 2020-01-21 #223: UPC - Odber suroveho tabaku
--
CREATE TABLE `macerations_parts_warehouse_history`
(
    `id`                         INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_macerations`             INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na maceraci',
    `id_parts_warehouse_history` INT(11)          NOT NULL COMMENT 'cizi klic na historii skladu',
    PRIMARY KEY (`id`),
    INDEX `id_macerations` (`id_macerations`),
    INDEX `id_parts_warehouse_history` (`id_parts_warehouse_history`),
    CONSTRAINT `FK__macerations` FOREIGN KEY (`id_macerations`) REFERENCES `macerations` (`id`),
    CONSTRAINT `FK__parts_warehouse_extern_history` FOREIGN KEY (`id_parts_warehouse_history`) REFERENCES `parts_warehouse_history` (`id`)
)
    COMMENT ='soucasti vstupujici do vyroby - propojovaci tabulka'
    COLLATE = 'utf8_czech_ci'
    ENGINE = InnoDB;

INSERT INTO users_acl (role, resource)
SELECT role, 'tobacco/ajax-maceration-info' FROM users_acl WHERE resource LIKE 'tobacco/parts-detail';


--
-- 2020-01-16 #221: UPC – Výpis odchylky poslední inventury na kartě macerace
--
UPDATE macerations_snapshot s SET s.is_fixed = 'n', s.id_macerations_history = NULL WHERE s.date = '2019-12-31';
DELETE FROM macerations_history WHERE last_change = '2019-12-31';
