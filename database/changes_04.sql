--
-- Toto je soubor pro databazove zmeny.
-- Zmeny vkladame na zacatek souboru - ne na konec (nejnovejsi zmeny jsou nahore).
-- Zmenu v tomto souboru nesmi obsahovat cele nazvy tabulek (ne db_name.db_table, jen db_table).
-- Kazda zmena databaze musi byt uvozena komentarem ve tvaru:
--
-- YYYY-MM-DD #123 - název issue z Eventum

--
-- 2019-11-22 #217 - Objednávka součástí - přidání času operací MD
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'orders/show-strategic-costs', 'Zobrazit naklady strategickych objednavek'),
('admin', 'orders/show-strategic-costs', NULL),
('manager', 'orders/show-strategic-costs', NULL),
('head_warehouseman', 'orders/show-strategic-costs', NULL);

--
-- 2019-11-15 #216 - Fakturace - dopravce a dodací adresa
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'invoice/load-shipment-from-order', 'nahrani dodaci adresy z objednavky'
FROM users_acl WHERE resource LIKE 'invoice/prepare';

--
-- 2019-11-15 #215 - Zadavani kritickeho mnozstvi
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco/warehouse-ajax-set-critical-amount', 'ajax nastaveni kritickeho mnozstvi' AS description
FROM users_acl WHERE resource LIKE 'tobacco/parts-edit';
UPDATE parts p SET p.measure = 'weight'
WHERE p.id_wh_type = 2 AND p.id_parts_ctg IN (34, 35, 40, 41, 42) AND p.measure != 'weight'

--
-- 2019-09-19 #94 - Chybné zobrazení fakturované částky
--
ALTER TABLE `invoice`
    ADD COLUMN `valid` ENUM('y','n') NOT NULL DEFAULT 'y'
        COMMENT 'je faktura validni vuci objednavce' AFTER `registered`;

--
-- 2019-07-18 #194 - Alveno napojeni
--
INSERT INTO `settings` (`name`, `value`) VALUES
    ('dsi_meduse_last', '20190619142804'),
    ('psi_medite_last', '20190628183514'),
    ('psi_meduse_vyroba_last', '20190628183514');

--
-- 2019-05-30 #194 - Odstranění transferových cen
--
ALTER TABLE `orders_products` DROP COLUMN `transfer_amount`;

--
-- 2019-05-24 #194 - Odstranění transferových cen
--
ALTER TABLE `customers` DROP COLUMN `transfer_perc`;

--
-- 2019-05-15 #192 - Editace názvu objednávky
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'order-edit/change-order-name', 'Menit rucne nazvy objednavek'),
('admin', 'order-edit/change-order-name', NULL),
('manager', 'order-edit/change-order-name', NULL);

--
-- 2019-05-14 #188 - Nová kolekce SeaBlooom
--
ALTER TABLE `collections`
    COMMENT='Číselník kolekcí součástí (produktů).',
    ADD COLUMN `abbr` VARCHAR(20) NOT NULL COMMENT 'zkratka kolekce' AFTER `name`,
    CHANGE COLUMN `abbr` `code` VARCHAR(2) NOT NULL DEFAULT '' COMMENT 'kod kolekce použitý v hologramech' COLLATE 'utf8_czech_ci' AFTER `abbr`;
ALTER TABLE `collections`
    ALTER `code` DROP DEFAULT;
ALTER TABLE `collections`
    CHANGE COLUMN `code` `code` VARCHAR(2) NOT NULL COMMENT 'kod kolekce použitý v hologramech' COLLATE 'utf8_czech_ci' AFTER `abbr`,
    ADD COLUMN `is_pipes` ENUM('y','n') NOT NULL DEFAULT 'y' COMMENT 'jedná se o kolekci dýmek?' AFTER `code`;
UPDATE `collections` SET `is_pipes`='n' WHERE  `id`=6;
UPDATE collections c SET c.abbr = TRIM(LEFT(c.`name`, LENGTH(c.`name`)-10)) WHERE c.id <> 6;

--
-- 2019-04-03 #180 - Nova fakturacni adresa
--
UPDATE addresses
SET street = 'Vintrovna', pop_number = '199', orient_number = '9', city = 'Popůvky', zip = '664 41'
WHERE id = 1;

--
-- 2019-01-17 #176 - Upravy pro celniky
--
ALTER TABLE `macerations_history`
	CHANGE COLUMN `amount` `amount` DECIMAL(9,3) NOT NULL DEFAULT '0.000' COMMENT 'velikost zmeny' AFTER `id_users`;

--
-- 2019-01-16 #177 - Naklady na objednavky
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'producers/parts-show-sums', 'Zobrazeni sumy nakladu na porizeni a na operace soucasti z objednavek'),
('admin', 'producers/parts-show-sums', NULL),
('manager', 'producers/parts-show-sums', NULL);

--
-- 2018-12-17 #167 - Prijemky pro suroviny
--
ALTER TABLE `productions`
	ADD COLUMN `id_wh_type` INT(11) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'oznaceni WH' AFTER `id`;
ALTER TABLE `productions_deliveries`
	ADD COLUMN `invoice_id` VARCHAR(50) NULL COMMENT 'oznaceni dodavatelske faktury' AFTER `description`;
INSERT INTO users_acl (role, resource) SELECT role, 'tobacco/productions' FROM users_acl WHERE resource LIKE 'tobacco/add-item';
INSERT INTO users_acl (role, resource) SELECT role, 'tobacco/productions-add' FROM users_acl WHERE resource LIKE 'tobacco/add-item';
INSERT INTO users_acl (role, resource) SELECT role, 'tobacco/productions-save' FROM users_acl WHERE resource LIKE 'tobacco/add-item';
INSERT INTO users_acl (role, resource) SELECT role, 'tobacco/productions-detail' FROM users_acl WHERE resource LIKE 'tobacco/add-item';
INSERT INTO users_acl (role, resource) SELECT role, 'tobacco/productions-delivery' FROM users_acl WHERE resource LIKE 'tobacco/add-item';
ALTER TABLE `productions_deliveries`
  CHANGE COLUMN `description` `description` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_czech_ci' AFTER `delivery_date`;

--
-- 2018-12-10 #129 - Exportni prodeje
--
UPDATE invoice i
	JOIN orders o ON i.id_orders = o.id
	LEFT JOIN addresses a ON o.id_addresses = a.id
SET i.export = (IF(a.country = 'CZ', 'n', 'y'))
WHERE i.id_wh_type = 2 AND i.export IS NULL;

--
-- 2018-09-14 #129 - Exportni prodeje
--
ALTER TABLE `invoice`
	ADD COLUMN `export` ENUM('y','n') NULL DEFAULT NULL COMMENT 'jedna se o exportni prodej' AFTER `show_excise`;


--
-- 2018-11-14 #33 - Nastavení doby odhlášení
--
ALTER TABLE `users`
	ADD COLUMN `expiration` SMALLINT UNSIGNED NOT NULL DEFAULT '60' COMMENT 'doba odhlaseni v minutach' AFTER `description`;

--
-- 2018-11-02 #163 - Expoprt ceniku pro eshop
--
ALTER TABLE `pricelists`
	ADD COLUMN `export` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'exportovat' AFTER `default`;

--
-- 2018-10-12 Palestina
--
INSERT INTO `c_country` (`code`, `name`, `inserted`) VALUES ('PS', 'Palestine', '2018-10-12 14:44:00');

--
-- 2018-10-16 #162 Výjimka DPH
--
ALTER TABLE `customers`
	ADD COLUMN `vat_invoice` ENUM('y','n','a') NOT NULL DEFAULT 'a'
	COMMENT 'vycislovat dan nebo automaticky dle adresy' AFTER `vat_checked`;

--
-- 2018-09-06 #154 - Vkladani objednavek z mailu
--
UPDATE parts SET sku = 'TO_ALPHONSO_10' WHERE id = 'TK010037'; -- STIGMA (Alphonso) 10g
UPDATE parts SET sku = 'TO_ALPHONSO_250' WHERE id = 'TK250059'; -- STIGMA (Alphonso) 250g
UPDATE parts SET sku = 'TO_ALPHONSO_50' WHERE id = 'TK050037'; -- STIGMA (Alphonso) 50g
UPDATE parts SET sku = 'TO_ANA_10' WHERE id = 'TK010028'; -- GAMA (Ananas) 10g
UPDATE parts SET sku = 'TO_ANA_250' WHERE id = 'TK250035'; -- GAMA (Ananas) 250g
UPDATE parts SET sku = 'TO_ANA_50' WHERE id = 'TK050017'; -- GAMA (Ananas) 50g
UPDATE parts SET sku = 'TO_ANN_10' WHERE id = 'TK010043'; -- 5. ANNIVERSARY (Marakuja, Jahoda, Švestka) 10g
UPDATE parts SET sku = 'TO_ANN_250' WHERE id = 'TK250066'; -- 5. ANNIVERSARY (Marakuja, Jahoda, Švestka) 250g
UPDATE parts SET sku = 'TO_ANN_50' WHERE id = 'TK050043'; -- 5. ANNIVERSARY (Marakuja, Jahoda, Švestka) 50g
UPDATE parts SET sku = 'TO_APPLE_10' WHERE id = 'TK010038'; -- ALFA (Jablko) 10g
UPDATE parts SET sku = 'TO_APPLE_250' WHERE id = 'TK250060'; -- ALFA (Jablko) 250g
UPDATE parts SET sku = 'TO_APPLE_50' WHERE id = 'TK050038'; -- ALFA (Jablko) 50g
UPDATE parts SET sku = 'TO_BAN_10' WHERE id = 'TK010003'; -- KOPPA (Banán) 10g
UPDATE parts SET sku = 'TO_BAN_250' WHERE id = 'TK250003'; -- KOPPA (Banán) 250g
UPDATE parts SET sku = 'TO_BAN_50' WHERE id = 'TK050003'; -- KOPPA (Banán) 50g
UPDATE parts SET sku = 'TO_BRO_10' WHERE id = 'TK010014'; -- CHÍ (Broskev) 10g
UPDATE parts SET sku = 'TO_BRO_250' WHERE id = 'TK250014'; -- CHÍ (Broskev) 250g
UPDATE parts SET sku = 'TO_BRO_50' WHERE id = 'TK050014'; -- CHÍ (Broskev) 50g
UPDATE parts SET sku = 'TO_CHA_10' WHERE id = 'TK010035'; -- FÍ PSÍ GAMA (Cantaloupe, Hrozen, Ananas) 10g
UPDATE parts SET sku = 'TO_CHA_250' WHERE id = 'TK250057'; -- FÍ PSÍ GAMA (Cantaloupe, Hrozen, Ananas) 250g
UPDATE parts SET sku = 'TO_CHA_50' WHERE id = 'TK050035'; -- FÍ PSÍ GAMA (Cantaloupe, Hrozen, Ananas) 50g
UPDATE parts SET sku = 'TO_CHO_10' WHERE id = 'TK010025'; -- THÉTA OMEGA (Čokoláda, Pomeranč) 10g
UPDATE parts SET sku = 'TO_CHO_250' WHERE id = 'TK250054'; -- THÉTA OMEGA (Čokoláda, Pomeranč) 250g
UPDATE parts SET sku = 'TO_CHO_50' WHERE id = 'TK050036'; -- THÉTA OMEGA (Čokoláda, Pomeranč) 50g
UPDATE parts SET sku = 'TO_CIT_10' WHERE id = 'TK010015'; -- YPSILON (Citrón) 10g
UPDATE parts SET sku = 'TO_CIT_250' WHERE id = 'TK250015'; -- YPSILON (Citrón) 250g
UPDATE parts SET sku = 'TO_CIT_50' WHERE id = 'TK050015'; -- YPSILON (Citrón) 50g
UPDATE parts SET sku = 'TO_COK_10' WHERE id = 'TK010023'; -- THÉTA (Čokoláda) 10g
UPDATE parts SET sku = 'TO_COK_250' WHERE id = 'TK250018'; -- THÉTA (Čokoláda) 250g
UPDATE parts SET sku = 'TO_COK_50' WHERE id = 'TK050024'; -- THÉTA (Čokoláda) 50g
UPDATE parts SET sku = 'TO_DVJ_10' WHERE id = 'TK010006'; -- BETA (Dvojité jablko) 10g
UPDATE parts SET sku = 'TO_DVJ_250' WHERE id = 'TK250006'; -- BETA (Dvojité jablko) 250g
UPDATE parts SET sku = 'TO_DVJ_50' WHERE id = 'TK050006'; -- BETA (Dvojité jablko) 50g
UPDATE parts SET sku = 'TO_EUC_10' WHERE id = 'TK010032'; -- ZETA (Eukalyptus) 10g
UPDATE parts SET sku = 'TO_EUC_250' WHERE id = 'TK250053'; -- ZETA (Eukalyptus) 250g
UPDATE parts SET sku = 'TO_EUC_50' WHERE id = 'TK050022'; -- ZETA (Eukalyptus) 50g
UPDATE parts SET sku = 'TO_EXP_10' WHERE id = 'TK010021'; -- PÍ (Hruška) 10g
UPDATE parts SET sku = 'TO_EXP_250' WHERE id = 'TK250016'; -- PÍ (Hruška) 250g
UPDATE parts SET sku = 'TO_EXP_50' WHERE id = 'TK050016'; -- PÍ (Hruška) 50g
UPDATE parts SET sku = 'TO_FCC_10' WHERE id = 'TK010018'; -- FÍ KAPPA (Cantaloupe, Kokos) 10g
UPDATE parts SET sku = 'TO_FCC_250' WHERE id = 'TK250022'; -- FÍ KAPPA (Cantaloupe, Kokos) 250g
UPDATE parts SET sku = 'TO_FCC_50' WHERE id = 'TK050027'; -- FÍ KAPPA (Cantaloupe, Kokos) 50g
UPDATE parts SET sku = 'TO_FGM_10' WHERE id = 'TK010016'; -- PSÍ MÍ (Hrozen+Máta) 10g
UPDATE parts SET sku = 'TO_FGM_250' WHERE id = 'TK250037'; -- PSÍ MÍ (Hrozen, Máta) 250g
UPDATE parts SET sku = 'TO_FGM_50' WHERE id = 'TK050028'; -- PSÍ MÍ (Hrozen, Máta) 50g
UPDATE parts SET sku = 'TO_FPSB_10' WHERE id = 'TK010030'; -- CHÍ RÓ KOPPA (Broskev, Jahoda, Banán) 10g
UPDATE parts SET sku = 'TO_FPSB_250' WHERE id = 'TK250031'; -- CHÍ RÓ KOPPA (Broskev, Jahoda, Banán) 250g
UPDATE parts SET sku = 'TO_FPSB_50' WHERE id = 'TK050033'; -- CHÍ RÓ KOPPA (Broskev, Jahoda, Banán) 50g
UPDATE parts SET sku = 'TO_GUA_10' WHERE id = 'TK010029'; -- SAN (Guava) 10g
UPDATE parts SET sku = 'TO_GUA_250' WHERE id = 'TK250032'; -- SAN (Guava) 250g
UPDATE parts SET sku = 'TO_GUA_50' WHERE id = 'TK050018'; -- SAN (Guava) 50g
UPDATE parts SET sku = 'TO_HAZEL_10' WHERE id = 'TK010036'; -- TAU (Lískový oříšek) 10g
UPDATE parts SET sku = 'TO_HAZEL_250' WHERE id = 'TK250056'; -- TAU (Lískový oříšek) 250g
UPDATE parts SET sku = 'TO_HAZEL_50' WHERE id = 'TK050026'; -- TAU (Lískový oříšek) 50g
UPDATE parts SET sku = 'TO_HRT_10' WHERE id = 'TK010007'; -- PSÍ (Hrozen) 10g
UPDATE parts SET sku = 'TO_HRT_250' WHERE id = 'TK250007'; -- PSÍ (Hrozen) 250g
UPDATE parts SET sku = 'TO_HRT_50' WHERE id = 'TK050007'; -- PSÍ (Hrozen) 50g
UPDATE parts SET sku = 'TO_HYM_10' WHERE id = 'TK010042'; -- HIMALAYA (Masala, Pryskyřice, Citrusové plody) 10g
UPDATE parts SET sku = 'TO_HYM_250' WHERE id = 'TK250065'; -- HIMALAYA (Masala, Pryskyřice, Citrusové plody) 250g
UPDATE parts SET sku = 'TO_HYM_50' WHERE id = 'TK050042'; -- HIMALAYA (Masala, Pryskyřice, Citrusové plody) 50g
UPDATE parts SET sku = 'TO_JAH_10' WHERE id = 'TK010001'; -- RÓ (Jahoda) 10g
UPDATE parts SET sku = 'TO_JAH_250' WHERE id = 'TK250001'; -- RÓ (Jahoda) 250g
UPDATE parts SET sku = 'TO_JAH_50' WHERE id = 'TK050001'; -- RÓ (Jahoda) 50g
UPDATE parts SET sku = 'TO_KOK_10' WHERE id = 'TK010012'; -- KAPPA (Kokos) 10g
UPDATE parts SET sku = 'TO_KOK_250' WHERE id = 'TK250012'; -- KAPPA (Kokos) 250g
UPDATE parts SET sku = 'TO_KOK_50' WHERE id = 'TK050012'; -- KAPPA (Kokos) 50g
UPDATE parts SET sku = 'TO_KW_10' WHERE id = 'TK010026'; -- DELTA (Kiwi) 10g
UPDATE parts SET sku = 'TO_KW_250' WHERE id = 'TK250033'; -- DELTA (Kiwi) 250g
UPDATE parts SET sku = 'TO_KW_50' WHERE id = 'TK050020'; -- DELTA (Kiwi) 50g
UPDATE parts SET sku = 'TO_LCM_10' WHERE id = 'TK010017'; -- EPSILON SIGMA MÍ (Třešeň, Limetka Máta) 10g
UPDATE parts SET sku = 'TO_LCM_250' WHERE id = 'TK250021'; -- EPSILON SIGMA MÍ (Třešeň, Limetka Máta)  250g
UPDATE parts SET sku = 'TO_LCM_50' WHERE id = 'TK050030'; -- EPSILON SIGMA MÍ (Třešeň, Limetka Máta)  50g
UPDATE parts SET sku = 'TO_LIM_10' WHERE id = 'TK010011'; -- EPSILON (Limetka) 10g
UPDATE parts SET sku = 'TO_LIM_250' WHERE id = 'TK250011'; -- EPSILON (Limetka) 250g
UPDATE parts SET sku = 'TO_LIM_50' WHERE id = 'TK050011'; -- EPSILON (Limetka) 50g
UPDATE parts SET sku = 'TO_MAN_10' WHERE id = 'TK010010'; -- IÓTA (Mango) 10g
UPDATE parts SET sku = 'TO_MAN_250' WHERE id = 'TK250010'; -- IÓTA (Mango) 250g
UPDATE parts SET sku = 'TO_MAN_50' WHERE id = 'TK050010'; -- IÓTA (Mango) 50g
UPDATE parts SET sku = 'TO_MAR_10' WHERE id = 'TK010008'; -- KSÍ (Marakuja) 10g
UPDATE parts SET sku = 'TO_MAR_250' WHERE id = 'TK250008'; -- KSÍ (Marakuja) 250g
UPDATE parts SET sku = 'TO_MAR_50' WHERE id = 'TK050008'; -- KSÍ (Marakuja) 50g
UPDATE parts SET sku = 'TO_MAT_10' WHERE id = 'TK010002'; -- MÍ (Máta) 10g
UPDATE parts SET sku = 'TO_MAT_250' WHERE id = 'TK250002'; -- Mí (Máta) 250g
UPDATE parts SET sku = 'TO_MAT_50' WHERE id = 'TK050002'; -- MÍ (Máta) 50g
UPDATE parts SET sku = 'TO_OG_10' WHERE id = 'TK010024'; -- OMEGA PSÍ (Pomeranč, Hrozen) 10g
UPDATE parts SET sku = 'TO_OG_250' WHERE id = 'TK250019'; -- OMEGA PSÍ (Pomeranč, Hrozen) 250g
UPDATE parts SET sku = 'TO_OG_50' WHERE id = 'TK050031'; -- OMEGA PSÍ (Pomeranč, Hrozen) 50g
UPDATE parts SET sku = 'TO_PFM_10' WHERE id = 'TK010020'; -- KSÍ MÍ (Marakuja, Máta) 10g
UPDATE parts SET sku = 'TO_PFM_250' WHERE id = 'TK250055'; -- KSÍ MÍ (Marakuja, Máta) 250g
UPDATE parts SET sku = 'TO_PFM_50' WHERE id = 'TK050032'; -- KSÍ MÍ (Marakuja, Máta) 50g
UPDATE parts SET sku = 'TO_PL_10' WHERE id = 'TK010027'; -- LAMBDA (Švestka) 10g
UPDATE parts SET sku = 'TO_PL_250' WHERE id = 'TK250034'; -- LAMBDA (Švestka) 250g
UPDATE parts SET sku = 'TO_PL_50' WHERE id = 'TK050019'; -- LAMBDA (Švestka) 50g
UPDATE parts SET sku = 'TO_POM_10' WHERE id = 'TK010013'; -- OMEGA (Pomeranč) 10g
UPDATE parts SET sku = 'TO_POM_250' WHERE id = 'TK250013'; -- OMEGA (Pomeranč) 250g
UPDATE parts SET sku = 'TO_POM_50' WHERE id = 'TK050013'; -- OMEGA (Pomeranč) 50g
UPDATE parts SET sku = 'TO_ROSE_10' WHERE id = 'TK010034'; -- OMIKRON (Růže) 10g
UPDATE parts SET sku = 'TO_ROSE_250' WHERE id = 'TK250061'; -- OMIKRON (Růže) 250g
UPDATE parts SET sku = 'TO_ROSE_50' WHERE id = 'TK050025'; -- OMIKRON (Růže) 50g
UPDATE parts SET sku = 'TO_SA_10' WHERE id = 'TK010040'; -- SAMPÍ (Grapefruit) 10g
UPDATE parts SET sku = 'TO_SA_250' WHERE id = 'TK250063'; -- SAMPÍ (Grapefruit) 250g
UPDATE parts SET sku = 'TO_SA_50' WHERE id = 'TK050040'; -- SAMPÍ (Grapefruit) 50g
UPDATE parts SET sku = 'TO_SKO_10' WHERE id = 'TK010041'; -- HETA (Skořice) 10g
UPDATE parts SET sku = 'TO_SKO_250' WHERE id = 'TK250064'; -- HETA (Skořice) 250g
UPDATE parts SET sku = 'TO_SKO_50' WHERE id = 'TK050041'; -- HETA (Skořice) 50g
UPDATE parts SET sku = 'TO_ŠV_LIM_10' WHERE id = 'TK010033'; -- LAMBDA EPSILON (Švestka, Limeta) 10g
UPDATE parts SET sku = 'TO_ŠV_LIM_250' WHERE id = 'TK250041'; -- LAMBDA EPSILON (Švestka, Limeta) 250g
UPDATE parts SET sku = 'TO_ŠV_LIM_50' WHERE id = 'TK050034'; -- LAMBDA EPSILON (Švestka, Limeta) 50g
UPDATE parts SET sku = 'TO_VAN_10' WHERE id = 'TK010031'; -- DIGMA (Vanilka) 10g
UPDATE parts SET sku = 'TO_VAN_250' WHERE id = 'TK250040'; -- DIGMA (Vanilka) 250g
UPDATE parts SET sku = 'TO_VAN_50' WHERE id = 'TK050021'; -- DIGMA (Vanilka) 50g
UPDATE parts SET sku = 'TO_VIS_10' WHERE id = 'TK010004'; -- SIGMA (Višeň) 10g
UPDATE parts SET sku = 'TO_VIS_250' WHERE id = 'TK250004'; -- SIGMA (Višeň) 250g
UPDATE parts SET sku = 'TO_VIS_50' WHERE id = 'TK050004'; -- SIGMA (Višeň) 50g
UPDATE parts SET sku = 'TO_WM_10' WHERE id = 'TK010022'; -- ÉTA (Vodní meloun) 10g
UPDATE parts SET sku = 'TO_WM_250' WHERE id = 'TK150016'; -- ÉTA (Vodní meloun) 250g
UPDATE parts SET sku = 'TO_WM_50' WHERE id = 'TK250017'; -- ÉTA (Vodní meloun) 50g
UPDATE parts SET sku = 'TO_WS_10' WHERE id = 'TK010039'; -- WINTER SEASON (Švestka, Jablko, Anýz, Skořice) 10g
UPDATE parts SET sku = 'TO_WS_250' WHERE id = 'TK250062'; -- WINTER SEASON (Švestka, Jablko, Anýz, Skořice) 250g
UPDATE parts SET sku = 'TO_WS_50' WHERE id = 'TK050039'; -- WINTER SEASON (Švestka, Jablko, Anýz, Skořice) 50g
UPDATE parts SET sku = 'TO_ZLM_10' WHERE id = 'TK010009'; -- FÍ (Cantaloupe) 10g
UPDATE parts SET sku = 'TO_ZLM_250' WHERE id = 'TK250009'; -- FÍ (Cantaloupe) 250g
UPDATE parts SET sku = 'TO_ZLM_50' WHERE id = 'TK050009'; -- FÍ (Cantaloupe) 50g

--
-- 2018-09-03 #154 - Vkladani objednavek z mailu
--
INSERT INTO users_acl (role, resource)
SELECT role, 'tobacco-orders/new-from-clipboard' FROM users_acl
WHERE resource LIKE 'tobacco-order-edit/index';

--
-- 2018-08-30 #154 - Vkladani objednavek z mailu
--
ALTER TABLE `parts`
	ADD COLUMN `sku` VARCHAR(50) NULL DEFAULT NULL COMMENT 'id z eshopu' AFTER `hs`;

--
-- 2018-06-29 #147 Budget pro B2B
--
INSERT INTO users_acl (role, resource, description) VALUES
  (NULL, 'customers/budget-save', 'Vlozeni/editace budgetu zakaznika'),
  ('admin', 'customers/budget-save', NULL),
  ('manager', 'customers/budget-save', NULL),
  ('seller', 'customers/budget-save', NULL);

--
-- 2018-06-29 #147 Budget pro B2B
--
CREATE TABLE `customers_budgets` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_customers` INT(11) NOT NULL DEFAULT '0',
	`contract_date` DATE NOT NULL COMMENT 'datum pocatku smlouvy',
	`contract_length` TINYINT(3) UNSIGNED NOT NULL COMMENT 'doba trvani smlouvy v letech',
	PRIMARY KEY (`id`),
	INDEX `FK_customers_budges_customers` (`id_customers`),
	CONSTRAINT `FK_customers_budges_customers` FOREIGN KEY (`id_customers`) REFERENCES `customers` (`id`)
)
	COMMENT='smluvni budgety b2b zakazniku'
	COLLATE='utf8_czech_ci'
	ENGINE=InnoDB
;
CREATE TABLE `customers_budgets_items` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customers_budgets` INT(11) UNSIGNED NOT NULL,
  `delta` YEAR NOT NULL COMMENT 'obdobi v ramci budgetu',
  `products_amount` DECIMAL(12,5) UNSIGNED NOT NULL DEFAULT '0.00000' COMMENT 'vyse budgetu pro produkty',
  `products_spent` DECIMAL(12,5) UNSIGNED NOT NULL DEFAULT '0.00000' COMMENT 'vyse cerpani produktu',
  `services_amount` DECIMAL(12,5) UNSIGNED NOT NULL DEFAULT '0.00000' COMMENT 'vyse budgetu pro sluzby',
  `services_spent` DECIMAL(12,5) UNSIGNED NOT NULL DEFAULT '0.00000' COMMENT 'vyse cerpani sluzeb',
  PRIMARY KEY (`id`),
  INDEX `FK_customers_budgets_items_customers_budgets` (`id_customers_budgets`),
  CONSTRAINT `FK_customers_budgets_items_customers_budgets` FOREIGN KEY (`id_customers_budgets`) REFERENCES `customers_budgets` (`id`)
)
  COMMENT='jednotliva obdobi plneni smluvniho budgetu '
  COLLATE='utf8_czech_ci'
  ENGINE=InnoDB
;

--
-- 2018-06-26 #148 Uprava fakturace
--
ALTER TABLE `invoice` ADD `use_actual_rate` ENUM('y','n') NOT NULL DEFAULT 'y' COMMENT 'pouzivat aktualni kurz z cnb' AFTER `rate`;

--
-- 2018-06-26 #146 Editace objednavky
--
ALTER TABLE `orders`
	ADD COLUMN `queue` VARCHAR(13) NULL DEFAULT NULL COMMENT 'id email fronty' AFTER `last_change`;


--
-- 2018-06-21 #145 Prehled predbeznych objednavek
--
ALTER TABLE `orders`
	ADD COLUMN `date_deadline_internal` DATE NULL DEFAULT NULL COMMENT 'interni deadline' AFTER `date_deadline`;

--
-- 2018-06-12 #142 Faktura - odpocet zalohy
--
ALTER TABLE `payments`
	ADD COLUMN `deduct_from` INT(11) NULL DEFAULT NULL COMMENT 'z jake faktury se ma castka odecist' AFTER `id_invoice`,
	ADD CONSTRAINT `FK_payments_invoice_2` FOREIGN KEY (`deduct_from`) REFERENCES `invoice` (`id`);
INSERT INTO users_acl (resource, role)
	SELECT 'invoice/ajax-save-deduct', role FROM users_acl
	WHERE resource = 'invoice/payment-edit';
INSERT INTO users_acl (resource, role)
	SELECT 'tobacco-invoice/ajax-save-deduct', role FROM users_acl
	WHERE resource = 'tobacco-invoice/payment-edit';

--
-- 2018-06-07 #136 - Overeni DIC
--
ALTER TABLE `customers`
	ADD COLUMN `vat_checked` ENUM('y','n') NULL DEFAULT NULL COMMENT 'overeni dic' AFTER `vat_no`;
INSERT INTO users_acl (resource, role)
	SELECT 'customers/ajax-verify-vat', role FROM users_acl
	WHERE resource = 'customers/edit';
INSERT INTO users_acl (resource, role)
	SELECT 'tobacco-customers/ajax-verify-vat', role FROM users_acl
	WHERE resource = 'tobacco-customers/edit';

--
-- 2018-04-12 #124 - Pridat kos do WH tabaku
--
INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'tobacco/parts-trash', 'Vypis smazanych soucasti'),
	('admin', 'tobacco/parts-trash', NULL),
	('manager', 'tobacco/parts-trash', NULL),
	('tobacco_whman', 'tobacco/parts-trash', NULL);

--
-- 2018-03-23 #102 - Kontrola ID
--
INSERT INTO users_acl (role, resource, description) VALUES
	('seller', 'parts/ajax-check-part-id', NULL),
	('tobacco_whman', 'parts/ajax-check-part-id', NULL),
	('tobacco_seller', 'parts/ajax-check-part-id', NULL),
	('sales_director', 'parts/ajax-check-part-id', NULL);

--
-- 2018-03-12 #104 - Výmaz faktury
--
INSERT INTO users_acl (role, resource, description) VALUES
	('tobacco_seller', 'tobacco-invoice/delete', NULL),
	('seller', 'invoice/delete', NULL),
	('sales_director', 'invoice/delete', NULL);

INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'tobacco-invoice/delete-regular', 'smazani ostre faktury'),
	('admin', 'tobacco-invoice/delete-regular', NULL),
	('manager', 'tobacco-invoice/delete-regular', NULL);

INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'invoice/delete-regular', 'smazani ostre faktury'),
	('admin', 'invoice/delete-regular', NULL),
	('manager', 'invoice/delete-regular', NULL);

--
-- 2018-01-11 #95 - Datum splatnosti
--
INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'invoice/overdue', 'prekroceni maximalni doby splatnosti'),
	('admin', 'invoice/overdue', NULL),
	('manager', 'invoice/overdue', NULL);
INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'tobacco-invoice/overdue', 'prekroceni maximalni doby splatnosti'),
	('admin', 'tobacco-invoice/overdue', NULL),
	('manager', 'tobacco-invoice/overdue', NULL);

--
-- 2018-02-27 #97 - Způsob platby PayPal
--
ALTER TABLE `orders`
	CHANGE COLUMN `payment_method` `payment_method`
	ENUM('cash_on_delivery','cash','card','transfer','free_presentation','free_claim','paypal')
	NULL DEFAULT NULL COMMENT 'zpusob platby: na dobirku, v hotovosti, prevodem'
COLLATE 'utf8_czech_ci' AFTER `proforma_no`;
ALTER TABLE `accounts`
  ADD COLUMN `payment_method` ENUM('cash_on_delivery','cash','card','transfer','free_presentation','free_claim','paypal') NULL DEFAULT NULL COMMENT 'svazana platebni metoda' AFTER `is_default`;


--
-- 2017-12-28 #80 - Report materialove cenotvorby
--
INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'export/products-costs', 'export materialove cenotvorby'),
	('admin', 'export/products-costs', NULL),
	('manager', 'export/products-costs', NULL);

--
-- 2018-01-02 #91 - Ciselne rady faktur 2018
--
ALTER TABLE `orders`
	CHANGE COLUMN `proforma_no` `proforma_no` VARCHAR(7) NULL DEFAULT NULL COMMENT 'cislo faktury' AFTER `purchaser`;

--
-- 2017-11-20 #72 - Skrýt obraty v reportech prodejů tabáku
--
INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'tobacco-reports/show-prices', 'zobrazit obraty v reportu prodeju'),
	('admin', 'tobacco-reports/show-prices', NULL),
	('manager', 'tobacco-reports/show-prices', NULL);

--
-- 2017-11-20 #73 - Report pro celniky
--
INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'tobacco-reports/duty-download', 'reporting pro celni spravu - stazeni souboru s exportem'),
	('admin', 'tobacco-reports/duty-download', NULL),
	('manager', 'tobacco-reports/duty-download', NULL),
	('tobacco_whman', 'tobacco-reports/duty-download', NULL);

INSERT INTO users_acl (role, resource, description) VALUES
  (NULL, 'tobacco-reports/duty-ajax-products', 'reporting pro celni spravu - ajax call'),
  ('admin', 'tobacco-reports/duty-ajax-products', NULL),
  ('manager', 'tobacco-reports/duty-ajax-products', NULL),
  ('tobacco_whman', 'tobacco-reports/duty-ajax-products', NULL);

--
-- 2017-11-14 #73 - Report pro celniky
--
ALTER TABLE `producers`
	ADD COLUMN `ident_no` VARCHAR(100) NULL DEFAULT NULL COMMENT 'ic dodavatele' AFTER `name`;
UPDATE producers SET ident_no = "47114983" WHERE id = 328;
UPDATE producers SET ident_no = "49689401" WHERE id = 332;
UPDATE producers SET ident_no = "25683446" WHERE id = 333;
UPDATE producers SET ident_no = "25683446" WHERE id = 334;
UPDATE producers SET ident_no = "25754602" WHERE id = 335;
UPDATE producers SET ident_no = "27568962" WHERE id = 336;
UPDATE producers SET ident_no = "25605666" WHERE id = 337;
UPDATE producers SET ident_no = "25605666" WHERE id = 338;
UPDATE producers SET ident_no = "25597086" WHERE id = 339;
UPDATE producers SET ident_no = "48035131" WHERE id = 340;
UPDATE producers SET ident_no = "48035131" WHERE id = 341;
UPDATE producers SET ident_no = "62908227" WHERE id = 342;
UPDATE producers SET ident_no = "28543190" WHERE id = 343;
UPDATE producers SET ident_no = "24263982" WHERE id = 344;
UPDATE producers SET ident_no = "25194798" WHERE id = 357;



--
-- 2017-12-08 #88 - Mazani proforma faktur
--
INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'tobacco-invoice/delete', 'smazani faktury'),
	('admin', 'tobacco-invoice/delete', NULL),
	('manager', 'tobacco-invoice/delete', NULL);

INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'invoice/delete', 'smazani faktury'),
	('admin', 'invoice/delete', NULL),
	('manager', 'invoice/delete', NULL);
--
-- 2017-10-20 #59 - Zalozeni noveho zakaznika
--
ALTER TABLE `customers`
	CHANGE COLUMN `mobile` `mobile` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_czech_ci' AFTER `phone`;

--
-- 2017-10-19 #54 - Povoleni fakturace
--
INSERT INTO users_acl (role, resource, description) VALUES
	(NULL, 'tobacco-customers/set-allow-invoicing', 'nastavit povoleni fakturace'),
	('admin', 'tobacco-customers/set-allow-invoicing', NULL),
	('manager', 'tobacco-customers/set-allow-invoicing', NULL);

--
-- 2017-10-13 #54 - Povoleni fakturace
--
ALTER TABLE `customers`
	ADD COLUMN `allow_invoicing` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'dovolit prodej na fakturu dopredu' AFTER `discount_type`;
ALTER TABLE `customers`
	ADD COLUMN `due` INT(2) NULL DEFAULT NULL COMMENT 'pocet dnu splatnosti' AFTER `allow_invoicing`;
ALTER TABLE `orders_carriers`
	ADD COLUMN `is_pos` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'jedna se o dobirku' AFTER `is_active`;

--
-- 2017-09-22 #53 - Fixace receptur
--
UPDATE parts p LEFT JOIN parts_ctg c ON p.id_parts_ctg = c.id
SET p.id_parts_ctg = 41
WHERE p.id_wh_type = 2 AND p.id LIKE 'TSP%' AND (p.id_parts_ctg IS NULL OR p.id_parts_ctg = 42);

ALTER TABLE `recipes_parts`
  CHANGE COLUMN `ratio` `ratio` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'pomer slozky v procentech' AFTER `id_parts`;


--
-- 2017-09-08 #53 - Fixace receptur
--
CREATE TABLE `parts_flavors` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_parts` VARCHAR(10) NOT NULL COLLATE 'utf8_czech_ci',
	`id_batch_flavors` INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `id_parts` (`id_parts`),
	INDEX `id_batch_flavors` (`id_batch_flavors`),
	CONSTRAINT `FK__batch_flavors` FOREIGN KEY (`id_batch_flavors`) REFERENCES `batch_flavors` (`id`),
	CONSTRAINT `FK__parts` FOREIGN KEY (`id_parts`) REFERENCES `parts` (`id`)
)
	COLLATE='utf8_czech_ci'
	ENGINE=InnoDB
;

INSERT INTO parts_flavors (id_parts, id_batch_flavors)
  SELECT p.id AS id_parts, f.id AS id_batch_flavors FROM parts p
    INNER JOIN batch_flavors f ON f.name LIKE p.name COLLATE utf8_czech_ci
  WHERE p.id_wh_type = 2 AND id_parts_ctg IN (41, 42);



--
-- 2017-09-08 #53 - Fixace receptur
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'tobacco-settings/recipes-proportion', 'administrace pomeru surovin v receptu'),
('admin', 'tobacco-settings/recipes-proportion', NULL),
('manager', 'tobacco-settings/recipes-proportion', NULL);

--
-- 2017-08-02 #32 - Vyhledávání zákazníka
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-customers/ajax-search', description FROM  users_acl
WHERE resource = 'tobacco-customers/index';
INSERT INTO users_acl (role, resource, description)
SELECT role, 'customers/ajax-search', description FROM  users_acl
WHERE resource = 'customers/index';

--
-- 2017-08-01 #46 - Fakturace služeb pro sklad tabáku
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-order-edit/services', description FROM users_acl
WHERE resource LIKE 'tobacco-order-edit/index';

--
-- 2017-07-31 #29 - Soukromé objednávky
--
DELETE FROM users_acl WHERE role = 'tobacco_whman' AND resource = 'orders/view-private';

--
-- 2017-07-24 #29 - Soukromé objednávky
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'orders/view-private', 'zobrazovat cizi soukrome objednavky'),
('admin', 'orders/view-private', NULL),
('manager', 'orders/view-private', NULL),
('head_warehouseman', 'orders/view-private', NULL);
INSERT INTO users_acl (role, resource, description) VALUES
('accountant_pipes', 'orders/view-private', NULL);
INSERT INTO users_acl (role, resource, description) VALUES
('tobacco_whman', 'orders/view-private', NULL),
('accountant_tobacco', 'orders/view-private', NULL);

--
-- 2017-07-21 #29 - Soukromé objednávky
--
ALTER TABLE `orders`
	ADD COLUMN `private` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'priznak privatni objednavky' AFTER `type`;

INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'orders/create-private', 'vytvaret soukrome objednavky'),
('admin', 'orders/create-private', NULL),
('manager', 'orders/create-private', NULL);

--
-- 2017-07-20 #36 - Úprava faktur
--
INSERT INTO users_acl (role, resource)
SELECT role, 'invoice/unpaid' FROM users_acl WHERE resource LIKE 'invoice/index';
INSERT INTO users_acl (role, resource)
SELECT role, 'tobacco-invoice/unpaid' FROM users_acl WHERE resource LIKE 'tobacco-invoice/index';
--
-- 2017-06-23 #25 - Doklad o přijetí platby
--
ALTER TABLE `payments`
	ADD COLUMN `no` VARCHAR(12) NULL COMMENT 'cislo dokladu' COLLATE 'utf8_czech_ci' AFTER `id_invoice`,
	ADD COLUMN `id_wh_type` INT(11) NOT NULL DEFAULT '1' COMMENT 'id wh' AFTER `id_invoice`;

ALTER TABLE `invoice`
	CHANGE COLUMN `invoiced_total` `invoiced_total` DECIMAL(13,5) NOT NULL DEFAULT '0.00000' COMMENT 'castka na fakure' AFTER `invoice_price`,
	CHANGE COLUMN `rate` `rate` FLOAT NULL DEFAULT '27' AFTER `currency`;

--
-- 2017-06-21 #18 - Úhrada faktur pro účetní
--
ALTER TABLE `invoice`
	CHANGE COLUMN `invoice_price` `invoice_price` DECIMAL(10,2) NULL DEFAULT '0.00' COMMENT 'procento na fakture' AFTER `language`,
	ADD COLUMN `invoiced_total` DECIMAL(10,5) NOT NULL DEFAULT '0.00' COMMENT 'castka na fakure' AFTER `invoice_price`;

--
-- 2017-06-21 #25 - Doklad o přijetí platby
--
ALTER TABLE payments DROP FOREIGN KEY FK_payments_invoice;
ALTER TABLE `payments`
	ALTER `id_invoice` DROP DEFAULT;
ALTER TABLE `payments`
	CHANGE COLUMN `id_invoice` `id_invoice` INT(11) NOT NULL COMMENT 'vztah k fakture' AFTER `id`;
ALTER TABLE `payments` ADD CONSTRAINT `FK_payments_invoice`
FOREIGN KEY (`id_invoice`) REFERENCES `invoice`(`id`)
ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-invoice/payments', description FROM users_acl WHERE resource LIKE 'tobacco-invoice/index';
INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-invoice/payment-add', description FROM users_acl WHERE resource LIKE 'tobacco-invoice/index';
INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-invoice/payment-edit', description FROM users_acl WHERE resource LIKE 'tobacco-invoice/index';
INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-invoice/payment-get-recipe', description FROM users_acl WHERE resource LIKE 'tobacco-invoice/index';
INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-invoice/payment-remove', description FROM users_acl WHERE resource LIKE 'tobacco-invoice/index';

--
-- 2017-06-08 #25 - Doklad o přijetí platby
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'invoice/payment-get-recipe', description FROM users_acl WHERE resource LIKE 'invoice/index';
INSERT INTO users_acl (role, resource, description)
SELECT role, 'invoice/payment-remove', description FROM users_acl WHERE resource LIKE 'invoice/index';

CREATE TABLE `payments` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_invoice` INT(11) NULL DEFAULT NULL COMMENT 'vztah k fakture',
	`amount` DOUBLE(13,5) UNSIGNED NOT NULL COMMENT 'castka',
	`currency` ENUM('CZK','EUR') NOT NULL COMMENT 'mena' COLLATE 'utf8_czech_ci',
	`rate` FLOAT UNSIGNED NOT NULL COMMENT 'kurz vuci CZK',
	`date` DATE NOT NULL COMMENT 'datum platby',
	PRIMARY KEY (`id`),
	INDEX `FK_payments_invoice` (`id_invoice`),
	CONSTRAINT `FK_payments_invoice` FOREIGN KEY (`id_invoice`) REFERENCES `invoice` (`id`)
)
COMMENT='přijaté platby'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB
AUTO_INCREMENT=12
;

--
-- 2017-06-14 #28 - Rozšíření pravomoci účetní
--
INSERT INTO users_acl (role, resource) VALUES
('accountant_pipes', 'customers/index'),
('accountant_pipes', 'customers/edit'),
('accountant_pipes', 'customers/detail'),
('accountant_pipes', 'customers/address'),
('accountant_pipes', 'invoice/prepare'),
('accountant_pipes', 'invoice/generate'),
('accountant_pipes', 'invoice/create-credit-note'),
('accountant_pipes', 'invoice/add-custom-item'),
('accountant_pipes', 'invoice/remove-custom-item'),
('accountant_pipes', 'invoice/json-get-custom-item'),
('accountant_pipes', 'invoice/show-versions'),
('accountant_pipes', 'invoice/json-get-account-currency'),
('accountant_pipes', 'invoice/en-outside-eu'),
('accountant_pipes', 'invoice/en'),
('accountant_pipes', 'invoice/cz'),
('accountant_pipes', 'orders/detail'),
('accountant_pipes', 'orders/change-address'),
('accountant_pipes', 'admin/json-get-cnb-eur-rate'),
('accountant_pipes', 'invoice/custom-texts-get-json');

INSERT INTO users_acl (role, resource) VALUES
('accountant_tobacco', 'tobacco-customers/index'),
('accountant_tobacco', 'tobacco-customers/edit'),
('accountant_tobacco', 'tobacco-customers/detail'),
('accountant_tobacco', 'tobacco-customers/address'),
('accountant_tobacco', 'tobacco-invoice/prepare'),
('accountant_tobacco', 'tobacco-invoice/generate'),
('accountant_tobacco', 'tobacco-invoice/create-credit-note'),
('accountant_tobacco', 'tobacco-invoice/add-custom-item'),
('accountant_tobacco', 'tobacco-invoice/remove-custom-item'),
('accountant_tobacco', 'tobacco-invoice/json-get-custom-item'),
('accountant_tobacco', 'tobacco-orders/detail'),
('accountant_tobacco', 'tobacco-invoice/custom-texts-get-json');

--
-- 2017-06-08 #25 - Doklad o přijetí platby
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'invoice/payment-get-recipe', description FROM users_acl WHERE resource LIKE 'invoice/index';
INSERT INTO users_acl (role, resource, description)
SELECT role, 'invoice/payment-remove', description FROM users_acl WHERE resource LIKE 'invoice/index';

CREATE TABLE `payments` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_invoice` INT(11) NULL DEFAULT NULL COMMENT 'vztah k fakture',
	`amount` DOUBLE(13,5) UNSIGNED NOT NULL COMMENT 'castka',
	`currency` ENUM('CZK','EUR') NOT NULL COMMENT 'mena' COLLATE 'utf8_czech_ci',
	`rate` FLOAT UNSIGNED NOT NULL COMMENT 'kurz vuci CZK',
	`date` DATE NOT NULL COMMENT 'datum platby',
	PRIMARY KEY (`id`),
	INDEX `FK_payments_invoice` (`id_invoice`),
	CONSTRAINT `FK_payments_invoice` FOREIGN KEY (`id_invoice`) REFERENCES `invoice` (`id`)
)
COMMENT='přijaté platby'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB
AUTO_INCREMENT=12
;

--
-- 2017-05-25 #25 - Doklad o přijetí platby
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'invoice/payments', description FROM users_acl WHERE resource LIKE 'invoice/index';
INSERT INTO users_acl (role, resource, description)
SELECT role, 'invoice/payment-add', description FROM users_acl WHERE resource LIKE 'invoice/index';
INSERT INTO users_acl (role, resource, description)
SELECT role, 'invoice/payment-edit', description FROM users_acl WHERE resource LIKE 'invoice/index';


--
-- 2017-05-25 #6 - Objednávka a fakturace služeb
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'order-edit/set-address', description FROM users_acl WHERE resource LIKE 'order-edit/customer';

--
-- 2017-05-22 #6 - Objednávka a fakturace služeb
--
ALTER TABLE `orders_services`
	ALTER `description` DROP DEFAULT;
ALTER TABLE `orders_services`
	CHANGE COLUMN `description` `description` VARCHAR(500) NOT NULL COMMENT 'popis sluzby' COLLATE 'utf8_czech_ci' AFTER `id_orders`;

--
-- 2017-05-22 #6 - Objednávka a fakturace služeb
--
ALTER TABLE `orders_templates`
	ADD COLUMN `type` ENUM('products', 'service') NOT NULL DEFAULT 'products' AFTER `payment_method`;


--
-- 2017-05-16 #6 - Objednávka a fakturace služeb
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'order-edit/services', description FROM users_acl WHERE resource LIKE 'order-edit/index';

--
-- 2017-05-06 #6 - Objednávka a fakturace služeb
--
CREATE TABLE `orders_services` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'primarni klic',
	`id_orders` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na objednavku',
	`description` VARCHAR(50) NOT NULL COMMENT 'popis sluzby' COLLATE 'utf8_czech_ci',
	`price` DECIMAL(13,5) UNSIGNED NOT NULL DEFAULT '0.00000' COMMENT 'jednotkova cena',
	`qty` INT(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'pocet',
	PRIMARY KEY (`id`),
	INDEX `id_orders` (`id_orders`),
	CONSTRAINT `FK__orders` FOREIGN KEY (`id_orders`) REFERENCES `orders` (`id`)
)
COMMENT='polozky objednavky sluzeb'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2017-05-05 #21 - Evidence změn faktur
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-invoice/deregister', 'deregistace faktury'),
('admin', 'tobacco-invoice/deregister', NULL),
('manager', 'tobacco-invoice/deregister', NULL),
('accountant_tobacco', 'tobacco-invoice/deregister', NULL);

--
-- 2017-05-05 #21 - Evidence změn faktur
--
ALTER TABLE `invoice`
	ADD COLUMN `registered` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'je faktura evidovana' AFTER `inserted`;

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'invoice/deregister', 'deregistace faktury'),
('admin', 'invoice/deregister', NULL),
('manager', 'invoice/deregister', NULL),
('accountant_pipes', 'invoice/deregister', NULL);

--
-- 2017-04-28 #2 - Zrychlení WH tabáku
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-orders/ajax-prepare-for-sale', 'AJAX priprava produktu'),
(NULL, 'tobacco-orders/ajax-get-order-queue', 'AJAX ziskani ID fronty'),
(NULL, 'tobacco-orders/ajax-send-order-queue', 'AJAX odeslani mailu z dat fronty'),
('admin', 'tobacco-orders/ajax-prepare-for-sale', NULL),
('admin', 'tobacco-orders/ajax-get-order-queue', NULL),
('admin', 'tobacco-orders/ajax-send-order-queue', NULL),
('manager', 'tobacco-orders/ajax-prepare-for-sale', NULL),
('manager', 'tobacco-orders/ajax-get-order-queue', NULL),
('manager', 'tobacco-orders/ajax-send-order-queue', NULL),
('tobacco_seller', 'tobacco-orders/ajax-prepare-for-sale', NULL),
('tobacco_seller', 'tobacco-orders/ajax-get-order-queue', NULL),
('tobacco_seller', 'tobacco-orders/ajax-send-order-queue', NULL),
('tobacco_whman', 'tobacco-orders/ajax-prepare-for-sale', NULL),
('tobacco_whman', 'tobacco-orders/ajax-get-order-queue', NULL),
('tobacco_whman', 'tobacco-orders/ajax-send-order-queue', NULL);

--
-- 2017-02-22 #694 - Datum importu dat z Alvena
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/fetch-dsi-last', 'zjisteni posledniho souboru s daty'),
('admin', 'employees/fetch-dsi-last', NULL),
('manager', 'employees/fetch-dsi-last', NULL),
('employee', 'employees/fetch-dsi-last', NULL),
('dept_leader', 'employees/fetch-dsi-last', NULL);

--
-- 2017-02-22 #701 - Krátké pole poznámky ve worksheetu
--
ALTER TABLE `employees_works`
	CHANGE COLUMN `description` `description` VARCHAR(150) NULL DEFAULT NULL COMMENT 'poznamka' COLLATE 'utf8_czech_ci' AFTER `km`;

--
-- 2017-02-20 #714 - Úprava administrace pracovníků
--
ALTER TABLE `employees`
	ADD COLUMN `active` ENUM('y','n') NOT NULL DEFAULT 'y' COMMENT 'aktivni' AFTER `id_departments`;

--
-- 2016-12-27 #636 - Dodavatele
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'tobacco-producers/index', 'prehled dodavatelu'),
('admin', 'tobacco-producers/index', NULL),
('manager', 'tobacco-producers/index', NULL),
('tobacco_whman', 'tobacco-producers/index', NULL);

INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'tobacco-producers/add', 'novy dodavatel'),
('admin', 'tobacco-producers/add', NULL),
('manager', 'tobacco-producers/add', NULL),
('tobacco_whman', 'tobacco-producers/add', NULL);

INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'tobacco-producers/edit', 'editace dodavatele'),
('admin', 'tobacco-producers/edit', NULL),
('manager', 'tobacco-producers/edit', NULL),
('tobacco_whman', 'tobacco-producers/edit', NULL);

--
-- 2016-12-27 #708 - Proplacení svátků
--
ALTER TABLE `employees`
	ADD COLUMN `reimburse_holidays` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'proplacet svatky' AFTER `vacation`;


--
-- 2016-12-08 #702 - Nové důvody volna
--
ALTER TABLE `employees_works`
	CHANGE COLUMN `type` `type` ENUM('work','bustrip','bustrip_frgn','hoffice','overtime','vacation','halfvacation','sickness','fmc','funeral','unpaid_vacation') NULL DEFAULT NULL COMMENT 'typ pracovniho zaznamu' COLLATE 'utf8_czech_ci' AFTER `date`;

ALTER TABLE `employees_rewards`
	ADD COLUMN `unpaid` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'pocet neproplacenych hodin' AFTER `hours`;


--
-- 2016-12-05 #506 - Reporty - celní správa
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'tobacco-reports/duty', 'reporting pro celni spravu'),
('admin', 'tobacco-reports/duty', NULL),
('manager', 'tobacco-reports/duty', NULL),
('tobacco_whman', 'tobacco-reports/duty', NULL);

--
-- 2016-12-05 #702 - Nové důvody volna
--
ALTER TABLE `employees_works`
	CHANGE COLUMN `type` `type` ENUM('work','bustrip','bustrip_frgn','hoffice','overtime','vacation','halfvacation','sickness','fmc','funeral') NULL DEFAULT NULL COMMENT 'typ pracovniho zaznamu' COLLATE 'utf8_czech_ci' AFTER `date`;

--
-- 2016-11-07 #689 - Převod dovolené
--
ALTER TABLE `employees`
	CHANGE COLUMN `vacation` `vacation` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '20' COMMENT 'rocni narok dovolene' AFTER `transport_allowance`;

--
-- 2016-11-01 #695 - Typ zaokrouhlování docházky
--
ALTER TABLE `employees`
	ADD COLUMN `rounding` TINYINT UNSIGNED NOT NULL DEFAULT '1' COMMENT 'zpusob zaokrouhlovani prichodu a odchodu' AFTER `overtime_extra_rate`;
UPDATE employees SET `rounding` = '0' WHERE `type` = 'supplier';
UPDATE employees SET `rounding` = '2' WHERE `id` = '4' OR `id` = '12';

--
-- 2016-10-24 #689 - Převod dovolené
--
UPDATE employees SET vacation = 20 WHERE vacation <> 0 AND id <> 13;

--
-- 2016-10-24 #687 - 1/2 den dovolené
--
ALTER TABLE `employees_works`
	CHANGE COLUMN `type` `type` ENUM('work','bustrip','bustrip_frgn','hoffice','overtime','vacation','halfvacation','sickness') NULL DEFAULT NULL COMMENT 'typ pracovniho zaznamu' COLLATE 'utf8_czech_ci' AFTER `date`;

--
-- 2016-10-20 #650 - Služby
--
ALTER TABLE `orders`
	ADD COLUMN `type` ENUM('products', 'service') NOT NULL DEFAULT 'products' COMMENT 'typ objenavky: produktova nebo sluzba' AFTER `id`;

--
-- 2016-10-06 #678 - Obědová pauza
--
ALTER TABLE `employees_works`
	ADD COLUMN `lunch` ENUM('y','n') NOT NULL DEFAULT 'y' COMMENT 'obedova pauza' AFTER `end`;

--
-- 2016-10-06 #686 - Znovuotevření měsíce
--
DELETE FROM users_acl WHERE role LIKE 'dept_leader' AND resource LIKE 'employees/reopen-month';

--
-- 2016-10-06 #683 - Odečet přesčasů
--
ALTER TABLE `employees_rewards`
	CHANGE COLUMN `overtime_cash` `overtime_cash` DECIMAL(5,2) NOT NULL DEFAULT '0.00' COMMENT 'hodiny k proplaceni' AFTER `transport_bonus`,
	CHANGE COLUMN `overtime_bonus` `overtime_bonus` DECIMAL(7,2) NOT NULL DEFAULT '0.00' COMMENT 'priplatek za prescas / odmena' AFTER `overtime_cash`;

--
-- 2016-10-03 #641 - Objednávka v eurech
--
ALTER TABLE `orders`
	ADD COLUMN `expected_payment_rate` DECIMAL(10,5) UNSIGNED NULL DEFAULT NULL
  COMMENT 'kurz CZK na EUR' AFTER `expected_payment_currency`;
