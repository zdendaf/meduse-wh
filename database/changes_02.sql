--
-- Toto je soubor pro databazove zmeny.
-- Zmeny vkladame na zacatek souboru - ne na konec (nejnovejsi zmeny jsou nahore).
-- Zmenu v tomto souboru nesmi obsahovat cele nazvy tabulek (ne db_name.db_table, jen db_table).
-- Kazda zmena databaze musi byt uvozena komentarem ve tvaru:
--
-- YYYY-MM-DD #123 - název issue z eventum

--
-- 2015-03-20 #470 - Procento z ceny na faktuře
--
ALTER TABLE `invoice`
	ADD COLUMN `invoice_price` DECIMAL(10,2) NULL DEFAULT '0' 
  COMMENT 'castka na fakture' AFTER `language`;


--
-- 2015-03-18 #471 - Zaokrouhlování na faktuře
--
ALTER TABLE `invoice`
	ADD COLUMN `rounding` VARCHAR(1) NULL DEFAULT 'n' 
  COMMENT 'zaokrouhlovat na cela eura' COLLATE 'utf8_czech_ci' AFTER `mask_zero_prices`;

--
-- 2015-03-19 #476 - defaultní BÚ
--
ALTER TABLE `accounts`
	ADD COLUMN `is_default` VARCHAR(1) NOT NULL DEFAULT 'n' COLLATE 'utf8_czech_ci' AFTER `bank_number`;

--
-- 2015-01-22 HOTFIX
--
ALTER TABLE `invoice`
	ADD COLUMN `show_vat_summary` VARCHAR(1) NULL DEFAULT 'n' 
    COMMENT 'zobrazit rozpis dane' COLLATE 'utf8_czech_ci' AFTER `mask_zero_prices`;

-- 
-- 2015-01-20 #460 - Nelze zobrazit vygenerovanou fakturu
--
INSERT INTO users_acl (role, resource) VALUES 
  (NULL, 'invoice/reset-texts'),
  ('admin', 'invoice/reset-texts'),
  ('manager', 'invoice/reset-texts'),
  ('seller', 'invoice/reset-texts');
INSERT INTO users_acl (role, resource) VALUES 
  (NULL, 'invoice/reset-prices'),
  ('admin', 'invoice/reset-prices'),
  ('manager', 'invoice/reset-prices'),
  ('seller', 'invoice/reset-prices');

-- 2015-01-19 #460 - Nelze zobrazit vygenerovanou fakturu
--
ALTER TABLE `invoice`
	ADD COLUMN `id_pricelists` INT(11) UNSIGNED NULL DEFAULT NULL 
    COMMENT 'FK na cenik, podle ktereho byla faktura sestavena' AFTER `id_users`,
	ADD INDEX `id_pricelists` (`id_pricelists`);
ALTER TABLE `invoice`
	ADD COLUMN `vat` FLOAT NOT NULL DEFAULT '1.21' COMMENT 'DPH na fakture' AFTER `rate`;

--
-- 2015-01-16 #460 - Nelze zobrazit vygenerovanou fakturu
--
ALTER TABLE `invoice`
	ADD COLUMN `item_prices` TEXT NULL COLLATE 'utf8_czech_ci' AFTER `item_lines`;

--
-- 2015-01-16 #460 - Nelze zobrazit vygenerovanou fakturu
--
ALTER TABLE `pricelists`
	ADD COLUMN `deleted` ENUM('y','n') NOT NULL DEFAULT 'n' 
  COMMENT 'priznak, ze je cenik smazan' COLLATE 'utf8_czech_ci' AFTER `valid`;

--
-- 2015-01-09 #349 - Umožnit odebírání kontaktů dodavatele (59)
--
INSERT INTO users_acl (role, resource) VALUES 
  (NULL, 'producers/remove-person'),
  ('admin', 'producers/remove-person'),
  ('manager', 'producers/remove-person'),
  ('head_warehouseman', 'producers/remove-person');

--
-- 2015-01-06 #349 - Propojit ceny s kalkulačkou
--
INSERT INTO users_acl (role, resource) VALUES 
  ('manager', 'orders/get-calculator-data'),
  ('seller', 'orders/get-calculator-data');
INSERT INTO users_acl (role, resource) VALUES 
  ('manager', 'pricelists/ajax-get-info'),
  ('seller', 'pricelists/ajax-get-info');

--
-- 2014-12-22 #453 - Možnost zadávat vstupní cenu - ROLLBACK
--
ALTER TABLE `parts`
	DROP COLUMN `manual_price`;

ALTER TABLE `producers_parts`
	ADD COLUMN `price` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT '0'
  COMMENT 'cena soucasti dodavatele' AFTER `producers_part_name`;

UPDATE producers_parts AS pp 
SET price = (IFNULL((SELECT price FROM parts AS p 
  WHERE p.id = pp.id_parts AND p.multipart = 'n'), 0));  

--
-- 2014-12-22 #453 - Možnost zadávat vstupní cenu
--
ALTER TABLE `parts`
	ADD COLUMN `manual_price` ENUM('y','n') NOT NULL DEFAULT 'n' 
  COMMENT 'cena slozene soucasti se neodviji od podsoucasti' 
  COLLATE 'utf8_czech_ci' AFTER `price`;

--
-- 2014-12-18 #451 - Chybné odečítání podsoučástí (60/61)
--
ALTER VIEW `w_productions_current` AS SELECT 
	`p`.`id` AS `id_productions`,
	`p`.`id_producers` AS `id_producers`,
	`p`.`date_ordering` AS `date_ordering`,
	`p`.`date_delivery` AS `date_delivery`,
	`pp`.`id_parts` AS `id_parts`,
	`pp`.`amount` AS `amount`,
	`pp`.`id_parts_operations` AS `id_parts_operations`,
	`pp`.`amount_remain` AS `amount_remain` 
FROM `productions` AS `p` 
JOIN `productions_parts` AS `pp` ON `p`.`id` = `pp`.`id_productions`
WHERE `p`.`status` = 'open';

--
-- 2014-12-04 #441 - Přiřazení HS kódu
--
INSERT INTO users_acl (role, resource) VALUES 
  (NULL, 'parts/ajax-hs-save'),
  ('admin', 'parts/ajax-hs-save'),
  ('manager', 'parts/ajax-hs-save'),
  ('head_warehouseman', 'parts/ajax-hs-save'),
  ('seller', 'parts/ajax-hs-save');

--
-- 2014-12-04 #440 - Sleva na faktuře
--
ALTER TABLE `invoice`
	ADD COLUMN `additional_discount_include` VARCHAR(1) NULL DEFAULT 'n' 
  COMMENT 'rozpocitat slevu' AFTER `additional_discount`;

--
-- 2014-12-03 HOTFIX
--
INSERT INTO `users_acl`(`role`, `resource`) VALUES ('seller', 'orders/special/status');

--
-- 2014-12-02 #423 Přebytečné položky v rezervaci (54)
--
DELETE FROM users_acl WHERE role = 'seller' AND resource = 'orders/special/status';

--
-- 2014-12-01 #425 Přidat u dodavatele sloupec pro cenu operace
--
ALTER TABLE `producers_parts`
	ALTER `producers_part_id` DROP DEFAULT;
ALTER TABLE `producers_parts`
	CHANGE COLUMN `producers_part_id` `producers_part_id` VARCHAR(80) NULL COLLATE 'utf8_czech_ci' AFTER `is_deleted`;
ALTER TABLE `producers_parts`
	ALTER `producers_part_name` DROP DEFAULT;
ALTER TABLE `producers_parts`
	CHANGE COLUMN `producers_part_name` `producers_part_name` VARCHAR(250) NULL COLLATE 'utf8_czech_ci' AFTER `producers_part_id`;
ALTER TABLE `producers_parts`
	ALTER `id_users` DROP DEFAULT;
ALTER TABLE `producers_parts`
	CHANGE COLUMN `id_users` `id_users` INT(11) NULL AFTER `min_amount`;

--
-- 2014-11-21 #384 Faktury - možnost editace řádky faktur
--
ALTER TABLE `invoice_custom_texts`
	CHANGE COLUMN `text` `text` VARCHAR(500) NULL DEFAULT NULL COLLATE 'utf8_czech_ci' AFTER `language`;
ALTER TABLE `invoice`
	CHANGE COLUMN `text1` `text1` VARCHAR(500) NULL DEFAULT NULL COLLATE 'utf8_czech_ci' AFTER `mask_zero_prices`;

--
-- 2014-11-20 #384 Faktury - možnost editace řádky faktur
--
ALTER TABLE `invoice`
	ADD COLUMN `delivery_text` VARCHAR(255) NULL DEFAULT NULL 
    COMMENT 'volitelny dodaci text' COLLATE 'utf8_czech_ci' 
    AFTER `allow_delivery_addr`;

--
-- 2014-11-07 #384 Faktury - možnost editace řádky faktur
--
INSERT INTO users_acl (role, resource) VALUES 
  (NULL, 'invoice/save-item-texts'),
  ('admin', 'invoice/save-item-texts'),
  ('manager', 'invoice/save-item-texts'),
  ('seller', 'invoice/save-item-texts');

ALTER TABLE `invoice`
	ADD COLUMN `item_lines` TEXT NULL DEFAULT NULL AFTER `text1`;

--
-- 2014-11-06 #384 Faktury - možnost editace řádky faktur
--
ALTER TABLE `invoice`
	ADD COLUMN `issue_date` DATE NULL COMMENT 'datum vystavení' AFTER `type`,
	ADD COLUMN `due_date` DATE NULL DEFAULT NULL COMMENT 'datum splatnosti' AFTER `issue_date`;

--
-- 2014-11-04 #384 Faktury - možnost editace řádky faktur
--
TRUNCATE `invoice`;
TRUNCATE `invoice_custom_items`;

--
-- 2014-08-22 #423 - Přebytečné položky v rezervaci (54)
--
DELETE FROM orders_products_parts WHERE id IN (544, 664, 665);
UPDATE parts_warehouse SET amount = amount + 1 WHERE id IN ('AS17-U', 'AN24', 'N02a-U');

--
-- 2014-08-22 #386 - Rychlost systemu - Cache ACL
--
INSERT INTO users_acl (role, resource) VALUES 
  (NULL, 'admin/flush-acl-cache'),
  ('admin', 'admin/flush-acl-cache');


--
-- 2014-08-22 #381 - Operace Meduse - Zmena
--
INSERT INTO users_acl (role, resource) VALUES ('manager', 'admin/operations-rate-settings');


--
-- 2014-08-22 #355 - Faktury - vylepseni
--
INSERT INTO users_acl (role, resource) VALUES
('manager', 'admin/json-get-cnb-eur-rate'),
('tobacco_whman', 'admin/json-get-cnb-eur-rate'),
('seller', 'admin/json-get-cnb-eur-rate');

--
-- 2014-08-21 #381 - Operace Meduse - Zmena
--
CREATE TABLE `application_operations_rate` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`rate` SMALLINT UNSIGNED NULL DEFAULT '0' COMMENT 'cena za hodinu prace',
	`valid` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'platnost',
	PRIMARY KEY (`id`)
)
COMMENT='cena za hodinu prace (pro Meduse operace)'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

--
-- 2014-08-12 #355 - oznaceni bankovniho uctu
--

ALTER TABLE `accounts`
	ADD COLUMN `designation` VARCHAR(50) NOT NULL DEFAULT 'N/A' AFTER `id`;

--
-- 2014-07-24 #360 - Vedouci skladu muze upravovat vyrobni ceny
--
INSERT INTO `users_acl` (`role`, `resource`) VALUES ('head_warehouseman', 'parts/detail/price');

--
-- 2014-06-24 #361 - Umoznit odstraneni soucasti dodavateli
--
ALTER TABLE `producers_parts` ADD `is_deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `id_parts`;

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'producers/delete-part', 'definice resource', '2014-06-24 20:48:17'),
(NULL, 'admin', 'producers/delete-part', 'prirazeni resource', '2014-06-24 20:48:18'),
(NULL, 'manager', 'producers/delete-part', NULL, '2014-06-24 21:10:16'),
(NULL, 'head_warehouseman', 'producers/delete-part', NULL, '2014-06-24 21:10:16');




--
-- 2015-04-30 #358 - Nova role + preklady
--

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'producers/special/price', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'manager', 'producers/special/price', NULL, CURRENT_TIMESTAMP),
(NULL, 'head_warehouseman', 'producers/special/price', NULL, CURRENT_TIMESTAMP),
(NULL, 'admin', 'producers/special/price', NULL, CURRENT_TIMESTAMP);


ALTER TABLE `users` CHANGE `role` `role`
ENUM('admin','manager','head_warehouseman','warehouseman','extern_wh','seller','brigadier','tobacco_whman','issue_reader')
CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL;

UPDATE `users` SET `role` = 'head_warehouseman' WHERE `users`.`id` = 4;

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`)
VALUES ('9', 'head_warehouseman', NULL, 'definice role', CURRENT_TIMESTAMP);


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, 'head_warehouseman', 'claims', 'menu', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'index', 'menu', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'login', 'menu', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders', 'menu', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts', 'menu', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products', 'menu', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'warehouse', 'menu', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'wraps', 'menu', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'claims/add', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'claims/add-wrap', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'claims/delete', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'claims/edit', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'claims/index', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'index/extend', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'index/index', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'login/clear', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'login/index', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'login/process', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/add', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/all-parts', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/all-products', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/cancel-all-order-items', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/cancel-all-prepared', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/cancel-prepared', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/complete', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/delete', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/delete-item', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/delete-wrap-item', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/detail', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/edit', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/expedition', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/index', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/open', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/parts', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/prepare', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/prepare-all', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/prepare-all-order-items', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/prepare-item', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/prepare-overview', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/product-parts', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/wraps', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/add-part', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/add-production', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/add-subphase', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/break', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/create-product', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/delete-part', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/delete-productions', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/delete-sub-part', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/delete-subphase', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/detail', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/edit', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/edit-subphase', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/edit-subphase-amount', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/extern-add', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/extern-edit', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/extern-move', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/extern-move-back', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/extern-overview', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/index', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/overview', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/productions', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/reduce-amount', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/reduce-extern-amount', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/select-producer', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/select-subparts', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/select-warehouse', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/warehouse', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/add', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/delete-part', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/delete-product', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/delete-set', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/delete-wrap', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/detail', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/edit', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/edit-amount', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/edit-wrap-amount', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/index', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/multi-edit', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/multi-edit-add', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/multi-edit-add-wrap', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/multi-edit2', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/multi-edit3', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/multi-edit4', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/overview', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/pack', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/save-as', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/select-parts', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'products/select-set', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'warehouse/index', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'warehouse/parts', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'wraps/add', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'wraps/delete', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'wraps/detail', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'wraps/edit', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'wraps/overview', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'wraps/reduce-amount', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'wraps/warehouse', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/special/all-orders', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/special/all-sets-counts', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/special/actions', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/address', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/show-all-accessories', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/toggle-set', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/operations', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers', 'menu', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/index', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/detail', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/edit', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/edit-person', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/toggle-product', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/show-order', 'na zadost jiriho 1.8.2011 ma pristup i rosta', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/parts', 'na zadost jiriho 1.8.2011 ma pristup i rosta', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/ajax-set-collection', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/detail/producer', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/assign', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/add-operation', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/edit-operation', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/delete-operation', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/delete-carrier', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/finish-productions', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/delete-from-trash', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/save-as', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/history', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/history-back', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/edit-productions', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/tracking-no', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/select-subparts-list', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/toggle-semifinished', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/return', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/payment-accept-date', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'multi-edit/step2', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'multi-edit/step3', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'multi-edit/step4', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'multi-edit/edit-amount', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'multi-edit/add', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/strategic-data', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/strategic-graph', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/special/strategic', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/show-production', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/add-delivery', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/close-production', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/categories', 'prirazeni resource ', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/add-category', 'prirazeni resource ', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/category-delete', 'prirazeni resource ', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/add', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/add-person', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/delete', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/remove-part', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/edit-productions-header', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/assign-holograms', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/holograms', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/show-holograms', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/add-multiple-productions', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/edit-part', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/index', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/payment', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/transport', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/customer', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/process', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/order', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'customers/index', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'customers/detail', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/asign-customer', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/assign-customer', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/remove-customer', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'customers', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'customers/add', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'customers/edit', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'customers/delete', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/detail', 'prirazeni resource ( automat )', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/categories', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/collections', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/add-collection', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/delete-collection', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/edit-collection', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/special/collections', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'order-edit/ajax-customer', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'pdf/production', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'producers/special/all-parts', '#112 ROSTA - nemuze prirazovat dodavatele ', '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/select-products-new', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'orders/special/select-only-extra-wraps', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/trash', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'index/report-bug', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'admin/user-pwd', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/ajax-check-part-id', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'parts/ajax-get-next-part-id', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'issues', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'issues/index', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'issues/detail', NULL, '2014-05-07 04:29:18'),
(NULL, 'head_warehouseman', 'issues/save-comment', NULL, '2014-05-07 04:29:18');


-- Struktura tabulky `users_roles`

CREATE TABLE IF NOT EXISTS `users_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `role_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `role_translation` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `role_description` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='Slovnik roli s preklady' AUTO_INCREMENT=10 ;

-- Vypisuji data pro tabulku `users_roles`

INSERT INTO `users_roles` (`id`, `is_active`, `role_name`, `role_translation`, `role_description`) VALUES
(1, 1, 'admin', 'Administrátor', ''),
(2, 1, 'manager', 'Manažer', ''),
(3, 1, 'warehouseman', 'Skladník dýmek', ''),
(4, 1, 'seller', 'Obchodník', ''),
(5, 1, 'issue_reader', 'Editor problémů', 'Vidí pouze reportované problémy a může je editovat.'),
(6, 1, 'tobacco_whman', 'Skladník tabáků', ''),
(7, 1, 'head_warehouseman', 'Vedoucí skladník dýmek', ''),
(8, 1, 'extern_wh', 'Externí skladník', 'Vidí pouze svůj externí sklad.'),
(9, 0, 'brigadier', 'Brigádník', 'Tato role nikdy nebyla využita.');


--
-- 2014-04-22 #350 - Faktury - krok 5 - dodelavky - REVERT
--
ALTER TABLE `parts_ctg`
	DROP COLUMN `name_eng`;


--
-- 2014-04-16 #350 - Faktury - krok 5 - dodelavky
--
ALTER TABLE `parts_ctg`
	ADD COLUMN `name_eng` VARCHAR(30) NOT NULL COLLATE 'utf8_czech_ci' AFTER `name`;
UPDATE parts_ctg SET name_eng = name;

--
-- 2014-04-14 #350 - Faktury - krok 5 - dodelavky
--
ALTER TABLE `customers`
	CHANGE COLUMN `discount` `discount` DECIMAL(10,2) NULL DEFAULT NULL COMMENT 'nepouzivat, zastarale' AFTER `email`,
	CHANGE COLUMN `discount_type` `discount_type` ENUM('vat','ex_vat') NULL DEFAULT NULL COMMENT 'nepouzivat, zastarale' COLLATE 'utf8_general_ci' AFTER `discount`;
UPDATE customers SET discount = 0;

ALTER TABLE `customers_parts_ctg_discount`
	COMMENT='zastarale, nepouzivat';
TRUNCATE TABLE customers_parts_ctg_discount;

ALTER TABLE `customers_products_discount`
	COMMENT='zastarale, nepouzivat';
TRUNCATE TABLE customers_products_discount;

--
-- 2014-04-09 #327 - otevrene zakazky
--
INSERT INTO users_acl (role, resource) VALUES (NULL, 'orders/edit-opened-products');
INSERT INTO users_acl (role, resource) VALUES ('admin', 'orders/edit-opened-products');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'orders/edit-opened-products');

--
-- 2014-04-22 #341 - Faktury - krok 4
--
INSERT INTO users_acl (role, resource) VALUES ('seller', 'invoice/prepare');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'invoice/generate');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'invoice/add-custom-item');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'invoice/remove-custom-item');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'invoice/json-get-custom-item');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'invoice/custom-texts');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'invoice/custom-texts-edit');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'invoice/custom-texts-remove');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'invoice/custom-texts-get-json');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'pricelists/index');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'pricelists/assign');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'pricelists/delete');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'pricelists/edit');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'pricelists/import');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'prices/index');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'prices/edit');
INSERT INTO users_acl (role, resource) VALUES ('seller', 'prices/save');

INSERT INTO users_acl (role, resource) VALUES ('manager', 'invoice/prepare');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'invoice/generate');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'invoice/add-custom-item');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'invoice/remove-custom-item');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'invoice/json-get-custom-item');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'invoice/custom-texts');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'invoice/custom-texts-edit');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'invoice/custom-texts-remove');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'invoice/custom-texts-get-json');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'pricelists/index');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'pricelists/assign');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'pricelists/delete');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'pricelists/edit');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'pricelists/import');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'prices/index');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'prices/edit');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'prices/save');

INSERT INTO users_acl (role, resource) VALUES ('manager', 'admin/accounts-list');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'admin/accounts-edit');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'admin/accounts-save');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'admin/accounts-delete');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'admin/rate-settings');
INSERT INTO users_acl (role, resource) VALUES ('manager', 'admin/rate-update');

--
-- 2014-03-26 #341 - Faktury - krok 4
--
ALTER TABLE `prices`
	CHANGE COLUMN `id_parts` `id_parts` VARCHAR(10) NOT NULL COMMENT 'id produktu (soucati)' AFTER `id`;
ALTER TABLE `pricelists`
	ADD COLUMN `abbr` VARCHAR(5) NOT NULL COMMENT 'zkratka ceniku' AFTER `name`,
	ALTER `name` DROP DEFAULT;

--
-- 2014-03-20 #341 - Faktury - krok 4
--
CREATE TABLE `pricelists` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'nazev ceniku' COLLATE 'utf8_czech_ci',
	`type` ENUM('b2b','b2c') NOT NULL DEFAULT 'b2b' COMMENT 'pro jaky typ zakaznika je cenik urcen' COLLATE 'utf8_czech_ci',
	`valid` DATE NOT NULL COMMENT 'datum platnosti',
	PRIMARY KEY (`id`)
)
COMMENT='seznam definovanych ceniku, de facto ciselnik typu cen'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE `customers_pricelists` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_customers` INT(11) UNSIGNED NOT NULL COMMENT 'id zakaznika',
	`id_pricelists` INT(11) UNSIGNED NOT NULL COMMENT 'id ceniku',
	PRIMARY KEY (`id`)
)
COMMENT='prirazeni ceniku k zakaznikum'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE `prices` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_parts` INT(11) UNSIGNED NOT NULL COMMENT 'id produktu (soucati)',
	`id_pricelists` INT(11) UNSIGNED NOT NULL COMMENT 'id ceniku',
	`price` DECIMAL(10,2) UNSIGNED NOT NULL COMMENT 'cena v eurech',
	`inserted` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COMMENT='ceny produktu prislusne cenikum'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2014-03-13 #333 - expedované objednávky na dobírku
--
INSERT INTO users_acl (`role`, `resource`) VALUES ('seller', 'orders/payment-accept-date');


--
-- 2014-03-04 #314 - Faktury - krok 3
--
UPDATE invoice SET id_accounts = 1 WHERE id_accounts IS NULL;

CREATE TABLE `invoice_custom_texts` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`description` VARCHAR(50) NOT NULL COLLATE 'utf8_czech_ci',
	`type` ENUM('B2B','B2C') NOT NULL DEFAULT 'B2B' COLLATE 'utf8_czech_ci',
	`region` ENUM('eu','non-eu') NOT NULL DEFAULT 'non-eu' COLLATE 'utf8_czech_ci',
	`language` ENUM('cs','en') NOT NULL DEFAULT 'en' COLLATE 'utf8_czech_ci',
	`text` VARCHAR(250) NULL DEFAULT NULL COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`id`)
)
COMMENT='uzivatelske texty do paticky'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2014-02-26 #314 - Faktury - krok 3
--
ALTER TABLE `invoice`
	ADD COLUMN `rate` FLOAT NOT NULL DEFAULT '27' AFTER `currency`;

CREATE TABLE `application_rate` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`rate` FLOAT UNSIGNED NOT NULL DEFAULT '0',
	`inserted` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COMMENT='kurzy CZK-EUR'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;


--
-- 2014-02-25 #314 - Faktury - krok 3
--
CREATE TABLE `accounts` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_czech_ci',
	`address` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_czech_ci',
	`iban` VARCHAR(24) NULL DEFAULT NULL COLLATE 'utf8_czech_ci',
	`bic` VARCHAR(12) NULL DEFAULT NULL COLLATE 'utf8_czech_ci',
	`account_prefix` VARCHAR(4) NULL DEFAULT NULL COLLATE 'utf8_czech_ci',
	`account_number` VARCHAR(12) NULL DEFAULT NULL COLLATE 'utf8_czech_ci',
	`bank_number` VARCHAR(4) NULL DEFAULT NULL COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

ALTER TABLE `invoice`
	ADD COLUMN `id_accounts` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `text1`,
	ADD INDEX `id_accounts` (`id_accounts`);

--
-- 2014-02-20 #314 - Faktury - krok 3
--
ALTER TABLE `invoice_custom_items`
	ADD COLUMN `type` ENUM('additional', 'packaging', 'delivery', 'shipping') NOT NULL
    DEFAULT 'additional' COMMENT 'typ volitelne polozky' AFTER `id_invoice`;

ALTER TABLE `invoice`
	DROP COLUMN `shipping_cost`,
	DROP COLUMN `packaging_cost`,
	DROP COLUMN `delivery_condition`,
	DROP COLUMN `delivery_condition_cost`,
	DROP COLUMN `custom_item`,
	DROP COLUMN `custom_item_cost`,
	DROP COLUMN `custom_item2`,
	DROP COLUMN `custom_item2_cost`;

--
-- 2014-02-10 #314 - Faktury - krok 3
--
CREATE TABLE `invoice_custom_items` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_invoice` INT(11) UNSIGNED NOT NULL COMMENT 'prislusnost k fakture',
	`description` VARCHAR(255) NOT NULL COMMENT 'popis volitene polozky' COLLATE 'utf8_czech_ci',
	`price` DECIMAL(10,2) UNSIGNED NOT NULL COMMENT 'cena polozky',
	PRIMARY KEY (`id`)
)
COMMENT='volitelne polozky k fakturam'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2014-02-05 #314 - Faktury - krok 3
--
ALTER TABLE `invoice`
	CHANGE COLUMN `allow_delivery_addr` `allow_delivery_addr` VARCHAR(1) NULL DEFAULT 'y' COMMENT 'zobrazovat dodaci adresu?' COLLATE 'utf8_czech_ci' AFTER `allow_no`;
ALTER TABLE `invoice`
	ADD COLUMN `additional_discount` TINYINT UNSIGNED NULL DEFAULT '0' COMMENT 'dodatecna sleva' AFTER `custom_item2_cost`,
	ADD COLUMN `mask_zero_prices` VARCHAR(1) NULL DEFAULT 'n' COMMENT 'maskovat nulove ceny' AFTER `additional_discount`;
ALTER TABLE `invoice`
	DROP COLUMN `allow_no`;

--
-- 2014-02-04 #314 - Faktury - krok 3
--
ALTER TABLE `invoice`
	DROP COLUMN `region`;
ALTER TABLE `invoice`
	DROP COLUMN `customer_type`;

--
-- 2014-01-29 #314 - Faktury - krok 3
--
ALTER TABLE `invoice`
	ADD COLUMN `packaging2` VARCHAR(256) NULL DEFAULT NULL AFTER `packaging`;

--
-- 2014-01-28 #314 - Faktury - krok 3
--
ALTER TABLE `invoice`
	CHANGE COLUMN `customer_type` `customer_type` ENUM('B2B','B2C') NULL DEFAULT NULL COLLATE 'utf8_czech_ci' AFTER `region`;

--
-- 2014-01-13 #307 - C05-2-U nefunguje v multieditaci
--
DELETE FROM parts_collections WHERE id_parts LIKE 'C05-2%' AND id_collections = 0;


--
-- 2014-01-22 #314 - Faktury - krok 3
--
ALTER TABLE `invoice`
	ADD COLUMN `currency` ENUM('CZK','EUR') NULL DEFAULT 'CZK' AFTER `language`;
ALTER TABLE `orders`
	CHANGE COLUMN `expected_payment_currency` `expected_payment_currency` ENUM('CZK','EUR') NOT NULL DEFAULT 'CZK' COMMENT 'u predbeznych objedavek - mena ocekavane platby' COLLATE 'utf8_czech_ci' AFTER `expected_payment`;

--
-- 2014-01-21 #314 - Faktury - krok 3
--
ALTER TABLE `invoice`
	ADD COLUMN `language` ENUM('cs','en') NULL DEFAULT 'en' AFTER `customer_type`;
UPDATE invoice SET `language` = 'en';

ALTER TABLE `c_country`
	ADD COLUMN `region` ENUM('eu','non-eu') NOT NULL DEFAULT 'non-eu' AFTER `name`;
UPDATE c_country SET region = 'eu' WHERE code IN ('AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'GB');
UPDATE addresses SET country = 'CZ' WHERE country = 'CzechRepublic'


--
-- 2014-01-16 #303 - Faktury - krok 2
--
ALTER TABLE `invoice`
	CHANGE COLUMN `allow_no` `allow_no` VARCHAR(1) NULL DEFAULT 'y' COMMENT 'zobrazovat cislo faktury?' AFTER `type`,
	CHANGE COLUMN `allow_delivery_addr` `allow_delivery_addr` VARCHAR(1) NULL DEFAULT 'n' COMMENT 'zobrazovat dodaci adresu?' AFTER `allow_no`,
	CHANGE COLUMN `no` `no` VARCHAR(12) NULL DEFAULT NULL COMMENT 'cislo faktury' AFTER `allow_delivery_addr`;
UPDATE invoice AS i SET i.allow_no = 'n' WHERE i.allow_no = '0';
UPDATE invoice AS i SET i.allow_no = 'y' WHERE i.allow_no = '1';
UPDATE invoice AS i SET i.allow_delivery_addr = 'n' WHERE i.allow_delivery_addr = '0';
UPDATE invoice AS i SET i.allow_delivery_addr = 'y' WHERE i.allow_delivery_addr = '1';

--
-- 2014-01-14 #307 - C05-2-U nefunguje v multieditaci
--
UPDATE parts AS p
LEFT JOIN (
	SELECT id_parts, MAX(id_collections) AS id_collections
	FROM parts_collections
	GROUP BY id_parts
) AS c ON p.id = c.id_parts
SET p.id_collections = c.id_collections
WHERE p.id_collections IS NULL
AND p.id_parts_ctg = 23;

--
-- 2013-12-17 #225 - vyroba - upozorneni mailem pri prekroceni dodaci lhuty
--
ALTER TABLE `productions` ADD COLUMN `date_notify` DATE NULL COMMENT 'datum upozorneni pri zpozdeni' AFTER `date_delivery`;
UPDATE productions set date_notify = NOW();

--
-- 2013-12-12 #296 - Zobrazovani issuses v systemu
--
INSERT INTO users_acl (role, resource) VALUES (NULL, "issues");
INSERT INTO users_acl (role, resource) VALUES (NULL, "issues/index");
INSERT INTO users_acl (role, resource) VALUES (NULL, "issues/detail");
INSERT INTO users_acl (role, resource) VALUES (NULL, "issues/save-comment");
INSERT INTO users_acl (role, resource) VALUES (NULL, "issues/change-status");
INSERT INTO users_acl (role, resource) VALUES ("admin", "issues/change-status");
INSERT INTO users_acl (role, resource) SELECT role, "issues" FROM users_acl WHERE resource IS NULL;
INSERT INTO users_acl (role, resource) SELECT role, "issues/index" FROM users_acl WHERE resource IS NULL;
INSERT INTO users_acl (role, resource) SELECT role, "issues/detail" FROM users_acl WHERE resource IS NULL;
INSERT INTO users_acl (role, resource) SELECT role, "issues/save-comment" FROM users_acl WHERE resource IS NULL;

--
-- 2013-12-10 #296 - Zobrazovani issues v systemu
--
CREATE TABLE `issues` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`status` ENUM('new','scheduled','closed','rejected') NOT NULL DEFAULT 'new' COMMENT 'stav problému' COLLATE 'utf8_czech_ci',
	`id_users` INT(11) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'uživatel (FK)',
	`summary` VARCHAR(255) NOT NULL COMMENT 'shrnutí problému' COLLATE 'utf8_czech_ci',
	`description` TEXT NULL COMMENT 'popis problému' COLLATE 'utf8_czech_ci',
	`page` VARCHAR(128) NULL DEFAULT NULL COMMENT 'odkaz (URL) na stránku s výskytem' COLLATE 'utf8_czech_ci',
	`browser` VARCHAR(128) NULL DEFAULT NULL COMMENT 'prohlížeč' COLLATE 'utf8_czech_ci',
	`changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'poslední změna',
	`inserted` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'čas reportu',
	PRIMARY KEY (`id`)
)
COMMENT='nahlášené reporty'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE `issues_comments` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`inserted` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`id_issues` INT(11) UNSIGNED NOT NULL COMMENT 'fk na issues',
	`id_users` INT(11) UNSIGNED NOT NULL COMMENT 'fk na users',
	`body` TEXT NOT NULL COMMENT 'obsah komentáře' COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`id`)
)
COMMENT='komentáře k reportovaným problémům'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2013-12-09 #299 - Přehled dodavatelů - odtranit dopravce
--
UPDATE producers SET id_producers_ctg = NULL WHERE id_producers_ctg = 21 AND id_orders_carriers IS NULL;
UPDATE producers SET id_producers_ctg = NULL WHERE id_orders_carriers IS NOT NULL;
DELETE FROM producers_ctg WHERE id = 21;

--
-- 2013-11-24 #282 - pridani resource pro admina a manazera
--
INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES (NULL, NULL, 'parts/ajax-check-part-id', NULL, CURRENT_TIMESTAMP), (NULL, 'admin', 'parts/ajax-check-part-id', NULL, CURRENT_TIMESTAMP), (NULL, 'manager', 'parts/ajax-check-part-id', NULL, CURRENT_TIMESTAMP), (NULL, 'warehouseman', 'parts/ajax-check-part-id', NULL, CURRENT_TIMESTAMP);
INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES (NULL, NULL, 'parts/ajax-get-next-part-id', NULL, CURRENT_TIMESTAMP), (NULL, 'admin', 'parts/ajax-get-next-part-id', NULL, CURRENT_TIMESTAMP), (NULL, 'manager', 'parts/ajax-get-next-part-id', NULL, CURRENT_TIMESTAMP), (NULL, 'warehouseman', 'parts/ajax-get-next-part-id', NULL, CURRENT_TIMESTAMP);

--
-- 2013-11-17 #286 - Oprava spatnych poctu ks zdarma na objednavce c 394
--

UPDATE  `orders_products` SET  `amount` = `free_amount` WHERE  `orders_products`.`id` =6021;
UPDATE  `orders_products` SET  `amount` = `free_amount` WHERE  `orders_products`.`id` =6022;
UPDATE  `orders_products` SET  `amount` = `free_amount` WHERE  `orders_products`.`id` =6023;


--
-- 2013-10-30 #37 - napojení sysému čárových kódů : oprava názvu cizího klíče
--
DROP TABLE `eans`;
CREATE TABLE `eans` (
	`prefix` VARCHAR(7) NOT NULL DEFAULT '8592936' COMMENT 'prefix země 859 a id firmy 2936' COLLATE 'utf8_czech_ci',
	`code` VARCHAR(5) NOT NULL COMMENT 'unikátní kód v rámci firmy - CTTTV' COLLATE 'utf8_czech_ci',
	`chnum` VARCHAR(1) NOT NULL COMMENT 'kontrolní číslice' COLLATE 'utf8_czech_ci',
	`id_parts` VARCHAR(50) NOT NULL COMMENT 'součást, ke které je čáový kód přiřazen' COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`code`, `prefix`),
	INDEX `id_parts` (`id_parts`)
)
COMMENT='EAN13 kódy pro produkty.'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2013-10-30 #272 - Implementace skladu tabáků - krok 03
--
INSERT INTO `users_acl` (`role`, `resource`, `inserted`) VALUES
('extern_wh', 'admin/user-pwd', '2013-05-20 06:01:59'),
('tobacco_whman', 'admin/user-pwd', '2013-05-20 06:01:59'),
('warehouseman', 'admin/user-pwd', '2013-05-20 06:01:59');

--
-- 2013-10-29 #272 - Implementace skladu tabáků - krok 03
--
ALTER TABLE `users`
	CHANGE COLUMN `role` `role` ENUM('admin','manager','warehouseman','extern_wh','seller','brigadier','tobacco_whman') NOT NULL COLLATE 'utf8_czech_ci' AFTER `phone`;
INSERT INTO `users_acl` (`id`, `role`, `description`, `inserted`) VALUES (7, 'tobacco_whman', 'definice role', '2013-01-02 05:30:34');
INSERT INTO users_acl (`role`, `resource`) SELECT 'tobacco_whman', resource FROM users_acl WHERE role = 'warehouseman' AND resource IS NOT NULL;
INSERT INTO `users` (`check_ip`, `is_active`, `is_delete`, `username`, `name`, `name_full`, `password`, `password_expiration`, `password_hash`, `email`, `phone`, `role`, `description`) VALUES (0, 1, 0, 'pishin', 'TC', 'Tomáš Cvrkal', '609691e92171988989734ece904c7fd60129f468', '0000-00-00', '526fd065f3630', '', '', 'tobacco_whman', 'skladník tabáků');


--
-- 2013-10-24 #270 - Implementace skladu tabáků - krok 02
--
UPDATE parts SET parts.measure = 'weight' WHERE parts.id_wh_type = 2;

ALTER TABLE `parts`
	CHANGE COLUMN `hs` `hs` VARCHAR(50) NULL DEFAULT NULL COMMENT 'hs code' COLLATE 'utf8_czech_ci' AFTER `end_price_eur`;
CREATE TABLE `users_profile` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_users` INT(11) NOT NULL COMMENT 'FK na users',
	`id_wh_type` INT(11) NOT NULL COMMENT 'FK na warehouse_types',
	`changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	INDEX `id_users` (`id_users`),
	INDEX `id_wh_type` (`id_wh_type`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;
INSERT INTO users_profile (id_users, id_wh_type) SELECT id, 1 FROM users;

ALTER TABLE `orders`
	ADD COLUMN `id_wh_type` INT(11) NOT NULL DEFAULT '1' COMMENT 'FK na typ skladu' AFTER `description`,
	ADD INDEX `id_wh_type` (`id_wh_type`);

--
-- 2013-10-23 #270 - Implementace skladu tabáků - krok 02
--
CREATE TABLE `warehouse_types` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;
INSERT INTO `warehouse_types` (`name`) VALUES ('dýmky');
INSERT INTO `warehouse_types` (`name`) VALUES ('tabáky');

ALTER TABLE `parts`
	ADD COLUMN `id_wh_type` INT(11) UNSIGNED NULL COMMENT 'číselník warehouse_types' AFTER `wh_type`,
	ADD INDEX `id_wh_type` (`id_wh_type`);

UPDATE parts SET parts.id_wh_type = 1 WHERE parts.wh_type = 'pipes';
UPDATE parts SET parts.id_wh_type = 2 WHERE parts.wh_type = 'tobacco';

ALTER TABLE `parts`
	DROP COLUMN `wh_type`;

ALTER TABLE `parts`
	ADD COLUMN `measure` ENUM('unit','weight') NULL DEFAULT 'unit' COMMENT 'jednotky nebo váha' AFTER `description`;

--
-- 2013-10-14 #266 - Implementace skladu tabáků - krok 01
--
ALTER TABLE `parts`
	ADD COLUMN `wh_type` ENUM('pipes', 'tobacco') NOT NULL DEFAULT 'pipes' COMMENT 'typ položky skladu (dýkmy nebo tabáky)' COLLATE 'utf8_czech_ci' AFTER `id`;
ALTER TABLE `producers_parts`
	CHANGE COLUMN `producers_part_id` `producers_part_id` VARCHAR(80) NOT NULL DEFAULT '' COLLATE 'utf8_czech_ci' AFTER `id_parts`;
ALTER TABLE `producers_parts`
	CHANGE COLUMN `producers_part_name` `producers_part_name` VARCHAR(250) NOT NULL DEFAULT '' COLLATE 'utf8_czech_ci' AFTER `producers_part_id`;
ALTER TABLE `producers_parts`
	CHANGE COLUMN `id_users` `id_users` INT(11) NOT NULL DEFAULT '1' AFTER `min_amount`;
