--
-- Toto je soubor pro databazove zmeny.
-- Zmeny vkladame na zacatek souboru - ne na konec (nejnovejsi zmeny jsou nahore).
-- Zmenu v tomto souboru nesmi obsahovat cele nazvy tabulek (ne db_name.db_table, jen db_table).
-- Kazda zmena databaze musi byt uvozena komentarem ve tvaru:
--
-- YYYY-MM-DD #123 - název issue z eventum

--
-- 2016-09-22 #675 - Optimalizace svátků a firemní dovolená
--
CREATE TABLE `employees_holidays` (
	`date` DATE NOT NULL COMMENT 'volno',
	`change` DATE NULL DEFAULT NULL COMMENT 'zamena',
	PRIMARY KEY (`date`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

INSERT INTO `employees_holidays` (`date`, `change`) VALUES
	('2016-06-04', NULL),
	('2016-11-18', '2016-09-28'),
	('2016-12-23', NULL),
	('2016-12-27', NULL),
	('2016-12-28', NULL),
	('2016-12-29', NULL),
	('2016-12-30', NULL);

INSERT INTO `users_acl` (role, resource, description) VALUES
(NULL, 'employees/holidays-add', 'vlozeni volna'),
('manager', 'employees/holidays-add', NULL),
('admin', 'employees/holidays-add', NULL);

INSERT INTO `users_acl` (role, resource, description) VALUES
(NULL, 'employees/holidays-remove', 'smazani volna'),
('manager', 'employees/holidays-remove', NULL),
('admin', 'employees/holidays-remove', NULL);

--
-- 2016-07-25 #655 - Úprava objednávek a fakturace
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-producers/edit-carrier-method', 'editace zpusobu dopravy'),
('admin', 'tobacco-producers/edit-carrier-method', NULL),
('manager', 'tobacco-producers/edit-carrier-method', NULL),
('tobacco_seller', 'tobacco-producers/edit-carrier-method', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-producers/add-carrier-method', 'vlozeni zpusobu dopravy'),
('admin', 'tobacco-producers/add-carrier-method', NULL),
('manager', 'tobacco-producers/add-carrier-method', NULL),
('tobacco_seller', 'tobacco-producers/add-carrier-method', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-producers/add-person', 'vlozeni kontaktni osoby'),
('admin', 'tobacco-producers/add-person', NULL),
('manager', 'tobacco-producers/add-person', NULL),
('tobacco_seller', 'tobacco-producers/add-person', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-producers/edit-person', 'editace kontaktni osoby'),
('admin', 'tobacco-producers/edit-person', NULL),
('manager', 'tobacco-producers/edit-person', NULL),
('tobacco_seller', 'tobacco-producers/edit-person', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-producers/remove-person', 'vymaz kontaktni osoby'),
('admin', 'tobacco-producers/remove-person', NULL),
('manager', 'tobacco-producers/remove-person', NULL),
('tobacco_seller', 'tobacco-producers/remove-person', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-producers/ajax-get-carrier-method-price', 'vrati vychozi cenu prepravni metody'),
('admin', 'tobacco-producers/ajax-get-carrier-method-price', NULL),
('manager', 'tobacco-producers/ajax-get-carrier-method-price', NULL),
('tobacco_seller', 'tobacco-producers/ajax-get-carrier-method-price', NULL);

ALTER TABLE `orders`
	CHANGE COLUMN `carrier_price` `carrier_price` DECIMAL(12,5) NULL DEFAULT NULL COMMENT 'cena za dopravu' AFTER `id_pricelists`;


--
-- 2016-07-22 #655 - Úprava objednávek a fakturace
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-producers/carriers', 'prehled dopravcu'),
('admin', 'tobacco-producers/carriers', NULL),
('manager', 'tobacco-producers/carriers', NULL),
('tobacco_seller', 'tobacco-producers/carriers', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-producers/detail-carrier', 'detail dopravce'),
('admin', 'tobacco-producers/detail-carrier', NULL),
('manager', 'tobacco-producers/detail-carrier', NULL),
('tobacco_seller', 'tobacco-producers/detail-carrier', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-producers/add-carrier', 'pridani dopravce'),
('admin', 'tobacco-producers/add-carrier', NULL),
('manager', 'tobacco-producers/add-carrier', NULL),
('tobacco_seller', 'tobacco-producers/add-carrier', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'tobacco-producers/edit-carrier', 'editace dopravce'),
('admin', 'tobacco-producers/edit-carrier', NULL),
('manager', 'tobacco-producers/edit-carrier', NULL),
('tobacco_seller', 'tobacco-producers/edit-carrier', NULL);


--
-- 2016-07-20 #655 - Úprava objednávek a fakturace
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'producers/add-carrier-method', 'pridani dorucovaci metody'),
('admin', 'producers/add-carrier-method', NULL),
('manager', 'producers/add-carrier-method', NULL),
('seller', 'producers/add-carrier-method', NULL),
(NULL, 'producers/edit-carrier-method', 'editace dorucovaci metody'),
('admin', 'producers/edit-carrier-method', NULL),
('manager', 'producers/edit-carrier-method', NULL),
('seller', 'producers/edit-carrier-method', NULL);

ALTER TABLE `orders_carriers`
	ADD COLUMN `default_price` DECIMAL(13,5) UNSIGNED NULL DEFAULT NULL COMMENT 'vychozi cena prepravni metody' AFTER `name`;


--
-- 2016-07-19 #665 - Úprava objednávek a fakturace
--
ALTER TABLE `producers`
	ADD COLUMN `id_wh_type` INT(11) NULL DEFAULT '1' COMMENT 'id skladu' AFTER `id_producers_ctg`;

ALTER TABLE `orders_carriers`
	ADD COLUMN `id_producers` INT(11) NULL DEFAULT NULL AFTER `id`,
	ADD CONSTRAINT `FK_orders_carriers_producers` FOREIGN KEY (`id_producers`) REFERENCES `producers` (`id`);
UPDATE orders_carriers dest,
(SELECT c.id AS cid, p.id AS pid, p.name AS producers FROM orders_carriers c
LEFT JOIN producers p ON p.id_orders_carriers = c.id
HAVING p.id IS NOT NULL) src
SET dest.id_producers = src.pid WHERE dest.id = src.cid;

ALTER TABLE `orders_carriers` ADD `is_active` ENUM('y','n') NOT NULL DEFAULT 'y' COMMENT 'je metoda aktivni' AFTER `inserted`;

--
-- 2016-06-23 #545 - DS - docházkový subsystém
--
DROP TABLE IF EXISTS `employees_works_types`;
/*
CREATE TABLE IF NOT EXISTS `employees_works_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL COMMENT 'nazev',
  `abbr` varchar(3) COLLATE utf8_czech_ci NOT NULL COMMENT 'zkratka',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='ciselnik typu pracovnich zaznamu';
INSERT INTO `employees_works_types` (`id`, `name`, `abbr`) VALUES
	(1, 'práce', 'pra'),
	(2, 'služ. cesta ČR/SK', 'slu'),
	(3, 'homeoffice', 'hom'),
	(4, 'čerpání přesčasu', 'pre'),
	(5, 'řádná dovolená', 'dov'),
	(6, 'nemocenská', 'nem');
*/

ALTER TABLE `employees_works`
	ALTER `id_employees` DROP DEFAULT;
ALTER TABLE `employees_works`
	CHANGE COLUMN `id_employees` `id_employees` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na pracovniky' AFTER `id`,
	CHANGE COLUMN `id_users` `id_users` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'autor zaznamu nebo zmeny' AFTER `id_employees`,
	ADD COLUMN `timestamp_changed` TIMESTAMP NULL DEFAULT NULL COMMENT 'casova znacka (po zmene)' AFTER `timestamp`,
	ADD COLUMN `deleted` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'vymazano' AFTER `description`,
	ADD CONSTRAINT `FK_employees_works_employees` FOREIGN KEY (`id_employees`) REFERENCES `employees` (`id`),
	ADD CONSTRAINT `FK_employees_works_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

ALTER TABLE `employees_works`
	CHANGE COLUMN `timestamp` `timestamp` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'casova znacka' AFTER `type`;

--
-- 2016-06-23 #632 - MEM - modul evidence majetku
--
ALTER TABLE `customers_warehouses_products`
	ALTER `aquisition_date` DROP DEFAULT;
ALTER TABLE `customers_warehouses_products`
	ADD COLUMN `id_orders` INT(11) NULL COMMENT 'cizi klic na objednavku' AFTER `id_parts`,
	CHANGE COLUMN `aquisition_date` `aquisition_date` DATETIME NOT NULL COMMENT 'datum porizeni' AFTER `id_orders`,
	ADD INDEX `id_orders` (`id_orders`);
ALTER TABLE `customers_warehouses_products`
	CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE `customers_warehouses_products`
	CHANGE COLUMN `id_orders` `id_orders` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic na objednavku' AFTER `id_parts`,
	ADD CONSTRAINT `fk_orders` FOREIGN KEY (`id_orders`) REFERENCES `orders` (`id`);


ALTER TABLE `customers_warehouses_products`
	DROP FOREIGN KEY `fk_customer_warehouses`;
ALTER TABLE `customers_warehouses`
	CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE `customers_warehouses_products`
	CHANGE COLUMN `id_customers_warehouses` `id_customers_warehouses` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na zakaznicky sklad' AFTER `id`;
ALTER TABLE `customers_warehouses_products`
	ADD CONSTRAINT `fk_warehouses` FOREIGN KEY (`id_customers_warehouses`) REFERENCES `customers_warehouses` (`id`);


ALTER TABLE `customers_warehouses`
	ALTER `id_customers` DROP DEFAULT;
ALTER TABLE `customers_warehouses`
	DROP FOREIGN KEY `fk_customers`,
	CHANGE COLUMN `id_customers` `id_customers` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na zakaznika' AFTER `id`
ALTER TABLE customers_addresses DROP FOREIGN KEY fk_customer_addresses_customers2;
ALTER TABLE `customers`
	CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE `customers_addresses`
	ALTER `id_customer` DROP DEFAULT;
ALTER TABLE `customers_addresses`
	CHANGE COLUMN `id_customer` `id_customers` INT(11) UNSIGNED NOT NULL AFTER `id_address`,
	ADD CONSTRAINT `fk_customers` FOREIGN KEY (`id_customers`) REFERENCES `customers` (`id`);

ALTER TABLE `customers_addresses`
	DROP FOREIGN KEY `fk_customer_addresses_addresses1`;
ALTER TABLE `addresses`
	CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE `customers`
	CHANGE COLUMN `id_addresses` `id_addresses` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'dodací adresa' AFTER `id_wh_type`,
	CHANGE COLUMN `id_addresses2` `id_addresses2` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'fakturační adresa' AFTER `id_addresses`,
	ADD INDEX `id_addresses` (`id_addresses`),
	ADD INDEX `id_addresses2` (`id_addresses2`);
ALTER TABLE `customers`
	ADD CONSTRAINT `FK_customers_addresses` FOREIGN KEY (`id_addresses`) REFERENCES `addresses` (`id`),
	ADD CONSTRAINT `FK_customers_addresses_2` FOREIGN KEY (`id_addresses2`) REFERENCES `addresses` (`id`);
ALTER TABLE `customers_addresses`
	ALTER `id_address` DROP DEFAULT;
ALTER TABLE `customers_addresses`
	CHANGE COLUMN `id_address` `id_address` INT(11) UNSIGNED NOT NULL AFTER `id`,
	ADD CONSTRAINT `FK_customers_addresses_addresses` FOREIGN KEY (`id_address`) REFERENCES `addresses` (`id`);
ALTER TABLE `customers_addresses`
	CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST;

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'customers/ajax-warehouse-item-update', 'update polozky zakaznickeho skladu'),
('admin', 'customers/ajax-warehouse-item-update', NULL),
('manager', 'customers/ajax-warehouse-item-update', NULL),
('seller', 'customers/ajax-warehouse-item-update', NULL);

--
-- 2016-06-13 #632 - MEM - modul evidence majetku
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'customers/warehouse-add', 'zalozeni zakaznickeho skladu'),
('admin', 'customers/warehouse-add', NULL),
('manager', 'customers/warehouse-add', NULL),
('seller', 'customers/warehouse-add', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'customers/warehouse-edit', 'uprava zakaznickeho skladu'),
('admin', 'customers/warehouse-edit', NULL),
('manager', 'customers/warehouse-edit', NULL),
('seller', 'customers/warehouse-edit', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'customers/warehouse-delete', 'odstraneni zakaznickeho skladu'),
('admin', 'customers/warehouse-delete', NULL),
('manager', 'customers/warehouse-delete', NULL),
('seller', 'customers/warehouse-delete', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'customers/warehouse-detail', 'detailni prehled zakaznickeho skladu'),
('admin', 'customers/warehouse-detail', NULL),
('manager', 'customers/warehouse-detail', NULL),
('seller', 'customers/warehouse-detail', NULL);

--
-- 2016-06-10 #632 - MEM - modul evidence majetku
--
CREATE TABLE `customers_warehouses` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_customers` INT(11) NOT NULL COMMENT 'cizi klic na zakaznika',
	`name` VARCHAR(50) NOT NULL COMMENT 'nazev skladu',
	PRIMARY KEY (`id`),
	INDEX `id_customers` (`id_customers`),
	CONSTRAINT `fk_customers` FOREIGN KEY (`id_customers`) REFERENCES `customers` (`id`)
)
COMMENT='zakaznicke sklady'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;
ALTER TABLE `customer_addresses`
	RENAME TO `customers_addresses`;

CREATE TABLE `customers_warehouses_products` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_customers_warehouses` INT(11) NOT NULL COMMENT 'cizi klic na zakaznicky sklad',
	`id_parts` VARCHAR(10) NOT NULL COMMENT 'cizi klic na produkt',
	`aquisition_date` DATETIME NOT NULL COMMENT 'cena porizeni',
	`aquisition_price` DECIMAL(13,5) NOT NULL COMMENT 'cena porizeni v eurech',
	`inventory_number` VARCHAR(25) NULL DEFAULT NULL COMMENT 'inventarni cislo',
	PRIMARY KEY (`id`),
	INDEX `id_customers_warekouses` (`id_customers_warehouses`),
	INDEX `id_parts` (`id_parts`),
	CONSTRAINT `fk_customer_warehouses` FOREIGN KEY (`id_customers_warehouses`) REFERENCES `customers_warehouses` (`id`),
	CONSTRAINT `fk_parts` FOREIGN KEY (`id_parts`) REFERENCES `parts` (`id`)
)
COMMENT='polozky zakaznickych skladu '
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'customers/warehouses', 'prehled zakaznickych skladu'),
('admin', 'customers/warehouses', NULL),
('manager', 'customers/warehouses', NULL),
('seller', 'customers/warehouses', NULL);

--
-- 2016-06-09 #644 - DÚZP
--
ALTER TABLE `invoice`
	ADD COLUMN `duzp` DATE NULL DEFAULT NULL COMMENT 'duzp' AFTER `issue_date`;
UPDATE invoice i SET i.duzp = i.issue_date;

--
-- 2016-06-06 #650 - Služby
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'orders/extra-show-services', 'zobrazeni sluzeb v objednavkach'),
('admin', 'orders/extra-show-services', NULL),
('manager', 'orders/extra-show-services', NULL),
('seller', 'orders/extra-show-services', NULL);

--
-- 2016-05-22 #602 - Více dodacích adres
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'orders/change-address', 'zmena adresy'),
('admin', 'orders/change-address', NULL),
('manager', 'orders/change-address', NULL),
('seller', 'orders/change-address', NULL);

--
-- 206-05-20 #615 - Čísla faktur v přehledu objenávek
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'invoice/show-versions', 'zobrazit historii PDF faktur'),
('admin', 'invoice/show-versions', NULL),
('manager', 'invoice/show-versions', NULL);

--
-- 2016-05-16 #602 - Více dodacích adres
--
UPDATE users_acl SET resource = 'customers/address' WHERE resource LIKE ('orders/address');

--
-- 2016-05-06 #629 - Upozornění měny vs. účtu
--
INSERT INTO users_acl (role, resource)
SELECT DISTINCT role, 'invoice/json-get-account-currency' FROM users_acl
WHERE resource LIKE 'invoice/prepare' AND role IS NOT NULL AND role <> 'admin';

--
-- 2016-04-25 #627 - Sjednocení výše krytí
--
UPDATE parts p SET p.cover_cost = NULL
WHERE p.product = 'y' AND p.cover_cost IS NOT NULL;


--
-- 2016-04-21 #624 - Suma operací
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'parts/recalculate', description FROM users_acl
WHERE resource LIKE 'parts/detail/price';


--
-- 2016-02-11 #602 - Umožnit si ke zákazníkům ukládat více dodacích adres.
--

CREATE TABLE `customer_addresses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_address` INT(11) NOT NULL,
  `id_customer` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_customer_adresses_addresses1_idx` (`id_address` ASC),
  INDEX `fk_customer_adresses_customers1_idx` (`id_customer` ASC),
  CONSTRAINT `fk_customer_addresses_addresses1`
    FOREIGN KEY (`id_address`)
    REFERENCES `addresses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_addresses_customers2`
    FOREIGN KEY (`id_customer`)
    REFERENCES `customers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



INSERT INTO `customer_addresses` (id_address, id_customer)
SELECT id_addresses, id FROM `customers`;

INSERT INTO `customer_addresses` (id_address, id_customer)
SELECT id_addresses2, id FROM `customers` WHERE id_addresses2 IS NOT NULL;

--
-- 2016-04-14 #624 - Suma operací
--
ALTER TABLE `parts`
	ADD COLUMN `operations_time` DECIMAL(10,2) NOT NULL DEFAULT '0' COMMENT 'suma operaci v minutach' AFTER `cover_cost`,
	ADD COLUMN `operations_cost` DECIMAL(10,2) NOT NULL DEFAULT '0' COMMENT 'suma nakladu na operace' AFTER `operations_time`;

--
-- 2016-03-23 #620 - Editace strategických zakázek
--
INSERT INTO users_acl (role, resource, description) VALUES
(NULL, 'orders/special/select-not-only-strategic', 'definice resource');

INSERT INTO users_acl (role, resource)
SELECT role, 'orders/special/select-not-only-strategic' FROM users_acl
WHERE resource LIKE 'orders/select-products' AND role IS NOT NULL;

INSERT INTO users_acl (role, resource) VALUES
('head_warehouseman', 'orders/select-products'),
('head_warehouseman', 'orders/special/select-not-only-extra-wraps'),
('head_warehouseman', 'orders/edit-product-amount');

--
-- 2016-02-15 #610 - 	Rozšíření práv pro editora
--
INSERT INTO users_acl (role, resource) VALUES ('wh_editor', 'parts/warehouse');

--
-- 2016-01-25 #603 - Storno faktura
--

INSERT INTO users_acl (role, resource)
	SELECT role, "invoice/create-credit-note" FROM users_acl WHERE resource="invoice/prepare";

ALTER TABLE `invoice`
	ALTER `type` DROP DEFAULT;
ALTER TABLE `invoice`
	CHANGE COLUMN `type` `type` ENUM('offer','proforma','regular','credit') NOT NULL COLLATE 'utf8_czech_ci' AFTER `id_wh_type`,
  CHANGE COLUMN `item_lines` `item_lines` TEXT NULL COMMENT 'nepoužívat' COLLATE 'utf8_czech_ci' AFTER `text1`,
	CHANGE COLUMN `item_prices` `item_prices` TEXT NULL COMMENT 'nepoužívat' COLLATE 'utf8_czech_ci' AFTER `item_lines`;


--
-- 2016-01-21 #561 - Fakturace neskladových položek
--

INSERT INTO `users_acl` (`role`, `resource`, `description`)
VALUES
	('seller', 'orders/expedition', NULL),
	('all_seller', 'orders/expedition', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`)
VALUES
	(NULL, 'orders/special/action-close-empty', 'expedice prazdne obj.'),
	('admin', 'orders/special/action-close-empty', NULL),
	('manager', 'orders/special/action-close-empty', NULL),
	('seller', 'orders/special/action-close-empty', NULL),
	('all_seller', 'orders/special/action-close-empty', NULL);

--
-- 2016-01-18 #604 - Propojení měny a účtu
--

ALTER TABLE `accounts`
	ADD COLUMN `currency` VARCHAR(3) NOT NULL DEFAULT 'CZK' AFTER `account_number`;

INSERT INTO `users_acl` (`role`, `resource`)
	SELECT `role`, 'invoice/json-account-currency' FROM `users_acl` WHERE `resource`='invoice/prepare';

--
-- 2016-01-14 #599 - Role Skladový editor
--
INSERT INTO `users_roles` (`is_active`, `role_name`, `role_translation`, `role_description`)
  VALUES (1, 'wh_editor', 'Skladový editor', 'Skladový editor');
INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`)
  VALUES (16, 'wh_editor', NULL, 'definice role');
UPDATE `users_acl` SET `id` = 14, `description` = 'definice role'
  WHERE `role` = 'sales_director' AND `resource` IS NULL;
UPDATE `users_acl` SET `id` = 15, `description` = 'definice role'
  WHERE `role` = 'employee' AND `resource` IS NULL;

INSERT INTO users_acl (role, resource)
SELECT 'wh_editor', resource FROM users_acl WHERE resource LIKE 'index/%' AND role LIKE 'warehouseman';
INSERT INTO users_acl (role, resource)
SELECT 'wh_editor', resource FROM users_acl WHERE resource LIKE 'login/%' AND role LIKE 'warehouseman';
INSERT INTO users_acl (role, resource)
SELECT 'wh_editor', resource FROM users_acl WHERE resource LIKE 'issues/%' AND role LIKE 'warehouseman';
INSERT INTO users_acl (role, resource)
SELECT 'wh_editor', resource FROM users_acl WHERE resource LIKE 'admin/%' AND role LIKE 'warehouseman';
INSERT INTO users_acl (role, resource)
SELECT 'wh_editor', resource FROM users_acl WHERE resource LIKE 'parts/%' AND role LIKE 'warehouseman'
AND resource NOT LIKE '%subphase%' AND resource NOT LIKE '%production%' AND resource NOT LIKE '%extern%'
AND resource NOT LIKE '%reduce%' AND resource NOT LIKE '%category%' AND resource NOT LIKE '%categories%' AND resource NOT LIKE '%collection%'
AND resource NOT LIKE '%history%' AND resource NOT LIKE '%break%' AND resource NOT LIKE '%warehouse%';

--
-- 2015-12-14 #594 - Zobrazení spotřební daně
--

ALTER TABLE `invoice`
	ADD COLUMN `show_excise` ENUM('y','n') NOT NULL DEFAULT 'y' COMMENT 'zobrazeni spotrebni dane na fakture WH tabaku' AFTER `id_accounts`;

--
-- 2015-12-03 #555 - Reporty prodejů
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'reports/sales'),
('admin', 'reports/sales'),
('manager', 'reports/sales');

--
-- HOTFIX - opravnění pro export cen
--
INSERT INTO users_acl (resource, role)
SELECT 'pricelists/export', role FROM users_acl
WHERE role IS NOT NULL
AND role NOT LIKE 'admin'
AND resource LIKE 'pricelists/import';

--
-- 2015-12-04 #589 - Nová role - obchodní ředitel
--
INSERT INTO `users_roles` (`is_active`, `role_name`, `role_translation`, `role_description`)
VALUES (1, 'sales_director', 'Obchodní ředitel', 'Obchodní ředitel');
INSERT INTO users_acl (role, resource)
SELECT 'sales_director', resource FROM users_acl WHERE role = 'seller';
INSERT INTO users_acl (role, resource)
VALUES ('sales_director', 'pricelists/show-margins');
--
-- 2015-12-03 #585 - Úprava krycího nákladu
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'admin/cover-cost-settings'),
('admin', 'admin/cover-cost-settings'),
('manager', 'admin/cover-cost-settings');

CREATE TABLE `application_cover_cost` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_wh_type` INT(11) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'cizi klic na wh',
	`cover_cost` DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'vyse kryciho nakladu',
	`inserted` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'datum vlozeni',
	PRIMARY KEY (`id`)
)
COMMENT='vychozi kryci naklad'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

ALTER TABLE `parts`
	CHANGE COLUMN `cover_cost` `cover_cost` DECIMAL(10,2) NULL DEFAULT NULL COMMENT 'kryci naklad v procentech' AFTER `end_price_eur`;

UPDATE parts SET parts.cover_cost = NULL;

--
-- 2015-12-02 #585 - Úprava krycího nákladu
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'parts/price-cover'),
('admin', 'parts/price-cover'),
('manager', 'parts/price-cover');
UPDATE parts SET parts.cover_cost = 10;

--
-- 2015-11-30 #393 - Reporty - historie
--

CREATE TABLE `system_action_log` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`id_users_action_log` INT NOT NULL,
	`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`action` VARCHAR(120) NOT NULL,
	`params` TEXT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK__users_action_log` FOREIGN KEY (`id_users_action_log`) REFERENCES `users_action_log` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COMMENT='logování automatických akcí v závislosti na akcích uživatele'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB
;

--
-- 2015-11-23 #584 - Distributorská kvóta
--

ALTER TABLE `customers`
	ADD COLUMN `transfer_perc` INT(2) NOT NULL DEFAULT '0' COMMENT 'procento transferovych skel' AFTER `type`;

ALTER TABLE `orders_products`
	ADD COLUMN `transfer_amount` INT(11) NOT NULL DEFAULT '0' COMMENT 'kusy za transferovou cenu' AFTER `free_amount`;

--
-- 2015-10-29 #575 - AJAX dotaz na seriová čísla
--
INSERT INTO `users_acl` (`role`, `resource`) VALUES
(NULL, 'holograms/ajax-get-available'),
('admin', 'holograms/ajax-get-available'),
('manager', 'holograms/ajax-get-available'),
('seller', 'holograms/ajax-get-available'),
('head_warehouseman', 'holograms/ajax-get-available'),
('warehouseman', 'holograms/ajax-get-available');

--
-- 2015-10-29 #574 - Kategorizace obalů
--
INSERT INTO parts_ctg (id, parent_ctg, ctg_order, name)
  VALUES (44, 22, 2250, 'safeboxes'), (45, 22, 2260, 'palety');
UPDATE parts SET parts.id_parts_ctg = 44
  WHERE parts.name LIKE 'safebox%' AND parts.id_parts_ctg = 25;
UPDATE parts SET parts.id_parts_ctg = 45
  WHERE parts.name LIKE 'palety%' AND parts.id_parts_ctg = 26;

--
-- 2015-10-19 #572 - PL - Průběžné ukládání
--
CREATE TABLE `packings_saves` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_orders` INT(11) NOT NULL COMMENT 'ID objednávky',
	`json` LONGTEXT NULL COMMENT 'data z rozhraní ve formě JSON' COLLATE 'utf8_czech_ci',
	`autosave` ENUM('y','n') NULL DEFAULT 'n' COMMENT 'automaticky uloženo' COLLATE 'utf8_czech_ci',
	`time` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'čas uložení',
	PRIMARY KEY (`id`)
)
COMMENT='Uložené podoby rozhraní packing listu'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2015-10-12 #552 - Kontrola nákladů součástí
--
INSERT INTO `users_acl` (`role`, `resource`) VALUES
(NULL, 'parts/check-costs'),
('admin', 'parts/check-costs'),
('manager', 'parts/check-costs'),
('head_warehouseman', 'parts/check-costs');

--
-- 2015-09-30 #547 - Přístup pro účetní
--
INSERT INTO `users_acl` (`role`, `resource`) VALUES
(NULL, 'invoice/show-non-numbered'),
('admin', 'invoice/show-non-numbered'),
('manager', 'invoice/show-non-numbered'),
('seller', 'invoice/show-non-numbered');

INSERT INTO `users_acl` (`role`, `resource`) VALUES
(NULL, 'tobacco-invoice/show-non-numbered'),
('admin', 'tobacco-invoice/show-non-numbered'),
('manager', 'tobacco-invoice/show-non-numbered'),
('tobacco_seller', 'tobacco-invoice/show-non-numbered');

--
-- 2015-09-09 #544 - Změna ceníku dle země
--
ALTER TABLE `pricelists`
	ADD COLUMN `region` ENUM('eu','non-eu') NOT NULL DEFAULT 'eu' COMMENT 'region ceníku' AFTER `type`;

--
-- 2015-09-07 #547 - Přístup pro účetní
--
INSERT INTO `users_roles` (`is_active`, `role_name`, `role_translation`, `role_description`) VALUES
(1, 'accountant_pipes', 'Účetní skladu dýmek', 'Účetní skladu týmek'),
(1, 'accountant_tobacco', 'Účetní skladu tabáku', 'Účetní skladu tabáku');

INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`) VALUES
(12, 'accountant_pipes', NULL, 'definice role'),
(NULL, 'accountant_pipes', 'invoice/index', NULL),
(NULL, 'accountant_pipes', 'invoice/download', NULL),
(13, 'accountant_tobacco', NULL, 'definice role'),
(NULL, 'accountant_tobacco', 'tobacco-invoice/index', NULL),
(NULL, 'accountant_tobacco', 'tobacco-invoice/download', NULL);

INSERT INTO `users_acl` (`role`, `resource`) VALUES
('accountant_pipes', 'index/index'),
('accountant_pipes', 'index/extend'),
('accountant_pipes', 'index/report-bug'),
('accountant_pipes', 'issues/index'),
('accountant_pipes', 'issues/detail'),
('accountant_pipes', 'issues/save-comment'),
('accountant_pipes', 'login/index'),
('accountant_pipes', 'login/process'),
('accountant_pipes', 'login/clear'),
('accountant_pipes', 'admin/user-pwd'),
('accountant_pipes', 'index/switch-warehouse');

INSERT INTO `users_acl` (`role`, `resource`) VALUES
('accountant_tobacco', 'index/index'),
('accountant_tobacco', 'index/extend'),
('accountant_tobacco', 'index/report-bug'),
('accountant_tobacco', 'issues/index'),
('accountant_tobacco', 'issues/detail'),
('accountant_tobacco', 'issues/save-comment'),
('accountant_tobacco', 'login/index'),
('accountant_tobacco', 'login/process'),
('accountant_tobacco', 'login/clear'),
('accountant_tobacco', 'admin/user-pwd'),
('accountant_tobacco', 'index/switch-warehouse');

--
-- 2015-08-12 FIX opravneni, jiz provedeno
--
INSERT INTO `users_acl` (`role`, `resource`) VALUES
('manager', 'pricelists/show-margins');

--
-- 2015-07-31 #536 - 	PL - definice databázového schématu
--
ALTER TABLE `wraps`
	RENAME TO `wraps_old`;

CREATE TABLE `wraps` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_packings` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT 'cizi klic na exportni jednotku',
	`id_parts` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic na obalovou polozku ze skladu',
	`id_wraps` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic na nadobal',
	`no` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'cislo boxu v ramci exportni jednotky',
	`weight_netto` DECIMAL(10,3) UNSIGNED NOT NULL DEFAULT '0.000' COMMENT 'cista vaha obsahu',
	`weight_brutto` DECIMAL(10,3) UNSIGNED NOT NULL DEFAULT '0.000' COMMENT 'celkova vaha',
	`width` DECIMAL(10,0) UNSIGNED NOT NULL DEFAULT '0.000' COMMENT 'sirka v cm',
	`height` DECIMAL(10,0) UNSIGNED NOT NULL DEFAULT '0.000' COMMENT 'vyska v cm',
	`depth` DECIMAL(10,0) UNSIGNED NOT NULL DEFAULT '0.000' COMMENT 'hloubka v cm',
	PRIMARY KEY (`id`),
	INDEX `id_packings_id_parts_id_wraps` (`id_packings`, `id_parts`, `id_wraps`)
)
COMMENT='obecny box'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;


CREATE TABLE `packings` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_invoice` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic na fakturu',
	`weight_netto` DECIMAL(10,3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'celkova cista vaha zbozi',
	`weight_brutto` DECIMAL(10,3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'celkova vaha',
	`date_issue` DATE NOT NULL COMMENT 'datum vystaveni',
	PRIMARY KEY (`id`),
	INDEX `id_invoice` (`id_invoice`)
)
COMMENT='exportni jednotka'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

ALTER TABLE `orders`
	ADD COLUMN `id_packings` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic na prepravni jednotku' AFTER `tracking_no`,
	ADD INDEX `id_packings` (`id_packings`);

CREATE TABLE `parts_wraps` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_parts` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na produkt',
	`id_wraps` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na obalovou jednotku',
	`title` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'nazev polozky' COLLATE 'utf8_czech_ci',
	`serial` VARCHAR(20) NULL DEFAULT NULL COMMENT 'hologram' COLLATE 'utf8_czech_ci',
	`weight` DECIMAL(10,3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'vaha produktu',
	PRIMARY KEY (`id`),
	INDEX `id_parts` (`id_parts`)
)
COMMENT='polozky v obalove jednotce'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2015-07-28 #535 - MARŽE na produktech
--
INSERT INTO `users_acl` (`role`, `resource`) VALUES
(NULL, 'pricelists/show-margins'),
('admin', 'pricelists/show-margins');

INSERT INTO `users_acl` (`role`, `resource`) VALUES
(NULL, 'pricelists/show-prices'),
('admin', 'pricelists/show-prices'),
('manager', 'pricelists/show-prices'),
('seller', 'pricelists/show-prices'),
('tobacco_seller', 'pricelists/show-prices');

--
-- 2015-07-27 #535 - MARŽE na produktech
--
ALTER TABLE `parts`
	ADD COLUMN `cover_cost` DECIMAL(10,2) NOT NULL DEFAULT '5.00' COMMENT 'kryci naklad v procentech' AFTER `end_price_eur`;

--
-- 2015-07-09 #527 - Zobrazení kategorií produktů v objednávce
--
INSERT INTO users_acl (role, resource) VALUES (NULL, 'orders/special/select-not-only-extra-wraps');
INSERT INTO users_acl (role, resource)
	SELECT ur.role_name, 'orders/special/select-not-only-extra-wraps' FROM users_roles ur
	WHERE ur.role_name NOT IN
	(
		SELECT ua.role
		FROM users_acl ua
		WHERE ua.resource LIKE 'orders/special/select-only-extra-wraps' AND ua.role IS NOT NULL
	);
DELETE FROM users_acl
	WHERE resource LIKE 'orders/special/select-only-extra-wraps';

--
-- HOTFIX: IBAN account
--
ALTER TABLE `accounts`
	CHANGE COLUMN `iban` `iban` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_czech_ci' AFTER `address`;

--
-- 2015-06-17 #525 - Oprávnění
--
DELETE FROM users_acl WHERE role = 'tobacco_whman' AND resource = 'orders/index';

--
-- 2015-06-16 #522 - Oprávnění
--
INSERT INTO users_acl (role, resource) VALUES ('manager', 'tobacco/parts-add');
CREATE TABLE `users_permissions` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_users` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na uzivatele',
	`id_users_roles` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na role',
	PRIMARY KEY (`id`),
	INDEX `id_users` (`id_users`, `id_users_roles`),
	CONSTRAINT `FK__users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COMMENT='prirazene role uzivatelum'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;
INSERT INTO users_permissions (id_users, id_users_roles)
  SELECT u.id, ur.id FROM users u
  LEFT JOIN users_roles ur ON ur.role_name = u.role;
ALTER TABLE `users`
	ALTER `role` DROP DEFAULT;
ALTER TABLE `users`
	CHANGE COLUMN `role` `role` ENUM('admin','manager','head_warehouseman','warehouseman','extern_wh','seller','brigadier','tobacco_whman','issue_reader','all_seller','tobacco_seller') NOT NULL COMMENT 'zastarale -- nepouzivat' COLLATE 'utf8_czech_ci' AFTER `phone`;
DELETE FROM users_acl WHERE resource = 'index/switch-warehouse';
INSERT INTO users_acl (role, resource) VALUES
(null, 'index/switch-warehouse'),
('admin', 'index/switch-warehouse'),
('manager', 'index/switch-warehouse'),
('all_seller', 'index/switch-warehouse'),
('tobacco_seller', 'index/switch-warehouse'),
('tobacco_whman', 'index/switch-warehouse');

--
-- 2015-06-15 #519 - Vyhledávání ve fakturách
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'invoice/ajax-company'),
('admin', 'invoice/ajax-company'),
('manager', 'invoice/ajax-company'),
('tobacco_whman', 'invoice/ajax-company')

--
-- 2015-05-28 #515 Šablony e-mailů
--
DELETE FROM users_acl WHERE role IS NOT NULL AND resource LIKE 'email-templates/%';
INSERT INTO users_acl (role, resource) SELECT 'admin', resource
	FROM users_acl WHERE role IS NULL AND resource LIKE 'email-templates/%';
INSERT INTO users_acl (role, resource) SELECT 'manager', resource
	FROM users_acl WHERE role IS NULL AND resource LIKE 'email-templates/%';
INSERT INTO users_acl (role, resource) SELECT 'seller', resource
	FROM users_acl WHERE role IS NULL AND resource LIKE 'email-templates/%';
INSERT INTO users_acl (role, resource) SELECT 'all_seller', resource
	FROM users_acl WHERE role IS NULL AND resource LIKE 'email-templates/%';


--
-- 2015-04-16 #475 - Ukládání PDF faktury na server
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'invoice/download' AS resource, description FROM users_acl
WHERE resource LIKE 'invoice/generate';
INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-invoice/download' AS resource, description FROM users_acl
WHERE resource LIKE 'tobacco-invoice/generate';
ALTER TABLE `invoice_versions`
	ADD COLUMN `id_users` INT(11) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'cizi klic na uzivatele - autor' AFTER `id_invoice`,
	ADD INDEX `id_invoice` (`id_invoice`),
	ADD INDEX `id_users` (`id_users`);
ALTER TABLE `invoice_versions`
	ALTER `filename` DROP DEFAULT;
ALTER TABLE `invoice_versions`
	CHANGE COLUMN `filename` `filename` VARCHAR(255) NOT NULL COMMENT 'jmeno souboru' COLLATE 'utf8_czech_ci' AFTER `created`;

--
-- 2015-04-15 #475 - Ukládání PDF faktury na server
--
CREATE TABLE `invoice_versions` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_invoice` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na invoice',
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'cas vytvoreni',
	`filename` VARCHAR(120) NOT NULL COMMENT 'jmeno souboru' COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`id`)
)
COMMENT='verze vygenerovanych pdf faktur'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;




--
-- 2015-04-13 #488 - Dodatečná sleva - částkou
--
ALTER TABLE `invoice_custom_items`
	CHANGE COLUMN `type` `type` ENUM('additional','packaging','delivery','shipping','discount') NOT NULL DEFAULT 'additional'
  COMMENT 'typ volitelne polozky' COLLATE 'utf8_czech_ci' AFTER `id_invoice`;
ALTER TABLE `invoice_custom_items`
	ALTER `price` DROP DEFAULT;
ALTER TABLE `invoice_custom_items`
	CHANGE COLUMN `price` `price` DECIMAL(10,2) NOT NULL COMMENT 'cena polozky' AFTER `description`;

--
-- 2015-03-23 #468 - Volba ceníků
--
ALTER TABLE `orders`
	ADD COLUMN `id_pricelists` INT(11) NULL DEFAULT NULL
  COMMENT 'cenik' AFTER `id_customers`;
