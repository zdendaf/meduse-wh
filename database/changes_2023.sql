--
-- 2023-01-02 MWH-366 Editovatelné fixní poplatky
--
create table application_fees (
    id      int UNSIGNED auto_increment,
    `name`    varchar(50)                         not null comment 'nazev poplatku',
    price   decimal(10, 4) UNSIGNED             not null comment 'vyse poplatku v eur',
    text_cs varchar(250)                        not null comment 'popis cestina',
    text_en varchar(250)                        not null comment 'popis anglictina',
    enabled bool      default true              not null comment 'je poplatek aktivni',
    updated TIMESTAMP default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment 'posledni zmena',
    constraint application_fees_pk
        primary key (id)
) comment 'fixni poplatky';
create table orders_fees (
    id        int UNSIGNED auto_increment,
    id_orders int UNSIGNED   not null comment 'cizi klic na objednavku',
    id_fees   int UNSIGNED   not null comment 'cizi klic na definovany poplatek',
    price     decimal(10, 4) not null comment 'vyse poplatku',
    constraint orders_fees_pk
        primary key (id),
    constraint orders_fees_application_fees_id_fk
        foreign key (id_fees) references application_fees (id),
    constraint orders_fees_orders_id_fk
        foreign key (id_orders) references orders (id)
            on update cascade on delete cascade
) comment 'fixni poplatky k objednavkam';
--
-- 2023-01-03 MWH-366 Editovatelné fixní poplatky
--
insert into users_acl (role, resource, description) select role, 'orders/fees-list' as resource, 'prehled fixnich poplatku' as description from users_acl where resource like 'invoice/settings-fees';
insert into users_acl (role, resource, description) select role, 'orders/fees-add' as resource, 'vlozeni fixniho poplatku' as description from users_acl where resource like 'invoice/settings-fees';
insert into users_acl (role, resource, description) select role, 'orders/fees-edit' as resource, 'editace fixniho poplatku' as description from users_acl where resource like 'invoice/settings-fees';
insert into users_acl (role, resource, description) select role, 'orders/fees-enable' as resource, 'aktivace/deaktivace fixniho poplatku' as description from users_acl where resource like 'invoice/settings-fees';

insert into users_acl (resource, role, description)
select 'order-edit/fees' as resource, role, 'editace fixnich poplatku objednavky' from users_acl where resource like ('order-edit/payment');

--
-- 2023-01-06 MWH-366 Editovatelné fixní poplatky
--
alter table orders_fees
    add constraint orders_fees_pk
        unique (id_orders, id_fees);

alter table application_fees
    add is_coo tinyint default 0 not null comment 'jedna se o poplatek za coo?';

alter table application_fees
    add is_iv tinyint default 0 not null comment 'jedna se o poplatek za overeni faktury';

truncate table application_fees;
insert into application_fees (id, name, price, text_cs, text_en, enabled, updated, is_coo, is_iv)
values  (1, 'COO', 60.0000, 'Certifikát původu', 'Certificate of Origin', 1, '2023-01-06 16:28:22.0', 1, 0),
        (2, 'OVI', 60.0000, 'Úřední ověření faktury', 'Officially Verified Invoice', 1, '2023-01-06 16:29:30.0', 0, 1),
        (3, 'COO+OVI', 100.0000, 'Certifikát původu a úřední ověření faktury', 'Certificate of Orig. & Officially Verified Invoice', 1, '2023-01-06 16:30:05.0', 1, 1);

truncate table orders_fees;
insert into orders_fees (id_orders, id_fees, price)
select id as id_orders, 1 as id_fees, 60 as price from orders where coo_requirement = 'y' and invoice_verify_requirement = 'n';
insert into orders_fees (id_orders, id_fees, price)
select id as id_orders, 2 as id_fees, 60 as price from orders where coo_requirement = 'n' and invoice_verify_requirement = 'y';
insert into orders_fees (id_orders, id_fees, price)
select id as id_orders, 3 as id_fees, 100 as price from orders where coo_requirement = 'y' and invoice_verify_requirement = 'y';

--
-- 2023-01-09 MWH-369 Název objednávky (editace)
--
insert into users_acl (role, resource, description)
select 'seller' as role, resource, description from users_acl
where resource like 'order-edit/change-order-name' and role is null;

--
-- 2023-01-31 MWH-368 Docházka
--
insert into users_acl (role, resource, description)
select role, 'employees/vacation-claim' as resource, 'narok dovolena na dalsi rok' as description from users_acl
where resource like 'employees/overtime-cash';

create table employees_vacation_claims
(
    id_employees int(11) unsigned not null comment 'cizi klic na pracovniky',
    year         year             not null,
    claimed      float            not null comment 'narok ze smlouvy',
    transferred  float            not null comment 'prevedeno z minuleho rok',
    primary key (year, id_employees),
    constraint employee_vacation_claims_employees_id_fk
        foreign key (id_employees) references employees (id)
)
    comment 'narok na dovolenou';


insert into employees_vacation_claims
select id,
       2022 as `year`,
       vacation as 'claimed',
       case id
           when 1 then 1.0 -- Kubacak
           when 13 then 2.0 -- Dlapkova
           when 17 then 6.5 -- Hybler
           else 0
        end as 'transferred'
from employees e where e.active = 1;

--
-- 2023-02-02 MWH-372 Stažení faktur neexpedovaných zakázek
--
insert into users_acl (role, resource, description)
select role, 'invoice/download-not-closed' as resource, 'stazeni faktur pro neexpedovane objednavky' as description
from users_acl where resource like 'invoice/download' and (role is null or role not like 'accountant_pipes');

insert into users_acl (role, resource, description)
select role, 'tobacco-invoice/download-not-closed' as resource, 'stazeni faktur pro neexpedovane objednavky' as description
from users_acl where resource like 'tobacco-invoice/download' and (role is null or role not like 'accountant_tobacco')

--
-- 2023-07-25 Povolení stažení faktur skladníkům
--
insert into users_acl (role, resource) values
('warehouseman', 'invoice/download'),
('head_warehouseman', 'invoice/download');

--
-- 2023-08-08 Zrychlení přehledu expedovaných objednávek
--
create index emails_id_orders_index on emails (id_orders);
create index emails_type_index on emails (type);

--
-- 2023-08-08 Oprávnění expedovat objednávku pro skladníka tabáků
--
insert into users_acl (role, resource) values ('tobacco_whman', 'tobacco-orders/expedition');


--
-- 2023-08-29 Kalkulátor macerace
--
insert into users_acl (role, resource, description)
select role, 'tobacco/maceration-calculator' as resource, description
from users_acl where resource like 'tobacco/macerations-edit';

--
-- 2023-09-20 Konsolidace čísel šarží
--
insert into users_acl (role, resource, description)
select role, 'tobacco/batch-consolidate' as resource, 'consolidate batches' as description
from users_acl where resource like 'tobacco-reports/amount';


--
-- 2023-12-04 MWH-377: Dokumentace procesů (jako MDX)
--
alter table documents
    add wh int default 1 not null comment 'prislusnost ke skladu dymek nebo tabaku' after id;
update documents set wh = 1 where wh = 0;

insert into users_acl (`role`, resource, `description`)
values
    (null, 'tobacco-documents/index', 'prehled dokumentu'),
    ('admin', 'tobacco-documents/index', null),
    ('manager', 'tobacco-documents/index', null),
    ('sales_director', 'tobacco-documents/index', null),
    ('tobacco_seller', 'tobacco-documents/index', null),
    ('tobacco_whman', 'tobacco-documents/index', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'tobacco-documents/add', 'vlozeni dokumentu'),
    ('admin', 'tobacco-documents/add', null),
    ('manager', 'tobacco-documents/add', null),
    ('sales_director', 'tobacco-documents/add', null),
    ('tobacco_seller', 'tobacco-documents/add', null),
    ('tobacco_whman', 'tobacco-documents/add', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'tobacco-documents/edit', 'editace dokumentu'),
    ('admin', 'tobacco-documents/edit', null),
    ('manager', 'tobacco-documents/edit', null),
    ('sales_director', 'tobacco-documents/edit', null),
    ('tobacco_seller', 'tobacco-documents/edit', null),
    ('tobacco_whman', 'tobacco-documents/edit', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'tobacco-documents/download', 'stazeni dokumentu'),
    ('admin', 'tobacco-documents/download', null),
    ('manager', 'tobacco-documents/download', null),
    ('sales_director', 'tobacco-documents/download', null),
    ('tobacco_seller', 'tobacco-documents/download', null),
    ('tobacco_whman', 'tobacco-documents/download', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'tobacco-documents/detail', 'detail dokumentu'),
    ('admin', 'tobacco-documents/detail', null),
    ('manager', 'tobacco-documents/detail', null),
    ('sales_director', 'tobacco-documents/detail', null),
    ('tobacco_seller', 'tobacco-documents/detail', null),
    ('tobacco_whman', 'tobacco-documents/detail', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'tobacco-documents/add-version', 'vlozeni nove verze dokumentu'),
    ('admin', 'tobacco-documents/add-version', null),
    ('manager', 'tobacco-documents/add-version', null),
    ('sales_director', 'tobacco-documents/add-version', null),
    ('tobacco_seller', 'tobacco-documents/add-version', null),
    ('tobacco_whman', 'tobacco-documents/add-version', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'tobacco-documents/delete', 'smazani dokumentu'),
    ('admin', 'tobacco-documents/delete', null),
    ('manager', 'tobacco-documents/delete', null),
    ('sales_director', 'tobacco-documents/delete', null),
    ('tobacco_seller', 'tobacco-documents/delete', null),
    ('tobacco_whman', 'tobacco-documents/delete', null);

--
-- 2025-01-29 Adresa dodavatele na fakturách
--
alter table invoice
    add id_seller int not null comment 'dodavatel' after id_users;
update invoice set id_seller = 0 where id_wh_type = 1;
update invoice set id_seller = 416 where id_wh_type = 2;
alter table invoice
    add constraint invoice_customers_id_fk
        foreign key (id_seller) references customers (id);

alter table customers
    add seller int default 0 not null comment 'volba na fakture';
create index seller_index
    on customers (seller);
update customers set seller = 1 where id = 0;
update customers set seller = 2 where id = 416;


