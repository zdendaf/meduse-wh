--
-- Toto je soubor pro databazove zmeny.
-- Zmeny vkladame na zacatek souboru - ne na konec (nejnovejsi zmeny jsou nahore).
-- Zmenu v tomto souboru nesmi obsahovat cele nazvy tabulek (ne db_name.db_table, jen db_table).
-- Kazda zmena databaze musi byt uvozena komentarem ve tvaru:
--
-- YYYY-MM-DD #123 - název issue z eventum
--


--
-- 2013-10-10 #228 - Periodická expirace hesla
--
ALTER TABLE `users`	ADD COLUMN `password_expiration` DATE NOT NULL COMMENT 'datum expirace současného hesla' AFTER `password`;
ALTER TABLE `users`	ADD COLUMN `password_hash` VARCHAR(20) NOT NULL COMMENT 'salt pro heslo' AFTER `password_expiration`;
INSERT INTO `users_acl` (`role`, `resource`) VALUES
('warehouseman', 'admin/user-pwd'),
('extern_wh', 'admin/user-pwd'),
('seller', 'admin/user-pwd'),
('brigadier', 'admin/user-pwd');

--
-- 2013-10-09 #194 - export skladovych zasob
--
INSERT INTO `users_acl` (`role`, `resource`) VALUES
(NULL, 'export/download'),
('admin', 'export/download'),
('manager', 'export/download'),
('manager', 'export/parts-warehouse-excel');

--
-- 2013-10-02 #258 - E-mail zakaznikovi po expedici objednavky
--
CREATE TABLE `email_templates` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL COLLATE 'utf8_czech_ci',
	`subject` VARCHAR(128) NULL DEFAULT NULL COLLATE 'utf8_czech_ci',
	`body` TEXT NULL COLLATE 'utf8_czech_ci',
	`changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COMMENT='Sablony pro odesilane maily'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

INSERT INTO `users_acl` (`role`, `resource`, `description`, `inserted`) VALUES
('manager', 'email-templates/ajax-get-template', NULL, CURRENT_TIMESTAMP),
('manager', 'email-templates/index', NULL, CURRENT_TIMESTAMP),
('manager', 'email-templates/add', NULL, CURRENT_TIMESTAMP),
('manager', 'email-templates/edit', NULL, CURRENT_TIMESTAMP),
('manager', 'email-templates/save', NULL, CURRENT_TIMESTAMP),
('manager', 'email-templates/delete', NULL, CURRENT_TIMESTAMP),
('manager', 'orders/notify', NULL, CURRENT_TIMESTAMP),
('seller', 'orders/notify', NULL, CURRENT_TIMESTAMP),
('seller', 'orders/confirm', NULL, CURRENT_TIMESTAMP),
('seller', 'email-templates/ajax-get-template', NULL, CURRENT_TIMESTAMP);


--
-- integrovane hlaseni chyb
--

INSERT INTO `users_acl` (`role`, `resource`, `description`, `inserted`) VALUES
('manager', 'index/report-bug', NULL, CURRENT_TIMESTAMP),
('seller', 'index/report-bug', NULL, CURRENT_TIMESTAMP),
('admin', 'index/report-bug', NULL, CURRENT_TIMESTAMP),
('warehouseman', 'index/report-bug', NULL, CURRENT_TIMESTAMP),
(NULL, 'index/report-bug', NULL, CURRENT_TIMESTAMP);

--
-- 2013-10-01 #109 - monitoring, kdo je online prihlaseny
--
CREATE TABLE `sessions` (
	`id` VARCHAR(32) NOT NULL COLLATE 'utf8_czech_ci',
	`modified` INT(11) NULL DEFAULT NULL,
	`lifetime` INT(11) NULL DEFAULT NULL,
	`data` TEXT NULL COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2013-09-30 #226 - pridan resource pro soft smazani uzivatele
--
INSERT INTO `users_acl` (`role`, `resource`) VALUES
	('admin', 'admin/user-delete'),
	('manager', 'admin/user-delete');
--
-- 2013-09-30 #226 - pridan priznak pro soft smazani uzivatele
--
ALTER TABLE `users` ADD `is_delete` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'soft delete' AFTER `is_active`;

--
-- 2013-09-17 #34 - databáze hologramů
--
DELETE FROM holograms WHERE id_categories IS NULL;
INSERT INTO `users_acl` (`role`, `resource`) VALUES
	(NULL, 'holograms/delete'),
	('admin', 'holograms/delete'),
	('manager', 'holograms/delete');

--
-- 2013-08-27 #249 - DOPRAVCE - zpristupneni obchodnikum
--
INSERT INTO `users_acl` (`role`, `resource`) VALUES
	('seller', 'orders/carriers'),
	(NULL, 'producers/detail-carrier'),
	('admin', 'producers/detail-carrier'),
	('seller', 'producers/detail-carrier'),
	('seller', 'producers/add-person'),
	('seller', 'producers/edit-person'),
	(NULL, 'producers/edit-carrier'),
	('admin', 'producers/edit-carrier'),
	('seller', 'producers/edit-carrier');
INSERT INTO `producers_ctg` (`name`) VALUES ('DOPRAVCI');
ALTER TABLE `producers`
	ALTER `id_producers_ctg` DROP DEFAULT;
ALTER TABLE `producers`
	CHANGE COLUMN `id_producers_ctg` `id_producers_ctg` INT(11) NULL COMMENT 'kategorie, do ktere dodavatel patri' AFTER `id_extern_warehouses`;

--
-- 2013-08-22 #208 - uprava vychozi hodnoty v polich cen
--
ALTER TABLE `parts` CHANGE `price` `price` DECIMAL(10,2) NOT NULL DEFAULT '0.00', CHANGE `old_price` `old_price` DECIMAL(10,2) NOT NULL DEFAULT '0.00', CHANGE `end_price_eur` `end_price_eur` DECIMAL(10,2) NOT NULL DEFAULT '0.00';

--
-- 2013-08-19 #37 - napojení sysému čárových kódů
--
CREATE TABLE `eans` (
	`prefix` VARCHAR(7) NOT NULL DEFAULT '8592936' COMMENT 'prefix země 859 a id firmy 2936',
	`code` VARCHAR(5) NOT NULL COMMENT 'unikátní kód v rámci firmy - CTTTV',
	`chnum` VARCHAR(1) NOT NULL COMMENT 'kontrolní číslice',
	`part_id` VARCHAR(50) NOT NULL COMMENT 'součást, ke které je čáový kód přiřazen' COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`code`, `prefix`),
	UNIQUE INDEX `part_id` (`part_id`),
	CONSTRAINT `part_id` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`)
)
COMMENT='EAN13 kódy pro produkty.'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

INSERT INTO `users_acl` (`role`, `resource`) VALUES ('manager', 'parts/get-barcode'), ('warehouseman', 'parts/get-barcode');

--
-- 2013-08-16 #49 - LIMIT kusů součástí pro objednání
--
ALTER TABLE `producers_parts`
	ADD COLUMN `min_amount` SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT 'minimální počet, který se může objednat v objednávce' AFTER `producers_part_name`;

--
-- 2013-08-15 #244 - redesign tabulky - Objednávka součástí
--
DROP TABLE `w_productions_current`;
CREATE VIEW `w_productions_current` AS select `p`.`id` AS `id_productions`,`p`.`id_producers` AS `id_producers`,`p`.`date_ordering` AS `date_ordering`,`p`.`date_delivery` AS `date_delivery`,`pp`.`id_parts` AS `id_parts`,`pp`.`amount` AS `amount`,`pp`.`amount_remain` AS `amount_remain` from (`productions` `p` join `productions_parts` `pp` on((`p`.`id` = `pp`.`id_productions`))) where (`p`.`status` = 'open');

--
-- 2013-08-14 #134 - upozorneni u vyexpedovanych zakazek
--
ALTER TABLE `orders` ADD COLUMN `date_notification` DATE NULL DEFAULT NULL COMMENT 'datum odeslání oznámení zákazníkovi' AFTER `date_exp`;
INSERT INTO `users_acl` (`role`, `resource`) VALUES ('admin', 'orders/notify'), ('manager', 'orders/notify'), ('warehouseman', 'orders/notify');

--
-- 2013-08-04 #231 - upgrade - porovnání strategických objednávek s předběžnými
--
INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES (NULL, NULL, 'orders/ajax-get-strategic-orders', NULL, CURRENT_TIMESTAMP), (NULL, 'admin', 'orders/ajax-get-strategic-orders', NULL, CURRENT_TIMESTAMP), (NULL, 'manager', 'orders/ajax-get-strategic-orders', NULL, CURRENT_TIMESTAMP);

--
-- 2013-08-03 #226 - rozsireni tabulky o sloupec je aktivni uzivatel
--
ALTER TABLE `users` ADD `is_active` TINYINT NOT NULL DEFAULT '1' AFTER `description`

--
-- 2013-08-05 #147, 241 - dopravce, detail dopravce
--
ALTER TABLE `w_productions_current`
	COLLATE='utf8_czech_ci',
	CONVERT TO CHARSET utf8;
ALTER TABLE `w_productions_current`
	CHANGE COLUMN `id_parts` `id_parts` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_czech_ci' AFTER `date_delivery`;
ALTER TABLE `producers`
	CHANGE COLUMN `id_producers_ctg` `id_producers_ctg` INT(11) NULL DEFAULT NULL
	COMMENT 'kategorie, do ktere dodavatel patri' AFTER `id_extern_warehouses`;
INSERT INTO `producers_ctg` (`name`) VALUES ('DOPRAVA');


--
-- 2013-07-26 #237 koš objednávek - rozšíření práv
--
INSERT INTO `users_acl` (`role`, `resource`)
    VALUES ('seller', 'orders/set-new');

--
-- 2013-07-26 #34 - databaze hologramu
--
DELETE FROM `collections` WHERE  `id`=0;
INSERT INTO `users_acl` (`role`, `resource`) VALUES
('manager',      'holograms/index'),
('warehouseman', 'holograms/index'),
('manager',      'holograms/import'),
('warehouseman', 'holograms/import'),
('manager',      'orders/show-holograms');

--
-- 2013-07-10 #34 - databáze hologramů
-- naplnění 1000 tesovacími kódy hologramů
--
INSERT INTO holograms (id_categories, hologram, inserted) VALUES (NULL, 10077856, now()), (NULL, 10334691, now()), (NULL, 10368989, now()), (NULL, 10511137, now()), (NULL, 10547126, now()), (NULL, 10564737, now()), (NULL, 10577825, now()), (NULL, 10636718, now()), (NULL, 10704936, now()), (NULL, 10896806, now()), (NULL, 10933676, now()), (NULL, 10935662, now()), (NULL, 11076309, now()), (NULL, 11078505, now()), (NULL, 11153480, now()), (NULL, 11494617, now()), (NULL, 11519700, now()), (NULL, 11549121, now()), (NULL, 11583385, now()), (NULL, 11615157, now()), (NULL, 11660850, now()), (NULL, 11749667, now()), (NULL, 12004346, now()), (NULL, 12036170, now()), (NULL, 12181511, now()), (NULL, 12220731, now()), (NULL, 12232111, now()), (NULL, 12239096, now()), (NULL, 12284317, now()), (NULL, 12310014, now()), (NULL, 12466065, now()), (NULL, 12484085, now()), (NULL, 12507326, now()), (NULL, 12539037, now()), (NULL, 12568793, now()), (NULL, 12691356, now()), (NULL, 13133532, now()), (NULL, 13205274, now()), (NULL, 13333420, now()), (NULL, 13395622, now()), (NULL, 13601348, now()), (NULL, 13660398, now()), (NULL, 13775002, now()), (NULL, 14173819, now()), (NULL, 14231828, now()), (NULL, 14241297, now()), (NULL, 14342932, now()), (NULL, 14363065, now()), (NULL, 14386915, now()), (NULL, 14560097, now()), (NULL, 14569838, now()), (NULL, 14605337, now()), (NULL, 14844305, now()), (NULL, 14938421, now()), (NULL, 14981025, now()), (NULL, 15016463, now()), (NULL, 15043956, now()), (NULL, 15047811, now()), (NULL, 15075464, now()), (NULL, 15085655, now()), (NULL, 15393738, now()), (NULL, 15401046, now()), (NULL, 15414501, now()), (NULL, 15606394, now()), (NULL, 15746804, now()), (NULL, 15884925, now()), (NULL, 15896165, now()), (NULL, 16129670, now()), (NULL, 16133309, now()), (NULL, 16141914, now()), (NULL, 16233805, now()), (NULL, 16338758, now()), (NULL, 16357049, now()), (NULL, 16373444, now()), (NULL, 16668589, now()), (NULL, 16755121, now()), (NULL, 16796994, now()), (NULL, 16809106, now()), (NULL, 16858700, now()), (NULL, 16865028, now()), (NULL, 16911135, now()), (NULL, 16949714, now()), (NULL, 16967166, now()), (NULL, 17175011, now()), (NULL, 17301325, now()), (NULL, 17392827, now()), (NULL, 17444907, now()), (NULL, 17486579, now()), (NULL, 17552041, now()), (NULL, 17687703, now()), (NULL, 17847126, now()), (NULL, 18029358, now()), (NULL, 18062623, now()), (NULL, 18143263, now()), (NULL, 18301287, now()), (NULL, 18352461, now()), (NULL, 18479321, now()), (NULL, 18480112, now()), (NULL, 18518964, now()), (NULL, 18600582, now()), (NULL, 18701818, now()), (NULL, 18713342, now()), (NULL, 18820919, now()), (NULL, 19066185, now()), (NULL, 19112884, now()), (NULL, 19153124, now()), (NULL, 19233309, now()), (NULL, 19359107, now()), (NULL, 19644824, now()), (NULL, 19675691, now()), (NULL, 19845368, now()), (NULL, 19957166, now()), (NULL, 20144708, now()), (NULL, 20200914, now()), (NULL, 20241958, now()), (NULL, 20296670, now()), (NULL, 20327611, now()), (NULL, 20360625, now()), (NULL, 20418869, now()), (NULL, 20538010, now()), (NULL, 20870770, now()), (NULL, 21260235, now()), (NULL, 21442681, now()), (NULL, 21562224, now()), (NULL, 21581536, now()), (NULL, 21794799, now()), (NULL, 21856918, now()), (NULL, 21918991, now()), (NULL, 22020710, now()), (NULL, 22194812, now()), (NULL, 22199057, now()), (NULL, 22256149, now()), (NULL, 22330932, now()), (NULL, 22624501, now()), (NULL, 22846995, now()), (NULL, 22869769, now()), (NULL, 22874032, now()), (NULL, 22900340, now()), (NULL, 22968695, now()), (NULL, 22976853, now()), (NULL, 23013416, now()), (NULL, 23127736, now()), (NULL, 23214756, now()), (NULL, 23279682, now()), (NULL, 23588334, now()), (NULL, 23712012, now()), (NULL, 23751514, now()), (NULL, 23860765, now()), (NULL, 23890556, now()), (NULL, 24118760, now()), (NULL, 24136716, now()), (NULL, 24310189, now()), (NULL, 24367153, now()), (NULL, 24436670, now()), (NULL, 24437627, now()), (NULL, 24469301, now()), (NULL, 24553920, now()), (NULL, 24598864, now()), (NULL, 24611279, now()), (NULL, 24791034, now()), (NULL, 24846828, now()), (NULL, 24851514, now()), (NULL, 25017939, now()), (NULL, 25045727, now()), (NULL, 25079239, now()), (NULL, 25148801, now()), (NULL, 25205082, now()), (NULL, 25497120, now()), (NULL, 25539438, now()), (NULL, 25756921, now()), (NULL, 25771313, now()), (NULL, 25877928, now()), (NULL, 25946127, now()), (NULL, 25966497, now()), (NULL, 26082844, now()), (NULL, 26204592, now()), (NULL, 26249296, now()), (NULL, 26284298, now()), (NULL, 26360949, now()), (NULL, 26367758, now()), (NULL, 26457966, now()), (NULL, 26471261, now()), (NULL, 26477052, now()), (NULL, 26482922, now()), (NULL, 26547550, now()), (NULL, 26751715, now()), (NULL, 26838780, now()), (NULL, 26841369, now()), (NULL, 26869678, now()), (NULL, 27012772, now()), (NULL, 27162154, now()), (NULL, 27185330, now()), (NULL, 27313440, now()), (NULL, 27326722, now()), (NULL, 27414484, now()), (NULL, 27493583, now()), (NULL, 27619682, now()), (NULL, 27642703, now()), (NULL, 27691066, now()), (NULL, 27800606, now()), (1, 27925972, now()), (1, 28037984, now()), (1, 28095387, now()), (1, 28189151, now()), (1, 28291531, now()), (1, 28364647, now()), (1, 28482494, now()), (1, 28512131, now()), (1, 28515154, now()), (1, 28663166, now()), (1, 28682005, now()), (1, 28741928, now()), (1, 28969787, now()), (1, 29061400, now()), (1, 29099231, now()), (1, 29284052, now()), (1, 29338304, now()), (1, 29433842, now()), (1, 29444773, now()), (1, 29511569, now()), (1, 29519491, now()), (1, 29526509, now()), (1, 29658445, now()), (1, 29812963, now()), (1, 29922423, now()), (1, 29995159, now()), (1, 30121293, now()), (1, 30173406, now()), (1, 30275153, now()), (1, 30390012, now()), (1, 30434698, now()), (1, 30484902, now()), (1, 30582451, now()), (1, 30644467, now()), (1, 30664119, now()), (1, 30667729, now()), (1, 30716438, now()), (1, 30768925, now()), (1, 30890122, now()), (1, 30994214, now()), (1, 31144534, now()), (1, 31193594, now()), (1, 31312926, now()), (1, 31393256, now()), (1, 31498266, now()), (1, 31546769, now()), (1, 31634277, now()), (1, 31793343, now()), (1, 32045319, now()), (1, 32192739, now()), (1, 32233746, now()), (1, 32263960, now()), (1, 32332793, now()), (1, 32333977, now()), (1, 32518910, now()), (1, 32552441, now()), (1, 32606747, now()), (1, 32635114, now()), (1, 32715528, now()), (1, 32736972, now()), (1, 32760125, now()), (1, 32909929, now()), (1, 33015401, now()), (1, 33027816, now()), (1, 33062351, now()), (1, 33572864, now()), (1, 33675723, now()), (1, 33677622, now()), (1, 33681834, now()), (1, 33714916, now()), (1, 33827244, now()), (1, 33844671, now()), (1, 33938487, now()), (1, 34447975, now()), (1, 34546382, now()), (1, 34833333, now()), (1, 34958513, now()), (1, 34977497, now()), (1, 35117894, now()), (1, 35267942, now()), (1, 35272061, now()), (1, 35335621, now()), (1, 35403402, now()), (1, 35417206, now()), (1, 35428245, now()), (1, 35481203, now()), (1, 35543841, now()), (1, 35548226, now()), (1, 35595042, now()), (1, 35659757, now()), (1, 35714668, now()), (1, 35735904, now()), (1, 35889164, now()), (1, 35902601, now()), (1, 35972994, now()), (1, 35979185, now()), (1, 36110598, now()), (1, 36185242, now()), (1, 36396369, now()), (1, 36600349, now()), (1, 36747327, now()), (1, 36862962, now()), (1, 36943650, now()), (1, 37178267, now()), (1, 37244447, now()), (1, 37271324, now()), (1, 37448176, now()), (1, 37488811, now()), (1, 37715197, now()), (1, 37909443, now()), (1, 37996822, now()), (1, 38033641, now()), (1, 38185716, now()), (1, 38197930, now()), (1, 38256127, now()), (1, 38287752, now()), (1, 38330438, now()), (1, 38355234, now()), (1, 38507370, now()), (1, 38508883, now()), (1, 38670568, now()), (1, 38689258, now()), (1, 38788143, now()), (1, 38950268, now()), (1, 38952230, now()), (1, 38988448, now()), (1, 39063472, now()), (1, 39082614, now()), (1, 39224683, now()), (1, 39268994, now()), (1, 39293416, now()), (1, 39485838, now()), (1, 39507342, now()), (1, 39507379, now()), (1, 39573936, now()), (1, 39577943, now()), (1, 39588631, now()), (1, 39690351, now()), (1, 39720284, now()), (1, 39791625, now()), (1, 39810374, now()), (1, 39885576, now()), (1, 39895311, now()), (1, 40084390, now()), (1, 40223882, now()), (1, 40418597, now()), (1, 40464390, now()), (1, 40533810, now()), (1, 40561450, now()), (1, 40599709, now()), (1, 40652121, now()), (1, 40700632, now()), (1, 40925646, now()), (1, 40954017, now()), (1, 40985268, now()), (1, 41036899, now()), (1, 41063511, now()), (1, 41146919, now()), (1, 41259973, now()), (1, 41336089, now()), (1, 41480374, now()), (1, 41662634, now()), (1, 41736081, now()), (1, 41877192, now()), (1, 41929233, now()), (1, 42033933, now()), (1, 42078246, now()), (1, 42115971, now()), (1, 42123117, now()), (1, 42154821, now()), (1, 42166744, now()), (1, 42169618, now()), (1, 42219650, now()), (1, 42342507, now()), (1, 42359383, now()), (1, 42591057, now()), (1, 42829173, now()), (1, 42840103, now()), (1, 42885766, now()), (1, 43083216, now()), (1, 43123304, now()), (1, 43490669, now()), (1, 43495346, now()), (1, 43561692, now()), (1, 43631133, now()), (1, 43641472, now()), (1, 43659777, now()), (1, 43836471, now()), (1, 43901309, now()), (1, 43995810, now()), (1, 44000797, now()), (1, 44023162, now()), (1, 44029921, now()), (1, 44050536, now()), (1, 44072462, now()), (1, 44283452, now()), (1, 44544976, now()), (1, 44569636, now()), (1, 44577078, now()), (1, 44835686, now()), (2, 44888545, now()), (2, 44931245, now()), (2, 45181727, now()), (2, 45228780, now()), (2, 45405154, now()), (2, 45454707, now()), (2, 45856259, now()), (2, 45882238, now()), (2, 45884882, now()), (2, 45969099, now()), (2, 45977034, now()), (2, 46050276, now()), (2, 46093436, now()), (2, 46126075, now()), (2, 46165911, now()), (2, 46173388, now()), (2, 46197162, now()), (2, 46211391, now()), (2, 46330553, now()), (2, 46350808, now()), (2, 46355380, now()), (2, 46356078, now()), (2, 46385256, now()), (2, 46411528, now()), (2, 46413273, now()), (2, 46503972, now()), (2, 46592894, now()), (2, 46592904, now()), (2, 46650689, now()), (2, 46842199, now()), (2, 47190440, now()), (2, 47195021, now()), (2, 47210985, now()), (2, 47298049, now()), (2, 47353144, now()), (2, 47467995, now()), (2, 47788094, now()), (2, 47947012, now()), (2, 48002085, now()), (2, 48020558, now()), (2, 48062148, now()), (2, 48130273, now()), (2, 48153895, now()), (2, 48162566, now()), (2, 48297747, now()), (2, 48324901, now()), (2, 48361348, now()), (2, 48481405, now()), (2, 48539098, now()), (2, 48612867, now()), (2, 48820024, now()), (2, 48985762, now()), (2, 49343404, now()), (2, 49525271, now()), (2, 49640758, now()), (2, 49895301, now()), (2, 49978500, now()), (2, 50102380, now()), (2, 50171621, now()), (2, 50204563, now()), (2, 50207919, now()), (2, 50238038, now()), (2, 50259914, now()), (2, 50424419, now()), (2, 50474826, now()), (2, 50475186, now()), (2, 50555910, now()), (2, 50580361, now()), (2, 50604665, now()), (2, 50640346, now()), (2, 50686813, now()), (2, 51039476, now()), (2, 51095193, now()), (2, 51258389, now()), (2, 51258914, now()), (2, 51431522, now()), (2, 51594113, now()), (2, 51690211, now()), (2, 51707081, now()), (2, 51732906, now()), (2, 51768664, now()), (2, 51843628, now()), (2, 51897859, now()), (2, 51917891, now()), (2, 51932129, now()), (2, 52000322, now()), (2, 52041236, now()), (2, 52119993, now()), (2, 52164649, now()), (2, 52192918, now()), (2, 52258052, now()), (2, 52326895, now()), (2, 52343499, now()), (2, 52625727, now()), (2, 52632254, now()), (2, 52838529, now()), (2, 53090478, now()), (2, 53436825, now()), (2, 53748681, now()), (2, 53757246, now()), (2, 53857905, now()), (2, 53883107, now()), (2, 54074404, now()), (2, 54185116, now()), (2, 54350005, now()), (2, 54404755, now()), (2, 54547554, now()), (2, 54581865, now()), (2, 54813320, now()), (2, 54839610, now()), (2, 54849585, now()), (2, 54949078, now()), (2, 55098113, now()), (2, 55157902, now()), (2, 55194446, now()), (2, 55252448, now()), (2, 55488414, now()), (2, 55533174, now()), (2, 55563251, now()), (2, 55769052, now()), (2, 55912798, now()), (2, 56338758, now()), (2, 56340631, now()), (2, 56401515, now()), (2, 56523191, now()), (2, 56549378, now()), (2, 56635513, now()), (2, 56638092, now()), (2, 56654930, now()), (2, 56684739, now()), (2, 56768966, now()), (2, 56789007, now()), (2, 56803931, now()), (2, 57069468, now()), (2, 57190507, now()), (2, 57326399, now()), (2, 57360190, now()), (2, 57470106, now()), (2, 57616701, now()), (2, 57659930, now()), (2, 57669958, now()), (2, 57919320, now()), (2, 58031822, now()), (2, 58045265, now()), (2, 58219768, now()), (2, 58233208, now()), (2, 58275003, now()), (2, 58483029, now()), (2, 58508991, now()), (2, 58642252, now()), (2, 58704910, now()), (2, 58723842, now()), (2, 58731708, now()), (2, 58737537, now()), (2, 58811368, now()), (2, 58971403, now()), (2, 59056898, now()), (2, 59131534, now()), (2, 59179540, now()), (2, 59186345, now()), (2, 59302876, now()), (2, 59444212, now()), (2, 59487802, now()), (2, 59631224, now()), (2, 59708508, now()), (2, 59716954, now()), (2, 59753953, now()), (2, 59846296, now()), (2, 59898136, now()), (2, 59906644, now()), (2, 60008205, now()), (2, 60017380, now()), (2, 60051678, now()), (2, 60176131, now()), (2, 60295182, now()), (2, 60430663, now()), (2, 60446013, now()), (2, 60605251, now()), (2, 60616392, now()), (2, 60661125, now()), (2, 60685602, now()), (2, 60785408, now()), (2, 61004523, now()), (2, 61082965, now()), (2, 61115318, now()), (2, 61457794, now()), (2, 61465488, now()), (2, 61699731, now()), (2, 61824680, now()), (2, 61899010, now()), (2, 61974969, now()), (2, 61981430, now()), (2, 62176594, now()), (2, 62358161, now()), (2, 62423964, now()), (2, 62443958, now()), (2, 62464837, now()), (2, 62627156, now()), (2, 62656649, now()), (2, 63149012, now()), (3, 63211105, now()), (3, 63281206, now()), (3, 63290424, now()), (3, 63686718, now()), (3, 63985835, now()), (3, 64015426, now()), (3, 64099851, now()), (3, 64115083, now()), (3, 64237001, now()), (3, 64346456, now()), (3, 64593038, now()), (3, 64627742, now()), (3, 64745176, now()), (3, 64845722, now()), (3, 64851846, now()), (3, 64897885, now()), (3, 65010663, now()), (3, 65130841, now()), (3, 65187191, now()), (3, 65246138, now()), (3, 65337157, now()), (3, 65350646, now()), (3, 65380299, now()), (3, 65447851, now()), (3, 65537310, now()), (3, 65558414, now()), (3, 65648093, now()), (3, 65864877, now()), (3, 65918553, now()), (3, 66011094, now()), (3, 66027123, now()), (3, 66030467, now()), (3, 66318940, now()), (3, 66381091, now()), (3, 66452710, now()), (3, 66543286, now()), (3, 66690976, now()), (3, 66874798, now()), (3, 66973842, now()), (3, 67099458, now()), (3, 67138946, now()), (3, 67173796, now()), (3, 67196527, now()), (3, 67232929, now()), (3, 67237939, now()), (3, 67301148, now()), (3, 67408168, now()), (3, 67458357, now()), (3, 67468496, now()), (3, 67597820, now()), (3, 67614736, now()), (3, 67899913, now()), (3, 68003504, now()), (3, 68029008, now()), (3, 68033837, now()), (3, 68188203, now()), (3, 68212413, now()), (3, 68215952, now()), (3, 68285232, now()), (3, 68298333, now()), (3, 68330315, now()), (3, 68353404, now()), (3, 68389715, now()), (3, 68394178, now()), (3, 68404110, now()), (3, 68454203, now()), (3, 68578847, now()), (3, 68646874, now()), (3, 68673791, now()), (3, 68700056, now()), (3, 68952174, now()), (3, 68963371, now()), (3, 69012644, now()), (3, 69136582, now()), (3, 69220156, now()), (3, 69317112, now()), (3, 69370644, now()), (3, 69399376, now()), (3, 69772665, now()), (3, 69776243, now()), (3, 69848122, now()), (3, 70042083, now()), (3, 70056992, now()), (3, 70177930, now()), (3, 70554614, now()), (3, 70627203, now()), (3, 70686262, now()), (3, 70698070, now()), (3, 70881051, now()), (3, 70883947, now()), (3, 71066083, now()), (3, 71142397, now()), (3, 71260400, now()), (3, 71382570, now()), (3, 71565317, now()), (3, 71610083, now()), (3, 71634676, now()), (3, 71727992, now()), (3, 71847850, now()), (3, 71850265, now()), (3, 71852123, now()), (3, 72060104, now()), (3, 72066077, now()), (3, 72098181, now()), (3, 72207015, now()), (3, 72266650, now()), (3, 72535017, now()), (3, 72566458, now()), (3, 72620721, now()), (3, 72635079, now()), (3, 72771963, now()), (3, 72794923, now()), (3, 72979121, now()), (3, 73296245, now()), (3, 73317952, now()), (3, 73481532, now()), (3, 73546540, now()), (3, 73770401, now()), (3, 73851272, now()), (3, 73879546, now()), (3, 73938346, now()), (3, 73946791, now()), (3, 74016103, now()), (3, 74223966, now()), (3, 74375578, now()), (3, 74442610, now()), (3, 74443714, now()), (3, 74486062, now()), (3, 74610768, now()), (3, 74662079, now()), (3, 74791256, now()), (3, 74886074, now()), (3, 75205882, now()), (3, 75422340, now()), (3, 75531608, now()), (3, 75549054, now()), (3, 75568875, now()), (3, 75586607, now()), (3, 75720547, now()), (3, 75868056, now()), (3, 75883430, now()), (3, 75984801, now()), (3, 76086402, now()), (3, 76098449, now()), (3, 76183056, now()), (3, 76422809, now()), (3, 76476008, now()), (3, 76516850, now()), (3, 76522123, now()), (3, 76658286, now()), (3, 76686170, now()), (3, 76746482, now()), (3, 76755054, now()), (3, 76775947, now()), (3, 77003053, now()), (3, 77041608, now()), (3, 77135646, now()), (3, 77243393, now()), (3, 77285715, now()), (3, 77493489, now()), (3, 77573209, now()), (3, 77660722, now()), (3, 77681482, now()), (3, 77755303, now()), (3, 77866350, now()), (3, 77867334, now()), (3, 77897522, now()), (3, 77975391, now()), (3, 78268795, now()), (3, 78286464, now()), (3, 78290388, now()), (3, 78323025, now()), (3, 78466496, now()), (3, 78537336, now()), (3, 78724599, now()), (3, 78726829, now()), (3, 79067604, now()), (3, 79142528, now()), (3, 79216590, now()), (3, 79246524, now()), (3, 79253123, now()), (3, 79274952, now()), (3, 79472267, now()), (3, 79490290, now()), (3, 79615527, now()), (3, 79624728, now()), (3, 79654194, now()), (3, 79782977, now()), (3, 79803547, now()), (3, 79861352, now()), (3, 79965132, now()), (3, 80019934, now()), (3, 80041635, now()), (3, 80072263, now()), (3, 80153264, now()), (3, 80163196, now()), (3, 80197557, now()), (3, 80209490, now()), (3, 80217187, now()), (3, 80249573, now()), (4, 80267069, now()), (4, 80422474, now()), (4, 80424596, now()), (4, 80427483, now()), (4, 80873232, now()), (4, 80938503, now()), (4, 81101946, now()), (4, 81275449, now()), (4, 81296758, now()), (4, 81306332, now()), (4, 81319662, now()), (4, 81342574, now()), (4, 81434151, now()), (4, 81510381, now()), (4, 81560613, now()), (4, 81775858, now()), (4, 81822393, now()), (4, 81993944, now()), (4, 82048741, now()), (4, 82142391, now()), (4, 82509305, now()), (4, 82605603, now()), (4, 82797824, now()), (4, 82804727, now()), (4, 82816939, now()), (4, 82904808, now()), (4, 82943064, now()), (4, 82980235, now()), (4, 83214076, now()), (4, 83494974, now()), (4, 83572192, now()), (4, 83642201, now()), (4, 83642766, now()), (4, 83704736, now()), (4, 83774306, now()), (4, 83806435, now()), (4, 84057697, now()), (4, 84058131, now()), (4, 84133789, now()), (4, 84175985, now()), (4, 84496514, now()), (4, 84657061, now()), (4, 84667638, now()), (4, 84684899, now()), (4, 84772210, now()), (4, 84873138, now()), (4, 85050809, now()), (4, 85278576, now()), (4, 85538528, now()), (4, 85937522, now()), (4, 86067516, now()), (4, 86131296, now()), (4, 86190307, now()), (4, 86307300, now()), (4, 86314477, now()), (4, 86328114, now()), (4, 86347181, now()), (4, 86356942, now()), (4, 86453449, now()), (4, 86565310, now()), (4, 86624890, now()), (4, 87415168, now()), (4, 87575944, now()), (4, 87719804, now()), (4, 87995062, now()), (4, 88083889, now()), (4, 88308052, now()), (4, 88348543, now()), (4, 88588243, now()), (4, 88636857, now()), (4, 88916582, now()), (4, 88981162, now()), (4, 89031985, now()), (4, 89046536, now()), (4, 89128057, now()), (4, 89529231, now()), (4, 89613171, now()), (4, 89767957, now()), (4, 89780484, now()), (4, 89788612, now()), (4, 89894647, now()), (4, 89980089, now()), (4, 89986869, now()), (4, 90106233, now()), (4, 90187644, now()), (4, 90245354, now()), (4, 90255956, now()), (4, 90399704, now()), (4, 90463872, now()), (4, 90517958, now()), (4, 90600703, now()), (4, 90723663, now()), (4, 90866687, now()), (4, 90948423, now()), (4, 90999746, now()), (4, 91173467, now()), (4, 91227359, now()), (4, 91373184, now()), (4, 91388974, now()), (4, 91484159, now()), (4, 91486924, now()), (4, 91677236, now()), (4, 91682932, now()), (4, 91687478, now()), (4, 91696411, now()), (4, 91915824, now()), (4, 91920174, now()), (4, 92051110, now()), (4, 92276448, now()), (4, 92304398, now()), (4, 92462698, now()), (4, 92522152, now()), (4, 92563088, now()), (4, 92699929, now()), (4, 92743307, now()), (4, 92751399, now()), (4, 92776091, now()), (4, 92807321, now()), (4, 92925833, now()), (4, 92931804, now()), (4, 92996495, now()), (4, 93075858, now()), (4, 93274899, now()), (4, 93275068, now()), (4, 93296800, now()), (4, 93459791, now()), (4, 93569505, now()), (4, 93627281, now()), (4, 93649826, now()), (4, 93777353, now()), (4, 93890137, now()), (4, 94005369, now()), (4, 94026749, now()), (4, 94056347, now()), (4, 94498737, now()), (4, 94780846, now()), (4, 94791012, now()), (4, 94869140, now()), (4, 94963292, now()), (4, 94972340, now()), (4, 95058183, now()), (4, 95246120, now()), (4, 95275276, now()), (4, 95391636, now()), (4, 95420097, now()), (4, 95605064, now()), (4, 95625886, now()), (4, 95665338, now()), (4, 95745717, now()), (4, 95826036, now()), (4, 95882955, now()), (4, 96002715, now()), (4, 96342350, now()), (4, 96477970, now()), (4, 96583007, now()), (4, 96618641, now()), (4, 96751658, now()), (4, 96808814, now()), (4, 96895881, now()), (4, 96957149, now()), (4, 96960714, now()), (4, 97013269, now()), (4, 97068043, now()), (4, 97096810, now()), (4, 97126979, now()), (4, 97140674, now()), (4, 97255473, now()), (4, 97351792, now()), (4, 97464701, now()), (4, 97486599, now()), (4, 97490160, now()), (4, 97561712, now()), (4, 97656813, now()), (4, 97748259, now()), (4, 97759664, now()), (4, 98063939, now()), (4, 98164995, now()), (4, 98172631, now()), (4, 98243558, now()), (4, 98298737, now()), (4, 98324808, now()), (4, 98512187, now()), (4, 98622157, now()), (4, 98896302, now()), (4, 98951191, now()), (4, 99044892, now()), (4, 99096330, now()), (4, 99130658, now()), (4, 99150693, now()), (4, 99191647, now()), (4, 99274592, now()), (4, 99299588, now()), (4, 99302481, now()), (4, 99324289, now()), (4, 99349340, now()), (4, 99367125, now()), (4, 99436600, now()), (4, 99501300, now()), (4, 99756248, now()), (4, 99869383, now());

--
-- 2013-07-09 #34 - darabáze hologramů
--
ALTER TABLE `holograms`
	ALTER `id_orders` DROP DEFAULT,
	ALTER `hologram` DROP DEFAULT;
ALTER TABLE `holograms`
	CHANGE COLUMN `id_orders` `id_orders` INT(11) UNSIGNED NULL AFTER `id`,
	ADD COLUMN `id_parts` VARCHAR(10) NULL DEFAULT NULL AFTER `id_orders`;
	ADD COLUMN `id_categories` INT(11) UNSIGNED NULL AFTER `id_parts`,
	CHANGE COLUMN `hologram` `hologram` INT(8) NOT NULL COLLATE 'utf8_czech_ci' AFTER `id_categories`,
	CHANGE COLUMN `changed` `changed` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `hologram`,
	CHANGE COLUMN `inserted` `inserted` TIMESTAMP NOT NULL AFTER `changed`,
	DROP COLUMN `id_holograms_types`,
        ADD UNIQUE INDEX `hologram` (`hologram`);
DROP TABLE `holograms_types`;

--
-- 2013-07-09 #34 - databáze hologramů
--
ALTER TABLE `collections`
	ADD COLUMN `abbr` VARCHAR(2) NOT NULL DEFAULT '' COMMENT 'zkratka kolekce použitá v hologramech' AFTER `name`;
UPDATE `collections` SET `abbr`='CR' WHERE  `id`=1;
UPDATE `collections` SET `abbr`='SE' WHERE  `id`=2;
UPDATE `collections` SET `abbr`='NA' WHERE  `id`=3;
UPDATE `collections` SET `abbr`='SN' WHERE  `id`=4;

--
-- 2013-07-02 #234 - potvrzenou objednávku zpět do předběžných
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES (NULL, 'orders/special/status', 'ruční změna stavu objednávky');
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES ('admin', 'orders/special/status', 'ruční změna stavu objednávky');
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES ('manager', 'orders/special/status', 'ruční změna stavu objednávky');

--
-- 2013-07-01 #172 Výroba součástí / poznámka
--
ALTER TABLE `productions` DROP COLUMN `description`;

--
-- 2013-06-24 #227 Manualni administrace kontroly IP adresy
--
ALTER TABLE `users` ADD `check_ip` TINYINT( 1 ) NOT NULL DEFAULT '1' COMMENT 'flag - kontroluj IP ano/ne' AFTER `id`;

--
-- 2013-06-05 #114 - šablony - mazání
--
ALTER TABLE `orders_templates`
	ALTER `title` DROP DEFAULT,
	ALTER `description` DROP DEFAULT,
	ALTER `purchaser` DROP DEFAULT;
ALTER TABLE `orders_templates`
	CHANGE COLUMN `title` `title` VARCHAR(50) NULL COLLATE 'utf8_czech_ci' AFTER `id_orders_carriers`,
	CHANGE COLUMN `description` `description` VARCHAR(500) NULL COLLATE 'utf8_czech_ci' AFTER `title`,
	CHANGE COLUMN `purchaser` `purchaser` VARCHAR(500) NULL COLLATE 'utf8_czech_ci' AFTER `description`;

--
-- 2013-06-03 #216 koš objednávek - upgrade (datum výmazu)
--
ALTER TABLE `orders` CHANGE COLUMN `tracking_no` `tracking_no` VARCHAR(100) NULL DEFAULT NULL COMMENT 'trackovaci cislo objednavky u dodavatele' COLLATE 'utf8_czech_ci' AFTER `strategic_months`;
ALTER TABLE `orders` ADD COLUMN `date_delete` DATE NULL DEFAULT NULL COMMENT 'datum vymazání (TRASH)' AFTER `date_payment_accept`;
UPDATE orders SET date_delete = date_request WHERE `status` IN ('trash');

--
-- 2013-06-17 #147 - rozsireni tabulky producers - chybejici SQL
--

INSERT INTO `producers` (`id`, `id_extern_warehouses`, `id_producers_ctg`, `name`, `address`, `www`, `activity`, `description`, `id_orders_carriers`, `is_active`, `inserted`)
VALUES (NULL, NULL, '', 'DPD dobírka', NULL, NULL, NULL, NULL, '1', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'DHL express', NULL, NULL, NULL, NULL, '2', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'DHL freight', NULL, NULL, NULL, NULL, '3', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'ex works', NULL, NULL, NULL, NULL, '4', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'DSV Road  aircargo', NULL, NULL, NULL, NULL, '6', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'Geis CZ Air', NULL, NULL, NULL, NULL, '7', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'Pošta', NULL, NULL, NULL, NULL, '8', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'AWT ČR (DSV Road)', NULL, NULL, NULL, NULL, '10', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'AWT ČR (DSV Road) - dobírka', NULL, NULL, NULL, NULL, '11', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'Pošta - EMS balík-obálka (zahraničí)', NULL, NULL, NULL, NULL, '13', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'Logwin', NULL, NULL, NULL, NULL, '14', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'Pošta - dobírka ČR', NULL, NULL, NULL, NULL, '15', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'Pošta - prioritní balík-obálka (zahraničí)', NULL, NULL, NULL, NULL, '16', 'y', CURRENT_TIMESTAMP),
(NULL, NULL, '', 'Geis CZ', NULL, NULL, NULL, NULL, '18', 'y', CURRENT_TIMESTAMP);

--
-- 2013-05-24 #147 - rozsireni tabulky producers
--
ALTER TABLE `producers` ADD `id_orders_carriers` INT NULL AFTER `description`


--
-- 2013-05-25 #206 - 5ti mistne ID pro zakazniky
--
ALTER TABLE `customers` ADD COLUMN `customer_no` MEDIUMINT(5) UNSIGNED NULL DEFAULT NULL COMMENT 'náhodně generované unikátní id' AFTER `id`;
ALTER TABLE `customers`	ADD UNIQUE INDEX `customer_no` (`customer_no`);
UPDATE customers SET customer_no = FLOOR(10000 + RAND(321) * 89999);

--
-- 2013-05-12 #36 - pridani koncove euro ceny
--
ALTER TABLE `parts` ADD `end_price_eur` DECIMAL NOT NULL AFTER `old_price`

--
-- 2013-05-06 #211 - pridani prav pro admina
--
INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES (NULL, 'admin', 'orders/compare', 'compare orders', CURRENT_TIMESTAMP);

--
-- 2013-05-06 #211 - pridani resource - orders / compare
--
INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES (NULL, NULL, 'orders/compare', 'compare orders', CURRENT_TIMESTAMP);

--
-- 2013-05-06 #213 - pridani sloupce pro id objednavky
--
ALTER TABLE `emails` ADD `id_orders` INT NULL AFTER `body` ;

--
-- 2013-05-06 #204 - pridani priznaku aktivity dodavatele
--
ALTER TABLE `producers` ADD `is_active` CHAR( 1 ) NOT NULL DEFAULT 'y' AFTER `description` ;


--
-- 2013-03-29 #202 - prejmenovani a pridani sloupce pro pozici osoby
--

ALTER TABLE `producers_persons` CHANGE `position` `position_old` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_czech_ci NULL DEFAULT NULL ;
ALTER TABLE `producers_persons` ADD `position` VARCHAR( 50 ) NULL AFTER `position_old`;



--
-- 2013-03-16 #171 - pridani relacni tabulky pro slevu zakaznik-kategorie
--
CREATE TABLE `customers_parts_ctg_discount` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_customers` INT NOT NULL ,
`id_parts_ctg` INT NOT NULL ,
`discount` INT NOT NULL ,
`inserted` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
`updated` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB;


--
-- 2013-03-08 #171 - pridani relacni tabulky pro slevu zakaznik-produkt
--
CREATE TABLE `customers_products_discount` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_customers` INT NOT NULL ,
`id_products` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL ,
`discount` INT NOT NULL ,
`inserted` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
`updated` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB;


--
-- 2013-02-24 - pridani sloupce pro alternativni text na fakture
--
ALTER TABLE `invoice` ADD `text1` VARCHAR( 250 ) NULL AFTER `custom_item2_cost`


--
-- 2013-02-19 #195 - rozsireni PSC
--
ALTER TABLE `addresses` CHANGE `zip` `zip` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_czech_ci NULL DEFAULT NULL ;

--
-- 2013-02-15 #200 - pridani pole casu operace (int minuty)
--
ALTER TABLE `parts_operations`  ADD `time` INT NOT NULL DEFAULT '0' COMMENT 'minutes' AFTER `price`


--
-- 2013-02-14 #195 - rozsireni pole ulice v adrese
--
ALTER TABLE `addresses` CHANGE `street` `street` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_czech_ci NULL DEFAULT NULL


--
-- 2013-01-21 #190 přesuntuí vyexpedovaných objednávek
--

UPDATE `orders` SET `date_exp` = '2012-09-14' WHERE `orders`.`id` =360;
UPDATE `orders` SET `date_exp` = '2012-09-14' WHERE `orders`.`id` =361;
UPDATE `orders` SET `date_exp` = '2012-09-14' WHERE `orders`.`id` =362;
UPDATE `orders` SET `date_exp` = '2012-09-14' WHERE `orders`.`id` =363;
UPDATE `orders` SET `date_exp` = '2012-09-14' WHERE `orders`.`id` =364;
UPDATE `orders` SET `date_exp` = '2012-09-16' WHERE `orders`.`id` =365;
UPDATE `orders` SET `date_exp` = '2012-09-20' WHERE `orders`.`id` =366;
UPDATE `orders` SET `date_exp` = '2012-09-16' WHERE `orders`.`id` =367;
UPDATE `orders` SET `date_exp` = '2012-09-16' WHERE `orders`.`id` =368;
UPDATE `orders` SET `date_exp` = '2012-09-16' WHERE `orders`.`id` =369;


--
-- 2013-01-08 #182 - editace DPH - nastavení pro warehouse
--

-- pridani role pro menu
INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'admin', 'menu', CURRENT_TIMESTAMP),
(NULL, 'admin', 'admin', 'menu', CURRENT_TIMESTAMP),
(NULL, 'manager', 'admin', 'menu', CURRENT_TIMESTAMP);

-- novy resource admin/settings-dph pro admina
INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`) VALUES
(NULL, NULL, 'admin/settings-dph', 'definice role', CURRENT_TIMESTAMP),
(NULL, 'admin', 'admin/settings-dph', 'definice role', CURRENT_TIMESTAMP),
(NULL, 'manager', 'admin/settings-dph', 'definice role', CURRENT_TIMESTAMP);

-- table application_dph
CREATE TABLE `application_dph` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`value` TINYINT NOT NULL, `active_from_date` DATE NOT NULL,
	`inserted` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;


--
-- 2013-01-02 #164 - nový učet pro sklad
--

-- novy sloupec, pro definici uzivatelu, kteri mohou pristupovat k danemu externimu skladu
ALTER TABLE `extern_warehouses`
ADD `users` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_czech_ci NULL DEFAULT NULL
COMMENT 'id uzivatelu, kteri maji ke skladu pristup (pro controller extern)';

-- nova role "extern_wh"
ALTER TABLE `users` CHANGE `role` `role` ENUM( 'admin', 'manager', 'warehouseman', 'extern_wh', 'seller', 'brigadier' )
CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL;

-- nova role pridana v ACL
INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`)
VALUES ('6', 'extern_wh', NULL, 'externi skladnik', CURRENT_TIMESTAMP);

-- povoleni pristupu nove roli v ACL:
INSERT INTO `users_acl` (`id`, `role`, `resource`, `description`, `inserted`)
VALUES
(NULL, NULL, 'extern', 'definice resource', CURRENT_TIMESTAMP),
(NULL, NULL, 'extern/index', 'definice resource', CURRENT_TIMESTAMP),
(NULL, 'extern_wh', 'login', 'kvuli menu', CURRENT_TIMESTAMP),
(NULL, 'extern_wh', 'login/index', '', CURRENT_TIMESTAMP),
(NULL, 'extern_wh', 'login/clear', '', CURRENT_TIMESTAMP),
(NULL, 'extern_wh', 'login/process', '', CURRENT_TIMESTAMP),
(NULL, 'extern_wh', 'index/index', NULL, CURRENT_TIMESTAMP),
(NULL, 'extern_wh', 'index/extend', NULL, CURRENT_TIMESTAMP),
(NULL, 'extern_wh', 'extern', '', CURRENT_TIMESTAMP),
(NULL, 'extern_wh', 'extern/index', '', CURRENT_TIMESTAMP),
(NULL, 'admin', 'extern', '', CURRENT_TIMESTAMP),
(NULL, 'admin', 'extern/index', '', CURRENT_TIMESTAMP);

-- novy uzivatel systemu v nove roli
INSERT INTO `users` (`id`, `username`, `name`, `name_full`, `password`, `email`, `phone`, `role`, `description`)
VALUES (NULL, 'starya', 'AS', 'Antonín Starý', 'xxx', 'vyroba@medusepipes.com', '', 'extern_wh', 'definice role');

