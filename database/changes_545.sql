--
-- Toto je soubor pro databazove zmeny v rámci
-- issue #545 - DS - Docházkový subsytem
--
-- Zmeny vkladame na zacatek souboru - ne na konec (nejnovejsi zmeny jsou nahore).
-- Zmenu v tomto souboru nesmi obsahovat cele nazvy tabulek (ne db_name.db_table, jen db_table).
-- Kazda zmena databaze musi byt uvozena komentarem ve tvaru:
--
-- YYYY-MM-DD #123 - název issue z eventum

--
-- 2016-09-19 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees`
	ADD COLUMN `birthday` DATE NULL DEFAULT NULL COMMENT 'datum narozeni' AFTER `insurance_code`;

--
-- 2016-09-15 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees_rewards`
	ADD COLUMN `overtime_cash` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'hodiny k proplaceni' AFTER `transport_bonus`;

INSERT INTO `users_acl` (role, resource, description) VALUES
(NULL, 'employees/reward-add', 'vlozeni odmeny'),
('dept_leader', 'employees/reward-add', NULL),
('manager', 'employees/reward-add', NULL),
('admin', 'employees/reward-add', NULL);

INSERT INTO `users_acl` (role, resource, description) VALUES
(NULL, 'employees/reward-remove', 'zruseni odmeny'),
('dept_leader', 'employees/reward-remove', NULL),
('manager', 'employees/reward-remove', NULL),
('admin', 'employees/reward-remove', NULL);

INSERT INTO `users_acl` (role, resource, description) VALUES
(NULL, 'employees/overtime-cash', 'zpenezeni prescasu'),
('dept_leader', 'employees/overtime-cash', NULL),
('manager', 'employees/overtime-cash', NULL),
('admin', 'employees/overtime-cash', NULL);

INSERT INTO `users_acl` (role, resource, description) VALUES
(NULL, 'employees/view-month', 'prehled mesice'),
('dept_leader', 'employees/view-month', NULL),
('manager', 'employees/view-month', NULL),
('admin', 'employees/view-month', NULL);

--
-- 2016-09-11 #545 - DS - Docházkový subsystém
--
UPDATE users_acl SET resource = 'employees/settings'
WHERE resource LIKE 'employees/diets-settings';

--
-- 2016-09-02 #545 - DS - Docházkový subsystém
--
INSERT INTO `users_acl` (role, resource, description) VALUES
(NULL, 'employees/account-report', 'prehled dovolenych a nemocenske'),
('admin', 'employees/account-report', NULL),
('manager', 'employees/account-report', NULL),
('accountant_pipes', 'employees/account-report', NULL),
('accountant_tobacco', 'employees/account-report', NULL);

--
-- 2016-09-01 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees`
	ADD COLUMN `nda` ENUM('y','n') NULL DEFAULT 'n' COMMENT 'dohoda o mlcenlivosti' AFTER `insurance`;

ALTER TABLE `employees`
	CHANGE COLUMN `bonus` `bonus` SMALLINT(5) UNSIGNED NULL DEFAULT NULL COMMENT 'mzdovy bonus' AFTER `wage`;

--
-- 2016-08-25 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees_rewards`
	ADD COLUMN `vacation` DECIMAL(5,2) NOT NULL DEFAULT '0.00' COMMENT 'pocet dni dovolene' AFTER `debt`,
	ADD COLUMN `sickness` DECIMAL(5,2) NOT NULL DEFAULT '0.00' COMMENT 'pocet dni nemoci' AFTER `vacation`;

INSERT INTO `users_acl` (role, resource, description) VALUES
('dept_leader', 'employees/show-month', NULL),
('manager', 'employees/show-month', NULL),
('employee', 'employees/show-month', NULL);
INSERT INTO `users_acl` (role, resource, description) VALUES
('dept_leader', 'employees/special/show-wage', NULL);
--
-- 2016-08-18 #545 - DS - Docházkový subsystém
--
CREATE TABLE `employees_rewards_dept` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'primarni klic',
	`id_employees_rewards` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na odmenu',
	`year_employees_rewards` YEAR(4) NOT NULL COMMENT 'cizi klic na odmenu',
	`month_employees_rewards` TINYINT(2) UNSIGNED ZEROFILL NOT NULL COMMENT 'cizi klic',
	`id_users` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na udelujici osobu',
	`value` DECIMAL(10,2) NOT NULL COMMENT 'vyse odmeny/penale',
	`description` VARCHAR(50) NOT NULL COMMENT 'popis odmeny',
	PRIMARY KEY (`id`),
	INDEX `id_employees_rewards` (`id_employees_rewards`),
	INDEX `year_employees_rewards` (`year_employees_rewards`),
	INDEX `month_employees_rewards` (`month_employees_rewards`),
	INDEX `id_users` (`id_users`),
	CONSTRAINT `FK_employees_rewards_dept` FOREIGN KEY (`id_employees_rewards`, `year_employees_rewards`, `month_employees_rewards`) REFERENCES `employees_rewards` (`id_employees`, `year`, `month`),
	CONSTRAINT `FK_employees_rewards_dept_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
)
COMMENT='odmeny nebo penale od vedouciho oddeleni'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;


--
-- 2016-08-15 #545 - DS - Docházkový subsystém
--
INSERT INTO `users_acl` (role, resource, description) VALUES
(NULL, 'employees/open-month', 'zalozeni noveho mesice'),
('admin', 'employees/open-month', NULL),
('dept_leader', 'employees/open-month', NULL),
('manager', 'employees/open-month', NULL),
('employee', 'employees/open-month', NULL);

INSERT INTO `users_acl` (role, resource, description) VALUES
('dept_leader', 'employees/close-month', NULL),
('dept_leader', 'employees/reopen-month', NULL);

--
-- 2016-08-07 #545 - DS - Docházkový subsystém
--
INSERT INTO `users_acl` (`role`, `resource`) VALUES
('employee', 'employees/fetch-dsi-records'),
('dept_leader', 'employees/fetch-dsi-records');

--
-- 2016-08-04 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees_works`
	CHANGE COLUMN `type` `type` ENUM('work','bustrip','bustrip_frgn','hoffice','overtime','vacation','sickness') NULL DEFAULT NULL COMMENT 'typ pracovniho zaznamu' COLLATE 'utf8_czech_ci' AFTER `date`;

--
-- 2016-07-27 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees_works`
	RENAME TO `employees_records`,
	COMMENT='zaznamy z dochazkove ctecky';

CREATE TABLE `employees_works` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_users` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic na uzivatele',
	`id_employees` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na pracovnika',
	`date` DATE NOT NULL COMMENT 'casova znacka zaznamu',
	`type` ENUM('work','bustrip','bustrip_frgn','hoffice','overtime','vacation','sickness') NOT NULL COMMENT 'typ pracovniho zaznamu' COLLATE 'utf8_czech_ci',
	`start` DATETIME NULL DEFAULT NULL COMMENT 'zacatek',
	`end` DATETIME NULL DEFAULT NULL COMMENT 'konec',
	`km` DECIMAL(10,1) NULL DEFAULT NULL COMMENT 'pocet najetych kilometru',
	`description` VARCHAR(50) NULL DEFAULT NULL COMMENT 'poznamka' COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`id`),
	INDEX `FK_employees_works2_employees` (`id_employees`),
	INDEX `id_users` (`id_users`),
	CONSTRAINT `FK_employees_works2_employees` FOREIGN KEY (`id_employees`) REFERENCES `employees` (`id`),
	CONSTRAINT `FK_employees_works2_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
)
COMMENT='polozky worksheetu'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;


ALTER TABLE `employees_records`
	ALTER `id_employees` DROP DEFAULT;
ALTER TABLE `employees_records`
	CHANGE COLUMN `id_employees` `id_employees` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na pracovniky' AFTER `id`,
	CHANGE COLUMN `id_users` `id_users` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'NEPOUZIVAT autor zaznamu nebo zmeny' AFTER `id_employees`,
	CHANGE COLUMN `type` `type` ENUM('work','bustrip','bustrip_frgn','hoffice','overtime','vacation','sickness') NULL DEFAULT NULL COMMENT 'NEPOUZIVAT typ prace' COLLATE 'utf8_czech_ci' AFTER `id_users`,
	CHANGE COLUMN `timestamp_changed` `timestamp_changed` TIMESTAMP NULL DEFAULT NULL COMMENT 'NEPOUZIVAT casova znacka (po zmene)' AFTER `timestamp`,
	CHANGE COLUMN `description` `description` VARCHAR(50) NULL DEFAULT NULL COMMENT 'NEPOUZIVAT popis ukonu' COLLATE 'utf8_czech_ci' AFTER `status`,
	CHANGE COLUMN `km` `km` DECIMAL(10,1) NULL DEFAULT NULL COMMENT 'NEPOUZIVAT ujete kilometry' AFTER `description`,
	CHANGE COLUMN `deleted` `deleted` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'NEPOUZIVAT vymazano' COLLATE 'utf8_czech_ci' AFTER `km`;


--
-- 2015-06-29 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees_works`
	CHANGE COLUMN `type` `type` ENUM('work','bustrip','bustrip_frgn','hoffice','overtime','vacation','sickness') NULL DEFAULT NULL COMMENT 'typ prace' COLLATE 'utf8_czech_ci' AFTER `id_users`;
ALTER TABLE `employees_rewards`
	ADD COLUMN `diets` TINYINT UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'pocet dietnich dnu' AFTER `km`,
	ADD COLUMN `diets_frgn` TINYINT UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'pocet dietnich dnu v zahranici' AFTER `diets`;
ALTER TABLE `employees_rewards`
	CHANGE COLUMN `diets` `diets` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'pocet dietnich dnu' AFTER `km`,
	CHANGE COLUMN `diets_frgn` `diets_frgn` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'pocet dietnich dnu v zahranici' AFTER `diets`;

--
-- 2015-06-28 #545 - DS - Docházkový subsystém
--
INSERT INTO `users_acl` (`id`, `role`) VALUES (17, 'dept_leader');
INSERT INTO `users_roles` (`is_active`, `role_name`, `role_translation`, `role_description`) VALUES (1, 'dept_leader', 'Vedoucí oddělení', 'Vedoucí oddělení');
INSERT INTO `users_acl` (`role`, `resource`) VALUES
('dept_leader', 'employees/admin'),
('dept_leader', 'employees/dashboard'),
('dept_leader', 'employees/index'),
('manager', 'employees/index'),
('dept_leader', 'index/switch-warehouse'),
('dept_leader', 'index/index'),
('dept_leader', 'index/report-bug'),
('dept_leader', 'login/clear'),
('dept_leader', 'login/index'),
('dept_leader', 'login/process'),
('dept_leader', 'admin/user-pwd'),
('employee', 'index/switch-warehouse'),
('employee', 'index/index'),
('employee', 'index/report-bug'),
('employee', 'login/clear'),
('employee', 'login/index'),
('employee', 'login/process'),
('employee', 'admin/user-pwd');


INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/special/show-wage', 'definice resource'),
('admin', 'employees/special/show-wage', NULL),
('manager', 'employees/special/show-wage', NULL);

ALTER TABLE `employees_works`
	ADD COLUMN `km` DECIMAL(10,1) NULL DEFAULT NULL COMMENT 'ujete kilometry' AFTER `description`;
ALTER TABLE `employees_rewards`
	ADD COLUMN `car` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'za pouzivani auta' AFTER `bonus`;
ALTER TABLE `employees_rewards`
	ADD COLUMN `km` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'ujete kilometry' AFTER `debt`;

ALTER TABLE `employees_rewards`
	CHANGE COLUMN `extra` `personal_bonus` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'osobni priplatek' AFTER `base`,
	ADD COLUMN `transport_bonus` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'priplatek na dopravu' AFTER `personal_bonus`,
	ADD COLUMN `overtime_bonus` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'priplatek za prescas' AFTER `transport_bonus`,
	CHANGE COLUMN `car` `car_bonus` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'za pouzivani auta' AFTER `overtime_bonus`,
	CHANGE COLUMN `reward` `dept_reward` DECIMAL(7,2) NOT NULL DEFAULT '0.00' COMMENT 'odmena/penale od vedouciho' AFTER `car_bonus`,
	CHANGE COLUMN `bonus` `main_reward` DECIMAL(7,2) NOT NULL DEFAULT '0.00' COMMENT 'hlavni odmena/penale' AFTER `dept_reward`;
ALTER TABLE `employees_rewards`
	ADD COLUMN `note` TEXT NULL DEFAULT NULL COMMENT 'poznamka' AFTER `status`;
ALTER TABLE `employees_rewards`
	CHANGE COLUMN `base` `base` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'mzdovy zaklad / hodinova sazba' AFTER `km`,
	CHANGE COLUMN `overtime_bonus` `overtime_bonus` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'priplatek za prescas / odmena' AFTER `transport_bonus`;

--
-- 2015-06-27 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees_works`
	CHANGE COLUMN `timestamp` `timestamp` TIMESTAMP NULL DEFAULT NULL COMMENT 'casova znacka' AFTER `type`;

--
-- 2015-05-19 #545 - DS - Docházkový subsystém
--
ALTER TABLE `departments`
	ADD COLUMN `id_wh_type` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na firmu' AFTER `id_users`,
	ADD INDEX `id_wh_type` (`id_wh_type`);
ALTER TABLE `employees`
	DROP COLUMN `id_wh_type`;

ALTER TABLE `employees_works`
	ALTER `status` DROP DEFAULT;
ALTER TABLE `employees_works`
	ADD COLUMN `type` ENUM('work', 'bustrip', 'bustrip_frgn', 'hoffice', 'overtime', 'vacation', 'sickness') NULL COMMENT 'typ prace' AFTER `id_users`;


--
-- 2015-05-19 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees`
	ADD COLUMN `business_phone_pin` VARCHAR(4) NULL DEFAULT NULL COMMENT 'firemni telefon - PIN' AFTER `business_phone`,
	ADD COLUMN `computer_name` VARCHAR(20) NULL DEFAULT NULL COMMENT 'nazev firemniho pocitace' AFTER `business_phone_pin`,
	ADD COLUMN `computer_login` VARCHAR(20) NULL DEFAULT NULL COMMENT 'login do pc' AFTER `computer_name`,
	ADD COLUMN `computer_password` VARCHAR(20) NULL DEFAULT NULL COMMENT 'heslo do pc' AFTER `computer_login`,
	ADD COLUMN `insurance` ENUM('y','n') NULL DEFAULT 'n' COMMENT 'pojisteni odpovednosti' AFTER `work_class`;

--
-- 2015-05-02 #545 - DS - Docházkový subsystém
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/bustrip-add', 'definice resource'),
('admin', 'employees/bustrip-add', NULL),
('manager', 'employees/bustrip-add', NULL),
('employee', 'employees/bustrip-add', NULL);

--
-- 2015-03-17 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees`
	ADD COLUMN `alveno` SMALLINT(4) UNSIGNED NULL DEFAULT NULL COMMENT 'id z alvena' AFTER `type`;
DROP TABLE `employees_fingerprints`;

--
-- 2015-03-11 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees`
	DROP FOREIGN KEY `fk_id_users_employees`;
ALTER TABLE `employees`
	CHANGE COLUMN `overtime_rate` `overtime_rate` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'koeficient sazby za normalni prescas' AFTER `vacation`,
	CHANGE COLUMN `overtime_extra_rate` `overtime_extra_rate` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'koeficient sazby za extra prescas' AFTER `overtime_rate`;
ALTER TABLE `employees`
	CHANGE COLUMN `obligation` `obligation` VARCHAR(5) NULL DEFAULT NULL COMMENT 'mesicni uvazek - zlomek nebo cislo' COLLATE 'utf8_czech_ci' AFTER `work_class`;
ALTER TABLE `employees_rewards`
	ALTER `month` DROP DEFAULT;
ALTER TABLE `employees_rewards`
	CHANGE COLUMN `month` `month` TINYINT(2) ZEROFILL NOT NULL COMMENT 'mesic' AFTER `year`;

--
-- 2015-03-02 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees_rewards`
	CHANGE COLUMN `hours` `total` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'pocet odpracovanych hodin za mesic' AFTER `month`,
	ADD COLUMN `hours` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'pocet hodin v mesici' AFTER `total`,
	CHANGE COLUMN `closed` `status` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'stav' COLLATE 'utf8_czech_ci' AFTER `bonus`;

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/reopen-month', 'definice resource'),
('admin', 'employees/reopen-month', NULL),
('manager', 'employees/reopen-month', NULL);

ALTER TABLE `employees_rewards`
	CHANGE COLUMN `debt` `debt` DECIMAL(5,2) NOT NULL DEFAULT '0.00' COMMENT 'dluh/prebytek z minula' AFTER `hours`,
	CHANGE COLUMN `base_netto` `extra` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'priplatky' AFTER `base`,
	CHANGE COLUMN `reward` `reward` DECIMAL(7,2) NOT NULL DEFAULT '0.00' COMMENT 'odmena' AFTER `extra`;
--
-- 2015-02-29 #545 - DS - Docházkový subsystém
--
ALTER TABLE `employees_works`
	ADD COLUMN `id_users` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `id_employees`,
	ADD INDEX `id_users` (`id_users`);

--
-- 2015-02-22 #545 - DS - Docházkový subsystém
--
CREATE TABLE `employees_fingerprints` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_employees` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na pracovnika',
	`fingerprint` TINYINT UNSIGNED NOT NULL COMMENT 'id otisku',
	PRIMARY KEY (`id`),
	INDEX `id_employees` (`id_employees`),
	INDEX `fingerprint` (`fingerprint`),
	CONSTRAINT `fk_id_employees` FOREIGN KEY (`id_employees`) REFERENCES `employees` (`id`)
)
COMMENT='id otisku ze ctecky'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/fetch-dsi-records', 'definice resource'),
('admin', 'employees/fetch-dsi-records', NULL),
('manager', 'employees/fetch-dsi-records', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/recalculate', 'definice resource'),
('admin', 'employees/recalculate', NULL),
('manager', 'employees/recalculate', NULL),
('employee', 'employees/recalculate', NULL);

--
-- 2015-02-15 #545 - DS - Docházkový subsystem
--
INSERT into users_acl (role, resource) VALUES ('manager', 'employees/index');

--
-- 2015-02-09 #545 - DS - Docházkový subsystem
--
ALTER TABLE `employees`
	ADD COLUMN `account_number` VARCHAR(50) NULL DEFAULT NULL COMMENT 'cislo bankovniho uctu' AFTER `birthplace`,
	ADD COLUMN `business_phone` VARCHAR(20) NULL DEFAULT NULL COMMENT 'firemni telefon' AFTER `account_number`,
	ADD COLUMN `private_phone` VARCHAR(20) NULL DEFAULT NULL COMMENT 'osobni telefon' AFTER `business_phone`,
	ADD COLUMN `business_email` VARCHAR(50) NULL DEFAULT NULL COMMENT 'firemni email' AFTER `private_phone`,
	ADD COLUMN `private_email` VARCHAR(50) NULL DEFAULT NULL COMMENT 'soukromý email' AFTER `business_email`,
	CHANGE COLUMN `wage_brutto` `wage` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'zaklad mzy' AFTER `obligation`,
	CHANGE COLUMN `wage_netto` `bonus` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'mzdovy bonus' AFTER `wage`,
	ADD COLUMN `transport_allowance` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'prispevek na dopravu' AFTER `bonus`,
	ADD COLUMN `diets` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'zahranicni dieta' AFTER `overtime_extra_rate`;

--
-- 2015-02-04 #545 - DS - Docházkový subsystem
--
ALTER TABLE `employees`
	ADD COLUMN `id_wh_type` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic skladu' AFTER `id_departments`,
	ADD COLUMN `type` ENUM('employee','supplier') NULL DEFAULT 'employee' COMMENT 'typ vztahu' AFTER `id_wh_type`,
	ADD COLUMN `insurance_code` VARCHAR(3) NULL DEFAULT NULL COMMENT 'kod pojistovny' AFTER `last_name`,
	ADD COLUMN `birthplace` VARCHAR(50) NULL DEFAULT NULL COMMENT 'misto narozeni' AFTER `insurance_code`,
	ADD COLUMN `work_class` ENUM('1','2','3') NULL DEFAULT '1' COMMENT 'pracovni trida' AFTER `position`,
	CHANGE COLUMN `obligation` `obligation` VARCHAR(5) NOT NULL DEFAULT '168' COMMENT 'mesicni uvazek - zlomek nebo cislo' AFTER `work_class`;

--
-- 2015-01-28 #545 - DS - Docházkový subsystem
--
ALTER TABLE `employees_vacation`
	RENAME TO `employees_absence`,
	COMMENT='zaznamy nepritomnosti, jako dovolena, nemoc, neplacene volno a take neomluvena absence',
	ADD COLUMN `type` ENUM('vacation', 'illness', 'other', 'free', 'banned') NOT NULL DEFAULT 'other' COMMENT 'typ nepritomnosti' AFTER `id_employees`,
	ADD COLUMN `description` VARCHAR(50) NULL DEFAULT NULL COMMENT 'poznamka' AFTER `total`,
	ADD INDEX `type` (`type`);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/absence-add', 'definice resource'),
('admin', 'employees/absence-add', NULL),
('manager', 'employees/absence-add', NULL);

CREATE TABLE `employees_bustrips` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_employees` INT(11) UNSIGNED NOT NULL COMMENT 'fk na zamestanace',
	`start` DATE NOT NULL COMMENT 'zacatek',
	`end` DATE NOT NULL COMMENT 'konec',
	`region` ENUM('cz/sk', 'other') NOT NULL DEFAULT 'cz/sk' COMMENT 'region',
	`description` VARCHAR(50) NULL DEFAULT NULL COMMENT 'poznamka',
	PRIMARY KEY (`id`),
	CONSTRAINT `fk_bsstrip_employees` FOREIGN KEY (`id_employees`) REFERENCES `employees` (`id`)
)
COMMENT='pracovni cesty'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2015-01-26 #545 - DS - Docházkový subsystem
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/vacation', 'definice resource'),
('admin', 'employees/vacation', NULL),
('manager', 'employees/vacation', NULL);

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/vacation-approve', 'definice resource'),
('admin', 'employees/vacation-approve', NULL),
('manager', 'employees/vacation-approve', NULL),
(NULL, 'employees/vacation-reject', 'definice resource'),
('admin', 'employees/vacation-reject', NULL),
('manager', 'employees/vacation-reject', NULL);

--
-- 2015-01-25 #545 - DS - Docházkový subsystem
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/diets-settings', 'definice resource'),
('admin', 'employees/diets-settings', NULL),
('manager', 'employees/diets-settings', NULL);

CREATE TABLE `settings` (
	`name` VARCHAR(50) NOT NULL COMMENT 'nazev promenne',
	`value` VARCHAR(50) NOT NULL COMMENT 'hodnota promenne',
	`type` SET('int', 'float', 'bool', 'string') NULL DEFAULT 'string' COMMENT 'typ promenne',
	`description` VARCHAR(100) NULL DEFAULT NULL COMMENT 'popis promenne',
	`modified` TIMESTAMP NOT NULL COMMENT 'posledni zmena',
	PRIMARY KEY (`name`)
)
COMMENT='systemove uloziste ruznych promennych'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

ALTER TABLE `employees`
	ADD COLUMN `overtime_rate` FLOAT UNSIGNED NOT NULL DEFAULT '1.25' COMMENT 'koeficient sazby za normalni prescas' AFTER `vacation`,
	ADD COLUMN `overtime_extra_rate` FLOAT UNSIGNED NOT NULL DEFAULT '1.50' COMMENT 'koeficient sazby za extra prescas' AFTER `overtime_rate`;


INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/vacation-add', 'definice resource'),
('admin', 'employees/vacation-add', NULL),
('employee', 'employees/vacation-add', NULL);

--
-- 2015-01-18 #545 - DS - Docházkový subsytem
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/ajax-edit-work', 'definice resource'),
('admin', 'employees/ajax-edit-work', NULL),
('manager', 'employees/ajax-edit-work', NULL),
('employee', 'employees/ajax-edit-work', NULL);

--
-- 2015-01-20 #545 - DS - Docházkový subsytem
--
UPDATE users_acl SET resource = 'employees/worksheet' WHERE resource = 'employees/work';

--
-- 2015-01-18 #545 - DS - Docházkový subsytem
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/dashboard', 'definice resource'),
('admin', 'employees/dashboard', NULL),
('manager', 'employees/dashboard', NULL);
UPDATE
 users_acl SET resource = 'employees/admin' WHERE resource = 'employees-admin/index';
UPDATE users_acl SET resource = CONCAT ('employees', SUBSTRING(resource FROM 16)) WHERE resource LIKE 'employees-admin%';

INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees/work', 'definice resource'),
('admin', 'employees/work', NULL),
('manager', 'employees/work', NULL),
('employee', 'employees/work', NULL);

--
-- 2015-01-13 #545 - DS - Docházkový subsytem
--
INSERT INTO `users_acl` (`role`, `resource`, `description`) VALUES
(NULL, 'employees-admin/close-month', 'definice resource'),
('admin', 'employees-admin/close-month', NULL),
('manager', 'employees-admin/close-month', NULL);

--
-- 2015-01-11 #545 - DS - Docházkový subsytem
--
CREATE TABLE `departments` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL COMMENT 'nazev oddeleni',
	`id_users` INT(11) UNSIGNED NOT NULL COMMENT 'cizi odkaz na vedouciho oddeleni',
	PRIMARY KEY (`id`),
	CONSTRAINT `fk_id_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
)
COMMENT='jednotliva oddeneni'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

ALTER TABLE `employees`
  ADD CONSTRAINT `fk_id_users_employees` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`),
	ADD COLUMN `id_departments` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic oddeleni' AFTER `id_users`,
	ADD CONSTRAINT `fk_id_departmens_employees` FOREIGN KEY (`id_departments`) REFERENCES `departments` (`id`);

CREATE TABLE `budgets` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_departments` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na oddeleni',
	`from` DATE NOT NULL COMMENT 'zacatek obodbi',
	`to` DATE NOT NULL COMMENT 'konec obdobi',
	`amount` MEDIUMINT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'castka v kc',
	PRIMARY KEY (`id`),
	CONSTRAINT `fk_id_departments_budgets` FOREIGN KEY (`id_departments`) REFERENCES `departments` (`id`)
)
COMMENT='prideleny rozpocet jednotlivym oddelenim'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;


INSERT INTO `users_acl` (`resource`, `description`) VALUES
('departments/index', 'definice resource'),
('departments/add', 'definice resource'),
('departments/edit', 'definice resource'),
('departments/remove', 'definice resource'),
('departments/budget', 'definice resource');

INSERT INTO `users_acl` (`role`, `resource`) VALUES
('admin', 'departments/index'), ('manager', 'departments/index'),
('admin', 'departments/add'), ('manager', 'departments/add'),
('admin', 'departments/edit'), ('manager', 'departments/edit'),
('admin', 'departments/remove'), ('manager', 'departments/remove'),
('admin', 'departments/budget'), ('manager', 'departments/budget');

--
-- 2015-12-21 #545 - DS - Docházkový subsytem
--
ALTER TABLE `employees`
	CHANGE COLUMN `wage` `wage_brutto` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'mesicni hruba mzda' AFTER `obligation`,
	ADD COLUMN `wage_netto` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'mesicni cista mzda' AFTER `wage_brutto`;
ALTER TABLE `employees_attendance`
	RENAME TO `employees_rewards`,
	ADD COLUMN `debt` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'dluh/prebytek z minula' AFTER `hours`,
	ADD COLUMN `base` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'mzdovy zaklad' AFTER `debt`,
	ADD COLUMN `base_netto` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'mzdovy zaklad - cisty' AFTER `base`,
	ADD COLUMN `reward` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'odmena' AFTER `base_netto`,
	ADD COLUMN `bonus` DECIMAL(7,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'bonus' AFTER `reward`;

CREATE TABLE `employees_works` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_employees` INT(11) UNSIGNED NOT NULL,
	`timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'casova znacka',
	`status` ENUM('checkin', 'breakin', 'breakout', 'checkout') NOT NULL COMMENT 'casova znacka',
	`description` VARCHAR(50) NULL COMMENT 'popis ukonu',
	PRIMARY KEY (`id`),
	INDEX `id_employees` (`id_employees`)
)
COMMENT='prace/dochazka zamestnancu'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;


INSERT INTO `users_acl` (`resource`, `description`) VALUES
('employees/set-status', 'definice resource');
INSERT INTO `users_acl` (`role`, `resource`) VALUES
('admin', 'employees/set-status'),
('employee', 'employees/set-status');

--
-- 2015-12-16 #545 - DS - Docházkový subsystem
--
INSERT INTO `users_acl` (`resource`, `description`) VALUES
('employees-admin/ajax-get-user-info', 'definice resource');
INSERT INTO `users_acl` (`role`, `resource`) VALUES
('admin', 'employees-admin/ajax-get-user-info'),
('manager', 'employees-admin/ajax-get-user-info');


--
-- 2015-12-15 #545 - DS - Docházkový subsystem
--
INSERT INTO `users_roles` (`is_active`, `role_name`, `role_translation`, `role_description`)
VALUES (1, 'employee', 'Zaměstnanec', 'obecná role zaměstnance');
INSERT INTO `users_acl` (`role`, `description`)
VALUES ('employee', 'definice role');
INSERT INTO `users_acl` (`resource`, `description`)
VALUES ('employees/index', 'definice resource');
INSERT INTO `users_acl` (`role`, `resource`)
VALUES ('admin', 'employees/index'), ('employee', 'employees/index');

CREATE TABLE `employees` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_users` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic na uzivatele systemu',
	`first_name` VARCHAR(50) NOT NULL COMMENT 'jmeno',
	`last_name` VARCHAR(50) NOT NULL COMMENT 'prijmeni',
	`start` DATE NULL DEFAULT NULL COMMENT 'datum nastupu',
	`end` DATE NULL DEFAULT NULL COMMENT 'datum ukonceni',
	`position` VARCHAR(50) NULL DEFAULT NULL COMMENT 'pozice',
	`obligation` SMALLINT UNSIGNED NOT NULL DEFAULT '168' COMMENT 'mesicni uvazek',
	`wage` SMALLINT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'hodinova mzda',
	`vacation` SMALLINT UNSIGNED NOT NULL DEFAULT '20' COMMENT 'rocni narok dovolene',
	PRIMARY KEY (`id`)
)
COMMENT='tabulka zamestnancu'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE `employees_attendance` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_employees` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na zamestnance',
	`year` YEAR NOT NULL COMMENT 'rok',
	`month` TINYINT NOT NULL COMMENT 'mesic',
	`hours` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'pocet hodin za mesic',
	`closed` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'stav' COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`id`),
	INDEX `id_employees` (`id_employees`),
	CONSTRAINT `FK_employees_attendance_employees` FOREIGN KEY (`id_employees`) REFERENCES `employees` (`id`)
)
COMMENT='mesicni dochazka'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE `employees_vacation` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_employees` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na zamestnance',
	`start` DATE NOT NULL COMMENT 'zacatek',
	`end` DATE NOT NULL COMMENT 'konec',
	`total` TINYINT UNSIGNED NOT NULL COMMENT 'pocet pracovnich dni',
	`approved` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'schvaleno',
	PRIMARY KEY (`id`),
	INDEX `id_employees` (`id_employees`),
	CONSTRAINT `FK__employees` FOREIGN KEY (`id_employees`) REFERENCES `employees` (`id`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

INSERT INTO `users_acl` (`resource`, `description`) VALUES
('employees-admin/index', 'definice resource'),
('employees-admin/add', 'definice resource'),
('employees-admin/edit', 'definice resource');

INSERT INTO `users_acl` (`role`, `resource`) VALUES
('admin', 'employees-admin/index'),
('manager', 'employees-admin/index'),
('admin', 'employees-admin/add'),
('manager', 'employees-admin/add'),
('admin', 'employees-admin/edit'),
('manager', 'employees-admin/edit');
