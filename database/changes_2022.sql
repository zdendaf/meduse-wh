--
-- #321: Tvorba ceníků
--
create table pricelists_ctg
(
    id       int                           not null comment 'id slevove kategorie',
    discount decimal(2) unsigned default 0 not null comment 'vyse slevy v procentech',
    constraint pricelists_ctg_pk
        primary key (id)
) comment 'slevove kategorie s vychozimi slevami';

insert into pricelists_ctg (id, discount)
values (1, 0),
       (2, 5),
       (3, 10),
       (4, 15),
       (5, 20),
       (6, 25),
       (7, 30),
       (8, 35);

alter table pricelists
    add id_pricelists_ctg int default NULL null comment 'cizi klic na cenikove kategorie' after id_wh_type,
    add master bool default false null comment 'jedna se o master cenik?';
    -- add constraint pricelists_pricelists_ctg_id_fk
    -- foreign key (id_pricelists_ctg) references pricelists_ctg (id);

alter table customers
    add id_pricelists_ctg int default NULL null comment 'cizi klic na slevove kategorie' after id_addresses2;
    -- add constraint customers_pricelists_ctg_id_fk
    --    foreign key (id_pricelists_ctg) references pricelists_ctg (id);

alter table orders
    add id_pricelists_ctg int default null null comment 'cizi klic na slevove kategorie' after id_pricelists;
    -- add constraint orders_pricelists_ctg_id_fk
    --    foreign key (id_pricelists_ctg) references pricelists_ctg (id);

create table parts_pricelists_ctg
(
    id                int(11) unsigned auto_increment,
    id_parts          varchar(10)         not null comment 'cizi klic na produkt',
    id_pricelists_ctg int                 not null comment 'cizi klic na kategorii',
    discount          decimal(2) unsigned not null comment 'vyse slevy v procentech',
    constraint parts_pricelists_ctg_pk
        primary key (id),
    constraint parts_pricelists_ctg_parts_id_fk
        foreign key (id_parts) references parts (id),
    constraint parts_pricelists_ctg_pricelists_ctg_id_fk
        foreign key (id_pricelists_ctg) references pricelists_ctg (id)
) comment 'slevove kategorie pro produkty';

create table collections_pricelists_ctg
(
    id                int(11) unsigned auto_increment,
    id_collections    int                 null comment 'cizi klic na kolekce',
    id_pricelists_ctg int                 not null comment 'cizi klic na slevovou kategorii',
    discount          decimal(2) unsigned not null comment 'vyse slevy v procentech',
    constraint collections_pricelists_ctg_pk
        primary key (id),
    constraint collections_pricelists_ctg_collections_id_fk
        foreign key (id_collections) references collections (id),
    constraint collections_pricelists_ctg_pricelists_ctg_id_id_fk
        foreign key (id_pricelists_ctg) references pricelists_ctg (id)
)
    comment 'slevove kategorie kolekci';

insert into users_acl (role, resource, description) values
(NULL, 'pricelists/edit-ctg', 'editace slevovych kategorii'),
('admin', 'pricelists/edit-ctg', 'editace slevovych kategorii'),
('manager', 'pricelists/edit-ctg', 'editace slevovych kategorii');

insert into users_acl (role, resource, description)
values (NULL, 'pricelists/ctg', 'prehled slevovych kategorii'),
       ('admin', 'pricelists/ctg', 'prehled slevovych kategorii'),
       ('manager', 'pricelists/ctg', 'prehled slevovych kategorii');

alter table collections_pricelists_ctg
    drop primary key,
    drop column id,
    add constraint collections_pricelists_ctg_pk
        primary key (id_pricelists_ctg, id_collections);

alter table parts_pricelists_ctg
    drop primary key,
    drop column id,
    add constraint parts_pricelists_ctg_pk
        primary key (id_parts, id_pricelists_ctg);

insert into users_acl (role, resource, description)
values (NULL, 'pricelists/pdf', 'export ceniku do PDF'),
       ('admin', 'pricelists/pdf', 'export ceniku do PDF'),
       ('manager', 'pricelists/pdf', 'export ceniku do PDF');

insert into users_acl (role, resource, description)
values (NULL, 'pricelists/generate', 'generovani ceniku'),
       ('admin', 'pricelists/generate', 'generovani ceniku'),
       ('manager', 'pricelists/generate', 'generovani ceniku');

update pricelists set master = 1 where id = 59;

update pricelists set
                      `name` = concat('Wholesale ', id_pricelists_ctg),
                      `abbr` = concat('W', id_pricelists_ctg)
where id_pricelists_ctg is not null;
alter table parts_ctg
    add name_eng varchar(30) default null null comment 'anglicke pojmenovani kategorie' after name;

update parts_ctg set name_eng = 'Meduse Pipe Sets' where id = 23;

--
-- #321: Tvorba ceníků - 02.02.2022
--
insert into pricelists (id_pricelists_ctg, name, abbr, type, valid)
values (null, 'Retail 2022', 'RTL', 'b2c', '2022-02-01');
set @last_insert_id  = last_insert_id();
insert into prices (id_parts, id_pricelists, price)
select id_parts, @last_insert_id, price from prices
join pricelists on pricelists.master = 1 and prices.id_pricelists = pricelists.id;
update pricelists set master = 0 where master = 1;
update pricelists set master = 1 where id = @last_insert_id;

create table parts_ctg_pricelists_ctg (
    id_parts_ctg    int                   not null comment 'cizi klic na kolekce',
    id_pricelists_ctg int                 not null comment 'cizi klic na slevovou kategorii',
    discount          decimal(2) unsigned not null comment 'vyse slevy v procentech',
    primary key (id_pricelists_ctg, id_parts_ctg)
) comment 'slevove kategorie produktovych kategorii';

alter table parts
    add designated enum ('B2B', 'B2C') default null null comment 'produkt je urcen pro B2B nebo B2C zakazniky.' after product;
update parts set parts.designated = 'B2C' where parts.id_wh_type = 1 and product = 'y';

insert into users_acl (role, resource, description)
    select role, 'parts/toggle-designated-for' as resource, 'prepnuti urcenosti pro typ zákazníka'
    from users_acl where resource like 'parts/toggle-product';

--
-- #330: Odstranění smazaných produktů z nedokončených objednávek
--
insert into users_acl (role, resource, description)
select role, 'parts/restore-part', 'obnova soucasti z kose'
from users_acl
where resource like 'parts/trash';


--
-- #332: Změna oprávnění pro účetní
--
delete from users_acl
where
      role like 'accountant%'
  and resource in ('customers/address',
                   'customers/address-delete',
                   'customers/address-undelete',
                   'customers/ajax-search',
                   'customers/ajax-verify-vat',
                   'customers/detail',
                   'customers/edit',
                   'customers/index',
                   'orders/detail',
                   'orders/change-address',
                   'orders/view-private',
                   'tobacco-orders/detail',
                   'tobacco-orders/index',
                   'tobacco-customers/address',
                   'tobacco-customers/address-delete',
                   'tobacco-customers/address-undelete',
                   'tobacco-customers/ajax-search',
                   'tobacco-customers/ajax-verify-vat',
                   'tobacco-customers/detail',
                   'tobacco-customers/edit',
                   'tobacco-customers/index'
                  );

--
-- #334: Nová funkce - příznak: Dvojitá fakturace
--
alter table orders
    add di enum ('y', 'n') default 'n' null comment 'di' after coo_date_validity;


--
-- #332: Změna oprávnění pro účetní
--
insert into users_acl (role, resource, description)
select
    role,
    'tobacco-orders/list-all-orders' as resource,
    'zobrazit vsechny objednavky' as description
from users_acl
where
    resource like 'tobacco-orders/index' and (role != 'accountant_tobacco' or role is null);

insert into users_acl (role, resource, description)
select
    role,
    'orders/list-all-orders' as resource,
    'zobrazit vsechny objednavky' as description
from users_acl
where
        resource like 'orders/index' and (role != 'accountant_pipes' or role is null);

insert into users_acl (role, resource) values ('accountant_pipes', 'orders/index');
insert into users_acl (role, resource) values ('accountant_tobacco', 'tobacco-orders/index');

update invoice set registered = 'n' where type = 'proforma' and registered = 'y';

--
-- MWH-34: Kategorie součástí MEDUSE
--
update parts_ctg set parent_ctg = null where parent_ctg = 0;
alter table parts_ctg
    add id_wh_type int default 1 null comment 'prislusnost k wh' after id;
update parts_ctg set id_wh_type = 2 where
    (parent_ctg is null and id in (34, 35)) or (parent_ctg in (34, 35, 36, 49));
                                              ;
--
-- MWH-377: Sjednocení úhrad faktury s platbou objednávky
--
alter table payments
    add method enum ('cash_on_delivery', 'cash', 'card', 'transfer', 'free_presentation', 'free_claim', 'paypal', 'payscz') null comment 'zpusob platby: na dobirku, v hotovosti, kartou, prevodem, zdarma prezentace, zdarma clo, PayPal, Pays.cz'
        default null                                                                                                        null comment 'zpusob platby' after `no`,
    add note   varchar(50)                                                                                                  null null comment 'poznamka k platbe' after date;

alter table orders
    add paid_percentage int unsigned default null comment 'procento uhrazeni' after expected_payment;


--
-- MWH-320: WH MEDUSE - MENU - Procesy
--

create table documents
(
    id            int unsigned auto_increment,
    id_users      int(11) unsigned                      not null comment 'cizi klic na uzivatele',
    title         varchar(50)                           not null comment 'nazev dokumentu',
    `description` varchar(255)                          null comment 'popis dokumentu',
    deleted       boolean   default 0                   null comment 'dokument je smazany',
    created       datetime  default NOW()               null comment 'vytvoreno',
    updated       timestamp default current_timestamp() not null comment 'posledni zmena'
        on update current_timestamp(),
    constraint documents_pk
        primary key (id),
    constraint documents_users_id_fk
        foreign key (id_users) references users (id)
            on update cascade on delete cascade
)
    comment 'dokumenty pro interni potrebu';

create index documents_title_index
    on documents (title);

create table documents_files
(
    id           int unsigned auto_increment,
    id_documents int unsigned     not null comment 'cizi klic na dokumenty',
    id_users     int(11) unsigned not null comment 'cizi klic na uzivatele',
    filename     varchar(255)     not null comment 'nazev souboru',
    inserted     timestamp default current_timestamp(),
    constraint documents_files_pk
        primary key (id),
    constraint documents_files_users_id_fk
        foreign key (id_users) references users (id)
            on update cascade on delete cascade
) comment 'soubory (verze) pro dokumenty';

insert into users_acl (`role`, resource, `description`)
values
    (null, 'documents/index', 'prehled dokumentu'),
    ('admin', 'documents/index', null),
    ('manager', 'documents/index', null),
    ('sales_director', 'documents/index', null),
    ('seller', 'documents/index', null),
    ('head_warehouseman', 'documents/index', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'documents/add', 'vlozeni dokumentu'),
    ('admin', 'documents/add', null),
    ('manager', 'documents/add', null),
    ('sales_director', 'documents/add', null),
    ('seller', 'documents/add', null),
    ('head_warehouseman', 'documents/add', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'documents/edit', 'editace dokumentu'),
    ('admin', 'documents/edit', null),
    ('manager', 'documents/edit', null),
    ('sales_director', 'documents/edit', null),
    ('seller', 'documents/edit', null),
    ('head_warehouseman', 'documents/edit', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'documents/download', 'stazeni dokumentu'),
    ('admin', 'documents/download', null),
    ('manager', 'documents/download', null),
    ('sales_director', 'documents/download', null),
    ('seller', 'documents/download', null),
    ('head_warehouseman', 'documents/download', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'documents/detail', 'detail dokumentu'),
    ('admin', 'documents/detail', null),
    ('manager', 'documents/detail', null),
    ('sales_director', 'documents/detail', null),
    ('seller', 'documents/detail', null),
    ('head_warehouseman', 'documents/detail', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'documents/add-version', 'vlozeni nove verze dokumentu'),
    ('admin', 'documents/add-version', null),
    ('manager', 'documents/add-version', null),
    ('sales_director', 'documents/add-version', null),
    ('seller', 'documents/add-version', null),
    ('head_warehouseman', 'documents/add-version', null);

insert into users_acl (`role`, resource, `description`)
values
    (null, 'documents/delete', 'smazani dokumentu'),
    ('admin', 'documents/delete', null),
    ('manager', 'documents/delete', null),
    ('sales_director', 'documents/delete', null),
    ('seller', 'documents/delete', null),
    ('head_warehouseman', 'documents/delete', null);

--
-- MHW-353: Reprodukce produktu – funkce „save as"
--
insert into users_acl (role, resource, description)
select role, 'tobacco/parts-save-as' as resource, 'ulozit soucast jako' from users_acl
where resource like 'tobacco/parts-edit';

--
-- MWH-355 Přesun úhrad proformy na ostrou fakturu
--
insert into users_acl (role, resource, description)
select role, 'invoice/payment-move-to' as resource, 'presunout uhradu' from users_acl
where resource like 'invoice/payment-add';


--
-- MWH-352 Přidat novou záložku do „Přehled macerace“  s názvem „vyřazené macerace“
--
alter table macerations
    add discarded enum ('n', 'y') default 'n' not null comment 'je macerat vyrazeny?';

insert into users_acl (role, resource, description)
select role, 'tobacco/macerations-discard' as resource, 'vyradit macerat' from users_acl
where resource like 'tobacco/macerations-edit';

insert into users_acl (role, resource, description)
select role, 'tobacco/macerations-restore' as resource, 'obnovit macerat' from users_acl
where resource like 'tobacco/macerations-edit';

--
-- MWH-355 Přesun úhrad proformy na ostrou fakturu
--
insert into users_acl (role, resource, description)
select role, 'tobacco-invoice/payment-move-to' as resource, 'presunout uhradu tabak' from users_acl
where resource like 'tobacco-invoice/payment-add';

--
-- MWH-351 Export skład + export macerace – spojit do jedné tabulky.
--
update users_acl set resource = 'tobacco/export-generate' where resource like 'tobacco/export-parts';
delete from users_acl where resource like 'tobacco/export-maceration';

--
-- MWH-357 Zákazníci založení ve WH tabáku mají id_wh_type = 0
--
alter table customers
    modify id_wh_type enum ('1', '2') default '1' not null comment 'id casti wh (pipes/tobacco)';


--
-- MWH-361 Samostaná správa bank. účtů pro WH tabáku
--
insert into users_acl (role, resource, description)
select role, CONCAT('tobacco-settings', RIGHT(resource, LENGTH(resource) - 5)) as resource, description from users_acl where resource like 'admin/accounts-%';
alter table accounts
    modify payment_method enum ('cash_on_delivery', 'cash', 'card', 'transfer', 'free_presentation', 'free_claim', 'paypal', 'payscz') null comment 'svazana platebni metoda';

--
-- MWH-365 Nelze vymazat přepravce
--
insert into users_acl (role, resource, description)
select role, 'producers/activate' as resource, 'aktivovat dodavatele' from users_acl
where resource like 'producers/delete';
insert into users_acl (role, resource, description)
select role, 'tobacco-producers/activate' as resource, 'aktivovat dodavatele' from users_acl
where resource like 'tobacco-producers/add';

--
-- MWH-364 Vylepšit cenotvorbu
--
insert into users_acl (role, resource, description)
select role, 'pricelists/mapping' as resource, 'mapovani ceniku' from users_acl
where resource like 'pricelists/ctg';
insert into users_acl (role, resource, description)
select role, 'pricelists/manage' as resource, 'sprava ceniku' from users_acl
where resource like 'pricelists/ctg';
insert into pricelists (id_wh_type, id_pricelists_ctg, name, abbr, type, region, valid, `default`, export, deleted,
                        master)
select id_wh_type,
       id_pricelists_ctg,
       concat(name, ' draft') as name,
       concat(abbr, 'D')      as abbr,
       type,
       region,
       valid,
       `default`,
       export,
       deleted,
       master
from pricelists
where id_pricelists_ctg is not null;

--
-- MWH-365 Nelze vymazat přepravce
--
insert into users_acl (role, resource, description) values
('seller', 'producers/activate', 'aktivovat dodavatele'),
('tobacco_seller', 'tobacco-producers/activate', 'aktivovat dodavatele');
