-- upravy kodovani

ALTER TABLE `macerations_history`
    CHANGE COLUMN `description` `description` VARCHAR(255) NOT NULL COMMENT 'popis zmeny' COLLATE 'utf8_czech_ci';
ALTER TABLE `users_action_log`
    CHANGE COLUMN `params` `params` TEXT NOT NULL COLLATE 'utf8_czech_ci';

-- ------ --
-- LOLITA --
-- ------ --

START TRANSACTION;

-- vstupni parametry
SET @old := 30;
SET @id := 79;
SET @datum1 := '2019-02-06'; -- L190204048
SET @mac1 := 375;
SET @datum2 := '2019-06-20'; -- L190301098
SET @mac2 := 388;
SET @datum3 := '2019-11-20'; -- L190509065
SET @mac3 := 432;
SET @datum4 := '2019-01-30';
-- fakturace


-- docasna prevodni tabulka
CREATE TEMPORARY TABLE tmp_trans
SELECT p.id AS src, CONCAT(LEFT(p.id, LENGTH(p.id)-3), 5, RIGHT(p.id, 2)) AS dst
FROM parts p
         JOIN parts_mixes pm ON pm.id_parts = p.id AND pm.id_mixes = @old;
UPDATE tmp_trans
SET dst = 'TA150047'
WHERE src = 'TA150038';
INSERT INTO tmp_trans (src, dst)
SELECT p.id AS src, CONCAT(LEFT(p.id, LENGTH(p.id)-3), 5, RIGHT(p.id, 2)) AS dst
FROM parts p
         JOIN parts_subparts ps ON ps.id_multipart = p.id
         JOIN parts_mixes pm ON pm.id_parts = ps.id_parts AND pm.id_mixes = @old;

-- docasna prevodni tabulka 2
CREATE TEMPORARY TABLE tmp_trans2
SELECT p.id AS src, CONCAT(LEFT(p.id, LENGTH(p.id)-3), 5, RIGHT(p.id, 2)) AS dst
FROM parts p
         JOIN parts_mixes pm ON pm.id_parts = p.id AND pm.id_mixes = @old;
UPDATE tmp_trans2
SET dst = 'TA150047'
WHERE src = 'TA150038';
INSERT INTO tmp_trans2 (src, dst)
SELECT p.id AS src, CONCAT(LEFT(p.id, LENGTH(p.id)-3), 5, RIGHT(p.id, 2)) AS dst
FROM parts p
         JOIN parts_subparts ps ON ps.id_multipart = p.id
         JOIN parts_mixes pm ON pm.id_parts = ps.id_parts AND pm.id_mixes = @old;

-- vytvoreni nekolkovanych produktu
INSERT INTO parts (id, id_wh_type, id_parts_ctg, `name`, measure, product, multipart, last_change)
SELECT t.dst                           AS id,
       p.id_wh_type,
       p.id_parts_ctg,
       CONCAT(p.`name`, ' (straight)') AS `name`,
       p.measure,
       p.product,
       p.multipart,
       p.last_change
FROM parts p
         JOIN tmp_trans t ON t.src = p.id AND t.src <> 'TA150038'
         JOIN parts_mixes pm ON pm.id_parts = p.id AND pm.id_mixes = @old;


-- vytvoreni kolkovanych produktu
INSERT INTO parts (id, id_wh_type, id_parts_ctg, `name`, measure, product, multipart, last_change)
SELECT t.dst                           AS id,
       p.id_wh_type,
       p.id_parts_ctg,
       CONCAT(p.`name`, ' (straight)') AS `name`,
       p.measure,
       p.product,
       p.multipart,
       p.last_change
FROM parts p
         JOIN tmp_trans t ON t.src = p.id
         JOIN parts_subparts ps ON ps.id_multipart = p.id
         JOIN parts_mixes pm ON pm.id_parts = ps.id_parts AND pm.id_mixes = @old;


-- napojeni podsoucasti a nadsoucasti
INSERT INTO parts_subparts (id_multipart, id_parts, amount)
SELECT t1.dst                                          AS id_multipart,
       IF(ps.id_parts LIKE 'TA%', t2.dst, ps.id_parts) AS id_parts,
       ps.amount
FROM parts_subparts ps
         JOIN tmp_trans t1 ON t1.src = ps.id_multipart
         LEFT JOIN tmp_trans2 t2 ON t2.src = ps.id_parts
WHERE ps.id_multipart IN (SELECT pm.id_parts FROM parts_mixes pm WHERE pm.id_mixes = @old);

INSERT INTO parts_subparts (id_multipart, id_parts, amount)
SELECT t3.dst                                          AS id_multipart,
       IF(ps.id_parts LIKE 'TA%', t4.dst, ps.id_parts) AS id_parts,
       ps.amount
FROM parts_subparts ps
         JOIN tmp_trans t3 ON t3.src = ps.id_multipart
         LEFT JOIN tmp_trans2 t4 ON t4.src = ps.id_parts
WHERE ps.id_multipart IN (SELECT ps.id_multipart
                          FROM parts_subparts ps
                                   JOIN parts_mixes pm ON pm.id_parts = ps.id_parts AND pm.id_mixes = @old);


-- prirazeni mixu k produktum
INSERT INTO parts_mixes (id_parts, id_mixes, amount)
SELECT t.dst AS id_parts,
       @id   AS id_mixes,
       pm.amount
FROM parts_mixes pm
         JOIN tmp_trans t ON t.src = pm.id_parts AND t.src <> 'TA150038'
WHERE pm.id_mixes = @old;


-- zalozeni nulovych stavu do skladu nekolkovanych
INSERT INTO parts_warehouse (id_parts, id_users, amount, critical_amount, last_change)
SELECT t.dst AS id_parts,
       pw.id_users,
       0     AS amount,
       0     AS critical_amount,
       pw.last_change
FROM parts_warehouse pw
         JOIN tmp_trans t ON t.src = pw.id_parts
         JOIN parts_mixes pm ON pm.id_parts = pw.id_parts AND pm.id_mixes = @old
WHERE t.src <> 'TA150038';


-- zalozeni nulovych stavu do skladu kolkovanych
INSERT INTO parts_warehouse (id_parts, id_users, amount, critical_amount, last_change)
SELECT t.dst AS id_parts,
       pw.id_users,
       0     AS amount,
       0     AS critical_amount,
       pw.last_change
FROM parts_warehouse pw
         JOIN tmp_trans t ON t.src = pw.id_parts
         JOIN parts_subparts ps ON ps.id_multipart = pw.id_parts
         JOIN parts_mixes pm ON pm.id_parts = ps.id_parts AND pm.id_mixes = @old;


-- uprava historie skladu
UPDATE parts_warehouse_history h
    JOIN tmp_trans t ON t.src = h.id_parts
SET h.id_parts    = t.dst,
    h.last_change = h.last_change
WHERE h.last_change >= @datum1;


-- zmena receptur na Lolita STR
UPDATE macerations
SET id_recipes = 66
WHERE id = @mac1;
UPDATE macerations
SET id_recipes = 66
WHERE id = @mac2;


-- regularni vyraz pro zmenu pohybu
SELECT @reg := GROUP_CONCAT(pm.id_parts SEPARATOR '|')
FROM parts_mixes pm
WHERE pm.id_mixes = @old;

-- 1. sud
CREATE TEMPORARY TABLE tmp_amount
SELECT mh.id_macerations, SUM(mh.amount) AS amount
FROM macerations_history mh
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum1
  AND mh.last_change < @datum2
GROUP BY id_macerations;
SELECT @diff1 := SUM(amount)
FROM tmp_amount;
UPDATE macerations m
SET m.amount_current = m.amount_current - @diff1
WHERE m.id = @mac1;
UPDATE macerations m JOIN tmp_amount t ON t.id_macerations = m.id SET
    m.amount_current = IF(m.amount_current = 0, 0, m.amount_current + t.amount);
UPDATE macerations_history mh
SET mh.id_macerations = @mac1
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum1
  AND mh.last_change < @datum2;
DROP TABLE tmp_amount;

-- 2. sud
CREATE TEMPORARY TABLE tmp_amount
SELECT mh.id_macerations, SUM(mh.amount) AS amount
FROM macerations_history mh
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum2
  AND mh.last_change < @datum3
GROUP BY id_macerations;
SELECT @diff2 := SUM(amount)
FROM tmp_amount;
UPDATE macerations m
SET m.amount_current = m.amount_current - @diff2
WHERE m.id = @mac2;
UPDATE macerations m JOIN tmp_amount t ON t.id_macerations = m.id SET
    m.amount_current = IF(m.amount_current = 0, 0, m.amount_current + t.amount);
UPDATE macerations_history mh
SET mh.id_macerations = @mac2
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum2
  AND mh.last_change < @datum3;
DROP TABLE tmp_amount;

-- 3. sud
CREATE TEMPORARY TABLE tmp_amount
SELECT mh.id_macerations, SUM(mh.amount) AS amount
FROM macerations_history mh
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum3
GROUP BY id_macerations;
SELECT @diff3 := SUM(amount)
FROM tmp_amount;
UPDATE macerations m
SET m.amount_current = m.amount_current - @diff3
WHERE m.id = @mac3;
UPDATE macerations m JOIN tmp_amount t ON t.id_macerations = m.id SET
m.amount_current = IF(m.amount_current = 0, 0, m.amount_current + t.amount);
UPDATE macerations_history mh
SET mh.id_macerations = @mac3
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum3;
DROP TABLE tmp_amount;


-- zmena popisu historie maceratu
UPDATE macerations_history mh JOIN tmp_trans t ON t.src = LEFT(RIGHT(mh.description, (LENGTH(mh.description)) - 9), 8)
SET mh.description = CONCAT('Packing: ', t.dst, RIGHT(mh.description, (LENGTH(mh.description)) - 17)),
    mh.last_change = mh.last_change
WHERE mh.last_change >= @datum1;


-- uprava polozek objednavek
UPDATE orders_products op
    JOIN tmp_trans t ON t.src = op.id_products
SET op.id_products = t.dst,
    op.last_change = op.last_change
WHERE op.last_change >= @datum4;


-- uprava polozek faktur
UPDATE invoice_items ii
    JOIN tmp_trans t ON t.src = ii.id_parts
    JOIN invoice i ON i.id = ii.id_invoice
SET ii.id_parts = t.dst
WHERE i.inserted >= @datum4;


-- uklid
SET @old := NULL;
SET @id := NULL;
SET @datum1 := NULL;
SET @mac1 := NULL;
SET @datum2 := NULL;
SET @mac2 := NULL;
SET @datum3 := NULL;
SET @mac3 := NULL;
SET @datum4 := NULL;
DROP TABLE tmp_trans;
DROP TABLE tmp_trans2;

COMMIT;

-- --------- --
-- // LOLITA --
-- --------- --


-- --------------- --
-- FITNESS BLENDER --
-- --------------- --

START TRANSACTION;

-- vstupni promenne
SET @old := 45;
SET @flavor := 90;
SET @recipe := 63;
SET @mix_name := 'Fitness Blender (straight)';
SET @datum1 := '2019-05-13';
SET @mac1 := 387;
SET @datum2 := '2019-10-16';
SET @mac2 := 440;
SET @datum3 := '2019-05-06';


-- vytvoreni nekolkovanych produktu
INSERT INTO parts (id, id_wh_type, id_parts_ctg, `name`, measure, product, multipart, last_change)
SELECT CONCAT(LEFT(p.id, LENGTH(p.id)-3), 5, RIGHT(p.id, 2)) AS id,
       p.id_wh_type,
       p.id_parts_ctg,
       CONCAT(p.`name`, ' (straight)')          AS `name`,
       p.measure,
       p.product,
       p.multipart,
       p.last_change
FROM parts p
         JOIN parts_mixes pm ON pm.id_parts = p.id AND pm.id_mixes = @old;


-- vytvoreni kolkovanych produktu
INSERT INTO parts (id, id_wh_type, id_parts_ctg, `name`, measure, product, multipart, last_change)
SELECT CONCAT(LEFT(p.id, LENGTH(p.id)-3), 5, RIGHT(p.id, 2)) AS id,
       p.id_wh_type,
       p.id_parts_ctg,
       CONCAT(p.`name`, ' (straight)')          AS `name`,
       p.measure,
       p.product,
       p.multipart,
       p.last_change
FROM parts p
         JOIN parts_subparts ps ON ps.id_multipart = p.id
         JOIN parts_mixes pm ON pm.id_parts = ps.id_parts AND pm.id_mixes = @old;


-- napojeni podsoucasti a nadsoucasti
INSERT INTO parts_subparts (id_multipart, id_parts, amount)
SELECT CONCAT(LEFT(ps.id_multipart, LENGTH(ps.id_multipart)-3), 5, RIGHT(ps.id_multipart, 2))                                  AS id_multipart,
       IF(ps.id_parts LIKE 'TA%', CONCAT(LEFT(ps.id_parts, LENGTH(ps.id_parts)-3), 5, RIGHT(ps.id_parts, 2)), ps.id_parts) AS id_parts,
       ps.amount
FROM parts_subparts ps
WHERE ps.id_multipart IN (SELECT pm.id_parts FROM parts_mixes pm WHERE pm.id_mixes = @old);
INSERT INTO parts_subparts (id_multipart, id_parts, amount)
SELECT CONCAT(LEFT(ps.id_multipart, LENGTH(ps.id_multipart)-3), 5, RIGHT(ps.id_multipart, 2))                                  AS id_multipart,
       IF(ps.id_parts LIKE 'TA%', CONCAT(LEFT(ps.id_parts, LENGTH(ps.id_parts)-3), 5, RIGHT(ps.id_parts, 2)), ps.id_parts) AS id_parts,
       ps.amount
FROM parts_subparts ps
WHERE ps.id_multipart IN (SELECT ps.id_multipart
                          FROM parts_subparts ps
                                   JOIN parts_mixes pm ON pm.id_parts = ps.id_parts AND pm.id_mixes = @old);


-- vytvoreni neexistujiciho mixu
INSERT INTO `mixes` (`id_flavor`, `name`, `create`)
VALUES (@flavor, @mix_name, NOW());
SET @id := LAST_INSERT_ID();
INSERT INTO `meduse_wh_test`.`mixes_recipes` (`id_mixes`, `id_recipes`, `ratio`)
VALUES (@id, @recipe, 100);


-- prirazeni noveho mixu k produktum
INSERT INTO parts_mixes (id_parts, id_mixes, amount)
SELECT CONCAT(LEFT(pm.id_parts, LENGTH(pm.id_parts)-3), 5, RIGHT(pm.id_parts, 2)) AS id_parts,
       @id                                                    AS id_mixes,
       pm.amount
FROM parts_mixes pm
WHERE pm.id_mixes = @old;


-- zalozeni nulovych stavu do skladu nekolkovanych
INSERT INTO parts_warehouse (id_parts, id_users, amount, critical_amount, last_change)
SELECT CONCAT(LEFT(pw.id_parts, LENGTH(pw.id_parts)-3), 5, RIGHT(pw.id_parts, 2)) AS id_parts,
       pw.id_users,
       0                                                      AS amount,
       0                                                      AS critical_amount,
       pw.last_change
FROM parts_warehouse pw
         JOIN parts_mixes pm ON pm.id_parts = pw.id_parts AND pm.id_mixes = @old;


-- zalozeni nulovych stavu do skladu kolkovanych
INSERT INTO parts_warehouse (id_parts, id_users, amount, critical_amount, last_change)
SELECT CONCAT(LEFT(pw.id_parts, LENGTH(pw.id_parts)-3), 5, RIGHT(pw.id_parts, 2)) AS id_parts,
       pw.id_users,
       0                                                      AS amount,
       0                                                      AS critical_amount,
       pw.last_change
FROM parts_warehouse pw
         JOIN parts_subparts ps ON ps.id_multipart = pw.id_parts
         JOIN parts_mixes pm ON pm.id_parts = ps.id_parts AND pm.id_mixes = @old;


-- uprava historie skladu
UPDATE parts_warehouse_history h
SET h.id_parts = CONCAT(LEFT(h.id_parts, LENGTH(h.id_parts)-3), 5, RIGHT(h.id_parts, 2))
WHERE h.id_parts IN (SELECT id_parts FROM parts_mixes pm WHERE pm.id_mixes = @old)
  AND h.last_change >= @datum1;
UPDATE parts_warehouse_history h
SET h.id_parts = CONCAT(LEFT(h.id_parts, LENGTH(h.id_parts)-3), 5, RIGHT(h.id_parts, 2))
WHERE h.id_parts IN
      (SELECT s.id_multipart
       FROM parts_subparts s
       WHERE s.id_parts IN (SELECT id_parts FROM parts_mixes pm WHERE pm.id_mixes = @old))
  AND h.last_change >= @datum1;


-- regularni vyraz pro zmenu pohybu
SELECT @reg := GROUP_CONCAT(pm.id_parts SEPARATOR '|')
FROM parts_mixes pm
WHERE pm.id_mixes = @old;


-- presun pohybu do sudu 1
CREATE TEMPORARY TABLE tmp_amount
SELECT mh.id_macerations, SUM(mh.amount) AS amount
FROM macerations_history mh
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum1
  AND mh.last_change < @datum2
GROUP BY id_macerations;
SELECT @diff1 := SUM(amount)
FROM tmp_amount;
UPDATE macerations m
SET m.amount_current = m.amount_current - @diff1
WHERE m.id = @mac1;
UPDATE macerations m JOIN tmp_amount t ON t.id_macerations = m.id SET
    m.amount_current = IF(m.amount_current = 0, 0, m.amount_current + t.amount);
UPDATE macerations_history mh
SET mh.id_macerations = @mac1
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum1
  AND mh.last_change < @datum2;
DROP TABLE tmp_amount;

-- presun pohybu do sudu 2
CREATE TEMPORARY TABLE tmp_amount
SELECT mh.id_macerations, SUM(mh.amount) AS amount
FROM macerations_history mh
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum2
GROUP BY id_macerations;
SELECT @diff2 := SUM(amount)
FROM tmp_amount;
UPDATE macerations m
SET m.amount_current = m.amount_current - @diff2
WHERE m.id = @mac2;
UPDATE macerations m JOIN tmp_amount t ON t.id_macerations = m.id SET
    m.amount_current = IF(m.amount_current = 0, 0, m.amount_current + t.amount);
UPDATE macerations_history mh
SET mh.id_macerations = @mac2
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum2;
DROP TABLE tmp_amount;

-- zmena popisu historie maceratu
UPDATE macerations_history mh
SET mh.description = CONCAT(LEFT(mh.description, 14), '5', RIGHT(mh.description, (LENGTH(mh.description)) - 15))
WHERE mh.description REGEXP (@reg)
  AND mh.last_change >= @datum1;


-- uprava polozek objednavek
UPDATE orders_products op
SET op.id_products = CONCAT(LEFT(op.id_products, LENGTH(op.id_products)-3), 5, RIGHT(op.id_products, 2))
WHERE (op.id_products IN (SELECT id_parts FROM parts_mixes pm WHERE pm.id_mixes = @old)
    OR op.id_products IN ((SELECT s.id_multipart
                           FROM parts_subparts s
                           WHERE s.id_parts IN (SELECT id_parts FROM parts_mixes pm WHERE pm.id_mixes = @old))))
  AND op.last_change >= @datum3;


-- uprava polozek faktur
UPDATE invoice_items ii JOIN invoice i ON i.id = ii.id_invoice
SET ii.id_parts = CONCAT(LEFT(ii.id_parts, LENGTH(ii.id_parts)-3), 5, RIGHT(ii.id_parts, 2))
WHERE (ii.id_parts IN (SELECT id_parts FROM parts_mixes pm WHERE pm.id_mixes = @old)
    OR ii.id_parts IN ((SELECT s.id_multipart
                        FROM parts_subparts s
                        WHERE s.id_parts IN (SELECT id_parts FROM parts_mixes pm WHERE pm.id_mixes = @old))))
  AND i.inserted >= @datum3;

-- uklid
SET @old := NULL;
SET @flavor := NULL;
SET @recipe := NULL;
SET @mix_name := NULL;
SET @datum1 := NULL;
SET @mac1 := NULL;
SET @datum2 := NULL;
SET @mac2 := NULL;
SET @datum3 := NULL;

COMMIT;

-- ------------------ --
-- // FITNESS BLENDER --
-- ------------------ --

START TRANSACTION;

-- oprava chybneho mnozstvi macerace na vstupu
UPDATE macerations m, (SELECT m.id, IFNULL(SUM(mp.amount), 0) AS amount_in
                       FROM macerations m
                                LEFT JOIN macerations_parts mp ON mp.id_macerations = m.id
                       GROUP BY m.id) s
SET m.amount_in = s.amount_in
WHERE m.id = s.id;

-- vymaz zaznamu o rucnim naskladneni a pridani zaznamu dle faktur
UPDATE parts_warehouse_history pwh
SET pwh.amount      = 0,
    pwh.last_change = pwh.last_change
WHERE pwh.id IN
      (66416, 67083, 67326, 68180, 69513, 86266, 185842, 229778, 229779, 229781, 229783, 231742, 232984, 232986);
UPDATE parts_warehouse_history pwh
SET pwh.amount      = 0,
    pwh.last_change = pwh.last_change
WHERE pwh.id IN (278732, 232985, 229782, 229785, 229784, 229777, 69585, 69496, 67325, 66411);
UPDATE parts_warehouse_history pwh
SET pwh.amount      = 50000,
    pwh.last_change = pwh.last_change
WHERE pwh.id = 299590;
UPDATE parts_warehouse_history pwh
SET pwh.amount      = 200000,
    pwh.last_change = pwh.last_change
WHERE pwh.id IN (66410, 301119);
UPDATE parts_warehouse_history pwh
SET pwh.amount      = 200000,
    pwh.last_change = '2016-11-22 14:00:04'
WHERE pwh.id = 174494;

-- odstraneni vsech zaznamu o vyskladneni
DELETE
FROM parts_warehouse_history
WHERE id_parts = 'TS001'
  AND amount <= 0;

INSERT INTO parts_warehouse_history (id_parts, amount, last_change, id_users)
SELECT mp.id_parts, -1000 * mp.amount, TIMESTAMP(m.`start`) AS last_change, 28 AS id_users
FROM macerations_parts mp
         JOIN macerations m ON m.id = mp.id_macerations
WHERE mp.id_parts = 'TS001';

-- nastaveni spravneho koncoveho  mnozstvi
UPDATE parts_warehouse pw
SET pw.amount      = pw.amount - 1388240,
    pw.last_change = pw.last_change
WHERE pw.id_parts = 'TS001';

-- tabulka pro potreby inventarizace
CREATE TABLE IF NOT EXISTS  `parts_warehouse_snapshot`
(
    `id`                         INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_parts`                   VARCHAR(10)      NOT NULL COMMENT 'cizi klic na soucast' COLLATE 'utf8_czech_ci',
    `id_users`                   INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na uzivatele',
    `id_parts_warehouse_history` INT(11)          NULL     DEFAULT NULL COMMENT 'cizi klic na historii',
    `amount_wh`                  INT(6) UNSIGNED  NOT NULL COMMENT 'mnozstvi k danemu datu ve wh',
    `amount_real`                INT(6) UNSIGNED  NOT NULL COMMENT 'namerene realne mnozstvi',
    `date`                       TIMESTAMP        NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `note`                       VARCHAR(100)     NULL     DEFAULT NULL COMMENT 'poznamka',
    `is_fixed`                   ENUM ('y','n')   NOT NULL DEFAULT 'n' COMMENT 'je stav vyrovnan?',
    PRIMARY KEY (`id`),
    INDEX `FK_parts_warehouse_snapshot_users` (`id_users`),
    INDEX `FK_parts_warehouse_snapshot_parts` (`id_parts`),
    INDEX `FK_parts_warehouse_snapshot_parts_warehouse_history` (`id_parts_warehouse_history`),
    CONSTRAINT `FK_parts_warehouse_snapshot_parts` FOREIGN KEY (`id_parts`) REFERENCES `parts` (`id`),
    CONSTRAINT `FK_parts_warehouse_snapshot_parts_warehouse_history` FOREIGN KEY (`id_parts_warehouse_history`) REFERENCES `parts_warehouse_history` (`id`),
    CONSTRAINT `FK_parts_warehouse_snapshot_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
)
    COMMENT ='zachycene realne stavy skladu pro inventurni ucely'
    COLLATE = 'utf8_general_ci'
    ENGINE = InnoDB;

-- opravneni k inventurizaci
INSERT INTO `users_acl` (`role`, `resource`, `description`)
VALUES (NULL, 'tobacco/warehouse-snapshot', 'prehled inventurizace'),
       ('admin', 'tobacco/warehouse-snapshot', NULL),
       ('manager', 'tobacco/warehouse-snapshot', NULL),
       ('tobacco_whman', 'tobacco/warehouse-snapshot', NULL),
       (NULL, 'tobacco/warehouse-snapshot-add', 'zavest inventurni stav'),
       ('admin', 'tobacco/warehouse-snapshot-add', NULL),
       ('manager', 'tobacco/warehouse-snapshot-add', NULL),
       ('tobacco_whman', 'tobacco/warehouse-snapshot-add', NULL),
       (NULL, 'tobacco/warehouse-snapshot-edit', 'zmenit inventurni stav'),
       ('admin', 'tobacco/warehouse-snapshot-edit', NULL),
       ('manager', 'tobacco/warehouse-snapshot-edit', NULL),
       ('tobacco_whman', 'tobacco/warehouse-snapshot-edit', NULL),
       (NULL, 'tobacco/warehouse-snapshot-fix', 'vyrovnani realneho stavu'),
       ('admin', 'tobacco/warehouse-snapshot-fix', NULL),
       ('manager', 'tobacco/warehouse-snapshot-fix', NULL),
       ('tobacco_whman', 'tobacco/warehouse-snapshot-fix', NULL);

-- tabulka pro potreby inventarizace
CREATE TABLE IF NOT EXISTS `macerations_snapshot`
(
    `id`                     INT(11) UNSIGNED        NOT NULL AUTO_INCREMENT,
    `id_macerations`         INT(11) UNSIGNED        NOT NULL COMMENT 'cizi klic na maceraci',
    `id_users`               INT(11) UNSIGNED        NOT NULL COMMENT 'cizi klic na uzivatele',
    `id_macerations_history` INT(11) UNSIGNED        NULL     DEFAULT NULL COMMENT 'cizi klic na historii - dorovnani',
    `amount_wh`              DECIMAL(10, 3) UNSIGNED NOT NULL DEFAULT 0.000 COMMENT 'mnozstvi k danemu datu ve wh',
    `amount_real`            DECIMAL(10, 3) UNSIGNED NOT NULL DEFAULT 0.000 COMMENT 'namerene realne mnozstvi',
    `date`                   TIMESTAMP               NOT NULL DEFAULT current_timestamp(),
    `note`                   VARCHAR(100)            NULL     DEFAULT NULL COMMENT 'poznamka',
    `is_fixed`               ENUM ('y','n')          NOT NULL DEFAULT 'n' COMMENT 'je stav vyrovnan?',
    PRIMARY KEY (`id`),
    INDEX `FK_maceratons_snapshot_users` (`id_users`),
    INDEX `FK_macerations_snapshot` (`id_macerations`),
    INDEX `FK_macerations_snapshot_macerations_history` (`id_macerations_history`),
    CONSTRAINT `FK_macerations_snapshot` FOREIGN KEY (`id_macerations`) REFERENCES `macerations` (`id`),
    CONSTRAINT `FK_macerations_snapshot_macerations_history` FOREIGN KEY (`id_macerations_history`) REFERENCES `macerations_history` (`id`),
    CONSTRAINT `FK_maceratons_snapshot_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
)
    COMMENT ='zachycene realne stavy macerace pro inventurni ucely'
    COLLATE = 'utf8_general_ci'
    ENGINE = InnoDB;


-- opravneni k inventurizaci
INSERT INTO `users_acl` (`role`, `resource`, `description`)
VALUES (NULL, 'tobacco/macerations-snapshot', 'prehled inventurizace'),
       ('admin', 'tobacco/macerations-snapshot', NULL),
       ('manager', 'tobacco/macerations-snapshot', NULL),
       ('tobacco_whman', 'tobacco/macerations-snapshot', NULL),
       (NULL, 'tobacco/macerations-snapshot-add', 'zavest inventurni stav'),
       ('admin', 'tobacco/macerations-snapshot-add', NULL),
       ('manager', 'tobacco/macerations-snapshot-add', NULL),
       ('tobacco_whman', 'tobacco/macerations-snapshot-add', NULL),
       (NULL, 'tobacco/macerations-snapshot-edit', 'zmenit inventurni stav'),
       ('admin', 'tobacco/macerations-snapshot-edit', NULL),
       ('manager', 'tobacco/macerations-snapshot-edit', NULL),
       ('tobacco_whman', 'tobacco/macerations-snapshot-edit', NULL),
       (NULL, 'tobacco/macerations-snapshot-fix', 'vyrovnani realneho stavu'),
       ('admin', 'tobacco/macerations-snapshot-fix', NULL),
       ('manager', 'tobacco/macerations-snapshot-fix', NULL),
       ('tobacco_whman', 'tobacco/macerations-snapshot-fix', NULL);

-- naplneni tabulky podle zaznamu z kontrol vazeni macerace
INSERT INTO macerations_snapshot (id_macerations, `date`, amount_real, amount_wh, id_users, is_fixed,
                                  id_macerations_history)
SELECT mh.id_macerations,
       mh.last_change                                                                     AS `date`,
       CAST(IF(TRIM(REPLACE(REPLACE(REPLACE(SUBSTRING(mh.description, 19, 7), 'k', ''), 'g', ''), ',', '.')) = '', 0,
               TRIM(REPLACE(REPLACE(REPLACE(SUBSTRING(mh.description, 19, 7), 'k', ''), 'g', ''), ',',
                            '.'))) AS DECIMAL(10, 3))                                     AS amount_real,
       CAST(IF(TRIM(REPLACE(REPLACE(REPLACE(SUBSTRING(mh.description, 19, 7), 'k', ''), 'g', ''), ',', '.')) = '', 0,
               TRIM(REPLACE(REPLACE(REPLACE(SUBSTRING(mh.description, 19, 7), 'k', ''), 'g', ''), ',',
                            '.'))) AS DECIMAL(10, 3)) - CAST(mh.amount AS DECIMAL(10, 3)) AS amount_wh,
       mh.id_users,
       'y'                                                                                AS is_fixed,
       mh.id
FROM macerations_history mh
WHERE description LIKE 'Kontrola: zváženo%'
ORDER BY mh.last_change;

-- opravneni ke ziskani mnostvi - ajax
INSERT INTO `users_acl` (`role`, `resource`, `description`)
VALUES (NULL, 'tobacco/ajax-get-part-amount', 'mnozstvi soucasni na sklade'),
       ('admin', 'tobacco/ajax-get-part-amount', NULL),
       ('manager', 'tobacco/ajax-get-part-amount', NULL),
       ('tobacco_whman', 'tobacco/ajax-get-part-amount', NULL),
       (NULL, 'tobacco/ajax-get-maceration-weight', 'mnostvi maceratu na sklade'),
       ('admin', 'tobacco/ajax-get-maceration-weight', NULL),
       ('manager', 'tobacco/ajax-get-maceration-weight', NULL),
       ('tobacco_whman', 'tobacco/ajax-get-maceration-weight', NULL);

/*
ALTER TABLE `macerations_history`
    ADD COLUMN `type` ENUM ('create', 'pack', 'check', 'merge_from', 'merge_to', 'break', 'destroy') NULL DEFAULT NULL COMMENT 'typ zmeny' AFTER `amount`,
    ADD INDEX `type` (`type`);
 */
ALTER TABLE `macerations_history`
    ALTER `description` DROP DEFAULT;

UPDATE macerations_history
SET `type`      = 'pack',
    amount      = -amount,
    last_change = last_change
WHERE description LIKE 'Packing%';

UPDATE macerations_history
SET `type`      = 'check',
    last_change = last_change
WHERE description LIKE 'Kontrola%';

UPDATE macerations_history
SET `type`      = 'merge_from',
    amount      = 0,
    last_change = last_change
WHERE description LIKE 'Přimícháno%';

UPDATE macerations_history
SET `type`      = 'merge_to',
    amount      = 0,
    last_change = last_change
WHERE description LIKE 'Vmícháno%';

UPDATE macerations_history
SET `type`      = 'break',
    last_change = last_change
WHERE description LIKE 'Rozklad%';

UPDATE macerations_history
SET `type`      = 'destroy',
    amount      = -amount,
    last_change = last_change
WHERE description LIKE 'Storno%';



SET SESSION sql_mode = '';

UPDATE macerations_history mh
    JOIN macerations m ON m.id = mh.id_macerations
    JOIN (SELECT s.`date`, s.amount AS amount, s.batch_from, s.batch_to
          FROM (
                   SELECT ual.`date`,
                          CAST(TRIM(REPLACE(RIGHT(ual.params, LENGTH(ual.params) - 77), ', Vmíchat=Vmíchat,',
                                            '')) AS DECIMAL(10, 3))                       AS amount,
                          RIGHT(LEFT(RIGHT(ual.params, LENGTH(ual.params) - 20), 21), 10) AS batch_from,
                          RIGHT(LEFT(RIGHT(ual.params, LENGTH(ual.params) - 20), 42), 10) AS batch_to
                   FROM users_action_log ual
                   WHERE ual.`action` = 'tobacco/macerations-merge'
                     AND params LIKE '%merge_amount=%'
               ) s
          WHERE s.amount > 0
            AND s.batch_from LIKE 'L%'
            AND s.batch_to LIKE 'L%') wm
    ON wm.batch_from = m.batch AND wm.date = mh.last_change
SET mh.amount      = -wm.amount,
    mh.last_change = mh.last_change
WHERE mh.`type` = 'merge_to';

UPDATE macerations_history mh
    JOIN macerations m ON m.id = mh.id_macerations
    JOIN (SELECT s.`date`, s.amount AS amount, s.batch_from, s.batch_to
          FROM (
                   SELECT ual.`date`,
                          CAST(TRIM(REPLACE(RIGHT(ual.params, LENGTH(ual.params) - 77), ', Vmíchat=Vmíchat,',
                                            '')) AS DECIMAL(10, 3))                       AS amount,
                          RIGHT(LEFT(RIGHT(ual.params, LENGTH(ual.params) - 20), 21), 10) AS batch_from,
                          RIGHT(LEFT(RIGHT(ual.params, LENGTH(ual.params) - 20), 42), 10) AS batch_to
                   FROM users_action_log ual
                   WHERE ual.`action` = 'tobacco/macerations-merge'
                     AND params LIKE '%merge_amount=%'
               ) s
          WHERE s.amount > 0
            AND s.batch_from LIKE 'L%'
            AND s.batch_to LIKE 'L%') wm
    ON wm.batch_to = m.batch AND wm.date = mh.last_change
SET mh.amount      = wm.amount,
    mh.last_change = mh.last_change
WHERE mh.`type` = 'merge_from';


UPDATE macerations m, (
    SELECT m.id, m.amount_in - SUM(ABS(mh.amount)) AS amount
    FROM macerations m
             JOIN macerations_history mh ON mh.id_macerations = m.id AND mh.`type` = 'check'
    WHERE DATE(mh.last_change) IN (
        SELECT DATE(last_change)
        FROM macerations_history
        WHERE id_macerations = m.id
          AND `type` = 'merge_from'
          AND amount = 0)
    GROUP BY m.id
) s
SET m.amount_in = IF(s.amount > 0, s.amount, 0)
WHERE m.id = s.id;

UPDATE macerations m, (
    SELECT m.id, m.amount_in - SUM(ABS(mh.amount)) AS amount
    FROM macerations m
             JOIN macerations_history mh ON mh.id_macerations = m.id AND mh.`type` = 'merge_from'
    GROUP BY m.id
) s
SET m.amount_in = IF(s.amount > 0, s.amount, 0)
WHERE m.id = s.id;

UPDATE macerations m
SET amount_in = 20
WHERE batch = 'L180605A02';
UPDATE macerations m
SET amount_in = 30
WHERE batch = 'L190524012';
UPDATE macerations m
SET amount_in = 20
WHERE batch = 'L180702033';
UPDATE macerations m
SET amount_in = 20
WHERE batch = 'L180702011';
UPDATE macerations m
SET amount_in = 30
WHERE batch = 'L180302A32';
UPDATE macerations m
SET amount_in = 5.001
WHERE batch = 'L190524032';
UPDATE macerations m
SET amount_in = 19.998
WHERE batch = 'L190204032';


UPDATE macerations_history mh
SET mh.amount      = 0,
    mh.last_change = mh.last_change
WHERE mh.`type` IN ('merge_to', 'merge_from')
  AND mh.last_change > '2019-04-01';

INSERT INTO macerations_history (id_macerations, id_users, amount, `type`, description, last_change)
SELECT m.id                AS id_macerations,
       28                  AS id_users,
       m.amount_in         AS amount,
       'create'            AS `type`,
       'založení macerátu' AS description,
       m.`start`           AS last_change
FROM macerations m;

INSERT INTO macerations_snapshot (id_macerations, id_users, amount_wh, amount_real, `date`, note, is_fixed)
SELECT m.id                AS id_macerations,
       28                  AS id_users,
       m.amount_in         AS amount_wh,
       m.amount_in         AS amount_real,
       m.`start`           AS `date`,
       'založení macerátu' AS note,
       'y'                 AS is_fixed
FROM macerations m;

-- reseni problemu s vadnym maceratem Winter Spices (receptura zmenena na Winter Season)
UPDATE macerations_history
SET id_macerations = 326,
    last_change    = last_change
WHERE id_macerations = 31
  AND `type` = 'pack'
  AND last_change < '2018-10-13';
UPDATE macerations_history
SET id_macerations = 346,
    last_change    = last_change
WHERE id_macerations = 31
  AND `type` = 'pack'
  AND last_change >= '2018-10-13';
DELETE
FROM macerations_snapshot
WHERE id_macerations = 31
  AND `date` > '2014-09-26';
DELETE
FROM macerations_history
WHERE id_macerations = 31
  AND last_change > '2014-09-26';

-- vytvoreni stare receptury
INSERT INTO recipes (`name`, `id_flavor`, `due`, `created_at`, `last_update`, `state`)
VALUES ('Winter Spices', '25', '2', '2014-11-14 17:26:24', '2014-11-14 17:26:24', 'inactive');
SET @id := LAST_INSERT_ID();
UPDATE macerations
SET amount_current = '6.010',
    id_recipes     = @id,
    checked        = NULL
WHERE id = '31';
INSERT INTO recipes_parts (`id_recipes`, `id_parts`, `ratio`)
SELECT @id AS id_recipes, id_parts, amount AS ratio
FROM macerations_parts
WHERE id_macerations = '31';

INSERT INTO parts_warehouse_snapshot (id_parts, id_users, amount_wh, amount_real, note, `date`, is_fixed)
SELECT
    id AS id_parts,
    14 AS id_users,
    0 AS amount_wh,
    0 AS amount_real,
    'init' AS note,
    '2019-01-01 00:00:00' AS `date`,
    'y' AS is_fixed
FROM parts p WHERE (p.id LIKE 'TA%' OR p.id LIKE 'TK%') AND p.id_wh_type = 2;

COMMIT;

INSERT INTO macerations_snapshot (id_macerations, id_users, id_macerations_history, amount_wh, amount_real, date, note, is_fixed) VALUES
(290, 28, NULL, 9.523, 9.523, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(334, 28, NULL, 12.354, 12.354, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(332, 28, NULL, 7.480, 7.480, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(344, 28, NULL, 8.263, 8.263, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(321, 28, NULL, 6.625, 6.625, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(343, 28, NULL, 20.553, 20.553, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(337, 28, NULL, 11.961, 11.961, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(352, 28, NULL, 21.238, 21.238, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(333, 28, NULL, 3.806, 3.806, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(320, 28, NULL, 4.688, 4.688, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(345, 28, NULL, 27.319, 27.319, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(330, 28, NULL, 4.688, 4.688, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(350, 28, NULL, 22.047, 22.047, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(340, 28, NULL, 31.728, 31.728, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(338, 28, NULL, 20.582, 20.582, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(349, 28, NULL, 18.561, 18.561, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(319, 28, NULL, 8.221, 8.221, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(316, 28, NULL, 1.884, 1.884, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(353, 28, NULL, 18.785, 18.785, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(288, 28, NULL, 1.756, 1.756, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(299, 28, NULL, 8.284, 8.284, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(279, 28, NULL, 17.799, 17.799, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(324, 28, NULL, 5.091, 5.091, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(348, 28, NULL, 28.977, 28.977, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(339, 28, NULL, 13.660, 13.660, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(300, 28, NULL, 25.494, 25.494, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(286, 28, NULL, 1.412, 1.412, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(351, 28, NULL, 28.830, 28.830, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(301, 28, NULL, 7.125, 7.125, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(346, 28, NULL, 24.924, 24.924, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(335, 28, NULL, 11.127, 11.127, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(341, 28, NULL, 8.751, 8.751, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(354, 28, NULL, 20.302, 20.302, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(355, 28, NULL, 5.584, 5.584, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(356, 28, NULL, 4.168, 4.168, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(343, 28, NULL, 0.050, 0.050, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(298, 28, NULL, 0.380, 0.380, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(361, 28, NULL, 6.008, 6.008, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(327, 28, NULL, 0.000, 0.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(359, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(328, 28, NULL, 1.082, 1.082, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(313, 28, NULL, 0.352, 0.352, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(271, 28, NULL, 0.400, 0.400, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(302, 28, NULL, 1.840, 1.840, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(360, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(357, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(323, 28, NULL, 0.440, 0.440, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(358, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(312, 28, NULL, 1.150, 1.150, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(281, 28, NULL, 0.400, 0.400, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(176, 28, NULL, 0.000, 0.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(329, 28, NULL, 2.040, 2.040, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(347, 28, NULL, 0.000, 0.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(87, 28, NULL, 0.000, 0.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(362, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(294, 28, NULL, 0.000, 0.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y');

INSERT INTO macerations_snapshot (id_macerations, id_users, id_macerations_history, amount_wh, amount_real, date, note, is_fixed) VALUES
(22, 28, NULL, 10.000, 10.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(23, 28, NULL, 10.000, 10.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(24, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(25, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(26, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(27, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(28, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(29, 28, NULL, 6.010, 6.010, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(30, 28, NULL, 6.000, 6.000, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y'),
(31, 28, NULL, 6.010, 6.010, '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y');

UPDATE macerations SET description = '5th Anniversary', name = '5th Anniversary' WHERE  id = 347;
UPDATE macerations SET name = 'Alphonso' WHERE  id = 294;

CREATE TEMPORARY TABLE tmp_delete
SELECT mh.id AS id_macerations_history, ms.id AS id_macerations_snapshot FROM macerations_snapshot ms
JOIN macerations_history mh ON ms.id_macerations_history = mh.id
WHERE ms.`date` > '2019-01-01';
DELETE FROM macerations_snapshot WHERE id IN (SELECT id_macerations_snapshot FROM tmp_delete);
DELETE FROM macerations_history WHERE id IN (SELECT id_macerations_history FROM tmp_delete);
DROP TABLE tmp_delete;

UPDATE macerations_snapshot SET amount_real = 20.603, amount_wh = 20.603 WHERE id = 772;
DELETE FROM macerations_snapshot  WHERE id = 802;


DELETE FROM macerations_snapshot WHERE id_macerations = 398;
DELETE FROM macerations_history WHERE id_macerations = 398;
DELETE FROM macerations_parts WHERE id_macerations = 398;
DELETE FROM macerations WHERE id = 398;

UPDATE macerations m JOIN (
    SELECT SUM(ms.amount_real) + SUM(h.changes) AS amount,
           m.id_recipes
    FROM macerations m
             LEFT JOIN macerations_snapshot ms ON m.id = ms.id_macerations AND ms.`date` = '2019-01-01'
             LEFT JOIN (
        SELECT mh.id_macerations, SUM(mh.amount) AS changes FROM macerations_history mh
                                                                     JOIN macerations m ON mh.id_macerations = m.id
        WHERE mh.last_change >= '2019-01-01' AND mh.`type` IN ('pack', 'break', 'create')
        GROUP BY mh.id_macerations) h ON h.id_macerations = m.id
    GROUP BY m.id_recipes
    HAVING amount IS NOT NULL
) a ON a.id_recipes = m.id_recipes
SET m.amount_current = IF(a.amount < 0, 0, a.amount)
WHERE m.amount_current > 0;


INSERT INTO parts_warehouse_snapshot (`id_parts`, `id_users`, `amount_wh`, `amount_real`, `date`, `note`, `is_fixed`) VALUES
('TS001', '28', '0', '0', '2019-01-01 00:00:00', 'inventura 31.12.2019', 'y');
DELETE FROM parts_warehouse_history WHERE id_parts = 'TS001' AND amount < 0 AND last_change >= '2019-01-01';
INSERT INTO parts_warehouse_history (amount, last_change, id_users, id_parts)
SELECT ROUND(m.amount_in / 6 * -1000) AS amount,
       m.`start` AS last_change,
       28 AS id_users,
       'TS001' AS id_parts
FROM macerations m WHERE m.batch LIKE 'L19%';
UPDATE parts_warehouse pw SET pw.amount = (
    SELECT 450000 + SUM(ROUND(m.amount_in / 6 * -1000)) AS current_amount
    FROM macerations m WHERE m.batch LIKE 'L19%')
WHERE pw.id_parts = 'TS001';
UPDATE parts_warehouse_history SET last_change = '2019-01-29 08:10:35' WHERE  id = 284207;
UPDATE productions SET date_delivery = '2019-01-29' WHERE  id = 1476;
INSERT INTO macerations_history (id_macerations, id_users, amount, `type`, description, last_change) VALUES
(390, 28, -0.05, 'break', 'xxx', '2019-12-20'), (432, 28, 5.459, 'break', 'xxx', '2019-12-20');
UPDATE macerations_history mh SET mh.amount = mh.amount - 0.014 WHERE mh.id = 28894;
UPDATE macerations_history mh SET mh.amount = mh.amount - 0.012 WHERE mh.id = 28893;
UPDATE macerations_history SET amount = '10.002' WHERE id = 28931;
DELETE FROM macerations_parts WHERE `id` = 2527;
DELETE FROM macerations_parts WHERE `id` = 2528;