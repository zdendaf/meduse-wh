--
-- Toto je soubor pro databazove zmeny.
-- Zmeny vkladame na zacatek souboru - ne na konec (nejnovejsi zmeny jsou nahore).
-- Zmenu v tomto souboru nesmi obsahovat cele nazvy tabulek (ne db_name.db_table, jen db_table).
-- Kazda zmena databaze musi byt uvozena komentarem ve tvaru:
--
-- YYYY-MM-DD #123 - název issue z eventum

--
-- 2016-06-27 #661 - Storno faktura
--
INSERT INTO users_acl (role, resource) VALUES
(NULL, 'tobacco-invoice/create-credit-note'),
('admin', 'tobacco-invoice/create-credit-note'),
('manager', 'tobacco-invoice/create-credit-note'),
('tobacco_seller', 'tobacco-invoice/create-credit-note');

--
-- 2016-06-09 #633 - Proces objednávek
--
ALTER TABLE `actualities`
	ADD COLUMN `id_wh_type` INT(11) NOT NULL DEFAULT '1' AFTER `id_users`;
ALTER TABLE `emails`
	CHANGE COLUMN `inserted` `inserted` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `id_orders`;

--
-- 2016-05-18 #635 - Seed ID
--
ALTER TABLE `customers`
	ADD COLUMN `seed_id` VARCHAR(13) NULL DEFAULT NULL COMMENT 'id celniho skladu (pro tabak)' AFTER `vat_no`;

--
-- 2016-05-18 #639 - Upozornění mailem
--
INSERT INTO users_acl (role, resource)
VALUES ('tobacco_seller', 'email-templates/ajax-get-template');
DELETE FROM users_acl WHERE resource LIKE 'tobacco-orders/notify' AND role LIKE 'tobacco_whman';
INSERT INTO users_acl (role, resource) VALUES
('tobacco_seller', 'tobacco-orders/notify'),
('sales_director', 'tobacco-orders/notify'),
('seller', 'tobacco-orders/notify');

--
-- 2016-04-25 #616 - Zpřesnění cen
--
ALTER TABLE `invoice_custom_items`
	ALTER `price` DROP DEFAULT;
ALTER TABLE `invoice_custom_items`
	CHANGE COLUMN `price` `price` DECIMAL(13,5) NOT NULL COMMENT 'cena polozky' AFTER `description`;
ALTER TABLE `invoice_items`
	CHANGE COLUMN `price` `price` DECIMAL(13,5) NULL DEFAULT NULL COMMENT 'cena polozky' AFTER `text_free`,
	CHANGE COLUMN `price_free` `price_free` DECIMAL(13,5) NULL DEFAULT NULL COMMENT 'cena free polozky' AFTER `price`;

--
-- 2016-03-11 #616 - Zpřesnění cen
--
ALTER TABLE `prices`
	ALTER `price` DROP DEFAULT;
ALTER TABLE `prices`
	CHANGE COLUMN `price` `price` DECIMAL(13,5) UNSIGNED NOT NULL COMMENT 'cena v eurech' AFTER `id_pricelists`;

--
-- 2015-11-16 #393 - Reporty - historie
--

ALTER TABLE `users_action_log`
	ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT FIRST,
	ADD PRIMARY KEY (`id`);


INSERT INTO users_acl (role, resource) VALUES
(null,            'tobacco-reports/history'),
('admin',         'tobacco-reports/history'),
('manager',       'tobacco-reports/history'),
('tobacco_whman', 'tobacco-reports/history');

--
-- 2015-08-12 #542 - PL - návrh rozhraní
--

INSERT INTO users_acl (role, resource) VALUES
(null, 'packings/pack'),
('admin', 'packings/pack'),
('manager', 'packings/pack'),
('tobacco_whman', 'packings/pack');

--
-- 2015-07-23 #531 - Optimalizace reportů pro tisk
--
DELETE FROM users_acl WHERE resource='tobacco/reports'

--
-- 2015-07-20 #533 - Sloučení sudů macerátů
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/macerations-merge'),
('admin', 'tobacco/macerations-merge'),
('manager', 'tobacco/macerations-merge'),
('tobacco_whman', 'tobacco/macerations-merge');

--
-- 2015-07-15 #520 - hromadné kolkování
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/cancel-all-prepared-total'),
('admin', 'tobacco-orders/cancel-all-prepared-total'),
('manager', 'tobacco-orders/cancel-all-prepared-total'),
('tobacco_whman', 'tobacco-orders/cancel-all-prepared-total');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/prepare-all-total'),
('admin', 'tobacco-orders/prepare-all-total'),
('manager', 'tobacco-orders/prepare-all-total'),
('tobacco_whman', 'tobacco-orders/prepare-all-total');

--
-- 2015-07-13 #512 - Reporty - Ekokom
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-reports/ekokom'),
('admin', 'tobacco-reports/ekokom'),
('manager', 'tobacco-reports/ekokom'),
('tobacco_whman', 'tobacco-reports/ekokom');

--
-- 2015-07-09 #520 - hromadné kolkování
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/stamp-all'),
('admin', 'tobacco-orders/stamp-all'),
('manager', 'tobacco-orders/stamp-all'),
('tobacco_whman', 'tobacco-orders/stamp-all');

--
-- 2015-06-29 #520 - hromadné kolkování
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/prepare-list'),
('admin', 'tobacco-orders/prepare-list'),
('manager', 'tobacco-orders/prepare-list'),
('tobacco_whman', 'tobacco-orders/prepare-list');

--
-- 2015-06-11 #512 - Reporty - Ekokom
--
CREATE TABLE `ekokom_ctg` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL COMMENT 'nazev skupiny' COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`id`)
)
COMMENT='skupiny obaloveho materialu EKOKOM'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB
AUTO_INCREMENT=7;
INSERT INTO `ekokom_ctg` (`id`, `name`) VALUES
	(1, 'papír'),
	(2, 'sklo'),
	(3, 'plasty měkké'),
	(4, 'plasty pevné'),
	(5, 'kov'),
	(6, 'kombinované materiály');

CREATE TABLE `parts_weight` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_parts` VARCHAR(10) NOT NULL COMMENT 'cizi klic na soucast' COLLATE 'utf8_czech_ci',
	`id_ekokom_ctg` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic na skupiny EKOKOM',
	`weight` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT 'vaha soucasti v gramech',
	PRIMARY KEY (`id`),
	INDEX `id_ekokom_ctg` (`id_ekokom_ctg`),
	UNIQUE INDEX `id_parts` (`id_parts`),
	CONSTRAINT `FK_parts_weight_parts` FOREIGN KEY (`id_parts`) REFERENCES `parts` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_parts_weight_ekokom_ctg` FOREIGN KEY (`id_ekokom_ctg`) REFERENCES `ekokom_ctg` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)
COMMENT='vaha soucasti s jejim zarazenim do skupiny EKOKOM'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;




--
-- 2015-06-11 #516 - Upozornění emailem
--

ALTER TABLE `email_templates`
	ADD COLUMN `id_wh_type` INT NOT NULL DEFAULT '1' COMMENT 'sklad, ke kteremu patri' AFTER `changed`,
	ADD COLUMN `default_for` INT NOT NULL DEFAULT '0' COMMENT 'vychozi sablona pro typ' AFTER `id_wh_type`;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-settings/email-templates'),
('admin', 'tobacco-settings/email-templates'),
('manager', 'tobacco-settings/email-templates'),
('tobacco_whman', 'tobacco-settings/email-templates'),
(null, 'tobacco-settings/email-templates-add'),
('admin', 'tobacco-settings/email-templates-add'),
('manager', 'tobacco-settings/email-templates-add'),
('tobacco_whman', 'tobacco-settings/email-templates-add'),
(null, 'tobacco-settings/email-templates-edit'),
('admin', 'tobacco-settings/email-templates-edit'),
('manager', 'tobacco-settings/email-templates-edit'),
('tobacco_whman', 'tobacco-settings/email-templates-edit'),
(null, 'tobacco-settings/email-templates-save'),
('admin', 'tobacco-settings/email-templates-save'),
('manager', 'tobacco-settings/email-templates-save'),
('tobacco_whman', 'tobacco-settings/email-templates-save'),
(null, 'tobacco-settings/email-templates-delete'),
('admin', 'tobacco-settings/email-templates-delete'),
('manager', 'tobacco-settings/email-templates-delete'),
('tobacco_whman', 'tobacco-settings/email-templates-delete');

--
-- 2015-06-08 #521 - limitní množství
--

ALTER TABLE `parts_warehouse`
	CHANGE COLUMN `critical_amount` `critical_amount` INT(6) NOT NULL DEFAULT '10' AFTER `amount`;

ALTER TABLE `macerations`
	ADD COLUMN `amount_critical` DECIMAL(9,3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'limitní množství' AFTER `amount_current`;

--
-- 2015-06-04 #516 - Upozornění emailem
--

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/notify'),
('admin', 'tobacco-orders/notify'),
('manager', 'tobacco-orders/notify'),
('tobacco_whman', 'tobacco-orders/notify');

--
-- 2015-05-21 #511 - Výchozí ceník
--

ALTER TABLE `pricelists`
	ADD COLUMN `default` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'vychozi cenik' AFTER `valid`;

--
-- 2015-05-04 #387 - Reporty - celkové množství tabáku
--

ALTER TABLE `batch_mixes`
	CHANGE COLUMN `batch` `batch` VARCHAR(10) NULL DEFAULT NULL COMMENT 'cislo sarze mixu' COLLATE 'utf8_czech_ci' AFTER `id_macerations`;

--
-- 2015-05-04 #387 - Reporty - celkové množství tabáku
--

ALTER TABLE `invoice_items`
	ALTER `id_parts` DROP DEFAULT;
ALTER TABLE `invoice_items`
	CHANGE COLUMN `id_parts` `id_parts` VARCHAR(10) NOT NULL COMMENT 'cizi klic na produkt' COLLATE 'utf8_czech_ci' AFTER `id_orders`,
	CHANGE COLUMN `text` `text` VARCHAR(255) NULL DEFAULT NULL COMMENT 'text polozky' COLLATE 'utf8_czech_ci' AFTER `id_parts`,
	CHANGE COLUMN `text_free` `text_free` VARCHAR(255) NULL DEFAULT NULL COMMENT 'text free polozky' COLLATE 'utf8_czech_ci' AFTER `text`;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-reports/waste'),
('admin', 'tobacco-reports/waste'),
('manager', 'tobacco-reports/waste'),
('tobacco_whman', 'tobacco-reports/waste');

--
-- 2015-04-27 #504 - Reporty - prodeje
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-reports/sales'),
('admin', 'tobacco-reports/sales'),
('manager', 'tobacco-reports/sales'),
('tobacco_whman', 'tobacco-reports/sales');

--
-- 2015-04-27 #504 - Reporty - prodeje
--
CREATE TABLE `invoice_items` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_invoice` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na fakturu',
	`id_orders` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na objednavku',
	`id_parts` VARCHAR(10) NOT NULL COMMENT 'cizi klic na produkt' COLLATE 'utf8_general_ci',
	`text` VARCHAR(255) NULL DEFAULT NULL COMMENT 'text polozky' COLLATE 'utf8_general_ci',
	`text_free` VARCHAR(255) NULL DEFAULT NULL COMMENT 'text free polozky' COLLATE 'utf8_general_ci',
	`price` DECIMAL(10,2) NULL DEFAULT NULL COMMENT 'cena polozky',
	`price_free` DECIMAL(10,2) NULL DEFAULT NULL COMMENT 'cena free polozky',
	PRIMARY KEY (`id`)
)
COMMENT='produktove polozky faktury'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

--
-- 2015-04-27 #387 - Reporty - celkové množství tabáku
--

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-reports/amount'),
('admin', 'tobacco-reports/amount'),
('manager', 'tobacco-reports/amount'),
('tobacco_whman', 'tobacco-reports/amount');

--
-- 2015-04-27 #415 - Pomoc s naskladňováním
--
INSERT INTO users_acl (role, resource) VALUES
('tobacco_whman', 'tobacco/parts-delete');

--
-- 2015-04-27 #474 - Importovat ceníky
--
ALTER TABLE `prices_coefs`
	ALTER `id_pricelists` DROP DEFAULT;
ALTER TABLE `prices_coefs`
	CHANGE COLUMN `id_pricelists` `id_pricelists_b2b` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na cenik b2b' AFTER `point`,
	ADD COLUMN `id_pricelists_b2c` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na cenik b2c' AFTER `id_pricelists_b2b`;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/set-pricelist'),
('admin', 'tobacco-orders/set-pricelist'),
('manager', 'tobacco-orders/set-pricelist'),
('all_seller', 'tobacco-orders/set-pricelist'),
('tobacco_seller', 'tobacco-orders/set-pricelist'),
('tobacco_whman', 'tobacco-orders/set-pricelist');

--
-- 2015-04-23 #507 - Správa číselníku příchutí
--

ALTER TABLE `batch_flavors`
	CHANGE COLUMN `is_mix` `is_mix` VARCHAR(1) NOT NULL DEFAULT 'n' COMMENT 'jedna se o mix prichuti' AFTER `name`;

UPDATE batch_flavors SET is_mix='y' WHERE is_mix='1';
UPDATE batch_flavors SET is_mix='n' WHERE is_mix='0';

ALTER TABLE `batch_flavors`
	ALTER `id` DROP DEFAULT;
ALTER TABLE `batch_flavors`
	CHANGE COLUMN `id` `id` INT(10) UNSIGNED NOT NULL FIRST;

--
-- 2015-04-23 #474 - Importovat ceníky
--
ALTER TABLE `prices_coefs`
	DROP COLUMN `id_pricescoefssets`,
	DROP COLUMN `coef`,
	ADD COLUMN `id_pricelists` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na cenik';
DROP TABLE `prices_coefs_sets`;

--
-- 2015-04-20 #507 - Správa číselníku příchutí
--

ALTER TABLE `batch_flavors`
	ADD COLUMN `active` VARCHAR(1) NOT NULL DEFAULT 'y' COMMENT 'neni smazane?' AFTER `is_mix`;

ALTER TABLE `batch_flavors`
	CHANGE COLUMN `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
	ADD PRIMARY KEY (`id`);


INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-settings/flavor-keys'),
('admin', 'tobacco-settings/flavor-keys'),
('manager', 'tobacco-settings/flavor-keys'),
('tobacco_whman', 'tobacco-settings/flavor-keys');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-settings/flavor-keys-delete'),
('admin', 'tobacco-settings/flavor-keys-delete'),
('manager', 'tobacco-settings/flavor-keys-delete'),
('tobacco_whman', 'tobacco-settings/flavor-keys-delete');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-settings/flavor-keys-edit'),
('admin', 'tobacco-settings/flavor-keys-edit'),
('manager', 'tobacco-settings/flavor-keys-edit'),
('tobacco_whman', 'tobacco-settings/flavor-keys-edit');

--
-- 2015-04-09 # 493 - Předpokládaná/výsledná cena
--
ALTER TABLE `application_rate`
	CHANGE COLUMN `rate` `rate` FLOAT UNSIGNED NOT NULL DEFAULT '25' AFTER `id_wh_type`;
ALTER TABLE `application_rate`
	CHANGE COLUMN `id_wh_type` `id_wh_type` INT(1) NOT NULL DEFAULT '1' AFTER `id`;

--
-- 2015-04-09 #499 - Našeptávač názvu objednávky
--
INSERT INTO users_acl (role, resource, description)
SELECT role, 'tobacco-order-edit/ajax-customer' AS resource, description
FROM users_acl WHERE resource LIKE 'order-edit/ajax-customer';

--
-- 2015-04-09 #492 - Výchozí kurz
--

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-settings/excise-add'),
('admin', 'tobacco-settings/excise-add'),
('manager', 'tobacco-settings/excise-add'),
('tobacco_whman', 'tobacco-settings/excise-add');

ALTER TABLE `application_rate`
	CHANGE COLUMN `rate` `rate` FLOAT UNSIGNED NOT NULL DEFAULT '1' AFTER `id_wh_type`;

--
-- 2015-04-06 #492 - Výchozí kurz
--

CREATE TABLE `application_excise` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`value` MEDIUMINT(9) NOT NULL,
	`active_from_date` DATE NOT NULL,
	`inserted` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-settings/excise'),
('admin', 'tobacco-settings/excise'),
('manager', 'tobacco-settings/excise'),
('tobacco_whman', 'tobacco-settings/excise');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-settings/excise-delete'),
('admin', 'tobacco-settings/excise-delete'),
('manager', 'tobacco-settings/excise-delete'),
('tobacco_whman', 'tobacco-settings/excise-delete');

--
-- 2015-04-05 #492 - Výchozí kurz
--

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-settings/rate-update'),
('admin', 'tobacco-settings/rate-update'),
('manager', 'tobacco-settings/rate-update'),
('tobacco_whman', 'tobacco-settings/rate-update');

--
-- 2015-04-02 #492 - Výchozí kurz
--

ALTER TABLE `application_rate`
	ADD COLUMN `id_wh_type` INT(1) NOT NULL DEFAULT '0' AFTER `id`;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-settings/rate'),
('admin', 'tobacco-settings/rate'),
('manager', 'tobacco-settings/rate'),
('tobacco_whman', 'tobacco-settings/rate');

--
-- 2015-03-31 #494 - Drobné úpravy faktury
--
ALTER TABLE `customers`
	ADD COLUMN `ident_no` VARCHAR(100) NULL DEFAULT NULL COMMENT 'identifikacni cislo (ic)' COLLATE 'utf8_czech_ci' AFTER `company`;


--
-- 2015-03-30 #474 - Importovat ceníky
--
ALTER TABLE `prices_coefs`
	ALTER `coef` DROP DEFAULT;
ALTER TABLE `prices_coefs`
	CHANGE COLUMN `coef` `coef` INT(2) NOT NULL AFTER `point`;

ALTER TABLE `prices_coefs`
	ALTER `point` DROP DEFAULT,
	ALTER `id_pricescoefssets` DROP DEFAULT;
ALTER TABLE `prices_coefs`
	CHANGE COLUMN `point` `point` DECIMAL(9,3) NOT NULL AFTER `id`,
	CHANGE COLUMN `coef` `coef` INT(2) NOT NULL DEFAULT '100' AFTER `point`,
	CHANGE COLUMN `id_pricescoefssets` `id_pricescoefssets` INT(11) NOT NULL AFTER `coef`;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricecoefs/view'),
('admin', 'tobacco-pricecoefs/view'),
('manager', 'tobacco-pricecoefs/view'),
('tobacco_whman', 'tobacco-pricecoefs/view'),
(null, 'tobacco-pricecoefs/edit'),
('admin', 'tobacco-pricecoefs/edit'),
('manager', 'tobacco-pricecoefs/edit'),
('tobacco_whman', 'tobacco-pricecoefs/edit'),
(null, 'tobacco-pricecoefs/save'),
('admin', 'tobacco-pricecoefs/save'),
('manager', 'tobacco-pricecoefs/save'),
('tobacco_whman', 'tobacco-pricecoefs/save'),
(null, 'tobacco-pricecoefs/delete'),
('admin', 'tobacco-pricecoefs/delete'),
('manager', 'tobacco-pricecoefs/delete'),
('tobacco_whman', 'tobacco-pricecoefs/delete');


--
-- 2015-03-30 #491 - Role obchodníka
--
INSERT INTO `users_roles` (`is_active`, `role_name`, `role_translation`, `role_description`)
  VALUES (1, 'tobacco_seller', 'Obchodník tabáků', 'Obchodník skladu tabáku');
INSERT INTO `users_roles` (`is_active`, `role_name`, `role_translation`, `role_description`)
  VALUES (1, 'all_seller', 'Obchodník D+T', 'Obchodník obou skladů');

INSERT INTO users_acl (id, role, resource, description)
  VALUES (11, 'tobacco_seller', NULL, 'definice role');
INSERT INTO users_acl (id, role, resource, description)
  VALUES (10, 'all_seller', NULL, 'definice role');

INSERT INTO users_acl (role, resource, description)
	SELECT 'all_seller' AS role, resource, description FROM users_acl
    WHERE role LIKE 'seller' AND resource IS NOT NULL;

ALTER TABLE `users`
	ALTER `role` DROP DEFAULT;
ALTER TABLE `users`
	CHANGE COLUMN `role` `role` ENUM(
    'admin',
    'manager',
    'head_warehouseman',
    'warehouseman',
    'extern_wh',
    'seller',
    'brigadier',
    'tobacco_whman',
    'issue_reader',
    'all_seller',
    'tobacco_seller'
  ) NOT NULL COLLATE 'utf8_czech_ci' AFTER `phone`;

INSERT INTO users_acl (role, resource)
	SELECT  'tobacco_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-orders/%'
		AND role IS NULL
		AND resource NOT IN (
			'tobacco-orders/cancel-all-prepared',
			'tobacco-orders/prepare-all',
			'tobacco-orders/prepare',
			'tobacco-orders/expedition'
		);
INSERT INTO users_acl (role, resource)
	SELECT  'all_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-orders/%'
		AND role IS NULL
		AND resource NOT IN (
			'tobacco-orders/cancel-all-prepared',
			'tobacco-orders/prepare-all',
			'tobacco-orders/prepare',
			'tobacco-orders/expedition'
		);

INSERT INTO users_acl (role, resource)
	SELECT  'tobacco_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-order-edit/%'
		AND role IS NULL;
INSERT INTO users_acl (role, resource)
	SELECT  'all_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-order-edit/%'
		AND role IS NULL;

INSERT INTO users_acl (role, resource)
	SELECT  'tobacco_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-customers/%'
		AND role IS NULL;
INSERT INTO users_acl (role, resource)
	SELECT  'all_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-customers/%'
		AND role IS NULL;

INSERT INTO users_acl (role, resource)
	SELECT  'tobacco_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-invoice/%'
		AND role IS NULL;
INSERT INTO users_acl (role, resource)
	SELECT  'all_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-invoice/%'
		AND role IS NULL;

INSERT INTO users_acl (role, resource)
	SELECT  'tobacco_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-pricelists/%'
		AND role IS NULL;
INSERT INTO users_acl (role, resource)
	SELECT  'all_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-pricelists/%'
		AND role IS NULL;

INSERT INTO users_acl (role, resource)
	SELECT  'tobacco_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-pricecoefs/%'
		AND role IS NULL;
INSERT INTO users_acl (role, resource)
	SELECT  'all_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'tobacco-pricecoefs/%'
		AND role IS NULL;

INSERT INTO users_acl (role, resource) VALUES ('all_seller', 'index/switch-warehouse');
INSERT INTO users_acl (role, resource) VALUES ('tobacco_seller', 'index');
INSERT INTO users_acl (role, resource) VALUES ('tobacco_seller', 'index/index');
INSERT INTO users_acl (role, resource) VALUES ('tobacco_seller', 'login/index');
INSERT INTO users_acl (role, resource) VALUES ('tobacco_seller', 'login/process');
INSERT INTO users_acl (role, resource) VALUES ('tobacco_seller', 'login/clear');

INSERT INTO users_acl (role, resource) VALUES ('tobacco_seller', 'orders/select-products');


INSERT INTO users_acl (role, resource)
	SELECT  'tobacco_seller' AS role, resource  FROM users_acl
	WHERE resource LIKE 'issues/%'
		AND role IS NULL;

--
-- 2015-03-26 #474 - Importovat ceníky
--
CREATE TABLE `prices_coefs` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`point` INT NOT NULL,
	`coef` INT(3) NOT NULL DEFAULT '100',
	`id_pricescoefssets` INT(3) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB
;


CREATE TABLE `prices_coefs_sets` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL COLLATE 'utf8_czech_ci',
	`active` INT(1) NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB
;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricecoefs/index'),
('admin', 'tobacco-pricecoefs/index'),
('manager', 'tobacco-pricecoefs/index'),
('tobacco_whman', 'tobacco-pricecoefs/index'),
(null, 'tobacco-pricecoefs/sets-delete'),
('admin', 'tobacco-pricecoefs/sets-delete'),
('manager', 'tobacco-pricecoefs/sets-delete'),
('tobacco_whman', 'tobacco-pricecoefs/sets-delete'),
(null, 'tobacco-pricecoefs/sets-edit'),
('admin', 'tobacco-pricecoefs/sets-edit'),
('manager', 'tobacco-pricecoefs/sets-edit'),
('tobacco_whman', 'tobacco-pricecoefs/sets-edit'),
(null, 'tobacco-pricecoefs/sets-save'),
('admin', 'tobacco-pricecoefs/sets-save'),
('manager', 'tobacco-pricecoefs/sets-save'),
('tobacco_whman', 'tobacco-pricecoefs/sets-save');
--
-- 2015-03-23 #468 - Volba ceníků
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/get-calculator-data'),
('admin', 'tobacco-orders/get-calculator-data'),
('manager', 'tobacco-orders/get-calculator-data'),
('tobacco_whman', 'tobacco-orders/get-calculator-data');


--
-- 2015-03-10 #459 - Fakturace
--
ALTER TABLE `prices`
	ADD CONSTRAINT `FK_prices_parts` FOREIGN KEY (`id_parts`)
  REFERENCES `parts` (`id`)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

--
-- 2014-03-03 #459 - Fakturace
--
ALTER TABLE `accounts`
	ADD COLUMN `id_wh_type` INT(11) UNSIGNED NOT NULL DEFAULT '1' AFTER `id`;
ALTER TABLE `invoice_custom_texts`
	ADD COLUMN `id_wh_type` INT(11) UNSIGNED NOT NULL DEFAULT '1' AFTER `id`;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/generate'),
('admin', 'tobacco-invoice/generate'),
('manager', 'tobacco-invoice/generate'),
('tobacco_whman', 'tobacco-invoice/generate');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/save-item-texts'),
('admin', 'tobacco-invoice/save-item-texts'),
('manager', 'tobacco-invoice/save-item-texts'),
('tobacco_whman', 'tobacco-invoice/save-item-texts');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/reset-texts'),
('admin', 'tobacco-invoice/reset-texts'),
('manager', 'tobacco-invoice/reset-texts'),
('tobacco_whman', 'tobacco-invoice/reset-texts');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/reset-prices'),
('admin', 'tobacco-invoice/reset-prices'),
('manager', 'tobacco-invoice/reset-prices'),
('tobacco_whman', 'tobacco-invoice/reset-prices');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/add-custom-item'),
('admin', 'tobacco-invoice/add-custom-item'),
('manager', 'tobacco-invoice/add-custom-item'),
('tobacco_whman', 'tobacco-invoice/add-custom-item');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/custom-texts'),
('admin', 'tobacco-invoice/custom-texts'),
('manager', 'tobacco-invoice/custom-texts'),
('tobacco_whman', 'tobacco-invoice/custom-texts');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/custom-texts-edit'),
('admin', 'tobacco-invoice/custom-texts-edit'),
('manager', 'tobacco-invoice/custom-texts-edit'),
('tobacco_whman', 'tobacco-invoice/custom-texts-edit');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/custom-texts-remove'),
('admin', 'tobacco-invoice/custom-texts-remove'),
('manager', 'tobacco-invoice/custom-texts-remove'),
('tobacco_whman', 'tobacco-invoice/custom-texts-remove');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/custom-texts-get-json'),
('admin', 'tobacco-invoice/custom-texts-get-json'),
('manager', 'tobacco-invoice/custom-texts-get-json'),
('tobacco_whman', 'tobacco-invoice/custom-texts-get-json');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/json-get-custom-item'),
('admin', 'tobacco-invoice/json-get-custom-item'),
('manager', 'tobacco-invoice/json-get-custom-item'),
('tobacco_whman', 'tobacco-invoice/json-get-custom-item');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/remove-custom-item'),
('admin', 'tobacco-invoice/remove-custom-item'),
('manager', 'tobacco-invoice/remove-custom-item'),
('tobacco_whman', 'tobacco-invoice/remove-custom-item');

INSERT INTO users_acl (role, resource) VALUES
('tobacco_whman', 'admin/json-get-cnb-eur-rate');

--
-- 2014-02-27 #459 - Fakturace
--
ALTER TABLE `pricelists`
	ADD COLUMN `id_wh_type` INT(11) UNSIGNED NOT NULL DEFAULT '1'
    COMMENT 'jedna se o sklad dymek nebo tabaku' AFTER `id`;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricelists/index'),
('admin', 'tobacco-pricelists/index'),
('manager', 'tobacco-pricelists/index'),
('tobacco_whman', 'tobacco-pricelists/index');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricelists/edit'),
('admin', 'tobacco-pricelists/edit'),
('manager', 'tobacco-pricelists/edit'),
('tobacco_whman', 'tobacco-pricelists/edit');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricelists/delete'),
('admin', 'tobacco-pricelists/delete'),
('manager', 'tobacco-pricelists/delete'),
('tobacco_whman', 'tobacco-pricelists/delete');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricelists/assign'),
('admin', 'tobacco-pricelists/assign'),
('manager', 'tobacco-pricelists/assign'),
('tobacco_whman', 'tobacco-pricelists/assign');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricelists/get-pricelists-ajax'),
('admin', 'tobacco-pricelists/get-pricelists-ajax'),
('manager', 'tobacco-pricelists/get-pricelists-ajax'),
('tobacco_whman', 'tobacco-pricelists/get-pricelists-ajax');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricelists/get-pricelist-ajax'),
('admin', 'tobacco-pricelists/get-pricelist-ajax'),
('manager', 'tobacco-pricelists/get-pricelist-ajax'),
('tobacco_whman', 'tobacco-pricelists/get-pricelist-ajax');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricelists/import'),
('admin', 'tobacco-pricelists/import'),
('manager', 'tobacco-pricelists/import'),
('tobacco_whman', 'tobacco-pricelists/import');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricelists/ajax-get-info'),
('admin', 'tobacco-pricelists/ajax-get-info'),
('manager', 'tobacco-pricelists/ajax-get-info'),
('tobacco_whman', 'tobacco-pricelists/ajax-get-info');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-pricelists/export'),
('admin', 'tobacco-pricelists/export'),
('manager', 'tobacco-pricelists/export'),
('tobacco_whman', 'tobacco-pricelists/export');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-prices/edit'),
('admin', 'tobacco-prices/edit'),
('manager', 'tobacco-prices/edit'),
('tobacco_whman', 'tobacco-prices/edit');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-prices/save'),
('admin', 'tobacco-prices/save'),
('manager', 'tobacco-prices/save'),
('tobacco_whman', 'tobacco-prices/save');

--
-- 2014-02-25 #459 - Fakturace
--
ALTER TABLE `invoice`
	ADD COLUMN `id_wh_type` INT(11) UNSIGNED NULL DEFAULT '1'
    COMMENT 'jedna se o sklad dymek nebo tabaku' AFTER `id_pricelists`;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/index'),
('admin', 'tobacco-invoice/index'),
('manager', 'tobacco-invoice/index'),
('tobacco_whman', 'tobacco-invoice/index');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-invoice/prepare'),
('admin', 'tobacco-invoice/prepare'),
('manager', 'tobacco-invoice/prepare'),
('tobacco_whman', 'tobacco-invoice/prepare');


--
-- 2014-02-25 #467 - 	Obal a kolek produktem
--
UPDATE parts SET product = 'n'
WHERE (id_parts_ctg = 36 OR id_parts_ctg = 38) AND id_wh_type = 2;


--
-- 2014-01-16 #457 - Nová kategorie Doplňky
--
INSERT INTO parts_ctg (id, parent_ctg, ctg_order, name) VALUES (43, 34, 5080, 'Doplňky');
UPDATE parts SET product = 'y' WHERE id_parts_ctg = 37;

--
-- 2014-01-13 #437 - Výběr šarže při rezervaci
--
CREATE TABLE `orders_products_batch` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_orders_products` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na polozky objednavky',
	`batch` VARCHAR(10) NOT NULL COMMENT 'cislo sarze' COLLATE 'utf8_czech_ci',
	`amount` INT(6) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'pocet kusu',
	PRIMARY KEY (`id`)
)
COMMENT='cisla sarzi k tabakovym produktum na objednavce'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

--
-- 2014-01-07 #415 - Pomoc s naskladňováním - moznost odkolkovani
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/parts-break'),
('admin', 'tobacco/parts-break'),
('manager', 'tobacco/parts-break'),
('tobacco_whman', 'tobacco/parts-break');

--
-- 2014-12-09 #445 - Vrácení zásilky
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/return'),
('admin', 'tobacco-orders/return'),
('manager', 'tobacco-orders/return'),
('tobacco_whman', 'tobacco-orders/return');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/delete'),
('admin', 'tobacco-orders/delete'),
('manager', 'tobacco-orders/delete'),
('tobacco_whman', 'tobacco-orders/delete');

--
-- 2014-11-14 #410 - Vyskladnění prodaných produktů – objednávky
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/delete-item'),
('admin', 'tobacco-orders/delete-item'),
('manager', 'tobacco-orders/delete-item'),
('tobacco_whman', 'tobacco-orders/delete-item');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/prepare'),
('admin', 'tobacco-orders/prepare'),
('manager', 'tobacco-orders/prepare'),
('tobacco_whman', 'tobacco-orders/prepare');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/prepare-all'),
('admin', 'tobacco-orders/prepare-all'),
('manager', 'tobacco-orders/prepare-all'),
('tobacco_whman', 'tobacco-orders/prepare-all');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/cancel-all-prepared'),
('admin', 'tobacco-orders/cancel-all-prepared'),
('manager', 'tobacco-orders/cancel-all-prepared'),
('tobacco_whman', 'tobacco-orders/cancel-all-prepared');

--
-- 2014-11-13 #410 - Vyskladnění prodaných produktů – objednávky
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/open'),
('admin', 'tobacco-orders/open'),
('manager', 'tobacco-orders/open'),
('tobacco_whman', 'tobacco-orders/open');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/pay'),
('admin', 'tobacco-orders/pay'),
('manager', 'tobacco-orders/pay'),
('tobacco_whman', 'tobacco-orders/pay');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/expedition'),
('admin', 'tobacco-orders/expedition'),
('manager', 'tobacco-orders/expedition'),
('tobacco_whman', 'tobacco-orders/expedition');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/select-products'),
('admin', 'tobacco-orders/select-products'),
('manager', 'tobacco-orders/select-products'),
('tobacco_whman', 'tobacco-orders/select-products');

--
-- 2014-11-13 #410 - Vyskladnění prodaných produktů – objednávky
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-order-edit/process'),
('admin', 'tobacco-order-edit/process'),
('manager', 'tobacco-order-edit/process'),
('tobacco_whman', 'tobacco-order-edit/process');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/detail'),
('admin', 'tobacco-orders/detail'),
('manager', 'tobacco-orders/detail'),
('tobacco_whman', 'tobacco-orders/detail');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-order-edit/customer'),
('admin', 'tobacco-order-edit/customer'),
('manager', 'tobacco-order-edit/customer'),
('tobacco_whman', 'tobacco-order-edit/customer');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-order-edit/payment'),
('admin', 'tobacco-order-edit/payment'),
('manager', 'tobacco-order-edit/payment'),
('tobacco_whman', 'tobacco-order-edit/payment');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-order-edit/transport'),
('admin', 'tobacco-order-edit/transport'),
('manager', 'tobacco-order-edit/transport'),
('tobacco_whman', 'tobacco-order-edit/transport');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-order-edit/assign-customer'),
('admin', 'tobacco-order-edit/assign-customer'),
('manager', 'tobacco-order-edit/assign-customer'),
('tobacco_whman', 'tobacco-order-edit/assign-customer');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-order-edit/remove-customer'),
('admin', 'tobacco-order-edit/remove-customer'),
('manager', 'tobacco-order-edit/remove-customer'),
('tobacco_whman', 'tobacco-order-edit/remove-customer');

--
-- 2014-11-12 #410 - Vyskladnění prodaných produktů – objednávky
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-order-edit/index'),
('admin', 'tobacco-order-edit/index'),
('manager', 'tobacco-order-edit/index'),
('tobacco_whman', 'tobacco-order-edit/index');


--
-- 2014-11-12 #410 - Vyskladnění prodaných produktů – objednávky
--
ALTER TABLE `customers`
	ADD COLUMN `id_wh_type` INT(11) UNSIGNED NOT NULL DEFAULT '1'
  COMMENT 'id casti wh (pipes/tobacco)' AFTER `customer_no`;

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-customers/detail'),
('admin', 'tobacco-customers/detail'),
('manager', 'tobacco-customers/detail'),
('tobacco_whman', 'tobacco-customers/detail');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-customers/edit'),
('admin', 'tobacco-customers/edit'),
('manager', 'tobacco-customers/edit'),
('tobacco_whman', 'tobacco-customers/edit');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-customers/delete'),
('admin', 'tobacco-customers/delete'),
('manager', 'tobacco-customers/delete'),
('tobacco_whman', 'tobacco-customers/delete');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-customers/address'),
('admin', 'tobacco-customers/address'),
('manager', 'tobacco-customers/address'),
('tobacco_whman', 'tobacco-customers/address');


--
-- 2014-11-12 #410 - Vyskladnění prodaných produktů – objednávky
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/index'),
('admin', 'tobacco-orders/index'),
('manager', 'tobacco-orders/index'),
('tobacco_whman', 'tobacco-orders/index');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-orders/new'),
('admin', 'tobacco-orders/new'),
('manager', 'tobacco-orders/new'),
('tobacco_whman', 'tobacco-orders/new');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-customers/index'),
('admin', 'tobacco-customers/index'),
('manager', 'tobacco-customers/index'),
('tobacco_whman', 'tobacco-customers/index');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco-customers/new'),
('admin', 'tobacco-customers/new'),
('manager', 'tobacco-customers/new'),
('tobacco_whman', 'tobacco-customers/new');

--
-- 2014-10-27 HOTFIX
--
INSERT INTO users_acl (role, resource) VALUES
('manager', 'tobacco/parts-stamp'),
('tobacco_whman', 'tobacco/parts-stamp');

--
-- 2014-10-27 #422 - Struktura tabákových mixů - implementace
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/parts-pack-check-ajax'),
('admin', 'tobacco/parts-pack-check-ajax'),
('manager', 'tobacco/parts-pack-check-ajax'),
('tobacco_whman', 'tobacco/parts-pack-check-ajax');


--
-- 2014-10-24 #422 - Struktura tabákových mixů - implementace
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/parts-pack'),
('admin', 'tobacco/parts-pack'),
('manager', 'tobacco/parts-pack'),
('tobacco_whman', 'tobacco/parts-pack');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/parts-pack-process'),
('admin', 'tobacco/parts-pack-process'),
('manager', 'tobacco/parts-pack-process'),
('tobacco_whman', 'tobacco/parts-pack-process');

--
-- 2014-10-24 #422 - Struktura tabákových mixů - implementace
--
ALTER TABLE `parts_recipes`
	ALTER `id_recipes` DROP DEFAULT;
ALTER TABLE `parts_recipes`
	RENAME TO `parts_mixes`,
	COMMENT='propojeni mixu na neprodejny tabak',
	CHANGE COLUMN `id_recipes` `id_mixes` INT(11) UNSIGNED NOT NULL AFTER `id_parts`;


--
-- 2014-10-24 #422 - Struktura tabákových mixů - implementace
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/ajax-mixes-recipes'),
('admin', 'tobacco/ajax-mixes-recipes'),
('manager', 'tobacco/ajax-mixes-recipes'),
('tobacco_whman', 'tobacco/ajax-mixes-recipes');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/ajax-parts-mix-save'),
('admin', 'tobacco/ajax-parts-mix-save'),
('manager', 'tobacco/ajax-parts-mix-save'),
('tobacco_whman', 'tobacco/ajax-parts-mix-save');

--
-- 2014-10-22 #422 - Struktura tabákových mixů - implementace
--
INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/mixes'),
('admin', 'tobacco/mixes'),
('manager', 'tobacco/mixes'),
('tobacco_whman', 'tobacco/mixes');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/mixes-add'),
('admin', 'tobacco/mixes-add'),
('manager', 'tobacco/mixes-add'),
('tobacco_whman', 'tobacco/mixes-add');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/mixes-detail'),
('admin', 'tobacco/mixes-detail'),
('manager', 'tobacco/mixes-detail'),
('tobacco_whman', 'tobacco/mixes-detail');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/mixes-edit'),
('admin', 'tobacco/mixes-edit'),
('manager', 'tobacco/mixes-edit'),
('tobacco_whman', 'tobacco/mixes-edit');

INSERT INTO users_acl (role, resource) VALUES
(null, 'tobacco/mixes-delete'),
('admin', 'tobacco/mixes-delete'),
('manager', 'tobacco/mixes-delete'),
('tobacco_whman', 'tobacco/mixes-delete');


--
-- 2014-10-21 #422 - Struktura tabákových mixů - implementace
--

CREATE TABLE `mixes` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_flavor` INT(11) UNSIGNED NOT NULL COMMENT 'prichut mixu',
	`name` VARCHAR(50) NOT NULL COMMENT 'oznaceni mixu' COLLATE 'utf8_czech_ci',
	`description` VARCHAR(255) NULL DEFAULT NULL COMMENT 'popis nebo poznamka k mixu' COLLATE 'utf8_czech_ci',
	`create` DATETIME NOT NULL COMMENT 'datum a cas vytvoreni',
	`update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'cas posledni zmeny',
	`state` ENUM('active','inactive','hidden') NOT NULL DEFAULT 'active' COMMENT 'stav mixu' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`id`),
	INDEX `id_flavor` (`id_flavor`)
)
COMMENT='tabakove mixy z maceratu'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

-- naplneni
INSERT INTO mixes (id, `name`, description, `create`, `state`, id_flavor)
SELECT id, `name`, description, NOW(), `state`, id_flavor FROM recipes;


CREATE TABLE `mixes_recipes` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_mixes` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na prislusny mix',
	`id_recipes` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na recept',
	`ratio` TINYINT(3) UNSIGNED NOT NULL COMMENT 'pomer slozky',
	PRIMARY KEY (`id`),
	INDEX `FK_mixes_recipes_mixes` (`id_mixes`),
	INDEX `FK_mixes_recipes_recipes` (`id_recipes`)
)
COMMENT='pomery tabaku v mixu'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

-- naplneni
INSERT INTO mixes_recipes (id_mixes, id_recipes, ratio)
SELECT id, id, 1 FROM mixes;


ALTER TABLE `recipes_flavors`
	ALTER `name` DROP DEFAULT;
ALTER TABLE `recipes_flavors`
	RENAME TO `batch_flavors`,
	CHANGE COLUMN `name` `name` VARCHAR(50) NOT NULL COMMENT 'nazev prichuti' AFTER `id`,
	ADD COLUMN `is_mix` TINYINT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'jedna se o mix prichuti' AFTER `name`;
CREATE TABLE `batch_mixes` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'primarni klic',
	`id_mixes` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na tabakovy mix',
	`id_macerations` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na na macerat',
	`batch` VARCHAR(10) NULL DEFAULT NULL COMMENT 'cislo sarze mixu',
	PRIMARY KEY (`id`),
	INDEX `id_mixes` (`id_mixes`),
	INDEX `id_macerations` (`id_macerations`)
)
COMMENT='cisla sarzi pro tabakove mixy'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

-- naplneni
INSERT INTO batch_mixes (id_mixes, id_macerations, batch)
SELECT macerations.id_recipes, macerations.id, macerations.batch FROM parts_batch
LEFT JOIN macerations ON parts_batch.batch = macerations.batch;



--
-- 2014-10-13 #403 - Detail a editace macerace
--
INSERT INTO users_acl (role, resource) VALUES
(NULL, 'tobacco/export'),
('admin', 'tobacco/export'),
('manager', 'tobacco/export'),
('tobacco_whman', 'tobacco/export');


--
-- 2014-10-13 #405 - Kategorizace surovin
--
DELETE FROM parts_ctg WHERE id >= 34;
INSERT INTO parts_ctg
(`id`, `parent_ctg`, `ctg_order`, `name`, `description`) VALUES
(34, NULL, 5000, 'TABÁKY', NULL),
(35, NULL, 5100, 'TABÁKY - SUROVINY', NULL),
(36, 34, 5040, 'Obaly', NULL),
(37, 34, 5050, 'Tabák neprodejný', NULL),
(38, 34, 5060, 'Kolky', NULL),
(39, 34, 5070, 'Tabák prodejný', NULL),
(40, 35, 10, 'Surový tabák', NULL),
(41, 35, 20, 'Příchutě', NULL),
(42, 35, 30, 'Ostatní suroviny', NULL);
UPDATE parts SET id_parts_ctg = 42 WHERE id_parts_ctg = 35;

--
-- 2014-10-13 #403 - Detail a editace macerace
--
INSERT INTO users_acl (role, resource) VALUES
(NULL, 'tobacco/recipe-edit'),
('admin', 'tobacco/recipe-edit'),
('manager', 'tobacco/recipe-edit'),
('tobacco_whman', 'tobacco/recipe-edit');

--
-- 2014-10-04 #403 - Detail a editace macerace
--
ALTER TABLE `macerations`
	ADD COLUMN `name` VARCHAR(50) NULL COMMENT 'nazev' AFTER `batch`;

--
-- 2014-10-04 #409 - Automatické generování čísla šarže
--
CREATE TABLE `recipes_flavors` (
	`id` INT UNSIGNED NOT NULL,
	`name` VARCHAR(50) NOT NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `recipes`
	ADD COLUMN `id_flavor` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '1' AFTER `name`;

INSERT INTO recipes_flavors (`id`, `name`) VALUES
(1, 'STRAWBERRY'),
(2, 'MINT'),
(3, 'BANANA'),
(4, 'CHERRY'),
(5, 'GREEN APPLE'),
(6, 'DOUBLE APPLE'),
(7, 'GRAPE'),
(8, 'PASSION FRUIT'),
(9, 'CANTALOUPE'),
(10, 'MANGO'),
(11, 'LIME'),
(12, 'COCONUT'),
(13, 'ORANGE'),
(14, 'PEACH'),
(15, 'LEMON'),
(16, 'MANGOBOY'),
(17, 'CITRUS GANG'),
(18, 'DESSERT ROSE'),
(19, 'EDENS APPLES'),
(20, 'KARKADE MIST'),
(21, 'LILY BREEZE'),
(22, 'MILKY WAY'),
(23, 'SWEET CRUISE'),
(24, 'VANILLA SKY'),
(25, 'WINTER SPICES'),
(26, 'MEDUSE BEACH'),
(27, 'KIWI GOLF');

--
-- 2014-09-18 #391 - Reporty - přehled nabalených produktů
--
ALTER TABLE `parts_recipes`
	ALTER `id_parts` DROP DEFAULT;
ALTER TABLE `parts_recipes`
	CHANGE COLUMN `id_parts` `id_parts` VARCHAR(10) NOT NULL COLLATE 'utf8_czech_ci' AFTER `id`;

--
-- 2014-09-30 #412 - Tlačítko pro odstranění součásti
--
INSERT INTO users_acl (role, resource) VALUES
(NULL, 'tobacco/parts-delete'),
('admin', 'tobacco/parts-delete'),
('manager', 'tobacco/parts-delete');

--
-- 2014-09-30 HOTFIX - opravneni
--
INSERT INTO users_acl (role, resource) VALUES
('manager', 'tobacco/delete-subpart'),
('tobacco_whman', 'tobacco/delete-subpart');

--
-- 2014-09-29 HOTFIX - opravneni
--
INSERT INTO users_acl (role, resource) VALUES
('manager', 'tobacco/parts-select-subparts'),
('tobacco_whman', 'tobacco/parts-select-subparts'),
(null, 'tobacco/ajax-subparts-count-change'),
('admin', 'tobacco/ajax-subparts-count-change'),
('manager', 'tobacco/ajax-subparts-count-change'),
('tobacco_whman', 'tobacco/ajax-subparts-count-change');

INSERT INTO users_acl (role, resource) VALUES
('manager', 'tobacco/ajax-parts-recipe-save'),
('tobacco_whman', 'tobacco/ajax-parts-recipe-save');

--
-- 2014-08-21 #368 - Prace na skladu tabaku
--
INSERT INTO users_acl (role, resource)
SELECT 'tobacco_whman' AS role, resource FROM users_acl
WHERE role = 'admin' AND resource LIKE 'tobacco%';

INSERT INTO users_acl (role, resource)
SELECT 'manager' AS role, resource FROM users_acl
WHERE role = 'admin' AND resource LIKE 'tobacco%';

INSERT INTO users_acl (role, resource) VALUES ('manager', 'index/switch-warehouse');

--
-- 2014-08-20 #368 - Prace na skladu tabaku
--
CREATE TABLE `parts_batch` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_parts` VARCHAR(10) NOT NULL COMMENT 'cizi klic na soucast' COLLATE 'utf8_czech_ci',
	`batch` VARCHAR(10) NOT NULL COMMENT 'cislo sarze' COLLATE 'utf8_czech_ci',
	`amount` INT(6) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'pocet kusu',
	PRIMARY KEY (`id`),
	INDEX `ip_parts` (`id_parts`),
	INDEX `batch` (`batch`)
)
COMMENT='cisla sarzi k tabakovym produktum'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;



--
-- 2014-08-19 #368 - Prace na skladu tabaku
--
ALTER TABLE `macerations`
	ALTER `amount_in` DROP DEFAULT,
	ALTER `amount_current` DROP DEFAULT;
ALTER TABLE `macerations`
	CHANGE COLUMN `amount_in` `amount_in` DECIMAL(9,3) UNSIGNED NOT NULL COMMENT 'vaha smesi na vstupu v gramech' AFTER `batch`,
	CHANGE COLUMN `amount_current` `amount_current` DECIMAL(9,3) UNSIGNED NOT NULL COMMENT 'soucasna vaha smesi v gramech' AFTER `amount_in`;

ALTER TABLE `macerations_history`
	CHANGE COLUMN `amount` `amount` DECIMAL(9,3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'velikost zmeny' AFTER `id_users`;

ALTER TABLE `parts_recipes`
	CHANGE COLUMN `amount` `amount` DECIMAL(9,3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'vaha maceratu v baleni tabaku' AFTER `id_recipes`;

ALTER TABLE `macerations_parts`
	CHANGE COLUMN `amount` `amount` DECIMAL(9,3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'mnozstvi pouzite slozky v gramech' AFTER `id_parts`;


--
-- 2014-08-12 #368 - Prace na skladu tabaku
--
CREATE TABLE `macerations_history` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_macerations` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na maceraty',
	`id_users` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na uzivatele',
	`amount` DECIMAL(6,0) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'velikost zmeny',
	`description` VARCHAR(255) NOT NULL COMMENT 'popis zmeny',
	`last_change` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'datum zmeny',
	PRIMARY KEY (`id`)
)
COMMENT='historie maceratu'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `parts_recipes`
	ALTER `id_parts` DROP DEFAULT;
ALTER TABLE `parts_recipes`
	CHANGE COLUMN `id_parts` `id_parts` VARCHAR(10) NOT NULL COLLATE 'utf8_czech_ci' AFTER `id`;


--
-- 2014-07-30 #375 - Recipes
--
ALTER TABLE `recipes`
	CHANGE COLUMN `due` `due` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '20' COMMENT 'doba macerace ve dnech' AFTER `description`;


--
-- 2014-07-30 #375 - Recipes
--
ALTER TABLE `recipes`
	ADD COLUMN `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'kdy vytvoreno' AFTER `due`,
	ADD COLUMN `last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'kdy naposledy upraveno' AFTER `created_at`,
	ADD COLUMN `state` ENUM('active','inactive','hidden') NULL DEFAULT 'active' AFTER `last_update`;

--
-- 2014-07-22 #368 - Prace na skladu tabaku
--
CREATE TABLE `parts_recipes` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_parts` VARCHAR(10) NOT NULL,
	`id_recipes` INT(11) UNSIGNED NOT NULL,
	`amount` DECIMAL(6,0) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'vaha maceratu v baleni tabaku',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `id_parts` (`id_parts`)
)
COMMENT='propojeni receptu na neprodejny tabak'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;


--
-- 2014-07-08 #368 - Prace na skladu tabaku
--
ALTER TABLE `macerations_parts`
	ALTER `id_parts` DROP DEFAULT;
ALTER TABLE `macerations_parts`
	CHANGE COLUMN `id_parts` `id_parts` VARCHAR(10) NOT NULL COMMENT 'cizi klic na slozku maceratu' AFTER `id_macerations`;
ALTER TABLE `macerations_parts`
	CHANGE COLUMN `amount` `amount` DECIMAL(6,0) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'mnozstvi pouzite slozky v gramech' AFTER `id_parts`;



--
-- 2014-07-03 #368 - Prace na skladu tabaku
--
ALTER TABLE `macerations`
	ALTER `amount_in` DROP DEFAULT,
	ALTER `amount_current` DROP DEFAULT;
ALTER TABLE `macerations`
	CHANGE COLUMN `amount_in` `amount_in` DECIMAL(6,0) UNSIGNED NOT NULL COMMENT 'vaha smesi na vstupu v gramech' AFTER `batch`,
	CHANGE COLUMN `amount_current` `amount_current` DECIMAL(6,0) UNSIGNED NOT NULL COMMENT 'soucasna vaha smesi v gramech' AFTER `amount_in`;
ALTER TABLE `macerations`
	ADD COLUMN `description` VARCHAR(50) NULL DEFAULT NULL COMMENT 'popis macerace' AFTER `id_recipes`;
ALTER TABLE `recipes_parts`
	ALTER `id_parts` DROP DEFAULT;
ALTER TABLE `recipes_parts`
	CHANGE COLUMN `id_parts` `id_parts` VARCHAR(10) NOT NULL COMMENT 'cizi klic slozky' AFTER `id_recipes`;
ALTER TABLE `recipes_parts`
	CHANGE COLUMN `ratio` `ratio` DECIMAL(3,0) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'pomer slozky v procentech' AFTER `id_parts`;


--
-- 2014-06-16 #368 - Prace na skladu tabaku
--
INSERT INTO `producers_ctg` (`name`) VALUES ('TABÁKOVÉ SUROVINY');

--
-- 2014-06-11 #368 - Prace na skladu tabaku
--
INSERT INTO `parts_ctg` (`id`, `parent_ctg`, `ctg_order`, `name`)
VALUES
(34, 0, 5000, 'TABÁKY'),
(35, 34, 1,   'Suroviny'),
(36, 34, 2,   'Obaly'),
(37, 34, 3,   'Tabák neprodejný'),
(38, 34, 4,   'Kolky'),
(39, 34, 5,   'Tabák prodejný');

--
-- 2014-05-28 #368 - Prace na skladu tabaku
--
CREATE TABLE `recipes` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL COMMENT 'nazev receptu' COLLATE 'utf8_czech_ci',
	`description` TEXT NULL COMMENT 'popis receptu' COLLATE 'utf8_czech_ci',
	`due` SMALLINT UNSIGNED NOT NULL COMMENT 'doba macerace ve dnech',
	PRIMARY KEY (`id`)
)
COMMENT='recepty na michani tabaku'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE `recipes_parts` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_recipes` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic receptu',
	`id_parts` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic slozky',
	`ratio` DECIMAL(3,2) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'pomer slozky v procentech',
	PRIMARY KEY (`id`)
)
COMMENT='jdenotlive polozky receptu v procentualnim vyjadreni'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE `macerations` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_recipes` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'cizi klic na recept, podle ktereho byla smes pripravena',
	`batch` VARCHAR(10) NOT NULL COMMENT 'kod sarze LRRMMDDSPP' COLLATE 'utf8_czech_ci',
	`amount_in` DECIMAL(6,2) UNSIGNED NOT NULL COMMENT 'vaha smesi na vstupu v gramech',
	`amount_current` DECIMAL(6,2) UNSIGNED NOT NULL COMMENT 'soucasna vaha smesi v gramech',
	`start` DATE NOT NULL COMMENT 'datum naskladneni',
	`end` DATE NULL DEFAULT NULL COMMENT 'datum vyskladneni',
	`checked` DATE NULL DEFAULT NULL COMMENT 'datum posledni kontroly',
	PRIMARY KEY (`id`)
)
COMMENT='sklad macerovanych smesi tabaku'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE `macerations_parts` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_macerations` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na macerat',
	`id_parts` INT(11) UNSIGNED NOT NULL COMMENT 'cizi klic na slozku maceratu',
	`amount` DECIMAL(6,2) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'mnozstvi pouzite slozky v gramech',
	PRIMARY KEY (`id`)
)
COMMENT='jednotlive slozky smesi macerovaneho tabaku'
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;
