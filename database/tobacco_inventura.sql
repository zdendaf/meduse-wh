-- odkolkovani okolkovanych produktu - pres WH

-- oprava chybne zadefinovanych FUSION mixu
UPDATE batch_mixes SET batch = CONCAT(SUBSTR(batch, 1, 8), '93') WHERE id_mixes = 35;
UPDATE batch_mixes SET batch = CONCAT(SUBSTR(batch, 1, 8), '94') WHERE id_mixes = 36;
UPDATE parts_batch SET batch = CONCAT(SUBSTR(batch, 1, 8), '93') WHERE id_parts LIKE '%010024';
UPDATE parts_batch SET batch = CONCAT(SUBSTR(batch, 1, 8), '93') WHERE id_parts LIKE '%250019';
UPDATE parts_batch SET batch = CONCAT(SUBSTR(batch, 1, 8), '94') WHERE id_parts LIKE '%010025';

-- snizeni mnoztvi sudu s maceraty na celkovou soucasne nulove mnoztvi
DELETE FROM macerations WHERE ID = 55; -- odtraneni naposledy pridaneho sudu
UPDATE macerations SET amount_in = amount_current WHERE amount_in < amount_current;
UPDATE macerations SET description = CONCAT (description, ' - chybna macerace, na likvidaci') WHERE id > 21 AND id < 32;
UPDATE macerations SET amount_in = amount_in - amount_current, amount_current = 0, description = CONCAT (description, ' - inventura 30.4.2015') WHERE id < 22 OR id > 31;

-- vlozeni realnych sudu s maceraty
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (10, 'L131025A03', 'Banan', '19.38', '19.38', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (10, 'L131025B03', 'Banan', '34.64', '34.64', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (5, 'L131024A09', 'Cantaloupe', '11.64', '11.64', '2013-10-24');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (5, 'L131024B09', 'Cantaloupe', '7.02', '7.02', '2013-10-24');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (16, 'L131025A12', 'Coconut', '6.64', '6.64', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (16, 'L131025B12', 'Coconut', '26.68', '26.68', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (9, 'L131025A06', 'Double apple', '19.24', '19.24', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (9, 'L131025B06', 'Double apple', '13.34', '13.34', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (9, 'L140919C06', 'Double apple', '10.6', '10.6', '2014-09-19');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (2, 'L140904A07', 'Grape', '38.26', '38.26', '2014-09-04');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (2, 'L131024B07', 'Grape', '9.46', '9.46', '2013-10-24');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (15, 'L131025A05', 'Green Apple', '22.26', '22.26', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (15, 'L131025B05', 'Green Apple', '22.08', '22.08', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (8, 'L131025A04', 'Cherry', '16.26', '16.26', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (17, 'L150108A28', 'Chockolate', '2.68', '2.68', '2015-01-08');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (12, 'L131025A15', 'Lemon', '16.46', '16.46', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (12, 'L131025B15', 'Lemon', '29.02', '29.02', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (14, 'L141105A11', 'Lime', '18.16', '18.16', '2014-11-05');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (6, 'L140124A10', 'Mango', '3', '3', '2014-01-24');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (6, 'L140919B10', 'Mango', '10.62', '10.62', '2014-09-19');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (6, 'L131024C10', 'Mango', '15.2', '15.2', '2013-10-24');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (3, 'L140904A02', 'Mint', '36.94', '36.94', '2014-09-04');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (3, 'L131025B02', 'Mint', '26.16', '26.16', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (13, 'L131025A13', 'Orange', '8.98', '8.98', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (13, 'L131025B13', 'Orange', '24.92', '24.92', '2013-10-25');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (7, 'L131024A08', 'Passion', '36.16', '36.16', '2013-10-24');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (4, 'L131024A14', 'Peach', '11.92', '11.92', '2013-10-24');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (4, 'L131024B14', 'Peach', '31.64', '31.64', '2013-10-24');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (29, 'L150116A30', 'Pear', '4.4', '4.4', '2015-01-16');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (11, 'L131024A01', 'Strawberry', '7.48', '7.48', '2013-10-24');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (11, 'L131024B01', 'Strawberry', '28.22', '28.22', '2013-10-24');
INSERT INTO macerations (id_recipes, batch, name, amount_in, amount_current, start) VALUES (18, 'L140904A29', 'Water Mellone', '5.4', '5.4', '2014-09-04');

INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 56, id_parts, amount FROM macerations_parts WHERE id_macerations = 37;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 57, id_parts, amount FROM macerations_parts WHERE id_macerations = 37;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 58, id_parts, amount FROM macerations_parts WHERE id_macerations = 44;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 59, id_parts, amount FROM macerations_parts WHERE id_macerations = 44;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 60, id_parts, amount FROM macerations_parts WHERE id_macerations = 47;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 61, id_parts, amount FROM macerations_parts WHERE id_macerations = 47;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 62, id_parts, amount FROM macerations_parts WHERE id_macerations = 41;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 63, id_parts, amount FROM macerations_parts WHERE id_macerations = 41;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 64, id_parts, amount FROM macerations_parts WHERE id_macerations = 41;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 65, id_parts, amount FROM macerations_parts WHERE id_macerations = 42;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 66, id_parts, amount FROM macerations_parts WHERE id_macerations = 42;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 67, id_parts, amount FROM macerations_parts WHERE id_macerations = 39;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 68, id_parts, amount FROM macerations_parts WHERE id_macerations = 39;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 69, id_parts, amount FROM macerations_parts WHERE id_macerations = 38;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 70, id_parts, amount FROM macerations_parts WHERE id_macerations = 17;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 71, id_parts, amount FROM macerations_parts WHERE id_macerations = 50;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 72, id_parts, amount FROM macerations_parts WHERE id_macerations = 50;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 73, id_parts, amount FROM macerations_parts WHERE id_macerations = 46;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 74, id_parts, amount FROM macerations_parts WHERE id_macerations = 45;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 75, id_parts, amount FROM macerations_parts WHERE id_macerations = 45;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 76, id_parts, amount FROM macerations_parts WHERE id_macerations = 45;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 77, id_parts, amount FROM macerations_parts WHERE id_macerations = 36;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 78, id_parts, amount FROM macerations_parts WHERE id_macerations = 36;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 79, id_parts, amount FROM macerations_parts WHERE id_macerations = 48;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 80, id_parts, amount FROM macerations_parts WHERE id_macerations = 48;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 81, id_parts, amount FROM macerations_parts WHERE id_macerations = 43;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 82, id_parts, amount FROM macerations_parts WHERE id_macerations = 49;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 83, id_parts, amount FROM macerations_parts WHERE id_macerations = 49;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 84, id_parts, amount FROM macerations_parts WHERE id_macerations = 52;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 85, id_parts, amount FROM macerations_parts WHERE id_macerations = 34;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 86, id_parts, amount FROM macerations_parts WHERE id_macerations = 34;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 87, id_parts, amount FROM macerations_parts WHERE id_macerations = 21;

-- vlozeni sudu za rok 2014
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (3, 10, 'rok 2014', 'L131010A03', 'BANANA', 8.19, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (9, 5, 'rok 2014', 'L131010A09', 'CANTALOUPE', 32.09, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (12, 16, 'rok 2014', 'L131010A12', 'COCONUT', 17.52, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (6, 9, 'rok 2014', 'L131010A06', 'DOUBLE APPLE', 17.69, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (7, 2, 'rok 2014', 'L131010A07', 'GRAPE', 33.29, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (5, 15, 'rok 2014', 'L131010A05', 'GREEN APPLE', 10.75, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (4, 8, 'rok 2014', 'L131010A04', 'CHERRY', 30.49, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (15, 12, 'rok 2014', 'L131010A15', 'LEMON', 7.47, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (11, 14, 'rok 2014', 'L131010A11', 'LIME', 45.58, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (10, 6, 'rok 2014', 'L131010A10', 'MANGO', 29.88, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (2, 3, 'rok 2014', 'L131010A02', 'MINT', 23.51, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (13, 13, 'rok 2014', 'L131010A13', 'ORANGE', 15.75, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (8, 7, 'rok 2014', 'L131010A08', 'PASSION FRUIT', 16.48, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (14, 4, 'rok 2014', 'L131010A14', 'PEACH', 12.3, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (16, 29, 'rok 2014', 'L131010A30', 'PEAR', 2.97, 0, '2013-10-10');
INSERT INTO macerations (id, id_recipes, description, batch, name, amount_in, amount_current, start) VALUES (1, 1, 'rok 2014', 'L131010A01', 'STRAWBERRY', 15.8, 0, '2013-10-10');
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 3, id_parts, amount FROM macerations_parts WHERE id_macerations = 37;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 9, id_parts, amount FROM macerations_parts WHERE id_macerations = 44;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 12, id_parts, amount FROM macerations_parts WHERE id_macerations = 47;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 6, id_parts, amount FROM macerations_parts WHERE id_macerations = 41;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 7, id_parts, amount FROM macerations_parts WHERE id_macerations = 42;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 5, id_parts, amount FROM macerations_parts WHERE id_macerations = 39;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 4, id_parts, amount FROM macerations_parts WHERE id_macerations = 38;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 15, id_parts, amount FROM macerations_parts WHERE id_macerations = 50;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 11, id_parts, amount FROM macerations_parts WHERE id_macerations = 46;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 10, id_parts, amount FROM macerations_parts WHERE id_macerations = 45;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 2, id_parts, amount FROM macerations_parts WHERE id_macerations = 36;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 13, id_parts, amount FROM macerations_parts WHERE id_macerations = 48;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 8, id_parts, amount FROM macerations_parts WHERE id_macerations = 43;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 14, id_parts, amount FROM macerations_parts WHERE id_macerations = 49;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 16, id_parts, amount FROM macerations_parts WHERE id_macerations = 52;
INSERT INTO macerations_parts (id_macerations, id_parts, amount) SELECT 1, id_parts, amount FROM macerations_parts WHERE id_macerations = 34;

-- nastaveni adhoc sarze pro veskere mixy
UPDATE batch_mixes SET batch = CONCAT('L141218A', SUBSTR(batch, 9, 2));

-- cisla sarzi v provedenych objednavkach
UPDATE orders_products_batch SET batch = CONCAT('L141218A', SUBSTR(batch, 9, 2));

INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10110, 'L131010A03', 289);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10116, 'L131010A09', 727);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10119, 'L131010A12', 605);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10113, 'L131010A06', 529);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10125, 'L131010A97', 464);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10123, 'L131010A99', 296);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10126, 'L131010A96', 344);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10124, 'L131010A98', 424);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10127, 'L131010A95', 244);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10114, 'L131010A07', 612);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10112, 'L131010A05', 298);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10111, 'L131010A04', 632);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10122, 'L131010A15', 209);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10118, 'L131010A11', 1381);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10117, 'L131010A10', 723);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10109, 'L131010A02', 540);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10120, 'L131010A13', 835);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10115, 'L131010A08', 662);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10121, 'L131010A14', 505);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10128, 'L131010A30', 272);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10108, 'L131010A01', 555);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10131, 'L131010A03', 76);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10137, 'L131010A09', 370);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10134, 'L131010A06', 119);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10135, 'L131010A07', 380);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10133, 'L131010A05', 83);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10132, 'L131010A04', 375);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10139, 'L131010A11', 430);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10138, 'L131010A10', 375);
INSERT INTO orders_products_batch (id_orders_products, batch, amount) VALUES (10130, 'L131010A02', 142);

-- update cisla sarzi dle poctu na skladu
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 52 FROM parts_batch WHERE id_parts = 'TA010001' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 12 FROM parts_batch WHERE id_parts = 'TA010002' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 52 FROM parts_batch WHERE id_parts = 'TA010003' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 30 FROM parts_batch WHERE id_parts = 'TA010004' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 22 FROM parts_batch WHERE id_parts = 'TA010005' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 11 FROM parts_batch WHERE id_parts = 'TA010006' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 30 FROM parts_batch WHERE id_parts = 'TA010007' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 36 FROM parts_batch WHERE id_parts = 'TA010008' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 76 FROM parts_batch WHERE id_parts = 'TA010009' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 35 FROM parts_batch WHERE id_parts = 'TA010010' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 40 FROM parts_batch WHERE id_parts = 'TA010011' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 59 FROM parts_batch WHERE id_parts = 'TA010012' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 27 FROM parts_batch WHERE id_parts = 'TA010013' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 43 FROM parts_batch WHERE id_parts = 'TA010014' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 24 FROM parts_batch WHERE id_parts = 'TA010015' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 55 FROM parts_batch WHERE id_parts = 'TA010016' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 16 FROM parts_batch WHERE id_parts = 'TA010017' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 61 FROM parts_batch WHERE id_parts = 'TA010018' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 13 FROM parts_batch WHERE id_parts = 'TA010019' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 48 FROM parts_batch WHERE id_parts = 'TA010020' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 36 FROM parts_batch WHERE id_parts = 'TA010021' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 67 FROM parts_batch WHERE id_parts = 'TA010022' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 78 FROM parts_batch WHERE id_parts = 'TA010023' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 36 FROM parts_batch WHERE id_parts = 'TA010024' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 46 FROM parts_batch WHERE id_parts = 'TA010025' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 3 FROM parts_batch WHERE id_parts = 'TA050001' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 1 FROM parts_batch WHERE id_parts = 'TA050003' GROUP BY id_parts;
INSERT INTO parts_batch (id_parts, batch, amount) SELECT id_parts, CONCAT('L141218A', SUBSTR(batch, 9, 2)), 1 FROM parts_batch WHERE id_parts = 'TA050005' GROUP BY id_parts;
DELETE FROM parts_batch WHERE batch NOT LIKE 'L141218A%';

-- uprava poctu produktu na skladu
UPDATE parts_warehouse SET amount = 0 WHERE id_parts LIKE 'TA%' OR id_parts LIKE 'TK%';
UPDATE parts_warehouse SET amount = 52 WHERE id_parts = 'TA010001';
UPDATE parts_warehouse SET amount = 12 WHERE id_parts = 'TA010002';
UPDATE parts_warehouse SET amount = 52 WHERE id_parts = 'TA010003';
UPDATE parts_warehouse SET amount = 30 WHERE id_parts = 'TA010004';
UPDATE parts_warehouse SET amount = 22 WHERE id_parts = 'TA010005';
UPDATE parts_warehouse SET amount = 11 WHERE id_parts = 'TA010006';
UPDATE parts_warehouse SET amount = 30 WHERE id_parts = 'TA010007';
UPDATE parts_warehouse SET amount = 36 WHERE id_parts = 'TA010008';
UPDATE parts_warehouse SET amount = 76 WHERE id_parts = 'TA010009';
UPDATE parts_warehouse SET amount = 35 WHERE id_parts = 'TA010010';
UPDATE parts_warehouse SET amount = 40 WHERE id_parts = 'TA010011';
UPDATE parts_warehouse SET amount = 59 WHERE id_parts = 'TA010012';
UPDATE parts_warehouse SET amount = 27 WHERE id_parts = 'TA010013';
UPDATE parts_warehouse SET amount = 43 WHERE id_parts = 'TA010014';
UPDATE parts_warehouse SET amount = 24 WHERE id_parts = 'TA010015';
UPDATE parts_warehouse SET amount = 55 WHERE id_parts = 'TA010016';
UPDATE parts_warehouse SET amount = 16 WHERE id_parts = 'TA010017';
UPDATE parts_warehouse SET amount = 61 WHERE id_parts = 'TA010018';
UPDATE parts_warehouse SET amount = 13 WHERE id_parts = 'TA010019';
UPDATE parts_warehouse SET amount = 48 WHERE id_parts = 'TA010020';
UPDATE parts_warehouse SET amount = 36 WHERE id_parts = 'TA010021';
UPDATE parts_warehouse SET amount = 67 WHERE id_parts = 'TA010022';
UPDATE parts_warehouse SET amount = 78 WHERE id_parts = 'TA010023';
UPDATE parts_warehouse SET amount = 36 WHERE id_parts = 'TA010024';
UPDATE parts_warehouse SET amount = 46 WHERE id_parts = 'TA010025';
UPDATE parts_warehouse SET amount = 3 WHERE id_parts = 'TA050001';
UPDATE parts_warehouse SET amount = 1 WHERE id_parts = 'TA050003';
UPDATE parts_warehouse SET amount = 1 WHERE id_parts = 'TA050005';

-- opravy integrity databaze
INSERT INTO `parts_batch` (`id_parts`, `amount`, `batch`) VALUES
('TA010006', 11, 'L141218A06'),
('TA010009', 76, 'L141218A09'),
('TA010016', 55, 'L141218A16'),
('TA010021', 36, 'L141218A21'),
('TA010022', 67, 'L141218A22'),
('TA010024', 36, 'L141218A24');

UPDATE macerations SET batch = 'L141105B11' WHERE id = 51;
