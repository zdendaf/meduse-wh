--
-- #272: MDT: uprava exportu, cena maceratu
--
insert into users_acl (role, resource)
select role, 'tobacco/parts-recalculate' as resource
from users_acl
where resource like 'tobacco/parts-edit';

--
-- #273: MDS: Sklad vyřazených součástí
--
insert into extern_warehouses (id, name, description)
values ('1', 'vyřazené součásti', 'virtuální sklad vyřazených součástí');
insert into users_acl (role, resource, description)
values (NULL, 'parts/batch-throwing', 'hromadné vyřazení produktů'),
       ('admin', 'parts/batch-throwing', NULL),
       ('manager', 'parts/batch-throwing', NULL),
       ('head_warehouseman', 'parts/batch-throwing', NULL);

--
-- #277: Docházka - zkracený úvazek
--
alter table employees
    add part_time enum ('y', 'n') default 'n' null comment 'zkraceny uvazek?' after obligation;

--
-- #279: Medite: Role Vedoucí skladu tabáků
--
insert into users_roles (is_active, role_name, role_translation, role_description)
values (1, 'tobacco_head', 'Vedoucí skladu tabáku', '');
update users_acl
set role = 'tobacco_head'
where role = 'tobacco_whman';
insert into users_acl (role, resource, description)
values ('tobacco_whman', null, 'definice role'),
       ('tobacco_whman', 'tobacco/warehouse-daily-packing', null);
--
-- #286: PDF verze faktur pro obchodníka
--
insert into users_acl (role, resource)
values ('seller', 'invoice/show-versions'), ('tobacco_seller', 'invoice/show-versions');

--
-- #287: Dodatečný řádek v adrese odběratele
--
alter table invoice
    add add_address1_head varchar(15) default null null comment 'zahlavi dodatecneho radku adresy' after text1;
alter table invoice
    add add_address1_data varchar(50) default null null comment 'obsah dodatecneho radku adresy' after add_address1_head;
alter table invoice
    add add_address2_data varchar(50) default null null comment 'obsah druheho dodatecneho radku adresy' after add_address1_data;

--
-- #284: Zpeněžení přesčasů
--
alter table employees_rewards
    add overtime_bank decimal(5,2) default 0.0 null comment 'hodiny k proplaceni na ucet' after overtime_cash;
alter table employees_rewards
    add overtime_bank_bonus decimal(7,2) default 0.0 null comment 'priplatek za prescas na ucet' after overtime_bonus;

--
-- #298: Rozdělení skladu obalů
--
insert into parts_ctg (id, parent_ctg, ctg_order, name, description)
values (49, 36, 1, 'etikety', 'etikety a nálepky'),
       (50, 36, 2, 'sáčky', 'sáčky'),
       (51, 36, 3, 'krabičky', 'krabičky'),
       (52, 36, 4, 'ostatní', 'ostatní obalový materiál');
update parts set id_parts_ctg = 49 where id_parts_ctg = 36 and id like '%E' or id like '%ET';
update parts set id_parts_ctg = 50 where id_parts_ctg = 36 and id like '%SA';
update parts set id_parts_ctg = 51 where id_parts_ctg = 36 and id like '%KR';
update parts set id_parts_ctg = 52 where id_parts_ctg = 36
                                     and id not like '%SA'
                                     and id not like '%KR'
                                     and id not like '%E' and id not like '%ET';

insert into parts_ctg (id, parent_ctg, ctg_order, name, description)
values (53, 36, 2, 'etikety na 10g krabičky', null),
       (54, 36, 3, 'etikety na 50g krabičky', null),
       (55, 36, 4, 'etikety na 10g sáčky', null);
update parts set id_parts_ctg = 53 where id_parts_ctg = 49 and id like 'TO010K%';
update parts set id_parts_ctg = 54 where id_parts_ctg = 49 and id like 'TO050K%';
update parts set id_parts_ctg = 55 where id_parts_ctg = 49 and id like 'TO010S%';
update parts_ctg set ctg_order = 5 where id = 50;
update parts_ctg set ctg_order = 6 where id = 51;
update parts_ctg set ctg_order = 7 where id = 52;

--
-- #276: Více trakovacích čísel k objednávce
--
create table orders_tracking_codes
(
    id int(11) unsigned auto_increment,
    id_orders int(11) unsigned not null,
    code varchar(255) not null,
    link varchar(255) null,
    constraint orders_tracking_codes_pk
        primary key (id),
    constraint orders_tracking_codes_orders_id_fk
        foreign key (id_orders) references orders (id)
            on update cascade on delete cascade
)
    comment 'trackovaci kody k objednavkam';

alter table orders modify tracking_no varchar(100) null comment 'nepoužívat!';

insert into orders_tracking_codes (id_orders, `code`, link)
select id as id_orders, tracking_no as code, tracking_link as link from orders where tracking_no != '';


--
-- #290: Editace HS kódů
--
alter table orders_products
    add hs_code varchar(50) default null null comment 'HS kódy v objednavce' after id_products;
update orders_products op
    join parts p on p.id = op.id_products
set op.hs_code = p.hs
where p.hs <> '' and p.hs is not null;
update users_acl set resource = 'orders/ajax-hs-save'
where resource like 'parts/ajax-hs-save';

insert into users_acl (role, resource, description) values
(null, 'orders/available-hs', 'nastaveni uzivatelskych hs kodu'),
('admin', 'orders/available-hs', null),
('manager', 'orders/available-hs', null),
('head_warehouseman', 'orders/available-hs', null);
alter table settings modify value varchar(1024) not null comment 'hodnota promenne';

--
-- #294: Úprava editace vložených výrob - personifikace
--
alter table productions
    add id_users int(11) unsigned default null null after id_producers,
    add constraint productions_users_id_fk
        foreign key (id_users) references users (id);

--
-- #292: Fakturace - redukce
--
alter table invoice
    add reduction tinyint unsigned default 0 not null after additional_discount_include;

--
-- #294: Úprava editace vložených výrob - personifikace
--
alter table productions_deliveries
    add id_users int(11) unsigned default null null after id_productions,
    add constraint productions_deliveries_users_id_fk
        foreign key (id_users) references users (id);

--
-- #296: MDS: DPH v rezimu OSS
--
alter table application_dph
    add code_c_country varchar(2) default 'CZ' not null comment 'cizi klic na ciselnik zemi' COLLATE 'utf8_czech_ci' after id,
    add constraint application_dph_c_country_code_fk
        foreign key (code_c_country) references c_country (code)
            on update cascade on delete cascade;

insert into application_dph (code_c_country, value, active_from_date) values
('AT', 20, '2021-01-01'),
('BE', 21, '2021-01-01'),
('BG', 20, '2021-01-01'),
('CY', 19, '2021-01-01'),
('DE', 19, '2021-01-01'),
('DK', 25, '2021-01-01'),
('EE', 20, '2021-01-01'),
('GR', 24, '2021-01-01'),
('ES', 21, '2021-01-01'),
('FI', 24, '2021-01-01'),
('FR', 20, '2021-01-01'),
('HR', 25, '2021-01-01'),
('HU', 27, '2021-01-01'),
('IE', 23, '2021-01-01'),
('IT', 22, '2021-01-01'),
('LT', 21, '2021-01-01'),
('LU', 17, '2021-01-01'),
('LV', 21, '2021-01-01'),
('MT', 18, '2021-01-01'),
('NL', 21, '2021-01-01'),
('PL', 23, '2021-01-01'),
('PT', 23, '2021-01-01'),
('RO', 19, '2021-01-01'),
('SE', 25, '2021-01-01'),
('SI', 22, '2021-01-01'),
('SK', 20, '2021-01-01');

--
-- #297: MDT: Funkce Save as pro receptury
--
insert into users_acl (role, resource, description)
select role, 'tobacco/recipe-save-as' as resource, 'vytvoreni nove verze receptury' as description
from users_acl where resource like 'tobacco/recipe-edit';


--
-- #290: Editace HS kódů
--
create table hs_codes
(
    code varchar(50) null comment 'HS kod',
    description varchar(255) not null comment 'popis',
    note varchar(255) null comment 'poznamka',
    constraint hs_codes_pk
        primary key (code)
)
    comment 'HS kody pro faktury';

--
-- #295: MDT Sleva vs. zaloha
--
insert into users_acl (role, resource, description)
select role, 'tobacco-invoice/load-shipment-from-order', 'nahrat dopravu z objednavky'
from users_acl where resource like 'tobacco-invoice/prepare';

--
-- Hotfix: opravneni pro ucetni na prehled objednavek/platby
--
insert into users_acl (role, resource) values
('accountant_tobacco', 'tobacco-orders/index'),
('accountant_tobacco', 'tobacco-orders/pay');

--
-- #299: Zobrazování loga MEUDSE na faktuře
--
alter table invoice
    add show_logo enum('y', 'n') default 'y' not null comment 'zobrazovat logo na fakture' after export;

--
-- #306: Úprava: nastavení / Uživatelské text (faktury)
--
alter table invoice_custom_texts
    add invoice_type enum('all', 'proforma', 'regular') default 'all' not null after description;
--
-- #300: Údaje zákazníka - telefon
--
ALTER TABLE customers
    CHANGE COLUMN `mobile` `mobile` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `phone`;
alter table customers modify mobile varchar(100) charset utf8 null comment 'nepouzivat';
update customers set phone = mobile
where (phone = '' or phone is null) and (mobile <> '' and mobile is not null);
alter table addresses modify phone varchar(100) null;
alter table producers_persons modify phone varchar(100) null;
alter table producers_persons modify phone2 varchar(100) null;

-- hotfix
insert into users_acl (role, resource, description)
select role, 'tobacco/parts-restore', description
from users_acl where resource like 'tobacco/parts-trash';

--
-- #290: Editace HS kodu
--
alter table hs_codes
    add weight smallint default 0 not null comment 'vaha';

--
-- #303: Zmena data expedice
--
insert into users_acl (role, resource, description)
values (null, 'orders/ajax-change-expedition-date', 'zmena data expedice'),
       ('admin', 'orders/ajax-change-expedition-date', null),
       ('manager', 'orders/ajax-change-expedition-date', null),
       ('head_warehouseman', 'orders/ajax-change-expedition-date', null);

--
-- #304: Polozky faktury vcetne DPH
--
alter table invoice_custom_items
    add vat_included enum('n', 'y') default 'n' not null comment 'cena je bez (n) nebo vcetne (y) DPH';
alter table invoice_items
    add vat_included enum('n', 'y') default 'n' not null comment 'cena je bez (n) nebo vcetne (y) DPH';

--
-- #307: Vložení skladové položky (HS Code)
--
alter table hs_codes
    alter column code drop default;
alter table hs_codes
    comment 'Dostupne HS kody pro produkty a fakturaci.';
alter table hs_codes
    add avail_products enum ('y', 'n') default 'n' not null comment 'dostupny pri editaci produktu' after code;
alter table hs_codes
    add avail_invoices enum ('y', 'n') default 'y' not null comment 'dostupny pri editaci objednavky/faktury' after avail_products;
update hs_codes set avail_products = 'y' where code = '73269098';
insert into hs_codes (code, description, avail_products, avail_invoices)
select distinct hs as 'code', hs as 'description', 'y' as 'avail_products', 'n' as 'avail_invoices' from parts where hs is not null and hs <> '' and hs <> '73269098';


--
-- Hotfix: Zákaz ruční změny stavu objednávky v editaci
--
delete from users_acl where resource like 'orders/special/status' and role in ('seller', 'all_seller');

--
-- #315: Editace profilu zákazníka + editace COO a ověření faktury
--
alter table customers
    add coo_requirement enum ('y', 'n') default 'n' not null comment 'je pozadovano COO?' after due,
    add invoice_verify_requirement enum ('y', 'n') default 'n' null comment 'je pozadovano overeni faktury?' after coo_requirement;
insert into users_acl (role, resource, description)
select role, 'invoice/settings-fees', 'nastaveni fixnich poplatku na fakture' from users_acl where resource like 'admin/rate-settings';

--
-- #316: Mazání dodacích adres zákazníka
--
alter table addresses
    add deleted bool default false not null comment 'je smazana?' after country;
insert into users_acl (role, resource, description)
select role, 'customers/address-delete', description
from users_acl where resource like 'customers/address';
insert into users_acl (role, resource, description)
select role, 'customers/address-undelete', description
from users_acl where resource like 'customers/address';
insert into users_acl (role, resource, description)
select role, 'tobacco-customers/address-delete', description
from users_acl where resource like 'tobacco-customers/address';
insert into users_acl (role, resource, description)
select role, 'tobacco-customers/address-undelete', description
from users_acl where resource like 'tobacco-customers/address';