<?php
/**
 * @author Jakub Niec <jakubniec@gmail.com>
 */
interface ZGrid_Renderer_Interface
{
	/**
	 * @return string XHTML code of the table
	 */
	public function render(ZGrid $grid);
}
