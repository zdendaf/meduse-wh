<?php
/**
 * @author Jakub Niec <jakubniec@gmail.com>
 */
class ZGrid_Renderer_Default implements ZGrid_Renderer_Interface
{
	/**
	 * Returns string representation of grid
	 * @param ZGrid $grid
	 * @return string XHTML
	 */
	public function render(ZGrid $grid)
	{
		$tmpGridId = 'grid0'; //TODO: add to the grid options
		$output = '<table id="'.$tmpGridId.'" class="'.$grid->getOption('css_class').'">'."\n";
		$view   = Zend_Layout::getMvcInstance()->getView();

		$output .= '<thead>'."\n";$columns = $grid->getOption('columns');
		$columnsCount = count($columns);
		if($grid->getOption('allow_multi_action')) {
			$columnsCount++;
		}
		if($grid->getOption('allow_title')) {
			$output .= '<tr>';
			$output .= '<th colspan="'.$columnsCount.'">'.$grid->getOption('title');
			if($grid->getOption('allow_add_link')) {
				$output .= '<a class="btn btn-normal btn-small" style="float: right" href="'.$grid->getOption('add_link_url').'" alt=""><i class="icon-plus"></i> '.$grid->getOption('add_link_title').'</a>';
			}
			$output .= '</th></tr>'."\n";
		}

		$output .= '<tr>';
		if($grid->getOption('allow_multi_action')) {
			$output .= '<th style="width: 20px; text-align: center;"><input type="checkbox" id="'.$grid->getOption('grid_id').'_check_all"></th>';
		}
		foreach($columns as $dbRowName => $colOptions) {
      $class = isset($colOptions['header_class']) ? ' class="' . $colOptions['header_class'] . '"' : '';
			if (isset($colOptions['sorting']) && $colOptions['sorting']) {
				$arrow = '';
				$order = null;
				$colOptions['header']= $view->translate($colOptions['header']);

				if ($grid->getOption('curr_sort') == $dbRowName && $grid->getOption('curr_sort_order') == ZGrid::ORDER_ASC) {
					$order = ZGrid::ORDER_DESC;
					$arrow = '<i class="icon-arrow-down"></i>';
				}
        elseif ($grid->getOption('curr_sort') == $dbRowName && $grid->getOption('curr_sort_order') == ZGrid::ORDER_DESC) {
					$order = ZGrid::ORDER_ASC;
					$arrow = '<i class="icon-arrow-up"></i>';
				}
				$output .= '<th' . $class . '><a href="'.$view->url(array($grid->getOption('url_sort_name')=>$dbRowName, $grid->getOption('url_sort_order_name') => $order)).'#'.$tmpGridId.'">'.$colOptions['header'].$arrow.'</a></th>';
			}
      else {
				$output .= '<th' . $class . '>'.$colOptions['header'].'</th>';
			}
		}
		$output .= '</tr>'."\n";

		$output .= '</thead>'."\n";

		$output .= '<tbody>'."\n";

		$sumArray = array();
		foreach($grid->getPaginator() as $dbRow) {
			$first = true;
			$output .= '<tr id="'.(isset($dbRow[$grid->getOption('id_row_name')]) ? $grid->getOption('grid_id').'_row_'.$dbRow[$grid->getOption('id_row_name')] : '').' " class="lines">';
			foreach($columns as $dbRowName => $colOptions) {
				if($first && $grid->getOption('allow_multi_action')) {
					$first = false;
					$output .= '<td style="width: 20px; text-align: center;"><input type="checkbox" id="'.$grid->getOption('grid_id').'_check_'.$dbRow[$grid->getOption('id_row_name')].'"></td>';//'.2.'
				}
				$columnClass = '';
				if(isset($colOptions['css_class'])) {
					$columnClass = ' class="'.$colOptions['css_class'].'" ';
				}
				$columnStyle = '';
				if(isset($colOptions['css_style'])) {
					$columnClass = ' style="'.$colOptions['css_style'].'" ';
				}

				$output .= '<td'.$columnClass.$columnStyle.'>';
				if(isset($colOptions['renderer'])) {
					$rendererName = strtr(ucwords(strtr($colOptions['renderer'], '_', ' ')), ' ', '_');
					$className = "ZGrid_Column_Renderer_" . $rendererName;
					$renderer = new $className;
					$output .= $renderer->render($dbRowName, $colOptions, $dbRow);
				} else {
					$output .= $dbRow[$dbRowName];
				}


				if(isset($colOptions['sum']) && $colOptions['sum']) {
					if(!isset($sumArray[$dbRowName])) {
						$sumArray[$dbRowName] = 0;
					}
					$sumArray[$dbRowName] += $dbRow[$dbRowName];
				}

				$output .= '</td>';

			}
			$output .= '</tr>'."\n";
		}

		if(count($sumArray) > 0) {
			$output .= '<tr>';
			foreach($columns as $dbRowName => $colOptions) {
				if(isset($sumArray[$dbRowName])){
					$sum = $sumArray[$dbRowName];
				} else {
					$sum = '';
				}

				if (isset($colOptions['renderer']) && $colOptions['renderer'] === 'currency') {


          // rounding
          if (isset($colOptions['currency_options']['rounding_precision'])) {
            $sum = number_format($sum, $colOptions['currency_options']['rounding_precision'], ".", " ");
          }

          // set currency
          if (isset($colOptions['currency_options']['currency_symbol'])) {
            $symbol = $dbRow[$colOptions['currency_options']['currency_symbol']];
          }
          else {
            $symbol = '';
          }

          // return
          if ($symbol == '$') {
            $sum = $symbol . $sum;
          }
          else {
            $sum = $sum . ' ' . $symbol;
          }
          $units = '';
        }
				else {
          if (isset($colOptions['sum_unit']) && !empty($colOptions['sum_unit'])) {
            $units = ' '.$colOptions['sum_unit'];
          }
          else {
            $units = '';
          }
        }
        $output .= '<th class="' . (isset($colOptions['header_class']) ? $colOptions['header_class'] : '') . '">'.$sum.$units.'</th>';
			}
			$output .= '</tr>'."\n";

		}

		$output .= '</tbody>'."\n";

		$output .= '<tfoot>'."\n";

		if($grid->getOption('allow_paginator')) {

			$output .= '<tr><td colspan="'.$columnsCount.'"><form style="margin-bottom: 0px;" class="form-inline">';

			if($grid->getOption('allow_multi_action')) {
//				$output .=
//					'<select style="width: 100px;" name="ipp" id="ipp>' .
//						'<option value="10" label="10" selected="selected">Action1</option>'.
//						'<option value="25" label="25">Action2</option>'.
//					'</select>'.
//					'<button class="btn btn-small btn-primary">GO</button>'; // onchange="return EmitoGrid.loadGridData(null, 'users-list', '/admin/fetch-Users-List')
			}
//			$output .= '
//				<span style="float: right;">
//							Ilość pozycji na stronę:
//							<select style="width: 60px;" name="ipp" id="ipp">
//							<option value="10" label="10" selected="selected">10</option>
//							<option value="25" label="25">25</option>
//							<option value="50" label="50">50</option>
//							<option value="75" label="75">75</option>
//							<option value="100" label="100">100</option>
//							</select>
//							Idź do strony:
//							<select style="width: 60px;" name="page" id="page">
//							<option value="0" label="1" selected="selected">1</option>
//							</select>
//							1 - 2 z 2 <a href="" class="disabled" onclick="return false" title="Poprzednia strona"><i class="icon-arrow-left"></i> Poprzednia</a> |  <a href="" class="disabled" onclick="return false" title="Następna strona">Następna <i class="icon-arrow-right"></i></a>
//						</span>
//				';// onchange="return EmitoGrid.loadGridData(null, 'users-list', '/admin/fetch-Users-List')

			$output .= $this->renderPaginator($grid->getPaginator(), $grid);

			$output .= '</form>';

			$output .= '</td></tr><tfoot>'."\n";
		}

		$output .= '</table>';
		return $output;
	}

	public function renderPaginator(Zend_Paginator $paginator, $grid)
	{

		$pages = $paginator->getPages();
		$view = Zend_Layout::getMvcInstance()->getView();

		if($pages->pageCount < 2) return '';

		$range = (int) $paginator->getPageRange();

		$output = '<span style="float: right;">';

		//začátek
    ///šipka na předcházející stránku
    if ($pages->current != 1) {
      $output .= '<a class="btn" style="float: left;" href="'.$view->url(array($grid->getOption('url_page_name') => ($pages->current-1))).'"><i class="icon-arrow-left"></i>&nbsp;</a>';
    }
    ///zobrazení první stránky + teček, pokud je mezera mezi ní a aktuálním rozsahem
		if($pages->firstPageInRange > 1) {
			$output .= '<a class="btn" style="float: left;" href="'.$view->url(array($grid->getOption('url_page_name') => null)).'">1</a>';
			$prewPage = $pages->lastPageInRange - $range;
			if($prewPage > 1) {
				$output .= '<a class="btn" style="float: left;" href="'.$view->url(array($grid->getOption('url_page_name') => $prewPage)).'">&hellip;</a>';
			}
		}

    //aktuální zúžený rozsah
		$output .= '<div class="btn-group" style="float: left;">';
		foreach ($pages->pagesInRange as $page) {
			if($page != $pages->current){
				$output .= '<a class="btn" href="'.$view->url(array($grid->getOption('url_page_name') => $page)).'">'.$page.'</a>';
			} else {
				$output .= '<a class="btn" href="'.$view->url(array($grid->getOption('url_page_name') => $page)).'">['.$page.']</a>';
			}

		}
		$output .= '</div>';

		//konec
    ///zobrazení poslední stránky + teček, pokud je mezera mezi ní a aktuálním rozsahem
		if($pages->lastPageInRange < $pages->pageCount) {
			$nextPage = $pages->firstPageInRange + $range;
			if($nextPage <= $pages->pageCount) {
				$output .= '<a class="btn" style="float: left;" href="'.$view->url(array($grid->getOption('url_page_name') => $nextPage)).'">&hellip;</a>';
			}
			$output .= '<a class="btn" style="float: left;" href="'.$view->url(array($grid->getOption('url_page_name') => $pages->pageCount)).'">'.$pages->pageCount.'</a>';
		}

    ///šipka na následující stranu
    if ($pages->current != $pages->pageCount) {
      $output .= '<a class="btn" style="float: left;" href="'.$view->url(array($grid->getOption('url_page_name') => ($pages->current+1))).'">&nbsp;<i class="icon-arrow-right"></i></a>';
    }

    $output .= '</span>';
		return $output;
	}
}
