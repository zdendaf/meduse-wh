<?php

/**
 * @author Jakub Hantak
 *
 * @example
 * 'currency_options' => array(
 *          'currency_value' => $dbRow[value_name] // eg. price
 *          'currency_symbol' => $dbRow[value_name]  // eg. price_currency
 *          'rounding_precision' => [int] // eg. 2
 *      )
 */
class ZGrid_Column_Renderer_Currency extends ZGrid_Column_Renderer_Abstract {

  public function render($dbRowName, $colOptions, $dbRow) {
    /***************************************************************************************************************************************************
     * Twitter Bootstrap @override
     * if page layout is setup as TB in controller
     * then return valid result from latest renderer class.
     * Latest renderer class is loaded here automatically, if layout was set to new TB
     */
    if (Zend_Layout::getMvcInstance()->getLayout() == 'bootstrap-basic') {
      $Class = new ZGrid_Column_Renderer_Bootstrap_Currency();
      return $Class->render($dbRowName, $colOptions, $dbRow);
    }
    /***************************************************************************************************************************************************
     ** part of code below this line will be deprecated soon
     **************************************************************************************************************************************************/

    if (!isset($colOptions['currency_options'])) {
      throw new Exception('Renderer::Currency, [currency_options] is missing. Check the description.');
    }

    // set defaults
    $value = '';
    $symbol = '';

    // set value
    if (isset($colOptions['currency_options']['currency_value'])) {
      $value = $dbRow[$colOptions['currency_options']['currency_value']];
    }

    // rounding
    if (isset($colOptions['currency_options']['rounding_precision'])) {
      $value = number_format($value, $colOptions['currency_options']['rounding_precision'], ".", " ");
    }

    // set currency
    if (isset($colOptions['currency_options']['currency_symbol'])) {
      $symbol = $dbRow[$colOptions['currency_options']['currency_symbol']];
    }

    // return
    if ($symbol == '$') {
      return $symbol . $value;
    }
    else {
      return $value . ' ' . $symbol;
    }


  }
}
