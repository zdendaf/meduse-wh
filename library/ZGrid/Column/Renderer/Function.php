<?php

/**
 * Renderer pro zavolání specifické funkce či metody s příslušnými parametry
 */
class ZGrid_Column_Renderer_Function extends ZGrid_Column_Renderer_Abstract {

  /**
   * Vyrenderování pole
   * 
   * @param  string $dbRowName  Nepoužívá se, libovolná hodnota
   * @param  array  $colOptions Atributy v gridu, vyžadován při nejmenším function_name, případně navíc class_name (při volání metody třídy)
   * @param  array  $dbRow      Soubor načtených polí
   * @return string             Výsledek volání funkce přetypovaný na string
   */
  public function render($dbRowName, $colOptions, $dbRow) {
    
    //Ošetření při nedefinovaných atributech pro volání fce
    if (!is_array($colOptions['function_attributes'])) {
      $colOptions['function_attributes'] = (array)$colOptions['function_attributes'];
    }
    
    //Přetypování atributů funkce
    $attributes = array();
    foreach ($colOptions['function_attributes'] as $col_option) {
      //Jestliže je atribut uzavřen do dvou hranatých závorek, jakože [[atribut]], je brán jako sloupec ze ZGridu s daným názvem.
      if (is_string($col_option) AND substr($col_option, 0, 2)=='[[' AND substr($col_option, -2, 2)==']]') {
        $attributes[] = $dbRow[substr($col_option, 2, -2)];
        
      //V opačném případě se hodnota převezme doslova.
      } else {
        $attributes[] = $col_option;
      }
    }
    
    //Jestliže je definována volba "class_name", volá se funkce jako metoda dané třídy, jinak jako samostatná fce.
    if (isset($colOptions['class_name'])) {
      $class = new $colOptions['class_name'];
      $output = call_user_func_array(array($class, $colOptions['function_name']), $attributes);
    } else {
      $output = call_user_func_array($colOptions['function_name'], $attributes);
    }
    
    return $output;
  }

}
