<?php

/**
 * @author Jakub Niec <jakubniec@gmail.com>
 */
class ZGrid_Column_Renderer_Url extends ZGrid_Column_Renderer_Abstract
{
    public function render($dbRowName, $colOptions, $dbRow)
    {
        if (!isset($colOptions['url'])) {
            throw new Exception('Parameter "url" is required for url renderer');
        }
        if (!isset($dbRow[$dbRowName])) {
            $linkText = $dbRowName;
        } else {
            $linkText = $dbRow[$dbRowName];
        }
        $url = $this->replaceUrl($colOptions['url'], $dbRow);
        return '<a href="' . $url . '">' . $linkText . '</a>';
    }
}
