<?php
/**
 * @author Jakub Hantak <dare@lanzone.cz>
 */
class ZGrid_Column_Renderer_Date extends ZGrid_Column_Renderer_Abstract {

    public function render($dbRowName, $colOptions, $dbRow) {
        if (is_null($dbRow[$dbRowName])) {
          return '';
        }
        else {
          return Meduse_Date::dbToForm($dbRow[$dbRowName]);
        }
    }
}
