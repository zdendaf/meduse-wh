<?php

/**
 * After evaluation of rows calls another renderer depending on condition status (true/false).
 * 
 * Uses options [condition_value] for evaluation, and arrays [condition_true]/[condition_false] for specification of renderer after evaluation.
 * 
 * Inside [condition_true]/[condition_false] can be used option [db_row_name] to change column of data.
 * 
 * Option [condition_false] is optional.
 *
 * @author Vojtěch Mrkývka
 */
class ZGrid_Column_Renderer_Condition extends ZGrid_Column_Renderer_Abstract {
  public function render($dbRowName, $colOptions, $dbRow) {
    
    // Checking if the right values of options are used
    #if (!isset($colOptions['condition_value'])) {}
    
    //evaluation
    if ($colOptions['condition_value'] == $dbRow[$dbRowName]) {
      if (array_key_exists('condition_true', $colOptions)) {
        //setting default $dbRowName for true values
        if (!isset($colOptions['condition_true']['db_row_name'])) {
          $colOptions['condition_true']['db_row_name'] = $dbRowName;
        }

        //calling another renderer (true)
        $rend_form = ($colOptions['condition_true']['renderer'] ? ('ZGrid_Column_Renderer_' . $colOptions['condition_true']['renderer']) : 'ZGrid_Renderer_Default');
        $renderer = new $rend_form();
        return $renderer->render($colOptions['condition_true']['db_row_name'], $colOptions['condition_true'], $dbRow);
      }
    } elseif (array_key_exists('condition_false', $colOptions)) {
      //setting default $dbRowName for false values
      if (!isset($colOptions['condition_false']['db_row_name'])) {
        $colOptions['condition_false']['db_row_name'] = $dbRowName;
      }

      //calling another renderer (false)
      $rend_form = ($colOptions['condition_false']['renderer'] ? ('ZGrid_Column_Renderer_' . $colOptions['condition_false']['renderer']) : 'ZGrid_Renderer_Default');
      $renderer = new $rend_form();
      return $renderer->render($colOptions['condition_false']['db_row_name'], $colOptions['condition_false'], $dbRow);
    }
  }

}
