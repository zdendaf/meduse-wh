<?php
/**
 * @author Jakub Niec <jakubniec@gmail.com>
 * @edited Jakub Hantak
 */
class ZGrid_Column_Renderer_Action extends ZGrid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow)
	{
        /***************************************************************************************************************************************************
         * Twitter Bootstrap @override
         * if page layout is setup as TB in controller
         * then return valid result from latest renderer class.
         * Latest renderer class is loaded here automatically, if layout was set to new TB
         */
        if(Zend_Layout::getMvcInstance()->getLayout() == 'bootstrap-basic') {
            $Class = new ZGrid_Column_Renderer_Bootstrap_Action();
            return $Class->render($dbRowName, $colOptions, $dbRow);
        }
        /***************************************************************************************************************************************************
         ** part of code below this line will be deprecated soon
         **************************************************************************************************************************************************/
        
        
        
        
		if(isset($colOptions['type']) && isset($colOptions['url'])) {
			$url = $this->replaceUrl($colOptions['url'], $dbRow);
			switch($colOptions['type']){
				case 'delete':
					return '<a href="'.$url.'"><img alt="smazat" src="/public/images/delete.png" onClick="return confirm(\'Opravdu chcete tent záznam odstranit?\')"></i></a>';
				case 'edit':
					return '<a href="'.$url.'"><img alt="smazat" src="/public/images/edit.png"></i></a>';
				case 'assign':
					return '<a href="'.$url.'">přiřadit</a>';
					return;
				default:
					throw new Exception('Unknown action type.');
			}
		} else {
//			Zend_Debug::dump($colOptions); die;
			throw new Exception('Parameters "type" and "url" are required for "action" renderer!');
		}
	}
}
