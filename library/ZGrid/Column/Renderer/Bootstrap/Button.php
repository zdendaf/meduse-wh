<?php

class ZGrid_Column_Renderer_Bootstrap_Button extends ZGrid_Column_Renderer_Abstract {
	
  public function render($dbRowName, $colOptions, $dbRow) {
		if (isset($colOptions['icon']) 
        && isset($colOptions['url'])) {
			$url = $this->replaceUrl($colOptions['url'], $dbRow);
      
      $confirm = (isset($colOptions['confirm']))?
        ' onClick="return confirm(\'' . $colOptions['confirm'] . '\')"' : '';
      
      if (isset($colOptions['on_click'])) {
        while(preg_match('/\[\[([^\]]*)\]\]/', $colOptions['on_click'] , $oc_match)) {
          $colOptions['on_click'] = str_replace($oc_match[0], $dbRow[$oc_match[1]], $colOptions['on_click']);
        }
        
        $confirm = ' onClick="' . $colOptions['on_click'] . '"';
      }
      
      
      $text = isset($colOptions['text'])? $this->replaceUrl($colOptions['text'], $dbRow): '';
      
      $alt = isset($colOptions['alt'])? $colOptions['alt'] : $text;
      
      $title = isset($colOptions['alt'])? $alt : $text;
      
      $icon = $colOptions['icon'];
      
      $class = isset($colOptions['class']) ? $colOptions['class'] : 'btn btn-normal btn-small';
      
      $html  = '<a class="' . $class . '" href="' . $url . '" title="' . $title . '"'
        . 'alt="' . $alt . '"' . $confirm . '><i class="' . $icon . '"></i> ' 
        . $text . '</a>';
      
      return $html;
    } else {
			throw new Exception('Parameters "icon", "url" are required for "button" renderer!');
		}
	}
}
