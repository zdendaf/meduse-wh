<?php
/**
 * @author Jakub Hantak
 * 
 */
class ZGrid_Column_Renderer_Bootstrap_Action extends ZGrid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow)
	{
		if(isset($colOptions['type']) && isset($colOptions['url'])) {
            // set url
			$url = $this->replaceUrl($colOptions['url'], $dbRow);
            
            // return url link by type
			switch($colOptions['type']) {
				case 'delete':
					return '<a class="btn btn-danger btn-small" href="'.$url.'" alt="smazat" onClick="return confirm(\'Opravdu chcete tento záznam odstranit?\')"><i class="icon-remove icon-white"></i></a>';
				case 'edit':
					return '<a class="btn btn-normal btn-small" href="'.$url.'" alt="upravit"><i class="icon-edit"></i></a>';
				case 'assign':
					return '<a href="'.$url.'">přiřadit</a>';
        case 'add':  
          return '<a class="btn btn-normal btn-small" href="'.$url.'" alt="přidat"><i class="icon-plus"></i></a>';
        case 'check':  
          return '<a class="btn btn-normal btn-small" href="'.$url.'" alt="zkontrolovat"><i class="icon-check"></i></a>';
        case 'remove':  
          return '<a class="btn btn-normal btn-small" href="'.$url.'" alt="odebrat"><i class="icon-remove"></i></a>';
        case 'saveAs':
          return '<a class="btn btn-success btn-small" href="'.$url.'" alt="odebrat"><i class="icon-plus-sign icon-white"></i></a>';
				default:
					throw new Exception('Unknown action type.');
			}
		} else {
			throw new Exception('Parameters "type" and "url" are required for "action" renderer!');
		}
	}
}
