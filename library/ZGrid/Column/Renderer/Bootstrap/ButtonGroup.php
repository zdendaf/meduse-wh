<?php

class ZGrid_Column_Renderer_Bootstrap_ButtonGroup extends ZGrid_Column_Renderer_Abstract {
	
  public function render($dbRowName, $colOptions, $dbRow) {
    
    if (empty($colOptions['group'])) {
      throw new Exception('Skupina neobsahuje žádné prvky');
    }
    $group = '';
    foreach ($colOptions['group'] as $button) {
      if (isset($button['icon']) 
          && isset($button['url'])) {
        $url = $this->replaceUrl($button['url'], $dbRow);
        $confirm = (isset($button['confirm']))?
          ' onClick="return confirm(\'' . $button['confirm'] . '\')"' : '';
        $text = isset($button['text'])? $button['text'] : '';
        $alt = isset($button['alt'])? $button['alt'] : $text;
        $title = isset($button['text'])? $button['text'] : $alt;
        $icon = $button['icon'];

        $buttonHtml  = '<a class="btn btn-normal" href="' . $url . '" title="' . $title . '"'
          . 'alt="' . $alt . '"' . $confirm . '><i class="' . $icon . '"></i> ' 
          . $text . '</a>';
      } else {
        throw new Exception('Parameters "icon", "url" are required for "button" renderer!');
      }      
      $group .= $buttonHtml;
    }
    return '<div class="btn-group">' . $group . '</div>';    
	}
}
