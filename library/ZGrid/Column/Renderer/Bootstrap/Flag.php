<?php
/**
 * @author Jakub Hantak
 *  
 * 
 * flag Css Twitter bootstrap
 * 
 * you can specify css value is necessary
 * or use default css class
 * OK - css_ok
 * NO - css_off
 * 
 */
class ZGrid_Column_Renderer_Bootstrap_Flag extends ZGrid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow)
	{
        // switch css
        // set default css class if is not setup
        $state = (isset($colOptions['states']))? 
          $dbRow[$dbRowName] == $colOptions['states'][1]: $dbRow[$dbRowName];
        if($state) {
            $imgCss = ((isset($colOptions['css_on'])) ? $colOptions['css_on'] : "icon-ok-circle");
        } else {
            $imgCss = ((isset($colOptions['css_off'])) ? $colOptions['css_off'] : ""); // icon-remove-circle
        }
		return '<i class="' . $imgCss. ' " alt=" ' .$dbRow[$dbRowName]. ' "></i>';
	}
}
