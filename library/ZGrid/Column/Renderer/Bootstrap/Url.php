<?php

/**
 * @author Zdeněk Filipec <zdendaf@gmail.com>
 *
 */ 

class ZGrid_Column_Renderer_Bootstrap_Url extends ZGrid_Column_Renderer_Abstract {
    public function render($dbRowName, $colOptions, $dbRow) {
        if (!isset($colOptions['url'])) {
            throw new Exception('Parameter "url" is required for url renderer');
        }
        if (!isset($dbRow[$dbRowName])) {
            $linkText = $dbRowName;
        } else {
            $linkText = $dbRow[$dbRowName];
        }
        $url = $this->replaceUrl($colOptions['url'], $dbRow);
        $attribs = '';
        if (isset($colOptions['attribs'])) {
          foreach ($colOptions['attribs'] as $attr => $value) {
            $value = $this->replaceUrl($value, $dbRow);
            $attribs .= $attr . '="' . $value . '" ';
          }
        }
        return '<a href="' . $url . '"' . $attribs . '>' . $linkText . '</a>';
    }
}
