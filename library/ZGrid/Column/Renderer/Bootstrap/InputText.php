<?php
class ZGrid_Column_Renderer_Bootstrap_InputText extends ZGrid_Column_Renderer_Abstract {

  public function render($dbRowName, $colOptions, $dbRow) {

    $value = $dbRow[$dbRowName] ?? '';
    $id = $colOptions['input_id'] ? $this->replaceUrl($colOptions['input_id'], $dbRow) : '';
    $class = $colOptions['input_class'] ? $this->replaceUrl($colOptions['input_class'], $dbRow) : '';
    $size = $colOptions['input_size'] ?? '';
    $data = [];
    foreach ($colOptions['input_data'] as $k => $v) {
      $data[] = 'data-' . $k . '="' . $this->replaceUrl($v, $dbRow)  . '"';
    }
    $data = implode(' ', $data);

    return "<input id='$id' type='text' value='$value' class='$class' size='$size' $data>";

	}
}
