<?php
/**
 * @author Jakub Niec <jakubniec@gmail.com>
 */
abstract class ZGrid_Column_Renderer_Abstract
{
	public abstract function render($dbRowName, $colOptions, $dbRow);

	/**
	 * Search in url {row_name} patterns and replace it with value from array
	 * there can be multiple rows patterns as long as they are in rowData
	 * @param string $url
	 * @param array $rowData
	 * @return string
	 */
    public function replaceUrl($url, $rowData)
    {
		/**
		 * TODO:
		 * Possible alternative solution - str_replace()
		 * str_replace(array('{'.$rowName.'}',...), $rowData, $url);
		 */

		$url = urldecode($url);
		$finalUrl = "";
		$found = false;
		$rowName = "";
		for($i = 0; $i < strlen($url); $i++){
			if($url[$i] == '}') {
				$found = false;
				$finalUrl .= $rowData[$rowName];
				continue;
			}
			if($url[$i] == '{'){
				$found = true;
				$rowName = "";
				continue;
			}
			if($found){
				$rowName .= $url[$i];

			} else {
				$finalUrl .= $url[$i];
			}
		}

		return $finalUrl;
	}
}
