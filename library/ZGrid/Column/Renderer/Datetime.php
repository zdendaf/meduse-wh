<?php
  
class ZGrid_Column_Renderer_Datetime extends ZGrid_Column_Renderer_Abstract {
    public function render($dbRowName, $colOptions, $dbRow) {
        if (is_null($dbRow[$dbRowName])) {
          return '';
        } else {
          $zdate = new Zend_Date($dbRow[$dbRowName]);
          return $zdate->toString('dd.MM.yyyy h:m');
        }
    }
}
