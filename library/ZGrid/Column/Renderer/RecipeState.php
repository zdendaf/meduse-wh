<?php

// Skoveho

class ZGrid_Column_Renderer_RecipeState extends ZGrid_Column_Renderer_Abstract {

  public function render($dbRowName, $colOptions, $dbRow) {
    $state = $dbRow[$dbRowName];
    switch ($state) {
      case 'active':
        return 'Aktivní';
      case 'inactive':
        return 'Neaktivní';
      default:
        return 'N/A';
    }
  }

}
