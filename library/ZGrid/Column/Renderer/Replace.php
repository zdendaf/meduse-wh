<?php
/**
 * @author Jakub Hantak
 * 
 * replace_value string        // value for compare
 * replace_conditions array   // conditions
 *   key = what
 *   val = replace for
 * 
 * @example 
 * 'replace_value' => 'name'
 * 'replace_conditions' => array(
 *          'aaa' => 'Joe',
 *          'bbb' => 'Frank'
 *      )
 */
class ZGrid_Column_Renderer_Replace extends ZGrid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow)
	{
        /***************************************************************************************************************************************************
         * Twitter Bootstrap @override
         * if page layout is setup as TB in controller
         * then return valid result from latest renderer class.
         * Latest renderer class is loaded here automatically, if layout was set to new TB
         */
        if(Zend_Layout::getMvcInstance()->getLayout() == 'bootstrap-basic') {
            $Class = new ZGrid_Column_Renderer_Bootstrap_Replace();
            return $Class->render($dbRowName, $colOptions, $dbRow);
        }
        /***************************************************************************************************************************************************
         ** part of code below this line will be deprecated soon
         **************************************************************************************************************************************************/
        
        if(!isset($colOptions['replace_conditions'])) {
            throw new Exception('Renderer::Replace, [replace_conditions] are missing for render Replace. Check the description.');
        }

        // set defaults
        $conditions;
        $value;
        
          
        // set value
        if(!isset($colOptions['replace_value'])) {
			throw new Exception('Renderer::Replace, [replace_value] not exist or not set. Check the description.');
		}
        else {
            $value = $colOptions['replace_value'];
        }
        
        // set conditions
		if(!isset($colOptions['replace_conditions']) && !is_array($colOptions['replace_conditions'])) {
			throw new Exception('Renderer::Replace, [replace_conditions] must be an array. Check the description.');
		}
        else {
            $conditions = $colOptions['replace_conditions'];
        }
   
        // check if conditions are met
        foreach($conditions as $key => $val)
        {
            if($dbRow[$value] == $key) {
                return $val;
            }
        }
        
        
	}
}
