<?php
/**
 * @author Jakub Niec <jakubniec@gmail.com>
 */
class ZGrid_Column_Renderer_Flag extends ZGrid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow)
	{
        /***************************************************************************************************************************************************
         * Twitter Bootstrap @override
         * if page layout is setup as TB in controller
         * then return valid result from latest renderer class.
         * Latest renderer class is loaded here automatically, if layout was set to new TB
         */
        if(Zend_Layout::getMvcInstance()->getLayout() == 'bootstrap-basic') {
            $Class = new ZGrid_Column_Renderer_Bootstrap_Flag();
            return $Class->render($dbRowName, $colOptions, $dbRow);
        }
        /***************************************************************************************************************************************************
         ** part of code below this line will be deprecated soon
         **************************************************************************************************************************************************/
        
        
        
        
		if(!isset($colOptions['img_on']) || !isset($colOptions['img_off'])){
			throw new Exception('Parameter "url" is required for url renderer');
		}
        if($dbRow[$dbRowName]) {
            $img = $colOptions['img_on'];
        } else {
            $img = $colOptions['img_off'];
        }
		return '<img src="'.$img.'" alt="'.$dbRow[$dbRowName].'">';
	}
}
