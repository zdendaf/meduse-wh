<?php
/**
 * @author Jakub Niec <jakubniec@gmail.com>
 */
class ZGrid
{
	/**
	 * Default grid options:
	 * @var array
	 */
	private $_options = array(
		'allow_add_link'     => false,        //show / hide new record link
		'allow_multi_action' => false,        //show / hide checkbox at the first column + action form on the bottom
		'allow_check_all'    => false,        //show / hide check-all checkbox - header line
		'allow_paginator'    => true,         //show / hide paginator
		'allow_title'        => true,         //show / hide table title
		'add_link_url'       => '#',          //new record link URL
		'add_link_title'     => 'New record', //new record link title
		'css_class'          => 'datalist single_table', //classes from: http://twitter.github.com/bootstrap/base-css.html#tables
		'title'              => '',           //table title
		'grid_id'            => 'datagrid',   //table id
		'columns'            => array(),      //columns options
		'multi_actions'      => array(),      //content of the actin select 'val' => 'title'
		'multi_action_url'   => '',           //url wehere should be send ajax request
		'paginator_class'    => '',           //standard paginator class used. ( can be replaced )
		'items_per_page'     => array(10,25,50,75,100),
		'id_row_name'        => 'id',         //name of primary key
		'renderer'           => 'ZGrid_Renderer_Default',
//		'url_params'         => array() //page/sort/limit/offset
		'url_page_name'      => 'page',
		'url_sort_name'      => 'sort',
		'url_sort_order_name'=> 'sorder',
		'curr_page'          => 1,
		'curr_items_per_page'=> 300,
		'curr_sort'          => '',
		'curr_sort_order'    => self::ORDER_ASC,
	);

	const ORDER_ASC = 'asc';

	const ORDER_DESC = 'desc';

	/**
	 * DOC: (column atributes)
	private $_column_options = array(
		'css_style', //td style
		'css_class', //td class
		'renderer',  //column renderer class name
		'header',    //haeader title
		'sorting',   //sorting flag ( bool )
	    'sum',       //summary flag ( bool )
	    'sum_unit',  //summary unit ( string )
	    // + renderer depending attributes
	);
	 */

	/**
	 * @var Zend_Db_Select
	 */
	private $_select    = null;

	/**
	 * @var Zend_Paginator
	 */
	private $_paginator = null;

	public function __construct($options = null)
	{
        // @override css_class to TB
        $this->_isSetTwitterBootstrapLayout();

		if(is_array($options)) {
			$this->_options = array_merge($this->_options, $options);
		}
	}

    /**
     * Serialize as string
     *
     * Proxies to {@link render()}.
     *
     * @return string
     */
    public function __toString()
    {
        try {
            $return = $this->render();
            return $return;
        } catch (Exception $e) {
            $message = "Exception caught by Grid: " . $e->getMessage()
                     . "\nStack Trace:\n" . $e->getTraceAsString();
			die("<pre>".$message);
//            trigger_error($message, E_USER_WARNING); //commented because triggering of error will cause throwing of exception in error controller, and that's not allowed in __toString function
//            return '';
        }
    }

	/**
	 * Returns grid option
	 * @param string $option
	 * @return mixed
	 * @throws Excetpion
	 */
	public function getOption($option)
	{
		if(array_key_exists($option,$this->_options)) {
			return $this->_options[$option];
		} else {
			throw new Exception("Unknown option $option");
		}
	}

	/**
	 * @return Zend_Paginator
	 */
	public function getPaginator()
	{
		return $this->_paginator;
	}

	/**
	 * @return Acquitec_Grid_Renderer_Interface
	 */
	public function getRenderer()
	{
		return new $this->_options['renderer'];
	}

	/**
	 * @return mixed
	 */
	public function getResult()
	{
		return Zend_Db_Table::getDefaultAdapter()->fetchAll($this->_select);
	}

    /**
     * Render grid
     *
     * @param  Zend_View_Interface $view
     * @return string
     */
    public function render()
    {
		$this->setPaginator();
        return $this->getRenderer()->render($this);
    }

	private function setPaginator()
	{
		$paginator = Zend_Paginator::factory($this->getResult());
		$paginator->setItemCountPerPage($this->_options['curr_items_per_page']);
		$paginator->setPageRange(5);
		$paginator->setCurrentPageNumber($this->_options['curr_page']);
		$this->_paginator = $paginator;
	}

	public function setRequest($requestParams)
	{
		if(isset($requestParams[$this->getOption('url_page_name')])) {
			$this->_options['curr_page'] = $requestParams[$this->getOption('url_page_name')];
		}


		//sorting
    
    
		if(!isset($requestParams[$this->getOption('url_sort_name')])){
			if (empty($this->_options['curr_sort'])) {
        $this->_options['curr_sort'] = null;
      }
		} else {
			$this->_options['curr_sort'] = $requestParams[$this->getOption('url_sort_name')];
		}
		if(!isset($requestParams[$this->getOption('url_sort_order_name')])){
			if (empty($this->_options['curr_sort_order'])) {
        $this->_options['curr_sort_order'] = self::ORDER_ASC;
      }
		} else {
			$this->_options['curr_sort_order'] = $requestParams[$this->getOption('url_sort_order_name')];
		}
		if($this->_options['curr_sort']) {
			$this->_select->reset(Zend_Db_Select::ORDER);
			$this->_select->order($this->_options['curr_sort'].' '.$this->_options['curr_sort_order']);
		}
	}

	/**
	 * @param Zend_Db_Select $select
	 * @throws Exception
	 */
	public function setSelect(Zend_Db_Select $select)
	{
		if($select instanceof Zend_Db_Select) {
			$this->_select = $select;
		} else {
			throw new Exception('select has to be instanceof Zend_Db_Select');
		}
	}

    /**
     * Set title
     * @param string $title
     */
	public function setTitle($title)
	{
		$this->_options['title'] = $title;
	}

    /**
     * Is set Twitter bootstrap layout
     * @override css_class for tb
     */
    private function _isSetTwitterBootstrapLayout()
    {
        $this->_options['css_class'] = ((Zend_Layout::getMvcInstance()->getLayout() == 'bootstrap-basic') ? 'table table-hover table-condensed' : $this->_options['css_class']);
    }
}
