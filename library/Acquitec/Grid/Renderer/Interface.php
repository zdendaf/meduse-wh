<?php
/**
 * @author Jakub Niec <jakub.niec@acquitec.pl>
 * $Id$
 */
interface Acquitec_Grid_Renderer_Interface
{
	/**
	 * @return string XHTML code of the table 
	 */
	public function render(Acquitec_Grid $grid);
}
