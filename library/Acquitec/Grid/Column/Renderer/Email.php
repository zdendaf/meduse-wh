<?php
/**
 * @author Jakub Niec <jakub.niec@acquitec.pl>
 * $Id$
 */
class Acquitec_Grid_Column_Renderer_Email extends Acquitec_Grid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow) 
	{
		return empty($dbRow[$dbRowName]) ? '' : '<a href="mailto:'.$dbRow[$dbRowName].'">'.$dbRow[$dbRowName].'</a>';
	}
}
