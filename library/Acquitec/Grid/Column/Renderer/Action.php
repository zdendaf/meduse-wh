<?php
/**
 * @author Jakub Niec <jakub.niec@acquitec.pl>
 * $Id$
 */
class Acquitec_Grid_Column_Renderer_Action extends Acquitec_Grid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow) 
	{
		if(isset($colOptions['type']) && isset($colOptions['url'])){
			$url = $this->replaceUrl($colOptions['url'], $dbRow);			
			switch($colOptions['type']){
				case 'delete':
					return '<a href="'.$url.'"><img alt="smazat" src="/public/images/delete.png" onClick="return confirm(\'Opravdu chcete tent záznam odstranit?\')"></i></a>';
				case 'edit':
					return '<a href="'.$url.'"><img alt="smazat" src="/public/images/edit.png"></i></a>';
				case 'assign':
					return '<a href="'.$url.'">přiřadit</a>';
					return;
				default:
					throw new Exception('Unknown action type.');
			}
		} else {
//			Zend_Debug::dump($colOptions); die;
			throw new Exception('Parameters "type" and "url" are required for "action" renderer!');
		}
	}
}
