<?php
/**
 * @author Jakub Niec <jakub.niec@acquitec.pl>
 * $Id$
 */
class Acquitec_Grid_Column_Renderer_Url extends Acquitec_Grid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow) 
	{
		if(!isset($colOptions['url'])){
			throw new Exception('Parameter "url" is required for url renderer');
		}
		$url   = $this->replaceUrl($colOptions['url'], $dbRow);
		return '<a href="'.$url.'">'.$dbRow[$dbRowName].'</a>';
	}
}
