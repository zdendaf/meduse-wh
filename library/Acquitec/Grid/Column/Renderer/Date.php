<?php
/**
 * @author Jakub Hantak <dare@lanzone.cz>
 * 
 */
class Acquitec_Grid_Column_Renderer_Date extends Acquitec_Grid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow) 
	{
		$zdate = new Zend_Date($dbRow[$dbRowName]);
                
                return $zdate->toString('dd.MM.yyyy');
	}
}
