<?php
/**
 * @author Jakub Niec <jakub.niec@acquitec.pl>
 * $Id$
 */
class Acquitec_Grid_Column_Renderer_DatePrice extends Acquitec_Grid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow) 
	{

		$modelInstrumentDay =  new Default_Model_InstrumentDay();
		
		$datePrice = $modelInstrumentDay->getPrice($dbRow['instrument_id'], $dbRow['date']);
		
        $diff = $dbRow['last_price'] - $datePrice;
        $percentage = ($diff / $datePrice) * 100;
                    
		return $percentage;
	}
}
