<?php
/**
 * @author Jakub Niec <jakub.niec@acquitec.pl>
 * $Id$
 */
class Acquitec_Grid_Column_Renderer_Flag extends Acquitec_Grid_Column_Renderer_Abstract
{
	public function render($dbRowName, $colOptions, $dbRow) 
	{
		if(!isset($colOptions['img_on']) || !isset($colOptions['img_off'])){
			throw new Exception('Parameter "url" is required for url renderer');
		}
        if($dbRow[$dbRowName]) {
            $img = $colOptions['img_on'];
        } else {
            $img = $colOptions['img_off'];
        }
		return '<img src="'.$img.'" alt="'.$dbRow[$dbRowName].'">';
	}
}
