<?php

class Meduse_Pdf_Invoice extends Meduse_Pdf {

  const PRODUCT_LINES_SINGLE_PAGE = 20;
  const PRODUCT_LINES_FIRST_PAGE = 35;
  const PRODUCT_LINES_MIDDLE_PAGE = 49;
  const PRODUCT_LINES_LAST_PAGE = 30;
  const PRODUCT_LINES_SIZE = 64;
  const PRODUCT_LINES_SHIFT = 28;
  const ADDRESS_LINES_SIZE = 50;
  const NOTICE_LINES_SIZE = 90;
  const PRODUCT_LINES_TYPE_NORMAL = 0;
  const PRODUCT_LINES_TYPE_HEADER = 1;
  const LINE_HEIGHT_ADDRESS = 2;
  const LINE_WIDTH = 536;

  const TEMPLATE_B2C = '../public/pdf/invoice_uni_regular_v11.pdf';

  const TEMPLATE_B2B = '../public/pdf/invoice_uni_regular_v11.pdf';

  const TEMPLATE_EU = '../public/pdf/invoice_uni_regular_v11.pdf';

  const TEMPLATE_EU_NOLOGO = '../public/pdf/invoice_uni_regular_v11_nologo.pdf';

  const TEMPLATE_EU_VAT = '../public/pdf/invoice_uni_regular_vat.pdf';

  const TEMPLATE_EU_VAT_NOLOGO = '../public/pdf/invoice_uni_regular_vat_nologo.pdf';

  const TEMPLATE_NON_EU = '../public/pdf/invoice_non-eu_uni_v11.pdf';

  const TEMPLATE_NON_EU_NOLOGO = '../public/pdf/invoice_non-eu_uni_v11_nologo.pdf';

  const TEMPLATE_TOBACCO = '../public/pdf/invoice_tobacco_v1.pdf';

  const TEMPLATE_TOBACCO_VAT = '../public/pdf/invoice_tobacco_v1_vat.pdf';

  protected $priceTotal = 0;
  protected $rounding = false;
  protected $deposit = 0;
  protected $currency = 'CZK';
  protected $currencyName = 'Czech Crown';
  protected $vat = 0.21;
  protected $rate = 27;
  protected $productRows = array();
  protected $supplierAddress = array();
  protected $address = array();
  protected $showHs = false;
  protected $shipping = array('cost' => 0);
  protected $delivery = array('cost' => 0);
  protected $packaging = array('cost' => 0);
  protected $pricelist;
  // sleva na fakture - procentni
  protected $discount = 0;
  protected $discountInclude = false;
  // polozkova cena - penezni
  protected $itemDiscount = 0;
  protected $itemDiscountText = '';
  protected $issueDate;
  protected $duzp;
  protected $dueDate = null;
  protected $maskZeroPrices = false;
  protected $language;

  public $showSubtotal = false;
  public $showNetPrices = false;
  public $showNetPricesLabel = true;
  public $showTotalNetPrice = false;
  public $showTotalVatPrice = true;
  public $showVatSummary = true;
  public $showZeroShipping = true;
  public $orderPrice = 0;
  public $no = '';

  protected $deductions = NULL;

  protected $reductionCoef = 1;

  public static function load($source = Meduse_Pdf_Invoice::TEMPLATE_B2B, $revision = NULL) {
    return new self($source, $revision, true);
  }

  public function setCurrency($code, $name) {
    $this->currency = $code;
    $this->currencyName = $name;
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function setPricelist($idPricelist) {
    $this->pricelist = $idPricelist;
  }

  public function getPricelist() {
    return $this->pricelist;
  }

  public function getOrderPrice() {
    return $this->orderPrice;
  }

  public function maskZeroPrices($bool = null) {
    if (is_null($bool)) {
      return $this->maskZeroPrices;
    } else {
      $this->maskZeroPrices = (bool) $bool;
    }
  }

  public function setProductTotal($price) {
    $addDisc = $this->discountInclude ? (100 - $this->discount) / 100 : 1;
    $this->priceTotal = $price * $addDisc * $this->reductionCoef;
  }

  public function setRounding($bool = true) {
    $this->rounding = $bool;
  }

  public function setDeposit($percentage) {
    $this->deposit = (float) $percentage;
  }

  public function setLanguage($lang) {
    $this->language = $lang;
  }

  public function getLanguage() {
    return $this->language;
  }

  public function getProductRows($items) {
    $this->generateProductRows($items);
    return $this->productRows;
  }

  /**
   * Generovni radku obsahu hlavni casti faktury
   * @param type $items - polozky produktu
   * @param type $customItems - dodatecne polozky
   */
  public function generateProductRows($items, $customItems = array()) {
    $allItems = array_merge($items, $customItems);
    $addDisc = $this->discountInclude ? (100 - $this->discount) / 100 : 1;
    foreach ($allItems as $count => $item) {
      $showHs = $this->getShowHs();
      $textline = wordwrap($item['text'], Meduse_Pdf_Invoice::PRODUCT_LINES_SIZE + ($this->showVatSummary || $showHs ? 0 : self::PRODUCT_LINES_SHIFT), "\n", false);
      $textArray = explode("\n", $textline);
      $unitPrice = null;
      $amount = null;
      $price = null;
      if (isset($item['end_price']) && isset($item['amount'])) {
        $unitPrice = ((float) $item['end_price']) * $addDisc;
        $amount = (int) $item['amount'];
        $price = ((float) $item['price']) * $addDisc;
      }
      if ($item['type'] != self::PRODUCT_LINES_TYPE_HEADER) {
        $this->productRows[] = array(
            'id' => isset($item['id']) ? $item['id'] : '',
            'text' => $textArray[0],
            'hs_code' => isset($item['hs_code']) ? $item['hs_code'] : '',
            'amount' => $amount,
            'end_price' => $this->showNetPrices ? $unitPrice : $unitPrice * (1 + $this->vat),
            'price' => $this->showNetPrices ? $price : $price * (1 + $this->vat),
            'price_vat' => $price * (1 + $this->vat),
            'type' => $item['type'],
        );
      }
      /* KATEGORIE BYLY Z FAKTUR ODEBRÁNY
        else {
        $this->productRows[] = array(
        'text'        => $textArray[0],
        'type'		    => $item['type'],
        );
        } */
      for ($it = 1; $it < count($textArray); $it++) {
        $this->productRows[]['text'] = $textArray[$it];
      }
    }
  }

  public function getVat() {
    return $this->vat;
  }

  public function getRate() {
    return (float) $this->rate;
  }

  public function setShowHs($bool) {
    $this->showHs = (bool) $bool;
  }

  public function getShowHs() {
    return $this->showHs;
  }

  public function setVat($vat) {
    $this->vat = $vat;
  }

  public function setRate($rate) {
    $this->rate = $rate;
  }

  public function setDiscount($percentage, $include = false) {
    $this->discount = $percentage;
    $this->discountInclude = $include;
  }

  public function setItemDiscount($price, $text) {
    $this->itemDiscount = $price;
    $this->itemDiscountText = $text;
  }

  public function getDiscount() {
    return $this->isIncludeDiscount() ? 0 : $this->discount;
  }

  public function getDiscountPrice() {
    $rCoef = $this->reductionCoef;
    return $this->isIncludeDiscount() ? 0 : ($this->priceTotal + ($this->itemDiscount * $rCoef)) * $this->discount / 100;
  }

  public function getTotalPrice() {
    $rCoef = $this->reductionCoef;
    $additional = ($this->shipping['cost'] + $this->delivery['cost'] + $this->packaging['cost']) * $rCoef;
    $total_price = $this->priceTotal + ($this->getItemDiscount() * $rCoef);
    if ($this->getDiscount() && !$this->isIncludeDiscount()) {
      $total_price = $total_price * ((100 - $this->getDiscount()) / 100);
    }
    return $total_price + $additional;
  }

  public function getDepositPercentage() {
    return (float) $this->deposit;
  }

  public function getDeposit() {
    $deposit = $this->deposit * $this->getTotalPrice() / 100;
    $deposit = $this->showVatSummary ? $deposit * (1 + $this->getVat()) : $deposit;
    return $deposit;
  }

  public function getItemDiscountText() {
    return $this->itemDiscountText;
  }

  public function getItemDiscount() {
      return $this->itemDiscount;
  }

  public function isIncludeDiscount() {
    return $this->discountInclude;
  }

  public function setIssueDate($dateDb) {
    $date = new Meduse_Date($dateDb, 'Y-m-d');
    $format = $this->language == 'cs' ? 'j.n.Y' : 'j. M Y';
    $this->issueDate = $date->toString($format, null, $this->language);
  }

  public function getIssueDate() {
    return $this->issueDate;
  }

  public function setDUZP($dateDb) {
    $date = new Meduse_Date($dateDb, 'Y-m-d');
    $format = $this->language == 'cs' ? 'j.n.Y' : 'j. M Y';
    $this->duzp = $date->toString($format, null, $this->language);
  }

  public function getDUZP() {
    return $this->duzp;
  }


  public function setDueDate($dateDb) {
    $date = is_null($dateDb) ? null : new Meduse_Date($dateDb, 'Y-m-d');
    $format = $this->language == 'cs' ? 'j.n.Y' : 'j. M Y';
    $this->dueDate = is_null($date) ? null : $date->toString($format, null, $this->language);
  }

  public function getDueDate() {
    return $this->dueDate;
  }

  public function setAddress($billing, $delivery) {
    $this->address = array(
        'billing' => $billing, 'delivery' => $delivery,
    );
  }

  public function setShipping($data) {
    $this->shipping = $data;
  }

  public function setDelivery($data) {
    $this->delivery = $data;
  }

  public function setPackaging($data) {
    $this->packaging = $data;
  }

  public function getProductRowsCount() {
    return count($this->productRows);
  }

  public function getPagesCount() {
    $rows = $this->getProductRowsCount();
    if ($rows <= Meduse_Pdf_Invoice::PRODUCT_LINES_SINGLE_PAGE) {
      return 1;
    }
    $rest = $rows - (Meduse_Pdf_Invoice::PRODUCT_LINES_FIRST_PAGE + Meduse_Pdf_Invoice::PRODUCT_LINES_LAST_PAGE);
    if ($rest <= 0) {
      return 2;
    }
    $count = 2;
    while ($rest > 0) {
      $rest -= Meduse_Pdf_Invoice::PRODUCT_LINES_MIDDLE_PAGE;
      $count++;
    }
    return $count;
  }

  public function prepare($items, $customItems = null) {
    $this->generateProductRows($items, $customItems);
    $page_count = $this->getPagesCount();
    if ($page_count == 1) {
      array_pop($this->pages);
      array_pop($this->pages);
      array_pop($this->pages);
    } else {
      $this->pages[] = new Zend_Pdf_Page($this->pages[1]);
      for ($it = 2; $it < $page_count; $it++) {
        $this->pages[] = new Zend_Pdf_Page($this->pages[2]);
      }
      $this->pages[] = new Zend_Pdf_Page($this->pages[3]);
      array_shift($this->pages);
      array_shift($this->pages);
      array_shift($this->pages);
      array_shift($this->pages);
    }
    $this->addPagination();
  }

  public static function preprocessText(&$text, $size = self::PRODUCT_LINES_SIZE) {
    $total = 0;
    $textArray = array();
    foreach ($text as $count => $line) {
      $textline = wordwrap($line, $size, "\n", false) . "\n";
      $textArray[$count] = explode("\n", $textline);
      $total += count($textArray[$count]);
    }
    $text = $textArray;
    return $total;
  }

  /**
   * nastaveni stylu pro adresu
   */
  public static function getAddressStyle() {
    $style = new Zend_Pdf_Style();
    $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/../library/Meduse/Pdf/Fonts/arial.ttf');
    $style->setFont($font, 8);
    $style->setFontSize(8);
    $style->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    return $style;
  }

  public static function getFooterStyle() {
    $style = new Zend_Pdf_Style();
    $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/../library/Meduse/Pdf/Fonts/arial.ttf');
    $style->setFont($font, 6);
    $style->setFontSize(6);
    $style->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    return $style;
  }

  public static function getVatNumberStyle() {
    $style = new Zend_Pdf_Style();
    $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/../library/Meduse/Pdf/Fonts/arial.ttf');
    $style->setFont($font, 8);
    $style->setFontSize(8);
    $style->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    return $style;
  }

  public static function getPhoneStyle() {
    $style = new Zend_Pdf_Style();
    $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/../library/Meduse/Pdf/Fonts/arial.ttf');
    $style->setFont($font, 8);
    $style->setFontSize(8);
    $style->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    return $style;
  }

  public static function getItemsHeaderStyle() {
    $style = new Zend_Pdf_Style();
    $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/../library/Meduse/Pdf/Fonts/arialbd.ttf');
    $style->setFont($font, 8);
    $style->setFontSize(8);
    $style->setLineColor(new Zend_Pdf_Color_GrayScale(0));
    $style->setFillColor(new Zend_Pdf_Color_GrayScale(0.9));
    $style->setLineWidth(0.125);
    return $style;
  }

  /**
   * vykresleni adresy na dane souradnice
   * @param type $id_address
   * @param type $x pocatecni x souradnice
   * @param type $y pocatecni y souradnice (pocitano ze spodu stranky)
   */
  public function fillAddress($text, $coords, $lineHeight = 2) {
    $this->fillText($text['company'], $coords, Meduse_Pdf::TEXT_ALIGN_LEFT, $lineHeight);
    $this->_draw($text['street'], null, null, null, $lineHeight);
    $this->_draw($text['zip'], null, null, null, $lineHeight);
    $this->_draw($text['country_name'], null, null, null, $lineHeight);
    unset($text['company']);
    unset($text['street']);
    unset($text['zip']);
    unset($text['country_name']);
    if (!empty($text)) {
      foreach ($text as $line) {
        $this->_draw($line, null, null, null, $lineHeight);
      }
    }
  }

  public function fillVatNumber($number, $coords) {
    $this->_draw($number, Meduse_Pdf_Invoice::getVatNumberStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_LEFT);
  }

  public function fillIDNumber($number, $coords) {
    $this->_draw($number, Meduse_Pdf_Invoice::getVatNumberStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_LEFT);
  }

  public function fillPhone($number, $coords) {
    $this->_draw($number, Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_LEFT);
  }

  public function fillDate($coords) {
    $dateStr = $this->issueDate;
    if ($this->duzp) {
      $dateStr .= ' / ' . $this->duzp;
    }
    if ($this->dueDate) {
      $dateStr .= ' / ' . $this->dueDate;
    }
    $this->_draw($dateStr, Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_LEFT);
  }

  public function setLastPage() {
    $this->setPageNumber($this->getPagesCount() - 1);
  }

  public function setFirstPage() {
    $this->setPageNumber(0);
  }

  public function fillCustomerAddress($texts, $coords1, $coords2, $coords3 = null) {
    $this->setFirstPage();

    $lines = 5;

    if (empty($this->address['billing']['vat']) && empty($this->address['billing']['reg_no'])) {
      unset($texts['reg_no']);
      unset($texts['vat_no']);
      $lines -= 1;
    }

    if (empty($this->address['billing']['person'])) {
      $lines--;
    }

    if ($coords3) {
      $streetLines = max(array(
          count($this->address['delivery']['street']), count($this->address['billing']['street'])));
      $zipLines = max(array(
          count($this->address['delivery']['zip']), count($this->address['billing']['zip'])));
    }
    else {
      $streetLines = count($this->address['billing']['street']);
      $zipLines = count($this->address['billing']['zip']);
    }

    $lines += $streetLines + $zipLines;
    if (isset($this->address['billing']['seed_id'])) {
      $lines++;
    }
    if (isset($this->address['billing']['additional_row']) || isset($this->address['delivery']['additional_row'])) {
      $lines++;
    }

    $lineHeight = self::LINE_HEIGHT_ADDRESS * (6 / $lines);

    // hlavicka
    $texts['address'] = array($texts['address']);
    for ($i = 1; $i < $streetLines; $i++) {
      $texts['address'][] = '';
    }
    $texts['city_zip'] = array($texts['city_zip']);
    for ($i = 1; $i < $zipLines; $i++) {
      $texts['city_zip'][] = '';
    }
    $this->fillTextBold($texts['company'], $coords1, Meduse_Pdf::TEXT_ALIGN_LEFT, $lineHeight);
    if (!empty($this->address['billing']['person'])) {
      $this->fillTextBold($texts['person'], NULL, Meduse_Pdf::TEXT_ALIGN_LEFT, $lineHeight);
    }
    $this->_draw($texts['address'], null, null, null, $lineHeight);
    $this->_draw($texts['city_zip'], null, null, null, $lineHeight);
    $this->_draw($texts['country'], null, null, null, $lineHeight);
    $this->_draw($texts['phone'], null, null, null, $lineHeight);

    $nos = array();
    $ts = array();
    if (isset($this->address['billing']['reg_no']) && !empty($this->address['billing']['reg_no'])) {
      $nos[] = $this->address['billing']['reg_no'];
      $ts[] = $texts['reg_no'];
    }
    if (isset($this->address['billing']['vat']) && !empty($this->address['billing']['vat'])) {
      $nos[] = $this->address['billing']['vat'];
      $ts[] = $texts['vat_no'];
    }

    if (isset($this->address['billing']['additional_row'])) {
      $this->_draw($texts['additional_row'], null, null, null, $lineHeight);
    }

    $this->_draw(implode(' / ', $ts), null, null, null, $lineHeight);
    if (isset($this->address['billing']['seed_id'])) {
      $this->_draw($texts['seed_id'], null, null, null, $lineHeight);
    }

    // fakturacni
    for ($i = count($this->address['billing']['street']); $i < $streetLines; $i++) {
      $this->address['billing']['street'][] = '';
    }
    for ($i = count($this->address['billing']['zip']); $i < $zipLines; $i++) {
      $this->address['billing']['zip'][] = '';
    }
    $this->fillText($this->address['billing']['company'], $coords2, Meduse_Pdf::TEXT_ALIGN_LEFT, $lineHeight);
    if (!empty($this->address['billing']['person'])) {
      $this->fillText($this->address['billing']['person'], NULL, Meduse_Pdf::TEXT_ALIGN_LEFT, $lineHeight);
    }
    $this->_draw($this->address['billing']['street'], null, null, null, $lineHeight);
    $this->_draw($this->address['billing']['zip'], null, null, null, $lineHeight);
    $this->_draw($this->address['billing']['country_name'], null, null, null, $lineHeight);
    $this->_draw($this->address['billing']['phone'], null, null, null, $lineHeight);
    if (isset($this->address['billing']['additional_row'])) {
      $this->_draw($this->address['billing']['additional_row'], null, null, null, $lineHeight);
    }
    $this->_draw(implode(' / ', $nos), null, null, null, $lineHeight);
    if (isset($this->address['billing']['seed_id'])) {
      $this->_draw($this->address['billing']['seed_id'], null, null, null, $lineHeight);
    }
    // dodaci
    if ($coords3) {
      for ($i = count($this->address['delivery']['street']); $i < $streetLines; $i++) {
        $this->address['delivery']['street'][] = '';
      }
      for ($i = count($this->address['delivery']['zip']); $i < $zipLines; $i++) {
        $this->address['delivery']['zip'][] = '';
      }

      $this->fillText($this->address['delivery']['company'], $coords3, Meduse_Pdf::TEXT_ALIGN_LEFT, $lineHeight);
      if (!empty($this->address['delivery']['person'])) {
        $this->fillText($this->address['delivery']['person'], NULL, Meduse_Pdf::TEXT_ALIGN_LEFT, $lineHeight);
      }
      $this->_draw($this->address['delivery']['street'], null, null, null, $lineHeight);
      $this->_draw($this->address['delivery']['zip'], null, null, null, $lineHeight);
      $this->_draw($this->address['delivery']['country_name'], null, null, null, $lineHeight);
      if (isset($this->address['delivery']['phone'])) {
        $this->_draw($this->address['delivery']['phone'], null, null, null, $lineHeight);
      }
      if (isset($this->address['delivery']['additional_row'])) {
        $this->_draw($this->address['delivery']['additional_row'], null, null, null, $lineHeight);
      }
    }
  }

  public function fillShipping($texts, $coords1, $coords2) {
    if (isset($this->shipping['show']) && !$this->shipping['show']) {
      return;
    }
    $this->setLastPage();
    $text = $this->delivery['text'] ? $texts[1] : $texts[0];
    $text = str_replace('%d', $this->delivery['text'], $text);
    $text = str_replace('%c', $this->shipping['text'], $text);
    $this->_draw($text, Meduse_Pdf::getDefaultStyle(), $coords1, Meduse_Pdf::TEXT_ALIGN_LEFT);
    $costs = $this->showNetPrices ?
            $this->shipping['cost'] + $this->delivery['cost'] : ($this->shipping['cost'] + $this->delivery['cost']) * (1 + $this->vat);
    $costs = $this->currency == 'CZK' ? $costs * $this->rate : $costs;
    $this->_draw(sprintf("%01.2f %s", $costs, Table_Invoices::getCurrencySign($this->currency, $this->language)), Meduse_Pdf::getDefaultStyle(), $coords2, Meduse_Pdf::TEXT_ALIGN_RIGHT);
  }

  public function fillPackaging($text, $coords1, $coords2) {
    $this->setFirstPage();
    $this->_draw($text, Meduse_Pdf::getBoldStyle(), $coords1, Meduse_Pdf::TEXT_ALIGN_LEFT, 1.2);
    $textArray = array_merge(array($this->packaging['text']), array(
        $this->packaging['weight'] . ' ' . $this->packaging["weight_unit"]));
    $this->_draw($textArray, Meduse_Pdf::getDefaultStyle(), $coords2, Meduse_Pdf::TEXT_ALIGN_RIGHT, 2.5);
  }

  public function fillPackagingCost($text, $coords1, $coords2) {
    // vypiseme, jen kdyz je packaging nenulovy
    if ($this->packaging['cost']) {
      $rCoef = $this->reductionCoef;
      $this->setLastPage();
      $this->_draw($text, Meduse_Pdf::getDefaultStyle(), $coords1, Meduse_Pdf::TEXT_ALIGN_LEFT);
      $costs = $this->showNetPrices ? $this->packaging['cost'] : $this->packaging['cost'] * (1 + $this->vat);
      $costs = ($this->currency == 'CZK' ? $costs * $this->rate : $costs) * $rCoef;
      $this->_draw(sprintf("%01.2f %s", $costs, Table_Invoices::getCurrencySign($this->currency, $this->language)), Meduse_Pdf::getDefaultStyle(), $coords2, Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
  }

  public function fillAdditionalDiscount($text, $coords1, $coords2, $coords3) {
    $this->setLastPage();
    $rCoef = $this->reductionCoef;
    $discount = $this->discountType == self::DISCOUNT_TYPE_PERCENT ?
            - $this->priceTotal * (($this->discount) / 100) : $this->discount;
    $discount = $this->currency == 'CZK' ? $discount * $this->rate : $discount;
    if ($discount < 0) {
      $this->_draw($text, Meduse_Pdf::getDefaultStyle(), $coords1, Meduse_Pdf::TEXT_ALIGN_LEFT);
      $this->_draw(sprintf("%01.2f %s", $this->showNetPrices ?
                              $discount * $rCoef: $discount * (1 + $this->vat) * $rCoef, Table_Invoices::getCurrencySign($this->currency, $this->language)), Meduse_Pdf::getDefaultStyle(), $coords3, Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
  }

  public function fillNonItemsPrice($header, $coords1, $coords2, $coords3) {
    $rCoef = $this->reductionCoef;
    $this->setLastPage();
    if (!$this->showVatSummary) {
      $coords2 = $coords3;
    }
    $item_discount = ($this->currency == 'CZK' ?
            $this->getItemDiscount() * $this->getRate() : $this->getItemDiscount()) * $rCoef;
    $additional_discount = $this->currency == 'CZK' ?
            $this->getDiscountPrice() * $this->getRate() : $this->getDiscountPrice();
    $shipping = $this->showNetPrices ?
            $this->shipping['cost'] : $this->shipping['cost'] + $this->delivery['cost'] * (1 + $this->getVat());
    $shipping = ($this->currency == 'CZK' ? $shipping * $this->getRate() : $shipping) * $rCoef;

    $show_shipping = $this->shipping['show'] && ($shipping || $this->showZeroShipping);

    // hlavicka

    if (!$this->showSubtotal) {
      unset($header['subtotal']);
    }
    if (!$item_discount) {
      unset($header['item_discount']);
    }
    if ($show_shipping) {
      $header['shipping'] = str_replace('%c', $this->shipping['text'], $header['shipping']);
    } else {
      unset($header['shipping']);
    }
    if (!$additional_discount) {
      unset($header['additional_discount']);
    }
    $this->_draw($header, Meduse_Pdf::getDefaultStyle(), $coords1, Meduse_Pdf::TEXT_ALIGN_LEFT);

    $this->setCoords($coords2);
    if ($this->showSubtotal) {
      $subtotal = $this->priceTotal;
      $coords = $this->getCoords();
      $this->_draw(
        Meduse_Currency::format($subtotal, Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
        Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
    if ($item_discount) {
      $coords = $this->getCoords();
      $this->_draw(
        Meduse_Currency::format($item_discount, Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
        Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
    if ($show_shipping) {
      $coords = $this->getCoords();
      $this->_draw(
        Meduse_Currency::format($shipping, Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
        Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
    if ($additional_discount) {
      $coords = $this->getCoords();
      $this->_draw(
        Meduse_Currency::format(-$additional_discount, Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
        Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }

    if ($this->showVatSummary) {
      $this->setCoords($coords3);
      if ($this->showSubtotal) {
        $subtotal = $this->priceTotal;
        $coords = $this->getCoords();
        $this->_draw(
          Meduse_Currency::format($subtotal * (1 + $this->getVat()), Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
          Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if ($item_discount) {
        $coords = $this->getCoords();
        $this->_draw(
          Meduse_Currency::format($item_discount * (1 + $this->getVat()), Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
          Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if ($show_shipping) {
        $coords = $this->getCoords();
        $this->_draw(
          Meduse_Currency::format($shipping * (1 + $this->getVat()), Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
          Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if ($additional_discount) {
        $coords = $this->getCoords();
        $this->_draw(
          Meduse_Currency::format(-$additional_discount * (1 + $this->getVat()), Table_Invoices::getCurrencySign($this->currency, $this->language), 2, FALSE),
          Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
    }


  }

  public function fillTotalPrice($header, $coords1, $coords2, $coords3) {

    if ($this->getDepositPercentage() == 0) {
      unset($header['deposit']);
    }
    if (!$this->showTotalNetPrice) {
      unset($header['total']);
    }
    if (!$this->showVatSummary) {
      unset($header['vat_tax']);
    }
    if (!$this->showTotalVatPrice) {
      if ($this->deductions) {
        $header['total_vat'] = $header['remains'];
      }
      else {
        unset($header['total_vat']);
      }
    }
    if ($this->currency == Table_Invoices::CURRENCY_CZK) {
      unset($header['currency']);
      $this->setRounding(TRUE);
    } else {
      $this->setRounding(FALSE);
    }

    if (!$this->rounding || $this->getDepositPercentage()) {
      unset($header['rounding']);
    } elseif (!$this->showVatSummary) {
      $rounding_header = $header['rounding'];
      unset($header['rounding']);
      array_unshift($header, $rounding_header);
    }

    if (!$this->deductions) {
      unset($header['deduction']);
      unset($header['deduction_vat']);
    }
    elseif (!$this->showVatSummary) {
      unset($header['deduction_vat']);
    }
    unset($header['remains']);
    $this->setLastPage();
    $coords = $coords1;
    $this->_draw($header, Meduse_Pdf::getDefaultStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_LEFT);
    $total = $this->getTotalPrice();

    $deduction = $this->getDeductionTotal();

    if ($this->currency == Table_Invoices::CURRENCY_CZK) {
      $this->setCoords($coords3);

      // CAST CZK
      // Vypocet
      // celkova cena bez dane
      if ($this->showTotalNetPrice) {
        $price_net = $total * $this->rate;
        if (!$this->showTotalVatPrice && $this->rounding) {
          $rounding = (round($price_net) - $price_net);
          $price_net = round($price_net);
        }
        $this->orderPrice = $price_net;
      }

      // rozpis dane
      if ($this->showVatSummary) {
        $vat = $total * $this->vat * $this->rate;
      }

      // celkova cena s dani
      if ($this->showTotalVatPrice) {
        // vypočítání ceny
        $price_vat = $total * (1 + $this->vat) * $this->rate;
        $rounding = 0;
        if ($this->rounding) {
          $rounding = (round($price_vat) - $price_vat);
          $price_net = round($price_net, 2) + $rounding / (1 + $this->vat);
          $vat = round($vat, 2) + $rounding * $this->vat / (1 + $this->vat);
          $price_vat = round($price_net + $vat);
        }
        $this->orderPrice = $price_vat;
      }

      // zaloha
      if ($this->getDeposit()) {
        $deposit = $this->rounding ? round($this->getDeposit() * $this->rate) : $this->getDeposit() * $this->rate;
      }

      // VYPIS
      /*
      if ($this->rounding && !$this->showVatSummary && !$this->getDepositPercentage()) {
        $number = number_format($rounding, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      */
      if ($this->showTotalNetPrice) {
        $number = number_format($price_net, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), $this->showTotalVatPrice || $this->getDeposit() ? Meduse_Pdf::getDefaultStyle() : Meduse_Pdf::getBoldStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if ($this->showVatSummary) {
        $number = number_format($vat, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }

      if ($deduction) {
        if ($this->showVatSummary) {
          $number = number_format($deduction['total'] * $this->rate, 2, ".", " ");
          $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
          $number = number_format($deduction['vat'] * $this->rate, 2, ".", " ");
          $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        }
        else {
          $number = number_format($deduction['total_vat'] * $this->rate, 2, ".", " ");
          $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        }
      }
      /*
      if ($this->rounding && $this->showVatSummary && !$this->getDepositPercentage()) {
        $number = number_format($rounding, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      */
      if ($this->showTotalVatPrice) {
        if ($deduction) {
          $price_vat += $deduction['total_vat'] * $this->rate;
        }
        $number = number_format($price_vat, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), ($this->getDeposit()) ? Meduse_Pdf::getDefaultStyle() : Meduse_Pdf::getBoldStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if ($this->getDeposit()) {
        $number = number_format($deposit, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getBoldStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if ($this->currency == Table_Invoices::CURRENCY_EUR) {
        $this->_draw(Table_Invoices::CURRENCY_CZK, Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
    }


    // CAST EUR
    if ($this->currency == Table_Invoices::CURRENCY_EUR) {
      $this->setCoords($coords3);


      // VYPOCET
      // celkova cena bez dane
      if ($this->showTotalNetPrice) {
        $this->orderPrice = $total;
        if (!$this->showTotalVatPrice && $this->rounding) {
          $this->orderPrice = round($total, 2);
          $rounding = $this->orderPrice - $total;
        }
        $price_net = $this->orderPrice;
      }

      // rozpis dane
      if ($this->showVatSummary) {
        $vat = $total * $this->vat;
      }
      // celkova cena s dani
      if ($this->showTotalVatPrice) {
        if ($this->rounding) {
          $this->orderPrice = round($total * ($this->vat + 1), 2);
          $rounding = $this->orderPrice - (round($vat, 2) + round($price_net, 2)); //$this->orderPrice - ($total * ($this->vat + 1));
          $price_net += $rounding / (1 + $this->vat);;
          $vat += $rounding * $this->vat / (1 + $this->vat);;
          $this->orderPrice = round($price_net + $vat);
        }
        else {
          $this->orderPrice = $total * ($this->vat + 1);
          // $price_vat = $this->orderPrice;
          $rounding = 0;
        }
      }

      // zaloha
      if ($this->getDeposit()) {
        $deposit = $this->rounding ? round($this->getDeposit()) : $this->getDeposit();
      }

      // VYPIS
      // přepočet na CZK - inicializace
      $czk_equiv = array();

      // zaokrouhleni
      /*
      if ($this->rounding && !$this->showVatSummary && !$this->getDepositPercentage()) {
        $number = number_format($rounding, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_EUR, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        $czk_equiv['rounding'] = number_format($rounding * $this->rate, 2, ".", " ");
      }
      */
      // celkova cena bez dane
      if ($this->showTotalNetPrice) {
        $number = number_format($price_net, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_EUR, $this->language), $this->showTotalVatPrice || $this->getDeposit() ?
                        Meduse_Pdf::getDefaultStyle() : Meduse_Pdf::getBoldStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        $czk_equiv['totalNet'] = number_format(round($price_net,2) * $this->rate, 2, ".", " ");
      }

      // rozpis dane
      if ($this->showVatSummary) {
        $number = number_format($vat, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_EUR, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        $czk_equiv['summaryVat'] = number_format(round($vat,2) * $this->rate, 2, ".", " ");
      }
      if ($deduction) {
        if ($this->showVatSummary) {
          $number = number_format($deduction['total'], 2, ".", " ");
          $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_EUR, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
          $number = number_format($deduction['vat'], 2, ".", " ");
          $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_EUR, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        }
        else {
          $number = number_format($deduction['total_vat'], 2, ".", " ");
          $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_EUR, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        }
      }
      /*
      if ($this->rounding && $this->showVatSummary && !$this->getDepositPercentage()) {
        $number = number_format($rounding, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_EUR, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        $czk_equiv['rounding2'] = number_format(round($rounding, 2) * $this->rate, 2, ".", " ");
      }
      */
      // celkova cena s dani
      if ($this->showTotalVatPrice || $deduction) {
        $number = number_format(round($this->orderPrice + $deduction['total_vat'], 2) , 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_EUR, $this->language), $this->getDeposit() ? Meduse_Pdf::getDefaultStyle() : Meduse_Pdf::getBoldStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        $czk_equiv['totalVat'] = number_format(round($this->orderPrice + $deduction['total_vat'], 2) * $this->rate, 2, ".", " ");
      }
      if ($this->getDeposit()) {
        $number = number_format($deposit, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_EUR, $this->language), Meduse_Pdf::getBoldStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        $czk_equiv['deposit'] = number_format(round($deposit, 2) * $this->rate, 2, ".", " ");
      }

      // mena
      if ($this->currency == Table_Invoices::CURRENCY_EUR) {
        $this->_draw(Table_Invoices::CURRENCY_EUR, Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }

      //VYPIS (PREPOCET) V CZK
      $this->setCoords($coords2);
      // VYPIS
      // zaokrouhleni
      if ($this->rounding && !$this->showVatSummary && !$this->getDepositPercentage()) {
        $this->_draw($czk_equiv['rounding'] . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }

      // celkova cena bez dane
      if ($this->showTotalNetPrice) {
        // $number = number_format($price_net, 2, ".", " ");
        $this->_draw($czk_equiv['totalNet'] . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), $this->showTotalVatPrice || $this->getDeposit() ?
                        Meduse_Pdf::getDefaultStyle() : Meduse_Pdf::getBoldStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }

      // rozpis dane
      if ($this->showVatSummary) {
        $this->_draw($czk_equiv['summaryVat'] . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }

      if ($deduction) {
        if ($this->showVatSummary) {
          $number = number_format(round($deduction['total'], 2) * $this->rate, 2, ".", " ");
          $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
          $number = number_format(round($deduction['vat'], 2) * $this->rate, 2, ".", " ");
          $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        }
        else {
          $number = number_format(round($deduction['total_vat'], 2) * $this->rate, 2, ".", " ");
          $this->_draw($number . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
        }
      }

      /*
      if ($this->rounding && $this->showVatSummary && !$this->getDepositPercentage()) {
        $this->_draw($czk_equiv['rounding2'] . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      */
      // celkova cena s dani
      if ($this->showTotalVatPrice || $deduction) {
        $this->_draw($czk_equiv['totalVat'] . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), $this->getDeposit() ? Meduse_Pdf::getDefaultStyle() : Meduse_Pdf::getBoldStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if ($this->getDeposit()) {
        // $number = number_format($deposit, 2, ".", " ");
        $this->_draw($czk_equiv['deposit'] . ' ' . Table_Invoices::getCurrencySign(Table_Invoices::CURRENCY_CZK, $this->language), Meduse_Pdf::getBoldStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }


      $this->_draw(Table_Invoices::CURRENCY_CZK, Meduse_Pdf::getDefaultStyle(), $this->getCoords(), Meduse_Pdf::TEXT_ALIGN_RIGHT);
    }
  }

  public function fillNotice($text, $coords) {
    $this->setLastPage();
    $textline = wordwrap($text, Meduse_Pdf_Invoice::NOTICE_LINES_SIZE, "\n", false);
    $textArray = explode("\n", $textline);
    $this->_draw($textArray, Meduse_Pdf::getSmallStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_LEFT, 1.2);
  }

  public function fillFooter($text, $coords = null) {
    if (is_null($coords)) {
      $coords = array(Meduse_Pdf_Invoice::LINE_WIDTH / 2, 0);
    }
    $this->setLastPage();
    $this->_draw(
            $text, Meduse_Pdf_Invoice::getFooterStyle(), $coords, Meduse_Pdf::TEXT_ALIGN_CENTER
    );
  }

  protected function _getPageIndex($count) {
    $count++;
    if ($this->getPagesCount() == 1 || $count <= Meduse_Pdf_Invoice::PRODUCT_LINES_FIRST_PAGE) {
      return 0;
    }
    if ($count > Meduse_Pdf_Invoice::PRODUCT_LINES_FIRST_PAGE + (($this->getPagesCount() - 2) * Meduse_Pdf_Invoice::PRODUCT_LINES_MIDDLE_PAGE)) {
      return $this->getPagesCount() - 1;
    }
    return floor(($count - (Meduse_Pdf_Invoice::PRODUCT_LINES_FIRST_PAGE)) / Meduse_Pdf_Invoice::PRODUCT_LINES_MIDDLE_PAGE) + 1;
  }

  public function fillItemsHeader($header, $coords, $coords2, $offsetX, $lineHeight = 1.2) {
    $pagesCount = $this->getPagesCount();
    for ($p = 0; $p < $pagesCount; $p++) {
      $this->setPageNumber($p);
      $height = $this->getLineHeightPixels($lineHeight);
      $cnt = 0;
      foreach ($header as $key => $item) {
        if ($key === 'hs' && !$this->showHs) {
          $cnt++;
          continue;
        }
        $textArray = explode("\n", $item);
        $offsetY = count($textArray) * $height / 2;
        // prvni strana
        if ($p == 0) {
          $this->_draw($textArray, Meduse_Pdf::getBoldStyle(), array($coords[0] + $offsetX[$cnt], $coords[1] + $offsetY), Meduse_Pdf::TEXT_ALIGN_CENTER, $lineHeight);
          // dalsi strana
        } else {
          $this->_draw($textArray, Meduse_Pdf::getBoldStyle(), array($coords2[0] + $offsetX[$cnt], $coords2[1] + $offsetY), Meduse_Pdf::TEXT_ALIGN_CENTER, $lineHeight);
        }
        $cnt++;
      }
    }
  }

  public function fillItems($coords, $coords2, $offset) {

    $rate = ($this->currency == Table_Invoices::CURRENCY_CZK) ? $this->rate : 1;
    $rCoef = $this->reductionCoef;

    $this->setCoords($coords);
    $this->setPageNumber(0);

    // pokud nevypisujeme radkovou sumu s dani, vynechame
    $shift = $this->showVatSummary ? 0 : $offset[6] - $offset[5];

    foreach ($this->productRows as $count => $row) {
      $new_page_index = $this->_getPageIndex($count);
      if ($new_page_index != $this->getPageNumber()) {
        $this->setPageNumber($new_page_index);
        $this->setCoords($coords2);
      }
      $newCoords = $this->getCoords();

      if (!isset($row['type'])) {
        $style = Meduse_Pdf::getDefaultStyle();
      } elseif ($row['type'] == Meduse_Pdf_Invoice::PRODUCT_LINES_TYPE_HEADER) {
        continue;
      } else {
        $style = Meduse_Pdf::getDefaultStyle();
      }
      if (isset($row['id']))
        $this->_draw($row['id'], $style, array($coords[0] + $offset[0], $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_CENTER);
      $this->_draw($row['text'], $style, array($coords[0] + $offset[1], $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_LEFT);
      if (isset($row['hs_code']) && $this->getShowHs())
        $this->_draw($row['hs_code'], $style, array($coords[0] + $offset[2] + $shift, $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_CENTER);
      if (isset($row['amount']))
        $this->_draw($row['amount'], $style, array($coords[0] + $offset[3] + $shift, $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      if (isset($row['end_price'])) {
        $number = number_format($row['end_price'] * $rate * $rCoef, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign($this->currency, $this->language), $style, array($coords[0] + $offset[4] + $shift, $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if (isset($row['price'])) {
        $number = number_format($row['price'] * $rate* $rCoef, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign($this->currency, $this->language), $style, array($coords[0] + $offset[5] + $shift, $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }
      if ($this->showVatSummary && isset($row['price_vat'])) {
        $number = number_format($row['price_vat'] * $rate * $rCoef, 2, ".", " ");
        $this->_draw($number . ' ' . Table_Invoices::getCurrencySign($this->currency, $this->language), $style, array($coords[0] + $offset[6], $newCoords[1]), Meduse_Pdf::TEXT_ALIGN_RIGHT);
      }

      $this->pages[$this->_page]->drawLine($coords[0] - 3, $newCoords[1] - 3 , $coords[0] + $offset[6] + 4, $newCoords[1] - 3);
    }
  }

  /**
   * Strankovani
   */
  public function addPagination() {
    $count = $this->getPagesCount();
    for ($i = 1; $i <= $count; $i++) {
      $this->setPageNumber($i - 1);
      $text = (!empty($this->no) ?  $this->no . ' – ' : '') . $i . '/' . $count;
      $this->_draw($text, Meduse_Pdf::getDefaultStyle(), array(300, 20), Meduse_Pdf::TEXT_ALIGN_CENTER);
    }
    $this->setPageNumber(0);
  }


  public function setNo($no) {
    $this->no = $no;
  }

  public function setDeductions($data) {
    $this->deductions = $data;
  }

  public function getDeductionTotal(): ?array
  {
    if (!$this->deductions) {
      return NULL;
    }
    $total = 0;
    foreach ($this->deductions as $d) {
      $total += ($d['currency'] === Table_Invoices::CURRENCY_CZK ? ($d['deducted'] / $this->rate) : $d['deducted']);
    }
    $vat = $this->getVat();
    return array(
      'total' => -($total/(1+$vat)),
      'vat'   => -($total/(1+$vat))*$vat,
      'total_vat' => -$total,
    );
  }

  public function setReduction($reduction) {
    $this->reductionCoef = (100 - $reduction) / 100;
  }

}
