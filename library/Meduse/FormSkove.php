<?php

class Meduse_FormSkove extends Meduse_FormBootstrap {
    public function init()
    {
        parent::init();

        // set default decorator
        $this->clearDecorators();
        $this->addDecorator('FormElements')
             ->addDecorator(array('Dashed'=>'HtmlTag'), array('tag'=>'div', 'class'=>'dashed-outline'))
             ->addDecorator('Form')
             ->addDecorator('Fieldset');
        if(!$this->_isModalForm) { // is NOT modal form css style
             $this->addDecorator('HtmlTag', array('tag' => 'div','class' => 'well'));
        }

        $this->_decorators($this->getElements());
    }
}