<?php
class Meduse_ipChecker {

	public static function checkIP($userName, $ip) {
		$db = Zend_Db_Table::getDefaultAdapter();
		$userArr = $db->fetchAssoc("SELECT * FROM users WHERE username = :uname", array('uname'=> $userName));
        if(is_array($userArr) && count($userArr) > 0) {
            $user = array_shift($userArr);
            if($user['check_ip'] == 0) {
                return true; //kontrola IP neni vyzadovana => IP je vporadku
            } else {
                //kontrola IP
                $row = $db->fetchRow("SELECT * FROM users_allowed_ip WHERE id_users = :user AND confirmed = 1 AND ip = :ip", array('user'=>$user['id'],'ip'=>$ip));
                if($row){
                    return true;
                }
            }
        }
		return false;
	}

	public static function insertIP($userName, $ip) {
		$db = Zend_Db_Table::getDefaultAdapter();
		$userId = $db->fetchOne("SELECT id FROM users WHERE username = :name", array('name'=> $userName));
		$row = $db->fetchRow("SELECT * FROM users_allowed_ip WHERE id_users = :user AND ip = :ip", array('user'=>$userId,'ip'=>$ip));
		if(!$row){
			$hash = sha1(time()+$userId);
			$db->insert('users_allowed_ip', array(
				'id_users'  => $userId,
				'ip'        => $ip,
				'hash'      => $hash, 
				'confirmed' => 0
			));
			return $hash;
		} else return $row['hash'];
	}

	public static function allowIP($hash) {
		$db = Zend_Db_Table::getDefaultAdapter();
		$row = $db->fetchRow("SELECT * FROM users_allowed_ip WHERE hash = :h", array('h'=>$hash));
		if($row){
			$db->update('users_allowed_ip',array('confirmed'=>1,'changed'=>new Zend_Db_Expr('NOW()')),"hash = ".$db->quote($hash));
			return true;
		}
		return false;
	}

}