<?php

class Meduse_Validate_PasswordStrength extends Zend_Validate_Abstract {

  const LENGTH = 'length';

  const LETTERS = 'letters';

  const DIGIT = 'digit';

  const CHARSET = 'charset';

  protected $_messageTemplates = [
    self::LENGTH => "'%value%' je kratší než 10 znaků",
    self::LETTERS => "'%value%' neobsahuje alespoň 5 písmen",
    self::DIGIT => "'%value%' neobsahuje alespoň 2 číslice",
    self::CHARSET => "'%value%' neobsahuje pouze povolené znaky: A-Za-z0-9_-.,;!@#$&*()+/",
  ];

  public function isValid($value) {
    $this->_setValue($value);

    $isValid = TRUE;

    if (strlen($value) < 10) {
      $this->_error(self::LENGTH);
      $isValid = FALSE;
    }

    if (preg_match_all('/[a-zA-Z]/', $value, $matches) < 5) {
      $this->_error(self::LETTERS);
      $isValid = FALSE;
    }

    if (preg_match_all('/\d/', $value, $matches) < 2) {
      $this->_error(self::DIGIT);
      $isValid = FALSE;
    }
    if (!preg_match('/^[A-Za-z0-9\-\.,;!@#\$&\*\\\(\)_\+\/]+$/', $value)) {
      $this->_error(self::CHARSET);
      $isValid = FALSE;
    }

    return $isValid;
  }

}
