<?php
class Meduse_Validate_DateMin extends Zend_Validate_Abstract
{
    const MIN = 'minimum';

    protected $_messageTemplates = array(
        self::MIN  => "Neodpovídá minimální datum (%min%).",
    );

    protected $_messageVariables = array(
        'min' => '_min'
    );

    protected $_min;
	protected $_format = 'd.m.Y';

    public function __construct($min, $format = 'yyyy-MM-dd') {
        $this->setMin($min);
		$this->setFormat($format);
    }

    public function getMin() {
        return $this->_min;
    }

    public function setMin($min) {
        $this->_min = $min;
        return $this;
    }

    public function setFormat($format) {
        $this->_format = $format;
        return $this;
    }

    public function getFormat() {
        return $this->_format;
    }

    public function isValid($value) {
        $this->_setValue($value);
		$tDate = new Zend_Date($this->_value, $this->_format);
		$mDate = new Zend_Date($this->_min, $this->_format);
        $isValid = true;
		if ($tDate->isEarlier($mDate)) {
            $this->_error(self::MIN);
            $isValid = false;
        }
        return $isValid;
    }
}
