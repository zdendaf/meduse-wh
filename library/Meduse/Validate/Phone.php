<?php

class Meduse_Validate_Phone extends Zend_Validate_Abstract {

  const BAD_FORMAT = 'bad_format';

  protected $_messageTemplates = [
    self::BAD_FORMAT => "Telefonní číslo '%value%' není ve správném formátu.",
  ];

  /**
   * @inheritDoc
   */
  public function isValid($value) {
    $this->_setValue($value);
    $isValid = (bool) preg_match('/^(\+[0-9]+)( [0-9]+)+$/', $value);
    if (!$isValid) {
      $this->_error(self::BAD_FORMAT);
    }
    return $isValid;
  }

}