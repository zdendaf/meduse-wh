<?php
  /**
   * @author Zdenek Filipec <zdendaf@gmail.com>
   */
  class Meduse_Currency {
    
    public static function format($number, $suffix = null, $precision = 2, $nbsp = TRUE) {
      $string = number_format($number, $precision, ".", ($nbsp ? '&nbsp;' : ' '));
      $string .= $suffix ?  ($nbsp ? '&nbsp;' : ' ') . $suffix : '';
      return $string;
    }
    
    public static function parse($value) {
      $float = (float) str_replace(' ', '', strtr((string) $value, ',', '.'));
      return $float;
    }
  }
