<?php
/**
 * Persistent form layer
 *
 * @author Jakub Hantak
 * @since 2013-10-09
 * @uses Zend_Form
 *
 */
class Meduse_FormBootstrap extends Zend_Form
{
    //@var default decorator file name
    protected $_decoratorFileName = 'TwitterBootstrap';
    protected $_isModalForm = false; // enable javascript Modal dialog form style
    protected $passwordShowingIncluded = FALSE;


    /**
     * Init
     * @overrride
     */
    public function init()
    {
        // set translate
        self::setDefaultTranslator($this->_translate());

        // set default decorator
        $this->clearDecorators();
        $this->addDecorator('FormElements')
             ->addDecorator(array('Dashed'=>'HtmlTag'), array('tag'=>'div', 'class'=>'dashed-outline'))
             ->addDecorator('Form')
             ->addDecorator('Fieldset');
        if(!$this->_isModalForm) { // is NOT modal form css style
             $this->addDecorator('HtmlTag', array('tag' => 'div','class' => 'well'));
             $this->addDecorator(array('Obal' => 'HtmlTag'), array('tag' => 'div','class' => 'form-well'));
        }

        $this->_decorators($this->getElements());
        $this->setDisplayGroupDecorators(array(
              'FormElements',
              'Fieldset'
          ));

        parent::init();
    }

    /**
     * Set CZE translate
     * @return Zend_Translate
     */
    protected function _translate()
    {
        $translate = new Zend_Translate('array', array(
                    Zend_Validate_NotEmpty::IS_EMPTY => 'Pole je prázdné. Musíte jej vyplnit',
                    Zend_Validate_EmailAddress::INVALID => 'Chybný datový typ',
                  #  Zend_Validate_EmailAddress::INVALID_FORMAT => '"%value%" není platný e-mail ve formátu jmeno@domena',
                    Zend_Validate_EmailAddress::INVALID_HOSTNAME => '"%hostname%" není platné hostname pro e-mailovou adresu "%value%"',
                    Zend_Validate_EmailAddress::INVALID_MX_RECORD => '"%hostname%" není platným MX záznamem pro e-mailovou adresu "%value%"',
                    Zend_Validate_EmailAddress::DOT_ATOM => '"%localPart%" neodpovídá dot-Atom formátu',
                    Zend_Validate_EmailAddress::QUOTED_STRING => '"%localPart%" neodpovídá Quoted-String formátu',
                    Zend_Validate_EmailAddress::INVALID_LOCAL_PART => '"%localPart%" není platnou lokální částí e-mailové adresy "%value%"',
                    Zend_Validate_EmailAddress::LENGTH_EXCEEDED => '"%value%" přesahuje povolenou délku',
                  #  Zend_Validate_Hostname::INVALID => 'Chybný datový typ',
                    Zend_Validate_Hostname::IP_ADDRESS_NOT_ALLOWED => '"%value%" není povolená IP adresa',
                    Zend_Validate_Hostname::UNKNOWN_TLD => '"%value%" není známá TLD',
                    Zend_Validate_Hostname::INVALID_DASH => '"%value%" obsahuje pomlčku na zakázaném místě',
                    Zend_Validate_Hostname::INVALID_HOSTNAME_SCHEMA => '"%value%" není platným DNS záznamem pro TLD "%tld%"',
                    Zend_Validate_Hostname::UNDECIPHERABLE_TLD => '"%value%" neobsahuje platnou TLD',
                    Zend_Validate_Hostname::INVALID_HOSTNAME => '"%value%" není platným hostname',
                    Zend_Validate_Hostname::INVALID_LOCAL_NAME => '"%value%" není platným názvem umístění v lokální síti',
                    Zend_Validate_Hostname::LOCAL_NAME_NOT_ALLOWED => '"%value%" není povoleným umístěním v lokální síti',
                  #  Zend_Validate_Hostname::CANNOT_DECODE_PUNYCODE => '"%value%" vzhledem k Punycode zápisu nelze dekódovat hostname',
                    Zend_Validate_Identical::NOT_SAME => 'TOKEN: Řetězce si neodpovídají',
                    Zend_Validate_Identical::MISSING_TOKEN => 'TOKEN: Chybějící znaky',
                    Zend_Validate_Digits::NOT_DIGITS => '"%value%" není číslo',
                    Zend_Validate_File_Extension::FALSE_EXTENSION => 'Soubor "%value%" má chybnou příponu',
                    Zend_Validate_File_Size::TOO_BIG => 'Maximální povolená velikost souboru "%value%" je "%max%" ale soubor má velikost "%size%"',
                    Zend_Validate_File_Upload::INI_SIZE => "Soubor '%value%' překročil limit"
                        ), 'en');

        return $translate;
    }

    /**
     * Default decorator for elements
     * Add prefix path of custom decorator of all elements without Submit
     * Submit have own decorator inherited from ViewHelper
     * @param object $elements
     */
    protected function _decorators($elements)
    {
        foreach ($elements as $element)
        {
            if($element instanceof ZendX_JQuery_Form_Element_UiWidget){
                $element->addPrefixPath('ZendX_JQuery_Form_Decorator', 'ZendX/JQuery/Form/Decorator/', 'decorator');
                $element->setDecorators(array(
                    array('UiWidgetElement', array('tag' => '')),
                    array('Label', array('tag' => 'div'))
                ));
            }
            elseif ($element instanceof Zend_Form_Element_File) {
              $element->addPrefixPath('Meduse_Form_Decorator', 'Meduse/Form/Decorator/', 'decorator');
              $element->setDecorators(
                array(
                  'File',
                  'Errors',
                  array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                  array('Label', array('tag' => 'th')),
                  array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                )
              );
            }
            else if (!$element instanceof Zend_Form_Element_Submit) {
                $element->addPrefixPath('Meduse_Form_Decorator', 'Meduse/Form/Decorator/', 'decorator');
                $element->setDecorators(array($this->_decoratorFileName));
                if (($element instanceof Zend_Form_Element_Password
                        || $element instanceof Meduse_Form_Element_Password)
                        && $this->passwordShowingIncluded == FALSE) {
                  $element->addDecorator('TwitterBootstrapPassword');
                  $this->passwordShowingIncluded = TRUE;
                }
            } else  {
                $element->removeDecorator('DtDdWrapper');
                $element->addDecorator('HtmlTag', array('tag' => 'div', 'class' => 'form-button-group'));
                $hasClass = $element->getAttrib('class');
                if(empty($hasClass)) {
                    $element->setAttrib('class', 'btn btn-primary');
                }
                $element->setOrder(99);
            }
        }
    }

    /**
     * Set decorator filename
     * @param string $name
     */
    protected function _setDecoratorFileName($name)
    {
        $this->_decoratorFileName = $name;
    }
}


