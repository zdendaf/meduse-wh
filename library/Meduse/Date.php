<?php

  /**
   * @author Zdeněk Filipec <zdendaf@gmail.com>
   */
  class Meduse_Date extends Zend_Date {

    protected $_holidays = array(
      '01/01', '01/05', '08/05', '05/07', '06/07',
      '28/09', '28/10', '17/11', '24/12', '25/12', '26/12',
    );
    protected $_freedays = array();

    public function __construct($date = null, $part = null) {
      self::setOptions(array('format_type' => 'php'));
      parent::__construct($date, $part, 'cs_CZ');
    }

    public static function dbToForm($dateString) {
      $retVal = NULL;
      if ($dateString) {
        self::setOptions(array('format_type' => 'php'));
        $date = new Zend_Date($dateString, 'Y-M-d');
        $retVal = $date->toString('d.m.Y');
      }
      return $retVal;
    }

    public static function formToDb($dateString) {
      $retVal = NULL;
      if ($dateString) {
        self::setOptions(array('format_type' => 'php'));
        $date = new Zend_Date($dateString, 'd.m.Y');
        $retVal = $date->toString('Y-m-d');
      }
      return $retVal;
    }

    public function getHolidays() {
      return $this->_holidays;
    }

    public function addHolidays(array $holidays) {
      $this->_holidays = array_merge($this->_holidays, $holidays);
    }

    public function isHoliday() {
      // vypocet Velkeho patku
      $year = (int) $this->get('Y');
      $easter = easter_date($year) + (60 * 60 * 24); // plus jeden den = velikonocni pondeli
      if ($this->toString('d/m') == date('d/m', $easter)) {
        return TRUE;
      }
      $easter -= (60 * 60 * 24 * 3); // minus tri dny = velky patek
      if ($this->toString('d/m') == date('d/m', $easter)) {
        return TRUE;
      }
      else {
        return $this->get('N') == '7' ? TRUE : in_array($this->toString('d/m'), $this->_holidays);
      }
    }

    public function isFreeday() {
      return $this->get('N') == '6' ? TRUE : in_array($this->toString('d/m'), $this->_freedays);
    }

    /**
     * Vrátí počet pracovních dnů do koncového data.
     *
     * @param Meduse_Date $endDate
     *    Koncové datum, do kterého se počet dnů počítá. Koncové datum musí
     *    být větší než datum objektu.
     * @param bool $includeEnd
     *    Volitelný parametr, určuje zda-li se má koncové datum započítávat.
     *    Výchozí hodnota je FALSE, tedy nezapočítává se.
     * @return int Počet pracovních dnů do daného koncového data.
     */
    public function getCountWorkingDaysTo(Meduse_Date $endDate, $includeEnd = FALSE) {
      $days = 0;
      $day = new Meduse_Date($this->get('Y-m-d'));
      $end = new Meduse_Date($endDate->get('Y-m-d'));
      while($day < $end || ($day == $end && $includeEnd)) {
        if (!$day->isFreeday() && !$day->isHoliday()) {
          $days++;
        }
        $day->addDay(1);
      }
      return (int) $days;
    }

    public static function pluralDen($number) {
      $abs = abs($number);
      if ($abs == round($abs)) {
        if ($abs == 1) { return $number . ' den'; }
        elseif ($abs >= 2 && $abs <= 4) { return $number . ' dny'; }
        else { return $number . ' dnů'; }
      }
      else {
        return $number . ' dne';
      }

    }
  }

