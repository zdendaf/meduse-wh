<?php

class Meduse_Controller_Action extends Zend_Controller_Action
{

    /** @var Zend_Controller_Action_Helper_FlashMessenger */
    protected $_flashMessenger;

    /**
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db;

    final public function preDispatch() {
        parent::preDispatch();

        $this
            ->getResponse()
            ->setHeader('Cache-Control', 'private, max-age=604800');

        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->notice = $this->_flashMessenger->getMessages();

        //tady checkovat prihlasovaci udaje atd.
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity() !== true && $this->_request->getParam('controller') != 'login') {
            $this->_redirect("login");
        } else {

            $params = '';
            foreach ($this->_request->getParams() as $key => $value) {
                if ($key != 'module' && $key != 'controller' && $key != 'action' && !is_object($value) && !is_array($value)) {
                    $params .= $key . '=' . $value . ", ";
                }
            }

            if (is_null($auth->getIdentity())) {
                $username = $_SERVER['REMOTE_ADDR'];
                $params .= ", " . $_SERVER['HTTP_USER_AGENT'];
            } else {
                $username = $auth->getIdentity();
                $authNamespace = new Zend_Session_Namespace('Zend_Auth');
                if (!Meduse_ipChecker::checkIP($auth->getIdentity(), $_SERVER['REMOTE_ADDR']) && !$authNamespace->passwordExpired) {

                    $hash = Meduse_ipChecker::insertIP($auth->getIdentity(), $_SERVER['REMOTE_ADDR']);
                    if (in_array('admin', $authNamespace->user_role) 
                      || in_array('manager', $authNamespace->user_role)) {
                        $this->_flashMessenger->addMessage("CHYBA: byly narušeny bezpečnostní podmínky využívání systému.<br />Během několika minut vám bude doručen e-mail s instrukcemi. V případě, že e-mail nepříjde opakujte přihlášení.");
                    } else {
                        $this->_flashMessenger->addMessage("CHYBA: byly narušeny bezpečnostní podmínky využívání systému.");
                    }

                    $tEmails = new Table_Emails();
                    $tEmails->sendEmail(Table_Emails::EMAIL_NEW_LOCATION, array('ip' => $_SERVER['REMOTE_ADDR'], 'hash' => $hash));

                    $auth->clearIdentity();
                    $this->_redirect('/login');
                }

                $resource = strtolower($this->_request->getParam('controller') . '/' . $this->_request->getParam('action'));
                //uzivatel ma identitu => kontrola opravneni
                $acl = Zend_Registry::get('acl');
                if (!$acl->existsResource($resource)) {
                    Zend_Registry::get('db')->insert('users_acl', array(
                        'role' => new Zend_Db_Expr("NULL"),
                        'resource' => $resource,
                        'description' => 'definice resource ( automat )',
                    ));
                    Zend_Registry::get('db')->insert('users_acl', array(
                        'role' => 'admin',
                        'resource' => $resource,
                        'description' => 'prirazeni resource ( automat )',
                    ));
                    Zend_Registry::set('acl', new Meduse_Acl()); //nacte do acl v registrech novy resource
                }
                if (!$acl->isAllowed($resource)) {
                    $this->_flashMessenger->addMessage("Nemáte dostatečné oprávnění pro požadovanou akci.");
                    //die($this->_request->getParam('controller').'/'.$this->_request->getParam('action')); //dbg pri povolovani prav
                    $this->_redirect("index");
                    return;
                }
            }

            $action = $this->_request->getParam('controller') . '/' . $this->_request->getParam('action');
            Zend_Registry::get('db')->insert('users_action_log', array(
                'username' => $username,
                'action' => $action,
                'params' => $action == 'login/process' ? '' : $params
            ));
        }

        //kontrola parametrů
        $fc = Zend_Controller_Front::getInstance();

        foreach ($fc->getRequest()->getParams() as $name => $value) {
            if (!is_object($value) && !is_array($value)) {
                $value = strip_tags($value);
                $value = str_replace(array("'", '"', ';', '\\'), '', $value);
                $value = trim($value);
                $fc->getRequest()->setParam($name, $value);
            }
        }

        $this->_db = Zend_Registry::get('db');
    }

}
