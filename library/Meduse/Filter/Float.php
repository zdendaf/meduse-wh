<?php



  /**
   * Description of Float
   *
   * @author Zdenek
   */
  class Meduse_Filter_Float implements Zend_Filter_Interface {
    public function filter($value) {
      $retval = str_replace(',', '.', $value);
      return (float) str_replace(' ', '', $retval);
    }  
  }
