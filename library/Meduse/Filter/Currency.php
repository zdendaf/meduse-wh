<?php



  /**
   * Description of Float
   *
   * @author Zdenek
   */
  class Meduse_Filter_Currency implements Zend_Filter_Interface {
    public function filter($value) {
      $float = (float) str_replace(' ', '', strtr((string) $value, ',', '.'));
      return number_format($float, 5, ".", "");
    }
  }
