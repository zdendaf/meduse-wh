<?php

class Meduse_Filter_Phone implements Zend_Filter_Interface {

  /**
   * @inheritDoc
   */
  public function filter($value) {

    $value = trim($value);

    if (!empty($value) && strpos($value, '00') === 0) {
      $value = '+' . substr($value, 2);
    }

    if (!empty($value) && strpos($value, '+') === FALSE) {
      $value = '+420 ' . $value;
    }

    $chunks = explode(' ', $value);
    $chunks = array_map(function($item) {
      return trim($item);
    }, $chunks);
    $chunks = array_filter($chunks, function($item) {
      return !empty($item);
    });

    return implode(' ', $chunks);
  }

}