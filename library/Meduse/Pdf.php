<?php
class Meduse_Pdf extends Zend_Pdf {

	protected $_coords = array(0, 0);
	protected $_page = 0;
	protected $_style;
	protected $_align = 'left';

  const BLANK_PDF_PATH = '../public/pdf/empty.pdf';

	const LINE_HEIGHT = 1.5;
	
    const TEXT_ALIGN_LEFT   = 'left';
    const TEXT_ALIGN_RIGHT  = 'right';
    const TEXT_ALIGN_CENTER = 'center';	
	
	/**
	 * prekryti rodicovske inicializacni funkce
	 * @param type $source PDF podkladova sablona, max. verze PDF 1.4 ! 
	 * @param type $revision
	 * @return \self
	 */
	public static function load($source = '/pdf/template/invoice.pdf', $revision = NULL) {
		return new self($source, $revision, true);
	}
	
	public function getCoords() {
		return $this->_coords;
	}
	
	public function setCoords($coords) {
		$this->_coords = $coords;
	}
	
	public function getPageNumber() {
		return $this->_page;
	}
	
	public function setPageNumber($page) {
		$this->_page = $page;
	}	

	public function getPage() {
		return $this->pages[$this->_page];
	}
	
	public static function getDefaultStyle() {
		$style = new Zend_Pdf_Style();
		$font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/../library/Meduse/Pdf/Fonts/arial.ttf');
		$style->setFont($font, 8);
    $style->setFontSize(8);
		$style->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    $style->setLineWidth(0.25);
		return $style;
	}
  
	public static function getSmallStyle() {
		$style = new Zend_Pdf_Style();
    $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/../library/Meduse/Pdf/Fonts/arial.ttf');
		$style->setFont($font, 7);
		$style->setFontSize(7);
		$style->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    $style->setLineWidth(0.25);
		return $style;
	}  

	public static function getBoldStyle() {
		$style = new Zend_Pdf_Style();
		$font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/../library/Meduse/Pdf/Fonts/arialbd.ttf');
    $style->setFont($font, 8);
		$style->setFontSize(8);
		$style->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    $style->setLineWidth(0.25);
		return $style;
	}	
	
	public static function getTitleStyle() {
		$style = new Zend_Pdf_Style();
		$font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/../library/Meduse/Pdf/Fonts/arialbd.ttf');
    $style->setFont($font, 10);
		$style->setFontSize(10);
		$style->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    $style->setLineWidth(0.25);
		return $style;
	}
	
	public function fillText($text, $coords, $align = Meduse_Pdf::TEXT_ALIGN_LEFT, $lineHeight = self::LINE_HEIGHT) {
		$this->_draw($text, Meduse_Pdf::getDefaultStyle(), $coords, $align, $lineHeight);
	}
	
	public function fillTextBold($text, $coords, $align = Meduse_Pdf::TEXT_ALIGN_LEFT, $lineHeight = self::LINE_HEIGHT) {
		$this->_draw($text, Meduse_Pdf::getBoldStyle(), $coords, $align, $lineHeight);
	}
	
	public function fillTitle($text, $coords, $align = Meduse_Pdf::TEXT_ALIGN_LEFT, $lineHeight = self::LINE_HEIGHT) {
		$this->_draw($text, Meduse_Pdf::getTitleStyle(), $coords, $align, $lineHeight);
	}
	
	/**
     * Return length of generated string in points
     *
     * @param string $string
     * @param Zend_Pdf_Resource_Font $font
     * @param int $font_size
     * @return double
     */
    public static function getTextWidth($text, Zend_Pdf_Resource_Font $font, $font_size) {
        $drawing_text = iconv('UTF-8', 'UTF-16BE', $text);
        $characters    = array();
        for ($i = 0; $i < strlen($drawing_text); $i++) {
            $characters[] = (ord($drawing_text[$i++]) << 8) | ord ($drawing_text[$i]);
        }
        $glyphs        = $font->glyphNumbersForCharacters($characters);
        $widths        = $font->widthsForGlyphs($glyphs);
        $text_width   = (array_sum($widths) / $font->getUnitsPerEm()) * $font_size;
        return $text_width; 
    }
	
	
	
	protected function _draw($text, $style = null, $coords = null, $align = null, $lineHeight = self::LINE_HEIGHT, $pageNum = null) {
		if (!is_array($text))   { $text = array($text); }
		if (!is_null($coords))  { $this->_coords = $coords; }
		if (!is_null($align))   { $this->_align = $align; }
		if (!is_null($pageNum)) { $this->_page = $pageNum; }
		if (!is_null($style))   { $this->_style = $style; }
		$page = $this->pages[$this->_page];
		$page->setStyle($this->_style);
		$h = $page->getFontSize();
		
		foreach ($text as $c => $line) {			
			switch ($this->_align) {
				case Meduse_Pdf::TEXT_ALIGN_CENTER:
					$width = Meduse_Pdf::getTextWidth($line, $this->_style->getFont(), $h);
					$x = $this->_coords[0] - $width/2;
					break;
				case Meduse_Pdf::TEXT_ALIGN_RIGHT:
					//Zend_Debug::dump($this->_style->getFont()); die;
					$width = Meduse_Pdf::getTextWidth($line, $this->_style->getFont(), $h);
					$x = $this->_coords[0] - $width;
					break;
				case Meduse_Pdf::TEXT_ALIGN_LEFT:
				default:
					$x = $this->_coords[0];
					break;				
			}
			$page->drawText($line, $x, $this->_coords[1], 'UTF-8');		
			$this->_coords[1] -= ($h * $lineHeight);
		}		
	}
	
	protected function getLineHeightPixels($lineHeight) {
		$page = $this->pages[$this->_page];
		$page->setStyle($this->_style);
		$h = $page->getFontSize();
		return $h * $lineHeight;
	}
}

