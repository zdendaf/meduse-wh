<?php
class Meduse_Form extends Zend_Form {
	
	private $style;
	
	private $description;
	
    public function __construct ($style, $description, $options = null){
        parent::__construct($options);
        
        $this->style = $style;
        
        $this->description = $description;
        
        $this->setDecorators(array(
		    'FormElements',
        	array(array('form_cnt' => 'HtmlTag'), array('tag' => 'table', 'class' => 'form_table')),
		    'Form',
        	array(array('form_wrap' => 'HtmlTag'), array('tag' => 'div', 'class' => 'box '.$this->style)),
        	array('Description', array('tag' => 'div', 'class' => 'box_header '.$this->style)),
		));
		$this->getDecorator('Description')->setOption('placement', 'prepend');
		
		$this->setDescription($this->description);		
        
    }
	
}
