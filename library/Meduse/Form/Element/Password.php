<?php
class Meduse_Form_Element_Password extends Zend_Form_Element_Password{
	
    public function __construct ($options = null){
        parent::__construct($options);
        
        $this->setDecorators(array(
		    'ViewHelper',
		    'Description',
		    'Errors',
		    
			array(array('data' => 'HtmlTag'), array('tag' => 'td', 'style' => 'text-align: left;')),
		    array('Label', array('tag' => 'td')),
		    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'tr')),
		    
		));
		
		$this->getDecorator('Label')->setRequiredSuffix(' *');
        
        
        
    }
    
}
