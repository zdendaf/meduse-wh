<?php
class Meduse_Form_Element_Collections extends Zend_Form_Element_MultiCheckbox{

    public function __construct ($options = null){
        parent::__construct($options);

        $this->setDecorators(array(
		    'ViewHelper',
		    'Description',
		    'Errors',

		    array(array('data2' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element_select')),
		    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'style' => 'text-align: left;')),
		    array('Label', array('tag' => 'td')),
		    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'tr')),

		));

		$this->getDecorator('Label')->setRequiredSuffix(' *');

		$db = Zend_Db_Table::getDefaultAdapter();
		$rows = $db->fetchAll($db->select()->from('collections'));
		$this->setLabel('Kolekce');
		$checked = array();
    	foreach($rows as $row){
			$this->addMultiOption($row['id'], " ".$row['name']);
			$checked[] = $row['id'];
    	}
		$this->setValue($checked);

    }

}