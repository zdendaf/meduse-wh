<?php
class Meduse_Form_Element_Submit extends Zend_Form_Element_Submit{
    public function __construct ($options = null){
    	parent::__construct($options);
    	
    	$this->setDecorators(array(
		    'ViewHelper',
		    'Description',
		    'Errors',
		    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'colspan' => '2', 'style' => 'text-align: center; padding-top: 1em;')),
		    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'tr')),
		    
		));
    	
    }
}
    