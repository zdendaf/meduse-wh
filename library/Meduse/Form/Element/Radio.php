<?php
class Meduse_Form_Element_Radio extends Zend_Form_Element_Radio{

    public function __construct ($options = null){
        parent::__construct($options);



        $this->setDecorators(array(
		    'ViewHelper',
		    'Description',
		    'Errors',

		    //array(array('data2' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element_text')),
		    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'style' => 'text-align: left; padding-top: 0.5em;')),
		    array('Label', array('tag' => 'td')),
		    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'tr')),

		));

		$this->getDecorator('Label')->setRequiredSuffix(' *');

    }

    public function setValue($value) {
      parent::setValue($value);
  }
}
