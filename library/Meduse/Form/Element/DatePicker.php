<?php
class Meduse_Form_Element_DatePicker extends ZendX_JQuery_Form_Element_DatePicker{

    public function __construct ($name, $options = null) {
		
		$options['jQueryParams']['changeMonth'] = 'true';
		$options['jQueryParams']['changeYear'] = 'true';
		
        parent::__construct($name, $options);

		//Zend_Debug::dump($this->getDecorators());  die();

        $this->setDecorators(array(
		    'UiWidgetElement',
		    'Description',
		    'Errors',

		    //array(array('data2' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element_text')),
		    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'style' => 'text-align: center;')),
		    array('Label', array('tag' => 'td')),
		    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'tr')),

		));

		$this->getDecorator('Label')->setRequiredSuffix(' *');

    }

}
