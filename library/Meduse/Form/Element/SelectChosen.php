<?php
/**
 * Select Chosen - jQuery plugin
 * @version 1.0
 * @link http://harvesthq.github.io/chosen/
 * set javascript for chosen element
 * TODO:: implement chosen option if is needed
 */
class Meduse_Form_Element_SelectChosen extends Zend_Form_Element_Select
{
    public function init()
    {     
        // set chosen attribs and classes
        $this->setAttrib('class', 'chosen-select');
        $this->setAttrib('data-placeholder', 'vyber..');
        
        // set chosen javascript
        $this->javascript = '<script> $("#' . $this->getName() . '").chosen(); </script>';


        parent::init();
    }
    
}