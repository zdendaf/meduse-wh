<?php

class Meduse_Form_Element_Phone extends Meduse_Form_Element_Text {

  private $attachedScripts = [];

  public function init() {
    $id = $this->getName();
    $script = "<script>
    (function($, w, d, c){
      const input = d.querySelector('#{$id}');
      c.debug('id: ', '{$id}', 'iti: w.intlTelInput');  
        if (typeof w.intlTelInput !== 'undefined' && typeof input !== 'undefined') {
          c.debug('iti input: ', input);  
          const iti = w.intlTelInput(input, {
            initialCountry: 'cz',
            utilsScript: '/public/js/iti/utils.js',
            separateDialCode: false
          });
          const jqInput =  $('#{$id}');
          const jqForm = input.closest('form');
          const normalizeNumber = function (jqInput) {
            const 
              number = iti.getNumber(),
              valid = iti.isValidNumber();
            c.debug('number: ', number, 'isValid: ', valid);
           
            if (number  === '') {
              jqInput.removeClass('invalid').removeClass('valid');
              return;
            }
            
            if (valid) {
              jqInput.removeClass('invalid').addClass('valid');
            }
            else {
              jqInput.removeClass('valid').addClass('invalid');
            }
            jqInput.val(number);
            return valid;
          }
          $(jqInput).on('blur', function() {
               normalizeNumber(jqInput);
          });
          $(jqForm).on('submit', function() {
               return normalizeNumber(jqInput);
          });
        }
    })(jQuery, window, document, console);
    </script>";
    $this->attachScript($script);
  }

  public function attachScript($script, $key = null) {
    $this->attachedScripts[$key] = $script;
  }

  public function render(Zend_View_Interface $view = NULL) {
    $content = parent::render($view);
    if ($this->attachedScripts) {
      foreach ($this->attachedScripts as $script) {
        $content .= PHP_EOL . $script;
      }
    }
    return $content;
  }

}