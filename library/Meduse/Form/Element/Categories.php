<?php

class Meduse_Form_Element_Categories extends Zend_Form_Element_Select
{

  public function __construct($options = null)
  {
    parent::__construct($options);

    $this->setDecorators(array(
      'ViewHelper',
      'Description',
      'Errors',

      array(array('data2' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element_select')),
      array(array('data' => 'HtmlTag'), array('tag' => 'td', 'style' => 'text-align: left;')),
      array('Label', array('tag' => 'td')),
      array(array('elementDiv' => 'HtmlTag'), array('tag' => 'tr')),

    ));

    $this->getDecorator('Label')->setRequiredSuffix(' *');

    //kategorie
    $type = $options['type'] ?? 'pipes';
    $db = Zend_Db_Table::getDefaultAdapter();
    $select = $db->select();
    $select->from('parts_ctg');
    switch ($type) {
      case 'tobacco':
        $select->where("parent_ctg = ?", Table_TobaccoWarehouse::CTG_TOBACCO);
        break;

      case 'tobacco_materials':
        $select->where("parent_ctg = ?", Table_TobaccoWarehouse::CTG_MATERIALS);
        break;

      default:
        $select->where("parent_ctg IS NULL");
        if ($type == 'pipes') {
          $select->where('id_wh_type = ?', 1);
        }
        break;
    }

    $select->order('name');
    $parents = $db->fetchAll($select);

    $s = $db->select()
      ->from('parts_ctg', array('id', 'name' => 'CONCAT(name)'))
      ->where('parent_ctg IS NULL')
      ->order('name');
    if ($type != 'tobacco_materials' && $type != 'tobacco') {
      $s->where('id NOT IN (?)', array(16, 20, 23,
        Table_TobaccoWarehouse::CTG_TOBACCO,
        Table_TobaccoWarehouse::CTG_MATERIALS));
    }
    $rows = $db->fetchPairs($s);

    $this->setLabel('Kategorie');
    if (isset($options['all']) && $options['all']) {
      $this->addMultiOption('null', '- všechny -');
    }
    $this->addMultiOption('23',
      $db->fetchOne("SELECT name FROM parts_ctg WHERE id = 23"));
    $this->addMultiOption('20',
      $db->fetchOne("SELECT name FROM parts_ctg WHERE id = 20"));
    $this->addMultiOption('16',
      $db->fetchOne("SELECT name FROM parts_ctg WHERE id = 16"));

    foreach ($parents as $row) {
      if ($row['parent_ctg'] === null) {
        $subCtg = $db->fetchAll($db->select()
          ->from('parts_ctg')
          ->where("parent_ctg = ?", $row['id'])
          ->order("name"));
        $this->addMultiOption($row['id'], $row['name']);
        foreach ($subCtg as $subRow) {
          $this->addMultiOption($subRow['id'], "- " . $subRow['name']);
        }
      }
    }
    $this->addMultiOptions($rows);

  }

}