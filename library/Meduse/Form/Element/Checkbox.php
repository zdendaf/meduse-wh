<?php
class Meduse_Form_Element_Checkbox extends Zend_Form_Element_Checkbox{
	
    public function __construct ($options = null){
        parent::__construct($options);
        
        $this->setDecorators(array(
		    'ViewHelper',
		    'Description',
		    'Errors',
		    
		    //array(array('data2' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element_text')),
		    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'style' => 'text-align: center;')),
		    array('Label', array('tag' => 'td')),
		    array(array('elementDiv' => 'HtmlTag'), array('tag' => 'tr')),
		    
		));
		
		$this->getDecorator('Label')->setRequiredSuffix(' *');
		
		$this->setCheckedValue('y');
		$this->setUncheckedValue('n');
        
    }
    
}
