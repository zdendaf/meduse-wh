<?php
/**
 * Form decorator for Twitter Bootstrap
 * 
 * @author Jakub Hantak
 * @since 2015-10-09
 * @uses Zend_Form_Decorator_Abstract
 *  
 * Contains data for rendering 
 * Support HTML5 feature
 */
require_once 'Zend/Form/Decorator/Abstract.php';

class Meduse_Form_Decorator_TwitterBootstrapPassword extends Zend_Form_Decorator_Abstract
{
    public function render($element = '')
    {  
      return '<script>
                $(document).ready(function() {
                  $(".add-on-password").addClass("btn");

                  $(".add-on-password").click(function() {
                    if ($(this).find("i").hasClass("icon-eye-close")) {
                      $(this).find("i").removeClass("icon-eye-close");
                      $(this).find("i").addClass("icon-eye-open");
                      $(this).siblings("input").prop("type", "text");
                    } else {
                      $(this).find("i").removeClass("icon-eye-open");
                      $(this).find("i").addClass("icon-eye-close");
                      $(this).siblings("input").prop("type", "password");
                    }
                  });
                });
              </script>' . $element;
    }
}