<?php
/**
 * Form decorator for Twitter Bootstrap
 *
 * @author Jakub Hantak
 * @since 2013-10-09
 * @uses Zend_Form_Decorator_Abstract
 *
 * Contains a rendering for a specific element
 * Support HTML5 feature
 */
require_once 'Zend/Form/Decorator/Abstract.php';

class Meduse_Form_Decorator_TwitterBootstrap extends Zend_Form_Decorator_Abstract
{
    private $_script = null;
    private $_html = null;
    private $_apended = null;

    /**
     * Generate attribs line for html element
     * @param object $element
     * @return string
     */
    private function _generateAttribs($element)
    {
        $attribs = '';
        if(count($element->getAttribs()) > 0) {
            foreach($element->getAttribs() as $key => $val) {
                // fix for checkbox or radio
                if($key == 'checked' && $val == '1') {
                    $attribs .= $key . '="' . $key . '"';
                }
                else if($key != 'options' && $key != 'checked') {
                    $attribs .= $key . '="' . $val . '"';
                }
            }
        }

        return $attribs;
    }


    /**
     * Build label
     * : will be automatically added to the end of name
     */
    public function buildLabel()
    {
        $element = $this->getElement();
        $label = $element->getLabel();
        if ($translator = $element->getTranslator()) {
            $label = $translator->translate($label);
        }

        if ($element->isRequired()) {
            $label .= '*';
        }
        $label .= ':';

        if($label == "*:" || $label == ":") {
            $label = "";
        }

        if(!empty($label)) {
            return $element->getView()->formLabel($element->getName(), $label);
        }
        else {
            return '';
        }
    }

    public function buildLabelRequiredSparkImg()
    {
        $label = $this->buildLabel();
        $new_label = str_replace('*:', ' <span class="required-spark">*</span>:', $label);

        return $new_label;
    }

    public function buildInput()
    {
        $this->setHTML5RequiredAttribute();

        $element = $this->getElement();

        $helper  = $element->helper;

        // radio
        if($element instanceof Zend_Form_Element_Radio
                || $element instanceof Meduse_Form_Element_Radio) {
           return $this->buildRadioElement($element);
        }

        if ($element instanceof Meduse_Form_Element_Select && $element->getAttrib('readonly') == 'readonly') {
          return $this->buildReadonlySelectElement($element);
        }

        // checkbox
        if ($element instanceof Zend_Form_Element_Checkbox
          || $element instanceof Meduse_Form_Element_Checkbox) {
           return $this->buildCheckboxElement($element);
        }

        // formMultiCheckbox
        if($element instanceof Zend_Form_Element_MultiCheckbox
                || $element instanceof Meduse_Form_Element_MultiCheckbox) {
           return $this->buildMultiCheckboxElement($element);
        }

        // formMultiCheckbox
        if($element instanceof Zend_Form_Element_Password
                || $element instanceof Meduse_Form_Element_Password) {
           return $this->buildPasswordElement($element);
        }

        // formSelectChosen
        if($element instanceof Meduse_Form_Element_SelectChosen) {
           $script = $element->getAttrib('javascript');
           if(isset($script)) {
                $this->_script = $element->getAttrib('javascript'); // set script for render
                $element->setAttrib('javascript', null); // remove script from list attribs
           }
        }

        // add css bootstrap style big
        if(!$element->getAttrib('class')) {
            $element->setAttrib('class', 'span4');
        }

        // add custom html after element
        $html = $element->getAttrib('html');
        if(isset($html)) {
            $this->_html = $html;
        }

        $apended = $element->getAttrib('apended');
        if(isset($apended)) {
            $this->_apended = $apended;
            $element->setAttrib('apended', NULL);
        }

        return $element->getView()->$helper(
            $this->getName(),
            $element->getValue(),
            $element->getAttribs(),
            $element->options
        );
    }

    public function buildRadioElement($element) {
      $value = $element->getValue();
      if (count($element->options) > 0) {
        $elements = '<div style="float: left">';
        foreach($element->options as $key => $val) {
          $elements .= '<label class="radio"><input type="radio" name="' . $this->getName() . '" id="' . $this->getName() . '-' . $key .'" value="' . $key . '" ' . $this->_generateAttribs($element) . ($value == $key ? 'checked="checked"' : '') . '>' . $val . '</label><br>';
        }
        $elements .= '</div><div style="clear:both"></div>';
      }
      else {
        $elements = '<input type="radio" name="' . $this->getName() . '" id="' . $this->getName() . '" ' . $this->_generateAttribs($element) . ($value == TRUE ? 'checked="checked"' : '') . '><div style="clear:both"></div>';
      }
      return $elements;
    }

    public function buildReadonlySelectElement($element) {
      $value = $element->getValue();
      $html = '<select readonly="readonly">';
      if ($options = $element->getMultiOptions()) {
        foreach ($options as $key => $val) {
          $selected = $value == $key ? 'selected="selected"' : '';
          $html .= '<option ' . $selected . ' value="' . $key . '" disabled="disabled">' . $val . '</option>';
        }
      }
      $html .= '</select>';
      $html .= '<input value="' . $value . '" type="hidden" name="' . $element->getName() .  '" id="' . $element->getName() .  '">';
      return $html;
    }

    public function buildMultiCheckboxElement($element)
    {
		$values = $element->getValue();
		if (!is_array($values)) {
			$values = array($values);
		}
        $elements = '<div style="float: left">';

        if(count($element->options) > 0) {
            foreach($element->options as $key => $val) {
                $elements .= '<label class="checkbox">';
				$elements .= '<input type="checkbox" name="' . $this->getName() . '" id="' . substr($this->getName(), 0, -2) . '-' . $key;
				$elements .= '" value="' . $key . '" ' . $this->_generateAttribs($element);
				$elements .= in_array($key, $values)? ' checked="checked"' : '';
				$elements .= '>' . $val . '</label><br>';
            }
        }
        $elements .= '</div><div style="clear:both"></div>';

        return $elements;
    }

    public function buildCheckboxElement($element) {
        if ($element->getAttrib('readonly')) {
          $element->setAttrib('disabled', 'disabled');
          $html =
              '<input type="hidden" value="' . $element->getValue() .  '" name="' . $this->getName() . '" id="' . $this->getName() . '" >'
            . '<input type="checkbox" name="' . $this->getName() . '_visual" id="' . $this->getName() . '_visual" ' . $this->_generateAttribs($element) . '>'
            . '<div style="clear:both"></div>';
        }
        else {
          $html =
              '<input type="hidden" value="n" name="' . $this->getName() . '" id="' . $this->getName() . '_n" ' . $this->_generateAttribs($element) . '>'
            . '<input type="checkbox" value="y" name="' . $this->getName() . '" id="' . $this->getName() . '" ' . $this->_generateAttribs($element) . '>'
            . '<div style="clear:both"></div>';
        }
        return $html;
    }

    public function buildPasswordElement($element) {
      return  '<div class="input-prepend">'
              . '<span class="add-on add-on-password"><i class="icon-eye-close"></i></span>'
              . '<input type="password" value="' . $element->getValue() . '" name="' . $this->getName() . '" id="' . $this->getName() . '" ' . $this->_generateAttribs($element) . '>'
            . '</div>';
    }

    public function buildErrors()
    {
        $element  = $this->getElement();
        $messages = $element->getMessages();
        if (empty($messages)) {
            return '';
        }

        return '<span class="form-errors">' .
               $element->getView()->formErrors($messages) . '</span>';
    }

    public function buildDescription()
    {
        $element = $this->getElement();
        $desc    = $element->getDescription();
        if (empty($desc)) {
            return '';
        }

        return '<span id="description-' . $this->getElement()->getName() . '" class="form-description">' . $desc . '</span>';
    }

    public function setHTML5RequiredAttribute()
    {
        $element = $this->getElement();

        if($element->isRequired()) {
            $element->setAttribs(array(
                'required' => 'required'
            ));
        }
    }

    public function getName()
    {
        if (null === ($element = $this->getElement())) {
            return '';
        }

        $name = $element->getName();

        if (!$element instanceof Zend_Form_Element) {
            return $name;
        }

        if (null !== ($belongsTo = $element->getBelongsTo())) {
            $name = $belongsTo . '['
                  . $name
                  . ']';
        }

        if ($element->isArray()) {
            $name .= '[]';
        }

        return $name;
    }

    public function render($content)
    {
        $element = $this->getElement();
        if (!$element instanceof Zend_Form_Element) {
            return $content;
        }
        if (null === $element->getView()) {
            return $content;
        }

        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
        $label     = $this->buildLabelRequiredSparkImg();
        $input     = $this->buildInput();
        $errors    = $this->buildErrors();
        $desc      = $this->buildDescription();


       /**
        * button or submit element
        */
       if($element instanceof Zend_Form_Element_Button || $element instanceof Zend_Form_Element_Submit) {
           $output = '<span class="form-button">'
                    . $input
                    . '</span>';
           return $content . $output;
       }
       else {
           /**
            *  normal element
            */
            $output = '<div class="form-element" id="form_element_' . $this->getElement()->getName() . '" ' . (($element instanceof Zend_Form_Element_Hidden) ? 'style="display:none"' : '') . '>'
                    . $label
                    . ((!empty($this->_apended)) ? '<div class="input-append">' : '')
                    . $input
                    . ((!empty($this->_apended)) ? $this->_apended . '</div>': '')
                    . '<span class="after-element">'
                    . $desc
                    . $errors
                    . '</span>'
                    . ((!empty($this->_html)) ? $this->_html : '')
                    . ((!empty($this->_script)) ? $this->_script : '')
                    .'</div>';

            switch ($placement) {
                case (self::PREPEND):
                    return $output . $separator . $content;
                case (self::APPEND):
                default:
                    return $content . $separator . $output;
            }
       }
    }
}