<?php
/**
 * Description of access control list
 *
 * @author niecj
 */
class Meduse_Acl extends Zend_Acl {

	const ROLE_ADMIN = 'admin';
	const ROLE_MANAGER = 'manager';
	const ROLE_WAREHOUSEMAN = 'warehouseman';
	const ROLE_SELLER = 'seller';
  const ROLE_BRIGADIER = 'brigadier';
  const ROLE_EXTERN_WHMAN = 'extern_wh';
  const ROLE_TOBACCO_WHMAN = 'tobacco_whman';
  const ROLE_ISSUE_READER = 'issue_reader';
  const ROLE_HEAD_WHMAN = 'head_warehouseman';

	/**
	 * @var Zend_Db_Adapter
	 */
	private $_db = null;
  private $_cache;

	public function  __construct() {

    $config = Zend_Registry::get('config');
    
    $frontendOptions = array(
       'lifetime' => $config->aclCache->lifetime,
       'automatic_serialization' => $config->aclCache->automaticSerialization,
    );
    $backendOptions = array(
        'cache_dir' => $config->aclCache->cacheDir,
    );
    $this->_cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
    
    $this->_db = $db = Zend_Registry::get('db');
     
    //resources
    if (($rows = $this->_cache->load('acl_resources')) === false) {
        $rows = $db->fetchAll("SELECT resource FROM users_acl WHERE role IS NULL");
        $this->_cache->save($rows, 'acl_resources');
    }				
		foreach($rows as $row){
			$this->add(new Zend_Acl_Resource($row['resource']));
		}

		//roles
    if (($rows = $this->_cache->load('acl_roles')) === false) {
        $rows = $db->fetchAll("SELECT role FROM users_acl WHERE resource IS NULL");
        $this->_cache->save($rows, 'acl_roles');
    }
		foreach($rows as $row){
			$this->addRole(new Zend_Acl_Role($row['role']));
		}
		
		//role -> resource
    if (($rows = $this->_cache->load('acl_role_resource')) === false) {
        $rows = $db->fetchAll("SELECT role, resource FROM users_acl WHERE role IS NOT NULL AND resource IS NOT NULL");
        $this->_cache->save($rows, 'acl_role_resource');
    }    
		foreach($rows as $row){
			$this->allow($row['role'], $row['resource']);
		}

	}

	/**
	 *
	 * @param string $resource
	 * @param <type> $privilege
	 * @return bool
	 */
	public function  isAllowed($resource = null, $role = null, $privilege = null) {
		$authNamespace = new Zend_Session_Namespace('Zend_Auth');
		if($role === null){
			if($authNamespace->user_role === null){
				return false;
			} else {
				$role = $authNamespace->user_role;
			}
		}
    $roles = is_array($role)? $role : array($role);
    foreach ($roles as $rolename) {
      if (parent::isAllowed($rolename, $resource, $privilege)) {
        return true;
      }
    }
		return false;
	}
  
	public function existsResource($resource){
		$row = $this->_db->fetchRow("SELECT * FROM users_acl WHERE resource = :r", array('r' => $resource));
		return empty($row) ? false : true;
	}
  
  public function flushCache() {
    $this->_cache->clean(Zend_Cache::CLEANING_MODE_ALL);
  }

}
